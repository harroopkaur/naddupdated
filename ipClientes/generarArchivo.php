<?php 
require_once("../configuracion/inicio.php");
require_once("../clases/clase_general.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $desde = $_POST["desdeIp"];
            $hasta = $_POST["hastaIp"];

            $arrayDesde = explode(".", $desde);
            $arrayHasta = explode(".", $hasta);

            if(!file_exists($GLOBALS['app_root'] . "/ipClientes/ips" . $_SESSION["client_id"])){
                mkdir($GLOBALS['app_root'] . "/ipClientes/ips" . $_SESSION["client_id"]);
            }

            $fp = fopen($GLOBALS['app_root'] . "/ipClientes/ips" . $_SESSION["client_id"] . "/Equipos.txt","w");
            $result =0;

            if($arrayDesde[0] > 0 && $arrayDesde[0] < 255 && $arrayDesde[1] >= 0 && $arrayDesde[1] <= 255 && $arrayDesde[2] >= 0 && $arrayDesde[2] <= 255 && $arrayDesde[3] > 0 && $arrayDesde[3] < 255 && $arrayHasta[0] > 0 && $arrayHasta[0] < 255 && $arrayHasta[1] >= 0 && $arrayHasta[1] <= 255 && $arrayHasta[2] >= 0 && $arrayHasta[2] <= 255 && $arrayHasta[3] > 0 && $arrayHasta[3] < 255){
                    if($arrayDesde[0] == $arrayHasta[0]){
                            if($arrayDesde[1] == $arrayHasta[1]){
                                    if($arrayDesde[2] == $arrayHasta[2]){
                                            for($i = $arrayDesde[3]; $i <= $arrayHasta[3]; $i++){
                                                    if($i < $arrayHasta[3]){
                                                            fwrite($fp, $arrayHasta[0] . "." . $arrayHasta[1] . "." . $arrayHasta[2] . "." . $i  . "," . PHP_EOL);
                                                    }
                                                    else{
                                                            fwrite($fp, $arrayHasta[0] . "." . $arrayHasta[1] . "." . $arrayHasta[2] . "." . $i . PHP_EOL);
                                                    }
                                                    $result = 1;
                                            }
                                    }
                                    else if($arrayDesde[2] < $arrayHasta[2]){
                                            for($j = $arrayDesde[2]; $j <= $arrayHasta[2]; $j++){
                                                    for($i = 1; $i <= 254; $i++){
                                                            if($j != $arrayHasta[2]){
                                                                    fwrite($fp, $arrayHasta[0] . "." . $arrayHasta[1] . "." . $j . "." . $i  . "," . PHP_EOL);
                                                            }
                                                            else if($j == $arrayHasta[2] && $i < $arrayHasta[3]){
                                                                    fwrite($fp, $arrayHasta[0] . "." . $arrayHasta[1] . "." . $j . "." . $i . "," . PHP_EOL);
                                                            }
                                                            else if($j == $arrayHasta[2] && $i == $arrayHasta[3]){
                                                                    fwrite($fp, $arrayHasta[0] . "." . $arrayHasta[1] . "." . $j . "." . $i . PHP_EOL);
                                                            }
                                                    }
                                            }
                                            $result = 1;
                                    }
                            }
                            else if($arrayDesde[1] < $arrayHasta[1]){
                                    for($k = $arrayDesde[1]; $k <= $arrayHasta[1]; $k++){
                                            for($j = 1; $j <= 254; $j++){
                                                    for($i = 1; $i <= 254; $i++){
                                                            if($k < $arrayHasta[1]){
                                                                    fwrite($fp, $arrayHasta[0] . "." . $k . "." . $j . "." . $i  . "," . PHP_EOL);
                                                            }
                                                            else if($k == $arrayHasta[1]){
                                                                    if($j < $arrayHasta[2]){
                                                                            fwrite($fp, $arrayHasta[0] . "." . $k . "." . $j . "." . $i  . "," . PHP_EOL);
                                                                    }
                                                                    else if($j == $arrayHasta[2] && $i < $arrayHasta[3]){
                                                                            fwrite($fp, $arrayHasta[0] . "." . $k . "." . $j . "." . $i . "," . PHP_EOL);
                                                                    }
                                                                    else if($j == $arrayHasta[2] && $i == $arrayHasta[3]){
                                                                            fwrite($fp, $arrayHasta[0] . "." . $k . "." . $j . "." . $i . PHP_EOL);
                                                                    }
                                                            }
                                                    }
                                            }
                                            $result = 1;
                                    }
                            }
                    }
            }
            fclose($fp);

            $zip = new ZipArchive;
            $dir = $GLOBALS['app_root'] . "/ipClientes/ips" . $_SESSION["client_id"] . "/";
            $zip->open($dir."Equipos.zip",ZipArchive::CREATE);
            $zip->addFile($dir."Equipos.txt", "Equipos.txt");

            $zip->close();

            $array = array(0=>array('result'=>$result,'archivo'=>$GLOBALS['domain_root'] . "/ipClientes/ips" . $_SESSION["client_id"] . "/Equipos.zip"));
    }
}
else{
    $general->eliminarSesion();
}
echo json_encode($array);