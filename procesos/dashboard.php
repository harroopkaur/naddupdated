<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_dashboard.php");

$general = new General();
$clientes = new Clientes();
$dash = new clase_dashboard();

$imgDashboard = $clientes->dashboard($_SESSION["client_id"]);
$listado = $dash->presentaciones($_SESSION["client_id"]);

$presentacion1 = "";
$presentacion2 = "";
$presentacion3 = "";
$presentacion4 = "";
$presentacion5 = "";
$presentacion6 = "";
$presentacion7 = "";
$presentacion8 = "";

$urlPresentacion = $GLOBALS["domain_root"] . "/dashboard/" . $_SESSION["client_id"] . "/";
$urlPresentacion1 = $GLOBALS["app_root"] . "/dashboard/" . $_SESSION["client_id"] . "/";

foreach($listado as $row){
    if($row["numero"] == 1 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion1 = $row["presentacion"];
    }
    
    if($row["numero"] == 2 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion2 = $row["presentacion"];
    }
    
    if($row["numero"] == 3 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion3 = $row["presentacion"];
    }
    
    if($row["numero"] == 4 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion4 = $row["presentacion"];
    }
    
    if($row["numero"] == 5 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion5 = $row["presentacion"];
    }
    
    if($row["numero"] == 6 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion6 = $row["presentacion"];
    }
    
    if($row["numero"] == 7 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion7 = $row["presentacion"];
    }
    
    if($row["numero"] == 8 && file_exists($urlPresentacion1 . $row["presentacion"])){
        $presentacion8 = $row["presentacion"];
    }
}
 

/*require_once($GLOBALS["app_root"] . "/clases/clase_fabricantes_activos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_graficos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_dashboard.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$fabricantes = new fabricantesActivos();
$nueva_funcion = new funcionesSam();
$general = new General();
$graficos = new claseGraficos();
$dashboard = new clase_dashboard();
$log = new log();

$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$adobeActivo = 0;
$IBMActivo = 0;
$MicrosoftActivo = 0;
$OracleActivo = 0;
$SAPMostrado = -1;
$UNIXIBMMostrado = -1;
$UNIXOracleMostrado = -1;
$VMWareMostrado = -1;
$controlFabSinGrafico = 0;

$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listarFabricantes = $fabricantes->gantt_inicio($_SESSION["client_id"], $_SESSION["client_empleado"]);

$optionAsignaciones = "<option value=''></option>";
foreach($asignaciones as $row){
    $optionAsignaciones .= "<option value='" . $row["asignacion"] . "'>" . $row["asignacion"] . "</option>";
}

foreach($listarFabricantes as $row){
    $fechaPagoCliente[$row["idFabricante"]] = $fabricantes->gantt_fechas($_SESSION["client_id"], $row["idFabricante"]);
    
    if($row["idFabricante"] == 1){
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_adobe.php");
        
        $detallesAdobe = new DetallesAdobe();
        $balanceAdobe = new balanceAdobe();
        
        $fechaUltEscaneoAdobe = $log->fechaUltimoEscaneo(7, $_SESSION["client_id"]);
        $fechaProximaAdobe = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleAdobe = $balanceAdobe->sobranteFaltante($_SESSION["client_id"]);
        
        $adobeActivo = 1;
        
        $tclientAdobe = 0;
        $tserverAdobe = 0;
        $total_1LAcAdobe = 0;
        $total_2DVcAdobe = 0;
        $total_AdobeActC = 0;
        $total_AbodeDVc = 0;
        $total_1AdobeInacC = 0;
        $total_AdobeActS = 0;
        $total_AdobeDVs = 0;
        $total_1AdobeInacS = 0;
        
        $detallesAdobe->listar_todo1Asignacion($_SESSION['client_id'], "", $asignaciones);

        foreach ($detallesAdobe->lista as $reg_equipos) {
            if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientAdobe++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_AdobeActC++;
                }

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_AbodeDVc ++;
                }
                else{
                    $total_1AdobeInacC++;
                }
            } else {//server
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_AdobeActS++;
                }

                $tserverAdobe++;

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_AdobeDVs++;
                }
                else{
                    $total_1AdobeInacS++;
                }
            }
        }
        $reconciliacionAdobeC = $total_AbodeDVc - $total_AdobeActC;
        $reconciliacionAdobeS = $total_AdobeDVs - $total_AdobeActS;    
        
        $totalGeneralActivoAdobe = $total_AdobeActC + $total_AdobeActS;
        $totalGeneralDVAdobe = $total_AbodeDVc + $total_AdobeDVs;
        
        if(($total_AbodeDVc + $total_AdobeActC) == 0){
            $porcAdobe = 0;
        } else{
            $porcAdobe = round($totalGeneralActivoAdobe * 100 / ($totalGeneralDVAdobe), 0);
        }
    } else if($row["idFabricante"] == 2){
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
        
        $detallesIBM = new DetallesIbm();
        $balanceIBM = new balanceIbm();
        
        $fechaUltEscaneoIBM = $log->fechaUltimoEscaneo(9, $_SESSION["client_id"]);
        $fechaProximaIBM = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleIBM = $balanceIBM->sobranteFaltante($_SESSION["client_id"]);
        
        $IBMActivo = 1;
        
        $tclientIBM = 0;
        $tserverIBM = 0;
        $total_IBMActC = 0;
        $total_IBMDVc = 0;
        $total_1IBMInacC = 0;
        $total_IBMActS = 0;
        $total_IBMDVs = 0;
        $total_1IBMInacS = 0;
        
        $lista = $detallesIBM->listar_todo1Asignacion($_SESSION['client_id'], "", $asignaciones);

        foreach ($lista as $reg_equipos) {
            if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientIBM++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_IBMActC++;
                }

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_IBMDVc ++;
                }
                else{
                    $total_1IBMInacC++;
                }
            } else {//server
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_IBMActS++;
                }

                $tserverIBM++;

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_IBMDVs++;
                }
                else{
                    $total_1IBMInacS++;
                }
            }
        }
        $reconciliacionIBMC = $total_IBMDVc - $total_IBMActC;
        $reconciliacionIBMS = $total_IBMDVs - $total_IBMActS;
        
        $totalGeneralActivoIBM = $total_IBMActC + $total_IBMActS;
        $totalGeneralDVIBM = $total_IBMDVc + $total_IBMDVs;        
        
        if(($total_IBMDVc + $total_IBMActC) == 0){
            $porcIBM = 0;
        } else{
            $porcIBM = round($totalGeneralActivoIBM * 100 / ($totalGeneralDVIBM), 0);
        }    
    } else if($row["idFabricante"] == 3){
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
        require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
        
        $detallesMicrosoft = new DetallesE_f();
        $balanceMicrosoft = new Balance_f();
        
        $fechaUltEscaneoMicrosoft = $log->fechaUltimoEscaneo(1, $_SESSION["client_id"]);
        $fechaProximaMicrosoft = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleMicrosoft = $balanceMicrosoft->sobranteFaltante($_SESSION["client_id"]);
        
        $MicrosoftActivo = 1;
        
        $tclientMicrosoft = 0;
        $tserverMicrosoft = 0;
        $total_MicrosoftActC = 0;
        $total_MicrosoftDVc = 0;
        $total_1MicrosoftInacC = 0;
        $total_MicrosoftActS = 0;
        $total_MicrosoftDVs = 0;
        $total_1MicrosoftInacS = 0;
        
        $lista = $detallesMicrosoft->listar_todo1Asignacion($_SESSION['client_id'], "", $asignaciones);

        foreach ($lista as $reg_equipos) {
            if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientMicrosoft++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_MicrosoftActC++;
                }

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_MicrosoftDVc ++;
                }
                else{
                    $total_1MicrosoftInacC++;
                }
            } else {//server
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_MicrosoftActS++;
                }

                $tserverMicrosoft++;

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_MicrosoftDVs++;
                }
                else{
                    $total_1MicrosoftInacS++;
                }
            }
        }
        $reconciliacionMicrosoftC = $total_MicrosoftDVc - $total_MicrosoftActC;
        $reconciliacionMicrosoftS = $total_MicrosoftDVs - $total_MicrosoftActS;
        
        $totalGeneralActivoMicrosoft = $total_MicrosoftActC + $total_MicrosoftActS;
        $totalGeneralDVMicrosoft = $total_MicrosoftDVc + $total_MicrosoftDVs;
        
        
        if(($total_MicrosoftDVc + $total_MicrosoftActC) == 0){
            $porcMicrosoft = 0;
        } else{
            $porcMicrosoft = round($totalGeneralActivoMicrosoft * 100 / ($totalGeneralDVMicrosoft), 0);
        }
    } else if($row["idFabricante"] == 4){
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_oracle.php");
        
        $detallesOracle = new DetallesOracle();
        $balanceOracle = new balanceOracle();
        
        $fechaUltEscaneoOracle = $log->fechaUltimoEscaneo(12, $_SESSION["client_id"]);
        $fechaProximaOracle = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleOracle = $balanceOracle->sobranteFaltante($_SESSION["client_id"]);
        
        $OracleActivo = 1;
        
        $tclientOracle = 0;
        $tserverOracle = 0;
        $total_OracleActC = 0;
        $total_OracleDVc = 0;
        $total_1OracleInacC = 0;
        $total_OracleActS = 0;
        $total_OracleDVs = 0;
        $total_1OracleInacS = 0;
        
        $lista = $detallesOracle->listar_todo1Asignacion($_SESSION['client_id'], "", $asignaciones);

        foreach ($lista as $reg_equipos) {
            if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientOracle++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_OracleActC++;
                }

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_OracleDVc ++;
                }
                else{
                    $total_1OracleInacC++;
                }
            } else {//server
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_OracleActS++;
                }

                $tserverOracle++;

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_OracleDVs++;
                }
                else{
                    $total_1OracleInacS++;
                }
            }
        }
        $reconciliacionOracleC = $total_OracleDVc - $total_OracleActC;
        $reconciliacionOracleS = $total_OracleDVs - $total_OracleActS;
        
        $totalGeneralActivoOracle = $total_OracleActC + $total_OracleActS;
        $totalGeneralDVOracle = $total_OracleDVc + $total_OracleDVs;
        
        
        if(($total_OracleDVc + $total_OracleActC) == 0){
            $porcOracle = 0;
        } else{
            $porcOracle = round($totalGeneralActivoOracle * 100 / ($totalGeneralDVOracle), 0);
        }
    } else if($row["idFabricante"] == 10){
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
        //require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
        
        $detallesSPLA = new Detalles_SPLA();
        //$balanceSPLA = new Balance_SPLA();
        
        $fechaUltEscaneoSPLA = $log->fechaUltimoEscaneo(19, $_SESSION["client_id"]);
        $fechaProximaSPLA = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        //$disponibleSPLA = $balanceOracle->sobranteFaltante($_SESSION["client_id"]);
        
        $SPLAActivo = 1;
        
        $tclientSPLA = 0;
        $tserverSPLA = 0;
        $total_SPLAActC = 0;
        $total_SPLADVc = 0;
        $total_1SPLAInacC = 0;
        $total_SPLAActS = 0;
        $total_SPLADVs = 0;
        $total_1SPLAInacS = 0;
        
        $lista = $detallesSPLA->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);

        foreach ($lista as $reg_equipos) {
            if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientSPLA++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_SPLAActC++;
                }

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_SPLADVc ++;
                }
                else{
                    $total_1SPLAInacC++;
                }
            } else {//server
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_SPLAActS++;
                }

                $tserverSPLA++;

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_SPLADVs++;
                }
                else{
                    $total_1SPLAInacS++;
                }
            }
        }
        $reconciliacionSPLAC = $total_SPLADVc - $total_SPLAActC;
        $reconciliacionSPLAS = $total_SPLADVs - $total_SPLAActS;
        
        $totalGeneralActivoSPLA = $total_SPLAActC + $total_SPLAActS;
        $totalGeneralDVSPLA = $total_SPLADVc + $total_SPLADVs;
        
        
        if(($total_SPLADVc + $total_SPLAActC) == 0){
            $porcSPLA = 0;
        } else{
            $porcSPLA = round($totalGeneralActivoSPLA * 100 / ($totalGeneralDVSPLA), 0);
        }
    } else if($row["idFabricante"] == 5){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_Sap.php");
        $SAPMostrado = 0;
        
        $balanceSAP = new balanceSap();
        
        $fechaUltEscaneoSAP = $log->fechaUltimoEscaneo(14, $_SESSION["client_id"]);
        $fechaProximaSAP = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleSAP = $balanceSAP->sobranteFaltante($_SESSION["client_id"]);
        
        $controlFabSinGrafico++;
    } else if($row["idFabricante"] == 6){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
        $VMWareMostrado = 0;
        
        $balanceVMWare = new balanceVMWare();
        
        $fechaUltEscaneoVMWare = $log->fechaUltimoEscaneo(25, $_SESSION["client_id"]);
        $fechaProximaVMWare = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleVMWare = $balanceOracle->sobranteFaltante($_SESSION["client_id"]);
        
        $controlFabSinGrafico++;
    } else if($row["idFabricante"] == 7){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixIBM.php");
        $UNIXIBMMostrado = 0;

        $balanceUnixIBM = new balanceUnixIBM();
        
        $fechaUltEscaneoUnixIBM = $log->fechaUltimoEscaneo(20, $_SESSION["client_id"]);
        $fechaProximaUnixIBM = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleUnixIBM = $balanceUnixIBM->sobranteFaltante($_SESSION["client_id"]);
        
        $controlFabSinGrafico++;
    } else if($row["idFabricante"] == 8){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
        $UNIXOracleMostrado = 0;
        
        $balanceUnixOracle = new balanceUnixOracle();
        
        $fechaUltEscaneoUnixOracle = $log->fechaUltimoEscaneo(23, $_SESSION["client_id"]);
        $fechaProximaUnixOracle = $nueva_funcion->fechaProximaUltContrato($_SESSION["client_id"], $row["idFabricante"]);
        $disponibleUnixOracle = $balanceUnixOracle->sobranteFaltante($_SESSION["client_id"]);
        
        $controlFabSinGrafico++;
    }
}*/
