<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_fabricantes_activos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$fabricantes = new fabricantesActivos();
$detallesAdobe = new DetallesAdobe();
$detallesIBM = new DetallesIbm();
$detallesMicrosoft = new DetallesE_f();
$detallesOracle = new DetallesOracle();
$detallesSPLA = new Detalles_SPLA();
$nueva_funcion = new funcionesSam();
$general = new General();
$log = new log();


$listarFabricantes = $fabricantes->gantt_inicio($_SESSION["client_id"], $_SESSION["client_empleado"]);
$renovFabricantes = $nueva_funcion->GANTT_inicio($_SESSION["client_id"], $_SESSION["client_empleado"]);

$fechaPagoCliente = array();
foreach($listarFabricantes as $row){
    $fechaPagoCliente[$row["idFabricante"]] = $fabricantes->gantt_fechas($_SESSION["client_id"], $row["idFabricante"]);
    
    if($row["idFabricante"] == 1){
        $tclientAdobe = 0;
        $total_1LAcAdobe = 0;
        $total_2DVcAdobe = 0;
        
        $detallesAdobe->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(7, $_SESSION["client_id"]);

        foreach ($detallesAdobe->lista as $reg_equipos) {
            //if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientAdobe++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAcAdobe++;
                }
                
                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVcAdobe++;
                }
            //}
        }

        if(($total_2DVcAdobe + $total_1LAcAdobe) == 0){
            $porcAdobe = 0;
        } else{
            $porcAdobe = round($total_1LAcAdobe * 100 / ($total_2DVcAdobe + $total_1LAcAdobe), 0);
        }
        
    } else if($row["idFabricante"] == 2){
        $tclientIBM = 0;
        $total_1LAcIBM = 0;
        $total_2DVcIBM = 0;
        
        $detallesIBM->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(9, $_SESSION["client_id"]);

        foreach ($detallesIBM->lista as $reg_equipos) {
            //if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientIBM++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAcIBM++;
                }
                
                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVcIBM++;
                }
            //}
        }

        if(($total_2DVcIBM + $total_1LAcIBM) == 0){
            $porcIBM = 0;
        } else{
            $porcIBM = round($total_1LAcIBM * 100 / ($total_2DVcIBM + $total_1LAcIBM), 0);
        }
        
    } else if($row["idFabricante"] == 3){
        $tclientMicrosoft = 0;
        $total_1LAcMicrosoft = 0;
        $total_2DVcMicrosoft = 0;

        $listar_equiposMicrosoft = $detallesMicrosoft->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(1, $_SESSION["client_id"]);

        foreach ($listar_equiposMicrosoft as $reg_equipos) {
            //if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientMicrosoft++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAcMicrosoft++;
                }
                
                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVcMicrosoft++;
                }
            //}
        }
        
        if(($total_2DVcMicrosoft + $total_1LAcMicrosoft) == 0){
            $porcMicrosoft = 0;
        } else{
            $porcMicrosoft = round($total_1LAcMicrosoft * 100 / ($total_2DVcMicrosoft + $total_1LAcMicrosoft), 0);
        }
        
    } else if($row["idFabricante"] == 4){
        $tclientOracle = 0;
        $total_1LAcOracle = 0;
        $total_2DVcOracle = 0;

        $lista = $detallesOracle->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(12, $_SESSION["client_id"]);

        foreach ($lista as $reg_equipos) {
            //if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientOracle++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAcOracle++;
                }
                
                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVcOracle++;
                }
            //}
        }

        if(($total_2DVcOracle + $total_1LAcOracle) == 0){
            $porcOracle = 0;
        } else{
            $porcOracle = round($total_1LAcOracle * 100 / ($total_2DVcOracle + $total_1LAcOracle), 0);
        }
        
    } else if($row["idFabricante"] == 5){
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(14, $_SESSION["client_id"]);
    } else if($row["idFabricante"] == 6){
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(25, $_SESSION["client_id"]);
    } else if($row["idFabricante"] == 7){
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(20, $_SESSION["client_id"]);
    } else if($row["idFabricante"] == 8){
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(23, $_SESSION["client_id"]);
    } else if($row["idFabricante"] == 10){
        $tclientSPLA = 0;
        $total_1LAcSPLA = 0;
        $total_2DVcSPLA = 0;

        $listar_equiposSPLA = $detallesSPLA->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $fechaLevant[$row["idFabricante"]] = $log->fechaUltimoEscaneo(19, $_SESSION["client_id"]);

        foreach ($listar_equiposSPLA as $reg_equipos) {
            //if ($reg_equipos["tipo"] == 1) {//cliente
                $tclientSPLA++;
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAcSPLA++;
                }
                
                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVcSPLA++;
                }
            //}
        }
        
        if(($total_2DVcSPLA + $total_1LAcSPLA) == 0){
            $porcSPLA = 0;
        } else{
            $porcSPLA = round($total_1LAcSPLA * 100 / ($total_2DVcSPLA + $total_1LAcSPLA), 0);
        }
        
    }
}

$ene = $feb = $mar = $abr = $may = $jun = $jul = $ago = $sep = $oct = $nov = $dic = array();

foreach($listarFabricantes as $row){
    $fechaAux = array();
    if(isset($fechaPagoCliente[$row["idFabricante"]])){
        if($fechaPagoCliente[$row["idFabricante"]]["aniversario"] != ""){
            $fechaAux = explode("-", $fechaPagoCliente[$row["idFabricante"]]["aniversario"]);
        } else{
            $fechaAux = explode("-", $fechaPagoCliente[$row["idFabricante"]]["renovacion"]);
        }
       
        if($row["idFabricante"] == 1){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Adobe_Logo.png" style="width:auto; height:90%; top:50%; margin-top:-15px; margin-left:-10px; position:absolute;">'; 
        } else if($row["idFabricante"] == 2){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Windows-IBM_Logo.png" style="width:100%; height:auto;">';
        } else if($row["idFabricante"] == 3){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Microsoft_Logo.png" style="width:auto; height:100%;">';
        } else if($row["idFabricante"] == 4){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Windows-Oracle_Logo.png" style="width:90%; height:auto; top:50%; margin-top:-5px; margin-left: -30px; position:absolute;">';
        } else if($row["idFabricante"] == 5){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/SAP_Logo.png" style="width:90%; height:auto;">';
        } else if($row["idFabricante"] == 6){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/VMware_Logo.png" style="width:100%; height:auto; top:50%; margin-top:-8px; margin-left:-34px; position:absolute;">';
        } else if($row["idFabricante"] == 7){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/UNIX-IBM_Logp.png" style="width:auto; height:100%;">';
        } else if($row["idFabricante"] == 8){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/UNIX-Oracle_Logo.png" style="width:90%; height:auto; top:50%; margin-top: -7px; margin-left:-30px; position:absolute;">';
        } else if($row["idFabricante"] == 10){
            $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Windows-IBM_Logo.png" style="width:100%; height:auto;">';
        }
        
        if(isset($fechaAux[1]) && $fechaAux[1] == 1){
            $ene[] = $logo;//$row["nombre"];
            $ene[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 2){
            $feb[] = $logo;
            $feb[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 3){
            $mar[] = $logo;
            $mar[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 4){
            $abr[] = $logo;
            $abr[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 5){
            $may[] = $logo;
            $may[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 6){
            $jun[] = $logo;
            $jun[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 7){
            $jul[] = $logo;
            $jul[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 8){
            $ago[] = $logo;
            $ago[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 9){
            $sep[] = $logo;
            $sep[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 10){
            $oct[] = $logo;
            $oct[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 11){
            $nov[] = $logo;
            $nov[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        } else if(isset($fechaAux[1]) && $fechaAux[1] == 12){
            $dic[] = $logo;
            $dic[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
        }
    }
}

if(count($ene) > 2){
    $ene[0] = "Varios";
    $ene[1] = "colorIncio";
}

if(count($feb) > 2){
    $feb[0] = "Varios";
    $feb[1] = "colorIncio";
}

if(count($mar) > 2){
    $mar[0] = "Varios";
    $mar[1] = "colorIncio";
}

if(count($abr) > 2){
    $abr[0] = "Varios";
    $abr[1] = "colorIncio";
}

if(count($may) > 2){
    $may[0] = "Varios";
    $may[1] = "colorIncio";
}

if(count($jun) > 2){
    $jun[0] = "Varios";
    $jun[1] = "colorIncio";
}

if(count($jul) > 2){
    $jul[0] = "Varios";
    $jul[1] = "colorIncio";
}

if(count($ago) > 2){
    $ago[0] = "Varios";
    $ago[1] = "colorIncio";
}

if(count($sep) > 2){
    $sep[0] = "Varios";
    $sep[1] = "colorIncio";
}

if(count($oct) > 2){
    $oct[0] = "Varios";
    $oct[1] = "colorIncio";
}

if(count($nov) > 2){
    $nov[0] = "Varios";
    $nov[1] = "colorIncio";
}

if(count($dic) > 2){
    $dic[0] = "Varios";
    $dic[1] = "colorIncio";
}

$eneP = $febP = $marP = $abrP = $mayP = $junP = $julP = $agoP = $sepP = $octP = $novP = $dicP = array();

foreach($renovFabricantes as $row){
    $fechaTrue = explode("-", $row["fechaExpiracion"]);
    $fechaReno = explode("-", $row["fechaProxima"]);
    $fechaPresup = explode("-", $row["presupuesto"]);
    
    if($row["idFabricante"] == 1){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Adobe_Logo.png" style="width:auto; height:90%; top:50%; margin-top:-15px; margin-left:-10px; position:absolute;">'; 
    } else if($row["idFabricante"] == 2){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Windows-IBM_Logo.png" style="width:100%; height:auto;">';
    } else if($row["idFabricante"] == 3){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Microsoft_Logo.png" style="width:auto; height:100%;">';
    } else if($row["idFabricante"] == 4){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Windows-Oracle_Logo.png" style="width:90%; height:auto; top:50%; margin-top:-5px; margin-left: -30px; position:absolute;">';
    } else if($row["idFabricante"] == 5){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/SAP_Logo.png" style="width:90%; height:auto;">';
    } else if($row["idFabricante"] == 6){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/VMware_Logo.png" style="width:100%; height:auto; top:50%; margin-top:-8px; margin-left:-34px; position:absolute;">';
    } else if($row["idFabricante"] == 7){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/UNIX-IBM_Logp.png" style="width:auto; height:100%;">';
    } else if($row["idFabricante"] == 8){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/UNIX-Oracle_Logo.png" style="width:90%; height:auto; top:50%; margin-top: -7px; margin-left:-30px; position:absolute;">';
    } else if($row["idFabricante"] == 10){
        $logo = '<img src="' . $GLOBALS["domain_root"] .'/imagenes/Windows-IBM_Logo.png" style="width:100%; height:auto;">';
    }

    $letra = "T";
    if(isset($fechaTrue[1]) && $fechaTrue[1] == 1){
        $eneP[] = $letra;
        $eneP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 2){
        $febP[] = $letra;
        $febP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 3){
        $marP[] = $letra;
        $marP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 4){
        $abrP[] = $letra;
        $abrP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 5){
        $mayP[] = $letra;
        $mayP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 6){
        $junP[] = $letra;
        $junP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 7){
        $julP[] = $letra;
        $julP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 8){
        $agoP[] = $letra;
        $agoP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 9){
        $sepP[] = $letra;
        $sepP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 10){
        $octP[] = $letra;
        $octP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 11){
        $novP[] = $letra;
        $novP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaTrue[1]) && $fechaTrue[1] == 12){
        $dicP[] = $letra;
        $dicP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    }
    
    $letra = "R";
    if(isset($fechaReno[1]) && $fechaReno[1] == 1){
        $eneP[] = $letra;
        $eneP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 2){
        $febP[] = $letra;
        $febP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 3){
        $marP[] = $letra;
        $marP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 4){
        $abrP[] = $letra;
        $abrP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 5){
        $mayP[] = $letra;
        $mayP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 6){
        $junP[] = $letra;
        $junP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 7){
        $julP[] = $letra;
        $julP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 8){
        $agoP[] = $letra;
        $agoP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 9){
        $sepP[] = $letra;
        $sepP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 10){
        $octP[] = $letra;
        $octP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 11){
        $novP[] = $letra;
        $novP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaReno[1]) && $fechaReno[1] == 12){
        $dicP[] = $letra;
        $dicP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    }
    
    $letra = "P";
    if(isset($fechaPresup[1]) && $fechaPresup[1] == 1){
        $eneP[] = $letra;
        $eneP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 2){
        $febP[] = $letra;
        $febP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 3){
        $marP[] = $letra;
        $marP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 4){
        $abrP[] = $letra;
        $abrP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 5){
        $mayP[] = $letra;
        $mayP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 6){
        $junP[] = $letra;
        $junP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 7){
        $julP[] = $letra;
        $julP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 8){
        $agoP[] = $letra;
        $agoP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 9){
        $sepP[] = $letra;
        $sepP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 10){
        $octP[] = $letra;
        $octP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 11){
        $novP[] = $letra;
        $novP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    } else if(isset($fechaPresup[1]) && $fechaPresup[1] == 12){
        $dicP[] = $letra;
        $dicP[] = $general->obtenerClaseColorInicio($row["idFabricante"]);
    }
}

if(count($eneP) > 2){
    $eneP[0] = "Varios";
    $eneP[1] = "colorIncio";
}

if(count($febP) > 2){
    $febP[0] = "Varios";
    $febP[1] = "colorIncio";
}

if(count($marP) > 2){
    $marP[0] = "Varios";
    $marP[1] = "colorIncio";
}

if(count($abrP) > 2){
    $abrP[0] = "Varios";
    $abrP[1] = "colorIncio";
}

if(count($mayP) > 2){
    $mayP[0] = "Varios";
    $mayP[1] = "colorIncio";
}

if(count($junP) > 2){
    $junP[0] = "Varios";
    $junP[1] = "colorIncio";
}

if(count($julP) > 2){
    $julP[0] = "Varios";
    $julP[1] = "colorIncio";
}

if(count($agoP) > 2){
    $agoP[0] = "Varios";
    $agoP[1] = "colorIncio";
}

if(count($sepP) > 2){
    $sepP[0] = "Varios";
    $sepP[1] = "colorIncio";
}

if(count($octP) > 2){
    $octP[0] = "Varios";
    $octP[1] = "colorIncio";
}

if(count($novP) > 2){
    $novP[0] = "Varios";
    $novP[1] = "colorIncio";
}

if(count($dicP) > 2){
    $dicP[0] = "Varios";
    $dicP[1] = "colorIncio";
}