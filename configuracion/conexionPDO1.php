<?php
################################################
# - INICIA VARIABLE DE SESION                  #
# - INCLUYE LAS VARIABLES DE CONFIGURACION     #
# - ABRE LA CONEXION A LA BASE DE DATOS        #
################################################
require("variables.php");
// Conectar Base de Datos
try{
    $conn = new PDO('mysql:host=' . $DBservidor . ';dbname=' . $DBnombre, $DBusuario, $DBcontrasena);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    print "ERROR: No se pudo realizar la conexión a la base de datos";
}
?>