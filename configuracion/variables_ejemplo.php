<?php
########################################################################
# VARIABLES DE CONFIGURACION:                                          #
# approot:      Ubicacion fisica de la aplicacion                      #
# Domain_root:  Ubicacion HTTP de la aplicacion                        #
# DBuser:       Nombre de usuario de la base de datos                  #
# DBpass:       Password del usuario de la base de dato                #
# DBname:       Nombre de la base de datos                             #
# DBserver:     Nombre del servidor de bases de datos                  #
########################################################################

$configuracion = 1;

if($configuracion == 3) {
    #CONFIGURACION SERVER#
    $RAIZ = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['domain_root'] = "https://".$_SERVER['HTTP_HOST'];
    $GLOBALS['domain_root1'] = "https://".$_SERVER['HTTP_HOST'];
    $DBusuario = "";
    $DBnombre = "";
    $DBservidor = "";
    $DBcontrasena = "";
    $TIEMPO_MAXIMO_SESION = 600; //3600;  // en segundos	
} else if($configuracion == 2) {
    #CONFIGURACION PRUEBA SERVER#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/webtoolTest";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/webtoolTest";
    $GLOBALS['domain_root'] = "https://".$_SERVER['HTTP_HOST']."/webtoolTest";
    $GLOBALS['domain_root1'] = "https://".$_SERVER['HTTP_HOST'];
    $DBusuario = "";
    $DBnombre = "";
    $DBservidor = "";
    $DBcontrasena = "";
    $TIEMPO_MAXIMO_SESION = 600; //3600;  // en segundos
    
} else {
    #CONFIGURACION LOCAL#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/webtool";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com";
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/webtool";
    $GLOBALS['domain_root'] = "http://".$_SERVER['HTTP_HOST']."/licensingassurance.com/webtool";
    $GLOBALS['domain_root1'] = "http://".$_SERVER['HTTP_HOST']."/licensingassurance.com";
    $DBusuario = "root";
    $DBnombre = "nadd";
    $DBservidor = "127.0.0.1";
    $DBcontrasena = "developer@jk123";
    $TIEMPO_MAXIMO_SESION = 600;  // en segundos
}
