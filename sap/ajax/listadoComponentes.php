<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_sap.php");
require_once($GLOBALS["app_root"] . "/clases/paginacion.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $resumen         = new ResumenSAP();

            $vert = "";
            $vert1 = "";
            $pagina = 1;
            $tabla = "";
            
            if(isset($_POST["vert"])){
                $vert = $general->get_escape($_POST["vert"]);
            }
            
            if(isset($_POST["vert1"])){
                $vert1 = $general->get_escape($_POST["vert1"]);
            }
            
            if(isset($_POST["pagina"]) && filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false){
                $pagina = $_POST["pagina"];
            }
            
            $limite = $general->limit_paginacion;
            if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                $limite = $_POST["limite"];
            }
            
            $inicio = ($pagina * $limite) - $limite;

            $listado = $resumen->listar_datos6Paginado($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1, $inicio, $limite);
            $totalListado =  $resumen->listar_datos6Total($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1);

            $paginacion = new Paginacion($totalListado, $pagina, "nuevaPagina");
            
            $i = $inicio + 1;
            foreach($listado as $reg_calculo){
                $tabla .= '<tr>
                    <td>' . $i . '</td>
                    <td>' . $reg_calculo["equipo"] . '</td>
                    <td>' . $reg_calculo["componente"] . '</td>
                    <td>' . $reg_calculo["familia"] . '</td>
                    <td>' . $reg_calculo["edicion"] . '</td>
                    <td>' . $reg_calculo["version"] . '</td>
                    <td>' . $reg_calculo["diasUltAcceso"] . '</td>
                </tr>';
                $i++;
            }	
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'paginacion'=>$paginacion->get_pag(), 
            'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);