<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_servidores_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_servidores_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuarios_modulo_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usabilidad_usuarios_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_Sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_SAP.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_usuarios_sap.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $detalle      = new DetallesSAP();
            $resumen      = new ResumenServidoresSAP();
            $archivosDespliegue = new clase_archivos_fabricantes();
            $usuariosModulo = new usuariosModuloSAP();
            $resumenUsuarios = new ResumenUsuariosSAP();
            $usabilidad = new UsabilidadUsuariosSAP();
            $compras         = new comprasSAP();
            $balance        = new balanceSap();
          
            $i = 8;
            $j = 0;
            if($detalle->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($resumen->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
           
            if($archivosDespliegue->eliminarArchivosFabricante($_SESSION["client_id"], $_SESSION["client_empleado"], 5)){
                $j++;
            }
            
            if($usuariosModulo->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($resumenUsuarios->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($usabilidad->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($compras->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($balance->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            $result = 0;
            if($i > $j){
                $result = 2;
            } else if($i == $j){
                $result = 1;
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);