<div style="width:950px; margin:0 auto; overflow:hidden;">
    <fieldset class="fieldset pointer" style="width:400px; height:300px; margin-top:20px; float:left;" id="asignarDepartamentoCargo">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Asignar Departamento/Cargo</span></legend>
        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/sam/asignarCostos.png" style="width:400px; height:auto;">
    </fieldset>
    <fieldset class="fieldset pointer" style="width:400px; height:300px; margin-top:20px; margin-left:60px; float:left; overflow:hidden; position:relative;" id="usuariosDepartamento">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Usuarios Departamento/Cargo</span></legend>
        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/sam/usuariosPorEquipo.png" style="width:250px; height:auto; top:50%; margin-top:-105px; left:50%; margin-left: -100px; position:absolute;">
    </fieldset>
</div>

<script>
    $(document).ready(function(){
        $("#listadoAsigUsuarioEquipo").tableHeadFixer();

        $("#asignarDepartamentoCargo").click(function(){
            location.href="asignarDepartamentoCargo.php";
        });

        $("#usuariosDepartamento").click(function(){
            location.href="usuariosDepartamentoCargo.php";
        });
    });
</script>