<input type="hidden" id="nombreArchivo"> 

<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:10px; margin-bottom:20px;">
    <div class="modal-personal modal-md-personal" id="modal-contratos">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Contratos</p></h1>
        </div>
        <div class="modal-personal-body">
            <form id="contratoRepoF" name="contratoRepoF" method="post">
                <input type="hidden" id="tokenContratos" name="token">
                <input type="hidden" name="fabricante" value="5">
                <table style="width:100%; border:1px solid;">
                    <thead style="background-color:#C1C1C1;">
                        <tr>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkContratosAll" name="checkAll" onclick="chequearListContratos('tablaLicencias')"></th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">N&uacute;mero de contrato</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Estado</th>
                        </tr>
                    </thead>
                    <tbody id="tablaContratos">
                    <?php
                    $i = 0;
                    foreach($query as $row){
                    ?>
                        <tr style="border-bottom: 1px solid #ddd">
                            <td style="text-align:center;"><input type="checkbox" id="checkContratos<?= $i ?>" name="checkContratos[]" value="<?= $row["idContrato"] ?>"></td>
                            <td><?= $row["numero"] ?></td>
                            <td><?= $row["estado"] ?></td>
                        </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="border:1px solid; float:right;" onclick="salirModalContratos()">Salir</div>
            <div class="boton1 botones_m2" onclick="buscarLicenciasContrato();" style="border:1px solid; float:right;">Cargar</div>
        </div>
    </div>

    <div class="modal-personal" id="modal-licenciasCompras">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Licencias</p></h1>
        </div>
        <div class="modal-personal-body">
            <form id="compraRepoF" name="compraRepoF" method="post" action = "comprasRepositorio.php">
                <input type="hidden" id="insertarRepoF" name="insertarRepoF" value="1">
                <table style="width:100%; border:1px solid;">
                    <thead style="background-color:#C1C1C1;">
                        <tr>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkAll" name="checkAll" onclick="chequearListLicencias('tablaLicencias')" checked='checked'></th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Fabricante</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">N&uacute;mero de contrato</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Producto</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Edici&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Versi&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Cantidades</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Asignaci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody id="tablaLicencias">
                    </tbody>
                </table>
            </form>
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="border:1px solid; float:right;" onclick="salirModalCompras()">Salir</div>
            <div class="boton1 botones_m2" onclick="guardarComprasRepoF();" style="border:1px solid; float:right;">Cargar</div>
        </div>
    </div>
    <!--fin modal compras repositorio-->

    <!--<div style="width:98%; padding:10px; overflow:hidden;">-->
    <h1 class="textog negro" style="margin:20px; text-align:center;">Compras</h1>

    <div class="boton1 botones_m2Alterno" onclick="comprasRepoF();" style="border:1px solid">Cargar Compras del Repositorio</div>

    <br style="clear:both;">
    <br>

    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar el siguiente <a class="link1" href="<?=$GLOBALS['domain_root']?>/reportes/formularioCompra.php">Formulario de Compras</a></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2. -&nbsp;</span>Completar los datos del formulario</p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3. -&nbsp;</span>Abrir un nuevo libro de Excel y copiar todos los datos de las columnas: Producto, Edici&oacute;n, Versi&oacute;n, SKU, Tipo, Cantidades, Precio Unitario y Asignación, en este archivo incluyendo encabezados. <a onClick="showhide('imagen1v');" class="link1">Ver Imagen 1</a> y <a onClick="showhide('imagen2v');"  class="link1">Imagen 2</a>. Seleccione la opci&oacute;n "<strong style="font-weight:bold;">Guardar como</strong>", y elija el tipo de archivo "<strong style="font-weight:bold;">CSV (delimitado por comas)(*.csv)</strong>". Ver <a onClick="showhide('imagen3v');"  class="link1">Imagen 1</a>, <a onClick="showhide('imagen4v');"  class="link1">Imagen 2</a>
    </p><br />
    <div style="display:none" id="imagen1v"><img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/ayuda1.jpg"></div>
    <div style="display:none" id="imagen2v"><img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/ayuda2.jpg"></div>
    <div style="display:none" id="imagen3v"><img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/ayuda3.jpg"></div>
    <div style="display:none" id="imagen4v"><img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/ayuda4.jpg"></div> 

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4. -&nbsp;</span>Subir archivo <strong style="font-weight:bold;">CSV </strong>guardado</p><br />
    
    <fieldset class="fieldset">
        <legend style=" font-weight:bold; margin-left:15px;">Compras</legend>
        
        <form enctype="multipart/form-data" name="form_c" id="form_c" method="post" action="compras.php" >
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <div class="contenedor_archivo">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivo" id="archivo" accept=".csv">
                    <input type="button" id="boton1" class="botones_m5" value="Buscar">
                </div>
                <div class="contenedor_boton_archivo">
                    <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargandoc');document.getElementById('form_c').submit();" class="botones_m5" />
                </div>
            </div>
        </form>

        <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
        <div id="cargandoc" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
        <?php if($exito2=='1'){ ?>
            <div class="exito_archivo">Archivo cargado con &eacute;xito</div>
            <script>
                $.alert.open('info', "Archivo cargado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }else{ ?>

        <?php }
        if($error=='1'){ ?>
            <div class="error_archivo">Debe ser un archivo con extensi&oacute;n CSV</div>
            <script>
                $.alert.open('alert', "Debe ser un archivo con extensión CSV", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error=='2'){ ?>
            <div class="error_archivo">Debe seleccionar un archivo</div>
            <script>
                $.alert.open('alert', "Debe seleccionar un archivo", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error==3){ ?>
            <div class="error_archivo">Los campos del archivo <br> no son compatibles</div>
            <script>
                $.alert.open('alert', "Los campos del archivo no son compatibles", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error=='4'){ ?>
            <div class="error_archivo"><?= $general->error ?></div>
            <script>
                $.alert.open('alert', "<?=$general->error?>", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error > 5){ ?>
            <div class="error_archivo"><?=$consolidado->error?></div>
            <script>
                $.alert.open('alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php } ?>
    </fieldset>

    <br>
    <div style="float:right;"><div class="botones_m2" id="boton1" onClick="location.href='detalle.php'">Detalle</div></div>
</div>

<script>
    $(document).ready(function(){
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
</script>