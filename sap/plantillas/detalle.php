<script>
    function MM_jumpMenu(targ,selObj,restore){ //v3.0
        eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
        if (restore) selObj.selectedIndex=0;
    }
</script>

<input type="hidden" id="vert" value="<?= $vert ?>">
<input type="hidden" id="vert1" value="<?= $vert1 ?>">

<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:20px; margin-bottom:20px;">
    <div style="width:20%; margin:0px; padding:0px; overflow:hidden; float:left;">
        <span style="color:#9F9F9F;">Seleccione el Producto</span><br><br> 

        <div>Familia: <strong style="color:#000; font-weight:bold;">
            <?php
                foreach($tablaMaestra->lista as $row){
                    switch($vert){
                        case $row["campo1"] : echo $row["campo1"]; break;
                    }
                }

                if($vert === "Usabilidad"){
                    echo "Usabilidad";
                }
            ?>
            </strong>
            <br> 

            <select onchange="MM_jumpMenu('self',this,0)">
                <option value="detalle.php" selected>Seleccione..</option>
                <?php 
                foreach($tablaMaestra->lista as $row){
                ?>
                    <option value="detalle.php?vert=<?= $row['campo1']?>"><?= $row["campo1"] ?></option>
                <?php
                }
                ?>
                <option value="detalle.php?vert=Usabilidad"><?= "Usabilidad" ?></option>
            </select>

        </div><br><br>

        <div>Grupo:<strong style="color:#000; font-weight:bold;">
            <?php
            foreach($ediciones as $row){
                switch($vert1){  
                    case $row["nombre"]: echo $row["nombre"]; break;
                }
            }
            ?></strong>
            <br> 

            <select onchange="MM_jumpMenu('self',this,0)" style="max-width:150px;">
                <option value="detalle.php" selected>Seleccione..</option>
                <?php 
                foreach($ediciones as $row){
                ?>
                    <option value="detalle.php?vert=<?= $vert ?>&vert1=<?= $row['nombre']?>"><?= $row["nombre"] ?></option>
                <?php
                }
                ?>
            </select>

            <br>
            <br>
            <div style='margin-left:-10px;'>
                <form id="formExportar" name="formExportar" method="post" action="reportes/excelSAPDetalle.php">
                    <input type="hidden" id="vert" name="vert" value="<?php echo $vert; ?>">
                    <input type="hidden" id="vert1" name="vert1" value="<?php echo $vert1; ?>">
                    <input type="submit" id="exportar" name="exportar" style="display:none">
                    <div class="botones_m2Alterno boton1" id="export">Exportar Excel</div>
                </form>
            </div>
        </div>
    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; min-height:450px; overflow:hidden;">
        <?php 
        if($vert !== "Usabilidad"){ 
        ?>
            <div id="container3" style="height:400px; width:95%; max-width: 95%; margin:10px; float:left;"></div>
            <div  style="width:98%; margin:10px; float:left; text-align:center;"><a onclick="mostrarTabla();" style="cursor:pointer;" >Ver Detalles</a></div>
        <?php
        }
        else{
        ?>
            <div id="container3" style="height:400px; width:95%; margin:10px; float:left;"></div>
            <!--<div id="container4" style="height:400px; width:47%; margin:10px; float:left;"></div>-->
            <div  style="width:98%; margin:10px; float:left; text-align:center;"><a onclick="mostrarTablaUsabilidad();" style="cursor:pointer;" >Ver Detalles</a></div>
            <!--<div  style="width:47%; margin:10px; float:left; text-align:center;"><a onclick="mostrarTablaUsabilidad(2);" style="cursor:pointer;" >Ver Detalles</a></div>-->
        <?php
        }
        ?>                      
    </div>

    <?php 
    if($vert !== "Usabilidad"){
    ?>
        <div id="nroRegistros" class="hide">
            <p class="bold">N° Registros:</p>
            <select style="max-width:150px;" id="limite" name="limite">
                <option value="50" <?php if($limite == 50){ echo 'selected="selected";'; } ?>>50</option>
                <option value="100">100</option>
                <option value="300">300</option>
                <option value="500">500</option>
                <option value="1000">1000</option>
                <option value="1500">1500</option>
            </select>
        </div>
    
        <div id="ttabla1" class="tablap" style="display:none; width:99%; height:400px; margin:10px; float:left;">
            <table style="margin-top:-5px" class="tablap" id="tablaSapDetalle">
                <thead>
                    <tr style="background:#333; color:#fff;">
                        <th valign="middle"><span>&nbsp;</span></th>
                        <th valign="middle"><span><?php if($vert === "SAP Módulo"){ echo "Usuarios"; } else { echo "Servidores"; } ?></span></th>
                        <th valign="middle"><span>Componente</span></th>
                        <th valign="middle"><span>Familia</span></th>
                        <th valign="middle"><span>Edici&oacute;n</span></th>
                        <th valign="middle"><span>Versi&oacute;n</span></th>
                        <th valign="middle"><span>D&iacute;as &Uacute;ltimo Acceso</span></th>
                    </tr>
                </thead>
                <tbody  id="listadoSAP">
                    <?php
                    if(count($listado) > 0){
                        $i = 1;
                        foreach($listado as $reg_calculo){
                        ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $reg_calculo["equipo"] ?></td>
                                <td><?= $reg_calculo["componente"] ?></td>
                                <td><?= $reg_calculo["familia"] ?></td>
                                <td><?= $reg_calculo["edicion"] ?></td>
                                <td><?= $reg_calculo["version"] ?></td>
                                <td><?= $reg_calculo["diasUltAcceso"] ?></td>
                            </tr>
                        <?php
                            $i++;
                        }	
                    }
                    ?>
                </tbody>
            </table>         
        </div>
    
        <br>
        <div class="text-center hide" id="paginacion"><?= $paginacion->get_pag() ?></div>    
    <?php 
    }
    else{
    ?>
        <div id="ttabla1" class="tablap" style="display:none; width:99%; height:400px; margin:10px; float:left;">
            <table style="margin-top:-5px" class="tablap" id="tablaUsabAlta">
                <thead>
                    <tr style="background:#333; color:#fff;">
                        <th valign="middle"><span>&nbsp;</span></th>
                        <th valign="middle"><span>Usuarios</span></th>
                        <th valign="middle"><span>Fecha Creaci&oacute;n</span></th>
                        <th valign="middle"><span>Fecha Baja</span></th>
                        <th valign="middle"><span>Alta</span></th>
                        <th valign="middle"><span>Baja</span></th>
                        <th valign="middle"><span>D&iacute;as &Uacute;ltimo Acceso</span></th>
                        <th valign="middle"><span>Usabilidad</span></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach($tablaUsabilidad as $reg_calculo){
                    ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $reg_calculo["usuarios"] ?></td>
                            <td><?= $general->sustituirSeparadorFecha($reg_calculo["fechaCreacion"], ".", "/") ?></td>
                            <td><?php if(trim($reg_calculo["fechaBaja"]) != "") { echo $general->sustituirSeparadorFecha($reg_calculo["fechaBaja"], ".", "/"); } ?></td>
                            <td class="text-center"><?= $reg_calculo["alta"] ?></td>
                            <td class="text-center"><?= $reg_calculo["baja"] ?></td>
                            <td class="text-center"><?= $reg_calculo["diasUltEntrada"] ?></td>
                            <td><?= $reg_calculo["usabilidad"] ?></td>
                        </tr>
                    <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>   
        </div>
    <?php 
    }
    ?>
</div>                               
<script>
    var vertAux = "";
    var opcAux  = "";
    $(document).ready(function(){
        $("#export").click(function(){
            $("#exportar").click();
        });
        
        $("#tablaSapDetalle").tablesorter();
        $("#tablaSapDetalle").tableHeadFixer();
        $("#tablaUsabAlta").tablesorter();
        $("#tablaUsabAlta").tableHeadFixer();
        
        $("#limite").change(function(){
            nuevaPagina(1);
        });
    });
    
    function mostrarTabla(){
        if($('#ttabla1').is(':visible')){
            $('#ttabla1').hide();
            $("#paginacion").hide();
            $("#nroRegistros").hide(); 
        }
        else{
            $('#ttabla1').show();
            $("#paginacion").show();
            $("#nroRegistros").show();
        }
    }
    
    function mostrarTablaUsabilidad(){
        if($('#ttabla1').is(':visible')){
            $('#ttabla1').hide();
        }
        else{
            $('#ttabla1').show();
        }
    }
    
    function nuevaPagina(pagina){
        $("#fondo").show();
        $.post("ajax/listadoComponentes.php", { vert: $("#vert").val(), vert1 : $("#vert1").val(), pagina : pagina, 
        limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>   
            $("body").scrollTop($("body").height());
            $("#fondo").hide();        
            $("#listadoSAP").empty();
            $("#paginacion").empty();
            $("#listadoSAP").append(data[0].tabla);
            $("#paginacion").append(data[0].paginacion);
            $("#tablaSapDetalle").trigger("update"); 
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
</script>