<div style="width:99%; margin:0 auto; overflow:hidden;">
    <div id="nroRegistros">
        <p class="bold">N° Registros:</p>
        <select style="max-width:150px;" id="limite" name="limite">
            <option value="50" <?php if($limite == 50){ echo 'selected="selected";'; } ?>>50</option>
            <option value="100" <?php if($limite == 100){ echo 'selected="selected";'; } ?>>100</option>
            <option value="300" <?php if($limite == 300){ echo 'selected="selected";'; } ?>>300</option>
            <option value="500" <?php if($limite == 500){ echo 'selected="selected";'; } ?>>500</option>
            <option value="1000" <?php if($limite == 1000){ echo 'selected="selected";'; } ?>>1000</option>
            <option value="1500" <?php if($limite == 1500){ echo 'selected="selected";'; } ?>>1500</option>
        </select>
    </div>
    
    <div id="ttabla1" class="tablap" style="width:99%; height:400px; margin:10px;">
        <table style="margin-top:-5px" class="tablap" id="tablaSapDetalle">
            <thead>
                <tr style="background:#333; color:#fff;">
                    <th valign="middle"><span>&nbsp;</span></th>
                    <th valign="middle"><span>Departamento</span></th>
                    <th valign="middle"><span>Cargo</span></th>
                    <th valign="middle"><span>Usuarios</span></th>
                    <th valign="middle"><span>Componente</span></th>
                    <th valign="middle"><span>Familia</span></th>
                    <th valign="middle"><span>Edici&oacute;n</span></th>
                    <th valign="middle"><span>Versi&oacute;n</span></th>
                </tr>
            </thead>
            <tbody  id="listadoSAP">
                <?php
                if(count($listado) > 0){
                    $i = $inicio + 1;
                    foreach($listado as $reg_calculo){
                    ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $reg_calculo["departamento"] ?></td>
                            <td><?= $reg_calculo["cargo"] ?></td>
                            <td><?= $reg_calculo["equipo"] ?></td>
                            <td><?= $reg_calculo["componente"] ?></td>
                            <td><?= $reg_calculo["familia"] ?></td>
                            <td><?= $reg_calculo["edicion"] ?></td>
                            <td><?= $reg_calculo["version"] ?></td>
                        </tr>
                    <?php
                        $i++;
                    }	
                }
                ?>
            </tbody>
        </table>         
    </div>

    <br>
    <div class="text-center" id="paginacion"><?= $paginacion->get_pag() ?></div> 

    <br><br>

    <div style="float:right;" class="botonesSAM boton5" onclick="window.open('<?= $GLOBALS["domain_root"] ?>/sap/reportes/usuariosDepartCargo.php');">Exportar</div>
    <div style="float:right;" class="botonesSAM boton5" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sap/asignacion.php';">Atras</div> 
</div>

<script>
    var inicio = <?= $inicio + 1 ?>;
    $(document).ready(function(){
        $("#tablaSapDetalle").tableHeadFixer();
        
        $("#limite").change(function(){
            nuevaPagina(1);
        });
    });
    
    function nuevaPagina(pagina){
        location.href='<?= $GLOBALS["domain_root"] ?>/sap/usuariosDepartamentoCargo.php?pagina=' + pagina + '&limite=' + $("#limite").val();
    }
</script>
