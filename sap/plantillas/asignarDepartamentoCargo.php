<?php
if($insertar == 1 && $result == 0){
?>
    <script>
        $.alert.open("alert", "No se guardó los registros del Departamento/Cargo", {"Aceptar" : "Aceptar"}); 
    </script>
<?php
} else if($insertar == 1 && $result == 1){
?>
    <script>
        $.alert.open("info", "Departamento/Cargo guardado con éxito", {"Aceptar" : "Aceptar"});
    </script>
<?php
} else if($insertar == 1 && $result == 2){
?>
    <script>
        $.alert.open("alert", "No se guardaron todos los registros del Departamento/Cargo", {"Aceptar" : "Aceptar"});
    </script>
<?php
}
?>

<div style="width:99%; margin:0 auto; overflow:hidden;">
    <div id="nroRegistros">
        <p class="bold">N° Registros:</p>
        <select style="max-width:150px;" id="limite" name="limite">
            <option value="50" <?php if($limite == 50){ echo 'selected="selected";'; } ?>>50</option>
            <option value="100" <?php if($limite == 100){ echo 'selected="selected";'; } ?>>100</option>
            <option value="300" <?php if($limite == 300){ echo 'selected="selected";'; } ?>>300</option>
            <option value="500" <?php if($limite == 500){ echo 'selected="selected";'; } ?>>500</option>
            <option value="1000" <?php if($limite == 1000){ echo 'selected="selected";'; } ?>>1000</option>
            <option value="1500" <?php if($limite == 1500){ echo 'selected="selected";'; } ?>>1500</option>
        </select>
    </div>
    
    <form id="form1" name="form1" method="post" action="asignarDepartamentoCargo.php">
        <input type="hidden" id="insertar" name="insertar" value="1">
        <input type="hidden" id="limite" name="limite" value="<?= $limite ?>">
        <input type="hidden" id="pagina" name="pagina" value="<?= $pagina ?>">
        <div id="ttabla1" class="tablap" style="width:99%; height:400px; margin:10px;">
            <table style="margin-top:-5px" class="tablap" id="tablaSapDetalle">
                <thead>
                    <tr style="background:#333; color:#fff;">
                        <th valign="middle"><span>&nbsp;</span></th>
                        <th valign="middle"><span>Usuarios</span></th>
                        <th valign="middle"><span>Componente</span></th>
                        <th valign="middle"><span>Familia</span></th>
                        <th valign="middle"><span>Edici&oacute;n</span></th>
                        <th valign="middle"><span>Versi&oacute;n</span></th>
                        <th valign="middle"><span>D&iacute;as &Uacute;ltimo Acceso</span></th>
                        <th valign="middle"><span>Departamento</span></th>
                        <th valign="middle"><span>Cargo</span></th>
                    </tr>
                </thead>
                <tbody  id="listadoSAP">
                    <?php
                    if(count($listado) > 0){
                        $i = $inicio + 1;
                        foreach($listado as $reg_calculo){
                        ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $reg_calculo["equipo"] ?></td>
                                <td><?= $reg_calculo["componente"] ?></td>
                                <td><?= $reg_calculo["familia"] ?></td>
                                <td><?= $reg_calculo["edicion"] ?></td>
                                <td><?= $reg_calculo["version"] ?></td>
                                <td><?= $reg_calculo["diasUltAcceso"] ?></td>
                                <td><input type="hidden" id="idUsuario<?= $i ?>" name="idUsuario[]" value="<?= $reg_calculo["id"] ?>">
                                    <input type="text" id="departamento<?= $i ?>" name="departamento[]" maxlength="70" value="<?= $reg_calculo["departamento"] ?>" readonly></td>
                                <td><input type="text" id="cargo<?= $i ?>" name="cargo[]" maxlength="70" value="<?= $reg_calculo["cargo"] ?>" readonly></td>
                            </tr>
                        <?php
                            $i++;
                        }	
                    }
                    ?>
                </tbody>
            </table>         
        </div>

        <br>
        <div class="text-center" id="paginacion"><?= $paginacion->get_pag() ?></div> 

        <br><br>
        <div style="float:right;" class="botonesSAM boton5" id="guardarAsignacion">Guardar</div> 
        <div style="float:right;" class="botonesSAM boton5" id="asignacion">Asignar</div>
        <div style="float:right;" class="botonesSAM boton5" id="editarDepartamentoCargo">Editar</div>
        <div style="float:right;" class="botonesSAM boton5" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sap/asignacion.php';">Atras</div> 
    </form>
</div>

<script>
    var inicio = <?= $inicio + 1 ?>;
    $(document).ready(function(){
        $("#tablaSapDetalle").tableHeadFixer();
        
        $("#limite").change(function(){
            nuevaPagina(1);
        });
        
        $("#asignacion").click(function(){
            for(i = inicio; i < ($("#listadoSAP tr").length + inicio); i++){
                $("#departamento" + i).prop("readonly", "");
                $("#cargo" + i).prop("readonly", "");
            }
            $("#departamento" + inicio).focus();
        });
        
        $("#editarDepartamentoCargo").click(function(){
            j = inicio;
            bandera = false;
            for(i = inicio; i < ($("#listadoSAP tr").length + inicio); i++){
                if($("#departamento" + i).val() === ""){
                    j++;
                } else{
                    $("#departamento" + i).prop("readonly", "");
                    $("#cargo" + i).prop("readonly", "");
                    if(bandera === false){
                        $("#departamento" + i).focus();
                        bandera = true;
                    }
                }
            }

            if(i === j){
                $.alert.open("warning", "Alert", "Debe asignar primero los departamentos y cargos", {"Aceptar" : "Aceptar"}, function() {
                });
            }
        });
        
        $("#guardarAsignacion").click(function(){
            $("#form1").submit();
        });
    });
    
    function nuevaPagina(pagina){
        location.href='<?= $GLOBALS["domain_root"] ?>/sap/asignarDepartamentoCargo.php?pagina=' + pagina + '&limite=' + $("#limite").val();
    }
</script>
