<input type="hidden" id="nombreArchivo">

<div style="width:98%; padding:20px; overflow:hidden;">
    <div style="width:auto;">
        <h1 class="textog negro" style="text-align:center; line-height:35px;" id="tituloDespliegue">
            Despliegue SAP
        </h1>
    </div>
    <br>

    <div>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Versiones</span></p>									
        <p>Release Producto</p>
        <p>4.6 SAP R/3</p>
        <p>4.7 SAP R/3</p>
        <p>5.0 ECC SAP ECC 5.0</p>
        <p>6.0 ECC SAP ECC 6.0</p>
        <br>

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Requisitos</span></p>									
        <p>Acceso al servidor desarrollo del Sistema SAP ECC y un usuario con licencia para ejecutar la transacci&oacute;n ZLICSAP. El SAP GUI debe permitir descargar archivos a la PC del usuario. En los reportes ALV se debe permitir la integraci&oacute;n con MS Excel (2007 a 2013).</p><br />

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Primera Parte	Descarga Herramienta</span></p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Ejecutar en la l&iacute;nea de comandos la transacci&oacute;n SE38 y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"> <span style="font-weight:bold; margin-left:25px;">"Favoritos"</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Ingresamos el nombre del programa ZSISTDE, Seleccionamos <span style="font-weight:bold;">"codigo fuente"</span> y pulsamos <span style="font-weight:bold;">"Crear"</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Descargar C&oacute;digo fuente.  <a class="link1" href="<?=$GLOBALS['domain_root']?>/ZSISTDET.txt" target="_blank">ZSISTDET.txt</a></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Reemplazamos todo por el codigo fuente</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>Guardar <img src="<?=$GLOBALS['domain_root']?>/imagenes/guardar.png" style="width:20px; margin-top:2px; position:absolute;"><span style="margin-left:25px; margin-right:5px;">y Generar </span><img src="<?=$GLOBALS['domain_root']?>/imagenes/generar1.png" style="width:22px; position:absolute;"></p><br>
        <br>

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Segunda Parte	Componentes de Software Instalados</span></p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Componentes de Software Instalado"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Hacer click en el icono.  Elegir la opci&oacute;n <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversi&oacute;n"</span>. El sistema crear&aacute; el archivo con el nombre <span style="font-weight:bold;">"Output_1_texto Componentes Sistema.txt"</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>CARGAR EL RESULTADO</p><br>

        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">Componentes Sistema</legend>
            
            <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="despliegue1.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionDespliegue" type="hidden" value="<?= $pestana ?>" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo" accept='.txt'>
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <?php if($exito=='1'){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }else{ ?>

            <?php }
            if($error=='1'){ ?>
                <div class="error_archivo"><?= $general->getMensajeTxt($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeTxt($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error=='2'){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error=='3'){ ?>
                <div class="error_archivo"><?= $general->getMensajeCabCompSistema($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabCompSistema($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error > 5){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'});
                </script>
            <?php } ?>
        </fieldset>
        <br />

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Tercera Parte	Usuarios por m&oacute;dulo</span></p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Usuarios por m&oacute;dulo"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Para generar el output del archivo damos click en el icono.  Elegir <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversion"</span>.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Hacer click al icono crear y el fichero ser&aacute; guardado con el nombre <span style="font-weight:bold;">"Output_1_texto_Usuarios x Modulo.txt"</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>CARGAR EL RESULTADO</p><br>
        
        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">Usuarios x Modulo</legend>
            
            <form enctype="multipart/form-data" name="form_modulo" id="form_modulo" method="post" action="despliegue2.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionDespliegue" type="hidden" value="<?= $pestana ?>" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file1" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo1" accept='.txt'>
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_modulo').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <?php if($exito1=='1'){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }else{ ?>

            <?php }
            if($error1=='1'){ ?>
                <div class="error_archivo"><?= $general->getMensajeTxt($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeTxt($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error1=='2'){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error1=='3'){ ?>
                <div class="error_archivo"><?= $general->getMensajeCabUsuModulo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabUsuModulo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error1 == 5){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'});
                </script>
            <?php } ?>
        </fieldset>
        <br />

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Cuarta Parte Usabilidad de Usuarios</span></p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Aging Usuarios"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Para generar el output del archivo damos click en el icono.  Elegir <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversion"</span>.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Hacer click al icono crear y el fichero ser&aacute; guardado con el nombre <span style="font-weight:bold;">"Output_1_texto Aging Usuarios.txt"</span></p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>CARGAR EL RESULTADO</p><br>

        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">Aging Usuarios</legend>
            
            <form enctype="multipart/form-data" name="form_usabilidad" id="form_usabilidad" method="post" action="despliegue3.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionDespliegue" type="hidden" value="<?= $pestana ?>" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file2" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo2" accept=".txt"> 
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_usabilidad').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <?php if($exito2=='1'){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>" . $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }else{ ?>

            <?php }
            if($error2=='1'){ ?>
                <div class="error_archivo"><?= $general->getMensajeTxt($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeTxt($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error2=='2'){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error2=='3'){ ?>
                <div class="error_archivo"><?= $general->getMensajeCabAging($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabAging($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error2 == 5){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'});
                </script>
            <?php } ?>
        </fieldset>
        <br />
    </div>

    <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='compras.php';">Compras</div></div>
</div>

<script>
    $(document).ready(function(){
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
        
        $("#archivo1").change(function(e){
            $("#nombreArchivo").val("#url_file1");
            addArchivo(e);
        });
        
        $("#archivo2").change(function(e){
            $("#nombreArchivo").val("#url_file2");
            addArchivo(e);
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
</script>