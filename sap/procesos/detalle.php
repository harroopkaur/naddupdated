<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usabilidad_usuarios_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalenciasPDO.php");
require_once($GLOBALS["app_root"] . "/clases/paginacion.php");

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;
$result = false;

$tablaMaestra    = new tablaMaestra();
$resumen         = new ResumenSAP();
$usabilidad      = new UsabilidadUsuariosSAP();
$equivalencia    = new EquivalenciasPDO();
$general         = new General();

$total_i     = 0;
$vert1 = "";
if(isset($_GET['vert'])){
    $vert  = $general->get_escape($_GET['vert']);
    if(isset($_GET['vert1'])){
        $vert1 = $general->get_escape($_GET['vert1']); 
        if($vert1 === "Enterprise Extension Defense Forces "){
            $vert1 = "Enterprise Extension Defense Forces & Public Security";
        }
    }
}
else{
    $vert  = 'SAP Módulo';
    $vert1 = '';
}

$ediciones = array();
if($vert != "Usabilidad"){
    
    //$ediciones = $equivalencia->edicionesProductoxNombre(5, $vert);
    $ediciones = $tablaMaestra->listaComponentesSAP($vert);
    /*if(!isset($_GET['vert1'])){
        $vert1 = $ediciones[0]["nombre"];
    }*/
    
    $titulo = $vert . ' ' . $vert1;
}
else{
    $titulo = "Usabilidad";
}

if($vert != "Usabilidad" && $vert1 != ""){    
    $result = $resumen->listar_datos6Agrupado($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1);
} else if($vert != "Usabilidad" && $vert1 == ""){    
    $result = $resumen->listar_datos6AgrupadoTodos($_SESSION['client_id'], $_SESSION['client_empleado'], $vert);
} else{
    /*if($usabilidad->listadoAlta($_SESSION['client_id'], $_SESSION['client_empleado'])){
        $result = $usabilidad->listadoBaja($_SESSION['client_id'], $_SESSION['client_empleado']);
    }
    
    if($usabilidad->listadoAltaGrafico($_SESSION['client_id'], $_SESSION['client_empleado'])){
        $result = $usabilidad->listadoBajaGrafico($_SESSION['client_id'], $_SESSION['client_empleado']);
    }*/
    
    $tablaUsabilidad = $usabilidad->listadoUsabilidad($_SESSION['client_id'], $_SESSION['client_empleado']);
    $graficoUsabilidad = $usabilidad->listadoUsabilidadGrafico($_SESSION['client_id'], $_SESSION['client_empleado']);
    
    $uso = 0;
    $probable = 0;
    $obsol = 0;
    foreach($graficoUsabilidad as $row){
        if($row["usabilidad"] == "En Uso"){
            $uso = $row["cantidad"];
        } else if($row["usabilidad"] == "Probablemente en Uso"){
            $probable = $row["cantidad"];
        } else{
            $obsol = $row["cantidad"];
        }
    }
}

if($vert != "Usabilidad" && count($result) > 0){
    $total_i = 0;
    foreach($result as $reg_calculo){
        $total_i += $reg_calculo["cantidad"];
    }	
}

$tablaMaestra->productosSalida(5);

$limite = $general->limit_paginacion;
$pagina = 1;
$inicio = ($pagina * $limite) - $limite;

//if($vert != "Otros" && $vert1 != ""){
$listado = $resumen->listar_datos6Paginado($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1, $inicio, $limite);
$totalListado =  $resumen->listar_datos6Total($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1);
/*} else if($vert != "Otros" && $vert1 == ""){
    $listado = $resumen->listar_datos6Paginado($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1, $inicio);
}*/

//$listado = array();

$paginacion = new Paginacion($totalListado, $pagina, "nuevaPagina");