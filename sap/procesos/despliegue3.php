<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usabilidad_usuarios_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$usabilidad = new UsabilidadUsuariosSAP();
$archivosDespliegue = new clase_archivos_fabricantes();
$general = new General();
$log = new log();

$exito=0;
$error=0;
$exito1=0;
$error1=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;
$exito4=0;
$error4=0;
$exito5=0;
$error5=0;

if(isset($_POST['insertar']) && $_POST['insertar'] == 1) {
    if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        $nombre_imagen = "Output_1_texto Aging Usuarios.txt"; //$_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // ValidacioWnes  
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "txt") && ($extension[$long] != "TXT")) { $error = 1; }
        }else{
            $error2=2;	
        }
    }
    else{
        $error2 = 1;
    }

    if($error2 == 0) {
        $carpeta = "archivos_csvf3";

        if(!file_exists($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
            mkdir($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
        }

        if(!file_exists($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp")){
            mkdir($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp", 0777, true);
        }

        $nombreArchivo = "Output_1_texto Aging Usuarios" . date("dmYHis");
        move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".txt");

        copy($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".txt", $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".csv");

        $usabilidad->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);

        $numFilas = 1;
        if(($fichero = fopen($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".csv", "r")) !== FALSE) {
            $error2 = 3;
            while (($datos = fgetcsv($fichero, 1000, "|")) !== FALSE) {
                if($numFilas == 4 && $datos[1] != "Usuarios" && $datos[2] != "Fe.creac." && $datos[3] != "Fecha Baja" && 
                $datos[4] != "Alta" && $datos[5] != "Baja" && utf8_encode($datos[6]) != 'Días de Ultima Entrada'){
                    $error2 = 3;
                    unlink($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/".$nombreArchivo . ".txt");
                    unlink($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/".$nombreArchivo . ".csv");
                    break;
                }
                else{
                    $error2 = 0;
                }
                $numFilas++;
            }
            fclose($fichero);
        }

        if($error2 == 0){
            if(($fichero = fopen($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] .  "/".$nombreArchivo.".csv", "r")) !== FALSE) {
                $i=1;
                while (($datos = fgetcsv($fichero, 1000, "|")) !== FALSE) {
                    if($i!=1){
                        if($i > 5 && $i < ($numFilas - 1)){
                            $alta = 0; 
                            if(filter_var($datos[4], FILTER_VALIDATE_INT) !== false){
                                $alta = $datos[4];
                            }
                            
                            $baja = 0;
                            if(filter_var($datos[5], FILTER_VALIDATE_INT) !== false){
                                $baja = $datos[5];
                            }
                            
                            $dias = 0;
                            if(filter_var($datos[6], FILTER_VALIDATE_INT) !== false){
                                $dias = $datos[6];
                            }
                            
                            if(!$usabilidad->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[1], $datos[2], $datos[3], $alta, $baja, $dias)){
                                echo $usabilidad->error;
                            }
                        }
                    }
                    $i++;
                    $exito2=1;	
                }
                fclose($fichero);

                if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 5) === false){
                    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 5);
                }

                $archivosDespliegue->actualizarDespliegue3($_SESSION["client_id"], $_SESSION["client_empleado"], 5, $nombreArchivo.".txt");
            }
            $exito1=2; 
        }
        
        $log->insertar(14, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue Aging Usuarios");
    }
}