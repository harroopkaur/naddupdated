<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_Sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_servidores_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_usuarios_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

$tablaMaestra      = new tablaMaestra();
$balance           = new balanceSap();
$resumenServidores = new ResumenServidoresSAP();
$resumenUsuarios   = new ResumenUsuariosSAP();
$general = new General();

$tablaMaestra->productosSalida(5);
$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';
    
$TotalSAPServerCompras = 0;
$TotalSAPServerUsar    = 0;
$TotalSAPServerSinUsar = 0;

$TotalSAPModuloCompras = 0;
$TotalSAPModuloUsar    = 0;
$TotalSAPModuloSinUsar = 0;

$clientTotalProductCompras = 0;
$clientTotalProductInstal  = 0;
$clientProductNeto         = 0;

//Application
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Server")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalSAPServerCompras += $reg_equipos["compra"];
    }
}

if($resumenServidores->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Server")) {
    foreach ($resumenServidores->lista as $reg_equipos) {
        $TotalSAPServerUsar  += $reg_equipos["cantidad"];
    }
}

$TotalSAPServerInstal = $TotalSAPServerUsar;
$SAPServerNeto        = $TotalSAPServerCompras - $TotalSAPServerInstal;


//Middleware
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Módulo")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalSAPModuloCompras += $reg_equipos["compra"];
    }
}

if($resumenUsuarios->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Módulo")) {
    foreach ($resumenUsuarios->lista as $reg_equipos) {
        $TotalSAPModuloUsar  += $reg_equipos["cantidad"];
    }
}

$TotalSAPModuloInstal = $TotalSAPModuloUsar;
$SAPModuloNeto        = $TotalSAPModuloCompras - $TotalSAPModuloInstal;