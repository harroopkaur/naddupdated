<?php
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuarios_modulo_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_usuarios_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

$tablaMaestra   = new tablaMaestra();
$usuariosModulo = new usuariosModuloSAP();
$resumen        = new ResumenUsuariosSAP();
$archivosDespliegue = new clase_archivos_fabricantes();
$general = new General();
$log = new log();

$exito=0;
$error=0;
$exito1=0;
$error1=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;
$exito4=0;
$error4=0;
$exito5=0;
$error5=0;

if(isset($_POST['insertar']) && $_POST['insertar'] == 1) {
    if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        $nombre_imagen = "Output_1_texto_Usuarios x Modulo.txt"; //$_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // ValidacioWnes  
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "txt") && ($extension[$long] != "TXT")) { $error = 1; }
        }else{
            $error1=2;	
        }
    }
    else{
        $error1 = 1;
    }

    if($error1 == 0) {
        $carpeta = "archivos_csvf2";

        if(!file_exists($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
            mkdir($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
        }

        if(!file_exists($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp")){
            mkdir($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp", 0777, true);
        }

        $nombreArchivo = "Output_1_texto_Usuarios x Modulo" . date("dmYHis");
        move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".txt");

        copy($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".txt", $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".csv");

        $usuariosModulo->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);

        $numFilas = 1;
        if(($fichero = fopen($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/".$nombreArchivo.".csv", "r")) !== FALSE) {
            $error1 = 3;
            while (($datos = fgetcsv($fichero, 1000, "|")) !== FALSE) {
                if($numFilas == 2 && $datos[1] != "Usuarios" && $datos[3] != "Componente" && $datos[4] != "Nombre Componente" && utf8_encode($datos[7]) != 'Días del última acceso'){
                    $error1 = 3;
                    unlink($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".txt");
                    unlink($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".csv");
                    break;
                }
                else{
                    $error1 = 0;
                }
                $numFilas++;
            }
            fclose($fichero);
        }

        if($error1 == 0){
            if(($fichero = fopen($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo.".csv", "r")) !== FALSE) {
                $i=1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                while (($datos = fgetcsv($fichero, 1000, "|")) !== FALSE) {
                    if($i!=1){
                        if($i > 3 && $i < ($numFilas - 1)){
                            /*if(!$usuariosModulo->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[1], $datos[3], utf8_encode($datos[4]), $datos[7])){
                                echo $usuariosModulo->error;
                            }*/
                            
                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :usuarios" . $j . ", :componente" . $j . ", :nombreComponente" . $j . ", :diasUltAcceso" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :usuarios" . $j . ", :componente" . $j . ", :nombreComponente" . $j . ", :diasUltAcceso" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":usuarios" . $j] = $general->truncarString($datos[1], 250);
                            $bloqueValores[":componente" . $j] = $general->truncarString($datos[3], 250);
                            $bloqueValores[":nombreComponente" . $j] = $general->truncarString(utf8_encode($datos[4]), 250);
                            $bloqueValores[":diasUltAcceso" . $j] = $datos[7];

                            if($j == $general->registrosBloque){
                                if(!$usuariosModulo->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo $usuariosModulo->error;
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                    }
                    $i++;
                    $exito1=1;	
                }
                
                if($insertarBloque === true){
                    if(!$usuariosModulo->insertarEnBloque($bloque, $bloqueValores)){    
                        echo $usuariosModulo->error;
                    }
                }
                
                fclose($fichero);

                if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 5) === false){
                    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 5);
                }

                $archivosDespliegue->actualizarDespliegue2($_SESSION["client_id"], $_SESSION["client_empleado"], 5, $nombreArchivo.".txt");
            }

            //resumen usuarios SAP
            $resumen->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

            /*$tablaMaestra->listadoProductosMaestra(5);
            foreach($tablaMaestra->listaProductos as $rowProductos){*/
                //if($usuariosModulo->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["descripcion"], $rowProductos["campo2"])){
                    
                    $usuariosModulo->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"]);
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;
                    foreach($usuariosModulo->lista as $reg_r){  
                        $componentes = $tablaMaestra->productosSustituirComponente(5, $reg_r["componente"]);
                        $producto  = $componentes["sustituir"];
                        $edicion = $componentes["grupo"];
                        $version = $componentes["descripcion"];
                        $idForaneo = $componentes["idForaneo"];    
                        
                        
                        /*if($tablaMaestra->productosSustituir(5, $reg_r["nombreComponente"])){
                            $producto  = $tablaMaestra->sustituir;
                            $idForaneo = $tablaMaestra->idForaneo;
                        }           

                        if($tablaMaestra->buscarSustituirMaestra($reg_r["nombreComponente"], $idForaneo, 4)){
                            if($tablaMaestra->sustituir == null){
                                $version = "";
                            }
                            else{
                                $version = $tablaMaestra->sustituir;
                            }
                        }

                        $tablaMaestra->sustituir = "";
                        if($tablaMaestra->buscarSustituirMaestra($reg_r["nombreComponente"], $idForaneo, 5)){
                            if($tablaMaestra->sustituir == null){
                                $edicion = "";
                            }
                            else{
                                $edicion = $tablaMaestra->sustituir;
                            }
                        }*/

                        /*if(!$resumen->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_r["usuarios"], $reg_r["componente"], $producto, $edicion, $version, $reg_r["diasUltAcceso"])){
                            echo $resumen->error;
                        }*/
                        
                        if($j == 0){
                            $insertarBloque = true;
                            $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :usuarios" . $j . ", :componente" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :diasUltAcceso" . $j . ")";
                        } else {
                            $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :usuarios" . $j . ", :componente" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :diasUltAcceso" . $j . ")";
                        } 

                        $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                        $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                        $bloqueValores[":usuarios" . $j] = $general->truncarString($reg_r["usuarios"], 250);
                        $bloqueValores[":componente" . $j] = $general->truncarString($reg_r["componente"], 250);
                        $bloqueValores[":familia" . $j] = $general->truncarString($producto, 70);
                        $bloqueValores[":edicion" . $j] = $general->truncarString($edicion, 70);
                        $bloqueValores[":version" . $j] = $general->truncarString($version, 70);
                        $bloqueValores[":diasUltAcceso" . $j] = $reg_r["diasUltAcceso"];

                        if($j == $general->registrosBloque){
                            if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo $resumen->error;
                            }

                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }
                    
                    if($insertarBloque === true){
                        if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){    
                            echo $resumen->error;
                        }
                    }
                //}
            //}
            //fin resumen usuarios SAP
            $exito1=1; 
        }
        
        $log->insertar(14, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue Usuarios por Módulo");
    }
}