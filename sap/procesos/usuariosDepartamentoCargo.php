<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_sap.php");
require_once($GLOBALS["app_root"] . "/clases/paginacion.php");

$error  = 0;

$resumen         = new ResumenSAP();
$general         = new General();

$limite = $general->limit_paginacion;
if(isset($_REQUEST["limite"]) && ctype_digit($_REQUEST["limite"])){
    $limite = $_REQUEST["limite"];
}

$pagina = 1;
if(isset($_REQUEST["pagina"]) && ctype_digit($_REQUEST["pagina"])){
    $pagina = $_REQUEST["pagina"];
}

$inicio = ($pagina * $limite) - $limite;

$listado = $resumen->listar_datos6Paginado($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Módulo", "", $inicio, $limite);
$totalListado =  $resumen->listar_datos6Total($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Módulo", "");

$paginacion = new Paginacion($totalListado, $pagina, "nuevaPagina");