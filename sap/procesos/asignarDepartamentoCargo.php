<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_usuarios_sap.php");
require_once($GLOBALS["app_root"] . "/clases/paginacion.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$error  = 0;

$resumen         = new ResumenSAP();
$usuarioSap = new ResumenUsuariosSAP();
$general         = new General();
$log = new log();

$insertar = 0;
if(isset($_POST["insertar"]) && $_POST["insertar"] == 1){
    $insertar = $_POST["insertar"];
    $idUsuario = $_POST["idUsuario"];
    $departamento = $_POST["departamento"];
    $cargo = $_POST["cargo"];

    $j = 0;
    for($i = 0; $i < count($idUsuario); $i++){
        $id = 0;
        if(filter_var($idUsuario[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idUsuario[$i];
        }

        $depart = null;
        if(isset($departamento[$i]) && $departamento[$i] != ""){
            $depart = $general->get_escape($departamento[$i]);
        }

        $cargo1 = null;
        if(isset($cargo[$i]) && $cargo[$i] != ""){
            $cargo1 = $general->get_escape($cargo[$i]);
        }

        if($usuarioSap->actualizarDepartCargo($id, $depart, $cargo1)){
            $j++;
        }
    }

    $result = 0;
    if($i != $j && $j > 0){
        $result = 2;
    } else if($i == $j){
        $result = 1;
    }
    
    $log->insertar(16, 2, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
}

$limite = $general->limit_paginacion;
if(isset($_REQUEST["limite"]) && (filter_var($_REQUEST["limite"], FILTER_VALIDATE_INT) !== false || ctype_digit($_REQUEST["limite"]))){
    $limite = $_REQUEST["limite"];
}

$pagina = 1;
if(isset($_REQUEST["pagina"]) && (filter_var($_REQUEST["pagina"], FILTER_VALIDATE_INT) !== false || ctype_digit($_REQUEST["pagina"]))){
    $pagina = $_REQUEST["pagina"];
}

$inicio = ($pagina * $limite) - $limite;

$listado = $resumen->listar_datos6Paginado($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Módulo", "", $inicio, $limite);
$totalListado =  $resumen->listar_datos6Total($_SESSION['client_id'], $_SESSION['client_empleado'], "SAP Módulo", "");

$paginacion = new Paginacion($totalListado, $pagina, "nuevaPagina");