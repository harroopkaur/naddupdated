<?php
//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once("../plantillas/middleware.php");
//fin middleware

require_once("../clases/clase_general.php");
require_once("../clases/clase_balance_Sap.php");
require_once("../clases/clase_equivalencias.php");
require_once("../clases/clase_compras_SAP.php");
require_once("../clases/clase_resumen_servidores_sap.php");
require_once("../clases/clase_resumen_usuarios_sap.php");
require_once("../sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$compra_datos         = new comprasSAP();
$resumenServidores    = new ResumenServidoresSAP();
$resumenUsuarios      = new ResumenUsuariosSAP();
//$resumenLinux         = new resumenOracleLinux($conn);
$balance_datos        = new balanceSap();
$equivalencia         = new Equivalencias();
$general              = new General();
/*$detalleSolaris       = new DetallesOracleSolaris($conn);
$detalleAIX           = new DetallesOracleAIX($conn);
$detalleLinux         = new DetallesOracleLinux($conn);*/
$log = new log();

$nueva_funcion = new funcionesSam();
$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 5);

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if (isset($_POST['insertarRepoF']) && $_POST["insertarRepoF"] == 1) {
    if ($error == 0) {
        $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $balance_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

        $tablaCompraRepo = array();
        if (isset($_POST["check"])) {
            $tablaCompraRepo = $_POST["check"];

            for ($i = 0; $i < count($tablaCompraRepo); $i++) {
                $newArray = explode("*", $tablaCompraRepo[$i]);
                $familia  = $general->get_escape($newArray[0]);
                $edicion  = $general->get_escape($newArray[1]);
                $version  = $general->get_escape($newArray[2]);
                $cantidad = 0;
                if(filter_var($newArray[3], FILTER_VALIDATE_FLOAT) !== false){
                    $cantidad = $newArray[3];
                }
                $precio = 0;
                if(filter_var($newArray[4], FILTER_VALIDATE_FLOAT) !== false){
                    $precio = $general->get_escape($newArray[4]);
                }
                if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], trim($familia), trim($edicion), trim($version), $cantidad, $precio)) {
                    echo $compra_datos->error;
                } else {

                    //$i++;

                    $exito = 1;
                }//exito*/
            }//WhILE
        }
    }

    if ($exito == 1){
        $listaEquivalencia = $equivalencia->listar_todo(5); //8 es el id fabricante SAP
        if ($listaEquivalencia) {
            foreach ($listaEquivalencia as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;
                
                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
                
                if($compra_datos->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    $resultCompra = $compra_datos->row;
                    $compra       = $resultCompra["compra"];
                    $precio       = $resultCompra["precio"];
                }
                $tipo = 0;
                if ($resumenServidores->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenServidores->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                        $tipo = 2;
                    }//foreach resumen
                }// ifresumen
                
                if ($resumenUsuarios->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenUsuarios->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                        $tipo = 1;
                    }//foreach resumen
                }// ifresumen
                
                if($compra == ""){
                    $compra = 0;
                }
                
                if($precio == ""){
                    $precio = 0;
                }
                
                $balancec1 = $compra - $instalaciones;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
            }//office
        }//office

        $exito2 = 1;
        
        $log->insertar(15, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Compras Repositorio");
    }
}