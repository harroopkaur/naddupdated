<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_Sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_SAP.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_servidores_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_usuarios_sap.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$compra_datos         = new comprasSAP();
$resumenServidores    = new ResumenServidoresSAP();
$resumenUsuarios      = new ResumenUsuariosSAP();
$balance_datos        = new balanceSap();
$equivalencia         = new Equivalencias();
$general              = new General();
$archivosDespliegue   = new clase_archivos_fabricantes();
$log = new log();

$nueva_funcion = new funcionesSam($conn);
$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 5);

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if (isset($_POST['insertar']) && $_POST['insertar'] == 1) {

    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];

    // Validaciones
    $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
    $long = count($extension) - 1;
    if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
        $error = 1;
    }  // Permitir subir solo imagenes JPG,

    if ($error == 0) {

        $imagen = $_SESSION['client_id'] . $_SESSION["client_empleado"] . "_compraSAP" . date("dmYHis") . ".csv";

        if (!$compra_datos->cargar_archivo($imagen, $temporal_imagen)) {
            $error = 5;
        } else {

            $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            $balance_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            
            if($general->obtenerSeparador("archivos_csvf4/" . $imagen) === true){
                if (($fichero = fopen("archivos_csvf4/" . $imagen, "r")) !== FALSE) {
                    $i = 1;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i == 1 && ($datos[0] != "Producto" || utf8_encode($datos[1]) != "Edición" || utf8_encode($datos[2]) != "Versión" || 
                        $datos[3] != "SKU" || $datos[4] != "Tipo" || $datos[5] != "Cantidades" || $datos[6] != "Precio Unitario" ||
                        utf8_encode($datos[7]) != "Asignación")){
                            $error = 3;
                            unlink("archivos_csvf4/" . $imagen);
                            break;
                        }
                        $i = 1;

                        $compraCantidad = 0;
                        if(filter_var($datos[5], FILTER_VALIDATE_INT) !== false){
                            $compraCantidad = $datos[5];
                        }
                        
                        $precio = 0;
                        if(filter_var($datos[6], FILTER_VALIDATE_FLOAT) !== false){
                            $precio = $datos[6];
                        }

                        if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], utf8_encode($datos[0]), $datos[1], $datos[2], $compraCantidad, $precio)) {
                            echo $compra_datos->error;
                        } else {

                            $i++;

                            $exito = 1;
                        }//exito
                    }//WhILE
                    
                    if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 5) === false){
                        $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 5);
                    }
                    $archivosDespliegue->actualizarCompras($_SESSION["client_id"], $_SESSION["client_empleado"], 5, $imagen);
                }//FICHERO
            }
            else{
                $error = 4;
            }
        }//cago
    }//is error


    if ($exito == 1){
        $listaEquivalencia = $equivalencia->listar_todo(5); //8 es el id fabricante UNIX-Oracle
        if ($listaEquivalencia) {
            foreach ($listaEquivalencia as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;

                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
                
                if($compra_datos->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    $resultCompra = $compra_datos->row;
                    $compra       = $resultCompra["compra"];
                    $precio       = $resultCompra["precio"];
                }
                $tipo = 0;
                if ($resumenServidores->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenServidores->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                        $tipo = 2;
                    }//foreach resumen
                }// ifresumen
                
                if ($resumenUsuarios->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenUsuarios->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                        $tipo = 1;
                    }//foreach resumen
                }// ifresumen
                
                if($compra == ""){
                    $compra = 0;
                }
                
                if($precio == 0 || $precio == ""){
                    $precio = $reg_o["precio"];
                }
                
                $balancec1 = $compra - $instalaciones;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                //$balance_datos->insertar($_SESSION['client_id'],$newFamilia,$newEdicion, $newVersion,$reg_o->precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
            }//office
        }//office

        $exito2 = 1;
        
        $log->insertar(15, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Compras Formulario");
    }
}