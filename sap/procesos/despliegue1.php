<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_servidores_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_servidores_sap.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$tablaMaestra = new tablaMaestra();
$detalle      = new DetallesSAP();
$resumen      = new ResumenServidoresSAP();
$archivosDespliegue = new clase_archivos_fabricantes();
$general = new General();
$log = new log();

$exito=0;
$error=0;
$exito1=0;
$error1=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;
$exito4=0;
$error4=0;
$exito5=0;
$error5=0;

if(isset($_POST['insertar']) && $_POST['insertar'] == 1) {
    if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // ValidacioWnes  
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "txt") && ($extension[$long] != "TXT")) { $error = 1; }
        }else{
            $error=2;	
        }
    }
    else{
        $error = 1;
    }

    if($error == 0) {
        $carpeta = "archivos_csvf1";
        $nombreArchivo = "Output_1_texto Componentes Sistema" . date("dmYHis");

        if(!file_exists($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
            mkdir($GLOBALS['app_root'] . "/sap/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
        }

        move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] . "/sap/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".txt");

        copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".txt", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo . ".csv");

        $detalle->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);

        $numFilas = 1;
        if(($fichero = fopen("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo.".csv", "r")) !== FALSE) {
            $error = 3;
            while (($datos = fgetcsv($fichero, 1000, "|")) !== FALSE) {
                if($numFilas == 2 && $datos[2] != "Servidor" && $datos[5] != "Comp.soft." && utf8_encode($datos[8]) != "Descripción breve componente software"){
                    $error = 3;
                    unlink("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo.".txt");
                    unlink("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo.".csv");
                    break;
                }
                else{
                    $error = 0;
                }
                $numFilas++;
            }
            fclose($fichero);
        }

        if($error == 0){
            if(($fichero = fopen("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/".$nombreArchivo.".csv", "r")) !== FALSE) {
                $i=1;
                while (($datos = fgetcsv($fichero, 1000, "|")) !== FALSE) {
                    if($i!=1){
                        if($i > 3 && $i < ($numFilas - 1)){
                            if(!$detalle->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[2], $datos[5], utf8_encode($datos[8]))){
                                echo $detalle->error;
                            }
                        }
                    }
                    $i++;
                    $exito=1;	
                }
                fclose($fichero);

                if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 5) === false){
                    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 5);
                }

                $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], 5, $nombreArchivo . ".txt", 0);
            }

            //resumen servidores SAP
            $resumen->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

            $tablaMaestra->listadoProductosMaestra(5);
            foreach($tablaMaestra->listaProductos as $rowProductos){
                //if($detalle->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["descripcion"], $rowProductos["campo2"])){
                
                $tablaComponentes = $detalle->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["campo2"]);
                $version = "";
                
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                foreach($tablaComponentes as $reg_r){                       
                    $componentes = $tablaMaestra->productosSustituirComponente(5, $reg_r["compSoft"]);
                    $producto  = $componentes["sustituir"];
                    $edicion = $componentes["grupo"];
                    $version = $componentes["descripcion"];
                    $idForaneo = $componentes["idForaneo"];

                    if($idForaneo != "" && $producto != ""){
                        /*if(!$resumen->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_r["equipo"], $reg_r["compSoft"], $producto, $edicion, $version)){
                            echo $resumen->error;
                        }*/
                        
                        if($j == 0){
                            $insertarBloque = true;
                            $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :compSoft" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                        } else {
                            $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :compSoft" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                        } 
                                                
                        $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                        $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                        $bloqueValores[":equipo" . $j] = $general->truncarString($reg_r["equipo"], 250);
                        $bloqueValores[":compSoft" . $j] = $general->truncarString($reg_r["compSoft"], 250);
                        $bloqueValores[":familia" . $j] = $general->truncarString($producto, 70);
                        $bloqueValores[":edicion" . $j] = $general->truncarString($edicion, 70);
                        $bloqueValores[":version" . $j] = $version;
                        
                        if($j == $general->registrosBloque){
                            if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo $resumen->error;
                            }
                            
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }
                }  
                
                if($insertarBloque === true){
                    if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){    
                    }
                }
            }
            //fin resumen servidores SAP

            $exito=1; 
        }
        
        $log->insertar(14, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue Componentes del Sistema");
    }
}