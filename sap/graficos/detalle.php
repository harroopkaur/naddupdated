<script type="text/javascript">
    $(function () {
        Highcharts.setOptions({
            colors: ['#DCDCDC', '#99BFDC', '#243C67', '#00AFF0', '#006FC0', '#95DEF8', '#579BCC', 
            '#344970', '#047FB3', '#05B5FE', '#81816A']
        });
        
        <?php 
        if($vert != "Usabilidad"){
        ?>
            $('#container3').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: '<?= $titulo ?>'
                },
                subtitle: {
                    text: '<?= $vert ?>'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Unidades'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
                },

                series: [{
                    name: '<?= $vert1 ?>',
                    colorByPoint: true,
                    data: [
                    <?php
                        $i = 0;
                        foreach($result as $reg_calculo){
                            ?>
                            {
                                name: '<?= $reg_calculo["familia"] . ' ' . $reg_calculo["edicion"] ?>',
                                y: <?= $reg_calculo["cantidad"] ?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                            <?php
                            $i++;
                        }	
                    ?>
                    ]
                }]
            });
        <?php 
        }
        else{
        ?>
            $('#container3').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Usabilidad'
                },
                subtitle: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Nro. Usuarios'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Usuarios: <b>{point.y:.0f}</b> de <b><?= count($tablaUsabilidad) ?></b><br/>'
                },
                series: [{
                    name: 'Usuarios',
                    colorByPoint: true,
                    data: [{
                        name: 'En Uso',
                        y: <?= round($uso, 0) ?>,
                        color: Highcharts.getOptions().colors[1]
                    },
                    {
                        name: 'Probablemente en Uso',
                        y: <?= round($probable, 0) ?>,
                        color: Highcharts.getOptions().colors[2]
                    },
                    {
                        name: 'Obsoleto',
                        y: <?= round($obsol, 0) ?>,
                        color: Highcharts.getOptions().colors[3]
                    }]
                }]
            });
        <?php
        }
        ?>
    });
</script>