<script type="text/javascript">
    $(function () {
        $('#container3').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'SAP Server'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Neto',
                data: [0, 0, <?= $SAPServerNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalSAPServerUsar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalSAPServerCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container4').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'SAP Módulo'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Neto',
                data: [0, 0, <?= $SAPModuloNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalSAPModuloUsar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalSAPModuloCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });
    });
</script>
  