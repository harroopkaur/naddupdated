*&---------------------------------------------------------------------*
*& Report  ZSISTDET
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*

REPORT  ZSISTDET.

*&---------------------------------------------------------------------*
*&                                                                     *
*&   Declaraciones Globales                                            *
*&                                                                     *
*&---------------------------------------------------------------------*

TABLES: CVERS, CVERS_REF, T000, AGR_USERS, AGR_TCODES, AGR_1251,
  TADIR, TDEVC, DF14L, DF14T, USR02, USH04.


DATA: BEGIN OF i_cvers,
  mandt like sy-mandt,
  cccategory like t000-cccategory,
  desc_cat(20) type c,
  component like cvers-component,
  release like cvers-release,
  extrelease like cvers-extrelease,
  desc_text like cvers_ref-desc_text,
END OF i_cvers.

DATA: xcvers like standard table of i_cvers.

DATA: wa_xcvers like line of xcvers.

DATA: BEGIN OF zcvers occurs 10,
   mandt like sy-mandt,
  cccategory like t000-cccategory,
  desc_cat(20) type c,
  component like cvers-component,
  release like cvers-release,
  extrelease like cvers-extrelease,
  desc_text like cvers_ref-desc_text.
DATA: END OF zcvers.


DATA: BEGIN OF ztcodes occurs 10,
     uname like agr_users-uname,
     tcode like agr_tcodes-tcode,
     ps_posid like df14l-ps_posid,
     name like df14t-name.
DATA: END OF ztcodes.


DATA: BEGIN OF zustrx occurs 10,
    uname like agr_users-uname,
    fam(3) type c,
    ps_posid like df14l-ps_posid,
    func(30) type c,
    inst(10) type p,
    licad(10) type c,
    flag(1) type c,
    diaul(15) type p.
DATA: END OF zustrx.

DATA: BEGIN OF zusage occurs 10,
     bname like usr02-bname,
     erdat like usr02-erdat,
     modda like ush04-modda,
     alta(5) type p,
     baja(5) type p,
     diaul(15) type p,
     notas(10) type c.
DATA: END OF zusage.

data: lv_diff type p.

*** Par�metros para salida ALV

FIELD-SYMBOLS <FS> TYPE ANY.

*_ALV  Campo para almacenar el nombre del reporte

data i_repid like sy-repid.

*_ALV  Campo para validar la longitud de la tabla.

data i_lines like sy-tabix.

*_ALV  Datos para mostrar salida ALV

TYPE-POOLS: SLIS.

data int_fcat type SLIS_T_FIELDCAT_ALV.

data int_fcat2 type SLIS_T_FIELDCAT_ALV.

data int_fcat3 type SLIS_T_FIELDCAT_ALV.

data: ls_fieldcat type slis_fieldcat_alv. "Nombres de Columnas

*ALV-- Para Eventos

data: it_events type slis_alv_event.
data: it_events1 type slis_t_event.
data: G_REPID LIKE SY-REPID.

*ALV- Para Ordenar los datos

DATA : it_sort TYPE slis_t_sortinfo_alv,
       wa_sort TYPE slis_sortinfo_alv.

*&---------------------------------------------------------------------*
*&                                                                     *
*&   Par�metros de Selecci�n                                           *
*&                                                                     *
*&---------------------------------------------------------------------*

SELECTION-SCREEN BEGIN OF BLOCK ONE WITH FRAME TITLE TEXT-001.

  PARAMETERS:
      REP1 RADIOBUTTON GROUP G1 USER-COMMAND ENT,
      REP2 RADIOBUTTON GROUP G1,
      REP3 RADIOBUTTON GROUP G1.


SELECTION-SCREEN END OF BLOCK ONE.


*&---------------------------------------------------------------------*
*&                                                                     *
*&   Proceso Principal                                                 *
*&                                                                     *
*&---------------------------------------------------------------------*

START-OF-SELECTION.

IF REP1 = 'X'.

  PERFORM BD_COMPONENTES.

ELSEIF REP2 = 'X'.

  PERFORM BD_USUARIOS_MODULO.

ELSEIF REP3 = 'X'.

  PERFORM BD_AGING_USUARIOS.

ENDIF.


END-OF-SELECTION.

*&---------------------------------------------------------------------*
*&      Form  BD_COMPONENTES
*&---------------------------------------------------------------------*

FORM BD_COMPONENTES.

clear xcvers. clear wa_xcvers.

select * from cvers.

  wa_xcvers-mandt = sy-mandt.

  select * from t000 where mandt = wa_xcvers-mandt.
  wa_xcvers-cccategory = t000-cccategory.
  exit.
  endselect.

  case  wa_xcvers-cccategory.
    when 'P'.
   wa_xcvers-desc_cat = 'Productivo'.
    when 'T'.
   wa_xcvers-desc_cat = 'Test'.
    when 'C'.
   wa_xcvers-desc_cat = 'Customizing'.
    when 'D'.
   wa_xcvers-desc_cat = 'Presentaci�n'.
    when 'E'.
   wa_xcvers-desc_cat = 'Formaci�n'.
    when 'S'.
   wa_xcvers-desc_cat = 'Referencia SAP'.
  endcase.

  wa_xcvers-component = cvers-component.
  wa_xcvers-release  = cvers-release.
  wa_xcvers-extrelease = cvers-extrelease.
  select * from cvers_ref where component = cvers-component
    and langu = sy-langu.
  wa_xcvers-desc_text = cvers_ref-desc_text.
  endselect.

  append wa_xcvers to xcvers.

endselect.

perform Genera_Listado_ALV.

  ENDFORM.


*&---------------------------------------------------------------------*
*&      Form  BD_USUARIOS_MODULO
*&---------------------------------------------------------------------*

FORM BD_USUARIOS_MODULO.

clear ztcodes.

*     uname like agr_users-uname,
*     tcode like agr_tcodes-tcode,
*     ps_posid like df14l-ps_posid.

* Base de Transacciones

select * from agr_users.

select * from agr_1251 where agr_name = agr_users-agr_name
  and object = 'S_TCODE'.

   ztcodes-uname = agr_users-uname.
   ztcodes-tcode = agr_1251-low.
   append ztcodes.

 endselect.

endselect.


* Asignaci�n de usuario por  M�dulo

loop at ztcodes.

  select * from tadir where PGMID = 'R3TR' and object = 'TRAN'
    and obj_name = ztcodes-tcode.

   select * from tdevc where devclass = tadir-devclass.

     select * from df14l where fctr_id = tdevc-component.

     ztcodes-ps_posid = df14l-ps_posid.

    select * from df14t where fctr_id = tdevc-component.
       ztcodes-name = df14t-name.
     endselect.


     modify ztcodes.

    endselect.

   endselect.

   endselect.
 endloop.

delete ztcodes where ps_posid is initial.


* Resumen de Usuarios por M�dulo al cual acceden.

clear zustrx. free zustrx.

loop at ztcodes.
    zustrx-uname =  ztcodes-uname.
    zustrx-fam = 'SAP'.
    zustrx-ps_posid = ztcodes-ps_posid.
    zustrx-func = ztcodes-name.
*    inst(10) type p,
*    licad(10) type c,

   collect zustrx.
endloop.

loop at zustrx.

  lv_diff = 0.

select * from usr02 where bname = zustrx-uname.

  CALL FUNCTION 'DAYS_BETWEEN_TWO_DATES'
    EXPORTING
i_datum_bis = sy-datum
i_datum_von = usr02-trdat
   IMPORTING
e_tage = lv_diff.

  zustrx-diaul = lv_diff.

endselect.

  if sy-subrc eq 0.
    modify zustrx.
  endif.

endloop.


perform Genera_Listado_ALV2.



  ENDFORM.


*&---------------------------------------------------------------------*
*&      Form  BD_AGINGN_USAURIOS
*&---------------------------------------------------------------------*

FORM BD_AGING_USUARIOS.

select * from usr02.

clear zusage.

     zusage-bname = usr02-bname.
     zusage-erdat = usr02-erdat.

select * from ush04 where bname = usr02-bname
  and nrpro = '1'
  order by modda ascending.
     zusage-modda = ush04-modda.
     exit.
endselect.

lv_diff = 0.

  CALL FUNCTION 'DAYS_BETWEEN_TWO_DATES'
    EXPORTING
i_datum_bis = sy-datum
i_datum_von = usr02-trdat
   IMPORTING
e_tage = lv_diff.

    zusage-diaul = lv_diff.

append zusage.

endselect.

* An�lisis de Alta y Baja

data: bajas(5) type p.

loop at zusage.
   bajas = 0.
  select * from ush04 where bname = zusage-bname
    and nrpro = '1'.
   bajas = bajas + 1.
  endselect.
   if sy-subrc = 0.
     zusage-baja = bajas.
     zusage-alta = bajas + 1.
  modify zusage.
   endif.
 endloop.

perform Genera_Listado_ALV3.

  ENDFORM.


*&---------------------------------------------------------------------*
*&      Form  actualiza_columnas_alv.
*&---------------------------------------------------------------------*
FORM actualiza_columnas_alv.

loop at int_fcat into ls_fieldcat.
*** Colocar Textos de Encabezados
case ls_fieldcat-fieldname.
when 'MANDT'.
ls_fieldcat-reptext_ddic  = 'Mandante'.
*ls_fieldcat-emphasize = 'C210'.
when 'TFIGL'.
ls_fieldcat-seltext_l  = 'Importe FI-GL ML'.
*ls_fieldcat-emphasize = 'C410'.
when 'DPCAGL'.
ls_fieldcat-seltext_l  = 'Diferencia ML'.
ls_fieldcat-emphasize = 'C310'.
endcase.
modify int_fcat from ls_fieldcat.
endloop.

ENDFORM.


*&---------------------------------------------------------------------*
*&      Form  actualiza_columnas_alv2.
*&---------------------------------------------------------------------*
FORM actualiza_columnas_alv2.

loop at int_fcat2 into ls_fieldcat.
*** Colocar Textos de Encabezados
case ls_fieldcat-fieldname.
when 'FAM'.
ls_fieldcat-reptext_ddic  = 'Familia'.
when 'INST'.
ls_fieldcat-reptext_ddic  = 'Instalaci�n'.
when 'LICAD'.
ls_fieldcat-reptext_ddic  = 'Licencia Adq'.
when 'DIAUL'.
ls_fieldcat-reptext_ddic  = 'D�as del �ltima acceso'.
when 'PS_POSID'.
ls_fieldcat-reptext_ddic  = 'Componente'.
when 'FUNC'.
ls_fieldcat-reptext_ddic  = 'Nombre Componente'.
when 'DIAUL'.
ls_fieldcat-reptext_ddic  = 'D�as de Ultima Entrada'.
ls_fieldcat-emphasize = 'C310'.
when 'FLAG'.
ls_fieldcat-reptext_ddic  = 'Activo'.
ls_fieldcat-no_out = 'X'.
*ls_fieldcat-emphasize = 'C410'.
endcase.
modify int_fcat2 from ls_fieldcat.
endloop.


*    uname like agr_users-uname,
*    fam(3) type c,
*    ps_posid like df14l-ps_posid,
*    func(30) type c,
*    inst(10) type p,
*    licad(10) type c,
*    diaul(15) type p.


ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  actualiza_columnas_alv3.
*&---------------------------------------------------------------------*
FORM actualiza_columnas_alv3.

loop at int_fcat3 into ls_fieldcat.
*** Colocar Textos de Encabezados
case ls_fieldcat-fieldname.
when 'MODDA'.
ls_fieldcat-reptext_ddic  = 'Fecha Baja'.
when 'ALTA'.
ls_fieldcat-reptext_ddic  = 'Alta'.
when 'BAJA'.
ls_fieldcat-reptext_ddic  = 'Baja'.
when 'DIAUL'.
ls_fieldcat-reptext_ddic  = 'D�as de Ultima Entrada'.
when 'NOTAS'.
ls_fieldcat-reptext_ddic  = 'Notas'.
endcase.
modify int_fcat3 from ls_fieldcat.
endloop.

ENDFORM.



*&---------------------------------------------------------------------*
*&      Form  Genera_listado_ALV
*&---------------------------------------------------------------------*

form Genera_Listado_ALV.

* Traslada Registros a Tabla de Salida

loop at xcvers into wa_xcvers.
  move-corresponding wa_xcvers to zcvers.
  append zcvers.
endloop.

*_ALV Almacenar el nombre del reporte

i_repid = sy-repid.

*_ALV Se crea el cat�logo de campos de la tabla interna

CALL FUNCTION 'REUSE_ALV_FIELDCATALOG_MERGE'
       EXPORTING
            I_PROGRAM_NAME         = i_repid
            I_INTERNAL_TABNAME     = 'ZCVERS'  "capital letters!
            I_INCLNAME             = i_repid
       CHANGING
            CT_FIELDCAT            = int_fcat
       EXCEPTIONS
            INCONSISTENT_INTERFACE = 1
            PROGRAM_ERROR          = 2
            OTHERS                 = 3.

IF SY-SUBRC <> 0.
    write: /
    'Returncode',
    sy-subrc,
    'from FUNCTION REUSE_ALV_FIELDCATALOG_MERGE'.
ENDIF.

*_ALV Actualizar el nombre de los campos

perform Actualiza_columnas_alv.

*_ALV Modifica el ordenamiento de los registros

*perform ordena_columnas_alv.

*_ALV Para Eventos

*perform list_display_events.


*_ALV Desplegar Lista

  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
       EXPORTING
            I_CALLBACK_PROGRAM = i_repid
*           I_CALLBACK_PF_STATUS_SET = 'SET_STATUS'
*           I_CALLBACK_USER_COMMAND = 'USER_COMMAND'
            IT_FIELDCAT        = int_fcat
*            IT_SORT            = it_sort
            I_SAVE             = 'A'
            IT_EVENTS = IT_EVENTS1
       TABLES
            T_OUTTAB           = zcvers
       EXCEPTIONS
            PROGRAM_ERROR      = 1
            OTHERS             = 2.
  IF SY-SUBRC <> 0.
    write: /
    'Returncode',
    sy-subrc,
    'from FUNCTION REUSE_ALV_GRID_DISPLAY'.
   ENDIF.
endform.



*&---------------------------------------------------------------------*
*&      Form  Genera_listado_ALV2
*&---------------------------------------------------------------------*

form Genera_Listado_ALV2.


*_ALV Almacenar el nombre del reporte

i_repid = sy-repid.

*_ALV Se crea el cat�logo de campos de la tabla interna

CALL FUNCTION 'REUSE_ALV_FIELDCATALOG_MERGE'
       EXPORTING
            I_PROGRAM_NAME         = i_repid
            I_INTERNAL_TABNAME     = 'ZUSTRX'  "capital letters!
            I_INCLNAME             = i_repid
       CHANGING
            CT_FIELDCAT            = int_fcat2
       EXCEPTIONS
            INCONSISTENT_INTERFACE = 1
            PROGRAM_ERROR          = 2
            OTHERS                 = 3.

IF SY-SUBRC <> 0.
    write: /
    'Returncode',
    sy-subrc,
    'from FUNCTION REUSE_ALV_FIELDCATALOG_MERGE'.
ENDIF.

*_ALV Actualizar el nombre de los campos

perform Actualiza_columnas_alv2.

*_ALV Modifica el ordenamiento de los registros

*perform ordena_columnas_alv.

*_ALV Para Eventos

*perform list_display_events.


*_ALV Desplegar Lista

  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
       EXPORTING
            I_CALLBACK_PROGRAM = i_repid
*           I_CALLBACK_PF_STATUS_SET = 'SET_STATUS'
*           I_CALLBACK_USER_COMMAND = 'USER_COMMAND'
            IT_FIELDCAT        = int_fcat2
*            IT_SORT            = it_sort
            I_SAVE             = 'A'
            IT_EVENTS = IT_EVENTS1
       TABLES
            T_OUTTAB           = zustrx
       EXCEPTIONS
            PROGRAM_ERROR      = 1
            OTHERS             = 2.
  IF SY-SUBRC <> 0.
    write: /
    'Returncode',
    sy-subrc,
    'from FUNCTION REUSE_ALV_GRID_DISPLAY'.
   ENDIF.
endform.



*&---------------------------------------------------------------------*
*&      Form  Genera_listado_ALV3
*&---------------------------------------------------------------------*

form Genera_Listado_ALV3.


*_ALV Almacenar el nombre del reporte

i_repid = sy-repid.

*_ALV Se crea el cat�logo de campos de la tabla interna

CALL FUNCTION 'REUSE_ALV_FIELDCATALOG_MERGE'
       EXPORTING
            I_PROGRAM_NAME         = i_repid
            I_INTERNAL_TABNAME     = 'ZUSAGE'  "capital letters!
            I_INCLNAME             = i_repid
       CHANGING
            CT_FIELDCAT            = int_fcat3
       EXCEPTIONS
            INCONSISTENT_INTERFACE = 1
            PROGRAM_ERROR          = 2
            OTHERS                 = 3.

IF SY-SUBRC <> 0.
    write: /
    'Returncode',
    sy-subrc,
    'from FUNCTION REUSE_ALV_FIELDCATALOG_MERGE'.
ENDIF.

*_ALV Actualizar el nombre de los campos

perform Actualiza_columnas_alv3.

*_ALV Modifica el ordenamiento de los registros

*perform ordena_columnas_alv.

*_ALV Para Eventos

*perform list_display_events.


*_ALV Desplegar Lista

  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
       EXPORTING
            I_CALLBACK_PROGRAM = i_repid
*           I_CALLBACK_PF_STATUS_SET = 'SET_STATUS'
*           I_CALLBACK_USER_COMMAND = 'USER_COMMAND'
            IT_FIELDCAT        = int_fcat3
*            IT_SORT            = it_sort
            I_SAVE             = 'A'
            IT_EVENTS = IT_EVENTS1
       TABLES
            T_OUTTAB           = zusage
       EXCEPTIONS
            PROGRAM_ERROR      = 1
            OTHERS             = 2.
  IF SY-SUBRC <> 0.
    write: /
    'Returncode',
    sy-subrc,
    'from FUNCTION REUSE_ALV_GRID_DISPLAY'.
   ENDIF.
endform.