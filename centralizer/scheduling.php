<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/centralizer/plantillas/sesion.php");
require_once($GLOBALS["app_root1"] . "/centralizer/procesos/scheduling.php");

$_SESSION["idioma"] = 2;
require_once($GLOBALS["app_root"] . "/plantillas/head.php");
require_once($GLOBALS['app_root1'] . "/centralizer/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno">
                <?php 
                $menuCentralizador = 2;
                include_once($GLOBALS["app_root1"] . "/centralizer/plantillas/menu.php");
                ?>
            </div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root1"] . "/centralizer/plantillas/scheduling.php"); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS["app_root"] . "/plantillas/pie.php");
?>