<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");

// Objetos
$usuario = new Clientes();
$empleado = new empleados();
// Inicializar error
$error = 0;

$_SESSION["idioma"] = 2;
if (isset($_POST['entrar']) && $_POST['entrar'] == 1) {
    if ($usuario->autenticarCentralizer($_POST['login'], $_POST['contrasena'])) {
        $nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/centralizer");
        $empleado->actUltAcceso($_SESSION["client_empleado"]);
        ?>
        <script>
            localStorage.licensingassuranceToken =  '<?= $nuevo_middleware->obtener_token(true) ?>';
            location.href = "agents.php";
        </script>
        <?php
    } else {
        header('location: index.php?idioma=' . $_SESSION["idioma"] . '&error=1');
    }
}
?>