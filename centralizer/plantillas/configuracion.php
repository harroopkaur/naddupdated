<?php
if ($actualizar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully updated', function() {
            location.href = '<?= $GLOBALS['domain_root1'] ?>/centralizer/configuration.php';
        });
    </script>
    <?php
} else if ($actualizar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to update record');
    </script>
    <?php
}
?>                                
                               
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Configuration data</span></legend>
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="actualizar" id="actualizar" value="1" />
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
             echo $centralizador->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="400" align="left" valign="top">Days that have passed to consider the inactive agent:</th>
                <td align="left"><input name="dias" id="dias" type="text" value="<?= $diasAgente["val"] ?>" size="30" maxlength="3" style="width:50px;" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_dias") ?></font></div></td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input name="actualizar" type="button" id="actualizar" value="UPDATE" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
       $("#dias").numeric(false);
    });
</script>