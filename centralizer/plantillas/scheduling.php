<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="deleteScheduling.php">
    <input type="hidden" id="id" name="id">
</form>

<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroScheduling" name="filtroScheduling" style="width:120px;" value="<?= $nombreScheduling ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroFechaCreacion" name="filtroFechaCreacion" style="width:120px;" value="<?= $fechaCreacion ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroFechaInicio" name="filtroFechaInicio" style="width:120px;" value="<?= $fechaInicio ?>"></th>
            <th colspan="2" align="center" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer">Search</div></th>
        </tr>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="midle" ><strong class="til">Scheduling</strong></th>
            <th  align="center" valign="midle" ><strong class="til">Creation Date</strong></th>
            <th  align="center" valign="midle" ><strong class="til">Start Date</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Update</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Delete</strong></th>
        </tr> 
    </thead>
    
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td  align="left"><?= $registro['tx_descrip'] ?></td>
                <td><?= $registro['fe_fecha_creacion'] ?></td>
                <td><?= $registro['fe_fecha_inicio'] ?></td>
                <td  align="center">
                    <a href="updateScheduling.php?id=<?= $registro['id'] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Update" title="Update" /></a>
                </td>
                <td align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Delete" title="Delete" /></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div id="paginador" style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>

<?php 
if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">There is no Scheduling</div>
<?php 
}
?>

<script>
    $(document).ready(function(){
        $("#filtroFechaCreacion").datepicker();
        $("#filtroFechaInicio").datepicker();
        
        $("#filtroEmpresa").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroScheduling").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
      
        $("#filtroFechaCreacion").click(function(){
            $("#filtroFechaCreacion").val("");
        });
        
        $("#filtroFechaInicio").click(function(){
            $("#filtroFechaInicio").val("");
        });
        
        $("#filtroEstado").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#buscar").click(function(){
            buscarData();
        }); 
    });
    
    function buscarData(){
        $.post("ajax/scheduling.php", { scheduling : $("#filtroScheduling").val(), 
        fechaCreacion : $("#filtroFechaCreacion").val(), fechaS : $("#filtroFechaInicio").val(), 
        estado : $("#filtroEstado").val(), pagina : 1, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root1'] ?>/centralizer/";
                return false;
            }
            $("#bodyTable").empty();
            $("#paginador").empty();
            $("#bodyTable").append(data[0].tabla);
            $("#paginador").append(data[0].paginador);
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, function() {
            });
        });
    }
    
    function eliminar(id){
        $.alert.open('confirm', 'You want to delete the record', {Yes: 'Yes', No: 'No'}, function(button) {
            if (button === 'Yes'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>