<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully updated', function() {
            location.href = '<?= $GLOBALS['domain_root1'] ?>/centralizer/scheduling.php';
        });
    </script>
    <?php
} else if ($agregar == 1 && $error > 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to update record');
    </script>
    <?php
}
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data" action="updateScheduling.php">
    <input type="hidden" name="insertar" id="insertar" value="1" />
    <input type="hidden" name="id" id="id" value="<?= $id ?>" />
    
    <table class="tablap2" style="width:500px; margin:0 auto; margin-top:20px;">
        <tr>
            <td width="150" align="left" valign="top"><input type="radio" name="tipoScheduling" id="SchedulingLATool" value="L" <?php if($datosScheduling["tx_tipo_scheduling"] == "L"){ echo "checked='checked'"; } ?>>LA Tool</td>
            <td width="150" align="left" valign="top"><input type="radio" name="tipoScheduling" id="SchedulingMetering" value="M" <?php if($datosScheduling["tx_tipo_scheduling"] == "M"){ echo "checked='checked'"; } ?>>Metering - 
                <input type="checkbox" name="activarScheduling" id="activarScheduling" value="2" <?php if($datosScheduling["tx_tipo_scheduling"] == "L"){ echo "disabled"; } ?>
                 <?php if($datosScheduling["tiny_status"] == 2){ echo "checked='checked'"; } ?>>Activar
            </td>
        </tr>
        <tr>
            <th width="150" align="left" valign="top">Task Name:</th>
            <td align="left"><input name="nombreTarea" id="nombreTarea" type="text" value="<?= $datosScheduling["tx_descrip"] ?>" size="30" maxlength="250" />
            </td>
        </tr>
    </table>
    
    <br>
    <div class="contentSchedule">
        <fieldset class="fieldsetSchedule">
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Schedule</span></legend>
            <input type="radio" name="schedule" id="scheduleDiario" value="Daily" <?php if($datosScheduling["tx_schedule"] == "Daily") echo "checked"; ?>>Daily<br>
            <input type="radio" name="schedule" id="scheduleSemanal" value="Weekly" <?php if($datosScheduling["tx_schedule"] == "Weekly") echo "checked"; ?>>Weekly<br>          
            <input type="radio" name="schedule" id="scheduleMensual" value="Monthly" <?php if($datosScheduling["tx_schedule"] == "Monthly") echo "checked"; ?>>Monthly<br>          
        </fieldset>

        <fieldset class="fieldsetInicio">
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Start</span></legend>
            <label>Start day</label><input type="text" name="fechaInicio" id="fechaInicio" readonly value="<?= $fecha ?>"><br>
            <label>Start time</label><input type="time" name="horaInicio" id="horaInicio" value="<?= $hora ?>">         
        </fieldset>
    </div>
   
    <fieldset class="fieldsetDaily <?php if($datosScheduling["tx_schedule"] != "Daily") echo "hide"; ?>" id="horarioDia">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Daily</span></legend>
        <span>
            Repeat each:
        </span>
        <input type="text" name="repetirDia" id="repetirDia" style="width:50px;" value="<?= $datosScheduling["int_repeat_days"] ?>">
    </fieldset>
    
    <fieldset class="fieldsetWeekly <?php if($datosScheduling["tx_schedule"] != "Weekly") echo "hide"; ?>" id="horarioSemana">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Weekly</span></legend>
        <div>
            <span>
                Repeat each:
            </span>
            <input type="text" name="repetirSemana" id="repetirSemana" style="width:50px;" value="<?= $datosScheduling["int_repeat_weeks"] ?>">

            <span>
                Week in:
            </span>
        </div>
        
        <span class="cont-monthly-label">
            Day:
        </span>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="dia[]" id="dia1" value="Sunday" <?php if(strpos($datosScheduling["tx_days"], 'Sunday') !== false){ echo "checked"; } ?>>Sunday<br><br>
            <input type="checkbox" name="dia[]" id="dia2" value="Monday" <?php if(strpos($datosScheduling["tx_days"], 'Monday') !== false){ echo "checked"; } ?>>Monday<br><br>
            <input type="checkbox" name="dia[]" id="dia3" value="Tuesday" <?php if(strpos($datosScheduling["tx_days"], 'Tuesday') !== false){ echo "checked"; } ?>>Tuesday<br><br>      
            <input type="checkbox" name="dia[]" id="dia4" value="Wednesday" <?php if(strpos($datosScheduling["tx_days"], 'Wednesday') !== false){ echo "checked"; } ?>>Wednesday
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="dia[]" id="dia5" value="Thursday" <?php if(strpos($datosScheduling["tx_days"], 'Thursday') !== false){ echo "checked"; } ?>>Thursday<br><br>
            <input type="checkbox" name="dia[]" id="dia6" value="Friday" <?php if(strpos($datosScheduling["tx_days"], 'Friday') !== false){ echo "checked"; } ?>>Friday<br><br>
            <input type="checkbox" name="dia[]" id="dia7" value="Saturday" <?php if(strpos($datosScheduling["tx_days"], 'Saturday') !== false){ echo "checked"; } ?>>Saturday
        </div>
    </fieldset>
    
    <fieldset class="fieldsetMonthly <?php if($datosScheduling["tx_schedule"] != "Monthly") echo "hide"; ?>" id="horarioMes">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Monthly</span></legend>
        <span class="cont-monthly-label">
            Months:
        </span>
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month1" value="January" <?php if(strpos($datosScheduling["tx_months"], 'January') !== false){ echo "checked"; } ?>>January<br><br>
            <input type="checkbox" name="month[]" id="month2" value="February" <?php if(strpos($datosScheduling["tx_months"], 'February') !== false){ echo "checked"; } ?>>February<br><br>
            <input type="checkbox" name="month[]" id="month3" value="March" <?php if(strpos($datosScheduling["tx_months"], 'March') !== false){ echo "checked"; } ?>>March<br><br>      
            <input type="checkbox" name="month[]" id="month4" value="April" <?php if(strpos($datosScheduling["tx_months"], 'April') !== false){ echo "checked"; } ?>>April
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month5" value="May" <?php if(strpos($datosScheduling["tx_months"], 'May') !== false){ echo "checked"; } ?>>May<br><br>
            <input type="checkbox" name="month[]" id="month6" value="June" <?php if(strpos($datosScheduling["tx_months"], 'June') !== false){ echo "checked"; } ?>>June<br><br>
            <input type="checkbox" name="month[]" id="month7" value="July" <?php if(strpos($datosScheduling["tx_months"], 'July') !== false){ echo "checked"; } ?>>July<br><br>    
            <input type="checkbox" name="month[]" id="month8" value="August" <?php if(strpos($datosScheduling["tx_months"], 'August') !== false){ echo "checked"; } ?>>August
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month9" value="September" <?php if(strpos($datosScheduling["tx_months"], 'September') !== false){ echo "checked"; } ?>>September<br><br>
            <input type="checkbox" name="month[]" id="month10" value="October" <?php if(strpos($datosScheduling["tx_months"], 'October') !== false){ echo "checked"; } ?>>October<br><br>
            <input type="checkbox" name="month[]" id="month11" value="November" <?php if(strpos($datosScheduling["tx_months"], 'November') !== false){ echo "checked"; } ?>>November<br><br>       
            <input type="checkbox" name="month[]" id="month12" value="December" <?php if(strpos($datosScheduling["tx_months"], 'December') !== false){ echo "checked"; } ?>>December
        </div>
       
        <br style="clear:both;"><br>
        <span class="cont-monthly-label">
            Day:
        </span>
        
        <?= $listaCheck ?>
    </fieldset>
    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <td colspan="2" align="center"><input name="crear" type="button" id="crear" value="UPDATE" onclick="validar();" class="boton" /></td>
        </tr>
    </table>
 </form>
    
<script>
    $(document).ready(function(){   
        $("#repetirDia").numeric(false);
        $("#repetirSemana").numeric(false);
        $("#fechaInicio").datepicker();
        
        $("#crear").click(function(){
            if (validar() === false){
                return false;
            }
            
            $("#form1").submit();
        });
        
        $("#fechaInicio").click(function(){
            $("#fechaInicio").val("");
        });
        
        $("#scheduleDiario").click(function(){
            ocultarCampos();
            $("#horarioDia").show();
        });
        
        $("#scheduleSemanal").click(function(){
            ocultarCampos();
            $("#horarioSemana").show();
        });
        
        $("#scheduleMensual").click(function(){
            ocultarCampos();
            $("#horarioMes").show();
        });
        
        $("#SchedulingLATool").click(function(){
            $("#activarScheduling").prop("disabled", true);
            $("#activarScheduling").prop("checked", false);
        });
        
        $("#SchedulingMetering").click(function(){
            $("#activarScheduling").prop("disabled", false);
            $("#activarScheduling").prop("checked", true);
        });
    });
    
    function ocultarCampos(){
        $("#horarioDia").hide();
        $("#horarioSemana").hide();
        $("#horarioMes").hide();
        reiniciarCampos();
    }
    
    function reiniciarCampos(){
        $("#repetirDia").val("");
        $("#repetirSemana").val("");
        
        for(index = 1; index < 8; index++){
            $("#dia" + index).prop("checked", false);
        }
        
        for(index = 1; index < 13; index++){
            $("#month" + index).prop("checked", false);
        }
        
        for(index = 1; index < 32; index++){
            $("#day" + index).prop("checked", false);
        }
    }
    
    
    function validar(){
        result = true;
        
        if ($("#nombreTarea").val() === ""){
            $.alert.open('warning', "You must fill the name of the task", function(){
                $("#nombreTarea").focus();
            });
            return false;
        }
        
        if ($("#fechaInicio").val() === ""){
            $.alert.open('warning', "You must fill the start date", function(){
                $("#fechaInicio").focus();
            });
            return false;
        }
        
        if ($("#horaInicio").val() === ""){
            $.alert.open('warning', "You must fill the start time", function(){
                $("#horaInicio").focus();
            });
            return false;
        }
        
        if ($("#scheduleDiario").prop("checked") && $("#repetirDia").val() === ""){
            $.alert.open('warning', "Must fill every how much the task is repeated", function(){
                $("#repetirDia").focus();
            });
            return false;
        } else if ($("#scheduleSemanal").prop("checked")){
            if ($("#repetirSemana").val() === ""){
                $.alert.open('warning', "Must fill every how much the task is repeated", function(){
                    $("#repetirSemana").focus();
                });
                return false;
            } 
            
            check = 0;
            for(index = 1; index < 8; index++){
                if($("#dia" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "You must select at least day of the week");
                return false;
            }
        } else if($("#scheduleMensual").prop("checked")){
            check = 0;
            for(index = 1; index < 13; index++){
                if($("#month" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "You must select at least one month");
                return false;
            }
            
            check = 0;
            for(index = 1; index < 32; index++){
                if($("#day" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "You must select at least one day of the month");
                return false;
            }
        }
        
        return true;
    }
</script>