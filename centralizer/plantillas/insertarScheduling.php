<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully inserted', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/centralizer/scheduling.php';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $error > 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to add record');
    </script>
    <?php
}
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data" action="newScheduling.php">
    <input type="hidden" name="insertar" id="insertar" value="1" />
    
    <table class="tablap2" style="width:500px; margin:0 auto; margin-top:20px;">
        <tr>
            <td width="150" align="left" valign="top"><input type="radio" name="tipoScheduling" id="SchedulingLATool" value="L" checked>LA Tool</td>
            <td width="150" align="left" valign="top"><input type="radio" name="tipoScheduling" id="SchedulingMetering" value="M" >Metering - 
                <input type="checkbox" name="activarScheduling" id="activarScheduling" value="2" disabled>Activar
            </td>
        </tr>
        <tr>
            <th width="150" align="left" valign="top">Task Name:</th>
            <td align="left"><input name="nombreTarea" id="nombreTarea" type="text" value="" size="30" maxlength="250" />
            </td>
        </tr>
    </table>
    
    <br>
    <div class="contentSchedule">
        <fieldset class="fieldsetSchedule">
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Schedule</span></legend>
            <input type="radio" name="schedule" id="scheduleDiario" value="Daily" checked>Daily<br>
            <input type="radio" name="schedule" id="scheduleSemanal" value="Weekly">Weekly<br>          
            <input type="radio" name="schedule" id="scheduleMensual" value="Monthly">Monthly<br>          
        </fieldset>

        <fieldset class="fieldsetInicio">
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Start</span></legend>
            <label>Start day</label><input type="text" name="fechaInicio" id="fechaInicio" readonly><br>
            <label>Start time</label><input type="time" name="horaInicio" id="horaInicio" value="">         
        </fieldset>
    </div>
   
    <fieldset class="fieldsetDaily" id="horarioDia">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Daily</span></legend>
        <span>
            Repeat each:
        </span>
        <input type="text" name="repetirDia" id="repetirDia" style="width:50px;">
    </fieldset>
    
    <fieldset class="fieldsetWeekly hide" id="horarioSemana">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Weekly</span></legend>
        <div>
            <span>
                Repeat each:
            </span>
            <input type="text" name="repetirSemana" id="repetirSemana" style="width:50px;">

            <span>
                Week in:
            </span>
        </div>
        
        <span class="cont-monthly-label">
            Day:
        </span>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="dia[]" id="dia1" value="Sunday">Sunday<br><br>
            <input type="checkbox" name="dia[]" id="dia2" value="Monday">Monday<br><br>
            <input type="checkbox" name="dia[]" id="dia3" value="Tuesday">Tuesday<br><br>      
            <input type="checkbox" name="dia[]" id="dia4" value="Wednesday">Wednesday
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="dia[]" id="dia5" value="Thursday">Thursday<br><br>
            <input type="checkbox" name="dia[]" id="dia6" value="Friday">Friday<br><br>
            <input type="checkbox" name="dia[]" id="dia7" value="Saturday">Saturday
        </div>
    </fieldset>
    
    <fieldset class="fieldsetMonthly hide" id="horarioMes">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Monthly</span></legend>
        <span class="cont-monthly-label">
            Months:
        </span>
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month1" value="January">January<br><br>
            <input type="checkbox" name="month[]" id="month2" value="February">February<br><br>
            <input type="checkbox" name="month[]" id="month3" value="March">March<br><br>      
            <input type="checkbox" name="month[]" id="month4" value="April">April
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month5" value="May">May<br><br>
            <input type="checkbox" name="month[]" id="month6" value="June">June<br><br>
            <input type="checkbox" name="month[]" id="month7" value="July">July<br><br>    
            <input type="checkbox" name="month[]" id="month8" value="August">August
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month9" value="September">September<br><br>
            <input type="checkbox" name="month[]" id="month10" value="October">October<br><br>
            <input type="checkbox" name="month[]" id="month11" value="November">November<br><br>       
            <input type="checkbox" name="month[]" id="month12" value="December">December
        </div>
       
        <br style="clear:both;"><br>
        <span class="cont-monthly-label">
            Día:
        </span>
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="day[]" id="day1" value="1">01&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day6" value="6">06&nbsp;&nbsp;         
            <input type="checkbox" name="day[]" id="day11" value="11">11&nbsp;&nbsp;  
            <input type="checkbox" name="day[]" id="day16" value="16">16&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day21" value="21">21&nbsp;&nbsp;          
            <input type="checkbox" name="day[]" id="day26" value="26">26&nbsp;&nbsp; 
            <input type="checkbox" name="day[]" id="day31" value="31">31<br><br> 
            
            <input type="checkbox" name="day[]" id="day2" value="2">02&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day7" value="7">07&nbsp;&nbsp;        
            <input type="checkbox" name="day[]" id="day12" value="12">12&nbsp;&nbsp; 
            <input type="checkbox" name="day[]" id="day17" value="17">17&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day22" value="22">22&nbsp;&nbsp;         
            <input type="checkbox" name="day[]" id="day27" value="27">27<br><br> 
            
            <input type="checkbox" name="day[]" id="day3" value="3">03&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day8" value="8">08&nbsp;&nbsp;        
            <input type="checkbox" name="day[]" id="day13" value="13">13&nbsp;&nbsp; 
            <input type="checkbox" name="day[]" id="day18" value="18">18&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day23" value="23">23&nbsp;&nbsp;         
            <input type="checkbox" name="day[]" id="day28" value="28">28<br><br> 
            
            <input type="checkbox" name="day[]" id="day4" value="4">04&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day9" value="9">09&nbsp;&nbsp;        
            <input type="checkbox" name="day[]" id="day14" value="14">14&nbsp;&nbsp; 
            <input type="checkbox" name="day[]" id="day19" value="19">19&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day24" value="24">24&nbsp;&nbsp;         
            <input type="checkbox" name="day[]" id="day29" value="29">29<br><br> 
            
            <input type="checkbox" name="day[]" id="day5" value="5">05&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day10" value="10">10&nbsp;&nbsp;        
            <input type="checkbox" name="day[]" id="day15" value="15">15&nbsp;&nbsp; 
            <input type="checkbox" name="day[]" id="day20" value="20">20&nbsp;&nbsp;
            <input type="checkbox" name="day[]" id="day25" value="25">25&nbsp;&nbsp;         
            <input type="checkbox" name="day[]" id="day30" value="30">30<br>  
        </div>
    </fieldset>
    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <td colspan="2" align="center"><input name="crear" type="button" id="crear" value="NEW" onclick="validar();" class="boton" /></td>
        </tr>
    </table>
 </form>
    
<script>
    $(document).ready(function(){   
        $("#repetirDia").numeric(false);
        $("#repetirSemana").numeric(false);
        $("#fechaInicio").datepicker();
        
        $("#crear").click(function(){
            if (validar() === false){
                return false;
            }
            
            $("#form1").submit();
        });
        
        $("#fechaInicio").click(function(){
            $("#fechaInicio").val("");
        });
        
        $("#scheduleDiario").click(function(){
            ocultarCampos();
            $("#horarioDia").show();
        });
        
        $("#scheduleSemanal").click(function(){
            ocultarCampos();
            $("#horarioSemana").show();
        });
        
        $("#scheduleMensual").click(function(){
            ocultarCampos();
            $("#horarioMes").show();
        });
        
        $("#SchedulingLATool").click(function(){
            $("#activarScheduling").prop("disabled", true);
            $("#activarScheduling").prop("checked", false);
        });
        
        $("#SchedulingMetering").click(function(){
            $("#activarScheduling").prop("disabled", false);
            $("#activarScheduling").prop("checked", true);
        });
    });
    
    function ocultarCampos(){
        $("#horarioDia").hide();
        $("#horarioSemana").hide();
        $("#horarioMes").hide();
        reiniciarCampos();
    }
    
    function reiniciarCampos(){
        $("#repetirDia").val("");
        $("#repetirSemana").val("");
        
        for(index = 1; index < 8; index++){
            $("#dia" + index).prop("checked", false);
        }
        
        for(index = 1; index < 13; index++){
            $("#month" + index).prop("checked", false);
        }
        
        for(index = 1; index < 32; index++){
            $("#day" + index).prop("checked", false);
        }
    }
    
    
    function validar(){
        result = true;
        
        if ($("#nombreTarea").val() === ""){
            $.alert.open('warning', "You must fill the name of the task", function(){
                $("#nombreTarea").focus();
            });
            return false;
        }
        
        if ($("#fechaInicio").val() === ""){
            $.alert.open('warning', "You must fill the start date", function(){
                $("#fechaInicio").focus();
            });
            return false;
        }
        
        if ($("#horaInicio").val() === ""){
            $.alert.open('warning', "You must fill the start time", function(){
                $("#horaInicio").focus();
            });
            return false;
        }
        
        if ($("#scheduleDiario").prop("checked") && $("#repetirDia").val() === ""){
            $.alert.open('warning', "Must fill every how much the task is repeated", function(){
                $("#repetirDia").focus();
            });
            return false;
        } else if ($("#scheduleSemanal").prop("checked")){
            if ($("#repetirSemana").val() === ""){
                $.alert.open('warning', "Must fill every how much the task is repeated", function(){
                    $("#repetirSemana").focus();
                });
                return false;
            } 
            
            check = 0;
            for(index = 1; index < 8; index++){
                if($("#dia" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "You must select at least day of the week");
                return false;
            }
        } else if($("#scheduleMensual").prop("checked")){
            check = 0;
            for(index = 1; index < 13; index++){
                if($("#month" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "You must select at least one month");
                return false;
            }
            
            check = 0;
            for(index = 1; index < 32; index++){
                if($("#day" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "You must select at least one day of the month");
                return false;
            }
        }
        
        return true;
    }
</script>