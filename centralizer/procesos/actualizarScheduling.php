<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/centralizer");
include_once($GLOBALS["app_root1"] . "/centralizer/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_centralizador_web.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");

// Objetos
$centralizador = new clase_centralizador_web();
$clientes = new Clientes();
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$id = 0;
if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $id = $_GET["id"];
}


if (isset($_POST['insertar'])) {
    $agregar = 1;
    // Validaciones
    
    $id = 0;
    if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
        $id = $_POST["id"];
    }

    $empresa = 0;
    if(isset($_POST["empresa"]) && filter_var($_POST["empresa"], FILTER_VALIDATE_INT) !== false){
        $empresa = $_POST["empresa"];
    }
    
    $nombreTarea = "";
    if(isset($_POST["nombreTarea"])){
        $nombreTarea = $general->get_escape($_POST["nombreTarea"]);
    }
    
    $schedule = "";
    if(isset($_POST["schedule"])){
        $schedule = $general->get_escape($_POST["schedule"]);
    }
    
    $fechaInicio = "";
    if(isset($_POST["fechaInicio"])){
        $fechaInicio = $general->reordenarFecha($_POST["fechaInicio"], "/", "-", "mm/dd/YYYY");
    }
    
    $horaInicio = "";
    if(isset($_POST["horaInicio"])){
        $horaInicio = $general->get_escape($_POST["horaInicio"]);
    }
    
    $repetirDia = 0;
    if(isset($_POST["repetirDia"]) && filter_var($_POST["repetirDia"], FILTER_VALIDATE_INT) !== false){
        $repetirDia = $_POST["repetirDia"];
    }
    
    $repetirSemana = 0;
    if(isset($_POST["repetirSemana"]) && filter_var($_POST["repetirSemana"], FILTER_VALIDATE_INT) !== false){
        $repetirSemana = $_POST["repetirSemana"];
    }
    
    $dia = array();
    if(isset($_POST["dia"])){
        $dia = $_POST["dia"];
    }
    $dias = "";
    foreach($dia as $row){
        if($dias != ""){
            $dias .= ",";
        }
        
        $dias .= $row;
    }
    
    $month = array();
    if(isset($_POST["month"])){
        $month = $_POST["month"];
    }
    $months = "";
    foreach($month as $row){
        if($months != ""){
            $months .= ",";
        }
        
        $months .= $row;
    }
    
    $day = array();
    if(isset($_POST["day"])){
        $day = $_POST["day"];
    }
    $days = "";
    foreach($day as $row){
        if($days != ""){
            $days .= ",";
        }
        
        $days .= $row;
    }
    
    if($nombreTarea == ""){
        $error = 1;
    } else if($fechaInicio == ""){
        $error = 2;
    } else if($horaInicio == ""){
        $error = 3;
    } else if($schedule == "Daily" && $repetirDia == ""){
        $error = 4;
    } else if($schedule == "Weekly"){
        if($repetirSemana == ""){
            $error = 5;
        } 
        
        if($dias == ""){
            $error = 6;
        }
    } else if($schedule == "Monthly"){
        if($months == ""){
            $error = 7;
        }
        
        if($days == ""){
            $error = 8;
        }
    }
    
    $tipoScheduling = $general->get_escape($_POST["tipoScheduling"]);
    
    $status = 1;
    if(isset($_POST["activarScheduling"])){
        $status = 2;
    }
    
    if ($error == 0 && $centralizador->updateConfigScheduling($id, $_SESSION["client_id"], $nombreTarea, $fechaInicio . " " . $horaInicio, 
    $schedule, $repetirDia, $repetirSemana, $dias, $months, $days, $tipoScheduling, $status)){
        $exito = 1;
    } else {
        $error = 9;
    }
}

//$empresas = $clientes->listar_todo();
$datosScheduling = $centralizador->infoScheduling($id);

$fechaArray = explode(" ", $datosScheduling["fe_fecha_inicio"]);
$fecha = $general->muestrafechaIngles($fechaArray[0]);
$hora = $fechaArray[1];
$num_days = explode(",", $datosScheduling["num_days"]);

$inicioBloque = '<div class="float-lt" style="margin-left:20px; padding:10px;">';
$finBloque = '</div>';
$listaCheck = "";

$checkDay = array();
for($index = 1; $index < 32; $index++){
    $checkDay[$index] = false;
    foreach($num_days as $row){
        if($index == $row){
            $checkDay[$index] = true;
        }
    }
}

for($index = 1; $index < 32; $index++){
    if(($index < 6 && $index == 1) || ($index < 11 && $index == 6) || ($index < 16 && $index == 11)
    || ($index < 21 && $index == 16) || ($index < 26 && $index == 21) || ($index < 31 && $index == 26) 
    || $index == 31){
        $listaCheck .= $inicioBloque;
    }

    $listaCheck .= '<input type="checkbox" name="day[]" id="day' . $index . '" value="' . $index . '" ';
    
    if($checkDay[$index] === true){
        $listaCheck .= 'checked';
    }
    $listaCheck .= '>';
    
    if($index < 10){
        $listaCheck .= "0"; 
    } 
    $listaCheck .= $index . '<br><br>'; 

            
    if(($index < 6 && $index == 5) || ($index < 11 && $index == 10) || ($index < 16 && $index == 15) 
    || ($index < 21 && $index == 20) || ($index < 26 && $index == 25) || ($index < 31 && $index == 30) 
    || $index == 31){
        $listaCheck .= $finBloque;
    }
}