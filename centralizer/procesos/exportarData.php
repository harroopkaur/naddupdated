<?php
ini_set('memory_limit', '-1');
//inicio middleware
/*require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");*/
//fin middleware

require_once($GLOBALS["app_root1"] . "/vendor/autoload.php");

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_centralizador_web.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$clientes = new Clientes();
$centralizador = new clase_centralizador_web();
$validator = new validator("form1");
$general = new General();

//procesos
$cliente = 0;
$error = 0;
$exito = 0;
$exportar = 0;

if (isset($_POST['exportar']) /*&& isset($_POST["cliente"]) && filter_var($_POST["cliente"], FILTER_VALIDATE_INT) !== false*/) {
    $cliente = $_SESSION["client_id"]; //$_POST["cliente"];
    // Validaciones
    $exportar = 1;
    
    $fecha = date("d-m-y");
    $fechaZIP = date("d-m-Y-H_i");
    $cabeceraLicencing = "Archivo Generado el: " . $fecha . " - Licensing Assurance LLC - info@licensingassurance.com";
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/add_remove.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/desinstalaciones.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/llaves.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/procesadores.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/procesos.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/resultados_escaneo.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/seguridad.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/seriales_maquina.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/servicios.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/sistema_operativo.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/sql.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/tipo_equipo.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/usabilidad_software.php");
    require_once($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/usuario_equipo.php");
    
    $password = 'InfoLA2015**';
    $fileName = 'LAD_Output[' . $fechaZIP . '].zip';
    $outfile = $GLOBALS["app_root"] . '/adminweb/centralizador/reportes/clientes/' . $cliente . '/' . $fileName;
    
    $encryptionMethod = \PhpZip\ZipFile::ENCRYPTION_METHOD_WINZIP_AES_256;
    $zipFile = new \PhpZip\ZipFile();
    
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Addremove.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Addremove.csv", "Consolidado Addremove.csv"); // add an entry from the file
    }
    
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Desinstalaciones.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Desinstalaciones.csv", "Consolidado Desinstalaciones.csv");
    }
    
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Llaves.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Llaves.csv", "Consolidado Llaves.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Procesadores.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Procesadores.csv", "Consolidado Procesadores.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Procesos.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Procesos.csv", "Consolidado Procesos.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Resultados_Escaneo.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Resultados_Escaneo.csv", "Resultados_Escaneo.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_seguridad.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_seguridad.csv", "Consolidado_seguridad.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_seriales_maquina.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_seriales_maquina.csv", "Consolidado_seriales_maquina.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_servicios.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_servicios.csv", "Consolidado_servicios.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Sistema Operativo.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Sistema Operativo.csv", "Consolidado Sistema Operativo.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado SQL.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado SQL.csv", "Consolidado SQL.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Tipo de Equipo.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Tipo de Equipo.csv", "Consolidado Tipo de Equipo.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_usabilidad_software.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_usabilidad_software.csv", "Consolidado_usabilidad_software.csv");
    }
        
    if(file_exists($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Usuario-Equipo.csv")){
        $zipFile->addFile($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Usuario-Equipo.csv", "Consolidado Usuario-Equipo.csv");
    }
        //header()
    
    $zipFile->setPassword($password, $encryptionMethod)
            ->saveAsFile($outfile) // save the archive to a file
            ->close(); // close archive
            
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Addremove.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Desinstalaciones.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Llaves.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Procesadores.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Procesos.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Resultados_Escaneo.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_seguridad.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_seriales_maquina.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_servicios.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Sistema Operativo.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado SQL.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Tipo de Equipo.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado_usabilidad_software.csv");
    @unlink($GLOBALS["app_root"] . "/adminweb/centralizador/reportes/clientes/" . $cliente . "/Consolidado Usuario-Equipo.csv");

    header("Content-type: application/octet-stream");   
    header("Content-disposition: attachment; filename=$fileName"); 
    header ("Content-Length: ".filesize($outfile));
    readfile($outfile);    
    @unlink($outfile);
}

//$listadoCliente = $clientes->listar_todo();

$validator->create_message("msj_cliente", "cliente", " Obligatorio", 0);