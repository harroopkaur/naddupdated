<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_centralizador_web.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/centralizer");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general = new General();
    
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $centralizador = new clase_centralizador_web();

            $nombreScheduling = "";
            if(isset($_POST["scheduling"])){
                $nombreScheduling = $general->get_escape($_POST["scheduling"]);
            }

            $fechaCreacion = "";
            if(isset($_POST["fechaCreacion"]) && $_POST["fechaCreacion"] != ""){
                $fechaCreacion = $general->reordenarFecha($_POST["fechaCreacion"], "/", "-", "mm/dd/YYYY"); 
            }
            
            $fechaInicio = "";
            if(isset($_POST["fechaS"]) && $_POST["fechaS"] != ""){
                $fechaInicio = $general->reordenarFecha($_POST["fechaS"], "/", "-", "mm/dd/YYYY"); 
            }
            
            $estadoSche = "";
            if(isset($_POST["estado"])){
                $estadoSche = $general->get_escape($_POST["estado"]);
            }

            $start_record = 1;
            if(isset($_POST["pagina"]) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false){
                $start_record = $_POST["pagina"];
            }

            $listado = $centralizador->listaSchedulingClient($_SESSION["client_id"], $nombreScheduling, $fechaCreacion, $fechaInicio, $estadoSche, $start_record);
            $count   = $centralizador->totalSchedulingClient($_SESSION["client_id"], $nombreScheduling, $fechaCreacion, $fechaInicio, $estadoSche);
            $condiciones = '&Sche=' . $nombreScheduling . '&fec=' . $fechaCreacion . '&fecS=' . $fechaInicio . '&est=' . $estadoSche;
            
            $pag = new paginator($count, $general->limit_paginacion, 'scheduling.php?', $condiciones);
            $i = $pag->get_total_pages();

            $tabla = "";
            foreach ($listado as $registro) {
                $tabla .= '<tr onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                    <td  align="left">' . $registro['tx_descrip'] . '</td>
                    <td>' . $registro['fe_fecha_creacion'] . '</td>
                    <td>' . $registro['fe_fecha_inicio'] . '</td>
                    <td  align="center">
                        <a href="updateScheduling.php?id=' . $registro['id'] . '"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                    </td>
                    <td align="center">
                        <a href="#" onclick="eliminar(' . $registro['id'] . ')"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                    </td>
                </tr>';
            }

            $paginador = $pag->print_paginatorAjax("");
            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$paginador, 'resultado'=>true));
        }  
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);