<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_centralizador_web.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/centralizer");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general = new General();
    
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $centralizador = new clase_centralizador_web();

            $empresa = "";
            if(isset($_POST["empresa"])){
                $empresa = $general->get_escape($_POST["empresa"]);
            }

            $agente = "";
            if(isset($_POST["agente"])){
                $agente = $general->get_escape($_POST["agente"]);
            }

            $hostname = "";
            if(isset($_POST["hostname"])){
                $hostname = $general->get_escape($_POST["hostname"]);
            }

            $IP = "";
            if(isset($_POST["IP"])){
                $IP = $general->get_escape($_POST["IP"]);
            }

            $lastDataSent = "";
            if(isset($_POST["lastDataSent"])){
                $lastDataSent = $general->get_escape($_POST["lastDataSent"]);
            }

            $status = "";
            if(isset($_POST["status"])){
                $status = $general->get_escape($_POST["status"]);
            }

            $start_record = 1;
            if(isset($_POST["pagina"]) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false){
                $start_record = $_POST["pagina"];
            }

            $listado = $centralizador->dataCentralizadorCliente($_SESSION["client_id"], $empresa, $agente, $hostname, $IP, $lastDataSent, $status, $start_record);
            $count   = $centralizador->totalCliente($_SESSION["client_id"], $empresa, $agente, $hostname, $IP, $lastDataSent, $status);
            $condiciones = '&emp=' . $empresa . '&age=' . $agente . '&host=' . $hostname . '&IP=' . $IP .
            '&dataSent=' . $lastDataSent . '&status=' . $status;

            $pag = new paginator($count, $general->limit_paginacion, 'index.php?', $condiciones);
            $i = $pag->get_total_pages();

            $tabla = "";
            foreach ($listado as $registro) {
                $tabla .= '<tr onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                    <td align="left">' . $registro['empresa'] . '</td>
                    <td  align="left">' . $registro['id'] . '</td>
                    <td>' . $registro['tx_host_name'] . '</td>
                    <td>' . $registro['tx_ip'] . '</td>
                    <td  align="center">' . $general->muestrafecha($registro['fe_ejeccn']) . '</td>
                    <td>' . $registro['estado'] . '</td>
                    <td align="center">
                        <a href="#" onclick="eliminar(' . $registro['id'] . ')"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                    </td>
                </tr>';
            }

            $paginador = $pag->print_paginatorAjax("");
            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$paginador, 'resultado'=>true));
        }  
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);