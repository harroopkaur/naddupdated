
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    //$.backstretch("assets/img/backgrounds/1.jpg");
    
    /*
	    Contact form
	*/
	$('.contact-form form input[type="text"], .contact-form form textarea').on('focus', function() {
		$('.contact-form form input[type="text"], .contact-form form textarea').removeClass('input-error');
	});
	$('.contact-form form').submit(function(e) {
		e.preventDefault();
	    $('.contact-form form input[type="text"], .contact-form form textarea').removeClass('input-error');
	    var postdata = $('.contact-form form').serialize();
	    $.ajax({
	        type: 'POST',
	        url: $("#domain_root").val() + '/assets/contact.php',
	        data: postdata,
	        dataType: 'json',
	        success: function(json) {
                
                    if(json.nameMessage != '') {
	                $('#contact-name').addClass('input-error');
	            }
	            if(json.emailMessage != '') {
	                //$('.contact-form form .contact-email').addClass('input-error');
                        $('#contact-email').addClass('input-error');
	            }
                    if(json.phoneMessage != '') {
	                $('#contact-phone').addClass('input-error');
	            }
                    if(json.countryMessage != '') {
	                $('#contact-country').addClass('input-error');
	            }
                    if(json.companyMessage != '') {
	                $('#contact-company').addClass('input-error');
	            }
	            if(json.subjectMessage != '') {
	                //$('.contact-form form .contact-subject').addClass('input-error');
                        $('#contact-subject').addClass('input-error');
	            }
	            if(json.messageMessage != '') {
	                //$('.contact-form form textarea').addClass('input-error');
                        $('#contact-message').addClass('input-error');
	            }
	            if(json.antispamMessage != '') {
	                //$('.contact-form form .contact-antispam').addClass('input-error');
                        $('#contact-antispam').addClass('input-error');
	            }
	            if(json.nameMessage == '' && json.emailMessage == '' && json.phoneMessage == '' && json.countryMessage == '' 
                    && json.companyMessage == '' && json.subjectMessage == '' && json.messageMessage == '' && json.antispamMessage == '') {
	                $('.contact-form form').fadeOut('fast', function() {
	                    $('.contact-form').append($("#mensaje").val());
	                    // reload background
                            //$.backstretch("resize");
	                });
	            }
	        }
	    });
	});
    
    
});
