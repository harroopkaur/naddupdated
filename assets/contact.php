<?php
// Email address verification
function isEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

if($_POST) {
    //print_r($_POST);
    // Enter the email where you want to receive the message
    $emailTo = 'info@licensingassurance.com';

    $name = addslashes(trim($_POST['name']));
    $phone = addslashes(trim($_POST['phone']));
    $clientEmail = addslashes(trim($_POST['email']));
    $country = addslashes(trim($_POST['country']));
    $company = addslashes(trim($_POST['company']));
    $subject = addslashes(trim($_POST['subject']));
    $message = addslashes(trim($_POST['message']));
    $antispam = addslashes(trim($_POST['antispam']));

    $array = array('nameMessage' => '', 'phoneMessage' => '', 'emailMessage' => '', 'countryMessage' => '', 'companyMessage' => '', 'subjectMessage' => '', 'messageMessage' => '', 'antispamMessage' => '');

    if($name == '') {
        $array['nameMessage'] = 'Empty Name!';
    }
    if($clientEmail == '') {
        $array['emailMessage'] = 'Empty email!';
    }
    if(!isEmail($clientEmail)) {
        $array['emailMessage'] = 'Invalid email!';
    }
    if($phone == '') {
        $array['phoneMessage'] = 'Empty phone!';
    }
    if($country == '') {
        $array['countryMessage'] = 'Empty country!';
    }
    if($company == '') {
        $array['companyMessage'] = 'Empty company!';
    }
    if($subject == '') {
        $array['subjectMessage'] = 'Empty subject!';
    }
    if($message == '') {
        $array['messageMessage'] = 'Empty message!';
    }
    if($antispam != '12') {
        $array['antispamMessage'] = 'Wrong antispam answer!';
    }
    if(isEmail($clientEmail) && $subject != '' && $message != '' && $antispam == '12') {
        // Send email

        require_once 'lib/swift_required.php';

        $transport = Swift_SmtpTransport::newInstance('p3plcpnl0552.prod.phx3.secureserver.net', 465, "ssl")
            ->setUsername('webform@vamoscloud.com')
            ->setPassword('Odesk123.');

        $mailer = Swift_Mailer::newInstance($transport);

        $message = Swift_Message::newInstance($subject)
            ->setFrom(array('assurancelicensing@gmail.com' => 'Contact Form'))
            ->setTo(array('info@licensingassurance.com', 'marianaperozo@licensingassurance.com'))
            ->setBody('Name: ' .$name."<br /> Email: " . $clientEmail . "<br> Phone: ".$phone."<br /> "
            . "Contry: " . $country . "<br> Company: " . $company . "<br> Subject: ". $subject."<br />".$message, 'text/html');

        $result = $mailer->send($message);
    }

    echo json_encode($array);
}