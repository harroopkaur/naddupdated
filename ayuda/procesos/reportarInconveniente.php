<?php
//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS['app_root'] . "/plantillas/middleware.php");

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_validator.php");
require_once($GLOBALS['app_root'] . "/clases/clase_configuraciones.php");



$configuracion  = new configuraciones();
$fabricante = $configuracion->listar_fabricantes("");

$general = new General();

$validator = new validator("form1");
$validator->create_message("msj_fabricante", "fabricante", " Campo Obligatorio", 0);
$validator->create_message("msj_falla", "falla", " Campo Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail valido", 3);
//fin middleware

$exito = false;
if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    $fabricante = 0;
    if(isset($_POST['fabricante']) && filter_var($_POST['fabricante'], FILTER_VALIDATE_INT)){
        $fabricante = $_POST["fabricante"];
    }

    $falla = "";
    if(isset($_POST["falla"])){
        $falla = $general->get_escape($_POST["falla"]);
    }

    $correo = "";
    if(isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $correo = $_POST["email"];
    }

    $exito = $configuracion->agregarFallas($_SESSION["client_id"], $fabricante, $falla, $correo);
}