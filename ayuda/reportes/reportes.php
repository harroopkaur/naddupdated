<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/procesos/indexFabricantes.php");
//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS['app_root'] . "/plantillas/middleware.php");
//fin middleware

require($GLOBALS['app_root'] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>
<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 0;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="bordeContenedor">
            <div class="contenido"> 
                <h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

                <br>

                <div class="botonesAyuda">
                    C&uacute;ales son los reportes que ofrece la Webtool
                </div>

                <br>

                <div class="opcionesAyuda" style="width:95%;">
                    <ul>
                        <li><a href="#">Licensign Assurance LLC. ofrece a sus clientes a traves de la Webtool reporte 
                            en tiempo real, dentro de los cuales destacan:</a></li>
                        <li><a href="#"><span class="bold">1.- Detalle:</span></a></li>
                        <li><p class="text-justify">Este reporte le permite visualizar los productos o licencias instalados en los 
                            equipos objeto de análisis, cuando se efectuó el despliegue. La manera en como la herramienta 
                            lo ordena es a través de tipo de producto y edición de producto. Por ejemplo, podrá seleccionar 
                            el Producto Windows OS y la edición Enterprise, donde la Webtool le informara qué equipos poseen 
                            esta licencia.</p><br></li>
                        <li><p class="text-justify">También cuenta con ventajas adicionales tales como: cualquiera de sus 
                            componentes (Nombre Equipo, Tipo, Sistema Operativo, Activo AD, LA Tool, Usabilidad) Exportar 
                            el listado de los equipos en formato Excel con solo hacer clic en el botón “Exportar Excel” 
                            Exportar la gráfica que indica el porcentaje de equipos activos e inactivos.</p><br></li>
                        <li><a href="#"><span class="bold">2.- Alcance:</span></a></li>
                        <li><p class="text-justify">Corresponde al alcance del descubirmiento. Este reporte apreciará cuáles, 
                            y cuántos equipos se encuentran: activos, en reconciliación, a cuáles pudo leer la herramienta y 
                            cuál fue la cobertura. Además, puede identificar cuáles equipos pertenecen a un cliente o usuario 
                            y cuáles son servidores.</p><br></li>
                        <li><a href="#"><span class="bold">3.- Usabilidad:</span></a></li>
                        <li><p class="text-justify">Si lo que requiere saber es la usabilidad de los equipos dentro del AD, 
                            seleccione el reporte Usabilidad ya que, le mostrará cuáles equipos están en uso, en uso probable 
                            u obsoletos, tomando en cuenta su última fecha de actividad y categorizándolos, de manera pueda 
                            evaluar la acciones a seguir en caso de que un equipo en estado "Obsoleto" tenga instalado licencias 
                            que puedan ser aprovechables.</p><br></li>
                        <li><a href="#"><span class="bold">4.- Balanza:</span></a></li>
                        <li><p class="text-justify">Si está interesado en comparar las compras vs. las instalaciones,lo obtendrá 
                            seleccionando el reporte Balanza. La herramienta le mostrará la información por producto y edición al 
                            igual que graficará esta información para que su análisis sea más completo.</p><br></li>
                        <li><a href="#"><span class="bold">5.- Optimización:</span></a></li>
                        <li><p class="text-justify">A tarves de este reporte podrá visualizar los productos que tiene instalados 
                            cada equipo y la usabilidad de dicho equipo. Este reporte le ofrece una vision del ahorro potencial 
                            que puede tener al evaluar la cantidad de equipos que cuentan con licencias que puedan ser aprovechadas, 
                            ahorrandose así la compra de nuevas licencias.</p><br></li>
                        <li><a href="#"><span class="bold">6.- Detalle por equipo:</span></a></li>
                        <li><p class="text-justify">Este reporte le ofrece el detalle de las licencias que tiene instalada cada equipo al cual se le hizo el analisis.</p><br></li>
                        <li><a href="#"><span class="bold">7.- Equipos no descubiertos:</span></a></li>
                        <li><p class="text-justify">Este repore le indica el listado de equipos que no pudieron ser analizados durante la ejecución de la herramienta.</p><br></li>
                    </ul>
                </div>

                <div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/';">Regresar</div></div>
                <br>
                <br>
                <br>
                </div>
            </div>
        </div>
    </section>
<?php
    require_once($GLOBALS['app_root'] . "/plantillas/pie.php");
?>