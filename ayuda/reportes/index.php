<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/procesos/indexFabricantes.php");
//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS['app_root'] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS['app_root'] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 0;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="bordeContenedor">
            <div class="contenido"> 
                <h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

                <br>

                <div class="botonesAyuda">
                    C&uacute;ales son los reportes que ofrece la Webtool
                </div>

                <br>

                <div class="opcionesAyuda">
                    <ol>
                        <li><a href="#">Licensign Assurance LLC. ofrece a sus clientes a traves de la Webtool reporte 
                            en tiempo real, dentro de los cuales destacan:</a><br></li>
                        <li><a href="#" class="link1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/reportes.php'">1.- Detalle.</a></li>
                        <li><a href="#" class="link1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/reportes.php'">2.- Alcance.</a></li>
                        <li><a href="#" class="link1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/reportes.php'">3.- Usabilidad.</a></li>
                        <li><a href="#" class="link1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/reportes.php'">4.- Balanza.</a></li>
                        <li><a href="#" class="link1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/reportes.php'">5.- Optimización.</a></li>
                        <li><a href="#" class="link1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/reportes.php'">6.- Detalle por equipo.</a></li>
                        <li><a href="#" class="link1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/reportes.php'">7.- Equipos no descubiertos.</a></li>
                    </ol>
                </div>

                <div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/';">Regresar</div></div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</section>

<?php
require_once($GLOBALS['app_root'] . "/plantillas/pie.php");
?>