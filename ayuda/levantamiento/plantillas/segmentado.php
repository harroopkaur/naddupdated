<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    C&oacute;mo ejecutar el levantamiento
</div>

<br>

<div class="opcionesAyuda">
    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/LA_Tool_Localv6.4.rar">LA_Tool_Localv6.4.rar</a></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Configurar el rango de IPs para ejecutar la herramienta</p>
    <p>Se generar&aacute; un archivo llamado "Equipos.zip", guarda ese archivo en el escritorio, descomprimirlo y reemplazar el archivo Equipo.txt guardado en el paso 3.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>En el paso anterior se generará un archivo llamado Equipos.zip, descomprimir el mismo y guardarlo en la misma carpeta en la que se encuentran los componentes de la herramienta reemplazando el archivo original Equipos.txt por el nuevo.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.1 -&nbsp;</span>Confirmar que el archivo nuevo Equipos.txt fue reemplazado correspondiente abriendo el mismo y confirmar que se encuentra el rango de IP s citado y no se encuentra solamente la máquina "Localhost"</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">10.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p><br />
</div>

<br>
<div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/levantamiento/tipoLevantamiento.php?fabricante=<?= $fabricante ?>';">Regresar</div></div>
<br>
<br>
<br>