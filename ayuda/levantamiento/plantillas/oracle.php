<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    C&oacute;mo ejecutar el levantamiento
</div>

<br>

<div class="opcionesAyuda">
    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, favor realizar los siguientes desde el Controlador de Dominio.  Para cualquier versi&oacute;n de Sistema Operativo Windows:</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar en una carpeta dentro del Controlador de Dominio el archivo <a class="link1" href="<?= $GLOBALS['domain_root'] ?>/LATool_Oracle.rar">LATool_Oracle.rar</a></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Descomprimir y ejecutar Renombrar.bat</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Hacer click derecho sobre el archivo OracleInstallations_LA.vbs y seleccionar "Abrir con el s&iacute;mbolo del sistema" u "Open with Command Prompt" como indica la imagen</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Se iniciar&aacute; una ventana de Command Prompt donde debe introducir el nombre de dominio y extensi&oacute;n para obtener los resultados</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>El script tomar&aacute; un tiempo variable por cada servidor conectado al dominio para obtener toda la informaci&oacute;n necesaria y crear&aacute; un archivo llamado "LAD_Oracle.rar"</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Favor subir el archivo "LAD_Oracle.rar" generado.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p><br />
</div>

<div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/levantamiento/';">Regresar</div></div>
<br>
<br>
<br>