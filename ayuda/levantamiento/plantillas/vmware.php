<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    C&oacute;mo ejecutar el levantamiento
</div>

<br>

<div class="opcionesAyuda">
    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, favor extraer licencias de ESXi administrados por vCenter Server</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1.-&nbsp;</span>Se ejecuta vSphere Client y accede a vCenter Server utilizando las credenciales Administrador. En este caso utiliza el usuario por defecto <strong style="font-weight:bold;">Administrator@vsphere.local</strong></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Una vez dentro seleccionar Home</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>En el panel de Administraci&oacute;n, seleccionar Licensing</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Seleccionar Export para extraer la lista en formato CSV</p><br>
</div>

<div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/levantamiento/';">Regresar</div></div>
<br>
<br>
<br>