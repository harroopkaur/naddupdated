<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    C&oacute;mo ejecutar el levantamiento
</div>

<br>

<div class="opcionesAyuda">
    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, favor realizar los siguientes pasos para cada Servidor <span class="bold">Solaris, AIX y Linux</span> por separado. 
        La herramienta debe ser ejecutada en un servidor dentro de la misma red de todos los equipos a levantarse ya que mediante este se conectar&aacute;
        a todas las maquinas que se encuentren enlazadas mediante ssh y obtendr&aacute; los productos, procesos y archivos.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar en una carpeta dentro del Controlador de Dominio el archivo <a class="link1" href="<?= $GLOBALS['domain_root'] ?>/LATool_Unix.zip">LATool_Unix.zip</a></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Copiar y Descomprimir la herramienta dentro de cualquier carpeta que tenga acceso de administrador. 
        Este directorio contiene dos archivos <span style="font-weight:bold;">Find_LA.sh</span> y <span style="font-weight:bold;">hosts</span>, además de dos carpetas scripts (OracleInstallations_LA.sh) y reports (vacio).</p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Convertir a los archivos <span style="font-weight:bold;">Find_LA.sh y OracleInstallations_LA.sh</span>
        en ejecutables mediante el comando:</p>
    <p>chmod 755 Find_LA.sh</p> 
    <p>chmod 755 OracleInstallations_LA.sh</p><br>

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Llenar el archivo hosts el cual cuenta con <span style="font-weight:bold;">localhost root</span>, como configuraci&oacute;n 
        b&aacute;sica y se deber&aacute; llenar el archivo con el IP del servidor y el usuario administrador separados con un espacio o tabulaci&oacute;n para que el 
        script principal pueda leer los equipos que ser&aacute;n escaneados. Cada equipo y usuario deber&aacute; especificarse en una l&iacute;nea distinta del archivo.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Ejecutar el script <span style="font-weight:bold;">Find_LA.sh</span> con el comando:</p>
    <p>./ Find_LA.sh hosts</p>
    <p>ya que este leer&aacute; los hosts especificados previamente y acceder&aacute; mediante ssh con la cuenta especificada (debe ser administrador) con el fin de realizar 
        una b&uacute;squeda de cualquier producto Oracle instalado.</p><br>

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>El script ir&aacute; copiando los resultados en la carpeta /reports. Una vez finalizado el escaneo favor ingresar a la carpeta y generar un .rar seleccionando todos los archivos dentro de la misma.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Favor subir el archivo .rar a continuaci&oacute;n.</p><br>
</div>

<div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/levantamiento/';">Regresar</div></div>
<br>
<br>
<br>