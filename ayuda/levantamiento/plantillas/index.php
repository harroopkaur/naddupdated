<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    C&oacute;mo ejecutar Levantamiento / Herramientas
</div>

<br>

<div class=" contenedorFlex">
    <div class="opcionesAyuda">
        <div class="botonesAyuda">
            Levantamiento
        </div>
        
        <div class="centrarDivAyudaLevantamiento">
            <ol>
                <li><a href="tipoLevantamiento.php?fabricante=Adobe" class="link1">1.- Levantamiento para Adobe</a></li>
                <li><a href="tipoLevantamiento.php?fabricante=IBM" class="link1">2.- Levantamiento para IBM</a></li>
                <li><a href="tipoLevantamiento.php?fabricante=Microsoft" class="link1">3.- Levantamiento para Microsoft</a></li>
                <li><a href="oracle.php" class="link1">4.- Levantamiento para Oracle</a></li>
                <li><a href="sap.php" class="link1">5.- Levantamiento para SAP</a></li>
                <li><a href="vmware.php" class="link1">6.- Levantamiento para VMWare</a></li>
                <li><a href="unixIBM.php" class="link1">7.- Levantamiento para UNIX-IBM</a></li>
                <li><a href="unixOracle.php" class="link1">8.- Levantamiento para UNIX-Oracle</a></li>
            </ol>
        </div>
    </div>

    <div class="opcionesAyuda">
        <div class="botonesAyuda">
            Herramientas / Guías 
        </div>
        
        <div class="centrarDivAyudaLevantamiento">
            <ol>
                <li><a href="<?= $GLOBALS["domain_root"] ?>/ayuda/guias/Citrix.rar" class="link1">1.- Citrix</a></li>
                <li><a href="<?= $GLOBALS["domain_root"] ?>/ayuda/guias/GSuits.rar" class="link1">2.- GSuits</a></li>
                <li><a href="<?= $GLOBALS["domain_root"] ?>/ayuda/guias/McAfee.rar" class="link1">3.- McAfee</a></li>
                <li><a href="<?= $GLOBALS["domain_root"] ?>/ayuda/guias/Oracle.rar" class="link1">4.- Oracle</a></li>
                <li><a href="<?= $GLOBALS["domain_root"] ?>/ayuda/guias/Salesforce.rar" class="link1">5.- Salesforce</a></li>
                <li><a href="<?= $GLOBALS["domain_root"] ?>/ayuda/guias/usabilidadSoftware.rar" class="link1">6.- Usabilidad del Software</a></li>
            </ol>
        </div>
    </div>
</div>

<div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/';">Regresar</div></div>
<br>
<br>
<br>