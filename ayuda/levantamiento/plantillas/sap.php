<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    C&oacute;mo ejecutar el levantamiento
</div>

<br>

<div class="opcionesAyuda">
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Versiones</span></p>									
    <p>Release Producto</p>
    <p>4.6 SAP R/3</p>
    <p>4.7 SAP R/3</p>
    <p>5.0 ECC SAP ECC 5.0</p>
    <p>6.0 ECC SAP ECC 6.0</p>
    <br>

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Requisitos</span></p>									
    <p>Acceso al servidor desarrollo del Sistema SAP ECC y un usuario con licencia para ejecutar la transacci&oacute;n ZLICSAP. El SAP GUI debe permitir descargar archivos a la PC del usuario. En los reportes ALV se debe permitir la integraci&oacute;n con MS Excel (2007 a 2013).</p><br />

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Primera Parte	Descarga Herramienta</span></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Ejecutar en la l&iacute;nea de comandos la transacci&oacute;n SE38 y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"> <span style="font-weight:bold; margin-left:25px;">"Favoritos"</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Ingresamos el nombre del programa ZSISTDE, Seleccionamos <span style="font-weight:bold;">"codigo fuente"</span> y pulsamos <span style="font-weight:bold;">"Crear"</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Descargar C&oacute;digo fuente.  <a class="link1" href="<?=$GLOBALS['domain_root']?>/ZSISTDET.txt" target="_blank">ZSISTDET.txt</a></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Reemplazamos todo por el codigo fuente</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>Guardar <img src="<?=$GLOBALS['domain_root']?>/imagenes/guardar.png" style="width:20px; margin-top:2px; position:absolute;"><span style="margin-left:25px; margin-right:5px;">y Generar </span><img src="<?=$GLOBALS['domain_root']?>/imagenes/generar1.png" style="width:22px; position:absolute;"></p><br>
    <br>

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Segunda Parte	Componentes de Software Instalados</span></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Componentes de Software Instalado"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Hacer click en el icono.  Elegir la opci&oacute;n <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversi&oacute;n"</span>. El sistema crear&aacute; el archivo con el nombre <span style="font-weight:bold;">"Output_1_texto Componentes Sistema.txt"</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>CARGAR EL RESULTADO</p><br>

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Tercera Parte	Usuarios por m&oacute;dulo</span></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Usuarios por m&oacute;dulo"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Para generar el output del archivo damos click en el icono.  Elegir <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversion"</span>.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Hacer click al icono crear y el fichero ser&aacute; guardado con el nombre <span style="font-weight:bold;">"Output_1_texto_Usuarios x Modulo.txt"</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>CARGAR EL RESULTADO</p><br>

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Cuarta Parte Usabilidad de Usuarios</span></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Aging Usarios"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Para generar el output del archivo damos click en el icono.  Elegir <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversion"</span>.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Hacer click al icono crear y el fichero ser&aacute; guardado con el nombre <span style="font-weight:bold;">"Output_1_texto Aging Usuarios.txt"</span></p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>CARGAR EL RESULTADO</p><br>
</div>

<div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/levantamiento/';">Regresar</div></div>
<br>
<br>
<br>