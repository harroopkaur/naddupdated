<?php
if ($exito == true) { ?>
    <script type="text/javascript">
        $.alert.open('info', "Pregunta de la falla enviada con éxito", {'Aceptar' : 'Aceptar'}, function() {
        });
    </script>
<?php
}
else if (isset($_POST['insertar']) && $_POST["insertar"] == 1 && $exito == false) {
?>
    <script type="text/javascript">
        $.alert.open('alert', "No se pudo enviar la pregunta de la falla", {'Aceptar' : 'Aceptar'}, function() {
        });
    </script>
<?php
}
?>
            
<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    Reportar inconvenientes con la Webtool
</div>

<br>

<p class="text-justify margenAyuda">Estimado cliente a través de este módulo puede reportar fallas o inconvenientes 
que tienen con la Webtool. Por favor, llene el siguiente formulario para que un experto dentro de nuestro equipo de 
Atención al Cliente, le  esté contactando para ofrecerle la solucion que usted requiere.</p>

<form id="form1" name="form1" method="post" action="reportarInconveniente.php">
    <?php $validator->print_script(); ?>
    <input type="hidden" id="insertar" name="insertar" value="1">
    <div class="contenedorPoliticasAyuda">
        <div class="contenedorRow">
            <div class="divSmall">
                <p>Fabricante: </p>
            </div>
            <div>
                <select id="fabricante" name="fabricante" style="height:30px; width:164px; margin-left:20px; border:2px solid;">
                    <option value="">--Selecciones--</option>
                    <?php 
                    foreach($fabricante as $row){
                    ?>
                        <option value="<?= $row["idFabricante"] ?>"><?= $row["nombre"] ?></option>
                    <?php
                    }
                    ?>
                </select>                                                
            </div>
            <div class="error_prog" style="margin-left:15px;"><font color="#FF0000"><?= $validator->show("msj_fabricante") ?></font></div>
        </div>

        <div class="contenedorRow">
            <div class="divSmall">
                <p>Falla:</p> 
            </div>
            <div>
                <textarea style="height:150px; width:300px; margin-left:20px; border:2px solid;" id="falla" name="falla"></textarea>
            </div>
            <div class="error_prog" style="margin-left:15px;"><font color="#FF0000"><?= $validator->show("msj_falla") ?></font></div>
        </div>

        <div class="contenedorRow">
            <div class="divSmall">
                <p>Correo Electrónico:</p> 
            </div>
            <div>
                <input type="text" style="height:30px; width:300px; margin-left:20px; border:2px solid;" id="email" name="email"> 
            </div>
            <div class="error_prog" style="margin-left:15px;"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div>
        </div>
    </div>

    <div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/';">Regresar</div></div>
    <div style="float:right;"><div class="botones_m2" id="boton1" onclick="validate();">Enviar</div></div>
    <br>
    <br>
    <br>
</form>