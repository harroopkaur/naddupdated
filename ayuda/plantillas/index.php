<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<div class="contenedorAyuda">
    <div class="vertical">
        <div class="botonesAyuda pointer" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/levantamiento/';">
            C&oacute;mo ejecutar Levantamiento / Herramientas
        </div>
    </div>

    <div class="vertical">
        <div class="botonesAyuda pointer" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/tutoriales.php';">
            Videos Tutoriales
        </div>
    </div>

    <div class="vertical">
        <div class="botonesAyuda pointer" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/compras/'">
            C&oacute;mo cargar las compras
        </div>
    </div>

    <div class="vertical">
        <div class="botonesAyuda pointer" onclick="location.href='reportarInconveniente.php'">
            Reportar inconvenientes con la Webtool
        </div>
    </div>

    <div class="vertical">
        <div class="botonesAyuda pointer" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/reportes/'">
            C&uacute;ales son los reportes que ofrece la Webtool
        </div>
    </div>

    <div class="vertical">
        <div class="botonesAyuda pointer" onclick="location.href='procesosClave.php'">
            Qu&eacute; son los procesos claves
        </div>
    </div>
</div>