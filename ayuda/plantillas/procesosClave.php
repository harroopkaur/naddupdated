<h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

<br>

<div class="botonesAyuda">
    Qu&eacute; son los procesos claves
</div>

<br>

<p class="text-justify margenAyuda">La construcción de un programa Software Asset Management (<span class="bold">SAM</span>) incluye el arduo trabajo de 
recopilación de datos y documentos, que pueden ser demasiado costosos para automatizar, es por 
ello que en Licensing Assurance apoyamos al cliente en brindar herramientas automatizadas que 
hagan este proceso realidad.</p>

<br>

<p class="text-justify margenAyuda">Las metas secundarias de <span class="bold">SAM</span> son la reducción de costos y el 
control de riesgos relacionados con violaciones de derechos de autor y el uso indebido de licencias.</p>

<br>

<p class="text-justify margenAyuda">Es por ello que cuando adquieres nuestro servicio Licensing Management o SAM Administration le hacemos entrega y de 
las politicas claves para que tenga un SIstema de Gestión y Administración de Software con los mejores recultados para su organizacipon.</p>

<br>

<p class="text-justify margenAyuda">Son dos los las claves o procesos para un optimo sistema de gestión, la cuales son destacan:</p>

<br>

<div class="contenedorPoliticasAyuda">
    <div class="dosBloques">
        <span class="bold">Claves o Políticas de Adquisiciones</span>
    </div>
    <div class="dosBloques">
        <span class="bold">Claves de Identificación de Activos de Software</span>
    </div>

    <div class="dosBloques">
        <span class="bold">Claves o Políticas de Comunicación</span>
    </div>
    <div class="dosBloques">
        <span class="bold">Claves de Documentación</span>
    </div>

    <div class="dosBloques">
        <span class="bold">Políticas en la Administración</span>
    </div>
    <div class="dosBloques">
        <span class="bold">Claves de Admin. de Finanzas</span>
    </div>

    <div class="dosBloques">
        <span class="bold">Claves o Políticas de Cumplimiento</span>
    </div>
    <div class="dosBloques">
        <span class="bold">Claves de Admin. de Programas de TI</span>
    </div>

    <div class="dosBloques">
        <span class="bold">Claves de Legislación</span>
    </div>
    <div class="dosBloques">
        <span class="bold">Claves de Admin. de Proyectos</span>
    </div>

    <div class="dosBloques">
        <span class="bold">Claves de Admin. de Eliminación de Software</span>
    </div>
    <div class="dosBloques">
        <span class="bold">Claves de Admin. y Gestión de Proveedores</span>
    </div>
</div>

<div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/';">Regresar</div></div>
<br>
<br>
<br>