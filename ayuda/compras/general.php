<?php
    require_once("../../configuracion/inicio.php");
    require_once($GLOBALS['app_root'] . "/plantillas/sesion.php");
    require_once($GLOBALS["app_root"] . "/procesos/indexFabricantes.php");
    require_once($GLOBALS['app_root'] . "/plantillas/head.php");
    require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 0;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="bordeContenedor">
            <div class="contenido"> 
                <h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

                <br>

                <div class="botonesAyuda">
                    C&oacute;mo cargar las compras
                </div>

                <br>

                <div class="opcionesAyuda" style="width:95%">
                    <p style="font-size:16px; color:#000; font-weight:300;"><p class="text-justify">Estimado Cliente, hay dos formas de cargar las compras a la Webtool. 
                    Si usted adquirió el servicio SAM Administration, cuenta con el apoyo de nuestro departamento 
                    de Atención al Cliente, donde personal calificado se encarga de subir a la Webtool el o los 
                    contratos de licencias de software que usted quiere administrar. Por lo tanto, al ir a la pestaña 
                    Compras se mostrará un botón llamado “Cargar Compras del Repositorio” y al hacer clic se desplegará 
                    el o los contratos activos para que, elija el que le conviene para el análisis que requiera hacer.</p><br>
                    <p class="text-justify">Por otro lado, si usted adquirió el servicio Licensing Webtool o Licensing 
                    Management puede subir las compras siguiendo los pasos a continuación:</p><br>
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar el siguiente <a class="link1" href="<?= $GLOBALS["domain_root"]?>/formulario_compras_clientes.xlsx">Formulario de Compras</a></p><br />
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Completar los datos del formulario</p><br>
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Abrir un nuevo libro de Excel y copiar todos los datos de las columnas: 
                    Producto, Edición, Versión, SKU, Tipo, Cantidades, Precio Unitario y Asignación, en este archivo 
                    incluyendo encabezados. Seleccione la opción "Guardar como", y elija el tipo de archivo "CSV 
                    (delimitado por comas)(*.csv)".</p><br>
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Subir archivo CSV guardado</p><br>
                </div>

                <div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/compras/';">Regresar</div></div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</section>

<?php
require_once($GLOBALS['app_root'] . "/plantillas/pie.php");
?>