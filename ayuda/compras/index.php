<?php
    require_once("../../configuracion/inicio.php");
    require_once($GLOBALS['app_root'] . "/plantillas/sesion.php");
    require_once($GLOBALS["app_root"] . "/procesos/indexFabricantes.php");
    require_once($GLOBALS['app_root'] . "/plantillas/head.php");
    require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 0;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="bordeContenedor">
            <div class="contenido"> 
                <h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

                <br>

                <div class="botonesAyuda">
                    C&oacute;mo cargar las compras
                </div>

                <br>

                <div class="opcionesAyuda">
                    <ol>
                        <li><a href="general.php" class="link1">1.- Adobe</a></li>
                        <li><a href="general.php" class="link1">2.- IBM</a></li>
                        <li><a href="general.php" class="link1">3.- Microsoft</a></li>
                        <li><a href="general.php" class="link1">4.- Oracle</a></li>
                        <li><a href="general.php" class="link1">5.- SAP</a></li>
                        <li><a href="vmware.php" class="link1">6.- VMWare</a></li>
                        <li><a href="general.php" class="link1">7.- UNIX-IBM</a></li>
                        <li><a href="general.php" class="link1">8.- UNIX-Oracle</a></li>
                    </ol>
                </div>

                <div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/';">Regresar</div></div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</section>

<?php
require_once($GLOBALS['app_root'] . "/plantillas/pie.php");
?>