<?php
    require_once("../../configuracion/inicio.php");
    require_once($GLOBALS['app_root'] . "/plantillas/sesion.php");
    require_once($GLOBALS["app_root"] . "/procesos/indexFabricantes.php");
    require_once($GLOBALS['app_root'] . "/plantillas/head.php");
    require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 0;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="bordeContenedor">
            <div class="contenido"> 
                <h1 class="textog negro" style="margin:20px; text-align:center;">Centro de Ayuda</h1>

                <br>

                <div class="botonesAyuda">
                    C&oacute;mo cargar las compras
                </div>

                <br>

                <div class="opcionesAyuda" style="width:95%">
                    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1.-&nbsp;</span>Para exportar y cotejar las licencias adquiridas con VMware se debe al portal de licenciamiento <a class="link1" href="https://my.vmware.com" target="_blank">https://my.vmware.com</a>. Ser&aacute;n necesarias las credenciales del usuario con rol "SuperUser" en el portal (correo que se provee a la hora de la compra de las licencias)</p><br />
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Seleccionar la opci&oacute;n "Manage License Keys"</p><br />
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Se ven todos los contratos, incluyendo aquellos que tienen soporte vencido. Presionar "Exporta all to CSV" para obtener un reporte de todos los contratos adquiridos con Vmware</p>
                    </p><br/>
                    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4. -&nbsp;</span>Subir archivo <strong style="font-weight:bold;">CSV </strong>guardado</p><br />
                </div>

                <div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/ayuda/compras/';">Regresar</div></div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</section>

<?php
require_once($GLOBALS['app_root'] . "/plantillas/pie.php");
?>