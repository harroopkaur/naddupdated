<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalenciasPDO.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_adobe.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $balance      = new balanceAdobe();
            $resumen      = new resumenAdobe();
            $equivalencia = new EquivalenciasPDO();
            $compras = new comprasAdobe();
            //$general = new General();
            $familia = "";
            if(isset($_POST["familia"])){
                $familia = $general->get_escape($_POST["familia"]);
            }
            
            $edicion = "Todo";
            if(isset($_POST["edicion"])){
                $edicion = $general->get_escape($_POST["edicion"]);
            }
            
            $asig = "";
            if(isset($_POST["asig"])){
                $asig = $general->get_escape($_POST["asig"]);
            }
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $seleccion    = "";
            //$TotalCompras = 0;
            $uso          = 0;
            $sinUso       = 0;
            //$TotalInstal  = 0;	
            //$Neto         = 0;        
            $titulo       = "";
            
            $listar = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones); 
            foreach ($listar as $reg_equipos) {
                if($reg_equipos["rango"] == 1){
                    $uso += $reg_equipos["conteo"];
                } else{
                    $sinUso += $reg_equipos["conteo"];
                }
            }
            $TotalInstal = $uso + $sinUso;
            $TotalCompras = $compras->totalCompras($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones);
            $Neto = $TotalCompras - ($uso + $sinUso);
            
            $familiaAux = $familia;
            if($edicion == ""){
                $familiaAux = $familia; 
            } else{
                $familiaAux = $edicion;
            }
            
            $listar_adobe = $balance->balanzaAsignacion($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones);
            //listar_todo_familias2
            
            /*if(($familia != "Otros" && $edicion == "") || ($familia == "Otros" && $edicion == "")){
                if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], $familia)) {
                    foreach ($balance->lista as $reg_equipos) {
                        $TotalCompras += $reg_equipos["compra"];
                        //$TotalInstal  += $reg_equipos["instalaciones"];
                    }

                }

                if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], $familia)) {
                    foreach ($resumen->lista as $reg_equipos) {
                        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                            $uso  += $reg_equipos["conteo"];
                        }
                        else{
                            $sinUso += $reg_equipos["conteo"];
                        }

                    }
                }

                $TotalInstal = $uso + $sinUso;
                $Neto = $TotalCompras - $TotalInstal;
            }
            /*else if($familia != "Otros" && $edicion != ""){
                if($balance->listar_todo_familias($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion)) {
                    foreach ($balance->lista as $reg_equipos) {
                        $TotalCompras += $reg_equipos["compra"];
                        //$TotalInstal  += $reg_equipos["instalaciones"];
                    }
                }

                if($resumen->graficoBalanzaEdicion($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion)) {
                    foreach ($resumen->lista as $reg_equipos) {
                        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                            $uso  += $reg_equipos["conteo"];
                        }
                        else{
                            $sinUso += $reg_equipos["conteo"];
                        }

                    }
                }

                $TotalInstal = $uso + $sinUso;
                $Neto = $TotalCompras - $TotalInstal;
            }
            else if($familia == "Otros"){
                if($balance->listar_todo_familias1($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion)) {
                    foreach ($balance->lista as $reg_equipos) {
                        $TotalCompras += $reg_equipos["compra"];
                        $TotalInstal  += $reg_equipos["instalaciones"];
                    }
                    $Neto = $TotalCompras - $TotalInstal;
                }

                if($resumen->graficoBalanzaEdicion1($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion)) {
                    foreach ($resumen->lista as $reg_equipos) {
                        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                            $uso  += $reg_equipos["conteo"];
                        }
                        else{
                            $sinUso += $reg_equipos["conteo"];
                        }

                    }
                }

                $TotalInstal = $uso + $sinUso;
                $Neto = $TotalCompras - $TotalInstal;
            }*/

            if($familia != "Otros"){
                $titulo    = $familia . " " . $edicion;
                $ediciones = $equivalencia->edicionesProductoxNombre(1, $familia);
            }
            else{
                $titulo    = $edicion;
                /*$notIn     = array("Adobe Acrobat", "Adobe Creative Cloud", "Adobe Creative Suite");
                $ediciones = $equivalencia->ProductoxNombre(1, $notIn);*/
                $balance->productosNoIncluir($_SESSION["client_id"], $_SESSION['client_empleado']); 
                $ediciones = $equivalencia->ProductoxNombre(1, $balance->listaNoIncluir);
                /*if($edicion != "Todos"){
                    $edicion = $ediciones[0]["nombre"];
                    if($_POST["edicion"] == ""){
                        $titulo    = $edicion;
                    }
                }*/
            }

            $tabla = "";
            foreach($listar_adobe as $reg_equipos){
                $tabla .= '<tr>
                        <td>' . $reg_equipos["familia"] . '</td>
                        <td>' . $reg_equipos["office"] . '</td>
                        <td>' . $reg_equipos["version"] . '</td>
                        <td>' . $reg_equipos["asignacion"] . '</td>
                        <td align="center">' . $reg_equipos["instalaciones"] . '</td>
                        <td align="center">' . round($reg_equipos["compra"], 0) . '</td>
                        <td align="center">' . round($reg_equipos["balance"], 0) . '</td>
                        <td align="center"';
                    if($reg_equipos["balancec"] < 0){
                        $tabla .= ' style="color:red;"';
                    }
                $tabla .= '>' . round($reg_equipos["balancec"], 0) . '</td>
                </tr>';
            }

            $options = "<option value='Todo'>Seleccione..</option>";
            foreach($ediciones as $row){
                $options .= "<option value='". $row["nombre"] ."' ";
                if($row["nombre"] == $edicion){
                    $options .= " selected='selected'";
                }
                $options .= ">" . $row["nombre"] . "</option>";
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'titulo'=>$titulo, 'compra'=>$TotalCompras, 'instalacion'=>$TotalInstal, 'tabla'=>$tabla, 
            'edicion'=>$options, 'neto'=>$Neto, 'uso'=>$uso, 'sinUso'=>$sinUso, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);