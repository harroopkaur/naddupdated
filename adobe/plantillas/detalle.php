<script>
    function MM_jumpMenu(targ,selObj,restore){ //v3.0
        eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
        if (restore) selObj.selectedIndex=0;
    }
</script>

<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:20px; margin-bottom:20px;">
    <div style="width:20%; margin:0px; padding:0px; overflow:hidden; float:left;">
        <span style="color:#9F9F9F;">Seleccione el Producto</span><br><br> 

        <div>Familia: <strong style="color:#000; font-weight:bold;">
            <?php
                $tablaMaestra->productosSalida(1);
                foreach($tablaMaestra->lista as $row){
                    switch($vert){
                        case $row["campo1"] : echo $row["campo1"]; break;
                    }
                }
            ?>
            </strong>
            <br> 

            <select onchange="MM_jumpMenu('self',this,0)">
                <option value="detalle.php" selected>Seleccione..</option>
                <?php 
                foreach($tablaMaestra->lista as $row){
                ?>
                    <option value="detalle.php?vert=<?= $row['campo1']?>"><?= $row["campo1"] ?></option>
                <?php
                }
                ?>
            </select>

        </div><br>

        <div>Edici&oacute;n:<strong style="color:#000; font-weight:bold;">
            <?php
            foreach($ediciones as $row){
                switch($vert1){  
                    case $row["nombre"]: echo $row["nombre"]; break;
                }
            }
            ?></strong>
            <br> 

            <select onchange="MM_jumpMenu('self',this,0)" style="max-width:150px;">
                <option value="detalle.php" selected>Seleccione..</option>
                <option value="detalle.php?vert=<?= $vert ?>&vert1=Todo">Todo</option>
                <?php 
                foreach($ediciones as $row){
                ?>
                    <option value="detalle.php?vert=<?= $vert ?>&vert1=<?= $row['nombre']?>"><?= $row["nombre"] ?></option>
                <?php
                }
                ?>
            </select>

            <br>
            <br>
            <div>Asignaci&oacute;n:<strong style="color:#000; font-weight:bold;">
                <?= $asig ?>

                <br>
                <select onchange="MM_jumpMenu('self',this,0)">
                    <option value="detalle.php" selected>Seleccione..</option>
                    <option value="detalle.php?vert=<?= $vert ?>&vert1=<?= $vert1 ?>">Todo</option>
                    <?php 
                    foreach($asignaciones as $row){
                    ?>
                        <option value="detalle.php?vert=<?= $vert ?>&vert1=<?= $vert1 ?>&asig=<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
                    <?php
                    }
                    ?>
                </select>

                <br><br>
                <form id="formExportar" name="formExportar" method="post" action="reportes/excelAdobeDetalle.php">
                    <input type="hidden" id="vert" name="vert" value="<?php echo $vert; ?>">
                    <input type="hidden" id="vert1" name="vert1" value="<?php echo $vert1; ?>">
                    <input type="hidden" id="asignacion" name="asignacion" value="<?php echo $asig; ?>">
                    <input type="submit" id="exportar" name="exportar" style="display:none">
                    <div class="botones_m2Alterno boton1" id="export">Exportar Excel</div>
                </form>
            </div>

        </div>

    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; min-height:450px; overflow:hidden;">
        <div id="container3" height:400px; width:95%; max-width: 95%; margin:10px; float:left;"></div>
        <div  style=" width:98%; max-width: 98%; margin:10px; float:left; text-align:center;"><a onclick="mostrarTabla();" style="cursor:pointer;" >Ver Detalles</a></div>
    </div>

    <div id="ttabla1" class="tablap" style="display:none; width:99%; max-height:400px; margin:10px; float:left;">
        <table class="tablap" id="tablaMicrosoftDetalle" style="margin-top: -5px;">
            <thead>
                <tr style="background:#333; color:#fff;">
                    <th valign="middle"><span>&nbsp;</span></th>
                    <th valign="middle"><span>Equipo</span></th>
                    <th valign="middle"><span>Tipo</span></th>
                    <th valign="middle"><span>Familia</span></th>
                    <th valign="middle"><span>Edici&oacute;n</span></th>
                    <th valign="middle"><span>Versi&oacute;n</span></th>
                    <th valign="middle"><span>Instalaci&oacute;n</span></th>
                    <th valign="middle"><span>Usabilidad</span></th>
                    <th valign="middle"><span>Observaci&oacute;n</span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($listado) > 0){
                    $i = 1;
                    foreach($listado as $reg_calculo){
                    ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $reg_calculo["equipo"] ?></td>
                            <td><?= $reg_calculo["tipo"] ?></td>
                            <td><?= $reg_calculo["familia"] ?></td>
                            <td><?= $reg_calculo["edicion"] ?></td>
                            <td><?= $reg_calculo["version"] ?></td>
                            <td><?= $reg_calculo["fecha_instalacion"] ?></td>
                            <td><?= $reg_calculo["rango"] ?></td>
                            <td>
                            <?php
                            if($resumen->duplicado($_SESSION['client_id'], $_SESSION["client_empleado"], 
                            $reg_calculo["equipo"], $reg_calculo["familia"], $reg_calculo["edicion"], $reg_calculo["version"]) > 1){
                                echo "Duplicado";
                            }
                            ?>
                            </td>
                        </tr>
                    <?php
                        $i++;
                    }	
                }
                ?>
            </tbody>
        </table>
    </div>
</div>    

<script>
    var vertAux = "";
    var opcAux  = "";
    $(document).ready(function(){
        $("#export").click(function(){
            $("#exportar").click();
        });
        
        $("#tablaMicrosoftDetalle").tablesorter();
        $("#tablaMicrosoftDetalle").tableHeadFixer();
    });
    
    function mostrarTabla(){
        if($('#ttabla1').is(':visible')){
            $('#ttabla1').hide();
        }
        else{
            $('#ttabla1').show();
        }
    }
</script>