<script type="text/javascript">
    $(function () {
        <?php
        if ($vert == 0) {
        ?>
            $('#container1').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    size: '75%'
                },
                title: {
                    text: 'Cliente'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1LAc + $reconciliacion1c ?>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        }
                    }
                },
                series: [{
                    name: 'Equipos',
                    colorByPoint: true,
                    data: [{
                        name: 'No Levantado',
                        y: <?= $reconciliacion1c ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Levantado',
                        y: <?= $total_1LAc ?>,
                        color: '<?= $color2 ?>'
                    }]
                }]
            });
        });


        $(function () {
            $('#container2').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    size: '75%'
                },
                title: {
                    text: 'Servidor'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1LAs + $reconciliacion1s ?>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        }
                    }
                },
                series: [{
                    name: 'Equipos',
                    colorByPoint: true,
                    data: [{
                        name: 'No levantado',
                        y: <?= $reconciliacion1s ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Levantado',
                        y: <?= $total_1LAs ?>,
                        color: '<?= $color2 ?>'
                    }]
                }]
            });
        <?php
        }//0
        if ($vert == 1 || $vert == 3) {
            if ($vert == 1) {
                $titulo = "Usabilidad";
            } else {
                $titulo = "Optimización";
            }
        ?>
                
            $('#container2').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Clientes'
                },
                subtitle: {
                    text: '<?php echo $titulo; ?>'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Nro. Equipos'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1 + $total_2 + $total_3 ?></b><br/>'
                },
                series: [{
                    name: 'Clientes',
                    colorByPoint: true,
                    data: [{
                        name: 'En Uso',
                        y: <?= round($total_1, 0) ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Probablemente en Uso',
                        y: <?= round($total_2, 0) ?>,
                        color: '<?= $color2 ?>'
                    },
                    {
                        name: 'Obsoleto',
                        y: <?= round($total_3, 0) ?>,
                        color: '<?= $color3 ?>'
                    }]
                }],
            });
        });

        $(function () {
            // Create the chart
            $('#container1').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Servidores'
                },
                subtitle: {
                    text: '<?php echo $titulo; ?>'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Nro. Equipos'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b><?= $total_4 + $total_5 + $total_6 ?></b><br/>'
                },
                series: [{
                    name: 'Servidores',
                    colorByPoint: true,
                    data: [{
                        name: 'En Uso',
                        y: <?= round($total_4, 0) ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Probablemente en Uso',
                        y: <?= round($total_5, 0) ?>,
                        color: '<?= $color2 ?>'
                    },
                    {
                        name: 'Obsoleto',
                        y: <?= round($total_6, 0) ?>,
                        color: '<?= $color3 ?>'
                    }]
                }],
            });
            
        <?php
        }//1
        if ($vert == 2) {
        ?>

            $('#container3').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Adobe Acrobat'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalAdobeAcrobatSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $AdobeAcrobatNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalAdobeAcrobatUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalAdobeAcrobatCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container4').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Adobe Creative Cloud'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalCreativeCloudSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $CreativeCloudNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalCreativeCloudUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalCreativeCloudCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container5').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Adobe Creative Suite'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },

                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalCreativeSuiteSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $CreativeSuiteNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalCreativeSuiteUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalCreativeSuiteCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container6').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Otros'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },

                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalOtrosSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $OtrosNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalOtrosUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalOtrosCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });
        <?php
        }//2
        ?>
     });
</script>