<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_adobeAux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_increm_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario_equipo_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");

$resumen      = new resumenAdobe();
$consolidado  = new consolidadoAdobe();
$consolidadoAux = new consolidadoAdobeAux();
$tablaMaestra = new tablaMaestra();
$archivosDespliegue = new clase_archivos_fabricantes();
$archivosIncremDespliegue = new clase_archivos_increm_fabricantes();
$general = new General();
$escaneo = new escaneoAdobe();
$detalles = new DetallesAdobe();
$usuarioEquipo = new usuarioEquipoAdobe();
$log = new log();
$passLAD = new clase_pass();
$fabricante = 1;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;
$lista = array();

$opcionDespliegue = "";
/*if(isset($_POST["opcionDespliegue"])){
    $opcionDespliegue = $general->get_escape($_POST["opcionDespliegue"]);
} else {
    $_SESSION["archivosCargados"] = "";
    $_SESSION["archCargados"] = array();
}*/

if(isset($_POST["opcionDespliegue"])){
    $opcionDespliegue = $general->get_escape($_POST["opcionDespliegue"]);
    $_SESSION["archivosCargadosLAE"] = "";
    $_SESSION["archCargadosLAE"] = array();
    $_SESSION["archivosCargadosSubsidiaria"] = "";
    $_SESSION["archCargadosSubsidiaria"] = array();
} else {
    $_SESSION["archivosCargados"] = "";
    $_SESSION["archCargados"] = array();
    $_SESSION["archivosCargadosLAE"] = "";
    $_SESSION["archCargadosLAE"] = array();
    $_SESSION["archivosCargadosSubsidiaria"] = "";
    $_SESSION["archCargadosSubsidiaria"] = array();
}

if(isset($_POST['insertar']) && $_POST['insertar'] == 1) {
    foreach($_FILES['archivo']['tmp_name'] as $key => $tmp_name){
        if(isset($_FILES['archivo']['tmp_name'][$key]) && is_uploaded_file($_FILES['archivo']['tmp_name'][$key])){
            /*$nombre_imagen = $_FILES['archivo']['name'];
            $tipo_imagen = $_FILES['archivo']['type'];
            $tamano_imagen = $_FILES['archivo']['size'];
            $temporal_imagen = $_FILES['archivo']['tmp_name'];*/
            
            $nombre_imagen = $key.$_FILES['archivo']['name'][$key];
            $tipo_imagen = $_FILES['archivo']['type'][$key];
            $tamano_imagen = $_FILES['archivo']['size'][$key];
            $temporal_imagen = $_FILES['archivo']['tmp_name'][$key];

            // Validaciones
            if($nombre_imagen!=""){
                $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
                $long = count($extension) - 1;
                 if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
                // Permitir subir solo imagenes JPG,
                }else{
                $error=2;	
            }

            if(!file_exists($GLOBALS['app_root'] . "/adobe/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'])){
                mkdir($GLOBALS['app_root'] . "/adobe/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'], 0755, true);
            }

            if(!file_exists($GLOBALS['app_root'] . "/adobe/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/")){
                mkdir($GLOBALS['app_root'] . "/adobe/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/", 0755, true);
            }

            if($error == 0){
                $listPassLAD = $passLAD->listar_todo();
                $pass = "";
               
                foreach($listPassLAD as $row){  
                    $psw = $passLAD->desencriptar($row["descripcion"]);
                    $rar_file = rar_open($temporal_imagen, $psw) or die("Can't open Rar archive");

                    $entries = rar_list($rar_file);   
                    
                    $direcEntry = $GLOBALS["app_root"] . '/tmp';

                    if(!file_exists($direcEntry)){
                        mkdir($direcEntry, 0755);
                    }
    
                    foreach ($entries as $entry) {
                        if($entry->extract($direcEntry)){
                            $pass = $psw;
                            $files = array_diff(scandir($direcEntry), array('.','..')); 
                            foreach ($files as $file) { 
                                unlink($direcEntry.'/'.$file); 
                            } 
                            rmdir($direcEntry);  
                        } 
                        
                        break;
                    }
                    
                    if($pass != ""){
                        break;
                    }
                }
                
                rar_close($rar_file);
                
                if($pass != ""){
                    $rar_file = rar_open($temporal_imagen, $pass) or die("Can't open Rar archive");
                    $entries = rar_list($rar_file);
        
                    $archivosExtraer = 3;
                    $archivosExtraidos = 0;
        
                    if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                        foreach ($entries as $entry) {
                            if($entry->getName() == "Consolidado Addremove.csv" || $entry->getName() == "Resultados_Escaneo.csv"
                               || $entry->getName() == "Consolidado Usuario-Equipo.csv"){
                                if($entry->extract('archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/")){
                                    $archivosExtraidos++;
                                }
                            }
                        }
                    } else{
                        foreach ($entries as $entry) {
                            if($entry->getName() == "Consolidado Addremove.csv" || $entry->getName() == "Resultados_Escaneo.csv"
                               || $entry->getName() == "Consolidado Usuario-Equipo.csv"){
                                if($entry->extract('archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/incremento/")){
                                    $archivosExtraidos++;
                                }
                            }
                        }
                    }
        
        
                    rar_close($rar_file);
        
                    if($archivosExtraer > $archivosExtraidos){
                        $error = 8;
                    }
                } else{
                    $error = -2;
                }
            }

            if($error == 0) {
                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Addremove.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Addremove" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo" . date("dmYHis") . ".csv");

                    $archivoConsolidado = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Addremove.csv";
                } else {
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Addremove.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Addremove" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo" . date("dmYHis") . ".csv");

                    $archivoConsolidado = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Addremove.csv";
                }

                if($general->obtenerSeparador($archivoConsolidado) === true){
                    if(($fichero = fopen($archivoConsolidado, "r")) !== false) {
                        $i=1;
                        $iDatoControl = $iHostName = $iRegistro = $iEditor = $iVersion = $iDiaInstalacion = $iSoftware = -1;
                        $procesarAddRemove = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $iDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $iHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Registro"){
                                        $iRegistro = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Editor"){
                                        $iEditor = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Version" || trim(utf8_encode($datos[$k])) == "Versión"){
                                        $iVersion = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Día de Instalacion" || trim(utf8_encode($datos[$k])) == "Día de Instalación"){
                                        $iDiaInstalacion = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Software"){
                                        $iSoftware = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }
                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Registro" 
                            || $datos[3] != "Editor" || utf8_encode($datos[4]) != "Versión" || utf8_encode($datos[5]) != "Día de Instalación" || $datos[6] != "Software")){
                                $error = 3;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($iDatoControl == -1 && $iHostName == -1 && $iRegistro == -1 && $iEditor == -1 && 
                        $iVersion == -1 && $iDiaInstalacion == -1 && $iSoftware == -1){
                            $procesarAddRemove = false;
                        } else if($iDatoControl == -1 || $iHostName == -1 || $iRegistro == -1 || $iEditor == -1 && 
                        $iVersion == -1 || $iDiaInstalacion == -1 || $iSoftware == -1){
                            $error = 3;
                        }
                    }
                } else{
                    //$error = 10;
                    $procesarAddRemove = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoEscaneo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";
                } else {
                    $archivoEscaneo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv";
                }

                if($general->obtenerSeparador($archivoEscaneo) === true){
                    if (($fichero = fopen($archivoEscaneo, "r")) !== false) {
                        $i=1;
                        $jHostname = $jStatus = $jError = -1;
                        $procesarEscaneo = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Hostname"){
                                        $jHostname = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Status"){
                                        $jStatus = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Error"){
                                        $jError = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }
                            /*if($i == 2 && ($datos[0] != "Hostname" ||  $datos[1] != "Status" || $datos[2] != "Error")){
                                $error = 4;
                                break;
                            }*/
                            $i++;
                        }

                        fclose($fichero);

                        if($jHostname == -1 && $jStatus == -1 && $jError == -1){
                            $procesarEscaneo = false;
                        } else if($jHostname == -1 || $jStatus == -1 || $jError == -1){
                            $error = 4;
                        }
                    }
                } else{
                    //$error = 11;
                    $procesarEscaneo = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoUsuarioEquipos = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Usuario-Equipo.csv";
                } else {
                    $archivoUsuarioEquipos = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Usuario-Equipo.csv";
                }

                if($general->obtenerSeparador($archivoUsuarioEquipos) === true){
                    if (($fichero = fopen($archivoUsuarioEquipos, "r")) !== false) {
                        $i=1;
                        $mDatoControl = $mHostName = $mDominio = $mUsuario = $mIP = -1;
                        $procesarUsuarioEquipos = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $mDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $mHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Dominio"){
                                        $mDominio = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Usuario"){
                                        $mUsuario = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "IP"){
                                        $mIP = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Dominio" || $datos[3] != "Usuario" || $datos[4] != "IP")){
                                $error = 9;
                                break;
                            }*/

                            $i++;
                        }
                        fclose($fichero);

                        if($mDatoControl == -1 && $mHostName == -1 && $mDominio == -1 && $mUsuario == -1 && $mIP == -1){
                            $procesarUsuarioEquipos = false;
                        } else if($mDatoControl == -1 || $mHostName == -1 || $mDominio == -1 || $mUsuario == -1 || $mIP == -1){
                            $error = 9;
                        }
                    }
                } else{
                    //$error = 14;
                    $procesarUsuarioEquipos = false;
                }
            }
        } else{
            $error = -1;
        }

        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
            for($i = 0; $i < count($_SESSION["archCargados"]); $i++){
                if($_SESSION["archCargados"][$i] == $nombre_imagen){
                    $error = 15;
                    break;
                }
            }
        }

        if($error == 0){ 
            if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                $_SESSION["archivosCargados"] .= $nombre_imagen."<br>";
                $_SESSION["archCargados"][] = $nombre_imagen;
            } else{
                $_SESSION["archivosCargados"] = "";
            }

            $nombreArchivo = 'LAD_Output' . date("dmYHis") . '.rar';
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                move_uploaded_file($temporal_imagen, 'archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/" . $nombreArchivo); 
            } else {
                move_uploaded_file($temporal_imagen, 'archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/incremento/" . $nombreArchivo); 
            }

            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], $fabricante) === false){
                    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $fabricante);
                }

                $tipoDespliegue = 0;
                if($opcionDespliegue == "completo"){
                    $tipoDespliegue = 1;
                }
                else if($opcionDespliegue == "segmentado"){
                    $tipoDespliegue = 2; 
                }

                $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION['client_empleado'], $fabricante, $nombreArchivo, $tipoDespliegue);

                $consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            } else{
                $archivosIncremDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante);

                $tipoDespliegue = 0;
                if($opcionDespliegue == "completo"){
                    $tipoDespliegue = 1;
                }
                else if($opcionDespliegue == "segmentado"){
                    $tipoDespliegue = 2; 
                }

                $fecha = $archivosIncremDespliegue->obtenerUltFecha($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante);
                $archivosIncremDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], $fecha, $fabricante, $nombreArchivo, $tipoDespliegue);
                $consolidadoAux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            }

            if($general->obtenerSeparador($archivoConsolidado) === true && $procesarAddRemove === true){
                if (($fichero = fopen($archivoConsolidado, "r")) !== false) {
                    $i=1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i > 1 && $datos[$iDatoControl] != "Dato de Control"){
                            /*if(!$consolidado->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $datos[0], $datos[1], addslashes($datos[2]), $datos[3], $datos[4], $datos[5], addslashes($datos[6]))){
                                echo $consolidado->error;
                            }*/

                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                                . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                                . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dato_control" . $j] = $general->truncarString($datos[$iDatoControl], 250);
                            $bloqueValores[":host_name" . $j] = $general->truncarString($datos[$iHostName], 250);
                            $bloqueValores[":registro" . $j] = $general->truncarString($datos[$iRegistro], 250);
                            $bloqueValores[":editor" . $j] = $general->truncarString($datos[$iEditor], 250);
                            $bloqueValores[":version" . $j] = $general->truncarString($datos[$iVersion], 250);
                            $bloqueValores[":fecha_instalacion" . $j] = $datos[$iDiaInstalacion];
                            if(filter_var($datos[$iDiaInstalacion], FILTER_VALIDATE_INT) === false){
                                $bloqueValores[":fecha_instalacion" . $j] = 0;
                            } 
                            $bloqueValores[":sofware" . $j] = $general->truncarString(utf8_encode($datos[$iSoftware]), 250);

                            if($j == $general->registrosBloque){
                                if(!$consolidado->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo "Consolidado: " . $consolidado->error;
                                }

                                if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                                    if(!$consolidadoAux->insertarEnBloque($bloque, $bloqueValores)){
                                        echo "ConsolidadoAux: " . $consolidadoAux->error;
                                    }
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $i++;
                        $exito=1;	
                    }
                    fclose($fichero);

                    if($insertarBloque === true){
                        if(!$consolidado->insertarEnBloque($bloque, $bloqueValores)){ 
                            echo "Consolidado: " . $consolidado->error;
                        }

                        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                            if(!$consolidadoAux->insertarEnBloque($bloque, $bloqueValores)){
                                echo "ConsolidadoAux: " . $consolidadoAux->error;
                            }
                        }
                    }
                }
            }

            //inicio resumen Adobe
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $resumen->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            }

            //if($consolidado->listarProductosAdobeAcrobat($_SESSION['client_id'], $_SESSION['client_empleado'])){
            $tablaMaestra->listadoProductosMaestra($fabricante);
            foreach($tablaMaestra->listaProductos as $rowProductos){  
                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $lista = $consolidado->listarProductosAdobe($_SESSION['client_id'], $_SESSION["client_empleado"], 
                    $rowProductos["campo3"], $rowProductos["campo2"]);
                } else {
                    $lista = $consolidadoAux->listarProductosAdobe($_SESSION['client_id'], $_SESSION["client_empleado"], 
                    $rowProductos["campo3"], $rowProductos["campo2"]);
                }

                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;

                foreach($lista as $reg_r){
                    $edicion = "";
                    $version = ""; 

                    $host=explode('.', $reg_r["host_name"]);
                    if(filter_var($reg_r["host_name"], FILTER_VALIDATE_IP)!== false){
                        $host[0] = $reg_r["host_name"];
                    }

                    $tablaMaestra->sustituir = "";
                    if($tablaMaestra->buscarSustituirMaestra($reg_r["version"], $rowProductos["idForaneo"], 4)){
                       $version = $tablaMaestra->sustituir;
                    }

                    $fecha_instalacion = "0000-00-00";
                    if($reg_r["fecha_instalacion"] != "" && $reg_r["fecha_instalacion"] != 0){
                        $f3 = substr($reg_r["fecha_instalacion"], 6, 2);
                        $f2 = substr($reg_r["fecha_instalacion"], 4, -2);
                        $f1 = substr($reg_r["fecha_instalacion"], 0, 4);

                        $fecha_instalacion = $f1.'-'.$f2.'-'.$f3;
                    }

                    $tablaMaestra->sustituir = "";
                    if($tablaMaestra->buscarSustituirMaestra($reg_r["sofware"], $rowProductos["idForaneo"], 5)){
                        $edicion = $tablaMaestra->sustituir;
                    }

                    /*if($edicion != "" && $version != ""){
                        $resumen->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $host[0], 'Adobe Acrobat', $edicion, $version, $fecha_instalacion);
                    }*/

                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                        . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                    } else {
                        $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                        . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                    } 

                    $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                    $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                    $bloqueValores[":equipo" . $j] = $host[0];
                    $bloqueValores[":familia" . $j] = $rowProductos["descripcion"];
                    $bloqueValores[":edicion" . $j] = utf8_encode($edicion);
                    $bloqueValores[":version" . $j] = $version;
                    $bloqueValores[":fecha_instalacion" . $j] = $fecha_instalacion;                

                    if($j == $general->registrosBloque){
                        if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){ 
                            echo "Resumen: " . $resumen->error."<br>";
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }

                if($insertarBloque === true){
                    if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){   
                        echo "Resumen: " . $resumen->error."<br>";
                    }
                }
            }
            //fin resumen Adobe

            //inicio usuario-equipo Adobe
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $usuarioEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            }

            if($general->obtenerSeparador($archivoUsuarioEquipos) === true && $procesarUsuarioEquipos === true){
                if (($fichero = fopen($archivoUsuarioEquipos, "r")) !== false) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {                   
                        if($i > 2 && $datos[$mDatoControl] != "Dato de Control"){
                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", :usuario" . $j . ", :ip" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", :usuario" . $j . ", :ip" . $j . ")";
                            } 

                            $host=explode('.',$datos[$mHostName]);
                            if(filter_var($datos[$mHostName], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[$mHostName];
                            }

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dato_control" . $j] = $datos[$mDatoControl];
                            $bloqueValores[":host_name" . $j] = $host[0];
                            $bloqueValores[":dominio" . $j] = $datos[$mDominio];
                            $bloqueValores[":usuario" . $j] = $datos[$mUsuario];
                            if(isset($datos[$mIP])){
                                $bloqueValores[":ip" . $j] = $datos[$mIP];
                            } else{
                                $bloqueValores[":ip" . $j] = null;
                            }


                            if($j == $general->registrosBloque){
                                if(!$usuarioEquipo->insertarEnBloque($bloque, $bloqueValores)){ 
                                    //echo $usuarioEquipo->error;
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $exito=1;	

                        $i++; 
                    }
                    fclose($fichero);

                    if($insertarBloque === true){
                        if(!$usuarioEquipo->insertarEnBloque($bloque, $bloqueValores)){    
                            //echo $usuarioEquipo->error;
                        }
                    }
                }
            }
            //fin usuario-equipo Adobe

            //inicio incremento
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $archivo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";
                $escaneo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);                
            } else {
                $archivo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv";
            }
            
            
            
            //if($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){
                //inicio escaneo
                //$archivo = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv";

                if($general->obtenerSeparador($archivo) === true && $procesarEscaneo === true){   
                    if (($fichero = fopen( $archivo, "r")) !== FALSE) {
                        $i = 1;
                        $j = 0;
                        $bloque = "";
                        $bloqueValores = array();
                        $insertarBloque = false;

                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                            if ($i != 1) {
                                $host = explode('.', $datos[$jHostname]);
                                if(filter_var($datos[$jHostname], FILTER_VALIDATE_IP)!== false){
                                    $host[0] = $datos[$jHostname];
                                }
                                /*if (!$escaneo->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $host[0], $datos[1], $datos[2])) {
                                }*/

                                if($j == 0){
                                    $insertarBloque = true;
                                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", "
                                    . ":errors" . $j . ")";
                                } else {
                                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", "
                                    . ":errors" . $j . ")";
                                } 

                                $bloqueValores[":cliente" . $j] = $_SESSION["client_id"];
                                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"]; 
                                $bloqueValores[":equipo" . $j] = $host[0];
                                $bloqueValores[":status" . $j] = $datos[$jStatus];
                                $bloqueValores[":errors" . $j] = $datos[$jError];

                                if($j == $general->registrosBloque){
                                    if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){
                                        //echo $escaneo->error;
                                    }
                                    $bloque = "";
                                    $bloqueValores = array();
                                    $j = -1;
                                    $insertarBLoque = false; 
                                }
                                $j++;
                            }
                            $i++;
                            $exito = 1;
                        }

                        if($insertarBloque === true){
                            if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){    
                                //echo $escaneo->error;
                            }
                        }
                    }
                }
                //fin escaneo


                /*//inicio actualizar escaneo
                $lista_equipos_scaneados = $escaneo->listar_todo2($_SESSION['client_id'], $_SESSION['client_empleado']);
                foreach ($lista_equipos_scaneados as $reg_e) {
                    if(!$detalles->actualizar($_SESSION['client_id'], $_SESSION['client_empleado'], addslashes($reg_e["equipo"]), $reg_e["errors"])){
                        echo "Act Detalle: " . $detalles->error;
                    }
                }
                //fin actualizar escaneo*/

                $log->insertar(7, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue LAD");
            /*} else{
                $log->insertar(7, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue LAD");
            }*/
            //fin incremento

            $exito=1;  
        }
    }
}