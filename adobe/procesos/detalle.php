<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalenciasPDO.php");

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$tablaMaestra = new tablaMaestra();
$balance      = new balanceAdobe();
$detalles1    = new DetallesAdobe();
$resumen      = new resumenAdobe();
$equivalencia = new EquivalenciasPDO();
$general      = new General();

$total_i     = 0;
$vert1 = "Todo";
if(isset($_GET['vert'])){
    $vert  = $general->get_escape($_GET['vert']);
    if(isset($_GET['vert1'])){
        $vert1 = $general->get_escape($_GET['vert1']); 
    }
}
else{
    $vert  = 'Acrobat';
    //$vert1 = 'Profesional';
}

$asig = "";
if(isset($_GET['asig'])){
    $asig = $general->get_escape($_GET["asig"]);
}

$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]); //$detalles1->listar_asignacion($_SESSION["client_id"], $_SESSION["client_empleado"]);

if($vert != "Otros"){
    $ediciones = $equivalencia->edicionesProductoxNombre(1, $vert);
    /*if(!isset($_GET['vert1'])){
        $vert1 = $ediciones[0]["nombre"];
    }*/
    
    $titulo = $vert . ' ' . $vert1;
}
else{
    $balance->productosNoIncluir($_SESSION["client_id"], $_SESSION['client_empleado']); 
    $ediciones = $equivalencia->ProductoxNombre(1, $balance->listaNoIncluir);
    /*if(!isset($_GET['vert1'])){
        $vert1 = $ediciones[0]["nombre"];
    }*/
    
    $titulo = $vert1;
}

if($vert != "Otros" && $vert1 != "Todo"){   
    $result = $resumen->listar_datos6AgrupadoAsignacion($_SESSION['client_id'], $vert, $vert1, $asig, $asignaciones);
} else if($vert != "Otros" && $vert1 == "Todo"){   
    $result = $resumen->listar_datos6AgrupadoTodosAsignacion($_SESSION['client_id'], $vert, $asig, $asignaciones);
} else{   
    $result = $resumen->listar_datos5AgrupadoAsignacion($_SESSION['client_id'], $vert1, $asig, $asignaciones);
} 

if(count($result) > 0){
    $total_i = 0;
    foreach($result as $reg_calculo){
        $total_i += $reg_calculo["cantidad"];
    }	
}

if($vert != "Otros" && $vert1 != "Todo"){
    $listado = $resumen->listar_datos6Asignacion($_SESSION['client_id'], $vert, $vert1, $asig, $asignaciones);
} else if($vert != "Otros" && $vert1 == "Todo"){
    $listado = $resumen->listar_datos6TodosAsignacion($_SESSION['client_id'], $vert, $asig, $asignaciones);
} else{
    $listado = $resumen->listar_datos5Asignacion($_SESSION['client_id'], $vert1, $asig, $asignaciones);
}