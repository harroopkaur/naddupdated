<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_adobe.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_adobe.php");

$tablaMaestra = new tablaMaestra();
$detalles = new DetallesAdobe();
$balance  = new balanceAdobe();
$resumen  = new resumenAdobe();
$general = new General();
$compras = new comprasAdobe();

$tablaMaestra->productosSalida(1);
$total_1LAc       = 0;
$total_1InacLAc   = 0;
$total_2DVc       = 0;
$total_1LAs       = 0;
$total_1InacLAs   = 0;
$total_2DVs       = 0;
$reconciliacion1c = 0;
$reconciliacion1s = 0;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$total_1 = 0;
$total_2 = 0;
$total_3 = 0;
$total_4 = 0;
$total_5 = 0;
$total_6 = 0;
$tclient = 0;
$tserver = 0;
$activosc = 0;
$activoss = 0;

$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';

$vert = 0;
if (isset($_GET['vert']) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false) {
    $vert = $_GET['vert'];
}

$asig = "";
if (isset($_GET['asig'])) {
    $asig = $_GET['asig'];
}

$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

if ($vert == 0) {
    if ($detalles->listar_todo1Asignacion($_SESSION['client_id'], $asig, $asignaciones)) {
    //if ($detalles->listar_todo($_SESSION['client_id'], $_SESSION['client_empleado'])) {
        foreach ($detalles->lista as $reg_equipos) {

            if ($reg_equipos["tipo"] == 1) {//cliente
                $tclient = ($tclient + 1);
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAc = ($total_1LAc + 1);
                }

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVc = ($total_2DVc + 1);
                }
                else{
                    $total_1InacLAc++;
                }
            } else {//server
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAs = ($total_1LAs + 1);
                }

                $tserver = ($tserver + 1);

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVs = ($total_2DVs + 1);
                }
                else{
                    $total_1InacLAs++;
                }
            }
        }
        $reconciliacion1c = $total_2DVc - $total_1LAc;
        $reconciliacion1s = $total_2DVs - $total_1LAs;
    }
}//0


if ($vert == 1 || $vert == 3) {
    if ($detalles->listar_todo1Asignacion($_SESSION['client_id'], $asig, $asignaciones)) {
        foreach ($detalles->lista as $reg_equipos) {

            if ($reg_equipos["tipo"] == 1) {
                $tclient = ($tclient + 1);

                if ($reg_equipos["rango"] == 1) {
                    $total_1 = ($total_1 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activosc = ($activosc + 1);
                    }
                } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2 = ($total_2 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activosc = ($activosc + 1);
                    }
                } else {
                    $total_3 = ($total_3 + 1);
                }
            } else {//server
                $tserver = ($tserver + 1);

                if ($reg_equipos["rango"] == 1) {
                    $total_4 = ($total_4 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activoss = ($activoss + 1);
                    }
                } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_5 = ($total_5 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activoss = ($activoss + 1);
                    }
                } else {
                    $total_6 = ($total_6 + 1);
                }
            }
        }
    }
}//1

if ($vert == 2) {
    $balanza = true;
    
    //$TotalAdobeAcrobatCompras = 0;
    $TotalAdobeAcrobatUsar    = 0;
    $TotalAdobeAcrobatSinUsar = 0;
    
    //$TotalCreativeCloudCompras = 0;
    $TotalCreativeCloudUsar    = 0;
    $TotalCreativeCloudSinUsar = 0;
   
    //$TotalCreativeSuiteCompras = 0;
    $TotalCreativeSuiteUsar    = 0;
    $TotalCreativeSuiteSinUsar = 0;
    
    //$TotalOtrosCompras = 0;
    $TotalOtrosUsar    = 0;
    $TotalOtrosSinUsar = 0;

    $clientTotalProductCompras = 0;
    $clientTotalProductInstal  = 0;
    $clientProductNeto         = 0;
    
    //Acrobat
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Acrobat")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalAdobeAcrobatCompras += $reg_equipos["compra"];
            //$TotalAdobeAcrobatInstal  += $reg_equipos["instalaciones"];
        }
    }  
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], "Acrobat")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalAdobeAcrobatUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalAdobeAcrobatSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalAdobeAcrobatInstal = $TotalAdobeAcrobatUsar + $TotalAdobeAcrobatSinUsar;
    $AdobeAcrobatNeto = $TotalAdobeAcrobatCompras - $TotalAdobeAcrobatInstal;*/
    
    $listar_Acrobat = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Acrobat", "Todo", $asig, $asignaciones); 
    foreach ($listar_Acrobat as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalAdobeAcrobatUsar += $reg_equipos["conteo"];
        } else{
            $TotalAdobeAcrobatSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalAdobeAcrobatCompras = $compras->totalCompras($_SESSION['client_id'], "Acrobat", "Todo", $asig, $asignaciones);
    $AdobeAcrobatNeto = $TotalAdobeAcrobatCompras - ($TotalAdobeAcrobatUsar + $TotalAdobeAcrobatSinUsar);
    
    //Creative Cloud
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Creative Cloud")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalCreativeCloudCompras += $reg_equipos["compra"];
            //$TotalCreativeCloudInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], "Creative Cloud")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalCreativeCloudUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalCreativeCloudSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }

    $TotalCreativeCloudInstal = $TotalCreativeCloudUsar + $TotalCreativeCloudSinUsar;
    $CreativeCloudNeto         = $TotalCreativeCloudCompras - $TotalCreativeCloudInstal;
    */
    
    $listar_CreativeCloud = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Creative Cloud", "Todo", $asig, $asignaciones); 
    foreach ($listar_CreativeCloud as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalCreativeCloudUsar += $reg_equipos["conteo"];
        } else{
            $TotalCreativeCloudSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalCreativeCloudCompras = $compras->totalCompras($_SESSION['client_id'], "Creative Cloud", "Todo", $asig, $asignaciones);
    $CreativeCloudNeto = $TotalCreativeCloudCompras - ($TotalCreativeCloudUsar + $TotalCreativeCloudSinUsar);
    
    
    
    //Creative Suite
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Creative Suite")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalCreativeSuiteCompras += $reg_equipos["compra"];
            //$TotalCreativeSuiteInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], "Creative Suite")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalCreativeSuiteUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalCreativeSuiteSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
  
    $TotalCreativeSuiteInstal = $TotalCreativeSuiteUsar + $TotalCreativeSuiteSinUsar;
    $CreativeSuiteNeto         = $TotalCreativeSuiteCompras - $TotalCreativeSuiteInstal;*/
    
    $listar_CreativeSuite = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Creative Suite", "Todo", $asig, $asignaciones); 
    foreach ($listar_CreativeSuite as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalCreativeSuiteUsar += $reg_equipos["conteo"];
        } else{
            $TotalCreativeSuiteSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalCreativeSuiteCompras = $compras->totalCompras($_SESSION['client_id'], "Creative Suite", "Todo", $asig, $asignaciones);
    $CreativeSuiteNeto = $TotalCreativeSuiteCompras - ($TotalCreativeSuiteUsar + $TotalCreativeSuiteSinUsar);
    
    //Otros
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Otros")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalOtrosCompras += $reg_equipos["compra"];
            //$TotalCreativeSuiteInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], "Otros")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalOtrosUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalOtrosSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
  
    $TotalOtrosInstal = $TotalOtrosUsar + $TotalOtrosSinUsar;
    $OtrosNeto        = $TotalOtrosCompras - $TotalOtrosInstal;*/
    
    $listar_otros = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Otros", "Todo", $asig, $asignaciones); 
    foreach ($listar_otros as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalOtrosUsar += $reg_equipos["conteo"];
        } else{
            $TotalOtrosSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalOtrosCompras = $compras->totalCompras($_SESSION['client_id'], "Otros", "Todo", $asig, $asignaciones);
    $OtrosNeto = $TotalOtrosCompras - ($TotalOtrosUsar + $TotalOtrosSinUsar);
}