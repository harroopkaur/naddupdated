<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS['app_root1'] . "/Cloud/procesos/resultados.php");

ob_start();
$html = ob_get_clean();
$html = utf8_encode($html);
$html .= '<style>
        .tituloSAMDiagnostic1{
            font-size:35px;
            text-align: center;
            font-weight: bold;
            color: #000000;
        }
            
        .tablapS {     
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
            font-size: 15px;   
            margin: 0 auto;   
            width: 90%; 
            text-align: left;    
            border-collapse: collapse; 
        }

        .tablapS thead th {     
            font-size: 25px;     
            font-weight: bold;     
            padding: 15px;     
            background: #377EBA;
            border-bottom: 2px solid #fff;   
            color: #ffffff; 
            text-align: center; 
        }

        .tablapS thead th.transTopLeft{
            background-color: transparent;
        }

        .tablapS thead th.trans{
            background-color: transparent;
        }

        .tablapS tbody td.trans{
            background-color: transparent;
            font-size: 20px;
        }

        .tablapS tbody td.trans1{
            background-color: transparent;
            border: 0;
        }

        .tablapS tbody td.trans2{
            background-color: transparent;
            border: 0;
            font-size: 20px;
        }

        .tablapS tbody td.titulo{
            font-size: 20px;
        }

        .tablapS tbody th {  
            padding: 8px;     
            background: #5C96C6;     
            border: 2px solid #fff;
            color: #000000;  
            text-align: center;
            vertical-align: middle;
            font-weight: normal;
        }

        .tablapS tbody td {    
            padding: 8px;     
            background: #A0C3DF;     
            border: 2px solid #fff;
            color: #000000;    
            text-align: center;
            vertical-align: middle;
        }

        .tablapS tbody td.sinBottom{
            border-bottom: 0;
        }
        
        .bold{
            font-weight: bold;
        }
    </style>
    
    <div style="width:100%; height:auto; overflow:hidden;" id="contPOC">
    <h1 class="tituloSAMDiagnostic1 colorBlanco">Cloud Assessment Report</h1>

    <br>
    <table class="tablapS">
        <thead>
            <tr>
                <th>Category</th>
                <th>Metric</th>
                <th>Description</th>
                <th>Result</th>
                <th colspan="3">Details</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold" rowspan="2">Overview</td>
                <td>Active Devices</td>
                <td>> 90% Active Devices</td>
                <td><img src="';
                    if($primero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; }
                $html .= '"></td>
                <td colspan="2">Active Devices: ' . $porcActivos . '%</td>
                <td>Devices: ' . $totalEquipos . '</td>
            </tr>
            <tr>
                <td>Discovery</td>
                <td>> 90% Discovery</td>
                <td><img src="';
                    if($segundo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="2">Scanned Devices: ' . $porcLevantado . '%</td>
                <td>Active Devices: ' . $totalEquiposActivos . '</td>
            </tr>
            <tr>
                <th rowspan="6" style="font-weight: bold;">Cloud Ready</th>
                <th rowspan="2" >Office 365</th>
                <th>Office</th>
                <th><img src="';
                    if($undecimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th colspan="2">E1: ' . $totalOfficeStandard . '</th>
                <th>E3: ' . $totalOfficeProfessional . '</th>
            </tr>
            <tr>
                <th>Project & Visio</th>
                <th><img src="';
                    if($duodecimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th colspan="2">E1: ' . $totalVisioProjectStandard . '</th>
                <th>E3: ' . $totalVisioProjectProfessional . '</th>
            </tr>
            <tr>
                <th rowspan="4" >Azure</th>
                <th>Windows Server: Newer</th>
                <th><img src="';
                    if($decimotercero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; }
                $html .= '"></th>
                <th style="width:165px;">Basic: ' . $MSCloudVMBasicNewer . '</th>
                <th style="width:165px;">Low Priority: ' . $MSCloudVMLowPriorityNewer . '</th>
                <th style="width:165px;">Standard: ' . $MSCloudVMStandardNewer . '</th>
            </tr>
            <tr>
                <th>Windows Server: Older</th>
                <th><img src="';
                    if($decimocuarto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th>Basic: ' . $MSCloudVMBasicOlder . '</th>
                <th>Low Priority: ' . $MSCloudVMLowPriorityOlder . '</th>
                <th>Standard: ' . $MSCloudVMStandardOlder . '</th>
            </tr>
            <tr>
                <th>SQL Server: Newer</th>
                <th><img src="';
                    if($decimoquinto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th>Basic: ' . $MSCloudDBBasicNewer . '</th>
                <th>Standard: ' . $MSCloudDBStandardNewer . '</th>
                <th>Premium: ' . $MSCloudDBPremiumNewer . '</th>
            </tr>
            <tr>
                <th>SQL Server: Older</th>
                <th><img src="';
                    if($decimosexto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th>Basic: ' . $MSCloudDBBasicOlder . '</th>
                <th>Standard: ' . $MSCloudDBStandardOlder . '</th>
                <th>Premium: ' . $MSCloudDBPremiumOlder . '</th>
            </tr>
            <tr>
                <td class="bold" rowspan="6">Optimization & Cybersecurity</td>
                <td>Unused Devices</td>
                <td>1 or more unused devices detected</td>
                <td><img src="';
                    if($tercero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="3">Unused Devices: ' . $UnusedDevice . '</td>
            </tr>
            <tr>
                <td>Unused Software</td>
                <td>1 or more unused software detected</td>
                <td><img src="'; 
                    if($cuarto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="2">Detected: ' . $unusedSW . '</td>
                <td>Estimated: ' . $porcEstApplications . '</td>
            </tr>
            <tr>
                <td>Duplicate Installations</td>
                <td>1 or more duplicate installations detected</td>
                <td><img src="';
                    if($quinto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="2">Detected: ' . $softwareDuplicate . '</td>
                <td>Estimated: ' . $porcEstDuplicate . '</td>
            </tr>
            <tr>
                <td>Erroneus Installations</td>
                <td>1 or more erroneus installations detected</td>
                <td><img src="'; 
                    if($sexto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="2">Detected: ' . $erroneus . '</td>
                <td>Estimated: ' . $porcEstErroneus . '</td>
            </tr>
            <tr>
                <td rowspan="2">Unsupported Software</td>
                <td>Clients</td>
                <td><img src="'; 
                    if($noveno == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="3">' . $softCliente . '</td>
            </tr>
            <tr>
                <td>Servers</td>
                <td><img src="'; 
                    if($decimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="3">' . $softServidor . '</td>
            </tr>
            <tr>
                <th colspan="3" style="font-weight: bold;">Assessment</th>
                <th><img src="';
                    if($fallas < 3){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } 
                    else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                    $html .= '"></th>
                <th colspan="3" style="font-weight: bold;">';
                    if($fallas <= 4){ $html .= 'Low ROI Expected'; } 
                    else if($fallas > 4 && $fallas <= 8){ $html .= 'Medium ROI Expected'; } else{ $html .= 'High ROI Expected'; } 
                $html .= '</th>
            </tr>
        </tbody>
    </table>
</div>

<br>

<table style="width:100%;">
    <tr>
        <td style="width:13%;">' . date("m/d/Y") . '</td>
        <td style="width:73%;text-align:center;">https://www.licensingassurance.com/Cloud/</td>
        <td style="width:13%;"><img style="height:60px;" align="right" src="../img/LA_Logo.png"></img></td>
    </tr>
</table>';
                
include($GLOBALS["app_root1"] . "/mpdf60/mpdf.php");
$mpdf = new mPDF('', 'A3-L', 0, '', 15, 15, 16, 16, 15, 9, 'L');
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in = "UTF-8";
$mpdf->WriteHTML($html);
$mpdf->output();
exit();