<?php
$_SESSION["idioma"] = 2;

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validar_procesoLAE_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_LAE.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_eliminar_equipos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_actualizar_LAE.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_actualizar_escaneo.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");


// Objetos
$general = new General();
$validadorGeneralLAE = new clase_validar_procesoLAE_general();
$procLAE = new clase_procesar_LAE();
$procesoElimArchivo = new clase_procesar_eliminar_equipos();
$procesoActualizarLAE = new clase_procesar_actualizar_LAE();
$procActualEscaneo = new clase_procesar_actualizar_escaneo();
$log = new log();

// Variables
$nombFabricante = "Cloud";
$fabricante = 3;
$archivoLAE = "";
$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$opcionDespliegue = "completo";
$incremArchivo = 0;

$idDiagnostic = $idDiagnosticLAE = $idDiagnosticResult = 0;
if(isset($_POST["idDiagnostic"]) && filter_var($_POST["idDiagnostic"], FILTER_VALIDATE_INT) !== false){
    $idDiagnostic = $idDiagnosticLAE = $_POST["idDiagnostic"];
}

$datosAux = array();
if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
        $nombArchivo = $_FILES['archivo']['name'];
        $temp = $_FILES['archivo']['tmp_name'];
        $error2 = $validadorGeneralLAE->validar_archivo($nombArchivo, "csv");

        if($error2 == 0){                
            $error2 = $validadorGeneralLAE->validarLAE($temp, $opcionDespliegue, $nombFabricante, $idDiagnostic);
        }
    } else{
        $error2 = -1;
    }

    if ($error2 == 0) {
        $fechaDespliegue = $general->get_escape($_POST["fechaDespliegue"]);
        //inicio agregar LAE
        $iDN = $validadorGeneralLAE->iDN;
        $iobjectClass = $validadorGeneralLAE->iobjectClass;
        $icn = $validadorGeneralLAE->icn;
        $iuserAccountControl = $validadorGeneralLAE->iuserAccountControl;
        $ilastLogon = $validadorGeneralLAE->ilastLogon;
        $ipwdLastSet = $validadorGeneralLAE->ipwdLastSet;
        $ioperatingSystem = $validadorGeneralLAE->ioperatingSystem;
        $ilastLogonTimestamp = $validadorGeneralLAE->ilastLogonTimestamp;
        $iWhenCreated = $validadorGeneralLAE->iWhenCreated;
        $procesarLAE = $validadorGeneralLAE->procesarLAE;

        $procLAE->moverArchivoLAE(0, 0, $temp, $incremArchivo, $opcionDespliegue, $nombFabricante, $idDiagnostic);

        $procLAE->procesarFilePCs($iDN, $iobjectClass, $icn, $iuserAccountControl, $ilastLogon, $ipwdLastSet, $ioperatingSystem, 
        $ilastLogonTimestamp, $iWhenCreated, $procesarLAE, $opcionDespliegue);
        //fin agregar LAE

        //inicio actualizar LAE
        $procesoActualizarLAE->procesarActualizarLAE(0, 0, $fechaDespliegue, 
        $opcionDespliegue, $incremArchivo, $nombFabricante, $_SESSION["idioma"], $idDiagnostic);
        //fin actualizar LAE

        //inicio actualizar escaneo
        $procActualEscaneo->procesarActualizacionEscaneo(0, $nombFabricante, $idDiagnostic);
        //fin actualizar escaneo   

        $exito2 = 1;
    }
}

/*// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/Cloud/clases/clase_detalles_equipo.php");
require_once($GLOBALS["app_root1"] . "/Cloud/clases/clase_filepcs.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_conversion.php");
require_once($GLOBALS["app_root1"] . "/Cloud/clases/clase_escaneo.php");
require_once($GLOBALS["app_root1"] . "/Cloud/clases/clase_consolidado_procesadores.php");
require_once($GLOBALS["app_root1"] . "/Cloud/clases/clase_consolidado_tipo_equipo.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root1"] . "/Cloud/clases/MSCloud.php");

// Objetos
$detalles     = new DetallesMSCloud();
$tablac       = new TablaC();
$filec        = new FilepcMSCloud();
$general      = new General();
$conversion   = new TablaC();
$escaneo      = new ScaneoMSCloud();
$procesadores = new ConsolidadoProcMSCloud();
$tipoEquipo   = new ConsTipoEquipoMSCloud();
$config       = new configuraciones();
$cabecera = new MSCloud();
$fabricante = 3;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;

$listaEdiciones = $config->listar_edicionTotal();
$listaVersiones = $config->listar_versionTotal();

$datosAux = array();

$idDiagnostic = $idDiagnosticLAE = $idDiagnosticResult = 0;
if(isset($_POST["idDiagnostic"]) && filter_var($_POST["idDiagnostic"], FILTER_VALIDATE_INT) !== false){
    $idDiagnostic = $idDiagnosticLAE = $_POST["idDiagnostic"];
}

$archivoEscaneo = "";
if(isset($_POST["escaneo"])){
    $archivoEscaneo = $general->get_escape($_POST["escaneo"]);
}

$archivoProcesadores = "";
if(isset($_POST["procesadores"])){
    $archivoProcesadores = $general->get_escape($_POST["procesadores"]);
}

$archivoEquipos = "";
if(isset($_POST["tipoEquipo"])){
    $archivoEquipos = $general->get_escape($_POST["tipoEquipo"]);
}

if($idDiagnostic == 0){
    $error2 = -2;
}

if ($error2 == 0 && isset($_POST['insertar']) && $_POST["insertar"] == 1){
    if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones
        if ($nombre_imagen != "") {
            $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
                $error2 = 1;
            }  // Permitir subir solo imagenes JPG,
        } else {
            $error2 = 2;
        }

        $baseLAD = $GLOBALS["app_root1"] . "/Cloud/archivosLAD/";
        $base = $GLOBALS["app_root1"] . "/Cloud/archivosLAE/";
        $imagen = "LAE_Output" . date("dmYHis") . ".csv";

        if (!$filec->cargar_archivo($imagen, $temporal_imagen)) {
            $error2 = 5;
        } else {
            if($general->obtenerSeparadorLAE_Output($base . $imagen) === true){
                if (($fichero = fopen($base . $imagen, "r")) !== false) {
                    $i = 1;
                    $iDN = $iobjectClass = $icn = $iuserAccountControl = $ilastLogon = $ipwdLastSet = $ioperatingSystem
                    = $ioperatingSystemVersion = $ilastLogonTimestamp = -1;
                    $procesarLAE = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        for($k = 0; $k < count($datos); $k++){
                            if(trim(utf8_encode($datos[$k])) == "DN"){
                                $iDN = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "objectClass"){
                                $iobjectClass = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "cn"){
                                $icn = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "userAccountControl"){
                                $iuserAccountControl = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "lastLogon"){
                                $ilastLogon = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "pwdLastSet"){
                                $ipwdLastSet = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "operatingSystem"){
                                $ioperatingSystem = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "operatingSystemVersion"){
                                $ioperatingSystemVersion = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "lastLogonTimestamp"){
                                $ilastLogonTimestamp = $k;
                            }
                        }
                        
                        $i++;
                        break;
                    }
                    fclose($fichero);
                    
                    if($iDN == -1 && $iobjectClass == -1 && $icn == -1 && $iuserAccountControl == -1 && $ilastLogon == -1 &&
                    $ipwdLastSet == -1 && $ioperatingSystem == -1 && $ioperatingSystemVersion == -1 && $ilastLogonTimestamp == -1){
                        $procesarLAE = false;
                    }
                }
            } else{
                //$error2 = 6;
                $procesarLAE = false;
            }
            
            if($general->obtenerSeparador($baseLAD . $archivoEscaneo) === true){
                if (($fichero = fopen($baseLAD . $archivoEscaneo, "r")) !== false) {
                    $i=1;
                    $jHostname = $jStatus = $jError = -1;
                    $procesarEscaneo = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Hostname"){
                                    $jHostname = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Status"){
                                    $jStatus = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Error"){
                                    $jError = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }
                       
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($jHostname == -1 && $jStatus == -1 && $jError == -1){
                        $procesarEscaneo = false;
                    } 
                }
            } else{
                //$error = 11;
                $procesarEscaneo = false;
            }

            if($general->obtenerSeparador($baseLAD . $archivoProcesadores) === true){    
                if (($fichero = fopen($baseLAD . $archivoProcesadores, "r")) !== false) {
                    $i=1;
                    $kDatoControl = $kHostName = $kTipoCPU = $kCPUs = $kCores = $kProcesadoresLogicos = $kTipoEscaneo = -1;
                    $procesarProcesadores = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                    $kDatoControl = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "HostName"){
                                    $kHostName = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Tipo de CPU"){
                                    $kTipoCPU = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "CPUs"){
                                    $kCPUs = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "Cores"){
                                    $kCores = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "Procesadores Lógicos" || trim(utf8_encode($datos[$k])) == "Procesadores Logicos"){
                                    $kProcesadoresLogicos = $k;
                                }
                                
                                if(strpos(trim(utf8_encode($datos[$k])), "Tipo de Escaneo") !== false){
                                    $kTipoEscaneo = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }
                       
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($kDatoControl == -1 && $kHostName == -1 && $kTipoCPU == -1 && $kCPUs == -1 && $kCores == -1 
                    && $kProcesadoresLogicos == -1 && $kTipoEscaneo == -1){
                        $procesarProcesadores = false;
                    }
                }
            } else{
                //$error = 12;
                $procesarProcesadores = false;
            }

            if($general->obtenerSeparador($baseLAD . $archivoEquipos) === true){
                if (($fichero = fopen($baseLAD . $archivoEquipos, "r")) !== false) {
                    $i=1;
                    $lDatoControl = $lHostName = $lFabricante = $lModelo = -1;
                    $procesarEquipos = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                    $lDatoControl = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "HostName"){
                                    $lHostName = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Fabricante"){
                                    $lFabricante = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "Modelo"){
                                    $lModelo = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }                        
                       
                        
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($lDatoControl == -1 && $lHostName == -1 && $lFabricante == -1 && $lModelo == -1){
                        $procesarEquipos = false;
                    }
                }
            } else{
                //$error = 13;
                $procesarEquipos = false;
            }
        }
    } else{
        $error2 = -1;
    }

    if ($error2 == 0) {
        $cabecera->actualizarLAE($idDiagnostic, $imagen);
        
        $filec->eliminar($idDiagnostic);
        
        if($general->obtenerSeparadorLAE_Output($base . $imagen) === true && $procesarLAE === true){
            if (($fichero = fopen($base . $imagen, "r")) !== false) {
                $i = 1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                    $existeEquipo = $filec->existeEquipo($idDiagnostic, $general->truncarString($datos[$icn], 250)); 
                    if ($i > 1 && $existeEquipo == 0) {
                        $sistema = $datos[$ioperatingSystem];

                        if ($conversion->codigo_existe($datos[$ioperatingSystem], 0)) {
                            $conversion->datos2($datos[$ioperatingSystem]);
                            $sistema = $conversion->os;
                        }

                        if($j == 0){
                            $insertarBloque = true;
                            $bloque .= "(:idDiagnostic" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                            . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                            . ":os" . $j . ", :lastlogontimes" . $j . ")";
                        } else {
                            $bloque .= ", (:idDiagnostic" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                            . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                            . ":os" . $j . ", :lastlogontimes" . $j . ")";
                        } 
                        
                        $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                        $bloqueValores[":dn" . $j] = $general->truncarString($datos[$iDN], 250);
                        $bloqueValores[":objectclass" . $j] = $general->truncarString($datos[$iobjectClass], 250);
                        $bloqueValores[":cn" . $j] = $general->truncarString($datos[$icn], 250);
                        $bloqueValores[":useracountcontrol" . $j] = $general->truncarString($datos[$iuserAccountControl], 250);
                        $bloqueValores[":lastlogon" . $j] = $general->truncarString($datos[$ilastLogon], 250);
                        $bloqueValores[":pwdlastset" . $j] = $general->truncarString($datos[$ipwdLastSet], 250);
                        $bloqueValores[":os" . $j] = $general->truncarString($sistema, 250);
                        $bloqueValores[":lastlogontimes" . $j] = $general->truncarString($datos[$ilastLogonTimestamp], 250);

                        if($j == $general->registrosBloque){
                            if(!$filec->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo "LAE: " . $filec->error;
                            }
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    } else if ($i > 1 && $existeEquipo > 0) {
                        if(!$filec->actualizarEquipo($idDiagnostic, $general->truncarString($datos[$iDN], 250), $general->truncarString($datos[$iobjectClass], 250),
                        $general->truncarString($datos[$icn], 250), $general->truncarString($datos[$iuserAccountControl], 250), $general->truncarString($datos[$ilastLogon], 250), 
                        $general->truncarString($datos[$ipwdLastSet], 250), $general->truncarString($sistema, 250), $general->truncarString($datos[$ilastLogonTimestamp], 250))){
                            echo "LAE: " . $filec->error;
                        } 
                    }
                    
                    $i++;
                    $exito2 = 1;
                } 
                
                if($insertarBloque === true){
                    if(!$filec->insertarEnBloque($bloque, $bloqueValores)){
                        echo "LAE: " . $filec->error;
                    }
                }
                
                $filec->eliminarEquiposDespliegue($idDiagnostic);
            }
        }

        if($error2 == 0){
            //inicio actualizar filepcs
            $detalles->eliminar($idDiagnostic);
            
            $lista_todos_files = $filec->listar_todo($idDiagnostic);

            if ($lista_todos_files) {
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                
                $fechaDespliegue = strtotime('now');
                
                if($general->validarFecha($_POST["fechaDespliegue"], "/", "mm/dd/aaaa")){
                    $fechaDespliegue = strtotime($general->cambiarfechasFormato($_POST["fechaDespliegue"]));
                }
               
                foreach ($lista_todos_files as $reg_f) {

                    $host = explode('.', $reg_f["cn"]);
                    if(filter_var($reg_f["cn"], FILTER_VALIDATE_IP)!== false){
                        $host[0] = $reg_f["cn"];
                    }

                    $sistema = $reg_f["os"];

                    if ($reg_f["lastlogon"] != "") {

                        $value = round(($reg_f["lastlogon"] - 116444735995904000) / 10000000);
                        $fecha1 = date('d/m/Y', $value);

                        $dias1 = $general->daysDiff($value, $fechaDespliegue);
                    } else {
                        $dias1 = NULL;
                    }

                    if ($reg_f["pwdlastset"] != "") {
                        $value2 = round(($reg_f["pwdlastset"] - 116444735995904000) / 10000000);
                        $fecha2 = date('d/m/Y', $value2);

                        $dias2 = $general->daysDiff($value2, $fechaDespliegue);
                    } else {
                        $dias2 = NULL;
                    }
                    if ($reg_f["lastlogontimes"] != "") {

                        $value3 = round(($reg_f["lastlogontimes"] - 116444735995904000) / 10000000);
                        $fecha3 = date('d/m/Y', $value3);

                        $dias3 = $general->daysDiff($value3, $fechaDespliegue);
                    } else {
                        $dias3 = NULL;
                    }

                    $minimos = $general->minimo($dias1, $dias2, $dias3);

                    $minimor = round(abs($minimos), 0);

                    if ($minimor <= 30) {
                        $minimo = 1;
                    } else if ($minimor <= 60) {
                        $minimo = 2;
                    } else if ($minimor <= 90) {
                        $minimo = 3;
                    } else if ($minimor <= 365) {
                        $minimo = 4;
                    } else {
                        $minimo = 5;
                    }

                    if ($minimo < 4) {
                        $activo = 1;
                    } else {
                        $activo = 0;
                    }

                    $tipos = $general->search_server($reg_f["os"]);

                    $tipo = $tipos[0];
                    $familia = "";
                    if(strpos($sistema, "Windows Server") !== false || (strpos($sistema, "Windows") !== false && strpos($sistema, "Server") !== false)){
                        $familia = "Windows Server";
                    }
                    else if(strpos($sistema, "Windows") !== false){
                        $familia = "Windows"; 
                    }

                    $edicion = "";
                    foreach($listaEdiciones as $rowEdiciones){
                        if(trim($rowEdiciones["nombre"]) != "" && strpos($sistema, trim($rowEdiciones["nombre"])) !== false && strlen(trim($rowEdiciones["nombre"])) > strlen($edicion)){
                             $edicion = trim($rowEdiciones["nombre"]);    
                        }
                    }
                    if($edicion == "" || ($familia == "Windows Server" && $edicion == "Server") || $edicion == "Windows 2000 Server"){
                        $edicion = "Standard";
                    }

                    $version = "";
                    foreach($listaVersiones as $rowVersiones){
                        if(trim($rowVersiones["nombre"]) != "" && strpos($sistema, trim($rowVersiones["nombre"])) !== false && strlen(trim($rowVersiones["nombre"])) > strlen($version)){
                             $version = trim($rowVersiones["nombre"]);    
                        }
                    } 

                    if($familia == ""){
                        $edicion = "";
                        $version = "";
                    }
                    
                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:idDiagnostic" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                        . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                        . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
                    } else {
                        $bloque .= ", (:idDiagnostic" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                        . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                        . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
                    } 
                   
                    $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                    $bloqueValores[":equipo" . $j] = $host[0];
                    $bloqueValores[":os" . $j] = $sistema;
                    $bloqueValores[":familia" . $j] = $familia;
                    $bloqueValores[":edicion" . $j] = $edicion;
                    $bloqueValores[":version" . $j] = $version;
                    $bloqueValores[":dias1" . $j] = $dias1;
                    $bloqueValores[":dias2" . $j] = $dias2;
                    $bloqueValores[":dias3" . $j] = $dias3;
                    $bloqueValores[":minimo" . $j] = $minimor;
                    $bloqueValores[":activo" . $j] = $activo;
                    $bloqueValores[":tipo" . $j] = $tipo;
                    $bloqueValores[":rango" . $j] = $minimo;

                    if($j == $general->registrosBloque){
                        if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){ 
                            echo "Detalles: " . $detalles->error;
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }//foreach
                
                if($insertarBloque === true){
                    if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){   
                        echo "Detalles: " . $detalles->error;
                    }
                }
            }//if
            //fin actualizar filepcs
            
            $escaneo->eliminar($idDiagnostic);
            
            if($general->obtenerSeparador($baseLAD . $archivoEscaneo) === true && $procesarEscaneo === true){   
                if (($fichero = fopen($baseLAD . $archivoEscaneo, "r")) !== false) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;
                
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if ($i > 2) {
                            $host = explode('.', $datos[$jHostname]);
                            if(filter_var($datos[$jHostname], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[$jHostname];
                            }
                            
                            if($j == 0){
                            $insertarBloque = true;
                                $bloque .= "(:idDiagnostic" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                            } else {
                                $bloque .= ", (:idDiagnostic" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                            } 

                            $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                            $bloqueValores[":equipo" . $j] = $host[0];
                            $bloqueValores[":status" . $j] = $datos[$jStatus];
                            $bloqueValores[":errors" . $j] = $datos[$jError];

                            if($j == $general->registrosBloque){
                                if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo "Escaneo: " . $escaneo->error;
                                }
                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                            
                        }
                        $i++;
                    }
                    
                    if($insertarBloque === true){
                        if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){   
                            echo "Escaneo: " . $escaneo->error;
                        }
                    }
                    $exito2 = 1;
                }
            }
            //fin escaneo


            //inicio actualizar escaneo
            $lista_equipos_scaneados = $escaneo->listar_todo2($idDiagnostic);
            foreach ($lista_equipos_scaneados as $reg_e) {
                if(!$detalles->actualizar($idDiagnostic, $reg_e["equipo"], $reg_e["errors"])){
                   echo $detalles->error;
                }
            }
            //fin actualizar escaneo

            //inicio cargar consolidado procesadores
            $procesadores->eliminar($idDiagnostic);
            
            if($general->obtenerSeparador($baseLAD . $archivoProcesadores) === true && $procesarProcesadores === true){   
                if (($fichero = fopen($baseLAD . $archivoProcesadores, "r")) !== false) {
                    $i=1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        $datosAux[0] = "";
                        $datosAux[1] = "";
                        $datosAux[2] = "";
                        $datosAux[3] = 0;  
                        $datosAux[4] = 0;
                        $datosAux[5] = 0;
                        $datosAux[6] = "";
                        if($i!=1){       
                            if(isset($datos[$kDatoControl]) && $datos[$kDatoControl] != 'Dato de Control' && isset($datos[$kTipoEscaneo]) && $datos[$kTipoEscaneo] != ""){
                                $datosAux[0] = $datos[$kDatoControl];

                                $host = explode('.', $datos[$kHostName]);
                                if(filter_var($datos[$kHostName], FILTER_VALIDATE_IP)!== false){
                                    $host[0] = $datos[$kHostName];
                                }

                                if(isset($datos[$kTipoCPU])){
                                    $datosAux[2] = $datos[$kTipoCPU];
                                }

                                if(isset($datos[$kCPUs]) && filter_var($datos[$kCPUs], FILTER_VALIDATE_INT) !== false){
                                    $datosAux[3] = $datos[$kCPUs];
                                }

                                if(isset($datos[$kCores]) && filter_var($datos[$kCores], FILTER_VALIDATE_INT) !== false){
                                    $datosAux[4] = $datos[$kCores];
                                }

                                if(isset($datos[$kProcesadoresLogicos]) && filter_var($datos[$kProcesadoresLogicos], FILTER_VALIDATE_INT) !== false){
                                    $datosAux[5] = $datos[$kProcesadoresLogicos];
                                }

                                if(isset($datos[$kTipoEscaneo])){
                                    $datosAux[6] = $datos[$kTipoEscaneo];
                                }

                                if($datosAux[0] != "" && isset($host[0]) && $host[0] != "" && $datosAux[2] != "" && $datosAux[3] != "" && $datosAux[4] != "" && $datosAux[5] != "" && $datosAux[6] != ""){                                
                                    
                                    if($j == 0){
                                    $insertarBloque = true;
                                        $bloque .= "(:idDiagnostic" . $j . ", :dato_control" . $j . ", "
                                        . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                        . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                                    } else {
                                        $bloque .= ", (:idDiagnostic" . $j . ", :dato_control" . $j . ", "
                                        . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                        . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                                    } 

                                    $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                                    $bloqueValores[":dato_control" . $j] = $datosAux[0];
                                    $bloqueValores[":host_name" . $j] = $host[0];
                                    $bloqueValores[":tipo_CPU" . $j] = $datosAux[2];
                                    $bloqueValores[":cpu" . $j] = $datosAux[3];
                                    $bloqueValores[":cores" . $j] = $datosAux[4];
                                    $bloqueValores[":procesadores_logicos" . $j] = $datosAux[5];
                                    $bloqueValores[":tipo_escaneo" . $j] = $datosAux[6];

                                    if($j == $general->registrosBloque){
                                        if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){ 
                                            echo "Procesadores: " . $procesadores->error;
                                        }
                                        $bloque = "";
                                        $bloqueValores = array();
                                        $j = -1;
                                        $insertarBLoque = false; 
                                    }
                                    $j++;
                                }
                            }
                        }
                        $i++;
                    }

                    if($insertarBloque === true){
                        if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){   
                            echo "Procesadores: " . $procesadores->error;
                        }
                    }

                    $exito2=1;	
                }
            }
            //fin cargar consolidado procesadores

            //inicio cargar consolidado tipo de equipo
            $tipoEquipo->eliminar($idDiagnostic);
            
            if($general->obtenerSeparador($baseLAD . $archivoEquipos) === true && $procesarEquipos === true){   
                if (($fichero = fopen($baseLAD . $archivoEquipos, "r")) !== false) {
                    $i=1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    while (($datos = fgetcsv($fichero, 2000, $general->separador)) !== false) {
                        if($i!=1){
                            $datosAux[0] = "";
                            $datosAux[1] = "";
                            $datosAux[2] = "";
                            $datosAux[3] = "";  
                            if(isset($datos[$lDatoControl])){
                                $datosAux[0] = $datos[$lDatoControl];
                            }
                            if($datosAux[0] != 'Dato de Control'){
                                $host = explode('.', $datos[$lHostName]);
                                if(filter_var($datos[$lHostName], FILTER_VALIDATE_IP)!== false){
                                    $host[0] = $datos[$lHostName];
                                }

                                if(isset($datos[$lFabricante])){
                                    $datosAux[2] = $datos[$lFabricante];
                                }
                                if(isset($datos[$lModelo])){
                                    $datosAux[3] = $datos[$lModelo];
                                }

                                if($j == 0){
                                $insertarBloque = true;
                                    $bloque .= "(:idDiagnostic" . $j . ", :dato_control" . $j . ", "
                                    . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                                } else {
                                    $bloque .= ", (:idDiagnostic" . $j . ", :dato_control" . $j . ", "
                                    . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                                } 

                                $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                                $bloqueValores[":dato_control" . $j] = $datosAux[0];
                                $bloqueValores[":host_name" . $j] = $host[0];
                                $bloqueValores[":fabricante" . $j] = $datosAux[2];
                                $bloqueValores[":modelo" . $j] = $datosAux[3];

                                if($j == $general->registrosBloque){
                                    if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){ 
                                        echo "Tipo Equipo: " . $tipoEquipo->error;
                                    }
                                    $bloque = "";
                                    $bloqueValores = array();
                                    $j = -1;
                                    $insertarBLoque = false; 
                                }
                                $j++;
                            }
                        }
                        $i++;
                    }

                    if($insertarBloque === true){
                        if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){   
                            echo "Tipo Equipo: " . $tipoEquipo->error;
                        }
                    }

                    $exito2=1;	
                }
            }
            //fin cargar consolidado tipo de equipo
        }
    }
}*/