<?php
// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_email.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resultados_general.php");

// Objetos
$general = new General();
$claseResultadosGeneral = new clase_resultados_general();

// Variables
$error = 0;
$idDiagnostic = 0;
if(isset($_POST["idDiagnostic"]) && filter_var($_POST["idDiagnostic"], FILTER_VALIDATE_INT)){
    $idDiagnostic = $_POST["idDiagnostic"];
}

$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $general->get_escape($_POST["nombre"]);
}

$email = "";
if(isset($_POST["email"])){
    $email = $general->get_escape($_POST["email"]);
}

$reportePDF = 0;
if(isset($_POST["reportePDF"]) && filter_var($_POST["reportePDF"], FILTER_VALIDATE_INT) !== false){
    $reportePDF = $_POST["reportePDF"];
}
$fallas = 10;
$primero = false;
$segundo = false;
$tercero = false;
$cuarto = false;
$quinto = false;
$sexto = false;
$septimo = false;
$octavo = false;
$noveno = false;
$decimo = false;
$undecimo = false;
$duodecimo = false;
$decimotercero = false;
$decimocuarto = false;
$decimoquinto = false;
$decimosexto = false;
$nombreFab = "Cloud";

if($idDiagnostic > 0){  
    $fallas = 0;
    $totalEquiposActivos = 0;
    $totalEquiposLevantados = 0;
    $totalEquipos = 0;
    
    $listar_equipos = $claseResultadosGeneral->listar_todo1Asignacion($nombreFab, $idDiagnostic);
    foreach ($listar_equipos as $reg_equipos) {
        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
            $totalEquiposLevantados++;
        }

        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
            $totalEquiposActivos++;
        }
        $totalEquipos++;
    }        
        
    //$totalEquiposActivos = $claseResultadosGeneral->totalEquiposActivos($nombre, $idDiagnostic);
    //$totalEquiposLevantados = $claseResultadosGeneral->totalEquiposLevantados($nombre, $idDiagnostic);
    //$totalEquipos = $claseResultadosGeneral->totalEquipos($nombre, $idDiagnostic);
    //$notScanChallenges = $claseResultadosGeneral->equiposNoEscaneados($nombre, $idDiagnostic);
    
    $notScanChallenges = $totalEquiposLevantados - $totalEquiposActivos;
    $procNotScanChallenges = round(($notScanChallenges / $totalEquiposActivos) * 100, 0);
    
    $porcLevantado = 0;
    
    if($totalEquiposActivos > 0){
        $porcLevantado = round(($totalEquiposLevantados / $totalEquiposActivos) * 100, 0);
    }
    
    if($porcLevantado <= 90){
        $fallas++;
    } else{
        $segundo = true;
    }
    
    //$totalEquiposUso = $claseResultadosGeneral->totalEquiposUso($nombre, $idDiagnostic);
    
    /*$porcUso = round(($totalEquiposUso / $totalEquipos) * 100, 0);
    if($porcUso < 75){
        $fallas++;
    } else{
        $tercero = true;
    }*/
    
    $porcActivos = 0;
    if($totalEquipos > 0){
        $porcActivos = round(($totalEquiposActivos / $totalEquipos) * 100, 0);
    }
    
    if($porcActivos > 90){
        $primero = true;
    }else{
        $fallas++;
    }
    
    $UnusedDevice = $totalEquipos - $totalEquiposActivos;
    
    $averageDevice = 0;
    if($totalEquiposLevantados > 0){ //el total de aplicaciones es respecto a Office, Visio, Project y Visual Studio y los equipos activos
        $averageDevice = round(($claseResultadosGeneral->totalAplicaciones($nombreFab, $idDiagnostic) / $totalEquiposLevantados), 0);
    }
    
    if($UnusedDevice > 1){
        $fallas++;
    } else{
        $tercero = true;
    }

    //$unusedSW = $claseResultadosGeneral->unusedSW($nombreFab, $idDiagnostic);
    $desusoOffice = $claseResultadosGeneral->listaProductosDesusoAsignacion($nombreFab, $idDiagnostic, "Office", "Windows");
    $desusoVisio = $claseResultadosGeneral->listaProductosDesusoAsignacion($nombreFab, $idDiagnostic, "Visio", "Windows");
    $desusoProject = $claseResultadosGeneral->listaProductosDesusoAsignacion($nombreFab, $idDiagnostic, "Project", "Windows");
    $desusoVisualStudio = $claseResultadosGeneral->listaProductosDesusoAsignacion($nombreFab, $idDiagnostic, "Visual Studio", "Windows");
    $desusoSQLServer = $claseResultadosGeneral->listaProductosDesusoAsignacion($nombreFab, $idDiagnostic, "SQL Server", "Windows Server");
    
    $unusedSW = count($desusoOffice) + count($desusoVisio) + count($desusoProject) + count($desusoVisualStudio) + count($desusoSQLServer);
    
    if($unusedSW == 0){
        $cuarto = true;
    } else{
        $fallas++;
    }
   
    $porcEstApplications = 0;
    if($porcLevantado > 0){
        $porcEstApplications = round(($unusedSW / $porcLevantado) * 100, 0);
    }
   
    /*$tabla = $claseResultadosGeneral->softwareDuplicate($nombreFab, $idDiagnostic);
    $dup = 0;
    foreach($tabla as $row){
        $dup += $row["cantidad"];
    }
    
    $softwareDuplicate = $dup;*/
    $softwareDuplicate = count($claseResultadosGeneral->softwareDuplicateGeneral($nombreFab, $idDiagnostic));
    
    if($softwareDuplicate == 0){
        $quinto = true; 
    } else{
        $fallas++;
    }
    //$porcEstDuplicate = round(($softwareDuplicate / $totalEquipos) * 100, 0);  
    $porcEstDuplicate = 0;
    if($porcLevantado > 0){
        $porcEstDuplicate = round(($softwareDuplicate / $porcLevantado) * 100, 0); 
    }
    
    //$erroneus = $claseResultadosGeneral->erroneus($nombreFab, $idDiagnostic);
    $erroneus = count($claseResultadosGeneral->erroneusGeneral($nombreFab, $idDiagnostic)); 
    if($erroneus == 0){
        $sexto = true;
    } else{
        $fallas++;
    }
    //$porcEstErroneus = round(($erroneus / $totalEquipos) * 100, 0);
    $porcEstErroneus = 0;
    if($porcLevantado > 0){
        $porcEstErroneus = round(($erroneus / $porcLevantado) * 100, 0);  
    }
    
    $totalOffice = $claseResultadosGeneral->totalOffice($nombreFab, $idDiagnostic);
    $totalOffice365 = $claseResultadosGeneral->totalOffice365($nombreFab, $idDiagnostic); //$claseResultadosGeneral->totalOfficeEdicion($nombreFab, $idDiagnostic, 'Office 365');
    $totalOfficeStandard = $claseResultadosGeneral->totalOfficeEdicion($nombreFab, $idDiagnostic, 'Standard');
    $totalOfficeProfessional = $claseResultadosGeneral->totalOfficeEdicion($nombreFab, $idDiagnostic, 'Professional');

    $totalVisioProjectStandard = $claseResultadosGeneral->totalVisioProjectEdicion($nombreFab, $idDiagnostic, 'Standard');
    $totalVisioProjectProfessional = $claseResultadosGeneral->totalVisioProjectEdicion($nombreFab, $idDiagnostic, 'Professional');
    
    if($totalOffice == ""){
        $totalOffice = 0;
    }
    
    if($totalOffice365 == ""){
        $totalOffice365 = 0;
    }
    
    if($totalOfficeStandard == ""){
        $totalOfficeStandard = 0;
    }
    
    if($totalOfficeProfessional == ""){
        $totalOfficeProfessional = 0;
    }
    
    if($totalOfficeStandard + $totalOfficeProfessional == 0){
        $undecimo = true;
    } else{
        $fallas++;
    }
    
    if($totalOfficeStandard + $totalOfficeProfessional == 0){
        $undecimo = true;
    } else{
        $fallas++;
    }
    
    if($totalVisioProjectStandard + $totalVisioProjectProfessional == 0){
        $duodecimo = true;
    } else{
        $fallas++;
    }
    
    $cloud = 0;
    if($totalOffice > 0){
        $cloud = round(($totalOffice365 / $totalOffice) * 100, 0);
    }
    
    if($cloud >= 50){
        $septimo = true;
    } else{
        $fallas++;
    }

    $totalServidorFisico = count($claseResultadosGeneral->windowServer($nombreFab, $idDiagnostic));
    $totalServidorVirtual = count($claseResultadosGeneral->windowServer($nombreFab, $idDiagnostic, "Virtual"));   
    
    if($totalServidorFisico == 0){
        $relacionServidores = 0;
    } else{
        $relacionServidores = round($totalServidorVirtual / $totalServidorFisico, 0);
    }
    if($relacionServidores >= 3){
        $octavo = true;
    } else{
        $fallas++;
    }
   
    $MSCloudDBBasicNewer = $claseResultadosGeneral->MSCloudDB($nombreFab, $idDiagnostic, "basic", "newer");
    $MSCloudDBStandardNewer = $claseResultadosGeneral->MSCloudDB($nombreFab, $idDiagnostic, "standard", "newer");
    $MSCloudDBPremiumNewer = $claseResultadosGeneral->MSCloudDB($nombreFab, $idDiagnostic, "premium", "newer");
    
    $MSCloudDBBasicOlder = $claseResultadosGeneral->MSCloudDB($nombreFab, $idDiagnostic, "basic", "older");
    $MSCloudDBStandardOlder = $claseResultadosGeneral->MSCloudDB($nombreFab, $idDiagnostic, "standard", "older");
    $MSCloudDBPremiumOlder = $claseResultadosGeneral->MSCloudDB($nombreFab, $idDiagnostic, "premium", "older");
    
    $MSCloudVMBasicNewer = $claseResultadosGeneral->MSCloudVM($nombreFab, $idDiagnostic, "basic", "newer");
    $MSCloudVMLowPriorityNewer = $claseResultadosGeneral->MSCloudVM($nombreFab, $idDiagnostic, "low", "newer");
    $MSCloudVMStandardNewer = $claseResultadosGeneral->MSCloudVM($nombreFab, $idDiagnostic, "standard", "newer");
    
    $MSCloudVMBasicOlder = $claseResultadosGeneral->MSCloudVM($nombreFab, $idDiagnostic, "basic", "older");
    $MSCloudVMLowPriorityOlder = $claseResultadosGeneral->MSCloudVM($nombreFab, $idDiagnostic, "low", "older");
    $MSCloudVMStandardOlder = $claseResultadosGeneral->MSCloudVM($nombreFab, $idDiagnostic, "standard", "older");
    
    if((count($MSCloudVMBasicNewer) + count($MSCloudVMLowPriorityNewer) + count($MSCloudVMStandardNewer)) == 0){
        $decimotercero = true;
    } else{
        $fallas++;
    }
    
    if((count($MSCloudVMBasicOlder) + count($MSCloudVMLowPriorityOlder) + count($MSCloudVMStandardOlder)) == 0){
        $decimocuarto = true;
    } else{
        $fallas++;
    }
    
    if((count($MSCloudDBBasicNewer) + count($MSCloudDBStandardNewer) + count($MSCloudDBPremiumNewer)) == 0){
        $decimoquinto = true;
    } else{
        $fallas++;
    }
    
    if((count($MSCloudDBBasicOlder) + count($MSCloudDBStandardOlder) + count($MSCloudDBPremiumOlder)) == 0){
        $decimosexto = true;
    } else{
        $fallas++;
    }
    
    $productosSinSoporteCliente = $claseResultadosGeneral->productosSinSoporte($nombreFab, $idDiagnostic, "cliente");
    if($productosSinSoporteCliente == 0){
        $noveno = true;
    } else{
        $fallas++;
    }
    
    $productosSinSoporte1Cliente = $claseResultadosGeneral->productosSinSoporte1($nombreFab, $idDiagnostic, "cliente");
    $softCliente = "Non Detected";
    
    $i = 0;
    foreach($productosSinSoporte1Cliente as $row){
        if($row["familia"] == "Windows"){
            $i++;
            break;
        }
        $softCliente = $row["familia"];
    }
    
    if($i > 0){
        $softCliente = "Windows";
    }
    
    if(count($productosSinSoporte1Cliente) > 1){
        $softCliente .= " and others";
    }
    
    $productosSinSoporteServidor = $claseResultadosGeneral->productosSinSoporte($nombreFab, $idDiagnostic, "servidor");
    if($productosSinSoporteServidor == 0){
        $decimo = true;
    } else{
        $fallas++;
    }
    
    $productosSinSoporte1Servidor = $claseResultadosGeneral->productosSinSoporte1($nombreFab, $idDiagnostic, "servidor");
    $softServidor = "Non Detected";
    foreach($productosSinSoporte1Servidor as $row){
        if($row["familia"] == "Windows Server"){
            $softServidor = $row["familia"];
            break;
        }
        $softServidor = $row["familia"];
    }
        
    if($reportePDF == 0){
        ob_start();
        $html = ob_get_clean();
        $html = utf8_encode($html);
        $html .= '<style>
                .tituloSAMDiagnostic1{
                    font-size:35px;
                    text-align: center;
                    font-weight: bold;
                    color: #000000;
                }

                .tablapS {     
                    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
                    font-size: 15px;   
                    margin: 0 auto;   
                    width: 90%; 
                    text-align: left;    
                    border-collapse: collapse; 
                }

                .tablapS thead th {     
                    font-size: 25px;     
                    font-weight: bold;     
                    padding: 15px;     
                    background: #377EBA;
                    border-bottom: 2px solid #fff;   
                    color: #ffffff; 
                    text-align: center; 
                }

                .tablapS thead th.transTopLeft{
                    background-color: transparent;
                }

                .tablapS thead th.trans{
                    background-color: transparent;
                }

                .tablapS tbody td.trans{
                    background-color: transparent;
                    font-size: 20px;
                }

                .tablapS tbody td.trans1{
                    background-color: transparent;
                    border: 0;
                }

                .tablapS tbody td.trans2{
                    background-color: transparent;
                    border: 0;
                    font-size: 20px;
                }

                .tablapS tbody td.titulo{
                    font-size: 20px;
                }

                .tablapS tbody th {  
                    padding: 8px;     
                    background: #5C96C6;     
                    border: 2px solid #fff;
                    color: #000000;  
                    text-align: center;
                    vertical-align: middle;
                    font-weight: normal;
                }

                .tablapS tbody td {    
                    padding: 8px;     
                    background: #A0C3DF;     
                    border: 2px solid #fff;
                    color: #000000;    
                    text-align: center;
                    vertical-align: middle;
                }

                .tablapS tbody td.sinBottom{
                    border-bottom: 0;
                }

                .bold{
                    font-weight: bold;
                }
            </style>

            <div style="width:100%; height:auto; overflow:hidden;" id="contPOC">
            <h1 class="tituloSAMDiagnostic1 colorBlanco">Cloud Assessment Report</h1>

            <br>
            <table class="tablapS">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Metric</th>
                        <th>Description</th>
                        <th>Result</th>
                        <th colspan="3">Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="bold" rowspan="2">Overview</td>
                        <td>Active Devices</td>
                        <td>> 90% Active Devices</td>
                        <td><img src="';
                            if($primero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; }
                        $html .= '"></td>
                        <td colspan="2">Active Devices: ' . $porcActivos . '%</td>
                        <td>Devices: ' . $totalEquipos . '</td>
                    </tr>
                    <tr>
                        <td>Discovery</td>
                        <td>> 90% Discovery</td>
                        <td><img src="';
                            if($segundo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="2">Scanned Devices: ' . $porcLevantado . '%</td>
                        <td>Active Devices: ' . $totalEquiposActivos . '</td>
                    </tr>
                    <tr>
                        <th rowspan="6" style="font-weight: bold;">Cloud Ready</th>
                        <th rowspan="2" >Office 365</th>
                        <th>Office</th>
                        <th><img src="';
                            if($undecimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th colspan="2">E1: ' . $totalOfficeStandard . '</th>
                        <th>E3: ' . $totalOfficeProfessional . '</th>
                    </tr>
                    <tr>
                        <th>Project & Visio</th>
                        <th><img src="';
                            if($duodecimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th colspan="2">E1: ' . $totalVisioProjectStandard . '</th>
                        <th>E3: ' . $totalVisioProjectProfessional . '</th>
                    </tr>
                    <tr>
                        <th rowspan="4" >Azure</th>
                        <th>Windows Server: Newer</th>
                        <th><img src="';
                            if($decimotercero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; }
                        $html .= '"></th>
                        <th style="width:165px;">Basic: ' . $MSCloudVMBasicNewer . '</th>
                        <th style="width:165px;">Low Priority: ' . $MSCloudVMLowPriorityNewer . '</th>
                        <th style="width:165px;">Standard: ' . $MSCloudVMStandardNewer . '</th>
                    </tr>
                    <tr>
                        <th>Windows Server: Older</th>
                        <th><img src="';
                            if($decimocuarto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th>Basic: ' . $MSCloudVMBasicOlder . '</th>
                        <th>Low Priority: ' . $MSCloudVMLowPriorityOlder . '</th>
                        <th>Standard: ' . $MSCloudVMStandardOlder . '</th>
                    </tr>
                    <tr>
                        <th>SQL Server: Newer</th>
                        <th><img src="';
                            if($decimoquinto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th>Basic: ' . $MSCloudDBBasicNewer . '</th>
                        <th>Standard: ' . $MSCloudDBStandardNewer . '</th>
                        <th>Premium: ' . $MSCloudDBPremiumNewer . '</th>
                    </tr>
                    <tr>
                        <th>SQL Server: Older</th>
                        <th><img src="';
                            if($decimosexto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th>Basic: ' . $MSCloudDBBasicOlder . '</th>
                        <th>Standard: ' . $MSCloudDBStandardOlder . '</th>
                        <th>Premium: ' . $MSCloudDBPremiumOlder . '</th>
                    </tr>
                    <tr>
                        <td class="bold" rowspan="6">Optimization & Cybersecurity</td>
                        <td>Unused Devices</td>
                        <td>1 or more unused devices detected</td>
                        <td><img src="';
                            if($tercero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="3">Unused Devices: ' . $UnusedDevice . '</td>
                    </tr>
                    <tr>
                        <td>Unused Software</td>
                        <td>1 or more unused software detected</td>
                        <td><img src="'; 
                            if($cuarto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="2">Detected: ' . $unusedSW . '</td>
                        <td>Estimated: ' . $porcEstApplications . '</td>
                    </tr>
                    <tr>
                        <td>Duplicate Installations</td>
                        <td>1 or more duplicate installations detected</td>
                        <td><img src="';
                            if($quinto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="2">Detected: ' . $softwareDuplicate . '</td>
                        <td>Estimated: ' . $porcEstDuplicate . '</td>
                    </tr>
                    <tr>
                        <td>Erroneus Installations</td>
                        <td>1 or more erroneus installations detected</td>
                        <td><img src="'; 
                            if($sexto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="2">Detected: ' . $erroneus . '</td>
                        <td>Estimated: ' . $porcEstErroneus . '</td>
                    </tr>
                    <tr>
                        <td rowspan="2">Unsupported Software</td>
                        <td>Clients</td>
                        <td><img src="'; 
                            if($noveno == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="3">' . $softCliente . '</td>
                    </tr>
                    <tr>
                        <td>Servers</td>
                        <td><img src="'; 
                            if($decimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="3">' . $softServidor . '</td>
                    </tr>
                    <tr>
                        <th colspan="3" style="font-weight: bold;">Assessment</th>
                        <th><img src="';
                            if($fallas < 3){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } 
                            else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                            $html .= '"></th>
                        <th colspan="3" style="font-weight: bold;">';
                            if($fallas <= 4){ $html .= 'Low ROI Expected'; } 
                            else if($fallas > 4 && $fallas <= 8){ $html .= 'Medium ROI Expected'; } else{ $html .= 'High ROI Expected'; } 
                        $html .= '</th>
                    </tr>
                </tbody>
            </table>
        </div>

        <br>

        <table style="width:100%;">
            <tr>
                <td style="width:13%;">' . date("m/d/Y") . '</td>
                <td style="width:73%;text-align:center;">https://www.licensingassurance.com/Cloud/</td>
                <td style="width:13%;"><img style="height:60px;" align="right" src="../img/LA_Logo.png"></img></td>
            </tr>
        </table>';

                        
        $path = $GLOBALS["app_root1"] . "/Cloud/resultados/";
        $nameArchivo = "Cloud" . date("dmYHms") . ".pdf";
        include($GLOBALS["app_root1"] . "/mpdf60/mpdf.php");
        $mpdf = new mPDF('', 'A3-L', 0, '', 15, 15, 16, 16, 15, 9, 'L');
        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = "UTF-8";
        $mpdf->WriteHTML($html);
        $mpdf->output($path . $nameArchivo, 'F');       
        
        $ip = $general->getRealIP();
        $geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
        $city = $geo["geoplugin_city"];
        $country = $geo["geoplugin_countryName"];     
        
        $mensaje = utf8_decode("IP Address: " . $ip . " "
            . "País: " . $country . " "
            . "Ciudad: " . $city);
        
        $claseEnvio = new email();
        /*if($claseEnvio->mail_file("Dimitri@licensingassurance.com", "soportetecnico@licensingassurance.com", 
        "Cloud Assessment: Nombre: " . $nombre . "   Email: " . $email, $mensaje, $path . $nameArchivo, $nameArchivo)){*/
        if($claseEnvio->mail_file("it@licensingassurance.com", "m_acero_n@hotmail.com", 
        "Cloud Assessment: Nombre: " . $nombre . "   Email: " . $email, $mensaje, $path . $nameArchivo, $nameArchivo)){
            try{
                $claseResultadosGeneral->actualizarMSCloud($idDiagnostic, $nombre, $email, $nameArchivo);
            }catch(PDOException $e){
                $error = 3;
            }
        } else{
            $error = 2;
        }
    }
} else{
    $error = 1;
}