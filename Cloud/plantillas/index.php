        <?php if($exito==1){ ?>
            <script>
                $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>');
            </script>
        <?php }else{ ?>
        <?php }
        if($error== -1){ ?>
            <script>
                $.alert.open('warning', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        if($error== -2){ ?>
            <script>
                $.alert.open('warning', 'Wrong Password');
            </script>
        <?php }
        else if($error==1){ ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        else if($error==2){ ?>
            <script>
              $.alert.open('warning', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        else if($error == 3){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?>');
            </script>
        <?php
        }
        else if($error == 4){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?>');
            </script>
        <?php
        }
        else if($error == 5){ ?>
            <script>
                $.alert.open('warning', "<?=$consolidado->error?>");
            </script>
        <?php } 
        else if($error == 6){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?>');
            </script>
        <?php
        }
        else if($error == 7){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?>');
            </script>
        <?php
        }
        else if($error == 8){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?>');
            </script>
        <?php
        }
        else if($error == 9){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?>');
            </script>
        <?php
        } else if($error == 10){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?>');
            </script>
        <?php
        } else if($error == 11){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?>');
            </script>
        <?php
        } else if($error == 12){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?>');
            </script>
        <?php
        } else if($error == 13){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?>');
            </script>
        <?php
        } else if($error == 14){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?>');
            </script>
        <?php
        } else if($error == 15){
        ?>
            <script>
                $.alert.open('warning', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>');
            </script>
        <?php
        } else if($error == 16){
        ?>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                    $("body").scrollTop(1000);
                });
            </script>
        <?php
        } else if($error == 17){
        ?>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                    $("body").scrollTop(1000);
                });
            </script>
        <?php
        }

        if($exito2==1){ ?>
            <script>
                $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>');
            </script>
        <?php }else{ ?>

        <?php }
        if($error2== -1){ ?>
            <script>
                $.alert.open('warning', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        else if($error2==1){ ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        else if($error2==2){ ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        else if($error2==3){ ?>
            <script>
                $.alert.open('warning', '<?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        else if($error2 == 5){ ?>
            <script>
                $.alert.open('warning', '<?=$consolidado->error?>');
            </script>
        <?php } else if($error2 == 6){ ?>
            <script>
                $.alert.open('warning', '<?= $general->getSeparadorLAE($_SESSION["idioma"]) ?>');
            </script>
        <?php }
        ?>

        <div class="container contenedorDiagnostic">
            <div class='row'>
                <div class='col-sm-12 col-md-6'>
                    <div style="width:280px; margin:0 auto;">
                        <p class="tituloImagenes text-center"><img src="<?= $GLOBALS["domain_root1"] ?>/img/optimizacion.png" style="width:auto; height:80px;" align="left"> Optimization</p> 
                    </div>
                    <p class="text-center tituloSAMDiagnostic2">Detects unused software,</p>
                    <p class="text-center tituloSAMDiagnostic2">unused devices, &</p>
                    <p class="text-center tituloSAMDiagnostic2">duplicate installations</p>
                </div>
                
                <div class='col-xs-12 visible-xs visible-sm'>&nbsp;</div>
                
                <div class='col-sm-12 col-md-6'>
                    <div style="width:280px; margin:0 auto;">
                        <p class="tituloImagenes text-center"><img src="<?= $GLOBALS["domain_root1"] ?>/img/readiness.png" style="width:auto; height:80px;" align="left"> Cloud Readiness</p> 
                    </div>
                    <p class="text-center tituloSAMDiagnostic2 bold">Discovers your software</p>
                    <p class="text-center tituloSAMDiagnostic2 bold">deployment, & migration</p>
                    <p class="text-center tituloSAMDiagnostic2 bold">path to Office 365 & Azure</p>
                </div>
            </div>

            <div class='row'>&nbsp;</div>

            <div class='row'>
                <!--<div class='col-xs-12'>
                    <div class="text-center tituloSAMDiagnostic2 pointer" 
                    style="width:380px; height:30px; margin:0 auto; line-height:30px; background-color:#ffffff;"
                    onclick="window.open('Sample Cloud.pdf')">
                        View Sample Cloud Assessment Report                
                    </div>
                </div>-->
                
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class='col-xs-12 col-sm-6'>
                        <div class="text-center tituloSAMDiagnostic2 pointer" 
                            style="width:280px; height:30px; margin:0 auto; line-height:30px; background-color:#ffffff;"
                            onclick="window.open('https://www.licensingassurance.com/English/Cloud.pdf')">
                            Sample Report                
                        </div>
                    </div>
                        
                    <div class='col-xs-12 col-sm-6'>
                        <div class="text-center tituloSAMDiagnostic2 pointer" 
                            style="width:280px; height:30px; margin:0 auto; line-height:30px; background-color:#ffffff;"
                            onclick="window.open('https://www.licensingassurance.com/Español/Cloud.pdf')">
                            Ejemplo de Informe                
                        </div>
                    </div>
                </div>
            </div>

            <div class='row'>&nbsp;</div>

            <div class='row'>
                <div class='col-sm-12 col-md-6'>
                    <h1 class="tituloSAMDiagnostic">3 SIMPLE STEPS TO YOUR CLOUD ASSESSMENT:</h1><br>
                    <div class="row">
                        <div class='col-xs-12 col-sm-6'>
                            <p class="bold">1) DOWNLOAD TOOL</p>
                            <div class="btn botonDiagnostic" id="descargar">
                                <i class="fa fa-download fa-lg" aria-hidden="true"> Download</i>
                            </div>
                        </div>
                        
                        <div class='col-xs-12 col-sm-2'>&nbsp;</div>
                        <div class='col-xs-12 col-sm-4'>
                            <p class="bold">2) RUN TOOL</p>
                            <div class="btn botonDiagnostic" onclick="window.open('User Guide.pdf');">
                                <i class="fa fa-download fa-lg" aria-hidden="true"> Instructions</i>
                            </div>
                            <div class="col-xs-12">&nbsp;</div>
                            <div class="btn botonDiagnostic" onclick="window.open('Guia de Uso.pdf');">
                                <i class="fa fa-download fa-lg" aria-hidden="true"> Instrucciones</i>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class='col-xs-12 col-sm-6'>                            
                            <p class="bold">3) LOAD RESULTS</p>
                            <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="procesarLAD.php">
                                <input name="insertar" id="insertar" type="hidden" value="1" >
                                <input type="hidden" name="idDiagnostic" id="idDiagnosticLAE" value="<?= $idDiagnostic ?>">
                                <input type="file" class="hide" name="archivo" id="archivo" accept=".rar">

                                <div class="btn botonDiagnostic" id="buscarLAD_Output">
                                    <i class="fa <?php if($exito == 0 && $exito2 == 0){ echo "fa-upload"; }
                                    else{ echo "fa-check"; } ?> fa-lg" aria-hidden="true"> LAD</i>
                                </div>
                                <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:180px;">Processing...</div>
                            </form>
                            
                            <div class='col-xs-12'>&nbsp;</div>
                            <form enctype="multipart/form-data" name="form_e2" id="form_e2" method="post" action="procesarLAE.php" >
                                <input name="insertar" id="insertar" type="hidden" value="1" >
                                <input type="hidden" name="idDiagnostic" id="idDiagnosticLAE" value="<?= $idDiagnostic ?>">
                                <input type="hidden" name="escaneo" id="escaneoLAE" value="<?= $archivoEscaneo ?>">
                                <input type="hidden" name="procesadores" id="procesadoresLAE" value="<?= $archivoProcesadores ?>">
                                <input type="hidden" name="tipoEquipo" id="tipoEquipoLAE" value="<?= $archivoEquipos ?>">
                                <input type="hidden" name="fechaDespliegue" id="fechaDespliegue">
                                <input type="file" class="hide" name="archivo" id="archivo2" accept=".csv">

                                <div class="btn botonDiagnostic" onclick="mostrarFechaDespliegue()">
                                    <i class="fa <?php if($exito2 == 0){ echo "fa-upload"; }
                                    else { echo  "fa-check"; } ?> fa-lg" aria-hidden="true"> LAE</i>
                                </div>
                                <div id="cargando1" style="display:none;text-align:center; color:#2D6BA4; width:180px;">Processing...</div>
                            </form>
                        </div>
                        
                        <div class='col-xs-12 col-sm-2'>&nbsp;</div>
                        <div class='col-xs-12 col-sm-4'>
                            <form id="formResult" name="formResult" method="post" action="resultados.php">
                                <input type="hidden" id="idDiagnosticResult" name="idDiagnostic" value="<?= $idDiagnosticLAE ?>">
                                <p class="bold">CONTACT INFO</p>
                                <input type="text" id="nombre" name="nombre" class="input" maxlength="70" placeholder="Name*" style="width:150px; margin-top:0px;"><br>
                                <input type="email" id="email" name="email" class="input" maxlength="70" placeHolder="Email*" style="width:150px;">
                                <input type="password" id="code" name="code" class="input" maxlength="10" placeHolder="Code*" style="width:150px;">
                            </form>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                            <div class="btn botonDiagnostic" style="margin:0 auto;" id="resultados">
                                <i class="fa fa-lg"  aria-hidden="true">RESULTS</i>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class='col-sm-12 col-md-6'>
                    <h1 class="tituloSAMDiagnostic bold text-center">Very Easy & Simple...</h1><br>
                    
                    <!--<div class="embed-responsive embed-responsive-16by9">-->
                    <div style="margin:0 auto; width:450px; overflow:hidden;">
                        <iframe width="450" height="300" src="https://www.youtube.com/embed/F2yN_on6VXo" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--inicio modal fecha despliegue-->
<div class="modal-personal modal-personal-md" id="modal-fechaDespliegue">
    <div class="modal-personal-head" style="height:auto;">
        <h1 class="textog negro" style="margin:10px; text-align:center;">Deployment <p style="color:#06B6FF; display:inline">Date</p></h1>
    </div>
    <div class="modal-personal-body text-center">
        <span class="bold">Date: </span><input type="text" id="fechaD" name="fechaD">
    </div>
    <div class="modal-personal-footer">
        <div class="btn botonDiagnostic" style="float:right; margin-left:10px;" id="salirFechaDespliegue">Exit</div>
        <div class="btn botonDiagnostic" style="float:right;" id="procesarLAE_Output">Accept</div>
    </div>
</div>

<script>
    var validEmail = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    $(document).ready(function(){
        $("#fechaD").datepicker();
        
        $("#descargar").click(function(){
            window.open("<?= $GLOBALS["domain_root"] ?>/<?= $general->ToolAD ?>");
        });
        
        $("#buscarLAD_Output").click(function(){
            $("#archivo").click();
        });
        
        $("#archivo").change(function(e){
            if(addArchivo(e) === false){
                $("#archivo").val("");
                $("#archivo").replaceWith($("#archivo").clone(true));
            } else{
                $("#cargando").show();
                $("#form_e").submit();
            }
        });
        
        $("#archivo2").change(function(e){
            if(addArchivo1(e) === false){
                $("#archivo2").val("");
                $("#archivo2").replaceWith($("#archivo2").clone(true));
            } else{
                $("#fondo1").show();
                $("#modal-fechaDespliegue").show();
            }
        }); 
        
        //inicio fecha despliegue
        $("#salirFechaDespliegue").click(function(){
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
            $("#archivo2").val("");
            $("#archivo2").replaceWith($("#archivo2").clone(true));
        });
        
        $("#procesarLAE_Output").click(function(){
            if(parseInt($("#idDiagnosticLAE").val()) === 0){
                $("#fondo1").hide();
                $("#modal-fechaDespliegue").hide();
                $("#archivo2").val("");
                $("#archivo2").replaceWith($("#archivo2").clone(true));
                $.alert.open('warning', 'Alert', "You must load the LAD_Output.rar file before you perform this step", {"Aceptar" : "Aceptar"});
                return false;
            }
            
            if($("#fechaD").val() === ""){
                $.alert.open('warning', 'Alert', "You must select the deployment date", {"Aceptar" : "Aceptar"});
                return false;
            }
             
            $("#fechaDespliegue").val($("#fechaD").val());
            $("#cargando1").show();
            document.getElementById('form_e2').submit();
           
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        //fin fecha despliegue
        
        $("#resultados").click(function(){
            if(parseInt($("#idDiagnosticResult").val()) === 0){
                $.alert.open('warning', 'Alert', "You can't see the results as long as you don't make the full step two", {"Aceptar" : "Aceptar"});
                return false;
            }
            if($("#nombre").val() === ""){
                $.alert.open('warning', 'Alert', "You must place your name", {"Aceptar" : "Aceptar"});
                return false;
            }
            if($("#email").val() === ""){
                $.alert.open('warning', 'Alert', "You must place your email", {"Aceptar" : "Aceptar"});
                return false;
            }
            if(validEmail.test($("#email").val()) === false){
                $.alert.open('warning', 'Alert', "Place a valid email", {"Aceptar" : "Aceptar"});
                return false;
            }
            if($("#code").val() === ""){
                $.alert.open('warning', 'Alert', "Enter validation code", {"Aceptar" : "Aceptar"});
                return false;
            }
            
            if($("#code").val() !== "Licensing"){
                $.alert.open('warning', 'Alert', "Incorrect validation code", {"Aceptar" : "Aceptar"});
                return false;
            }
            document.getElementById('formResult').submit();
        });
    });

    function addArchivo(e){
        file        = e.target.files[0]; 
        nombre      = file.name;
        arrayNombre = nombre.split("."); 
        if(arrayNombre.pop() !== "rar"){
            $.alert.open('warning', 'Alert', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', {"Aceptar" : "Aceptar"});
            return false;
        }
        else{
            if(parseInt(file.size) / 1024 > 30720 ){
                $.alert.open('warning', 'Alert', 'The allowed size is 30 MB', {"Aceptar" : "Aceptar"});
                return false;
            }
            else{
                reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
                return true;
            }
        }  
    }

    function addArchivo1(e){
        file        = e.target.files[0];
        nombre      = file.name;
        arrayNombre = nombre.split("."); 
        if(arrayNombre.pop() !== "csv"){
            $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {"Aceptar" : "Aceptar"});
            return false;
        }
        else{
            if(parseInt(file.size) / 1024 > 30720 ){
                $.alert.open('warning', 'Alert', 'The allowed size is 30 MB', {"Aceptar" : "Aceptar"});
                return false;
            }
            else{
                reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
                return true;
            }
        }  
    }

    function fileOnload(e) {
        result=e.target.result;
    }
    
    function mostrarFechaDespliegue(){
        $("#archivo2").click();
    }
</script>