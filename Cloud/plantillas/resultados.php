        <h1 class="tituloSAMDiagnostic1">CLOUD ASSESSMENT REPORT</h1>
        <br>

        <table class="tablapS">
            <thead>
                <tr>
                    <th style="width:15%">Category</th>
                    <th style="width:15%">Metric</th>
                    <th style="width:15%">Description</th>
                    <th style="width:10%">Result</th>
                    <th colspan="3" style="width:45%">Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="bold" rowspan="2">Overview</td>
                    <td>Active Devices</td>
                    <td>> 90% Active Devices</td>
                    <td><img src="<?php if($primero == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                    <td colspan="2"><?= 'Active Devices: ' . $porcActivos . "%" ?></td>
                    <td><?= 'Devices: ' . $totalEquipos ?></td>
                </tr>
                <tr>
                    <td>Discovery</td>
                    <td>> 90% Discovery</td>
                    <td><img src="<?php if($segundo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                    <td colspan="2"><?= 'Scanned Devices: ' . $porcLevantado . "%" ?></td>
                    <td><?= 'Active Devices: ' . $totalEquiposActivos ?></td>
                </tr>
                
                <tr>
                    <th class="bold" rowspan="6"><img src="<?= $GLOBALS["domain_root1"] ?>/img/readiness.png" style="width:auto; height:100px;"><br>
                        Cloud Ready</th>
                    <th rowspan="2" ><img src="<?= $GLOBALS["domain_root1"] ?>/img/office365.png"></th>
                    <th>Office</th>
                    <th><img src="<?php if($undecimo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                    <th colspan="2"><?= 'E1: ' . $totalOfficeStandard ?></th>
                    <th><?= 'E3: ' . $totalOfficeProfessional ?></th>
                </tr>
                <tr>
                    <th>Project & Visio</th>
                    <th><img src="<?php if($duodecimo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                    <th colspan="2"><?= 'E1: ' . $totalVisioProjectStandard ?></th>
                    <th><?= 'E3: ' . $totalVisioProjectProfessional ?></th>
                </tr>
                
                <tr>
                    <th rowspan="4" ><img src="<?= $GLOBALS["domain_root1"] ?>/img/azure.png"></th>
                    <th>Windows Server: Newer</th>
                    <th><img src="<?php if($decimotercero == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                    <th style="width:165px;"><?= 'Basic: ' . $MSCloudVMBasicNewer ?></th>
                    <th style="width:165px;"><?= 'Low Priority: ' . $MSCloudVMLowPriorityNewer ?></th>
                    <th style="width:165px;"><?= 'Standard: ' . $MSCloudVMStandardNewer ?></th>
                </tr>
                <tr>
                    <th>Windows Server: Older</th>
                    <th><img src="<?php if($decimocuarto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                    <th><?= 'Basic: ' . $MSCloudVMBasicOlder ?></th>
                    <th><?= 'Low Priority: ' . $MSCloudVMLowPriorityOlder ?></th>
                    <th><?= 'Standard: ' . $MSCloudVMStandardOlder ?></th>
                </tr>
                <tr>
                    <th>SQL Server: Newer</th>
                    <th><img src="<?php if($decimoquinto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                    <th><?= 'Basic: ' . $MSCloudDBBasicNewer ?></th>
                    <th><?= 'Standard: ' . $MSCloudDBStandardNewer ?></th>
                    <th><?= 'Premium: ' . $MSCloudDBPremiumNewer ?></th>
                </tr>
                <tr>
                    <th>SQL Server: Older</th>
                    <th><img src="<?php if($decimosexto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                    <th><?= 'Basic: ' . $MSCloudDBBasicOlder ?></th>
                    <th><?= 'Standard: ' . $MSCloudDBStandardOlder ?></th>
                    <th><?= 'Premium: ' . $MSCloudDBPremiumOlder ?></th>
                </tr>

                <tr>
                    <td class="bold" rowspan="6"><img src="<?= $GLOBALS["domain_root1"] ?>/img/optimizacion.png" style="width:auto; height:100px;"><br>
                        Optimization & Cybersecurity</td>
                    <td>Unused Devices</td>
                    <td>1 or more unused devices detected</td>
                    <td><img src="<?php if($tercero == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>" title="<?= 'Unused Devices: ' . $UnusedDevice ?>"></td>
                    <td colspan="3"><?= 'Unused Devices: ' . $UnusedDevice ?></td>
                </tr>
                <tr>
                    <td>Unused Software</td>
                    <td>1 or more unused software detected</td>
                    <td><img src="<?php if($cuarto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>" title="<?= 'Unused Software: ' . $unusedSW . '   Est Applications: ' . $porcEstApplications ?>"></td>
                    <td colspan="2"><?= 'Detected: ' . $unusedSW ?></td>
                    <td><?= 'Estimated: ' . $porcEstApplications ?></td>
                </tr>
                <tr>
                    <td>Duplicate Installations</td>
                    <td>1 or more duplicate installations detected</td>
                    <td><img src="<?php if($quinto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>" title="<?= 'Duplicate Installations: ' . $softwareDuplicate . '   Est Applications: ' . $porcEstDuplicate ?>"></td>
                    <td colspan="2"><?= 'Detected: ' . $softwareDuplicate ?></td>
                    <td><?= 'Estimated: ' . $porcEstDuplicate ?></td>
                </tr>
                <tr>
                    <td>Erroneus Installations</td>
                    <td>1 or more erroneus installations detected</td>
                    <td><img src="<?php if($sexto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>" title="<?= 'Erroneus Installations: ' . $erroneus . ' Est Erroneous: ' . $porcEstErroneus ?>"></td>
                    <td colspan="2"><?= 'Detected: ' . $erroneus ?></td>
                    <td><?= 'Estimated: ' . $porcEstErroneus ?></td>
                </tr>
                <tr>
                    <td rowspan="2">Unsupported Software</td>
                    <td>Clients</td>
                    <td><img src="<?php if($noveno == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>" title="<?= $soft ?>"></td>
                    <td colspan="3"><?= $softCliente ?></td>
                </tr>
                <tr>
                    <td>Servers</td>
                    <td><img src="<?php if($decimo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>" title="<?= $soft ?>"></td>
                    <td colspan="3"><?= $softServidor ?></td>
                </tr>
                
                <tr>
                    <th class="bold" colspan="3">Assessment</th>
                    <th><img src="<?php if($fallas < 3){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } 
                    else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                    <th colspan="3" class="bold"><?php if($fallas <= 4){ echo 'Low ROI Expected'; } 
                    else if($fallas > 4 && $fallas <= 8){ echo 'Medium ROI Expected'; } else{ echo 'High ROI Expected'; } ?></th>
                </tr>
            </tbody>
        </table>

        <br>

        <div style="width:800px; margin: 0 auto; overflow:hidden;">
            <form id="form1" name="form1" method="post" action="exportar.php" target="_blank">
                <input type="hidden" id="idDiagnostic" name="idDiagnostic" value="<?= $idDiagnostic ?>">
                <input type="hidden" id="reportePDF" name="reportePDF" value="1">
                <div class="btn botonDiagnostic float-lt" onclick="document.getElementById('form1').submit();">
                    <i class="fa fa-download fa-lg" aria-hidden="true"> Download</i>
                </div>
                <div class="btn botonDiagnostic float-rt" onclick="location.href='MSCloud.php';">
                    <i class="fa fa-lg" aria-hidden="true">NEXT STEP</i>
                </div>
            </form>
        </div>
    </div>                    
</section>