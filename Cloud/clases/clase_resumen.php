<?php
class ResumenMSCloud extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO resumen_MSCloud(idDiagnostic, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES " . $bloque;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($idDiagnostic) {
        $this->conexion();
        $query = "DELETE FROM resumen_MSCloud WHERE idDiagnostic = :idDiagnostic";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productosSinSoporte($idDiagnostic, $opcion){
        $this->conexion();
        $where = " AND resumen_MSCloud.familia NOT IN ('SQL Server') ";
        $where1 = " AND detalles_equipoMSCloud.familia = 'Windows' ";
        if ($opcion == "servidor"){
            $where = " AND resumen_MSCloud.familia IN ('SQL Server') ";
            $where1 = " AND detalles_equipoMSCloud.familia = 'Windows Server' ";
        }
        
        $query = "SELECT COUNT(tabla.id) AS cantidad
            FROM (SELECT resumen_MSCloud.id
                FROM resumen_MSCloud
                    INNER JOIN detalleMaestra ON resumen_MSCloud.familia = detalleMaestra.descripcion AND
                    resumen_MSCloud.edicion = detalleMaestra.campo1 AND resumen_MSCloud.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic " . $where . "
                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version
                
                UNION 
                
                SELECT detalles_equipoMSCloud.id
                FROM detalles_equipoMSCloud
                    INNER JOIN detalleMaestra ON detalles_equipoMSCloud.familia = detalleMaestra.descripcion AND
                    detalles_equipoMSCloud.edicion = detalleMaestra.campo1 AND detalles_equipoMSCloud.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic " . $where1 . "
                GROUP BY detalles_equipoMSCloud.equipo, detalles_equipoMSCloud.familia, detalles_equipoMSCloud.edicion, detalles_equipoMSCloud.version) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function productosSinSoporte1($idDiagnostic, $opcion){
        $this->conexion();
        $where = " AND resumen_MSCloud.familia NOT IN ('SQL Server') ";
        $where1 = " AND detalles_equipoMSCloud.familia = 'Windows' ";
        if ($opcion == "servidor"){
            $where = " AND resumen_MSCloud.familia IN ('SQL Server') ";
            $where1 = " AND detalles_equipoMSCloud.familia = 'Windows Server' ";
        }
        
        $this->conexion();
        $query = "SELECT tabla.familia,
                tabla.edicion,
                    COUNT(tabla.id) AS cantidad
            FROM (SELECT resumen_MSCloud.familia,
                    resumen_MSCloud.edicion,
                    resumen_MSCloud.id
                FROM resumen_MSCloud
                    INNER JOIN detalleMaestra ON resumen_MSCloud.familia = detalleMaestra.descripcion AND
                    resumen_MSCloud.edicion = detalleMaestra.campo1 AND resumen_MSCloud.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic " . $where . "
                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version
                
                UNION 
                
                SELECT detalles_equipoMSCloud.familia,
                    detalles_equipoMSCloud.edicion,
                    detalles_equipoMSCloud.id
                FROM detalles_equipoMSCloud
                    INNER JOIN detalleMaestra ON detalles_equipoMSCloud.familia = detalleMaestra.descripcion AND
                    detalles_equipoMSCloud.edicion = detalleMaestra.campo1 AND detalles_equipoMSCloud.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic " . $where1 . "
                GROUP BY detalles_equipoMSCloud.equipo, detalles_equipoMSCloud.familia, detalles_equipoMSCloud.edicion, detalles_equipoMSCloud.version) tabla
            GROUP BY tabla.familia";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function MSCloudDB($idDiagnostic, $tipo, $opcion){
        $this->conexion();
        if($opcion == "newer"){
            $where = " AND resumen_MSCloud.version > 2008 ";
        } else{
            $where = " AND (resumen_MSCloud.version <= 2008 OR resumen_MSCloud.version = '' OR resumen_MSCloud.version IS NULL) ";
        }
        
        if($tipo == "basic"){
            $where .= " AND resumen_MSCloud.edicion LIKE '%Standard%' AND consolidadoProcesadoresMSCloud.cores <= 8 ";
        } else if($tipo == "standard"){
            $where .= " AND resumen_MSCloud.edicion LIKE '%Standard%' AND consolidadoProcesadoresMSCloud.cores > 8 ";
        } else if($tipo == "premium"){
            $where .= " AND resumen_MSCloud.edicion LIKE '%Enterprise%' ";
        }
       
        $query = "SELECT COUNT(tabla.familia) AS cantidad
            FROM (SELECT resumen_MSCloud.familia
                FROM (SELECT resumen_MSCloud.idDiagnostic,
                        resumen_MSCloud.equipo,
                        resumen_MSCloud.familia,
                        resumen_MSCloud.edicion,
                        resumen_MSCloud.version
                    FROM resumen_MSCloud
                    WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server'
                    GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud
                        INNER JOIN consolidadoProcesadoresMSCloud ON resumen_MSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                        AND resumen_MSCloud.equipo = consolidadoProcesadoresMSCloud.host_name
                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server' " . $where . " 
                GROUP BY resumen_MSCloud.equipo) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudDBBasic($idDiagnostic){
        $this->conexion();
        $query = "SELECT COUNT(tabla.familia) AS cantidad
            FROM (SELECT resumen_MSCloud.familia
                FROM (SELECT resumen_MSCloud.idDiagnostic,
                        resumen_MSCloud.equipo,
                        resumen_MSCloud.familia,
                        resumen_MSCloud.edicion
                    FROM resumen_MSCloud
                    WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server'
                    GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud
                        INNER JOIN consolidadoProcesadoresMSCloud ON resumen_MSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                        AND resumen_MSCloud.equipo = consolidadoProcesadoresMSCloud.host_name
                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server'
                AND resumen_MSCloud.edicion LIKE '%Standard%' AND consolidadoProcesadoresMSCloud.cores <= 8
                GROUP BY resumen_MSCloud.equipo) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudDBStandard($idDiagnostic){
        $this->conexion();
        $query = "SELECT COUNT(tabla.familia) AS cantidad
            FROM (SELECT resumen_MSCloud.familia
                FROM (SELECT resumen_MSCloud.idDiagnostic,
                        resumen_MSCloud.equipo,
                        resumen_MSCloud.familia,
                        resumen_MSCloud.edicion
                    FROM resumen_MSCloud
                    WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server'
                    GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud
                        INNER JOIN consolidadoProcesadoresMSCloud ON resumen_MSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                        AND resumen_MSCloud.equipo = consolidadoProcesadoresMSCloud.host_name
                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server'
                AND resumen_MSCloud.edicion LIKE '%Standard%' AND consolidadoProcesadoresMSCloud.cores > 8
                GROUP BY resumen_MSCloud.equipo) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudDBPremium($idDiagnostic){
        $this->conexion();
        $query = "SELECT COUNT(tabla.familia) AS cantidad
            FROM (SELECT resumen_MSCloud.familia
                FROM (SELECT resumen_MSCloud.idDiagnostic,
                        resumen_MSCloud.equipo,
                        resumen_MSCloud.familia,
                        resumen_MSCloud.edicion
                    FROM resumen_MSCloud
                    WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server'
                    GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud
                        INNER JOIN consolidadoProcesadoresMSCloud ON resumen_MSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                        AND resumen_MSCloud.equipo = consolidadoProcesadoresMSCloud.host_name
                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'SQL Server'
                AND resumen_MSCloud.edicion LIKE '%Enterprise%'
                GROUP BY resumen_MSCloud.equipo) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
}