<?php

class ConsolidadoProcMSCloud extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $datoControl;
    var $hostName;
    var $tipoCpu;
    var $cpu;
    var $cores;
    var $procesadoresLogicos;
    var $tipoEscaneo;
    var $error;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoProcesadoresMSCloud (idDiagnostic, dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($idDiagnostic) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM consolidadoProcesadoresMSCloud WHERE idDiagnostic = :idDiagnstic");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}
