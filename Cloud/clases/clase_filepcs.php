<?php
class FilepcMSCloud extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dn;
    var $objectclass;
    var $cn;
    var $useracountcontrol;
    var $lastlogon;
    var $pwdlastset;
    var $os;
    var $lastlogontimes;
    var $error = NULL;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO filepcsMSCloud (idDiagnostic, dn, objectclass, cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) ";
        $query .= "VALUES " . $bloque;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Eliminar
    function eliminar($idDiagnostic) {
        $this->conexion();
        $query = "DELETE FROM filepcsMSCloud WHERE idDiagnostic = :idDiagnostic";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Obtener datos 
    /*function datos($id) {
        $this->conexion();
        $query = "SELECT * FROM filepcs2 WHERE id = :id";
        try { 
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->dn = $usuario['dn'];
            $this->objectclass = $usuario['objectclass'];
            $this->cn = $usuario['cn'];
            $this->useracountcontrol = $usuario['useracountcontrol'];
            $this->lastlogon = $usuario['lastlogon'];
            $this->pwdlastset = $usuario['pwdlastset'];
            $this->os = $usuario['os'];
            $this->lastlogontimes = $usuario['lastlogontimes'];
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo($idDiagnostic) {
        $this->conexion();
        $query = "SELECT * FROM filepcsMSCloud WHERE idDiagnostic = :idDiagnostic ORDER BY id";
        try { 
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }
    
    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        $this->conexion();
        if ($titulo_archivo != "") {
            $ruta = "archivosLAE/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = "Su archivo excede el límite de tamaño de 16MB.  Por favor contacte a un representante <a href='info@licensingassurance.com' target='_blank'>info@licensingassurance.com</a>";
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}