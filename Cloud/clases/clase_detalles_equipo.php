<?php
class DetallesMSCLoud extends General{
    ########################################  Atributos  ########################################
    public $error = null;
    
    #######################################  Operaciones  #######################################
    // Insertar     
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipoMSCloud (idDiagnostic, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($idDiagnostic) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM detalles_equipoMSCloud WHERE idDiagnostic = :idDiagnostic");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar
    function actualizar($idDiagnostic, $equipo, $errors) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipoMSCloud SET errors = :errors "
            . "WHERE equipo = :equipo AND idDiagnostic = :idDiagnostic");
            $sql->execute(array(':equipo'=>$equipo, ':idDiagnostic'=>$idDiagnostic, ':errors'=>$errors));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function totalEquiposActivos($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(idDiagnostic) AS cantidad
                FROM detalles_equipoMSCloud
                WHERE idDiagnostic = :idDiagnostic AND rango IN (1, 2, 3)");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquiposUso($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(idDiagnostic) AS cantidad
                FROM detalles_equipoMSCloud
                WHERE idDiagnostic = :idDiagnostic AND rango IN (1, 2, 3)");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquiposLevantados($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(idDiagnostic) AS cantidad
                FROM detalles_equipoMSCloud
                WHERE idDiagnostic = :idDiagnostic AND rango IN (1, 2, 3) AND errors = 'Ninguno'");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function scanChallenges($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM detalles_equipoMSCloud
                WHERE idDiagnostic = :idDiagnostic AND equipo NOT IN (
                    SELECT equipo FROM detalles_equipoMSCloud 
                    WHERE idDiagnostic = :idDiagnostic AND errors = 'Ninguno' GROUP BY equipo)
                AND NOT errors IS NULL");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquipos($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM detalles_equipoMSCloud
                WHERE idDiagnostic = :idDiagnostic");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function unusedSW($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.familia) AS cantidad
                FROM (SELECT resumen_MSCloud.familia
                    FROM detalles_equipoMSCloud
                        INNER JOIN resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                        AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                        resumen_MSCloud.familia IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
                    WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic 
                    AND detalles_equipoMSCloud.rango NOT IN (1)
                    GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalAplicaciones($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM (SELECT resumen.id
                FROM detalles_equipoMSCloud
                    INNER JOIN resumen_MSCloud AS resumen ON detalles_equipoMSCloud.equipo = resumen.equipo
                    AND resumen.familia IN ('Office', 'Visio', 'Project', 'Visual Studio') AND resumen.idDiagnostic = detalles_equipoMSCloud.idDiagnostic
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.rango IN (1)
                GROUP BY resumen_MSCloud.equipo, resumen.familia, resumen.edicion, resumen.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function softwareDuplicate($idDiagnostic){
        try{
            $this->conexion();
            /*$sql = $this->conn->prepare("SELECT resumen_MSCloud.id,
                    resumen_MSCloud.equipo,
                    resumen_MSCloud.familia,
                    resumen_MSCloud.edicion,
                    resumen_MSCloud.version,
                    resumen_MSCloud.fecha_instalacion
                FROM detalles_equipoMSCloud
                    INNER JOIN resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                    AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                    resumen_MSCloud.familia IN ('Office', 'Visio', 'Project', 'Visual Studio')
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 1
                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia
                HAVING COUNT(resumen_MSCloud.equipo) > 1");*/
            $sql = $this->conn->prepare("SELECT tabla.equipo,
                    tabla.familia,
                    tabla.edicion,
                    COUNT(tabla.equipo) AS cantidad
                FROM (SELECT resumen_MSCloud.equipo,
                        resumen_MSCloud.familia,
                        resumen_MSCloud.edicion
                    FROM resumen_MSCloud
                    WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic 
                    GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion,
                    resumen_MSCloud.version) tabla
                GROUP BY tabla.equipo, tabla.familia
                HAVING COUNT(tabla.equipo) > 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function erroneus($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(equipo) AS cantidad
                FROM (SELECT equipo
                    FROM (SELECT resumen_MSCloud.equipo,
                            resumen_MSCloud.familia,
                            resumen_MSCloud.edicion,
                            resumen_MSCloud.version
                        FROM detalles_equipoMSCloud
                            INNER JOIN resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                            AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                            (resumen_MSCloud.familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                            OR resumen_MSCloud.edicion LIKE '%Server%') AND resumen_MSCloud.edicion NOT LIKE '%Endpoint Protection%' AND resumen_MSCloud.edicion NOT LIKE '%Express Edition%' AND resumen_MSCloud.edicion NOT LIKE '%MUI%' AND resumen_MSCloud.edicion NOT LIKE '%Update%'
                        WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 1

                        UNION

                        SELECT resumen_MSCloud.equipo,
                               resumen_MSCloud.familia,
                               resumen_MSCloud.edicion,
                               resumen_MSCloud.version
                        FROM detalles_equipoMSCloud
                            INNER JOIN resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                            AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                            (resumen_MSCloud.familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                            AND NOT resumen_MSCloud.edicion LIKE '%Server%') AND resumen_MSCloud.edicion NOT LIKE '%MUI%' AND resumen_MSCloud.edicion NOT LIKE '%Update%' 
                        WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 2) tabla
                    GROUP BY equipo, familia, edicion, version) tabla1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalOffice($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(resumen_MSCloud.equipo) AS cantidad
                FROM detalles_equipoMSCloud
                    INNER JOIN (SELECT resumen_MSCloud.idDiagnostic,
                                    resumen_MSCloud.equipo,
                                    resumen_MSCloud.familia
                                FROM resumen_MSCloud
                                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'Office'
                                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                    AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                    resumen_MSCloud.familia = 'Office'
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    /*function totalOfficeEdicion($idDiagnostic, $edicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(resumen_MSCloud.equipo) AS cantidad
                FROM detalles_equipoMSCloud
                    INNER JOIN (SELECT resumen_MSCloud.idDiagnostic,
                                    resumen_MSCloud.equipo,
                                    resumen_MSCloud.familia,
                                    resumen_MSCloud.edicion,
                                    resumen_MSCloud.version
                                FROM resumen_MSCloud
                                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia = 'Office'
                                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                    AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                    resumen_MSCloud.familia = 'Office' AND resumen_MSCloud.edicion = :edicion AND resumen_MSCloud.version NOT IN (365)
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic, ':edicion'=>$edicion));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    /*function totalOfficeEdicion($idDiagnostic, $edicion){
        try{
            if($edicion == "Professional"){
                $edicion1 = " AND (resumen_MSCloud.edicion LIKE :edicion OR resumen_MSCloud.edicion LIKE '%ProPlus%') ";
            }
            else{
                $edicion1 = " AND resumen_MSCloud.edicion LIKE :edicion ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(resumen_MSCloud.equipo) AS cantidad
                FROM detalles_equipoMSCloud
                    INNER JOIN (SELECT resumen_MSCloud.idDiagnostic,
                                    resumen_MSCloud.equipo,
                                    resumen_MSCloud.familia,
                                    resumen_MSCloud.edicion,
                                    resumen_MSCloud.version
                                FROM resumen_MSCloud
                                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia IN ('Office')
                                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                    AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                    resumen_MSCloud.familia IN ('Office') " . $edicion1 . " AND resumen_MSCloud.edicion NOT LIKE '%Office 365%'
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic, ':edicion'=>'%' . $edicion . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function totalOfficeEdicion($idDiagnostic, $edicion){
        try{
            if($edicion == "Professional"){
                $edicion1 = " AND (resumen_MSCloud.edicion LIKE :edicion OR resumen_MSCloud.edicion LIKE '%ProPlus%' OR resumen_MSCloud.edicion LIKE '%Profesional%') ";
            }
            else{
                $edicion1 = " AND resumen_MSCloud.edicion LIKE :edicion ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT resumen_MSCloud.equipo
                    FROM resumen_MSCloud 
                        INNER JOIN detalles_equipoMSCloud ON resumen_MSCloud.equipo = detalles_equipoMSCloud.equipo 
                        AND resumen_MSCloud.idDiagnostic = detalles_equipoMSCloud.idDiagnostic 
                    WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia LIKE '%Office%' " . $edicion1 . "
                    GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version
                    ORDER BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic, ':edicion'=>'%' . $edicion . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    /*function totalVisioProjectEdicion($idDiagnostic, $edicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(resumen_MSCloud.equipo) AS cantidad
                FROM detalles_equipoMSCloud
                    INNER JOIN (SELECT resumen_MSCloud.idDiagnostic,
                                    resumen_MSCloud.equipo,
                                    resumen_MSCloud.familia,
                                    resumen_MSCloud.edicion,
                                    resumen_MSCloud.version
                                FROM resumen_MSCloud
                                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia IN ('Visio', 'Project')
                                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                    AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                    resumen_MSCloud.familia IN ('Visio', 'Project') AND resumen_MSCloud.edicion = :edicion AND resumen_MSCloud.version NOT IN (365)
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic, ':edicion'=>$edicion));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function totalVisioProjectEdicion($idDiagnostic, $edicion){
        try{
            if($edicion == "Professional"){
                $edicion1 = " AND (resumen_MSCloud.edicion LIKE :edicion OR resumen_MSCloud.edicion LIKE '%ProPlus%' OR resumen_MSCloud.edicion LIKE '%Profesional%') ";
            }
            else{
                $edicion1 = " AND resumen_MSCloud.edicion LIKE :edicion ";
            }
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(resumen_MSCloud.equipo) AS cantidad
                FROM detalles_equipoMSCloud
                    INNER JOIN (SELECT resumen_MSCloud.idDiagnostic,
                                    resumen_MSCloud.equipo,
                                    resumen_MSCloud.familia,
                                    resumen_MSCloud.edicion,
                                    resumen_MSCloud.version
                                FROM resumen_MSCloud
                                WHERE resumen_MSCloud.idDiagnostic = :idDiagnostic AND resumen_MSCloud.familia IN ('Visio', 'Project')
                                GROUP BY resumen_MSCloud.equipo, resumen_MSCloud.familia, resumen_MSCloud.edicion, resumen_MSCloud.version) resumen_MSCloud ON detalles_equipoMSCloud.idDiagnostic = resumen_MSCloud.idDiagnostic
                    AND detalles_equipoMSCloud.equipo = resumen_MSCloud.equipo AND
                    resumen_MSCloud.familia IN ('Visio', 'Project') " . $edicion1 . " AND resumen_MSCloud.version NOT IN (365)
                WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.tipo = 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic, ':edicion'=>$edicion));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    /*function totalServidorFisico($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        LEFT JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name
                        AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        LEFT JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name
                        AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.os LIKE '%Windows Server%'
                    AND NOT consolidadoTipoEquipoMSCloud.modelo LIKE '%Virtual%'
                    GROUP BY detalles_equipoMSCloud.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function totalServidorFisico($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(tabla.equipo) AS cantidad
FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        LEFT JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        LEFT JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.os LIKE "%Windows Server%" AND detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND (IF(consolidadoTipoEquipoMSCloud.modelo LIKE "%Virtual%", "Virtual", "Fisico")) = "Fisico" AND detalles_equipoMSCloud.rango IN (1, 2, 3) AND detalles_equipoMSCloud.errors = "Ninguno"
                    GROUP BY detalles_equipoMSCloud.equipo, consolidadoProcesadoresMSCloud.cpu, consolidadoProcesadoresMSCloud.cores) tabla');
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    /*function totalServidorVirtual($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        INNER JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name
                        AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        INNER JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name
                        AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.os LIKE '%Windows Server%'
                    AND consolidadoTipoEquipoMSCloud.modelo LIKE '%Virtual%'
                    GROUP BY detalles_equipoMSCloud.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function totalServidorVirtual($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(tabla.equipo) AS cantidad
FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        LEFT JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        LEFT JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.os LIKE "%Windows Server%" AND detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND (IF(consolidadoTipoEquipoMSCloud.modelo LIKE "%Virtual%", "Virtual", "Fisico")) = "Virtual" AND detalles_equipoMSCloud.rango IN (1, 2, 3) AND detalles_equipoMSCloud.errors = "Ninguno"
                    GROUP BY detalles_equipoMSCloud.equipo, consolidadoProcesadoresMSCloud.cpu, consolidadoProcesadoresMSCloud.cores) tabla');
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudVM($idDiagnostic, $tipo, $opcion){
        try{
            if($opcion == "newer"){
                $where = " AND detalles_equipoMSCloud.version > 2008 ";
            } else if($opcion == "older"){
                $where = " AND detalles_equipoMSCloud.version <= 2008 ";
            }
            
            if($tipo == "basic"){
                $where1 = " AND consolidadoProcesadoresMSCloud.cores <= 8 ";
            } else if($tipo == "low"){
                $where1 = " AND consolidadoProcesadoresMSCloud.cores > 8 AND consolidadoProcesadoresMSCloud.cores <= 16 ";
            } else if($tipo == "standard"){
                $where1 = " AND consolidadoProcesadoresMSCloud.cores > 16 ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        INNER JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name
                        AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        INNER JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name
                        AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.os LIKE '%Windows Server%' " . $where . "
                    AND consolidadoTipoEquipoMSCloud.modelo LIKE '%Virtual%' " . $where1 . " AND detalles_equipoMSCloud.rango IN (1, 2, 3)
                        AND detalles_equipoMSCloud.errors = 'Ninguno'
                    GROUP BY detalles_equipoMSCloud.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudVMBasic($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        INNER JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name
                        AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        INNER JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name
                        AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.os LIKE '%Windows Server%'
                    AND consolidadoTipoEquipoMSCloud.modelo LIKE '%Virtual%' AND consolidadoProcesadoresMSCloud.cores <= 4
                    GROUP BY detalles_equipoMSCloud.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudVMLowPriority($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        INNER JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name
                        AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        INNER JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name
                        AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.os LIKE '%Windows Server%'
                    AND consolidadoTipoEquipoMSCloud.modelo LIKE '%Virtual%' AND consolidadoProcesadoresMSCloud.cores >= 8 
                    AND consolidadoProcesadoresMSCloud.cores <= 16 
                    GROUP BY detalles_equipoMSCloud.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudVMStandard($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT detalles_equipoMSCloud.equipo
                    FROM detalles_equipoMSCloud
                        INNER JOIN consolidadoTipoEquipoMSCloud ON detalles_equipoMSCloud.equipo = consolidadoTipoEquipoMSCloud.host_name
                        AND detalles_equipoMSCloud.idDiagnostic = consolidadoTipoEquipoMSCloud.idDiagnostic
                        INNER JOIN consolidadoProcesadoresMSCloud ON consolidadoTipoEquipoMSCloud.host_name = consolidadoProcesadoresMSCloud.host_name
                        AND consolidadoProcesadoresMSCloud.cpu > 0 AND detalles_equipoMSCloud.idDiagnostic = consolidadoProcesadoresMSCloud.idDiagnostic
                    WHERE detalles_equipoMSCloud.idDiagnostic = :idDiagnostic AND detalles_equipoMSCloud.os LIKE '%Windows Server%'
                    AND consolidadoTipoEquipoMSCloud.modelo LIKE '%Virtual%' AND consolidadoProcesadoresMSCloud.cores > 16 
                    GROUP BY detalles_equipoMSCloud.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
}