<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/cotizacion/proveedor/procesos/nueva.php");
require_once($GLOBALS["app_root"] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1   = 0;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="divMenuContenido">
            <?php
            $menuCotizacion = 2;
            include_once($GLOBALS['app_root'] . "/cotizacion/proveedor/plantillas/menu.php");
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php include_once($GLOBALS["app_root"] . "/cotizacion/proveedor/plantillas/nueva.php"); ?>  
            </div>
        </div>
    </div>
</section>
<?php
require_once($GLOBALS['app_root'] . "/plantillas/pie.php");