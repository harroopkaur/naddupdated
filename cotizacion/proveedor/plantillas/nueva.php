<div style="padding:20px; overflow:hidden;">
<form id="form1" name="form1" method="post">
    <input type="hidden" id="token" name="token">
    <p>En Licensing Assurance, LLC nos importa que administre bien los recursos financieros al momento 
    de adquirir nuevas licencias de software.</p>       

    <br>

    <p>Solicite en línea una cotización y nos encargaremos de encontrar para usted la mejor opción según sus requerimiento.</p>

    <br>

    <p>Por favor complete el siguiente formulario de solicitud:</p>

    <div style="margin-top:15px;">
        <div style="display:inline-block; width:150px;">Número de Solicitud:</div>
        <div style="display:inline-block;"><input type="text" id="nroCotizacion" value="<?= $nroCotizacion + 1 ?>"  readonly></div>
    </div>

    <div style="margin-top:15px;"> 
        <div style="display:inline-block; width:150px;">Fabricante:</div>
        <div style="display:inline-block">
            <select id="fabricante" name="fabricante">
                <option value="">--Seleccione--</option>
                <?php 
                foreach($fabricante as $row){
                ?>
                    <option value="<?= $row["nombre"] ?>"><?= $row["nombre"] ?></option>
                <?php
                }    
                ?>
            </select>
        </div>
    </div>

    <div style="margin-top:15px;">
        <div style="float:left; width:83%;">
            <table style="width:100%; margin: 0;" class="tablap" id="tablaDetalle">
                <thead>
                <tr style="background:#333; color:#fff;">
                    <th align="center" valign="middle"><span>Descripción</span></th>
                    <th align="center" valign="middle" style="width:100px;"><span>Cantidad</span></th>
                    <th align="center" valign="middle" style="width:50px;"><span>&nbsp;</span></th>
                </tr>
                </thead>
                <tbody id="tablaCotizacion">
                </tbody>
            </table>
        </div>
        <div style="float:left; width:15%; margin-top:-5px;">
            <div class="botones_m2 boton1" id="agrProducto">Agregar</div>
        </div>                                            
    </div>

    <br style="clear:both;">
    <div style="margin-top:15px;">
        <div style="float:right; margin-right:10px;"><div class="botones_m2 boton1" id="enviarCotizacion">Enviar Solicitud</div></div>
        <div style="float:right;"><div class="botones_m2 boton1" id="cancelarCotizacion">Cancelar</div></div>
    </div>
</form>
</div>
                               
<div class="modal-personal" id="modal-cotizacion">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Productos</p></h1>
    </div>
    <div class="modal-personal-body">
        <input type="hidden" id="tokenContratos" name="token">

        <table style="margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th style="width:40px;">&nbsp;</th>
                    <th  align="center" valign="middle" ><input type="text" id="filtroFamilia" name="filtroFamilia" style="width:120px;" value=""></th>
                    <th  align="center" valign="middle" ><input type="text" id="filtroEdicion" name="filtroEdicion" style="width:120px;" value=""></th>
                    <th  align="center" valign="middle" ><input type="text" id="filtroVersion" name="filtroVersion" style="width:120px;" value=""></th>
                    <th  align="center" valign="middle" class="til"><div id="buscar" class="pointer" style="background-color:#000000; color:#FFF; border-radius: 15px;padding:3px;width:100px;">Buscar</div></th>
                </tr>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th style="width:40px;">&nbsp;</th>
                    <th  align="center" valign="middle" ><strong class="til" id="fabricante" name="fabricante">Familia</strong></th>
                    <th  align="center" valign="middle" ><strong class="til" id="familia" name="familia">Edición</strong></th>
                    <th  align="center" valign="middle" ><strong class="til" id="edicion" name="edicion">Versión</strong></th>
                    <th  align="center" valign="middle" class="til" >&nbsp;</th>
                </tr> 
            </thead>
            <tbody id="bodyTable">
            </tbody>
        </table>

        <div class="text-center" id="paginacion"></div>                    
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirListado">Salir</div>
        <div class="boton1 botones_m2" id="cargarProducto" style="float:right;">Cargar</div>
    </div>
</div>
       
<script type="text/javascript">
    var fila = 0;
    $(document).ready(function () {
        $("#agrProducto").click(function(){
            if($("#fabricante").val() === ""){
                $.alert.open('alert', "Debe seleccionar el fabricante", {'Aceptar' : 'Aceptar'}, function() {
                    $("#fabricante").focus();
                });
                return false;
            }
            $("#fondo1").show();
            filtrarProducto(1);
            $("#modal-cotizacion").show();
        });

        $("#filtroFamilia, #filtroEdicion, #filtroVersion").keyup(function(e){
            if(e.keyCode === 13){
                filtrarProducto(1);
            }
        });

        $("#buscar").click(function(){
            filtrarProducto(1);
        });

        $("#salirListado").click(function(){
            $("#fondo1").hide();
            $("#filtroFamilia").val(""); 
            $("#filtroEdicion").val("");
            $("#filtroVersion").val("");
            $("#modal-cotizacion").hide();
        });

        $("#cancelarCotizacion").click(function(){
            $("#fabricante").val("");
            $("#tablaCotizacion").empty();
            fila = 0;
        });

        $("#cargarProducto").click(function(){
            var aux;
            var campos = "";

            for(i = 0; i < $("#bodyTable tr").length; i++){
                if($("#producto" + i).prop("checked")){
                    aux = $("#producto" + i).val();
                    break;
                }
            }
            arrayAux = aux.split("*");


            for(i = 0; i < fila; i++){
                if($("#prodAgred" + i).val() === arrayAux[0] + " " + arrayAux[1] + " " + arrayAux[2]){
                    $("#fondo1").hide();
                    $("#modal-cotizacion").hide();
                    $.alert.open('alert', "Este producto ya fue agregado", {'Aceptar' : 'Aceptar'}, function() {
                    });
                    return false;
                }
            }                  

            $("#fondo1").hide();
            $("#modal-cotizacion").hide();

            campos += "<tr id='row" + fila + "' name='row" + fila + "'>";
            campos += "<td><input type='hidden' id='prodAgred" + fila + "' name='prodAgred[]' value='" + arrayAux[0] + " " + arrayAux[1] + " " + arrayAux[2] + "'>" + arrayAux[0] + " " + arrayAux[1] + " " + arrayAux[2] + "</td>";
            campos += "<td><input type='text' id='cant" + fila + "' name='cant[]' style='width:70px; text-align:right;' value='0' onclick='limpiarCampo(" + fila + ")'></td>";
            campos += '<td><a href="#" onclick="eliminar(' + fila + ')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>';
            campos += "</tr>";

            $("#tablaCotizacion").append(campos);
            $("#cant" + fila).numeric(false);
            fila++;
        });

        $("#enviarCotizacion").click(function(){
            if($("#tablaCotizacion tr").length === 0){
                $.alert.open('alert', "No existen productos", {'Aceptar' : 'Aceptar'}, function() {
                });
                return false;
            }

            for(i = 0; i < fila; i++){
                if(parseInt($("#cant" + i).val()) === 0 || $("#cant" + i).val() === ""){
                    $.alert.open('alert', "Existen Productos con cantidad 0", {'Aceptar' : 'Aceptar'}, function() {
                        $("#cant" + i).val("");
                        $("#cant" + i).focus();
                    });
                    return false;
                }
            }

            $("#fondo").show();
            $("#token").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form1")[0]);	
            $.ajax({
                type: "POST",
                url: "ajax/guardar.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    result = data[0].result;
                    if(result === 0){
                        $.alert.open("alert", "No se pudo guardar la cabecera de la cotización", {"Aceptar" : "Aceptar"}, function() {
                        });
                    }
                    else if(result === 1){
                        $.alert.open("info", "Solicitud de cotización enviada con éxito", {"Aceptar" : "Aceptar"}, function() {
                            $("#cancelarCotizacion").click();
                            $("#nroCotizacion").val(data[0].nroCotizacion);
                        });
                    }
                    else if(result === 2){
                        $.alert.open("alert", "No se guardó todo el detalle de la cotización", {"Aceptar" : "Aceptar"}, function() {
                        });
                    }
                    $("#fondo").hide();
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });
    });

    function filtrarProducto(pagina){
        $.post("ajax/listaProductos.php", { fab : $("#fabricante").val(), fam : $("#filtroFamilia").val(), 
        edi : $("#filtroEdicion").val(), ver : $("#filtroVersion").val(), pagina : pagina, token : localStorage.licensingassuranceToken }, function(data){
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                
            $("#bodyTable").empty();
            $("#paginacion").empty();

            $("#bodyTable").append(data[0].tabla);
            $("#paginacion").append(data[0].paginacion);
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo1").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        }); 
    }

    function selecProducto(indice){
        for(i = 0; i < $("#bodyTable tr").length; i++){
            $("#producto" + i).prop("checked", "");
        }

        $("#fila"+indice).css("color", "#000000");
        $("#producto" + indice).prop("checked", "checked");
    }

    function eliminar(indice){
        $("#row" + indice).remove();
        if($("#tablaCotizacion tr").length === 0){
            fila = 0;
        }
    }

    function limpiarCampo(indice){
        $("#cant" + indice).val("");
    }
</script>