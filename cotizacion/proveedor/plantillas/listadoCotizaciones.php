<div style="padding:20px; overflow:hidden;">
    <p>A continuación encontrará la o las cotizaciones según las solicitudes que usted haya realizado a través de este módulo</p>
    <br>

    <table style="width:calc(100% - 20px); margin: 0;" class="tablap" id="tablaDetalle">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th align="center" valign="middle"><span>Nro. Solicitud</span></th>
            <th align="center" valign="middle"><span>Fecha Solicitud</span></th>
            <th align="center" valign="middle"><span>Fabricante</span></th>
            <th align="center" valign="middle"><span>Nro. Cotización</span></th>
            <th align="center" valign="middle"><span>Proveedor</span></th>
            <th align="center" valign="middle"><span>Estatus</span></th>
            <th align="center" valign="middle"><span>Ofertas</span></th>
            <th align="center" valign="middle" style="width:60px;"><span></span></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            foreach($tabla as $row){
            ?>
                <tr>
                    <td align="center" valign="middle"><?= $row["id"] ?></td>
                    <td align="center" valign="middle"><?= $row["fechaCot"] ?></td>
                    <td align="center" valign="middle"><?= $row["fabricante"] ?></td>
                    <td align="center" valign="middle"><?= $row["nroCotizacion"] ?></td>
                    <td align="center" valign="middle"><?= $row["proveedor"] ?></td>
                    <td align="center" valign="middle"><?= $row["estatus"] ?></td>
                    <td align="center" valign="middle"><a href="#" onclick="descargar('<?= $row["archivo"] ?>')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_181_download_alt.png" border="0" alt="Descargar" title="Descargar" style="<?php if($row['archivo'] ==  ''){ echo 'display:none;'; } ?>"/></a></td>
                    <td align="center" valign="middle"><a href="#" onclick="eliminar(<?= $row["id"] ?>, '<?= $row["idCotProv"] ?>')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
                                    
<script type="text/javascript">
    function eliminar(nroCotizacion, respCotizacion){
        $.post("ajax/eliminar.php", { nroCotizacion : nroCotizacion, respCotizacion : respCotizacion, token : localStorage.licensingassuranceToken }, function(data){
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#fondo").hide();
            if(data[0].result === 0){
                $.alert.open('alert', "No se pudo eliminar la cotización", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else{
                $.alert.open('info', "Cotización eliminada con éxito", {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?= $GLOBALS["domain_root"] ?>/cotizacion/proveedor/listadoCotizaciones.php";
                });
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo1").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        }); 
    }

    function descargar(archivo){
        window.open("<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/archivos/" + archivo);
    }
</script>