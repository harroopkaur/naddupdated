<?php
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_proveedor.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

$cotizacion = new cotizacionProveedor();
$tabla = $cotizacion->listadoCotizaciones($_SESSION["client_id"], $_SESSION["client_empleado"]);
$general = new General();