<?php
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_proveedor.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware


$configuracion = new configuraciones();
$fabricante = $configuracion->listar_fabricantes("");

$cotizacion = new cotizacionProveedor();
$nroCotizacion = $cotizacion->obtenerNroCotizacion();
$general = new General();