<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_proveedor.php");
require_once($GLOBALS["app_root"] . '/clases/clase_detalle_cotizacion_prov.php');
require_once($GLOBALS["app_root"] . '/clases/clase_equivalencias.php');

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $result = 0;
            
            $cabecera = new cotizacionProveedor();
            $detalle =  new detalleCotizacionProv();
            $fab = new Equivalencias();
            
            $fabricante = $general->get_escape($_POST["fabricante"]);
            $idFabricante = $fab->obtenerIdFabNombre($fabricante);

            if($cabecera->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $idFabricante)){
                $idCotizacion = $cabecera->obtenerNroCotizacion();
                $productos = $_POST["prodAgred"];
                $cantidades = $_POST["cant"];
                
                $registrado = 0;
                for($i = 0; $i < count($productos); $i++){
                    $prod = $general->get_escape($productos[$i]);
                    $cant = 0;
                    if(filter_var($cantidades[$i], FILTER_VALIDATE_INT)){
                        $cant = $cantidades[$i];
                    }

                    if($detalle->insertar($idCotizacion, $prod, $cant)){
                        $registrado++;
                    }
                }
                
                $result = 1;
                if($registrado != count($productos)){
                    $result = 2;
                }
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'nroCotizacion'=>$idCotizacion + 1, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);