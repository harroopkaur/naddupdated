<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_proveedor.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $cotizacion = new cotizacionProveedor();
            $id = 0;
            if(isset($_POST["nroCotizacion"]) && filter_var($_POST["nroCotizacion"], FILTER_VALIDATE_INT)){
                $id = $_POST["nroCotizacion"];
            }
            
            $idRespCotizacion = "";
            if(isset($_POST["respCotizacion"]) && filter_var($_POST["respCotizacion"], FILTER_VALIDATE_INT)){
                $idRespCotizacion = $_POST["respCotizacion"];
            }
            
            if($idRespCotizacion == ""){
                $archivos = $cotizacion->CotizacionesEspecificaId($id, $idRespCotizacion);
                foreach($archivos as $row){
                    if($row["archivo"] != "" && file_exists ( $GLOBALS['app_root'] . "/adminweb/cotizaciones/archivos/" . $row["archivo"])){
                        unlink($GLOBALS['app_root'] . "/adminweb/cotizaciones/archivos/" . $row["archivo"]);
                    }
                }
            }
            else{
                $archivos = $cotizacion->listadoCotizacionesId($id);
                foreach($archivos as $row){
                    if($row["archivo"] != "" && file_exists ( $GLOBALS['app_root'] . "/adminweb/cotizaciones/archivos/" . $row["archivo"])){
                        unlink($GLOBALS['app_root'] . "/adminweb/cotizaciones/archivos/" . $row["archivo"]);
                    }
                }
            }
            
            $result = $cotizacion->eliminar($id, $idRespCotizacion);
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);