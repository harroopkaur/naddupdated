<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . '/clases/paginacion.php');

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $productos = new Equivalencias(); 
            $general = new General(); 
            
            $pagina = 1;
            if(isset($_POST["pagina"]) && filter_var($_POST["pagina"], FILTER_VALIDATE_INT)){
                $pagina = $_POST["pagina"];
            }
            
            $fab = "Microsoft";
            if(isset($_POST['fab'])){
                $fab = $general->get_escape($_POST['fab']);
            }

            $fam = '';
            if(isset($_POST['fam'])){
                $fam = $general->get_escape($_POST['fam']);
            }

            $edi = '';
            if(isset($_POST['edi'])){
                $edi = $general->get_escape($_POST['edi']);
            }

            $ver = '';
            if(isset($_POST['ver'])){
                $ver = $general->get_escape($_POST['ver']);
            }
            
            $listado = $productos->listar_todo_paginado($fab, $fam, $edi, $ver, $pagina - 1, $general->limit_paginacion);
            $count = $productos->total($fab, $fam, $edi, $ver); 
            
            $tabla = "";
            $i = 0;
            foreach($listado as $row){
                $tabla .= '<tr class="pointer" onclick="selecProducto(' . $i . ');">
                    <td align="center"><input type="radio" id="producto' . $i . '" name="producto" value="' . $row["familia"] . '*' . $row["edicion"] . '*' . $row["version"] . '"></td>
                    <td align="left">' . $row["familia"] . '</td>
                    <td align="left">' . $row["edicion"] . '</td>
                    <td align="left">' . $row["version"] . '</td>
                    <td align="center" valign="middle" class="til" >&nbsp;</td>
                </tr>';
                $i++;
            }	
            
            $paginacion = new Paginacion($count, $pagina, 'filtrarProducto');
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'paginacion'=>$paginacion->get_pag(), 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);