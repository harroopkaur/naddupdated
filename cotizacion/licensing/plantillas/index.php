<?php 
if($exito == 1){
?>
    <script>
        $.alert.open('info', 'Solicitud enviada con éxito. Muchas gracias por su interés, en poco tiempo será contactado por personal calificado para atender su solicitud.  Saludos Cordiales!', {'Aceptar': 'Aceptar'}, function(button) {
        });
    </script>
<?php
}
else if($exito == -1){
?>
    <script>
        $.alert.open('alert', 'No se pudo agregar la información, ni enviar el correo', {'Aceptar': 'Aceptar'}, function(button) {
        });
    </script>
<?php
}
else if($exito == 2){
?>
    <script>
        $.alert.open('alert', 'No se pudo enviar el correo', {'Aceptar': 'Aceptar'}, function(button) {
        });
    </script>
<?php
}
?>
        
<div style="padding:20px; overflow:hidden;">
    <form id="form1" name="form1" method="post">
        <input type="hidden" id="insertar" name="insertar" value="1">

        <?php $validator->print_script(); ?>
        <h1 class="text-center textog negro">Solicitud de Cotización</h1>
        <br>

        <p class="text-justify" style="padding-right:20px;">Estimado cliente a través de este módulo usted podrá solicitar una contización de nuestros servicios 
        de Licenciamieto de Software. Por favor llene los datos a continuación y en poco tiempo será contactado 
        por personal calificado que gustosamente atenderá su solicitud. Muchas gracias.</p>

        <br>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Nombre del Solicitante:</div>
            <div class="divCotizLicensing"><input type="text" id="nombre" name="nombre" maxlength="70"></div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nombre") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Correo Electrónico:</div>
            <div class="divCotizLicensing"><input type="text" id="email" name="email" maxlength="80"></div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Teléfono de Contacto:</div>
            <div class="divCotizLicensing"><input type="text" id="telefono" name="telefono" maxlength="70"></div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_telefono") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Horario de Contacto:</div>
            <div class="divCotizLicensing"><input type="text" id="horario" name="horario" maxlength="70"></div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_horario") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Nivel de Servicio a Contratar:</div>
            <div class="divCotizLicensing">
                <select id="nivelServicio" name="nivelServicio">
                    <option value="">--Seleccione--</option>
                    <?php 
                    foreach($tablaNivelServicio as $row){
                    ?>
                        <option value="<?= $row['id'] ?>"><?= $row['descripcion'] ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nivelServicio") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Cantidad de PCs:</div>
            <div class="divCotizLicensing"><input type="text" id="pcs" name="pcs" maxlength="3" require></div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pcs") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Cantidad de Servidores:</div>
            <div class="divCotizLicensing"><input type="text" id="servidores" name="servidores" maxlength="3"></div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_servidores") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Fabricante(s):</div>
            <div class="divCotizLicensing"><input type="text" id="fabricante" name="fabricante" maxlength="100"></div>
        </div>
        <div class="contenedorFlex">
            <div class="divCotizLicensing"></div>
            <div class="divCotizLicensing"><div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fabricante") ?></font></div></div>
        </div>

        <div class="contenedorFlex">
            <div class="divCotizLicensing">Otro Dato Relevante:</div>
            <div class="divCotizLicensing"><input type="text" id="otroDato" name="otroDato" maxlength="70"></div>
        </div>
        <br>
        <div style="float:right; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="validate();">Enviar</div></div>
    </form>
</div>
                                
<script type="text/javascript">
    $(document).ready(function(){
        $("#pcs").numeric(false);
        $("#servidores").numeric(false);
    });
</script>