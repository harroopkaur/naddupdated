<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_nivel_servicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_licensing.php");
require_once($GLOBALS["app_root"] . "/clases/clase_email.php");

$nivelServicio = new nivelServicio();
$tablaNivelServicio = $nivelServicio->listar();
$general = new General();
$email = new email();

$validator = new validator("form1");
$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);
$validator->create_message("msj_telefono", "telefono", " Obligatorio", 0);
$validator->create_message("msj_horario", "horario", " Obligatorio", 0);
$validator->create_message("msj_nivelServicio", "nivelServicio", " Obligatorio", 0);
$validator->create_message("msj_pcs", "pcs", " Obligatorio", 0);
$validator->create_message("msj_servidores", "servidores", " Obligatorio", 0);
$validator->create_message("msj_fabricante", "fabricante", " Obligatorio", 0);

$exito = 0;
if(isset($_POST["insertar"]) && filter_var($_POST["insertar"], FILTER_VALIDATE_INT) !== false){
    $cotizacion = new cotizacionLicensing();
    $nombre = "";
    if(isset($_POST["nombre"])){
        $nombre = $general->get_escape($_POST["nombre"]);
    }
    
    $correo = "";
    if(isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false){
        $correo = $general->get_escape($_POST["email"]);
    }
    
    $telefono = "";
    if(isset($_POST["telefono"])){
        $telefono = $general->get_escape($_POST["telefono"]);
    }
    
    $horario = "";
    if(isset($_POST["horario"])){
        $horario = $general->get_escape($_POST["horario"]);
    }
    
    $nivel = 0;
    if(isset($_POST["nivelServicio"]) && filter_var($_POST["nivelServicio"], FILTER_VALIDATE_INT) !== false){
        $nivel = $_POST["nivelServicio"];
    }
    
    $pcs = 0;
    if(isset($_POST["pcs"]) && filter_var($_POST["pcs"], FILTER_VALIDATE_INT) !== false){
        $pcs = $_POST["pcs"];
    }
    
    $servidores = 0;
    if(isset($_POST["servidores"]) && filter_var($_POST["servidores"], FILTER_VALIDATE_INT) !== false){
        $servidores = $_POST["servidores"];
    }
    
    $fabricantes = "";
    if(isset($_POST["fabricante"])){
        $fabricantes = $general->get_escape($_POST["fabricante"]);
    }
    
    $otros = "";
    if(isset($_POST["otroDato"])){
        $otros = $general->get_escape($_POST["otroDato"]);
    }
    
    $exito = $cotizacion->insertar($nombre, $correo, $telefono, $horario, $nivel, $pcs, $servidores, $fabricantes, $otros);
    if($exito === 1){
        $row = $nivelServicio->nivelServicioEspecifico($nivel);
        
        $mensaje = utf8_decode("El cliente " . $nombre . ", ha solicitado una cotización con las siguientes caracteristicas:<br>");
        $mensaje .= utf8_decode("Nivel de Servicio: " . $row["descripcion"] . "<br>");
        $mensaje .= utf8_decode("Fabricante(s): " . $fabricantes . "<br>");
        $mensaje .= utf8_decode("Cantidad de Pcs: " . $pcs . "<br>");
        $mensaje .= utf8_decode("Cantidad de Servidores: " . $servidores . "<br>");
        $mensaje .= utf8_decode("Otros Datos: " . $otros . "<br>");
        $mensaje .= utf8_decode("Teléfono de Contacto: " . $telefono . "<br>");
        $mensaje .= utf8_decode("Horario de Contacto: " . $horario . "<br>");
        
        if($email->enviar_cotizacionLiensing($mensaje) === true){
            $exito = 1;
        } 
        else{
            $exito = 2;
        }
    }
}