<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/unixIbm/procesos/detalle.php");
require_once($GLOBALS['app_root'] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 7;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="divMenuContenido">
            <?php
            $menuUnixIBM = 4;
            include_once($GLOBALS['app_root'] . "/unixIbm/plantillas/menu.php");
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php 
                include_once($GLOBALS['app_root'] . "/unixIbm/graficos/detalle.php");
                include_once($GLOBALS['app_root'] . "/unixIbm/plantillas/detalle.php");
                ?>  
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS['app_root'] . "/plantillas/pie.php");