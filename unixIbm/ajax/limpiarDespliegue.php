<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_solaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_AIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_linux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_UnixSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_UnixSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_UnixLinux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixLinux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_UnixLinux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_UnixAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_UnixAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixIBM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_UnixIBM.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $archivosDespliegue = new clase_archivos_fabricantes();
            $pvuSolaris = new pvuClienteSolaris();
            $pvuAIX = new pvuClienteAIX();
            $pvuLinux = new pvuClienteLinux();
            $resumenSolaris      = new resumenUnixSolaris();
            $consolidadoSolaris  = new consolidadoUnixSolaris();
            $detalleSolaris      = new DetallesUnixSolaris();
            $resumenAIX      = new resumenUnixAIX();
            $consolidadoAIX  = new consolidadoUnixAIX();
            $detalleAIX      = new DetallesUnixAIX();
            $resumenLinux      = new resumenUnixLinux();
            $consolidadoLinux  = new consolidadoUnixLinux();
            $detalleLinux      = new DetallesUnixLinux();
            $compras   = new comprasUnixIBM();
            $balance  = new balanceUnixIBM();
                      
            $i = 15;
            $j = 0;
            
            if($consolidadoSolaris->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($consolidadoAIX->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($consolidadoLinux->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($detalleSolaris->eliminar($_SESSION["client_id"], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($detalleAIX->eliminar($_SESSION["client_id"], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($detalleLinux->eliminar($_SESSION["client_id"], $_SESSION['client_empleado'])){
                $j++;
            }

            if($resumenSolaris->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($resumenAIX->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($resumenLinux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
           
            if($archivosDespliegue->eliminarArchivosFabricante($_SESSION["client_id"], $_SESSION["client_empleado"], 7)){
                $j++;
            }
            
            if($pvuSolaris->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($pvuAIX->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($pvuLinux->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($compras->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($balance->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            $result = 0;
            if($i > $j){
                $result = 2;
            } else if($i == $j){
                $result = 1;
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);