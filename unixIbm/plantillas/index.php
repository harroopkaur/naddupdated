<div style="width:98%; padding:10px; overflow:hidden;">
    <h1 class="textog negro" style="margin:20px; text-align:center;">Administraci&oacute;n de Licenciamiento</h1>
    <p style="margin-top:20px; font-size:24px; text-align:center;">Administra tus activos de software en 3 simples pasos</p>
    <br>
    <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/imgGeneral.png" style="margin: 0 auto;width: 85%;display: block;"/>
    <br>
    <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='despliegue1.php';">Despliegue</div></div>
    <div style="float:right;"><div class="botones_m2Alterno boton1" id="limpiar">Limpiar Despliegue</div></div>
</div>

<script>
    $(document).ready(function(){
        $("#limpiar").click(function(){
            $("#fondo").show();
            $.post("ajax/limpiarDespliegue.php", { token : localStorage.licensingassuranceToken }, function (data) {
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
           
            if(data[0].result === 0){
                $.alert.open('alert', "No se pudo eliminar el despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else if(data[0].result === 1){
                $.alert.open('info', "Despliegue eliminado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else{
                $.alert.open('alert', "No se eliminó por completo el despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                });
            }
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
        });
    });
</script>