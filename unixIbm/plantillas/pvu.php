<?php
if ($modificar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'PVU actualizado con éxito', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/unixIbm/pvu.php';
        });
    </script>
<?php  
} else if ($modificar == 1 && $exito == 0){
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar el PVU', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/unixIbm/pvu.php';
        });
    </script>
<?php  
} else if ($modificar == 1 && $exito == 2){
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todos los registros del PVU', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/unixIbm/pvu.php';
        });
    </script>
<?php
}
?>
    
<div style="padding:20px; overflow:hidden;">
    <form id="form1Solaris" name="form1Solaris" method="post" action="pvu.php">
        <input type="hidden" id="modificar" name="modificar" value="1">
        <input type="hidden" id="opc" name="opc" value="Solaris">
        
        <div id="ttabla1Solaris"  style="width:99%; max-height:400px; overflow:auto;">
            <table style="width:1712px; margin-top:-5px;" class="tablap" id="tablaPVUSolaris">
                <thead>
                <tr style="background:#333; color:#fff;">
                    <th rowspan="2" align="center" valign="middle"><span>Servidor</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Tipo de Procesador</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Producto</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Tiene</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Usa</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>PVU</span></th>
                    <th rowspan="2" align="center" valign="middle">Conteo<span></span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Total PVU</span></th>
                </tr>
                <tr style="background:#333; color:#fff;">
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                </tr>
                </thead>
                <tbody id="tablaSolaris">
                    <?php 
                    $i=0;
                    foreach($tablaPVUSolaris as $row){
                    ?>
                        <tr>
                            <td><input type="hidden" name="idPvuClienteSolaris[]" value="<?= $row["id"] ?>"><?= $row["servidor"] ?></td>
                            <td style="width:150px;"><?= $row["tipoProcesador"] ?></td>
                            <td style="width:150px;"><?= $row["producto"] ?></td>
                            <td class="text-center"><?= $row["conteoProcesadores"] ?></td>
                            <td class="text-center"><?= $row["conteoCore"] ?></td>
                            <td><input type="text" id="usaProcSolaris<?= $i ?>" name="usaProcSolaris[]" value="<?= $row["usaProcesadores"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaProcSolaris<?= $i ?>')" readonly></td>
                            <td><input type="text" id="usaCoreSolaris<?= $i ?>" name="usaCoreSolaris[]" value="<?= $row["usaCore"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaCoreSolaris<?= $i ?>')" readonly></td>
                            <td class="text-center"><?= $row["pvu"] ?></td>
                            <td class="text-center"><?= $row["conteo"] ?></td>
                            <td class="text-center"><?= $row["totalPvu"] ?></td>
                        </tr>
                    <?php 
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <br>
        <div style="float:left; margin-left:0;" class="botonesSAM boton5" id="editarSolaris">Editar</div>
        <div style="float:left;" class="botonesSAM boton5" id="actualizarSolaris">Actualizar</div>
    </form>
</div>
    
<div style="padding:20px; overflow:hidden;">
    <form id="form1AIX" name="form1AIX" method="post" action="pvu.php">
        <input type="hidden" id="modificar" name="modificar" value="1">
        <input type="hidden" id="opc" name="opc" value="AIX">
        
        <div id="ttabla1AIX"  style="width:99%; max-height:400px; overflow:auto;">
            <table style="width:1712px; margin-top:-5px;" class="tablap" id="tablaPVUAIX">
                <thead>
                <tr style="background:#333; color:#fff;">
                    <th rowspan="2" align="center" valign="middle"><span>Servidor</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Tipo de Procesador</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Producto</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Tiene</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Usa</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>PVU</span></th>
                    <th rowspan="2" align="center" valign="middle">Conteo<span></span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Total PVU</span></th>
                </tr>
                <tr style="background:#333; color:#fff;">
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                </tr>
                </thead>
                <tbody id="tablaAIX">
                    <?php 
                    $i=0;
                    foreach($tablaPVUAIX as $row){
                    ?>
                        <tr>
                            <td><input type="hidden" name="idPvuClienteAIX[]" value="<?= $row["id"] ?>"><?= $row["servidor"] ?></td>
                            <td style="width:150px;"><?= $row["tipoProcesador"] ?></td>
                            <td style="width:150px;"><?= $row["producto"] ?></td>
                            <td class="text-center"><?= $row["conteoProcesadores"] ?></td>
                            <td class="text-center"><?= $row["conteoCore"] ?></td>
                            <td><input type="text" id="usaProcAIX<?= $i ?>" name="usaProcAIX[]" value="<?= $row["usaProcesadores"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaProcAIX<?= $i ?>')" readonly></td>
                            <td><input type="text" id="usaCoreAIX<?= $i ?>" name="usaCoreAIX[]" value="<?= $row["usaCore"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaCoreAIX<?= $i ?>')" readonly></td>
                            <td class="text-center"><?= $row["pvu"] ?></td>
                            <td class="text-center"><?= $row["conteo"] ?></td>
                            <td class="text-center"><?= $row["totalPvu"] ?></td>
                        </tr>
                    <?php 
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <br>
        <div style="float:left; margin-left:0;" class="botonesSAM boton5" id="editarAIX">Editar</div>
        <div style="float:left;" class="botonesSAM boton5" id="actualizarAIX">Actualizar</div>
    </form>
</div>

<div style="padding:20px; overflow:hidden;">
    <form id="form1Linux" name="form1Linux" method="post" action="pvu.php">
        <input type="hidden" id="modificar" name="modificar" value="1">
        <input type="hidden" id="opc" name="opc" value="Linux">
        
        <div id="ttabla1Linux"  style="width:99%; max-height:400px; overflow:auto;">
            <table style="width:1712px; margin-top:-5px;" class="tablap" id="tablaPVULinux">
                <thead>
                <tr style="background:#333; color:#fff;">
                    <th rowspan="2" align="center" valign="middle"><span>Servidor</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Tipo de Procesador</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Producto</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Tiene</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Usa</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>PVU</span></th>
                    <th rowspan="2" align="center" valign="middle">Conteo<span></span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Total PVU</span></th>
                </tr>
                <tr style="background:#333; color:#fff;">
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                </tr>
                </thead>
                <tbody id="tablaLinux">
                    <?php 
                    $i=0;
                    foreach($tablaPVULinux as $row){
                    ?>
                        <tr>
                            <td><input type="hidden" name="idPvuClienteLinux[]" value="<?= $row["id"] ?>"><?= $row["servidor"] ?></td>
                            <td style="width:150px;"><?= $row["tipoProcesador"] ?></td>
                            <td style="width:150px;"><?= $row["producto"] ?></td>
                            <td class="text-center"><?= $row["conteoProcesadores"] ?></td>
                            <td class="text-center"><?= $row["conteoCore"] ?></td>
                            <td><input type="text" id="usaProcLinux<?= $i ?>" name="usaProcLinux[]" value="<?= $row["usaProcesadores"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaProcLinux<?= $i ?>')" readonly></td>
                            <td><input type="text" id="usaCoreLinux<?= $i ?>" name="usaCoreLinux[]" value="<?= $row["usaCore"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaCoreLinux<?= $i ?>')" readonly></td>
                            <td class="text-center"><?= $row["pvu"] ?></td>
                            <td class="text-center"><?= $row["conteo"] ?></td>
                            <td class="text-center"><?= $row["totalPvu"] ?></td>
                        </tr>
                    <?php 
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <br>
        <div style="float:left; margin-left:0;" class="botonesSAM boton5" id="editarLinux">Editar</div>
        <div style="float:left;" class="botonesSAM boton5" id="actualizarLinux">Actualizar</div>
    </form>
</div>
    
<script>
    for(i = 0; i < $("#tablaSolaris tr").length; i++){
        $("#usaProcSolaris" + i).numeric(false);
        $("#usaCoreSolaris" + i).numeric(false);
    }
    
    for(i = 0; i < $("#tablaAIX tr").length; i++){
        $("#usaProcAIX" + i).numeric(false);
        $("#usaCoreAIX" + i).numeric(false);
    }
    
    for(i = 0; i < $("#tablaLinux tr").length; i++){
        $("#usaProcLinux" + i).numeric(false);
        $("#usaCoreLinux" + i).numeric(false);
    }
        
    $(document).ready(function(){        
        $("#tablaPVUSolaris").tableHeadFixer({"left" : 3});
        $("#tablaPVUAIX").tableHeadFixer({"left" : 3});
        $("#tablaPVULinux").tableHeadFixer({"left" : 3});
        
        $("#editarSolaris").click(function(){
            for(i = 0; i < $("#tablaSolaris tr").length; i++){
                $("#usaProcSolaris" + i).removeAttr("readonly");
                $("#usaCoreSolaris" + i).removeAttr("readonly");
            }
            $("#ttabla1Solaris").scrollLeft(300);
        });
        
        $("#editarAIX").click(function(){
            for(i = 0; i < $("#tablaAIX tr").length; i++){
                $("#usaProcAIX" + i).removeAttr("readonly");
                $("#usaCoreAIX" + i).removeAttr("readonly");
            }
            $("#ttabla1AIX").scrollLeft(300);
        });
        
        $("#editarLinux").click(function(){
            for(i = 0; i < $("#tablaLinux tr").length; i++){
                $("#usaProcLinux" + i).removeAttr("readonly");
                $("#usaCoreLinux" + i).removeAttr("readonly");
            }
            $("#ttabla1Linux").scrollLeft(300);
        });
        
        $("#actualizarSolaris").click(function(){
            $("#form1Solaris").submit();
        });
        
        $("#actualizarAIX").click(function(){
            $("#form1AIX").submit();
        });
        
        $("#actualizarLinux").click(function(){
            $("#form1Linux").submit();
        });
    });
    
    function limpiar(variable){
        if(!$("#" + variable).prop("readonly")){
            $("#" + variable).val("");
        }
    }
</script>