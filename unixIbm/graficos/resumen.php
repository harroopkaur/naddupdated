<script type="text/javascript">
    $(function () {
        $('#container3').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Tivoli Storage'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Neto',
                data: [0, 0, <?= $TivoliStorageNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalTivoliStorageUsar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalTivoliStorageCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container4').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'WebSphere'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Neto',
                data: [0, 0, <?= $WebSphereNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalWebSphereUsar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalWebSphereCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container5').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'DB2'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },

            series: [{
                name: 'Neto',
                data: [0, 0, <?= $DB2Neto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalDB2Usar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalDB2Compras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });
    });
</script>