<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixIBM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_UnixIBM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixIBM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

$tablaMaestra = new tablaMaestra();
$balance  = new balanceUnixIBM();
$compras  = new comprasUnixIBM();
$resumen  = new resumenUnixIBM();
$general = new General();
$tablaMaestra->productosSalida(7);
$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';
    
$TotalTivoliStorageCompras = 0;
$TotalTivoliStorageUsar    = 0;

$TotalWebSphereCompras = 0;
$TotalWebSphereUsar    = 0;

$TotalDB2Compras = 0;
$TotalDB2Usar    = 0;

$clientTotalProductCompras = 0;
$clientTotalProductInstal  = 0;
$clientProductNeto         = 0;

//Tivoli Storage
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Tivoli Storage")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalTivoliStorageCompras += $reg_equipos["compra"];
    }
}

$listaTivoli = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "Tivoli Storage");
foreach ($listaTivoli as $reg_equipos) {
    $TotalTivoliStorageUsar  += $reg_equipos["cantidad"];
}

$TotalTivoliStorageInstal = $TotalTivoliStorageUsar;
$TivoliStorageNeto        = $TotalTivoliStorageCompras - $TotalTivoliStorageInstal;


//WebSphere
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "WebSphere")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalWebSphereCompras += $reg_equipos["compra"];
    }
}

$listaWebSphere = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "WebSphere");
foreach ($listaWebSphere as $reg_equipos) {
    $TotalWebSphereUsar  += $reg_equipos["cantidad"];
}

$TotalWebSphereInstal = $TotalWebSphereUsar;
$WebSphereNeto        = $TotalWebSphereCompras - $TotalWebSphereInstal;

//DB2
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "DB2")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalDB2Compras += $reg_equipos["compra"];
    }
}

$listaDB2 = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "DB2");
foreach ($listaDB2 as $reg_equipos) {
    $TotalDB2Usar  += $reg_equipos["cantidad"];
}

$TotalDB2Instal = $TotalDB2Usar;
$DB2Neto        = $TotalDB2Compras - $TotalDB2Instal;