<?php
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_UnixAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_UnixAIX.php");

$resumen      = new resumenUnixAIX();
$consolidado  = new consolidadoUnixAIX();
$detalle      = new DetallesUnixAIX();

$consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
$detalle->eliminar($_SESSION["client_id"], $_SESSION['client_empleado']);
$directorio = 'archivos_csvf2/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp';
$ficheros   = scandir($directorio);

$tablaMaestra->listadoEncontrarCampo(7);
$paquetes  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"AIX","Paquete");
$procesos  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"AIX","Procesos");
$archivos  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"AIX","Archivos");

foreach($ficheros as $row => $value){
    $memoriaEncon   = 0;
    $osEncon        = 0;
    $TIVsmCapiEncon = 0;
    $TIVsmCbaEncon  = 0;
    $mqmEncon       = 0;
    $equipo         = "";
    $os             = "";
    $versionOs      = "";
    $cpu            = 0;
    $version        = "";
    $core           = 0;

    $fichero = 'archivos_csvf2/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/' . $value;
    if($fichero != 'archivos_csvf2/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/.' && $fichero != 'archivos_csvf2/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/..'){
        if(($f = fopen($fichero, "r")) !== FALSE) {
            $packet  = 0;
            $process = 0;
            $file    = 0;
            while(!feof($f)) {
                $linea = fgets($f);
                if(strpos($linea, $tablaMaestra->listaCampos[0]["descripcion"]) !== FALSE){
                    $aux = $tablaMaestra->listaCampos[0]["descripcion"];
                    $equipo = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[7]["descripcion"]) !== FALSE){
                    $aux = $tablaMaestra->listaCampos[7]["descripcion"];
                    $os = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[4]["descripcion"]) !== FALSE){
                    $aux       = $tablaMaestra->listaCampos[4]["descripcion"];
                    $versionOs = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[5]["descripcion"]) !== FALSE){
                    $aux = $tablaMaestra->listaCampos[5]["descripcion"];
                    $cpu = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[6]["descripcion"]) !== FALSE){
                    $aux     = $tablaMaestra->listaCampos[6]["descripcion"];
                    $version = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, "proc".$core) !== FALSE){
                    $core++;
                }
                
                $software = "";
                if(strpos($linea, "Packets Found:") !== FALSE){
                    $packet++;
                }

                if(strpos($linea, "Processes Found:") !== FALSE){
                    $packet = -1;
                    $process++;
                }
                
                if(strpos($linea, "Files Found:") !== FALSE){
                    $process = -1;
                    $file++;
                }

                if($packet > 0){
                    foreach($paquetes as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }

                if($process > 0){
                    foreach($procesos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }

                if($file > 0){
                    foreach($archivos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }
            }

            if($equipo != "" && $os != ""){                               
                $detalle->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $os, $versionOs, $cpu, $version);
            } 
        }
        fclose($f);
    }
}

if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 7) === false){
    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 7);
}

$archivosDespliegue->actualizarDespliegue2($_SESSION["client_id"], $_SESSION['client_empleado'], 7, $nombreArchivo);