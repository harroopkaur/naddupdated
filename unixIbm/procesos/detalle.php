<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixIBM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalenciasPDO.php");

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$tablaMaestra = new tablaMaestra();
$resumen      = new resumenUnixIBM();
$equivalencia = new EquivalenciasPDO();
$general      = new General();

$total_i     = 0;
$vert1 = "";
if(isset($_GET['vert'])){
    $vert  = $general->get_escape($_GET['vert']);
    if(isset($_GET['vert1'])){
        $vert1 = $general->get_escape($_GET['vert1']); 
    }
}
else{
    $vert  = 'Business Process Management';
    $vert1 = '';
}

if($vert != "Otros"){
    
    $ediciones = $equivalencia->edicionesProductoxNombre(7, $vert);
    /*if(!isset($_GET['vert1'])){
        $vert1 = $ediciones[0]["nombre"];
    }*/
    
    $titulo = $vert . ' ' . $vert1;
}

if($vert != "Otros" && $vert1 != ""){    
    $result = $resumen->listar_datos6Agrupado($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1);
} else if($vert != "Otros" && $vert1 == ""){
    $result = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], $vert);
}

if(count($result) > 0){
    $total_i = 0;
    foreach($result as $reg_calculo){
        $total_i += $reg_calculo["cantidad"];
    }	
}

if($vert != "Otros" && $vert1 != ""){
    $listado = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], $vert, $vert1);
} else if ($vert != "Otros" && $vert1 == ""){
    $listado = $resumen->listar_datos5($_SESSION["client_id"], $_SESSION['client_empleado'], $vert);                   
}