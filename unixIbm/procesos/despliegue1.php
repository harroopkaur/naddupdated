<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_solaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_AIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_linux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$tablaMaestra = new tablaMaestra();
$archivosDespliegue = new clase_archivos_fabricantes();
$pvuSolaris = new pvuClienteSolaris();
$pvuAIX = new pvuClienteAIX();
$pvuLinux = new pvuClienteLinux();
$general = new General();
$log = new log();

$exito=0;
$error=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;
$exito4=0;
$error4=0;
$exito5=0;
$error5=0;

$pestana = "solaris";
if(isset($_POST["opcionDespliegue"])){
    $pestana = $_POST["opcionDespliegue"];
}

if(isset($_POST['insertar']) && $_POST['insertar'] == 1 && isset($_POST["opcionDespliegue"])) {
    if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // ValidacioWnes 
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
        }else{
            $error=2;	
        }
    }
    else{
        $error = 1;
    }

    if($error == 0) {
        if($pestana === "solaris"){
            $carpeta = "archivos_csvf1";
        }
        else if($pestana === "aix"){
            $carpeta = "archivos_csvf2";
        }
        else if($pestana === "linux"){
            $carpeta = "archivos_csvf3";
        }

        if(!file_exists($GLOBALS['app_root'] . "/unixIbm/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
            mkdir($GLOBALS['app_root'] . "/unixIbm/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
        }

        if(!file_exists($GLOBALS['app_root'] . "/unixIbm/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp")){
            mkdir($GLOBALS['app_root'] . "/unixIbm/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp", 0777, true);
        }

        $nombreArchivo = "archivosConsolidados" . date("dmYHis") . ".rar";
        move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] . "/unixIbm/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo);
        $rar_file = rar_open($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo);

        if($rar_file === false){
            $error = -1;
        }
        else{

            $entries = rar_list($rar_file);

            foreach ($entries as $entry) {
                $entry->extract($carpeta . '/' . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . '/tmp/');
            }

            rar_close($rar_file);
        }
    }

    if($error == 0){
        if($pestana == "solaris"){
            require_once($GLOBALS["app_root"] . "/unixIbm/procesos/solaris.php");
        }
        else if($pestana == "aix"){
            require_once($GLOBALS["app_root"] . "/unixIbm/procesos/aix.php");
        }
        else if($pestana == "linux"){
            require_once($GLOBALS["app_root"] . "/unixIbm/procesos/linux.php");
        }

        //inicio resumen UNIX-IBM
        $resumen->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        if($resumen->error != ""){
            echo $resumen->error;
        }

        $tablaMaestra->listadoProductosMaestra(7);
        foreach($tablaMaestra->listaProductos as $rowProductos){
            if($consolidado->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["descripcion"], $rowProductos["campo2"])){
                foreach($consolidado->lista as $reg_r){  
                    $producto = "";

                    if($tablaMaestra->productosSustituir(7, $reg_r["product"])){
                        $producto  = $tablaMaestra->sustituir;
                        $idForaneo = $tablaMaestra->idForaneo;
                    }

                    $edicion = "";
                    $version = "";               

                    if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                        if($tablaMaestra->sustituir == null){
                            $version = "";
                        }
                        else{
                            $version = $tablaMaestra->sustituir;
                        }
                    }

                    $tablaMaestra->sustituir = "";
                    if($tablaMaestra->buscarSustituirMaestraCampo2($reg_r["product"], $idForaneo, $pestana, 5)){
                        if($tablaMaestra->sustituir == null){
                            $edicion = "";
                        }
                        else{
                            $edicion = $tablaMaestra->sustituir;
                        }
                    }

                    $resumen->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_r["equipo"], $producto, $edicion, $version);
                    echo $resumen->error;
                } 
            }
        }

        $files = glob($carpeta . '/' . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . '/tmp/*'); // obtiene todos los archivos
        foreach($files as $file => $valor){
            if(is_file($valor)){
                unlink($valor);
            }
        }
        //fin resumen UNIX-IBM
        
        if($pestana == "solaris"){
            //inicio procesar PVU
            $pvuSolaris->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);

            $listado = $pvuSolaris->listadoProcesadoresUnixSolaris($_SESSION["client_id"], $_SESSION["client_empleado"]);

            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            foreach($listado as $row){
                $rowPVU = $pvuSolaris->procesarConsolidadoProcesadores($row["tipo_CPU"]);

                /*if(!$pvu->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $row['host_name'], $row['tipo_CPU'], 
                $row['producto'], $row['cpu'], $row['cores'], 0, 0, $rowPVU['pvu'], 1, $rowPVU['pvu'])){
                    echo $pvu->error;
                }*/

                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :servidor" . $j . ", :tipoProcesador" . $j . ", "
                    . ":producto" . $j . ", :conteoProcesadores" . $j . ", :conteoCore" . $j . ", :usaProcesadores" . $j . ", "
                    . ":usaCore" . $j . ", :pvu" . $j . ", :conteo" . $j . ", :totalPvu" . $j . ")";
                } else {
                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :servidor" . $j . ", :tipoProcesador" . $j . ", "
                    . ":producto" . $j . ", :conteoProcesadores" . $j . ", :conteoCore" . $j . ", :usaProcesadores" . $j . ", "
                    . ":usaCore" . $j . ", :pvu" . $j . ", :conteo" . $j . ", :totalPvu" . $j . ")";
                } 

                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                $bloqueValores[":servidor" . $j] = $row['equipo'];
                $bloqueValores[":tipoProcesador" . $j] =  $row['tipo_CPU']; 
                $bloqueValores[":producto" . $j] = $row['producto'];
                $bloqueValores[":conteoProcesadores" . $j] = $row['cpu'];
                $bloqueValores[":conteoCore" . $j] = $row['core'];
                $bloqueValores[":usaProcesadores" . $j] = 0;
                $bloqueValores[":usaCore" . $j] = 0;
                $bloqueValores[":pvu" . $j] = $rowPVU['pvu'];
                $bloqueValores[":conteo" . $j] = 1; 
                $bloqueValores[":totalPvu" . $j] = $rowPVU['pvu'];

                if($j == $general->registrosBloque){
                    if(!$pvuSolaris->insertarEnBloque($bloque, $bloqueValores)){ 
                        echo "pvu: " . $pvuSolaris->error;
                    }

                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
            }

            if($insertarBloque === true){
                if(!$pvuSolaris->insertarEnBloque($bloque, $bloqueValores)){    
                    echo "pvu: " . $pvuSolaris->error;
                }
            }
            //fin procesar PVU
            
            $log->insertar(20, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue Solaris");
        }
        else if($pestana == "aix"){
            //inicio procesar PVU
            $pvuAIX->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);

            $listado = $pvuAIX->listadoProcesadoresUnixAIX($_SESSION["client_id"], $_SESSION["client_empleado"]);

            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            foreach($listado as $row){
                $rowPVU = $pvuAIX->procesarConsolidadoProcesadores($row["tipo_CPU"]);

                /*if(!$pvu->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $row['host_name'], $row['tipo_CPU'], 
                $row['producto'], $row['cpu'], $row['cores'], 0, 0, $rowPVU['pvu'], 1, $rowPVU['pvu'])){
                    echo $pvu->error;
                }*/

                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :servidor" . $j . ", :tipoProcesador" . $j . ", "
                    . ":producto" . $j . ", :conteoProcesadores" . $j . ", :conteoCore" . $j . ", :usaProcesadores" . $j . ", "
                    . ":usaCore" . $j . ", :pvu" . $j . ", :conteo" . $j . ", :totalPvu" . $j . ")";
                } else {
                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :servidor" . $j . ", :tipoProcesador" . $j . ", "
                    . ":producto" . $j . ", :conteoProcesadores" . $j . ", :conteoCore" . $j . ", :usaProcesadores" . $j . ", "
                    . ":usaCore" . $j . ", :pvu" . $j . ", :conteo" . $j . ", :totalPvu" . $j . ")";
                } 

                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                $bloqueValores[":servidor" . $j] = $row['equipo'];
                $bloqueValores[":tipoProcesador" . $j] =  $row['tipo_CPU']; 
                $bloqueValores[":producto" . $j] = $row['producto'];
                $bloqueValores[":conteoProcesadores" . $j] = $row['cpu'];
                $bloqueValores[":conteoCore" . $j] = $row['core'];
                $bloqueValores[":usaProcesadores" . $j] = 0;
                $bloqueValores[":usaCore" . $j] = 0;
                $bloqueValores[":pvu" . $j] = $rowPVU['pvu'];
                $bloqueValores[":conteo" . $j] = 1; 
                $bloqueValores[":totalPvu" . $j] = $rowPVU['pvu'];

                if($j == $general->registrosBloque){
                    if(!$pvuAIX->insertarEnBloque($bloque, $bloqueValores)){ 
                        echo "pvu: " . $pvuAIX->error;
                    }

                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
            }

            if($insertarBloque === true){
                if(!$pvuAIX->insertarEnBloque($bloque, $bloqueValores)){    
                    echo "pvu: " . $pvuAIX->error;
                }
            }
            //fin procesar PVU
            
            $log->insertar(20, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue AIX");
        }
        else if($pestana == "linux"){
            //inicio procesar PVU
            $pvuLinux->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);

            $listado = $pvuLinux->listadoProcesadoresUnixLinux($_SESSION["client_id"], $_SESSION["client_empleado"]);

            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            foreach($listado as $row){
                $rowPVU = $pvuLinux->procesarConsolidadoProcesadores($row["tipo_CPU"]);

                /*if(!$pvu->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $row['host_name'], $row['tipo_CPU'], 
                $row['producto'], $row['cpu'], $row['cores'], 0, 0, $rowPVU['pvu'], 1, $rowPVU['pvu'])){
                    echo $pvu->error;
                }*/

                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :servidor" . $j . ", :tipoProcesador" . $j . ", "
                    . ":producto" . $j . ", :conteoProcesadores" . $j . ", :conteoCore" . $j . ", :usaProcesadores" . $j . ", "
                    . ":usaCore" . $j . ", :pvu" . $j . ", :conteo" . $j . ", :totalPvu" . $j . ")";
                } else {
                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :servidor" . $j . ", :tipoProcesador" . $j . ", "
                    . ":producto" . $j . ", :conteoProcesadores" . $j . ", :conteoCore" . $j . ", :usaProcesadores" . $j . ", "
                    . ":usaCore" . $j . ", :pvu" . $j . ", :conteo" . $j . ", :totalPvu" . $j . ")";
                } 

                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                $bloqueValores[":servidor" . $j] = $row['equipo'];
                $bloqueValores[":tipoProcesador" . $j] =  $row['tipo_CPU']; 
                $bloqueValores[":producto" . $j] = $row['producto'];
                $bloqueValores[":conteoProcesadores" . $j] = $row['cpu'];
                $bloqueValores[":conteoCore" . $j] = $row['core'];
                $bloqueValores[":usaProcesadores" . $j] = 0;
                $bloqueValores[":usaCore" . $j] = 0;
                $bloqueValores[":pvu" . $j] = $rowPVU['pvu'];
                $bloqueValores[":conteo" . $j] = 1; 
                $bloqueValores[":totalPvu" . $j] = $rowPVU['pvu'];

                if($j == $general->registrosBloque){
                    if(!$pvuLinux->insertarEnBloque($bloque, $bloqueValores)){ 
                        echo "pvu: " . $pvuLinux->error;
                    }

                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
            }

            if($insertarBloque === true){
                if(!$pvuLinux->insertarEnBloque($bloque, $bloqueValores)){    
                    echo "pvu: " . $pvuLinux->error;
                }
            }
            //fin procesar PVU
            
            $log->insertar(20, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue Linux");
        }
        
        $exito=1;  
    }
}