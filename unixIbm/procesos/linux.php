<?php
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_UnixLinux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixLinux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_UnixLinux.php");

$resumen      = new resumenUnixLinux();
$consolidado  = new consolidadoUnixLinux();
$detalle      = new DetallesUnixLinux();

$consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);

if($consolidado->error != ""){
    echo $consolidado->error;
}
$detalle->eliminar($_SESSION["client_id"], $_SESSION['client_empleado']);

if($detalle->error != ""){
    echo $detalle->error;
}
$directorio = 'archivos_csvf3/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp';
$ficheros   = scandir($directorio);

$tablaMaestra->listadoEncontrarCampo(7);
$paquetes  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"Linux","Paquete");
$procesos  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"Linux","Procesos");
$archivos  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"Linux","Archivos");

foreach($ficheros as $row => $value){
    $memoriaEncon   = 0;
    $osEncon        = 0;
    $TIVsmCapiEncon = 0;
    $TIVsmCbaEncon  = 0;
    $mqmEncon       = 0;
    $equipo         = "";
    $os             = "";
    $versionOs      = "";
    $virtual        = "NO";
    $cpu            = 0;
    $version        = "";
    $core           = "";

    $fichero = 'archivos_csvf3/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/' . $value;
    if($fichero != 'archivos_csvf3/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/.' && $fichero != 'archivos_csvf3/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/..'){
        if(($f = fopen($fichero, "r")) !== FALSE) {
            $packet  = 0;
            $process = 0;
            $file    = 0;
            $bandVersion = false;
            while(!feof($f)) {
                $linea = fgets($f);
                if(strpos($linea, $tablaMaestra->listaCampos[0]["descripcion"]) !== FALSE){
                    $aux = $tablaMaestra->listaCampos[0]["descripcion"];
                    $equipo = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[7]["descripcion"]) !== FALSE){
                    $aux = $tablaMaestra->listaCampos[7]["descripcion"];
                    $os = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[4]["descripcion"]) !== FALSE){
                    $aux       = $tablaMaestra->listaCampos[4]["descripcion"];
                    $versionOs = trim(substr($linea, strlen($aux), strlen($linea)));
                }
                
                if(strpos($linea, $tablaMaestra->listaCampos[8]["descripcion"]) !== FALSE){
                    $virtual = "SI";
                }

                if(strpos($linea, $tablaMaestra->listaCampos[9]["descripcion"]) !== FALSE){
                    /*$aux = $tablaMaestra->listaCampos[9]["descripcion"];
                    $cpuAux = trim(substr($linea, strlen($aux) + 1, strlen($linea)));
                    if($cpu < $cpuAux){
                        $cpu = $cpuAux;
                    }*/
                    $cpu++;
                }

                if(strpos($linea, $tablaMaestra->listaCampos[10]["descripcion"]) !== FALSE && $bandVersion === false){
                    $aux     = $tablaMaestra->listaCampos[10]["descripcion"];
                    $version = str_replace(":", "", trim(substr($linea, strlen($aux), strlen($linea))));
                    $bandVersion = true;
                }

                if(strpos($linea, $tablaMaestra->listaCampos[11]["descripcion"]) !== FALSE){
                    $aux  = $tablaMaestra->listaCampos[11]["descripcion"];
                    $core += trim(substr($linea, strlen($aux) + 1, strlen($linea)));
                }

                $software = "";
                if(strpos($linea, "Packets Found:") !== FALSE){
                    $packet++;
                }

                if(strpos($linea, "Processes Found:") !== FALSE){
                    $packet = -1;
                    $process++;
                }

                if(strpos($linea, "Files Found:") !== FALSE){
                    $process = -1;
                    $file++;
                }

                if($packet > 0){
                    foreach($paquetes as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }

                if($process > 0){
                    foreach($procesos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }

                if($file > 0){
                    foreach($archivos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }
            }

            if($equipo != "" && $os != ""){                               
                $detalle->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $os, $versionOs, $virtual, $cpu, $version, $core);
            } 
        }
        fclose($f);
    }
}

if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 7) === false){
    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 7);
}

$archivosDespliegue->actualizarDespliegue3($_SESSION["client_id"], $_SESSION['client_empleado'], 7, $nombreArchivo);