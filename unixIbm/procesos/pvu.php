<?php   
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_solaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_AIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente_linux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$pvuSolaris = new pvuClienteSolaris();
$pvuAIX = new pvuClienteAIX();
$pvuLinux = new pvuClienteLinux();
$modificar = 0;
$tablaPVUSolaris = array();
$tablaPVUAIX = array();
$tablaPVULinux = array();
$general = new General();
$log = new log();

if(isset($_POST["modificar"]) && $_POST["modificar"] == 1){
    $modificar = 1;
    $idPvuCliente = array();
    $usaProc = array();
    $usaCore = array();
    
    $opc = "";
    if(isset($_POST["opc"])){
        $opc = $general->get_escape($_POST["opc"]);
    }
    
    if($opc == "Solaris"){
        if(isset($_POST["idPvuClienteSolaris"])){
            $idPvuCliente = $_POST["idPvuClienteSolaris"];
        }

        if(isset($_POST["usaProcSolaris"])){
            $usaProc = $_POST["usaProcSolaris"];
        }

        if(isset($_POST["usaCoreSolaris"])){
            $usaCore = $_POST["usaCoreSolaris"];
        }
    } else if($opc == "AIX"){
        if(isset($_POST["idPvuClienteAIX"])){
            $idPvuCliente = $_POST["idPvuClienteAIX"];
        }

        if(isset($_POST["usaProcAIX"])){
            $usaProc = $_POST["usaProcAIX"];
        }

        if(isset($_POST["usaCoreAIX"])){
            $usaCore = $_POST["usaCoreAIX"];
        }
    } else if($opc == "Linux"){
        if(isset($_POST["idPvuClienteLinux"])){
            $idPvuCliente = $_POST["idPvuClienteLinux"];
        }

        if(isset($_POST["usaProcLinux"])){
            $usaProc = $_POST["usaProcLinux"];
        }

        if(isset($_POST["usaCoreLinux"])){
            $usaCore = $_POST["usaCoreLinux"];
        }
    } 
    
    $j = 0;
    for($i = 0; $i < count($idPvuCliente); $i++){
        $id = 0;
        if(filter_var($idPvuCliente[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idPvuCliente[$i];
        }
        
        $proc = 0;
        if(filter_var($usaProc[$i], FILTER_VALIDATE_INT) !== false){
            $proc = $usaProc[$i];
        }
        
        $core = 0;
        if(filter_var($usaCore[$i], FILTER_VALIDATE_INT) !== false){
            $core = $usaCore[$i];
        }
        
        if($opc == "Solaris"){
            if($pvuSolaris->actualizar($id, $proc, $core)){
               $j++; 
            }
        } else if($opc == "AIX"){
            if($pvuAIX->actualizar($id, $proc, $core)){
               $j++; 
            }
        } else if($opc == "Linux"){
            if($pvuLinux->actualizar($id, $proc, $core)){
               $j++; 
            }
        }
    }
    
    if($j == 0){
        $exito = 0;
    }
    else if($j > 0 && $j == $i){
        $exito = 1;
    }
    else{
        $exito = 2;
    }
    
    $log->insertar(22, 2, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Conteo PVU " . $opc);
} else{
    $tablaPVUSolaris = $pvuSolaris->listadoPVUCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $tablaPVUAIX = $pvuAIX->listadoPVUCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $tablaPVULinux = $pvuLinux->listadoPVUCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
}