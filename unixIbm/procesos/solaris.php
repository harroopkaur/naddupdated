<?php
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_UnixSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_UnixSolaris.php");

$resumen      = new resumenUnixSolaris();
$consolidado  = new consolidadoUnixSolaris();
$detalle      = new DetallesUnixSolaris();

$consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
$detalle->eliminar($_SESSION["client_id"], $_SESSION['client_empleado']);
$directorio = 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp';
$ficheros   = scandir($directorio);

$tablaMaestra->listadoEncontrarCampo(7);
$paquetes  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"Solaris","Paquete");
$procesos  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"Solaris","Procesos");
$archivos  = $tablaMaestra->sustituirCamposDespliegue2(3,7,"Solaris","Archivos");

foreach($ficheros as $row => $value){
    $memoriaEncon   = 0;
    $osEncon        = 0;
    $TIVsmCapiEncon = 0;
    $TIVsmCbaEncon  = 0;
    $mqmEncon       = 0;
    $equipo         = "";
    $os             = "";
    $memoria        = "";
    $cpu            = 0;
    $totalCores     = 0;

    $fichero = 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/' . $value;
    if($fichero != 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/.' && $fichero != 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] .  '/tmp/..'){
        if(($f = fopen($fichero, "r")) !== FALSE) {
            $packet  = 0;
            $process = 0;
            $file    = 0;
            $bandTipoCPU = false;
            $z = 0;
            $tipoCPU = null;
            
            while(!feof($f)) {
                $linea = fgets($f);
                if(strpos($linea, $tablaMaestra->listaCampos[0]["descripcion"]) !== FALSE){
                    $aux = $tablaMaestra->listaCampos[0]["descripcion"];
                    $equipo = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[2]["descripcion"]) !== FALSE /*&& $osEncon == 1*/){
                    $aux = $tablaMaestra->listaCampos[2]["descripcion"];
                    $os = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[1]["descripcion"]) !== FALSE /*&& $memoriaEncon == 0*/){
                    $memoAux = explode(" ", $linea);
                    $memoria = $memoAux[2];
                    //$memoriaEncon++;
                }
                
                if($z == 1 && $bandTipoCPU === false){
                    $tipoCPU = $linea;
                    $bandTipoCPU = true;
                }

                if(strpos($linea, $tablaMaestra->listaCampos[3]["descripcion"]) !== FALSE){
                    $linea = trim(substr($linea, strpos($linea,"(") + 1, strlen($linea)));
                    $linea = trim(substr($linea, 0, strlen($linea) - 1));
                    $newArray = explode("-", $linea);
                    $cores = $newArray[1] - $newArray[0] + 1;
                    $totalCores += $cores;
                    $cpu++;
                    $z++;
                }

                $software = "";
                if(strpos($linea, "Packets Found:") !== FALSE){
                    $packet++;
                }

                if(strpos($linea, "Processes Found:") !== FALSE){
                    $packet = -1;
                    $process++;
                }

                if(strpos($linea, "Files Found:") !== FALSE){
                    $process = -1;
                    $file++;
                }
                
                if($packet > 0){
                    foreach($paquetes as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }

                if($process > 0){
                    foreach($procesos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }

                if($file > 0){
                    foreach($archivos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            } 
                        }
                    }
                }
            }

            if($equipo != "" && $os != "" && $memoria != ""){
                $detalle->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $os, $memoria, $cpu, $tipoCPU, $totalCores);
            } 
        }
        fclose($f);
    }
}

if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 7) === false){
    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 7);
}

$archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION['client_empleado'], 7, $nombreArchivo, 0);