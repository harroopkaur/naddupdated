<div style="width:98%; padding:20px; overflow:hidden;">    
    <h1 class="textog negro" style="text-align:center; line-height:35px;">Historial de Certificados</h1>
    
    <br>

    <table style="width:95%;" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
        <thead>
            <tr>
                <th class="text-center" style="width:60%">Descripción</th> 
                <th class="text-center" style="width:20%">Fecha</th> 
                <th class="text-center" style="width:50px;">Archivo</th> 
            </tr>
        </thead>
        <tbody id="contTabla">
            <?php 
            $i = 0;
            foreach($listado as $row){
            ?>
                <tr>
                    <td><?= $row["descripcion"] ?></td>
                    <td><?= $general->muestraFechaHora($row["fecha"]) ?></td>
                    <td class="text-center"><a href='#' onclick='descargar("<?= $row['ruta'] ?>")'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_181_download_alt.png' border='0' alt='Descargar' title='Descargar' /></a></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>

    <div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>
</div>

<script>
    function descargar(archivo){
        window.open("<?= $GLOBALS["domain_root"] ?>/adminweb/clientes/certificados/<?= $_SESSION["client_id"] ?>/" + archivo);
    }
</script>