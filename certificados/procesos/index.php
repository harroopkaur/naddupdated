<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_certificaciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");


// Objetos
$general = new General();
$validator = new validator("form1");
$certificaciones =  new certificaciones();

if(isset($_GET["pg"]) && filter_var($_GET["pg"], FILTER_VALIDATE_INT) !== false){
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = ''; //filtro adicional si se le coloca input para filtrar por columnas
}
else{
    $start_record = 0;
    $parametros   = '';
}

$listado = $certificaciones->certificadosCliente($_SESSION["client_id"], $start_record);
$count   = $certificaciones->total($_SESSION["client_id"]);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);