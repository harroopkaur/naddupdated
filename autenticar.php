<?php
require_once("configuracion/inicio.php");
require_once("clases/clase_general.php");
require_once("clases/clase_clientes.php");
require_once("clases/middleware.php");
require_once("clases/clase_empleados.php");

// Objetos
$usuario = new Clientes();
$empleado = new empleados();
// Inicializar error
$error = 0;

if (isset($_POST['entrar']) && $_POST['entrar'] == 1) {
    if ($usuario->autenticarEmpleado($_POST['login'], $_POST['contrasena'])) {
        $nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
        $empleado->actUltAcceso($_SESSION["client_empleado"]);
        ?>
        <script>
            localStorage.licensingassuranceToken =  '<?= $nuevo_middleware->obtener_token(true) ?>';
            //location.href = "microsoft/";
            location.href = "inicio.php";
        </script>
        <?php
    } else {
        header('location: index.php?error=1');
    }
}
?>