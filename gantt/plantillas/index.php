<div style="width:98%; padding:10px; overflow:hidden;">
    <h1 class="textog negro" style="margin:20px; text-align:center;">GANTT</h1>
    
    
    <style>
        .contenedorGanttResumen{
            width: calc(98% - 40px);
            background: #7F7F7F;
            margin: 0 auto;
            padding: 20px;
            overflow: hidden;
            border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
        }
        
        .cajaGantt{
            width: 150px;
            height: 70px;
            line-height: 70px;
            text-align: center;
            color: #000000;
            overflow: hidden;
            font-weight: bold;
        }
        
        .cajaFabricantes{
            width: 120px;
            height: 70px;
            line-height: 70px;
            text-align: center;
            color: #000000;
            overflow: hidden;
            font-weight: bold;
        }

        .contenedorResumen{
            width:760px;
            overflow:hidden;
            margin:0 auto;
            /*border: 1px solid;*/
        }
        
        .contenedorFabricantes{
            width:auto;
            overflow:hidden;
            margin:0 auto;
        }
        
        .contenedorGanttFechas{
            width: 98%;
            margin: 0 auto;
            overflow: hidden;
            border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
        }
        
        .cajaGanttHorizontal{
            width: 19.8%;
            line-height: 50px;
            text-align: center;
            color: #FFF;
            overflow: hidden;
            border: 1px solid #FFFFFF;
            font-weight:bold;
        }
        
        .cajaGanttVertical{
            width: 100%;
            height: 50px;
            line-height: 50px;
            text-align: center;
            color: #FFF;
            overflow: hidden;
            border: 1px solid #FFFFFF;
            font-weight:bold;
        }
        
        .cajaGanttFechas{
            width: 19.8%;
            height: 50px;
            line-height: 50px;
            text-align: center;
            color: #FFF;
            overflow: hidden;
            border: 1px solid #FFFFFF;
            font-weight:bold;
        }
     
        .contenedorFechas{
            width:99%;
            overflow:hidden;
            margin:0 auto;
        }
        
        .textColor0{
            color: #000000;
        }
        
        .textColor1{
            color: #FFFFFF;
        }
        
        .color1{
            background-color: #243C67;
        }
        
        .color2{
            background-color: #99BFDC;
        }
        
        .color3{
            background-color: #81816A;
        }
        
        .color4{
            background-color: #00AFF0;
        }
        
        .circuloResumen {
            width: 40px;
            height: 40px;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            top: 50%;
            margin-top: -20px;
            left: 50%;
            margin-left: -20px;
            position:relative;
            line-height: 40px;
        }
        
        .contCirculoResumen{
            float:left;
            width:49%;
            height: 100%;
            margin: 0 auto;
            overflow:hidden;
        }
        
        .contVerticalResumen{
            font-size: 15px;
            line-height: 30px;
            overflow: hidden;
            width: 25px;
            height: 255px;
            text-align: center;
            writing-mode: vertical-lr;
            transform: rotate(180deg);
            background-color: #95DEF8;
            display: absolute;
            border: 1px solid #000000;
            margin-top:-260px;
        }
    </style>  
        
    <div class="contenedorGanttResumen">
        <div class="contenedorResumen">
            <p class="titulo1">Resumen</p>
            
            <br>
            
            <div class="contenedorFlex">
                <div class="cajaGantt backgroundColor1 pointer" id="contratos" title="Resumen de Contratos">Contratos</div>
                <div class="cajaGantt backgroundColor2 pointer" id="licencias" title="Licencias de Software Adquiridas">Licencias</div>            
                <div class="cajaGantt backgroundColor3 pointer" id="instalaciones" title="Resumen de Instalaciones Descubiertas">Instalaciones</div>
                <div class="cajaGantt backgroundColor4 pointer" id="equipos" title="Listado de Equipos Analizados">Equipos</div>
                <div class="cajaGantt backgroundColor5 pointer" id="informeEjecutivo" title="Informe Ejecutivo Exportable">Informe Ejecutivo</div>
            </div>
        </div>
    </div>
    
    <br>
    
    <div class="contenedorGanttFechas">
        <div class="contenedorFechas">
            <table class="tablap">
                <thead>
                    <tr>
                        <th colspan="10" class="text-center">Leyenda</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="text-center">L</th>
                        <td class="text-center">Levantamiento</td>

                        <th class="text-center">TS</th>
                        <td class="text-center">Troubleshooting</td>

                        <th class="text-center">RS</th>
                        <td class="text-center">Reporte Servidores</td>

                        <th class="text-center">RP</th>
                        <td class="text-center">Reporte PCs</td>

                        <th class="text-center">Opt</th>
                        <td class="text-center">Optimización</td>
                    </tr>

                    <tr>
                        <th class="text-center">Mit</th>
                        <td class="text-center">Mitigación</td>

                        <th class="text-center">Reno</th>
                        <td class="text-center">Renovación</td>

                        <th class="text-center">TU</th>
                        <td class="text-center">True UP</td>

                        <th class="text-center">Rev</th>
                        <td class="text-center">Revisión</td>

                        <th class="text-center">P</th>
                        <td class="text-center">Presupuesto</td>                    
                    </tr>
                </tbody>
            </table>
            
            <br>
            
            <div class="contenedorFlex">
                <div class="cajaGanttFechas backgroundColor5">Fases</div>
                <div class="cajaGanttFechas backgroundColor5">Trimestre 1</div>            
                <div class="cajaGanttFechas backgroundColor5">Trimestre 2</div>
                <div class="cajaGanttFechas backgroundColor5">Trimestre 3</div>
                <div class="cajaGanttFechas backgroundColor5">Trimestre 4</div>
                
                <div class="cajaGanttHorizontal">
                    <div class="cajaGanttVertical backgroundColor2 color0">Levantamiento</div>
                    <div class="cajaGanttVertical backgroundColor2 color0">Reportes</div>
                    <div class="cajaGanttVertical backgroundColor2 color0">Optimización & Mitigación</div>
                    <div class="cajaGanttVertical backgroundColor2 color0">Adquisiciones y Auditorías</div>
                    <div class="cajaGanttVertical backgroundColor2 color0">Procesos y Políticas SAM</div>
                </div>
                
                <div class="cajaGanttHorizontal">
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["L1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">L</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TS1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">TS</div>
                            </div>
                        <?php
                        }                        
                        ?>
                    </div>   
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["RS1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">RS</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["RP1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">RP</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>  
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Opt1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">Opt</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["Mit1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">Mit</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>      
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Reno1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">Reno</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TU1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">TU</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>   
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Rev1"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color1">Rev</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    
                    <?php 
                    if($rowResumen["P1"] != ""){
                    ?>
                        <div class="contVerticalResumen">Presupuesto</div>
                    <?php
                    }
                    ?>
                </div>
                
                <div class="cajaGanttHorizontal">
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["L2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">L</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TS2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">TS</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["RS2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">RS</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["RP2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">RP</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Opt2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">Opt</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["Mit2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">Mit</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Reno2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">Reno</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TU2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">TU</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php
                        if($rowResumen["Rev2"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color2">Rev</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    
                    <?php 
                    if($rowResumen["P2"] != ""){
                    ?>
                        <div class="contVerticalResumen">Presupuesto</div>
                    <?php
                    }
                    ?>
                </div>
                
                <div class="cajaGanttHorizontal">
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["L3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">L</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TS3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">TS</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["RS3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">RS</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["RP3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">RP</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Opt3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">Opt</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["Mit3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">Mit</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Reno3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">Reno</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TU3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">TU</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php
                        if($rowResumen["Rev3"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color3">Rev</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    
                    <?php 
                    if($rowResumen["P3"] != ""){
                    ?>
                        <div class="contVerticalResumen">Presupuesto</div>
                    <?php
                    }
                    ?>
                </div>
                
                <div class="cajaGanttHorizontal">
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["L4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">L</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TS4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">TS</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["RS4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">RS</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["RP4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">RP</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Opt4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">Opt</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["Mit4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">Mit</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php 
                        if($rowResumen["Reno4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">Reno</div>
                            </div>
                        <?php
                        }

                        if($rowResumen["TU4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">TU</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="cajaGanttVertical backgroundColor1">
                        <?php
                        if($rowResumen["Rev4"] != ""){
                        ?>
                            <div class="contCirculoResumen">
                                <div class="circuloResumen color4">Rev</div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    
                    <?php 
                    if($rowResumen["P4"] != ""){
                    ?>
                        <div class="contVerticalResumen">Presupuesto</div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <br>
    
    <div class="contenedorGanttResumen">
        <div class="contenedorFabricantes">
            <p class="titulo1">Entregables</p>
            
            <br>
            
            
            <div id="entreg" style="margin:0 auto;">
                <div class="contenedorFlex">
                    <?php 
                    $z = 1;
                    foreach($listadoFabricActivos as $row){
                    ?>
                    <div class="cajaFabricantes backgroundColor<?= $z ?> pointer" onclick="reportes(<?= $row["idFabricante"] ?>)" title="Reportes Entregables de <?= $row["nombre"] ?>"><?= $row["nombre"] ?></div>
                    <?php
                        $z++;
                    }
                    ?>
                </div>
            </div>
            
            <script>
                $("#entreg").width(<?= (($z - 1) * 123) ?>); 
            </script>
        </div>
    </div>
</div>

<div class="modal-personal" id="modal-contratos">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Contratos</p></h1>
    </div>
    <div class="modal-personal-body">
        <table id="listContrato" style="margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th  align="center" valign="middle" ><strong class="til">Fabricante</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Nro Contrato</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Fecha Efectiva</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Fecha Expiración</strong></th>
                    <th  align="center" valign="middle" class="til" >Monto US$</th>
                </tr> 
            </thead>
            <tbody>
                <?php 
                $i = 0;
                foreach($listadoContratos as $row){
                ?>
                    <tr  bgcolor="#333333" style="color:#FFF;">
                        <td  align="center" valign="middle" ><?= $row["nombre"] ?></td>
                        <td  align="center" valign="middle" ><?= $row["numero"] ?></td>
                        <td  align="center" valign="middle" ><?= $general->muestrafecha($row["fechaEfectiva"]) ?></td>
                        <td  align="center" valign="middle" <?php if(($count - 1) == $i){ echo "class='bold'"; } ?>>
                            <?php
                            if(($count - 1) == $i){
                                echo $row["fechaExpiracion"];
                            } else{
                                echo $general->muestrafecha($row["fechaExpiracion"]);
                            }
                            ?>
                        </td>
                        <td  align="right" valign="middle" <?php if(($count - 1) == $i){ echo "class='bold'"; } ?>><?= $row["total"] ?> $</td>
                    </tr> 
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>                    
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirContratos">Salir</div>
    </div>
</div>

<div class="modal-personal" id="modal-licencias">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Licencias de Software</p></h1>
    </div>
    <div class="modal-personal-body">
        <table id="listLicencias" style="margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th  align="center" valign="middle" ><strong class="til">Fabricante</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Nro Contrato</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Producto</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Edición</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Versión</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Asignación</strong></th>
                    <th  align="center" valign="middle" ><strong class="til">Cantidad</strong></th>
                    <th  style="width:80px;" align="center" valign="middle" class="til" >Costo US$</th>
                </tr> 
            </thead>
            <tbody>
                <?php 
                $i = 0;
                foreach($listadoLicencias as $row){
                ?>
                    <tr  bgcolor="#333333" style="color:#FFF;">
                        <td  align="center" valign="middle" ><?= $row["nombre"] ?></td>
                        <td  align="center" valign="middle" ><?= $row["numero"] ?></td>
                        <td  align="center" valign="middle" ><?= $row["producto"] ?></td>
                        <td  align="center" valign="middle" ><?= $row["edicion"] ?></td>
                        <td  align="center" valign="middle" ><?= $row["version"] ?></td>
                        <td  align="center" valign="middle" ><?= $row["asignacion"] ?></td>
                        <td  align="center" valign="middle" <?php if(($countLicencias - 1) == $i){ echo "class='bold'"; } ?>><?php if(filter_var($row["cantidad"], FILTER_VALIDATE_INT) !== false){ echo round($row["cantidad"], 0); } else{ $row["cantidad"]; } ?></td>
                        <td  align="right" valign="middle" <?php if(($countLicencias - 1) == $i){ echo "class='bold'"; } ?>><?= round($row["total"]) ?> $</td>
                    </tr> 
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>                    
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirLicencias">Salir</div>
    </div>
</div>

<div class="modal-personal" id="modal-instalaciones">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Instalaciones Descubiertas</p></h1>
    </div>
    <div class="modal-personal-body">
        <div style="width:220px; margin:0 auto;">
            <span class="bold">Fabricante:</span>
            <select id="fabricanteInstalados" name="fabricante">
                <option value="">--Seleccione--</option>
                <option value="1">Adobe</option>
                <option value="3">Microsoft</option>
                <option value="10">SPLA</option>
                <option value="2">Windows-IBM</option>
                <option value="4">Windows-Oracle</option>
            </select>
        </div>
        <div id="contenedorInstalados">
        </div>
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirInstalaciones">Salir</div>
    </div>
</div>

<div class="modal-personal" id="modal-equipos">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Equipos Analizados</p></h1>
    </div>
    <div class="modal-personal-body">
        <div style="width:220px; margin:0 auto;">
            <span class="bold">Fabricante:</span>
            <select id="fabricanteAnalizados" name="fabricante">
                <option value="">--Seleccione--</option>
                <option value="1">Adobe</option>
                <option value="3">Microsoft</option>
                <option value="10">SPLA</option>
                <option value="2">Windows-IBM</option>
                <option value="4">Windows-Oracle</option>
            </select>
        </div>
        <div id="contenedorAnalizados" style="width:100%; overflow-x:auto; overflow-y:hidden;  max-height:350px;">
        </div>
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirEquipos">Salir</div>
    </div>
</div>

<div class="modal-personal" id="modal-entregable">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline" id="nombFabricante"></p></h1>
    </div>
    <div class="modal-personal-body">
        <table id="listLicencias" style="margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th align="center" valign="middle" ><strong class="til">Fecha</strong></th>
                    <th style="width:80px;">&nbsp;</th>
                </tr> 
            </thead>
            <tbody id="bodyEntregable">
            </tbody>
        </table>                    
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirEntregable">Salir</div>
    </div>
</div>

<div class="modal-personal modal-personal-lg" id="modal-informeEjecutivo">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Informe <p style="color:#06B6FF; display:inline">Ejecutivo</p></h1>
    </div>
    <div class="modal-personal-body">
        <div style="width:220px; margin:0 auto;">
            <span class="bold">Fabricante:</span>
            <select id="fabricanteInformeEjecutivo" name="fabricante">
                <option value="">--Seleccione--</option>
                <option value="1">Adobe</option>
                <option value="3">Microsoft</option>
                <option value="5">SAP</option>
                <option value="10">SPLA</option>
                <option value="7">UNIX-IBM</option>
                <option value="8">UNIX-Oracle</option>
                <option value="6">VMWare</option>
                <option value="2">Windows-IBM</option>
                <option value="4">Windows-Oracle</option>
            </select>
        </div>
        <br>
        <table class="tablap">
            <thead>
                <tr>
                    <th class="text-center">Familia</th>
                    <th class="text-center">Total Compras</th>
                    <th class="text-center">Total Instalado Propio</th>
                    <th class="text-center">Neto</th>
                    <th class="text-center">Precio Unitario</th>
                    <th class="text-center">Total</th>
                </tr>
            </thead>
            <tbody id="contenedorInformeEjecutivo"></tbody>            
        </table>
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2Alterno" style="float:right;" id="salirInformeEjecutivo">Salir</div>
        <div class="boton1 botones_m2Alterno" style="float:right;" id="exportarExcel" onclick="window.open('reportes/excelInformeEjecutivo.php?fabricante=' + $('#fabricanteInformeEjecutivo').val());">Exportar Excel</div>
        <div class="boton1 botones_m2Alterno" style="float:right;" id="exportarPDF" onclick="window.open('reportes/PDFInformeEjecutivo.php?fabricante=' + $('#fabricanteInformeEjecutivo').val());">Exportar PDF</div>
    </div>
</div>

<script>
    $(document).ready(function(){        
        $("#listContrato").tablesorter();
        $("#listLicencias").tablesorter();
        
        $("#contratos").click(function(){
            $("#fondo1").show();
            $("#modal-contratos").show();
        });
        
        $("#salirContratos").click(function(){
            $("#fondo1").hide();
            $("#modal-contratos").hide();
        });
        
        $("#licencias").click(function(){
            $("#fondo1").show();
            $("#modal-licencias").show();
        });
        
        $("#salirLicencias").click(function(){
            $("#fondo1").hide();
            $("#modal-licencias").hide();
        });
        
        $("#instalaciones").click(function(){
            $("#fondo1").show();
            $("#ttabla1").show();
            $("#modal-instalaciones").show();
        });
        
        $("#salirInstalaciones").click(function(){
            $("#fondo1").hide();
            $("#modal-instalaciones").hide();
            $("#fabricanteInstalados").val("");
            $("#contenedorInstalados").empty();
        });
        
        $("#equipos").click(function(){
            $("#fondo1").show();
            $("#modal-equipos").show();
            
        });
        
        $("#salirEquipos").click(function(){
            $("#fondo1").hide();
            $("#modal-equipos").hide();
            $("#contenedorAnalizados").empty();
            $("#fabricanteAnalizados").val("");
        });
        
        $("#salirEntregable").click(function(){
            $("#fondo1").hide();
            $("#modal-entregable").hide();
            $("#nombFabricante").empty();
            $("#bodyEntregable").empty();
        });
        
        $("#fabricanteInstalados").change(function(){
            if($("#fabricanteInstalados").val() === ""){
                $.alert.open('warning', 'Alert', 'Debe seleccionar un fabricante', {'Aceptar': 'Aceptar'});
                return false;
            }
            
            $("#modal-instalaciones").hide();
            $("#fondo1").hide();
            $("#fondo").show();
            $.post("ajax/softwareInstalados.php", { fabricante : $("#fabricanteInstalados").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                $("#contenedorInstalados").empty();
                $("#contenedorInstalados").append(data[0].tabla);
                $("#fondo").hide();
                $("#fondo1").show();
                $("#modal-instalaciones").show();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    $("#contenedorInstalados").empty();
                    $("#fondo1").show();
                    $("#modal-instalaciones").show();
                });
            });
        });
        
        $("#fabricanteAnalizados").change(function(){
            if($("#fabricanteAnalizados").val() === ""){
                $.alert.open('warning', 'Alert', 'Debe seleccionar un fabricante', {'Aceptar': 'Aceptar'});
                return false;
            }
            
            $("#modal-equipos").hide();
            $("#fondo1").hide();
            $("#fondo").show();
            $.post("ajax/equiposAnalizados.php", { fabricante : $("#fabricanteAnalizados").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                $("#contenedorAnalizados").empty();
                $("#contenedorAnalizados").append(data[0].tabla);
                $("#fondo").hide();
                $("#fondo1").show();
                $("#modal-equipos").show();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    $("#contenedorAnalizados").empty();
                    $("#fondo1").show();
                    $("#modal-equipos").show();
                });
            });
        });
        
        //inicio modal informe ejecutivo
        $("#informeEjecutivo").click(function(){
            $("#fabricanteInformeEjecutivo").val("");
            $("#contenedorInformeEjecutivo").empty();
            $("#fondo1").show();
            $("#modal-informeEjecutivo").show();
        });
        
        $("#salirInformeEjecutivo").click(function(){
            $("#fondo1").hide();
            $("#modal-informeEjecutivo").hide();
        });
        
        $("#fabricanteInformeEjecutivo").change(function(){
            $("#salirInformeEjecutivo").click();
            $("#fondo").show();
            $.post("ajax/informeEjecutivo.php", { fabricante : $("#fabricanteInformeEjecutivo").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                $("#fondo").hide();
                $("#contenedorInformeEjecutivo").empty();
                $("#contenedorInformeEjecutivo").append(data[0].tabla);
                $("#fondo1").show();
                $("#modal-informeEjecutivo").show();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        //fin modal informe ejecutivo
    });
    
    function reportes(id){
        $.post("ajax/fechasSam.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
      
            $("#nombFabricante").append(data[0].nombre);
            $("#bodyEntregable").append(data[0].tabla);
            
            $("#fondo1").show();
            $("#modal-entregable").show();
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function descargar(id, archivo){
        if(id === 1){
            window.open("<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelAlcance.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelOptimizacion.php?vert1=" + archivo);
        } else if(id === 2){
            window.open("<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelAlcance.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelOptimizacion.php?vert1=" + archivo);
        } else if(id === 3){
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelAlcance.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelEquiposNoDescubiertos.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelEquiposDesuso.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelOptimizacion.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelServidor.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelPruebaDesarrollo.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelDetalleEquipo.php?vert1=" + archivo);
        } else if(id === 4){
            window.open("<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelAlcance.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelOptimizacion.php?vert1=" + archivo);
        } else if(id === 5){
            window.open("<?= $GLOBALS['domain_root'] ?>/sap/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/sap/reportes/repoUsabilidad.php?vert=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/sap/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
        } else if(id === 6){
            window.open("<?= $GLOBALS['domain_root'] ?>/VMWare/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/VMWare/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/VMWare/reportes/excelListadoContrato.php?vert1=" + archivo);
        } else if(id === 7){
            window.open("<?= $GLOBALS['domain_root'] ?>/unixIbm/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/unixIbm/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
        } else if(id === 8){
            window.open("<?= $GLOBALS['domain_root'] ?>/unixOracle/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/unixOracle/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
        } else if(id === 10){
            window.open("<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelDetalle.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelAlcance.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelBalanza.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelOptimizacion.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelServidor.php?vert1=" + archivo);
        }
    }
</script>