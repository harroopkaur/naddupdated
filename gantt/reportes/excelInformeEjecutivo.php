<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_adobe.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("");
    
    $fabricante = 0;
    if(isset($_GET["fabricante"]) && filter_var($_GET["fabricante"], FILTER_VALIDATE_INT) !== false){
        $fabricante = $_GET["fabricante"];
    }  

    $tabla = "";
    if($fabricante == 1){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_adobe.php");
        $balance = new balanceAdobe();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "Adobe";
    } else if($fabricante == 2){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
        $balance = new balanceIbm();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);   
        $nomFab = "Windows-IBM";
    } else if($fabricante == 3){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
        $balanceMicrosoft = new Balance_f();
        $listado = $balanceMicrosoft->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "Microsoft";
    } else if($fabricante == 4){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_oracle.php");
        $balance = new balanceOracle();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "Windows-Oracle";
    } else if($fabricante == 5){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_Sap.php");
        $balance = new balanceSap();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "SAP";
    } else if($fabricante == 6){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
        $balance = new balanceVMWare();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "VMWare";
    } else if($fabricante == 7){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixIBM.php");
        $balance = new balanceUnixIBM();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "UNIX-IBM";
    } else if($fabricante == 8){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
        $balance = new balanceUnixOracle();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "UNIX-Oracle";
    } else if($fabricante == 10){
        require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
        $balance = new Balance_SPLA();
        $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
        $nomFab = "SPLA";
    }

    $total = 0;
    $sobrante = 0;
    $faltante = 0;
    // Add some data

    //inicio cabecera de la tabla
    $i = 2;
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Familia')
                ->setCellValue('B1', 'Total Compras')
                ->setCellValue('C1', 'Total Instalado Propio')
                ->setCellValue('D1', 'Neto')
                ->setCellValue('E1', 'Precio Unitario')
                ->setCellValue('F1', 'Total');
    //fin cabecera de la tabla
    
    foreach($listado as $row){
        $total += $row["totalCompras"] * $row["precio"]; 

        if($row["total"] < 0){
            $faltante +=  $row["total"];
        } else{
            $sobrante += $row["total"];
        }
        
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $row["familia"])
                ->setCellValue('B' . $i, round($row["totalCompras"], 0))
                ->setCellValue('C' . $i, round($row["totalInstalaciones"], 0))
                ->setCellValue('D' . $i, round($row["neto"], 0))
                ->setCellValue('E' . $i, round($row["precio"], 0))
                ->setCellValue('F' . $i, round($row["total"], 0));
        $i++;
    }

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, 'Inversión en SW')
                ->setCellValue('B' . $i, round($total, 0))
                ->setCellValue('C' . $i, '')
                ->setCellValue('D' . $i, '')
                ->setCellValue('E' . $i, 'Sobrante')
                ->setCellValue('F' . $i, round($sobrante, 0));
    
    $i++;
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, '')
                ->setCellValue('B' . $i, '')
                ->setCellValue('C' . $i, '')
                ->setCellValue('D' . $i, '')
                ->setCellValue('E' . $i, 'Faltante')
                ->setCellValue('F' . $i, round($faltante, 0));

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe Ejecutivo de ' . $nomFab . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesion($mensaje);
}
?>