<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$fabricante = 0;
if(isset($_GET["fabricante"]) && filter_var($_GET["fabricante"], FILTER_VALIDATE_INT) !== false){
    $fabricante = $_GET["fabricante"];
}  

$tabla = "";
if($fabricante == 1){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_adobe.php");
    $balance = new balanceAdobe();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "Adobe";
} else if($fabricante == 2){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
    $balance = new balanceIbm();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);   
    $nomFab = "Windows-IBM";
} else if($fabricante == 3){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
    $balanceMicrosoft = new Balance_f();
    $listado = $balanceMicrosoft->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "Microsoft";
} else if($fabricante == 4){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_oracle.php");
    $balance = new balanceOracle();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "Windows-Oracle";
} else if($fabricante == 5){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_Sap.php");
    $balance = new balanceSap();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "SAP";
} else if($fabricante == 6){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
    $balance = new balanceVMWare();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "VMWare";
} else if($fabricante == 7){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixIBM.php");
    $balance = new balanceUnixIBM();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "UNIX-IBM";
} else if($fabricante == 8){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
    $balance = new balanceUnixOracle();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "UNIX-Oracle";
} else if($fabricante == 10){
    require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
    $balance = new Balance_SPLA();
    $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $nomFab = "SPLA";
}

$total = 0;
$sobrante = 0;
$faltante = 0;

ob_start();
$html = ob_get_clean();
$html = utf8_encode($html);
$html .= '<style>
        .tituloSAMDiagnostic1{
            font-size:35px;
            text-align: center;
            font-weight: bold;
        }
        
        .tablap {     
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
            font-size: 12px; 
            margin: 0 auto;   
            width: 99%; 
            text-align: left;    
            border-collapse: collapse; 
        }

        .tablap th {     
            font-size: 13px;     
            font-weight: normal;     
            padding: 8px;     
            background: #aabcfe;
            border-top: 4px solid #aabcfe;    
            border-bottom: 1px solid #fff; 
            color: #039; 
            vertical-align: middle;
        }

        .tablap td {    
            padding: 8px;     
            background: #e8edff;     
            border-bottom: 1px solid #fff;
            color: #669;    
            border-top: 1px solid transparent; 
            vertical-align: middle;
        }

        .tablap tr:hover td { 
            background: #d0dafd; 
            color: #339; 
        }
    </style>
    
    <h1 class="tituloSAMDiagnostic1 colorBlanco">Informe Ejecutivo de ' . $nomFab . ' </h1>

    <table class="tablap">
        <thead>
            <tr>
                <th>Familia</th>
                <th>Total Compras</th>
                <th>Total Instalado Propio</th>
                <th>Neto</th>
                <th>Precio Unitario</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody id="contenedorInformeEjecutivo">';
            foreach($listado as $row){
                $total += $row["totalCompras"] * $row["precio"]; 

                if($row["total"] < 0){
                    $faltante +=  $row["total"];
                } else{
                    $sobrante += $row["total"];
                }
                $html .= '<tr>
                        <td class="text-center">' . $row["familia"] . '</td>
                        <td class="text-right">' . round($row["totalCompras"], 0) . '</td>
                        <td class="text-right">' . round($row["totalInstalaciones"], 0) . '</td>
                        <td class="text-right">' . round($row["neto"], 0) . '</td>
                        <td class="text-right">' . round($row["precio"], 0) . '</td>
                        <td class="text-right">' . round($row["total"], 0) . '</td>
                    </tr>';
            }

            $html .= '<tr>
                        <td class="text-left bold">Inversión en SW</td>
                        <td class="text-right">' . round($total, 0) . '</td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-left bold">Sobrante</td>
                        <td class="text-right bold">' . round($sobrante, 0) . '</td>
                    </tr>
                    <tr>
                        <td class="text-left bold"></td>
                        <td class="text-right"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-left bold">Faltante</td>
                        <td class="text-right bold">' . round($faltante, 0) . '</td>
                    </tr>';
        $html .= '</tbody>            
    </table>
    
    <br>

    <table style="width:100%;">
        <tr>
            <td style="width:13%;">' . date("m/d/Y") . '</td>
            <td style="width:73%;text-align:center;">https://www.licensingassurance.com</td>
            <td style="width:13%;"><img style="height:60px;" align="right" src="' . $GLOBALS["domain_root1"] . '/img/LA_Logo.png"></img></td>
        </tr>
    </table>';
                
include($GLOBALS["app_root1"] . "/mpdf60/mpdf.php");
$mpdf = new mPDF();
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in = "UTF-8";
$mpdf->WriteHTML($html);
$mpdf->output();
exit();