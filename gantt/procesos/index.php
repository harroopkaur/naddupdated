<?php
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_fabricantes_activos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_ganttDetalle.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware


$funciones = new funcionesSam();
$general = new General();
$detalles1 = new DetallesE_f();
$fabricantesActivos =  new fabricantesActivos();
$ganttDetalle =  new ganttDetalle();

$listadoContratos = $funciones->listadoContratosClienteGantt($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listadoLicencias = $funciones->listadoLicenciasClienteGantt($_SESSION["client_id"], $_SESSION["client_empleado"]);

$count = count($listadoContratos);
$countLicencias = count($listadoLicencias);

//inicio resumen
$rowResumen = $ganttDetalle->datosGanttDetalleUlt($_SESSION["client_id"]);
//

//inicio entregables
$listadoFabricActivos = $fabricantesActivos->listar_fabricantes($_SESSION["client_id"]);
//fin entregables