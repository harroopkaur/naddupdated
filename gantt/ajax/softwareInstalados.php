<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }  
            
            $tabla = "";
            if($fabricante == 1){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
                $detalles1 = new DetallesAdobe();
                //inicio software en uso
                $totalEquiposOS = 0;

                $usoWindows = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows');
                //$usoWindowsServer = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows Server');
                $instaladosAdobe = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Adobe Acrobat");
                $instaladosCloud = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Adobe Creative Cloud");
                $instaladosCreative = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Adobe Creative Suite");
                
                $familia = array("Adobe Acrobat", "Adobe Creative Cloud", "Adobe Creative Suite");
                $usoOtros = $detalles1->cantidadProductosOtrosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], $familia);

                $cantUsoOS = $usoWindows["cantidad"];

                $cantUsoAdobe = $instaladosAdobe["cantidad"];
                $cantAux = $cantUsoAdobe;

                $cantUsoCloud = $instaladosCloud["cantidad"];      
                if($cantUsoCloud > $cantAux){
                    $cantAux = $cantUsoCloud;
                }

                $cantUsoCreative = $instaladosCreative["cantidad"];
                if($cantUsoCreative > $cantAux){
                    $cantAux = $cantUsoCreative;
                }

                if($cantUsoOS > 0){
                    $porcentajeUsoCliente = round($cantAux * 100 / $cantUsoOS, 0);
                } else{
                    $porcentajeUsoCliente = 0;
                }
                
                $cantUsoOtros =  $usoOtros["cantidad"];
                if($cantUsoOtros > $cantAux){
                    $cantAux = $cantUsoOtros;
                }
                
                $productosUsoCliente = $detalles1->productosUsoCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //$productosUsoServidor = $detalles1->productosUsoServidor($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //fin software en uso
                
                $tabla .= '<div id="ttabla1"  style="width:99%; margin:10px; float:left;">
                    <table class="tablap">
                        <thead>
                        <tr style="background:#333; color:#fff;">
                            <!--<th  align="center" valign="middle"><span>&nbsp;</span></th>-->
                            <th  align="center" valign="middle"><span>OS</span></th>
                            <th  align="center" valign="middle"><span>Adobe Acrobat</span></th>
                            <th  align="center" valign="middle"><span>Adobe Creative Cloud</span></th>
                            <th  align="center" valign="middle"><span>Adobe Creative Suite</span></th>
                            <th  align="center" valign="middle"><span>Otros</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <!--<th align="center"><span></span></th>-->
                                <td class="text-center">' . $cantUsoOS . '</td>
                                <td class="text-center">' . $cantUsoAdobe . '</td>
                                <td class="text-center">' . $cantUsoCloud . '</td>
                                <td class="text-center">' . $cantUsoCreative . '</td>
                                <td class="text-center">' . $cantUsoOtros . '</td>
                                <td class="text-center">' . $porcentajeUsoCliente  . '</td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <table class="tablap" id="tablaDesusoCliente">
                        <thead>
                        <!--<tr>
                            <th align="center" valign="middle" colspan="5"><span>Clientes</span></th>
                        </tr>-->
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>Familia</span></th>
                            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Cantidad</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>'; 
                            foreach($productosUsoCliente as $row){
                                $tabla .= '<tr>
                                    <td class="text-left">' . $row["familia"] . '</td>
                                    <td class="text-left">' . $row["edicion"] . '</td>
                                    <td class="text-left">' . $row["version"] . '</td>
                                    <td class="text-center">' . $row["cantidad"] . '</td>
                                    <td class="text-center">';
                                    if($cantUsoOS > 0){
                                        $tabla .= round($row["cantidad"] * 100 / $cantUsoOS, 0);
                                    } else{
                                        $tabla .= 0;
                                    }
                                    $tabla .= '</td>
                                </tr>';
                            }
                        $tabla .= '</tbody>
                    </table>
                </div>';                
            } else if($fabricante == 2){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
                $detalles1 = new DetallesIbm();
                //inicio software en uso
                $totalEquiposOS = 0;

                $usoWindows = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows');
                //$usoWindowsServer = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows Server');
                $instaladosIBMDSStorage = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM DS Storage");
                $instaladosIBMHigh = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM High");
                $instaladosIBMInformix = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Informix");
                $instaladosIBMSPSS = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM SPSS");
                $instaladosIBMIntegration = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Integration");
                $instaladosIBMSecurityAppScan = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Security AppScan");                
                $instaladosIBMSterlingCertificate = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Sterling Certificate");
                $instaladosIBMSterlingConnect = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Sterling Connect");
                $instaladosIBMSterlingExternal = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Sterling External");
                $instaladosIBMSterlingSecureProxy = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Sterling Secure Proxy");
                $instaladosIBMTivoliStorage = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Tivoli Storage");
                $instaladosIBMWebservice = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM Web service");   
                $instaladosIBMWebSphere = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "IBM WebSphere");                
                
                $cantUsoOS = $usoWindows["cantidad"];

                $cantUsoIBMDSStorage = $instaladosIBMDSStorage["cantidad"];
                $cantAux = $cantUsoIBMDSStorage;

                $cantUsoIBMHigh = $instaladosIBMHigh["cantidad"];      
                if($cantUsoIBMHigh > $cantAux){
                    $cantAux = $cantUsoIBMHigh;
                }

                $cantUsoIBMInformix = $instaladosIBMInformix["cantidad"];
                if($cantUsoIBMInformix > $cantAux){
                    $cantAux = $cantUsoIBMInformix;
                }

                $cantUsoIBMSPSS = $instaladosIBMSPSS["cantidad"];      
                if($cantUsoIBMSPSS > $cantAux){
                    $cantAux = $cantUsoIBMSPSS;
                }

                $cantUsoIBMIntegration = $instaladosIBMIntegration["cantidad"];
                if($cantUsoIBMIntegration > $cantAux){
                    $cantAux = $cantUsoIBMIntegration;
                }
                
                $cantUsoIBMSecurityAppScan = $instaladosIBMSecurityAppScan["cantidad"];      
                if($cantUsoIBMSecurityAppScan > $cantAux){
                    $cantAux = $cantUsoIBMSecurityAppScan;
                }

                $cantUsoIBMSterlingCertificate = $instaladosIBMSterlingCertificate["cantidad"];
                if($cantUsoIBMSterlingCertificate > $cantAux){
                    $cantAux = $cantUsoIBMSterlingCertificate;
                }

                $cantUsoIBMSterlingConnect = $instaladosIBMSterlingConnect["cantidad"];      
                if($cantUsoIBMSterlingConnect > $cantAux){
                    $cantAux = $cantUsoIBMSterlingConnect;
                }

                $cantUsoIBMSterlingExternal = $instaladosIBMSterlingExternal["cantidad"];
                if($cantUsoIBMSterlingExternal > $cantAux){
                    $cantAux = $cantUsoIBMSterlingExternal;
                }
                
                $cantUsoIBMSterlingSecureProxy = $instaladosIBMSterlingSecureProxy["cantidad"];      
                if($cantUsoIBMSterlingSecureProxy > $cantAux){
                    $cantAux = $cantUsoIBMSterlingSecureProxy;
                }

                $cantUsoIBMTivoliStorage = $instaladosIBMTivoliStorage["cantidad"];
                if($cantUsoIBMTivoliStorage > $cantAux){
                    $cantAux = $cantUsoIBMTivoliStorage;
                }

                $cantUsoIBMWebservice = $instaladosIBMWebservice["cantidad"];      
                if($cantUsoIBMWebservice > $cantAux){
                    $cantAux = $cantUsoIBMWebservice;
                }

                $cantUsoIBMWebSphere = $instaladosIBMWebSphere["cantidad"];
                if($cantUsoIBMWebSphere > $cantAux){
                    $cantAux = $cantUsoIBMWebSphere;
                }
                
                if($cantUsoOS > 0){
                    $porcentajeUsoCliente = round($cantAux * 100 / $cantUsoOS, 0);
                } else{
                    $porcentajeUsoCliente = 0;
                }
                
                $productosUsoCliente = $detalles1->productosUsoCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //$productosUsoServidor = $detalles1->productosUsoServidor($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //fin software en uso
                
                $tabla .= '<div id="ttabla1"  style="width:99%; margin:10px; float:left; overflow-x:auto; overflow-y:hidden;">
                    <table class="tablap" style="width:1500px;">
                        <thead>
                        <tr style="background:#333; color:#fff;">
                            <!--<th  align="center" valign="middle"><span>&nbsp;</span></th>-->
                            <th  align="center" valign="middle"><span>OS</span></th>
                            <th  align="center" valign="middle"><span>IBM DS Storage</span></th>
                            <th  align="center" valign="middle"><span>IBM High</span></th>
                            <th  align="center" valign="middle"><span>IBM Informix</span></th>
                            <th  align="center" valign="middle"><span>IBM SPSS</span></th>
                            <th  align="center" valign="middle"><span>IBM Integration</span></th>
                            <th  align="center" valign="middle"><span>IBM Security AppScan</span></th>
                            <th  align="center" valign="middle"><span>IBM Sterling Certificate</span></th>
                            <th  align="center" valign="middle"><span>IBM Sterling Connect</span></th>
                            <th  align="center" valign="middle"><span>IBM Sterling External</span></th>
                            <th  align="center" valign="middle"><span>IBM Sterling Secure Proxy</span></th>
                            <th  align="center" valign="middle"><span>IBM Tivoli Storage</span></th>
                            <th  align="center" valign="middle"><span>IBM Web service</span></th>
                            <th  align="center" valign="middle"><span>IBM WebSphere</span></th>                            
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <!--<th align="center"><span></span></th>-->
                                <td class="text-center">' . $cantUsoOS . '</td>
                                <td class="text-center">' . $cantUsoIBMDSStorage . '</td>
                                <td class="text-center">' . $cantUsoIBMHigh . '</td>
                                <td class="text-center">' . $cantUsoIBMInformix . '</td>
                                <td class="text-center">' . $cantUsoIBMSPSS . '</td>
                                <td class="text-center">' . $cantUsoIBMIntegration . '</td>
                                <td class="text-center">' . $cantUsoIBMSecurityAppScan . '</td>
                                <td class="text-center">' . $cantUsoIBMSterlingCertificate . '</td>
                                <td class="text-center">' . $cantUsoIBMSterlingConnect . '</td>
                                <td class="text-center">' . $cantUsoIBMSterlingExternal . '</td>
                                <td class="text-center">' . $cantUsoIBMSterlingSecureProxy . '</td>
                                <td class="text-center">' . $cantUsoIBMTivoliStorage . '</td>
                                <td class="text-center">' . $cantUsoIBMWebservice . '</td>
                                <td class="text-center">' . $cantUsoIBMWebSphere . '</td>
                                <td class="text-center">' . $porcentajeUsoCliente  . '</td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <table class="tablap" id="tablaDesusoCliente">
                        <thead>
                        <!--<tr>
                            <th align="center" valign="middle" colspan="5"><span>Clientes</span></th>
                        </tr>-->
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>Familia</span></th>
                            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Cantidad</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>'; 
                            foreach($productosUsoCliente as $row){
                                $tabla .= '<tr>
                                    <td class="text-left">' . $row["familia"] . '</td>
                                    <td class="text-left">' . $row["edicion"] . '</td>
                                    <td class="text-left">' . $row["version"] . '</td>
                                    <td class="text-center">' . $row["cantidad"] . '</td>
                                    <td class="text-center">';
                                    if($cantUsoOS > 0){
                                        $tabla .= round($row["cantidad"] * 100 / $cantUsoOS, 0);
                                    } else{
                                        $tabla .= 0;
                                    }
                                    $tabla .= '</td>
                                </tr>';
                            }
                        $tabla .= '</tbody>
                    </table>
                </div>';
            } else if($fabricante == 3){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
                $detalles1 = new DetallesE_f();
                //inicio software en uso
                $totalEquiposOS = 0;

                $usoWindows = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows');
                $usoWindowsServer = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows Server');
                $instaladosOffice = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Office", "Windows");
                $instaladosVisio = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Visio", "Windows");
                $instaladosProject = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Project", "Windows");
                $instaladosVisualStudio = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Visual Studio", "Windows");
                $instaladosSQLServer = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "SQL Server", "Windows Server");

                $familia = array("Office", "Visio", "Project", "Visual Studio", "SQL Server");
                $usoOtros = $detalles1->cantidadProductosOtrosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], $familia, "Windows");

                $cantUsoOS = $usoWindows["cantidad"]; 
                $cantUsoWindowsServer = $usoWindowsServer["cantidad"];

                $totalEquiposWindowsServer = $cantUsoWindowsServer;

                $cantUsoOffice = $instaladosOffice["cantidad"];
                $cantAux = $cantUsoOffice;

                $cantUsoVisio = $instaladosVisio["cantidad"];      
                if($cantUsoVisio > $cantAux){
                    $cantAux = $cantUsoVisio;
                }

                $cantUsoProject = $instaladosProject["cantidad"];
                if($cantUsoProject > $cantAux){
                    $cantAux = $cantUsoProject;
                }

                $cantUsoVisualStudio = $instaladosVisualStudio["cantidad"];
                if($cantUsoVisualStudio > $cantAux){
                    $cantAux = $cantUsoVisualStudio;
                }

                $cantUsoSQLServer = $instaladosSQLServer["cantidad"];
                $cantUsoOtros =  $usoOtros["cantidad"];
                if($cantUsoOtros > $cantAux){
                    $cantAux = $cantUsoOtros;
                }

                if($cantUsoOS > 0){
                    $porcentajeUsoCliente = round($cantAux * 100 / $cantUsoOS, 0);
                } else{
                    $porcentajeUsoCliente = 0;
                }

                if($totalEquiposWindowsServer > 0){
                    $porcentajeUsoServidor = round($cantUsoSQLServer * 100 / $totalEquiposWindowsServer, 0);
                } else{
                    $porcentajeUsoServidor = 0;
                }


                $productosUsoCliente = $detalles1->productosUsoCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $productosUsoServidor = $detalles1->productosUsoServidor($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //fin software en uso
                
                $tabla .= '<div id="ttabla1"  style="width:99%; margin:10px; float:left;">
                    <table class="tablap">
                        <thead>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>&nbsp;</span></th>
                            <th  align="center" valign="middle"><span>OS</span></th>
                            <th  align="center" valign="middle"><span>Office</span></th>
                            <th  align="center" valign="middle"><span>Visio</span></th>
                            <th  align="center" valign="middle"><span>Project</span></th>
                            <th  align="center" valign="middle"><span>Visual Studio</span></th>
                            <th  align="center" valign="middle"><span>Otros</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th align="center"><span>Clientes</span></th>
                                <td class="text-center">' . $cantUsoOS . '</td>
                                <td class="text-center">' . $cantUsoOffice . '</td>
                                <td class="text-center">' . $cantUsoVisio . '</td>
                                <td class="text-center">' . $cantUsoProject . '</td>
                                <td class="text-center">' . $cantUsoVisualStudio . '</td>
                                <td class="text-center">' . $cantUsoOtros . '</td>
                                <td class="text-center">' . $porcentajeUsoCliente  . '</td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <table class="tablap">
                        <thead>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>&nbsp;</span></th>
                            <th  align="center" valign="middle"><span>Windows Server</span></th>
                            <th  align="center" valign="middle"><span>SQL Server</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th align="center"><span>Servidores</span></th>
                                <td class="text-center">' . $cantUsoWindowsServer . '</td>
                                <td class="text-center">' . $cantUsoSQLServer . '</td>
                                <td class="text-center">' . $porcentajeUsoCliente . '</td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <table class="tablap" id="tablaDesusoCliente">
                        <thead>
                        <tr>
                            <th align="center" valign="middle" colspan="5"><span>Clientes</span></th>
                        </tr>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>Familia</span></th>
                            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Cantidad</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>'; 
                            foreach($productosUsoCliente as $row){
                                $tabla .= '<tr>
                                    <td class="text-left">' . $row["familia"] . '</td>
                                    <td class="text-left">' . $row["edicion"] . '</td>
                                    <td class="text-left">' . $row["version"] . '</td>
                                    <td class="text-center">' . $row["cantidad"] . '</td>
                                    <td class="text-center">';
                                    if($cantUsoOS > 0){
                                        $tabla .= round($row["cantidad"] * 100 / $cantUsoOS, 0);
                                    } else{
                                        $tabla .= 0;
                                    }
                                    $tabla .= '</td>
                                </tr>';
                            }
                        $tabla .= '</tbody>
                    </table>

                    <br>

                    <table class="tablap" id="tablaDesusoServidor">
                        <thead>
                        <tr>
                            <th align="center" valign="middle" colspan="5"><span>Servidores</span></th>
                        </tr>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>Familia</span></th>
                            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Cantidad</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>'; 
                            foreach($productosUsoServidor as $row){
                                $tabla .= '<tr>
                                    <td class="text-left">' . $row["familia"] . '</td>
                                    <td class="text-left">' . $row["edicion"] . '</td>
                                    <td class="text-left">' . $row["version"] . '</td>
                                    <td class="text-center">' . $row["cantidad"] . '</td>
                                    <td class="text-center">';
                                    if ($cantUsoWindowsServer > 0){
                                        $tabla .= round($row["cantidad"] * 100 / $cantUsoWindowsServer, 0);
                                    } else{
                                        $tabla .= 0;
                                    }
                                    $tabla .= '</td>
                                </tr>';
                            }
                        $tabla .= '</tbody>
                    </table>
                </div>';
            } else if($fabricante == 4){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");
                $detalles1 = new DetallesOracle();
                //inicio software en uso
                $totalEquiposOS = 0;

                $usoWindows = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows');
                //$usoWindowsServer = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows Server');
                $instaladosApplication = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Application");
                $instaladosDatabase = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Database");
                $instaladosMiddleware = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Middleware");

                $cantUsoOS = $usoWindows["cantidad"];

                $cantUsoApplication = $instaladosApplication["cantidad"];
                $cantAux = $cantUsoApplication;

                $cantUsoDatabase = $instaladosDatabase["cantidad"];      
                if($cantUsoDatabase > $cantAux){
                    $cantAux = $cantUsoDatabase;
                }

                $cantUsoMiddleware = $instaladosMiddleware["cantidad"];
                if($cantUsoMiddleware > $cantAux){
                    $cantAux = $cantUsoMiddleware;
                }

                if($cantUsoOS > 0){
                    $porcentajeUsoCliente = round($cantAux * 100 / $cantUsoOS, 0);
                } else{
                    $porcentajeUsoCliente = 0;
                }
               
                $productosUsoCliente = $detalles1->productosUsoCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //$productosUsoServidor = $detalles1->productosUsoServidor($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //fin software en uso
                
                $tabla .= '<div id="ttabla1"  style="width:99%; margin:10px; float:left;">
                    <table class="tablap">
                        <thead>
                        <tr style="background:#333; color:#fff;">
                            <!--<th  align="center" valign="middle"><span>&nbsp;</span></th>-->
                            <th  align="center" valign="middle"><span>OS</span></th>
                            <th  align="center" valign="middle"><span>Application</span></th>
                            <th  align="center" valign="middle"><span>Database</span></th>
                            <th  align="center" valign="middle"><span>Middleware</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <!--<th align="center"><span></span></th>-->
                                <td class="text-center">' . $cantUsoOS . '</td>
                                <td class="text-center">' . $cantUsoApplication . '</td>
                                <td class="text-center">' . $cantUsoDatabase . '</td>
                                <td class="text-center">' . $cantUsoMiddleware . '</td>
                                <td class="text-center">' . $porcentajeUsoCliente  . '</td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <table class="tablap" id="tablaDesusoCliente">
                        <thead>
                        <!--<tr>
                            <th align="center" valign="middle" colspan="5"><span>Clientes</span></th>
                        </tr>-->
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>Familia</span></th>
                            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Cantidad</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>'; 
                            foreach($productosUsoCliente as $row){
                                $tabla .= '<tr>
                                    <td class="text-left">' . $row["familia"] . '</td>
                                    <td class="text-left">' . $row["edicion"] . '</td>
                                    <td class="text-left">' . $row["version"] . '</td>
                                    <td class="text-center">' . $row["cantidad"] . '</td>
                                    <td class="text-center">';
                                    if($cantUsoOS > 0){
                                        $tabla .= round($row["cantidad"] * 100 / $cantUsoOS, 0);
                                    } else{
                                        $tabla .= 0;
                                    }
                                    $tabla .= '</td>
                                </tr>';
                            }
                        $tabla .= '</tbody>
                    </table>
                </div>';
            } else if($fabricante == 10){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
                $detalles1 = new Detalles_SPLA();
                //inicio software en uso
                $totalEquiposOS = 0;

                $usoWindows = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows');
                $usoWindowsServer = $detalles1->cantidadProductosOSUso($_SESSION["client_id"], $_SESSION["client_empleado"], 'Windows Server');
                $instaladosOffice = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Office", "Windows");
                $instaladosVisio = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Visio", "Windows");
                $instaladosProject = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Project", "Windows");
                $instaladosVisualStudio = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "Visual Studio", "Windows");
                $instaladosSQLServer = $detalles1->cantidadProductosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], "SQL Server", "Windows Server");

                $familia = array("Office", "Visio", "Project", "Visual Studio", "SQL Server");
                $usoOtros = $detalles1->cantidadProductosOtrosInstalados($_SESSION["client_id"], $_SESSION["client_empleado"], $familia, "Windows");

                $cantUsoOS = $usoWindows["cantidad"]; 
                $cantUsoWindowsServer = $usoWindowsServer["cantidad"];

                $totalEquiposWindowsServer = $cantUsoWindowsServer;

                $cantUsoOffice = $instaladosOffice["cantidad"];
                $cantAux = $cantUsoOffice;

                $cantUsoVisio = $instaladosVisio["cantidad"];      
                if($cantUsoVisio > $cantAux){
                    $cantAux = $cantUsoVisio;
                }

                $cantUsoProject = $instaladosProject["cantidad"];
                if($cantUsoProject > $cantAux){
                    $cantAux = $cantUsoProject;
                }

                $cantUsoVisualStudio = $instaladosVisualStudio["cantidad"];
                if($cantUsoVisualStudio > $cantAux){
                    $cantAux = $cantUsoVisualStudio;
                }

                $cantUsoSQLServer = $instaladosSQLServer["cantidad"];
                $cantUsoOtros =  $usoOtros["cantidad"];
                if($cantUsoOtros > $cantAux){
                    $cantAux = $cantUsoOtros;
                }

                if($cantUsoOS > 0){
                    $porcentajeUsoCliente = round($cantAux * 100 / $cantUsoOS, 0);
                } else{
                    $porcentajeUsoCliente = 0;
                }

                if($totalEquiposWindowsServer > 0){
                    $porcentajeUsoServidor = round($cantUsoSQLServer * 100 / $totalEquiposWindowsServer, 0);
                } else{
                    $porcentajeUsoServidor = 0;
                }


                $productosUsoCliente = $detalles1->productosUsoCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $productosUsoServidor = $detalles1->productosUsoServidor($_SESSION["client_id"], $_SESSION["client_empleado"]);
                //fin software en uso
                
                $tabla .= '<div id="ttabla1"  style="width:99%; margin:10px; float:left;">
                    <table class="tablap">
                        <thead>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>&nbsp;</span></th>
                            <th  align="center" valign="middle"><span>OS</span></th>
                            <th  align="center" valign="middle"><span>Office</span></th>
                            <th  align="center" valign="middle"><span>Visio</span></th>
                            <th  align="center" valign="middle"><span>Project</span></th>
                            <th  align="center" valign="middle"><span>Visual Studio</span></th>
                            <th  align="center" valign="middle"><span>Otros</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th align="center"><span>Clientes</span></th>
                                <td class="text-center">' . $cantUsoOS . '</td>
                                <td class="text-center">' . $cantUsoOffice . '</td>
                                <td class="text-center">' . $cantUsoVisio . '</td>
                                <td class="text-center">' . $cantUsoProject . '</td>
                                <td class="text-center">' . $cantUsoVisualStudio . '</td>
                                <td class="text-center">' . $cantUsoOtros . '</td>
                                <td class="text-center">' . $porcentajeUsoCliente  . '</td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <table class="tablap">
                        <thead>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>&nbsp;</span></th>
                            <th  align="center" valign="middle"><span>Windows Server</span></th>
                            <th  align="center" valign="middle"><span>SQL Server</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th align="center"><span>Servidores</span></th>
                                <td class="text-center">' . $cantUsoWindowsServer . '</td>
                                <td class="text-center">' . $cantUsoSQLServer . '</td>
                                <td class="text-center">' . $porcentajeUsoCliente . '</td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <table class="tablap" id="tablaDesusoCliente">
                        <thead>
                        <tr>
                            <th align="center" valign="middle" colspan="5"><span>Clientes</span></th>
                        </tr>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>Familia</span></th>
                            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Cantidad</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>'; 
                            foreach($productosUsoCliente as $row){
                                $tabla .= '<tr>
                                    <td class="text-left">' . $row["familia"] . '</td>
                                    <td class="text-left">' . $row["edicion"] . '</td>
                                    <td class="text-left">' . $row["version"] . '</td>
                                    <td class="text-center">' . $row["cantidad"] . '</td>
                                    <td class="text-center">';
                                    if($cantUsoOS > 0){
                                        $tabla .= round($row["cantidad"] * 100 / $cantUsoOS, 0);
                                    } else{
                                        $tabla .= 0;
                                    }
                                    $tabla .= '</td>
                                </tr>';
                            }
                        $tabla .= '</tbody>
                    </table>

                    <br>

                    <table class="tablap" id="tablaDesusoServidor">
                        <thead>
                        <tr>
                            <th align="center" valign="middle" colspan="5"><span>Servidores</span></th>
                        </tr>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle"><span>Familia</span></th>
                            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
                            <th  align="center" valign="middle"><span>Cantidad</span></th>
                            <th  align="center" valign="middle"><span>%</span></th>
                        </tr>
                        </thead>
                        <tbody>'; 
                            foreach($productosUsoServidor as $row){
                                $tabla .= '<tr>
                                    <td class="text-left">' . $row["familia"] . '</td>
                                    <td class="text-left">' . $row["edicion"] . '</td>
                                    <td class="text-left">' . $row["version"] . '</td>
                                    <td class="text-center">' . $row["cantidad"] . '</td>
                                    <td class="text-center">';
                                    if ($cantUsoWindowsServer > 0){
                                        $tabla .= round($row["cantidad"] * 100 / $cantUsoWindowsServer, 0);
                                    } else{
                                        $tabla .= 0;
                                    }
                                    $tabla .= '</td>
                                </tr>';
                            }
                        $tabla .= '</tbody>
                    </table>
                </div>';
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);