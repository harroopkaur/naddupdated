<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }  
            
            $tabla = array();
            if($fabricante == 1){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
                $detallesAdobe = new DetallesAdobe();
                $detallesAdobe->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $tabla = $detallesAdobe->lista;
            } else if($fabricante == 2){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
                $detallesIBM = new DetallesIbm();
                $detallesIBM->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $tabla = $detallesIBM->lista;
            } else if($fabricante == 3){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
                $detallesMicrosoft = new DetallesE_f();
                $tabla = $detallesMicrosoft->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
            } else if($fabricante == 4){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");
                $detallesOracle = new DetallesOracle();
                $tabla = $detallesOracle->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
            } else if($fabricante == 10){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
                $detallesSPLA = new Detalles_SPLA();
                $tabla = $detallesSPLA->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
            }
            
            $listado = '<table id="listEquipos" class="tablap" id="tablaAlcance" style="width: 600px; margin-top:-5px;">
                <thead>
                <tr style="background:#333; color:#fff;">
                    <th  align="center" valign="middle"><span>&nbsp;</span></th>
                    <th  align="center" valign="middle" style="width:200px;"><span>Nombre Equipo</span></th>
                    <th  align="center" valign="middle" style="width:20%;"><span>Tipo</span></th>
                    <th  align="center" valign="middle" style="width:20%;"><span>Activo AD</span></th>
                    <th  align="center" valign="middle" style="width:20%;"><span>LA Tool</span></th>
                </tr>
                </thead>
                <tbody>';
            $i = 1;
            foreach($tabla as $reg_equipos2){
                $listado .= '<tr>
                        <td align="center">' . $i . '</td>
                        <td >' . $reg_equipos2["equipo"] . '</td>
                        <td align="center">';
                            if ($reg_equipos2["tipo"] == 1) {
                                $listado .= 'Cliente';
                            } else {
                                $listado .= 'Servidor';
                            }
                $listado .= '</td>
                        <td align="center">';
                            if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                                $listado .= 'Si';
                            } else {
                                $listado .= 'No';
                            }
                $listado .= '</td>
                        <td align="center">';
                            if ($reg_equipos2["errors"] == 'Ninguno') {
                                $listado .= 'Si';
                            } else {
                                $listado .= 'No';
                            }
                $listado .= '</td>
                    </tr>';
                $i++;
            }
            $listado .= '</tbody>
            </table>
            <script>
                $("#listEquipos").tablesorter();
                $("#listEquipos").tableHeadFixer();
            </script>';
                   
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$listado, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);