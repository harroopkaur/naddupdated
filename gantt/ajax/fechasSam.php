<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $funciones = new funcionesSam();
            $fabricante = new configuraciones(); 
            
            $id = 1;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["id"];
            }  

            $row = $fabricante->fabricanteEspecifico($id);
            $nombre = $row["nombre"];           
            
            $tabla = $funciones->fechaReporteSamGantt($_SESSION["client_id"], $_SESSION["client_empleado"], $id);
            
            $listado = "";
            foreach($tabla as $row){
                $listado .= '<tr>
                    <td class="rowMiddle">' . $row["fecha"] . '</td>
                    <td align="center" valign="middle"><a href="#" onclick="descargar(' . $id . ', ' . $row["archivo"] . ')"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_181_download_alt.png" border="0" alt="Descargar" title="Descargar"/></a></td>
                </tr>';
            }
                   
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$listado, 'nombre'=>$nombre, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);