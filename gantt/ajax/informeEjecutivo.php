<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            } 
            $listado = array();
            
            $tabla = "";
            if($fabricante == 1){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_adobe.php");
                $balance = new balanceAdobe();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            } else if($fabricante == 2){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
                $balance = new balanceIbm();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);                
            } else if($fabricante == 3){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
                $balanceMicrosoft = new Balance_f();
                $listado = $balanceMicrosoft->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            } else if($fabricante == 4){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_oracle.php");
                $balance = new balanceOracle();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            } else if($fabricante == 5){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_Sap.php");
                $balance = new balanceSap();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            } else if($fabricante == 6){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
                $balance = new balanceVMWare();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            } else if($fabricante == 7){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixIBM.php");
                $balance = new balanceUnixIBM();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            } else if($fabricante == 8){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
                $balance = new balanceUnixOracle();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            } else if($fabricante == 10){
                require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
                $balance = new Balance_SPLA();
                $listado = $balance->balanceEjecutivo($_SESSION["client_id"], $_SESSION["client_empleado"]);
            }
            
            $total = 0;
            $sobrante = 0;
            $faltante = 0;
            foreach($listado as $row){
                $total += $row["totalCompras"] * $row["precio"]; 
                
                if($row["total"] < 0){
                    $faltante +=  $row["total"];
                } else{
                    $sobrante += $row["total"];
                }
                $tabla .= '<tr>
                        <td class="text-center">' . $row["familia"] . '</td>
                        <td class="text-right">' . number_format(round($row["totalCompras"], 0), 0, ".", ",") . '</td>
                        <td class="text-right">' . number_format(round($row["totalInstalaciones"], 0), 0, ".", ",") . '</td>
                        <td class="text-right">' . number_format(round($row["neto"], 0), 0, ".", ",") . '</td>
                        <td class="text-right">' . number_format(round($row["precio"], 0), 0, ".", ",") . '</td>
                        <td class="text-right">' . number_format(round($row["total"], 0), 0, ".", ",") . '</td>
                    </tr>';
            }
            
            $tabla .= '<tr>
                        <td class="text-left bold">Inversión en SW</td>
                        <td class="text-right bold">' . number_format(round($total, 0), 0, ".", ",") . '</td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-left bold">Sobrante</td>
                        <td class="text-right bold">' . number_format(round($sobrante, 0), 0, ".", ",") . '</td>
                    </tr>
                    <tr>
                        <td class="text-left bold"></td>
                        <td class="text-right"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-left bold">Faltante</td>
                        <td class="text-right bold">' . number_format(round($faltante, 0), 0, ".", ",") . '</td>
                    </tr>';
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);