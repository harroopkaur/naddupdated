<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalenciasPDO.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $balance      = new balanceUnixOracle();
            $resumen      = new resumenUnixOracle();
            $equivalencia = new EquivalenciasPDO();

            $familia = "";
            if(isset($_POST["familia"])){
                $familia = $general->get_escape($_POST["familia"]);
            }
            
            $edicion = "";
            if(isset($_POST["edicion"])){
                $edicion = $general->get_escape($_POST["edicion"]);
            }
            $seleccion    = "";
            $TotalCompras = 0;
            $uso          = 0;
            $TotalInstal  = 0;	
            $Neto         = 0;        
            $titulo       = "";

            if(($familia != "Otros" && $edicion == "") || ($familia == "Otros" && $edicion == "")){
                if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], $familia)) {
                    foreach ($balance->lista as $reg_equipos) {
                        $TotalCompras += $reg_equipos["compra"];
                    } 
                }

                $lista = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], $familia);
                foreach ($lista as $reg_equipos) {
                    $uso  += $reg_equipos["cantidad"];
                }

                $TotalInstal = $uso;
                $Neto = $TotalCompras - $TotalInstal;
            }
            else if($familia != "Otros" && $edicion != ""){
                if($balance->listar_todo_familias($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion)) {
                    foreach ($balance->lista as $reg_equipos) {
                        $TotalCompras += $reg_equipos["compra"];
                    }
                }

                $lista = $resumen->listar_datos6Agrupado($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion);
                foreach ($lista as $reg_equipos) {
                    $uso  += $reg_equipos["cantidad"];
                }

                $TotalInstal = $uso;
                $Neto = $TotalCompras - $TotalInstal;
            }
            else if($familia == "Otros"){
                if($balance->listar_todo_familias1($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion)) {
                    foreach ($balance->lista as $reg_equipos) {
                        $TotalCompras += $reg_equipos["compra"];
                    }
                    $Neto = $TotalCompras - $TotalInstal;
                }

                if($resumen->listar_datos5Agrupado($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion)) {
                    foreach ($resumen->lista as $reg_equipos) {
                        $uso  += $reg_equipos["cantidad"];
                    }
                }

                $TotalInstal = $uso;
                $Neto = $TotalCompras - $TotalInstal;
            }

            if($familia != "Otros"){
                $titulo    = $familia . " " . $edicion;
                $ediciones = $equivalencia->edicionesProductoxNombre(8, $familia);
            }
            else{
                $titulo    = $edicion;
                $balance->productosNoIncluir($_SESSION["client_id"], $_SESSION['client_empleado']); 
                $ediciones = $equivalencia->ProductoxNombre(8, $balance->listaNoIncluir);
                $edicion   = $ediciones[0]["nombre"];
                if($_POST["edicion"] == ""){
                    $titulo    = $edicion;
                }
            }

            $tabla = "";
            foreach($balance->lista as $reg_equipos){
                $tabla .= '<tr>
                        <td>' . $reg_equipos["familia"] . '</td>
                        <td>' . $reg_equipos["office"] . '</td>
                        <td>' . $reg_equipos["version"] . '</td>
                        <td align="center">' . $reg_equipos["instalaciones"] . '</td>
                        <td align="center">' . round($reg_equipos["compra"], 0) . '</td>
                        <td align="center"';
                if($reg_equipos["balance"] < 0){
                    $tabla .= ' style="color:red;"';
                }
                $tabla .= '>' . round($reg_equipos["balance"], 0) . '</td>
                </tr>';
            }

            $options = "<option value=''>Seleccione..</option>";
            foreach($ediciones as $row){
                $options .= "<option value='". $row["nombre"] ."' ";
                if($row["nombre"] == $titulo){
                    $options .= "selected='selected'";
                }
                $options .= ">" . $row["nombre"] . "</option>";
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'titulo'=>$titulo, 'compra'=>$TotalCompras, 
            'instalacion'=>$TotalInstal, 'tabla'=>$tabla, 'edicion'=>$options, 'neto'=>$Neto, 'uso'=>$uso, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);