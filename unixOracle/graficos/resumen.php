<script type="text/javascript">
    $(function () {
        $('#container3').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Application'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Neto',
                data: [0, 0, <?= $ApplicationNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalApplicationUsar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalApplicationCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container4').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Middleware'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Neto',
                data: [0, 0, <?= $MiddlewareNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalMiddlewareUsar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalMiddlewareCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container5').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Database'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },

            series: [{
                name: 'Neto',
                data: [0, 0, <?= $DatabaseNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'En Uso',
                data: [0, <?= $TotalDatabaseUsar ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $TotalDatabaseCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });
    });
</script>