<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

$tablaMaestra = new tablaMaestra();
$balance  = new balanceUnixOracle();
$compras  = new comprasUnixOracle();
$resumen  = new resumenUnixOracle();
$general = new General();
$tablaMaestra->productosSalida(8);
$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';
    
$TotalApplicationCompras = 0;
$TotalApplicationUsar    = 0;
$TotalApplicationSinUsar = 0;

$TotalMiddlewareCompras = 0;
$TotalMiddlewareUsar    = 0;
$TotalMiddlewareSinUsar = 0;

$TotalDatabaseCompras = 0;
$TotalDatabaseUsar    = 0;
$TotalDatabaseSinUsar = 0;

$clientTotalProductCompras = 0;
$clientTotalProductInstal  = 0;
$clientProductNeto         = 0;

//Application
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Application")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalApplicationCompras += $reg_equipos["compra"];
    }
}

$listaApplication = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "Application");
foreach ($listaApplication as $reg_equipos) {
    $TotalApplicationUsar  += $reg_equipos["cantidad"];
}

$TotalApplicationInstal = $TotalApplicationUsar;
$ApplicationNeto        = $TotalApplicationCompras - $TotalApplicationInstal;


//Middleware
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Middleware")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalMiddlewareCompras += $reg_equipos["compra"];
    }
}

$listaMiddleware = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "Middleware");
foreach ($listaMiddleware as $reg_equipos) {
    $TotalMiddlewareUsar  += $reg_equipos["cantidad"];
}

$TotalMiddlewareInstal = $TotalMiddlewareUsar;
$MiddlewareNeto        = $TotalMiddlewareCompras - $TotalMiddlewareInstal;

//Database
if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Database")) {
    foreach ($balance->lista as $reg_equipos) {
        $TotalDatabaseCompras += $reg_equipos["compra"];
    }
}

$listaDatabase = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "Database");
foreach ($listaDatabase as $reg_equipos) {
    $TotalDatabaseUsar  += $reg_equipos["cantidad"];
}

$TotalDatabaseInstal = $TotalDatabaseUsar;
$DatabaseNeto        = $TotalDatabaseCompras - $TotalDatabaseInstal;