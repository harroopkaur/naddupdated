<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_OracleSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_OracleAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_OracleLinux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_OracleSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_OracleAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_OracleLinux.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$compra_datos   = new comprasUnixOracle();
$resumenSolaris = new resumenOracleSolaris();
$resumenAIX     = new resumenOracleAIX();
$resumenLinux   = new resumenOracleLinux();
$consolidadoSolaris  = new consolidadoOracleSolaris();
$consolidadoAIX  = new consolidadoOracleAIX();
$consolidadoLinux  = new consolidadoOracleLinux();
$balance_datos  = new balanceUnixOracle();
$equivalencia   = new Equivalencias();
$general        = new General();
$archivosDespliegue   = new clase_archivos_fabricantes();
$nueva_funcion = new funcionesSam();
$tablaMaestra = new tablaMaestra();
$log = new log();

$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 8);

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if (isset($_POST['insertar']) && $_POST['insertar'] == 1) {
    //inicio resumen UNIX-Solaris
    $resumenSolaris->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

    $tablaMaestra->listadoProductosMaestra(8);
    foreach($tablaMaestra->listaProductos as $rowProductos){
        if($consolidadoSolaris->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["descripcion"], $rowProductos["campo2"])){
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            
            foreach($consolidadoSolaris->lista as $reg_r){  
                $producto = "";

                if($tablaMaestra->productosSustituir(8, $reg_r["product"])){
                    $producto  = $tablaMaestra->sustituir;
                    $idForaneo = $tablaMaestra->idForaneo;
                }

                $edicion = "";
                $version = "";               

                if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                    if($tablaMaestra->sustituir == null){
                        $version = "";
                    }
                    else{
                        $version = $tablaMaestra->sustituir;
                    }
                }

                $tablaMaestra->sustituir = "";
                if($tablaMaestra->buscarSustituirMaestraCampo2($reg_r["product"], $idForaneo, "solaris", 5)){
                    if($tablaMaestra->sustituir == null){
                        $edicion = "";
                    }
                    else{
                        $edicion = $tablaMaestra->sustituir;
                    }
                }
                
                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                } else {
                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                } 

                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                $bloqueValores[":equipo" . $j] = $general->truncarString($reg_r["equipo"], 70);
                $bloqueValores[":familia" . $j] = $general->truncarString($producto, 70);
                $bloqueValores[":edicion" . $j] = $general->truncarString($edicion, 70);
                $bloqueValores[":version" . $j] = $general->truncarString($version, 70);
                
                if($j == $general->registrosBloque){
                    if(!$resumenSolaris->insertarEnBloque($bloque, $bloqueValores)){ 
                        echo $resumenSolaris->error;
                    }
                    
                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
                
                //$resumenLinux->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_r["equipo"], $producto, $edicion, $version);
            }  
            
            if($insertarBloque === true){
                if(!$resumenSolaris->insertarEnBloque($bloque, $bloqueValores)){    
                    echo $resumenSolaris->error;
                }
            }
        }
    }
    //fin resumen UNIX-Solaris
    
    //inicio resumen UNIX-AIX
    $resumenAIX->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

    $tablaMaestra->listadoProductosMaestra(8);
    foreach($tablaMaestra->listaProductos as $rowProductos){
        if($consolidadoAIX->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["descripcion"], $rowProductos["campo2"])){
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            
            foreach($consolidadoAIX->lista as $reg_r){  
                $producto = "";

                if($tablaMaestra->productosSustituir(8, $reg_r["product"])){
                    $producto  = $tablaMaestra->sustituir;
                    $idForaneo = $tablaMaestra->idForaneo;
                }

                $edicion = "";
                $version = "";               

                if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                    if($tablaMaestra->sustituir == null){
                        $version = "";
                    }
                    else{
                        $version = $tablaMaestra->sustituir;
                    }
                }

                $tablaMaestra->sustituir = "";
                if($tablaMaestra->buscarSustituirMaestraCampo2($reg_r["product"], $idForaneo, "aix", 5)){
                    if($tablaMaestra->sustituir == null){
                        $edicion = "";
                    }
                    else{
                        $edicion = $tablaMaestra->sustituir;
                    }
                }
                
                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                } else {
                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                } 

                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                $bloqueValores[":equipo" . $j] = $general->truncarString($reg_r["equipo"], 70);
                $bloqueValores[":familia" . $j] = $general->truncarString($producto, 70);
                $bloqueValores[":edicion" . $j] = $general->truncarString($edicion, 70);
                $bloqueValores[":version" . $j] = $general->truncarString($version, 70);
                
                if($j == $general->registrosBloque){
                    if(!$resumenAIX->insertarEnBloque($bloque, $bloqueValores)){ 
                        echo $resumenAIX->error;
                    }
                    
                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
                
                //$resumenLinux->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_r["equipo"], $producto, $edicion, $version);
            }  
            
            if($insertarBloque === true){
                if(!$resumenAIX->insertarEnBloque($bloque, $bloqueValores)){    
                    echo $resumenAIX->error;
                }
            }
        }
    }
    //fin resumen UNIX-AIX

    //inicio resumen UNIX-IBM
    $resumenLinux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

    $tablaMaestra->listadoProductosMaestra(8);
    foreach($tablaMaestra->listaProductos as $rowProductos){
        if($consolidadoLinux->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["descripcion"], $rowProductos["campo2"])){
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            
            foreach($consolidadoLinux->lista as $reg_r){  
                $producto = "";

                if($tablaMaestra->productosSustituir(8, $reg_r["product"])){
                    $producto  = $tablaMaestra->sustituir;
                    $idForaneo = $tablaMaestra->idForaneo;
                }

                $edicion = "";
                $version = "";               

                if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                    if($tablaMaestra->sustituir == null){
                        $version = "";
                    }
                    else{
                        $version = $tablaMaestra->sustituir;
                    }
                }

                $tablaMaestra->sustituir = "";
                if($tablaMaestra->buscarSustituirMaestraCampo2($reg_r["product"], $idForaneo, "linux", 5)){
                    if($tablaMaestra->sustituir == null){
                        $edicion = "";
                    }
                    else{
                        $edicion = $tablaMaestra->sustituir;
                    }
                }
                
                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                } else {
                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", :edicion" . $j . ", :version" . $j . ")";
                } 

                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                $bloqueValores[":equipo" . $j] = $general->truncarString($reg_r["equipo"], 70);
                $bloqueValores[":familia" . $j] = $general->truncarString($producto, 70);
                $bloqueValores[":edicion" . $j] = $general->truncarString($edicion, 70);
                $bloqueValores[":version" . $j] = $general->truncarString($version, 70);
                
                if($j == $general->registrosBloque){
                    if(!$resumenLinux->insertarEnBloque($bloque, $bloqueValores)){ 
                        echo $resumenLinux->error;
                    }
                    
                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
                
                //$resumenLinux->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_r["equipo"], $producto, $edicion, $version);
            }  
            
            if($insertarBloque === true){
                if(!$resumenLinux->insertarEnBloque($bloque, $bloqueValores)){    
                    echo $resumenLinux->error;
                }
            }
        }
    }
    //fin resumen UNIX-IBM

    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];

    // Validaciones
    $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
    $long = count($extension) - 1;
    if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
        $error = 1;
    }  // Permitir subir solo imagenes JPG,

    if ($error == 0) {

        $imagen = $_SESSION['client_id'] . $_SESSION["client_empleado"] . "_compraUnixOracle" . date("dmYHis") . ".csv";

        if (!$compra_datos->cargar_archivo($imagen, $temporal_imagen)) {
            $error = 5;
        } else {

            $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            $balance_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

            if($general->obtenerSeparador("archivos_csvf4/" . $imagen) === true){
                if (($fichero = fopen("archivos_csvf4/" . $imagen, "r")) !== FALSE) {
                    $i = 1;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i == 1 && ($datos[0] != "Producto" || utf8_encode($datos[1]) != "Edición" || utf8_encode($datos[2]) != "Versión" || 
                        $datos[3] != "SKU" || $datos[4] != "Tipo" || $datos[5] != "Cantidades" || $datos[6] != "Precio Unitario" ||
                        utf8_encode($datos[7]) != "Asignación")){
                            $error = 3;
                            break;
                        }
                        $i = 1;

                        $compraCantidad = 0;
                        if(filter_var($datos[5], FILTER_VALIDATE_INT) !== false){
                            $compraCantidad = $datos[5];
                        }
                        
                        $precio = 0;
                        if(filter_var($datos[6], FILTER_VALIDATE_FLOAT) !== false){
                            $precio = $datos[6];
                        }

                        if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[0], $datos[1], $datos[2], $compraCantidad, $precio)) {
                            echo $compra_datos->error;
                        } else {
                            $i++;
                            $exito = 1;
                        }//exito
                    }//WhILE
                    
                    if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 8) === false){
                        $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 8);
                    }
                    $archivosDespliegue->actualizarCompras($_SESSION["client_id"], $_SESSION["client_empleado"], 8, $imagen);
                }//FICHERO
            }
            else{
                $error = 4;
            }
        }//cago
    }//is error


    if ($exito == 1){
        $listaEquivalencia = $equivalencia->listar_todo(8); //8 es el id fabricante UNIX-Oracle
        if ($listaEquivalencia) {
            foreach ($listaEquivalencia as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;

                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
               
                if($compra_datos->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    $resultCompra = $compra_datos->row;
                    $compra       = $resultCompra["compra"];
                    $precio       = $resultCompra["precio"];
                }
                
                if ($resumenSolaris->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenSolaris->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen
                
                if ($resumenAIX->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenAIX->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen
                
                if ($resumenLinux->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenLinux->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen

                if($precio == 0 || $precio == ""){
                    $precio = $reg_o["precio"];
                }
                
                $balancec1 = $compra - $instalaciones;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                $tipo = 1;
             
                //$balance_datos->insertar($_SESSION['client_id'],$newFamilia,$newEdicion, $newVersion,$reg_o->precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
            }//office
        }//office

        $exito2 = 1;
        
        $log->insertar(24, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Compras Formulario");
    }
}