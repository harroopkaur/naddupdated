<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$tablaMaestra = new tablaMaestra();
$archivosDespliegue = new clase_archivos_fabricantes();
$general = new General();
$log = new log();

$exito=0;
$error=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;
$exito4=0;
$error4=0;
$exito5=0;
$error5=0;

$pestana = "solaris";
if(isset($_POST["opcionDespliegue"])){
    $pestana = $_POST["opcionDespliegue"];
}

$incremento = "false";
if(isset($_POST["incremento"]) && $_POST["incremento"] === "true"){
    $incremento = "true";
}

if(isset($_POST['insertar']) && $_POST['insertar'] == 1 && isset($_POST["opcionDespliegue"])) {
    if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // ValidacioWnes  
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
        }else{
            $error=2;	
        }
    }
    else{
        $error = 1;
    }

    if($error == 0) {
        if($pestana === "solaris" || $pestana === "incremento solaris"){
            $carpeta = "archivos_csvf1";
        }
        else if($pestana === "aix" || $pestana === "incremento aix"){
            $carpeta = "archivos_csvf2";
        }
        else if($pestana === "linux" || $pestana === "incremento linux"){
            $carpeta = "archivos_csvf3";
        }

        if(!file_exists($GLOBALS['app_root'] . "/unixOracle/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
            mkdir($GLOBALS['app_root'] . "/unixOracle/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
        }

        if(!file_exists($GLOBALS['app_root'] . "/unixOracle/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp")){
            mkdir($GLOBALS['app_root'] . "/unixOracle/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/tmp", 0777, true);
        }
        
        $nombreArchivo = "archivosConsolidados" . date("dmYHis") . ".rar";
        move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] . "/unixOracle/" . $carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/archivosConsolidados" . date("dmYHis") . ".rar");
        $rar_file = rar_open($carpeta . "/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $nombreArchivo) or die("Can't open Rar archive");

        $entries = rar_list($rar_file);

        foreach ($entries as $entry) {
            $entry->extract($carpeta . '/' . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . '/tmp/');
        }

        rar_close($rar_file);

        //$imagen = $_SESSION['client_id'] . "/" . $nombre_imagen;
        $pestanaAux = "";
        if($pestana == "solaris" || $pestana == "incremento solaris"){
            require_once($GLOBALS["app_root"] . "/unixOracle/procesos/solaris.php");
            $log->insertar(23, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue Solaris");
            //$pestanaAux = "solaris";
        }
        else if($pestana == "aix" || $pestana == "incremento aix"){
            require_once($GLOBALS["app_root"] . "/unixOracle/procesos/aix.php");
            $log->insertar(23, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue AIX");
            //$pestanaAux = "aix";
        }
        else if($pestana == "linux" || $pestana == "incremento linux"){
            require_once($GLOBALS["app_root"] . "/unixOracle/procesos/linux.php");
            $log->insertar(23, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue Linux");
            //$pestanaAux = "linux";
        }

        //inicio resumen UNIX-Oracle
        if($incremento === 'false'){
            $resumen->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        }
        
        $tablaMaestra->listadoProductosMaestra(8);
        foreach($tablaMaestra->listaProductos as $rowProductos){
            if($consolidado->listarProductos($_SESSION['client_id'], $_SESSION["client_empleado"], $rowProductos["descripcion"], $rowProductos["campo2"])){
                foreach($consolidado->lista as $reg_r){  
                    $producto = "";

                    if($tablaMaestra->productosSustituir(8, $reg_r["product"])){
                        $producto  = $tablaMaestra->sustituir;
                        $idForaneo = $tablaMaestra->idForaneo;
                    }

                    $edicion = "";
                    $version = "";               

                    if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                        if($tablaMaestra->sustituir == null){
                            $version = "";
                        }
                        else{
                            $version = $tablaMaestra->sustituir;
                        }
                    }
                    
                    $tablaMaestra->sustituir = "";
                    if($tablaMaestra->buscarSustituirMaestraCampo2($reg_r["product"], $idForaneo, $pestanaAux, 5)){
                        if($tablaMaestra->sustituir == null){
                            $edicion = "";
                        }
                        else{
                            $edicion = $tablaMaestra->sustituir;
                        }
                    }

                    $resumen->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_r["equipo"], $producto, $edicion, $version);

                }  
            }
        }
        //fin resumen UNIX-Oracle

        $files = glob($carpeta . '/' . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . '/tmp/*'); // obtiene todos los archivos
        foreach($files as $file => $valor){
            if(is_file($valor)){
                unlink($valor);
            }
        }
        $exito=1;  
    }
}