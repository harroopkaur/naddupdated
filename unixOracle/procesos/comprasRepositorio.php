<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_UnixOracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_OracleSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_OracleAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_OracleLinux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_OracleSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_OracleAIX.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_OracleLinux.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$compra_datos         = new comprasUnixOracle();
$resumenSolaris       = new resumenOracleSolaris();
$resumenAIX           = new resumenOracleAIX();
$resumenLinux         = new resumenOracleLinux();
$balance_datos        = new balanceUnixOracle();
$equivalencia         = new Equivalencias();
$general              = new General();
$detalleSolaris       = new DetallesOracleSolaris();
$detalleAIX           = new DetallesOracleAIX();
$detalleLinux         = new DetallesOracleLinux();
$nueva_funcion = new funcionesSam();
$log = new log();

$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 8);

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if (isset($_POST['insertarRepoF']) && $_POST["insertarRepoF"] == 1) {
    if ($error == 0) {
        $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $balance_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

        $tablaCompraRepo = array();
        if (isset($_POST["check"])) {
            $tablaCompraRepo = $_POST["check"];

            for ($i = 0; $i < count($tablaCompraRepo); $i++) {
                $newArray = explode("*", $tablaCompraRepo[$i]);
                $familia  = $general->get_escape($newArray[0]);
                $edicion  = $general->get_escape($newArray[1]);
                $version  = $general->get_escape($newArray[2]);
                $cantidad = 0;
                if(filter_var($newArray[3], FILTER_VALIDATE_FLOAT) !== false){
                    $cantidad = $newArray[3];
                }
                $precio = 0;
                if(filter_var($newArray[4], FILTER_VALIDATE_FLOAT) !== false){
                    $precio = $general->get_escape($newArray[4]);
                }
                if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], trim($familia), trim($edicion), trim($version), $cantidad, $precio)) {
                    echo $compra_datos->error;
                } else {

                    //$i++;

                    $exito = 1;
                }//exito*/
            }//WhILE
        }
    }

    if ($exito == 1){
        $listaEquivalencia = $equivalencia->listar_todo(8); //8 es el id fabricante UNIX-Oracle
        if ($listaEquivalencia) {
            foreach ($listaEquivalencia as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;
                
                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
                
                if($compra_datos->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    $resultCompra = $compra_datos->row;
                    $compra       = $resultCompra["compra"];
                    $precio       = $resultCompra["precio"];
                }
                
                if ($resumenSolaris->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenSolaris->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen
                
                if ($resumenAIX->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenAIX->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen
                
                if ($resumenLinux->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumenLinux->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen

                if($compra == ""){
                    $compra = 0;
                }
                
                if($precio == ""){
                    $precio = 0;
                }
                
                $balancec1 = $compra - $instalaciones;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                $tipo = 1;
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
            }//office
        }//office

        $exito2 = 1;
        
        $log->insertar(24, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Compras Repositorio");
    }
}