<?php
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_OracleSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_OracleSolaris.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_OracleSolaris.php");

$resumen      = new resumenOracleSolaris();
$consolidado  = new consolidadoOracleSolaris();
$detalle      = new DetallesOracleSolaris();

if($incremento == false){
    $consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
    $detalle->eliminar($_SESSION["client_id"], $_SESSION['client_empleado']);
}

$directorio = 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp';
$ficheros   = scandir($directorio);

$tablaMaestra->listadoEncontrarCampo(7);
$procesos  = $tablaMaestra->sustituirCamposDespliegue2(3,8,"Solaris","Procesos");
$variables = $tablaMaestra->sustituirCamposDespliegue2(3,8,"Solaris","Variable");
$archivos  = $tablaMaestra->sustituirCamposDespliegue2(3,8,"Solaris","Archivos");

foreach($ficheros as $row => $value){
    $memoriaEncon   = 0;
    $osEncon        = 0;
    $TIVsmCapiEncon = 0;
    $TIVsmCbaEncon  = 0;
    $mqmEncon       = 0;
    $equipo         = "";
    $os             = "";
    $memoria        = "";
    $cpu            = 0;
    $totalCores     = 0;


    $fichero = 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/' . $value;
    if($fichero != 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . '/tmp/.' && $fichero != 'archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . '/tmp/..'){
        if(($f = fopen($fichero, "r")) !== FALSE) {
            $process  = 0;
            $variable = 0;
            $file     = 0;
            
            $jDetalle  = 0;
            $bloqueDetalle = "";
            $bloqueValoresDetalle = array();
            $insertarBloqueDetalle = false;
            
            $softwareAux = array();
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            
            $jVariable = 0;
            $bloqueVariable = "";
            $bloqueValoresVariable = array();
            $insertarBloqueVariable = false;
         
            $jFile = 0;
            $bloqueFile = "";
            $bloqueValoresFile = array();
            $insertarBloqueFile = false;

            while(!feof($f)) {
                $linea = fgets($f);
                if(strpos($linea, $tablaMaestra->listaCampos[0]["descripcion"]) !== FALSE){
                    $aux = $tablaMaestra->listaCampos[0]["descripcion"];
                    $equipo = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[2]["descripcion"]) !== FALSE /*&& $osEncon == 1*/){
                    $aux = $tablaMaestra->listaCampos[2]["descripcion"];
                    $os = trim(substr($linea, strlen($aux), strlen($linea)));
                }

                if(strpos($linea, $tablaMaestra->listaCampos[1]["descripcion"]) !== FALSE /*&& $memoriaEncon == 0*/){
                    $memoAux = explode(" ", $linea);
                    $memoria = $memoAux[2];
                    //$memoriaEncon++;
                }

                if(strpos($linea, $tablaMaestra->listaCampos[3]["descripcion"]) !== FALSE){
                    $linea = trim(substr($linea, strpos($linea,"(") + 1, strlen($linea)));
                    $linea = trim(substr($linea, 0, strlen($linea) - 1));
                    $newArray = explode("-", $linea);
                    $cores = $newArray[1] - $newArray[0] + 1;
                    $totalCores += $cores;
                    $cpu++;
                }

                $software = "";
                if(strpos($linea, "Processes Found:") !== FALSE){
                    $process++;
                }

                if(strpos($linea, "Variables Found:") !== FALSE){
                    $process = -1;
                    $variable++;
                }

                if(strpos($linea, "Files Found:") !== FALSE){
                    $variable = -1;
                    $file++;
                }

                if($process > 0){
                    foreach($procesos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            /*if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $equipo, $software);
                            }*/
                            
                            if(count($softwareAux) == 0){
                                $softwareAux[0] = $software;
                            }
                            
                            if($equipo != "" && !$consolidado->verificarRegistroArray($softwareAux, $software)){
                                $softwareAux[] = $software;
                                if($j == 0){
                                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :software" . $j . ")";
                                } else {
                                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :software" . $j . ")";
                                } 
                                
                                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                                
                                /*$host = explode('.', $equipo);
                                if(filter_var($equipo, FILTER_VALIDATE_IP)!== false){
                                    $host[0] = $equipo;
                                }*/
                                $bloqueValores[":equipo" . $j] = $general->truncarString($equipo, 70);
                                $bloqueValores[":software" . $j] = $general->truncarString($software, 100);
                                //$consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                                $j++;
                            } 
                        }
                    }
                }

                if($variable > 0){
                    foreach($variables as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            /*if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $equipo, $software);
                            }*/
                            
                            if(count($softwareAux) == 0){
                                $softwareAux[0] = $software;
                            }
                            
                            if($equipo != "" && !$consolidado->verificarRegistroArray($softwareAux, $software)){
                                $softwareAux[] = $software;
                                if($jVariable == 0){
                                    $bloqueVariable .= "(:cliente" . $jVariable . ", :empleado" . $jVariable . ", :equipo" . $jVariable . ", :software" . $jVariable . ")";
                                } else {
                                    $bloqueVariable .= ", (:cliente" . $jVariable . ", :empleado" . $jVariable . ", :equipo" . $jVariable . ", :software" . $jVariable . ")";
                                } 
                                
                                $bloqueValoresVariable[":cliente" . $jVariable] = $_SESSION['client_id'];
                                $bloqueValoresVariable[":empleado" . $jVariable] = $_SESSION["client_empleado"];
                                
                                /*$host = explode('.', $equipo);
                                if(filter_var($equipo, FILTER_VALIDATE_IP)!== false){
                                    $host[0] = $equipo;
                                }*/
                                
                                $bloqueValoresVariable[":equipo" . $jVariable] = $general->truncarString($equipo, 70);
                                $bloqueValoresVariable[":software" . $jVariable] = $general->truncarString($software, 100);
                                //$consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                                $jVariable++;
                            }
                        }
                    }
                }

                if($file > 0){
                    foreach($archivos as $row){
                        if(strpos($linea, $row["descripcion"]) !== FALSE){
                            $software = $row["campo1"];
                            /*if($equipo != ""){
                                $consolidado->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $equipo, $software);
                            }*/
                            
                            if(count($softwareAux) == 0){
                                $softwareAux[0] = $software;
                            }
                            
                            if($equipo != "" && !$consolidado->verificarRegistroArray($softwareAux, $software)){
                                $softwareAux[] = $software;
                                if($jFile == 0){
                                    $bloqueFile .= "(:cliente" . $jFile . ", :empleado" . $jFile . ", :equipo" . $jFile . ", :software" . $jFile . ")";
                                } else {
                                    $bloqueFile .= ", (:cliente" . $jFile . ", :empleado" . $jFile . ", :equipo" . $jFile . ", :software" . $jFile . ")";
                                } 
                                
                                $bloqueValoresFile[":cliente" . $jFile] = $_SESSION['client_id'];
                                $bloqueValoresFile[":empleado" . $jFile] = $_SESSION["client_empleado"];
                                
                                /*$host = explode('.', $equipo);
                                if(filter_var($equipo, FILTER_VALIDATE_IP)!== false){
                                    $host[0] = $equipo;
                                }*/
                                
                                $bloqueValoresFile[":equipo" . $jFile] = $general->truncarString($equipo, 70);
                                $bloqueValoresFile[":software" . $jFile] = $general->truncarString($software, 100);
                                
                                $jFile++;
                                //$consolidado->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], $equipo, $software);
                            }
                        }
                    }
                }                           
            }
            
            if($j > 0 && !$consolidado->insertarEnBloque($bloque, $bloqueValores)){ 
                echo "Consolidado: " . $consolidado->error . "<br>";
            }
          
            if($jVariable > 0 && !$consolidado->insertarEnBloque($bloqueVariable, $bloqueValoresVariable)){ 
                echo "Consolidado Variable: " . $consolidado->error . "<br>";
            }
            
            if($jFile > 0 && !$consolidado->insertarEnBloque($bloqueFile, $bloqueValoresFile)){ 
                echo "Consolidado File: " . $consolidado->error . "<br>";
            }

            if($equipo != "" && $os != "" && $memoria != ""){
                if($jDetalle == 0){
                    $insertarBloqueDetalle = true;
                    $bloqueDetalle .= "(:cliente" . $jDetalle . ", :empleado" . $jDetalle . ", :equipo" . $jDetalle . ", "
                    . ":os" . $jDetalle . ", :memoria" . $jDetalle . ", :cpu" . $jDetalle . ", :core" . $jDetalle . ")";
                } else {
                    $bloqueDetalle .= ", (:cliente" . $jDetalle . ", :empleado" . $jDetalle . ", :equipo" . $jDetalle . ", "
                    . ":os" . $jDetalle . ", :memoria" . $jDetalle . ", :cpu" . $jDetalle . ", :core" . $jDetalle . ")";
                } 

                $bloqueValoresDetalle[":cliente" . $jDetalle] = $_SESSION['client_id'];
                $bloqueValoresDetalle[":empleado" . $jDetalle] = $_SESSION["client_empleado"];
                $bloqueValoresDetalle[":equipo" . $jDetalle] = $general->truncarString($equipo, 70);
                $bloqueValoresDetalle[":os" . $jDetalle] = $general->truncarString($os, 150);
                $bloqueValoresDetalle[":memoria" . $jDetalle] = $general->truncarString($memoria, 20);
                $bloqueValoresDetalle[":cpu" . $jDetalle] = $cpu;
                $bloqueValoresDetalle[":core" . $jDetalle] = $totalCores;
                
                //$detalle->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $equipo, $os, $memoria, $cpu, $totalCores);
                if($jDetalle == $general->registrosBloque){
                    if(!$detalle->insertarEnBloque($bloqueDetalle, $bloqueValoresDetalle)){ 
                        echo "Detalle: " . $detalle->error . "<br>";
                    }

                    $bloqueDetalle = "";
                    $bloqueValoresDetalle = array();
                    $jDetalle = -1;
                    $insertarBLoqueDetalle = false; 
                }
                $jDetalle++;
            } 
        }
        fclose($f);
        
        if($insertarBloqueDetalle === true){
            if(!$detalle->insertarEnBloque($bloqueDetalle, $bloqueValoresDetalle)){    
                echo "Detalle: " . $detalle->error . "<br>";
            }
        }
    }
}

if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 8) === false){
    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 8);
}

$archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], 8, $nombreArchivo, 0);