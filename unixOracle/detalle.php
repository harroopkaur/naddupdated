<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/unixOracle/procesos/detalle.php");
require_once($GLOBALS['app_root'] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 8;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="divMenuContenido">
            <?php
            $menuUnixOracle = 4;
            include_once($GLOBALS['app_root'] . "/unixOracle/plantillas/menu.php");
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php 
                include_once($GLOBALS['app_root'] . "/unixOracle/graficos/detalle.php");
                include_once($GLOBALS['app_root'] . "/unixOracle/plantillas/detalle.php");
                ?>  
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS['app_root'] . "/plantillas/pie.php");