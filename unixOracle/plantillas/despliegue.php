<input type="hidden" id="nombreArchivo">

<div style="width:98%; padding:20px; overflow:hidden;">
    <div style="width:auto;">
        <h1 class="textog negro" style="text-align:center; line-height:35px;" id="tituloDespliegue">
            <?php 
            $servidor = "Solaris";
            if($pestana === "solaris"){
                echo "Despliegue Solaris";
            }
            else if($pestana === "aix"){
                echo "Despliegue AIX";
                $servidor = "AIX";
            }
            else if($pestana === "linux"){
                echo "Despliegue Linux";
                $servidor = "Linux";
            } else if($pestana === "incremento solaris"){
                echo "Despliegue Incremento Solaris";
            }
            else if($pestana === "incremento aix"){
                echo "Despliegue Incremento AIX";
                $servidor = "AIX";
            }
            else if($pestana === "incremento linux"){
                echo "Despliegue Incremento Linux";
                $servidor = "Linux";
            }
            ?>
        </h1>
    </div>
    <br>

    <div style="text-align:center;">
        <div class="botonDespliegue" onclick="despliegue('solaris')">Solaris</div>
        <div class="botonDespliegue" onclick="despliegue('aix')">AIX</div>
        <div class="botonDespliegue" onclick="despliegue('linux')">Linux</div>
    </div>
    
    <br>
    <div style="text-align:center;">
        <div class="botonDespliegue" onclick="despliegue('incremento solaris')">Incremento Solaris</div>
        <div class="botonDespliegue" onclick="despliegue('incremento aix')">Incremento AIX</div>
        <div class="botonDespliegue" onclick="despliegue('incremento linux')">Incremento Linux</div>
    </div>
    <br>

    <div id="UNIX-Oracle" name="UNIX-Oracle">
        <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, favor realizar los siguientes pasos para cada Servidor <span id="servidor"><?= $servidor ?></span>. 
            La herramienta debe ser ejecutada en un servidor dentro de la misma red de todos los equipos a levantarse ya que mediante este se conectar&aacute;
            a todas las maquinas que se encuentren enlazadas mediante ssh y obtendr&aacute; los productos, procesos y archivos.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar en una carpeta dentro del Controlador de Dominio el archivo <a class="link1" href="<?= $GLOBALS['domain_root'] ?>/LATool_Unix.zip">LATool_Unix.zip</a></p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Copiar y Descomprimir la herramienta dentro de cualquier carpeta que tenga acceso de administrador. 
            Este directorio contiene dos archivos <span style="font-weight:bold;">Find_LA.sh</span> y <span style="font-weight:bold;">hosts</span>, además de dos carpetas scripts (OracleInstallations_LA.sh) y reports (vacio).</p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Convertir a los archivos <span style="font-weight:bold;">Find_LA.sh y OracleInstallations_LA.sh</span>
            en ejecutables mediante el comando:</p>
        <p>chmod 755 Find_LA.sh</p> 
        <p>chmod 755 OracleInstallations_LA.sh</p><br>

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Llenar el archivo hosts el cual cuenta con <span style="font-weight:bold;">localhost root</span>, como configuraci&oacute;n 
            b&aacute;sica y se deber&aacute; llenar el archivo con el IP del servidor y el usuario administrador separados con un espacio o tabulaci&oacute;n para que el 
            script principal pueda leer los equipos que ser&aacute;n escaneados. Cada equipo y usuario deber&aacute; especificarse en una l&iacute;nea distinta del archivo.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Ejecutar el script <span style="font-weight:bold;">Find_LA.sh</span> con el comando:</p>
        <p>./ Find_LA.sh hosts</p>
        <p>ya que este leer&aacute; los hosts especificados previamente y acceder&aacute; mediante ssh con la cuenta especificada (debe ser administrador) con el fin de realizar 
            una b&uacute;squeda de cualquier producto Oracle instalado.</p><br>

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>El script ir&aacute; copiando los resultados en la carpeta /reports. Una vez finalizado el escaneo favor ingresar a la carpeta y generar un .rar seleccionando todos los archivos dentro de la misma.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Favor subir el archivo .rar a continuaci&oacute;n.</p><br>
        <br>

        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">Archivo</legend>
            
            <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="despliegue1.php">
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionDespliegue" type="hidden" value="<?= $pestana ?>">
                <input type="hidden" id="incremento" name="incremento" value="<?= $incremento ?>">
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo" accept='.rar'>
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <?php if($exito=='1'){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <div id="mensAdvCompras" class="text-center" style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ."<br>". $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }else{ ?>

            <?php }
            if($error==-1){ ?>
                <div class="error_archivo"><?= $general->getMensajeNoDescRar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeNoDescRar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error=='1'){ ?>
                <div class="error_archivo"><?= $general->getMensajeRar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error=='2'){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function() {
                    });
                </script>
            <?php }
            if($error > 5){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'});
                </script>
            <?php } ?>
        </fieldset>
    </div>
    
    <br />

    <div style="float:right;"><div class="botones_m2" id="boton1" onClick="location.href='compras.php';">Compras</div></div>
</div>


<script>
    $(document).ready(function(){
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
</script>