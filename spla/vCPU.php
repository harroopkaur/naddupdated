<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/spla/procesos/vCPU.php");
require_once($GLOBALS["app_root"] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1   = 10;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="divMenuContenido">
            <?php
            $menuSPLA = 2;
            include_once($GLOBALS['app_root'] . "/spla/plantillas/menu.php");            
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php include_once($GLOBALS["app_root"] . "/spla/plantillas/vCPU.php"); ?>
            </div>
        </div>
    </div>
</section>
<?php
include_once($GLOBALS['app_root'] . "/plantillas/pie.php");