<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_spla_vCPU.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $virtualMachine = new clase_SPLA_vCPU();
    $clientes = "";
    if(isset($_POST["clientes"])){
        $clientes = $general->get_escape($_POST["clientes"]);
    }

    $producto = "";
    if(isset($_POST["producto"])){
        $producto = $general->get_escape($_POST["producto"]);
    }
    
    $query = $virtualMachine->appValidationFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], $clientes, $producto, 1, 15000);
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                ->setTitle("Apps List");


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'VM Name')
                ->setCellValue('B1', 'Customer')
                ->setCellValue('C1', 'VM Date')
                ->setCellValue('D1', 'Installed Date')
                ->setCellValue('E1', 'Prod')
                ->setCellValue('F1', 'Edition')
                ->setCellValue('G1', 'Sockets')
                ->setCellValue('H1', 'Cores')
                ->setCellValue('I1', 'CPUs')
                ->setCellValue('J1', 'Qty')
                ->setCellValue('K1', 'SKU')
                ->setCellValue('L1', 'Type');

    
    $i = 2;
    foreach($query as $row){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row["VM"])
                    ->setCellValue('B' . $i, $row["nombreCliente"])
                    ->setCellValue('C' . $i, $row["VMDate"])
                    ->setCellValue('D' . $i, $row["hostDate"])
                    ->setCellValue('E' . $i, $row["familia"])
                    ->setCellValue('F' . $i, $row["edicion"])
                    ->setCellValue('G' . $i, $row["sockets"])
                    ->setCellValue('H' . $i, $row["cores"])
                    ->setCellValue('I' . $i, $row["cpu"])
                    ->setCellValue('J' . $i, $row["Qty"])
                    ->setCellValue('K' . $i, $row["SKU"])
                    ->setCellValue('L' . $i, $row["type"]);
        $i++;
    }

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="appValidation.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}