<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_spla_cliente.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $clientesSPLA = new clase_SPLA_cliente();
    $producto = "";
    if(isset($_GET["producto"])){
        $producto = $general->get_escape($_GET["producto"]);
    }

    $cliente = "";
    if(isset($_GET["cliente"])){
        $cliente = $general->get_escape($_GET["cliente"]);
    }

    $contrato = "";
    if(isset($_GET["contrato"])){
        $contrato = $general->get_escape($_GET["contrato"]);
    }
    
    $query = $clientesSPLA->listar_todoFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], $producto, $cliente, $contrato, 0, 15000);
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                ->setTitle("Listado Clientes");


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Nombre Cliente')
                ->setCellValue('B1', 'Contrato')
                ->setCellValue('C1', 'Inicio Contrato')
                ->setCellValue('D1', 'Fin Contrato')
                ->setCellValue('E1', 'Type')
                ->setCellValue('F1', 'VM')
                ->setCellValue('G1', 'Licencias Win')
                ->setCellValue('H1', 'Licencias SQL');

    
    $i = 2;
    foreach($query as $row){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row["nombreCliente"])
                    ->setCellValue('B' . $i, $row["contrato"])
                    ->setCellValue('C' . $i, $row["inicioContrato"])
                    ->setCellValue('D' . $i, $row["finContrato"])
                    ->setCellValue('E' . $i, $row["type"])
                    ->setCellValue('F' . $i, $row["VM"])
                    ->setCellValue('G' . $i, $row["licenciasWin"])
                    ->setCellValue('H' . $i, $row["licenciasSQL"]);
        $i++;
    }

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="listadoClientes.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}