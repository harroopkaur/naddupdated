<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
            die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"
    
    $detalles = new Detalles_SPLA();
    $vert = 0;
    if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
        $vert = $_POST["vert"];
    }
    
    if($vert == 0){
        $total_1LAc       = 0;
        $total_1InacLAc   = 0;
        $total_2DVc       = 0;
        $total_1LAs       = 0;
        $total_1InacLAs   = 0;
        $total_2DVs       = 0;
        $reconciliacion1c = 0;
        $reconciliacion1s = 0;
        
        $tabla = $detalles->listar_todo($_SESSION['client_id'], $_SESSION['client_empleado']);
        if ($tabla) {
            foreach ($tabla as $reg_equipos) {

                if ($reg_equipos["tipo"] == 1) {//cliente
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $total_1LAc++;
                    }

                    if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_2DVc++;
                    }
                    else{
                        $total_1InacLAc++;
                    }
                } else {//server
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $total_1LAs++;
                    }

                    if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_2DVs++;
                    }
                    else{
                        $total_1InacLAs++;
                    }
                }
            }
            $reconciliacion1c = $total_2DVc - $total_1LAc;
            $reconciliacion1s = $total_2DVs - $total_1LAs;
        }
    } else if($vert == 1){ 
        $total_1 = 0;
        $total_2 = 0;
        $total_3 = 0;
        $total_4 = 0;
        $total_5 = 0;
        $total_6 = 0;
        $tclient = 0;
        $tserver = 0;
        $activosc = 0;
        $activoss = 0;
        
        $tabla = $detalles->listar_todo($_SESSION['client_id'], $_SESSION['client_empleado']);
        if ($tabla) {
            foreach ($tabla as $reg_equipos) {

                if ($reg_equipos["tipo"] == 1) {
                    $tclient++;

                    if ($reg_equipos["rango"] == 1) {
                        $total_1++;
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activosc++;
                        }
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_2++;
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activosc++;
                        }
                    } else {
                        $total_3++;
                    }
                } else {//server
                    $tserver++;

                    if ($reg_equipos["rango"] == 1) {
                        $total_4++;
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activoss++;
                        }
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_5++;
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activoss++;
                        }
                    } else {
                        $total_6++;
                    }
                }
            }
        }
    } else if($vert == 2){
        $balance2= new Balance_SPLA();
        
        $familia = "";
        if(isset($_POST["familiaExcel"])){
            $familia = $general->get_escape($_POST["familiaExcel"]);
        }
        
        $edicion = "";
        if(isset($_POST["edicionExcel"])){
            $edicion = $general->get_escape($_POST["edicionExcel"]);
        }

        $clientTotalOfficeCompras  = 0;
        $clientTotalOfficeInstal   = 0;	
        $clientOfficeNeto          = 0;

        if($familia == "Windows"){
            if($edicion != "Otros"){
                $listar_Of = $balance2->listar_todo_familias6($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion);
            }
            else{
                $listar_Of = $balance2->listar_todo_familias8($_SESSION['client_id'], $_SESSION['client_empleado'], $familia);
            }
        }
        else if($familia == "Others"){
            $listar_Of = $balance2->listar_todo_familias7($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion);
        }
        else{
            if($edicion != "Otros"){
                $listar_Of = $balance2->listar_todo_familias1($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion);
            }
            else{
                $listar_Of = $balance2->listar_todo_familias9($_SESSION['client_id'], $_SESSION['client_empleado'], $familia);
            }
        }
    }
    else if($vert == 3){
        $optimizacion = new Detalles_SPLA();
        $opcion = $_POST["opcion"];

        if($opcion == "cliente"){
            $listarOptimizacion = $optimizacion->listar_optimizacion1($_SESSION['client_id'], $_SESSION['client_empleado']);
        }
        else if($opcion == "servidor"){
            $listarOptimizacion = $optimizacion->listar_optimizacion1Servidor($_SESSION['client_id'], $_SESSION['client_empleado']);
        }
    }
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("");


    // Add some data
    if($vert == 0){
        
        $nombre = "excelSPLAAlcance.xlsx";
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Tipo')
                    ->setCellValue('B1', 'Activo AD')
                    ->setCellValue('C1', 'Inactivo AD')
                    ->setCellValue('D1', 'Reconciliación')
                    ->setCellValue('E1', 'LA Tool')
                    ->setCellValue('F1', 'Cobertura');
        
        $t1 = $total_2DVc - $total_1LAc;
        $pot11 = 0;
        if($total_2DVc != 0){
            $pot11 = ($total_1LAc / $total_2DVc) * 100;
        }
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', 'Clientes')
                    ->setCellValue('B2', $total_2DVc)
                    ->setCellValue('C2', $total_1InacLAc)
                    ->setCellValue('D2', $t1)
                    ->setCellValue('E2', $total_1LAc)
                    ->setCellValue('F2', round($pot11));
        
        $t2 = $total_2DVs - $total_1LAs;
        $pot12 = 0;
        if($total_2DVs != 0){
            $pot12 = ($total_1LAs / $total_2DVs) * 100;
        }
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A3', 'Servidores')
                    ->setCellValue('B3', $total_2DVs)
                    ->setCellValue('C3', $total_1InacLAs)
                    ->setCellValue('D3', $t2)
                    ->setCellValue('E3', $total_1LAs)
                    ->setCellValue('F3', round($pot12)); 
        
        $to1 = $total_1LAc + $total_1LAs;
        $to2 = $total_2DVc + $total_2DVs;
        $pot13 = 0;
        
        if($to2 != 0){
            $pot13 = ($to1 / $to2) * 100;
        }
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A4', 'Total')
                    ->setCellValue('B4', $total_2DVc + $total_2DVs)
                    ->setCellValue('C4', $total_1InacLAc + $total_1InacLAs)
                    ->setCellValue('D4', $reconciliacion1c + $reconciliacion1s)
                    ->setCellValue('E4', $total_1LAc + $total_1LAs)
                    ->setCellValue('F4', round($pot13)); 
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', '')
                    ->setCellValue('B6', 'Nombre Equipo')
                    ->setCellValue('C6', 'Tipo')
                    ->setCellValue('D6', 'Activo AD')
                    ->setCellValue('E6', 'LA Tool'); 
        
        $i = 7;
        $j = 1;
        foreach ($tabla as $reg_equipos2) {
            $tipo = 'Servidor';
            if ($reg_equipos2["tipo"] == 1) {
                $tipo = 'Cliente';
            } 
            
            $rango = 'No';
            if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                $rango = 'Si';
            }
        
            $errors = 'No';
            if ($reg_equipos2["errors"] == 'Ninguno') {
                $errors = 'Si';
            } 
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $j)
                        ->setCellValue('B' . $i, $reg_equipos2["equipo"])
                        ->setCellValue('C' . $i, $tipo)
                        ->setCellValue('D' . $i, $rango)
                        ->setCellValue('E' . $i, $errors); 
            $i++;
            $j++;
        }
    } else if ($vert == 1){
        $nombre = "excelSPLAUsabilidad.xlsx";
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Usabilidad')
                    ->setCellValue('B1', 'Clientes')
                    ->setCellValue('C1', '%')
                    ->setCellValue('D1', 'Servidores')
                    ->setCellValue('E1', '%');
                    //->setCellValue('F1', 'Activo');
        
        $porct1 = 0;
        if($tclient > 0){
            $porct1 = ($total_1 / $tclient) * 100;
        }
        
        $porct11 = 0;
        if($tserver > 0){
            $porct11 = ($total_4 / $tserver) * 100;
        }
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', 'En Uso')
                    ->setCellValue('B2', $total_1)
                    ->setCellValue('C2', round($porct1))
                    ->setCellValue('D2', $total_4)
                    ->setCellValue('E2', round($porct11));
                    //->setCellValue('F2', 'Si');
        
        $porct2 = 0;
        if($tclient > 0){
            $porct2 = ($total_2 / $tclient) * 100;
        }
        
        $porct22 = 0;
        if($tserver > 0){
            $porct22 = ($total_5 / $tserver) * 100;
        }
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A3', 'Probablemente en Uso')
                    ->setCellValue('B3', $total_2)
                    ->setCellValue('C3', round($porct2))
                    ->setCellValue('D3', $total_5)
                    ->setCellValue('E3', round($porct22));
                    //->setCellValue('F3', 'Si');
        
        $porct3 = 0;
        if($tclient > 0){
            $porct3 = ($total_3 / $tclient) * 100;
        }
        
        $porct33 = 0;
        if($tserver > 0){
            $porct33 = ($total_6 / $tserver) * 100;
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A4', 'Obsoleto')
                    ->setCellValue('B4', $total_3)
                    ->setCellValue('C4', round($porct3))
                    ->setCellValue('D4', $total_6)
                    ->setCellValue('E4', round($porct33));
                    //->setCellValue('F4', 'No');    
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A5', 'Gran total')
                    ->setCellValue('B5', $tclient)
                    ->setCellValue('C5', '')
                    ->setCellValue('D5', $tserver)
                    ->setCellValue('E5', '');
                    //->setCellValue('F5', '');       
           
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', '')
                    ->setCellValue('B7', 'Nombre Equipo')
                    ->setCellValue('C7', 'Tipo')
                    ->setCellValue('D7', 'Usabilidad')
                    ->setCellValue('E7', 'Escaneado');
        
        $i = 8; //13;
        $j = 1;
        foreach ($tabla as $reg_equipos2) {
            $tipo = 'Servidor';
            if ($reg_equipos2["tipo"] == 1) {
                $tipo = 'Cliente';
            }
            
            $uso = '';
            if ($reg_equipos2["rango"] == 1) {
                $uso = 'En Uso';
            } else if ($reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3){
                $uso = 'Probablemente en uso';
            } else {
                $uso = 'Obsoleto';
            }
            
            $escaneado = 'No';
            if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                $escaneado = 'Si';
            }
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $j)
                    ->setCellValue('B' . $i, $reg_equipos2["equipo"])
                    ->setCellValue('C' . $i, $tipo)
                    ->setCellValue('D' . $i, $uso)
                    ->setCellValue('E' . $i, $escaneado);
            
            $i++;
            $j++;
        }   
    } else if($vert == 2){
        $nombre = "excelSPLAResumen.xlsx";
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Producto')
                    ->setCellValue('B1', 'Edición')
                    ->setCellValue('C1', 'Versión')
                    ->setCellValue('D1', 'Instalaciones')
                    ->setCellValue('E1', 'Compras')
                    ->setCellValue('F1', 'Neto')
                    ->setCellValue('G1', 'Neto ($)');

        $i = 2;
        foreach ($listar_Of as $reg_equipos) {
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $reg_equipos["familia"])
                        ->setCellValue('B' . $i, $reg_equipos["office"])
                        ->setCellValue('C' . $i, $reg_equipos["version"])
                        ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                        ->setCellValue('E' . $i, $reg_equipos["compra"])
                        ->setCellValue('F' . $i, $reg_equipos["balance"])
                        ->setCellValue('G' . $i, $reg_equipos["balancec"]);
            $i++;
        }
    }
    else if($vert == 3){    
        if($opcion == "cliente"){
            $nombre = "excelSPLAOptimizacionCliente.xlsx";
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', '')
                    ->setCellValue('B1', 'Equipo')
                    ->setCellValue('C1', 'Office')
                    ->setCellValue('D1', 'Visio')
                    ->setCellValue('E1', 'Project')
                    ->setCellValue('F1', 'Visual')
                    ->setCellValue('G1', 'Usabilidad')
                    ->setCellValue('H1', 'Observación');
        }
        else if($opcion == "servidor"){
            $nombre = "excelSPLAOptimizacionServidor.xlsx";
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', '')
                    ->setCellValue('B1', 'Equipo')
                    ->setCellValue('C1', 'Windows Server')
                    ->setCellValue('D1', 'SQL Server')
                    ->setCellValue('G1', 'Usabilidad')
                    ->setCellValue('H1', 'Observación');
        }


        $i = 2;
        $j = 1;

        if($opcion == "cliente"){
            foreach ($listarOptimizacion as $reg_equipos) {
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $i, $j)
                            ->setCellValue('B' . $i, $reg_equipos["equipo"])
                            ->setCellValue('C' . $i, $reg_equipos["office"])
                            ->setCellValue('D' . $i, $reg_equipos["visio"])
                            ->setCellValue('E' . $i, $reg_equipos["project"])
                            ->setCellValue('F' . $i, $reg_equipos["visual"])
                            ->setCellValue('G' . $i, $reg_equipos["usabilidad"])
                            ->setCellValue('H' . $i, $reg_equipos["duplicado"]);
                $i++;
                $j++;
            }
        }
        else if($opcion == "servidor"){
            foreach ($listarOptimizacion as $reg_equipos) {
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $i, $j)
                            ->setCellValue('B' . $i, $reg_equipos["equipo"])
                            ->setCellValue('C' . $i, $reg_equipos["os"])
                            ->setCellValue('D' . $i, $reg_equipos["SQLServer"])
                            ->setCellValue('G' . $i, $reg_equipos["usabilidad"])
                            ->setCellValue('H' . $i, $reg_equipos["duplicado"]);
                $i++;
                $j++;
            }
        }

    }


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesion($mensaje);
}