<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_SPLA_historico.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $virtualMachine = new clase_SPLA_historico();

    $VM = "";
    if(isset($_GET["VM"])){
        $VM = $general->get_escape($_GET["VM"]);
    }

    $fechaInicio = date("Y") . "-01-01";
    if(isset($_GET["fechaInicio"]) && $_GET["fechaInicio"] != ""){
        if($general->validarFecha($_POST["fechaInicio"], "/", "mm/dd/aaaa")){
            $fechaInicio = $general->reordenarFecha($_GET["fechaInicio"], "/", "-", $formato = "mm/dd/YYYY");
        }
    }

    $fechaFin = date("Y-m-d");
    if(isset($_GET["fechaFin"]) && $_GET["fechaFin"] != ""){
        if($general->validarFecha($_GET["fechaFin"], "/", "mm/dd/aaaa")){
            $fechaFin = $general->reordenarFecha($_GET["fechaFin"], "/", "-", $formato = "mm/dd/YYYY");
        }
    }
    
    $query = $virtualMachine->reportHistoricalVMPaginado($_SESSION["client_id"], $VM, $fechaInicio, $fechaFin, 1, 15000);

    $mesInicio = date("n", strtotime($fechaInicio)); 
    $mesFin = date("n", strtotime($fechaFin));
    $yearInicio = date("Y", strtotime($fechaInicio)); 
    $yearFin = date("Y", strtotime($fechaFin));

    if($yearFin > $yearInicio){
        $mesFin += 12;
    }

    $mesFin++;
    
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                ->setTitle("Historical Reporting VMs");    

    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Host Name')
                ->setCellValue('B1', 'Host Date')
                ->setCellValue('C1', 'VM Name')
                ->setCellValue('D1', 'VM Date');
            
    
    $a = 0;
    for($index = $mesInicio; $index < $mesFin; $index++){
        $valor = $general->completarCeroNumeros($index);
        $campo = $valor . '/'.  $yearInicio;
        if($index > 12){
            $campo = $valor . '/'.  $yearFin;
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($virtualMachine->cambiarIndiceLetra($a, 1), $campo);
    
        $a++;
    }
    
    $i = 2;
    $totFechas = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    foreach($query as $row){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row["host"])
                    ->setCellValue('B' . $i, $row["hostDate"])
                    ->setCellValue('C' . $i, $row["VM"])
                    ->setCellValue('D' . $i, $row["VMDate"]);
        
        $a = 0;
        for($index = $mesInicio; $index < $mesFin; $index++){
            $valor = $general->completarCeroNumeros($index);
            $campo = $valor . '/'.  $yearInicio;
            if($index > 12){
                $campo = $valor . '/'.  $yearFin;
            }
            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($virtualMachine->cambiarIndiceLetra($a, $i), $row[$campo]);
            $totFechas[$index] += intval($row[$campo]); 
            $a++;
        }    
        $i++;
    }
    
    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, 'Total')
                    ->setCellValue('B' . $i, '')
                    ->setCellValue('C' . $i, '')
                    ->setCellValue('D' . $i, '');
    
    $a = 0;
    for($index = $mesInicio; $index < $mesFin; $index++){
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($virtualMachine->cambiarIndiceLetra($a, $i), $totFechas[$index]); 
        $a++;
    }   

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="historicalReportingVMs.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}