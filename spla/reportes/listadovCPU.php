<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_spla_vCPU.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $virtualMachine = new clase_SPLA_vCPU();
    $producto = "";
    if(isset($_GET["producto"])){
        $producto = $general->get_escape($_GET["producto"]);
    }

    $cluster = "";
    if(isset($_GET["cluster"])){
        $cluster = $general->get_escape($_GET["cluster"]);
    }

    $host = "";
    if(isset($_GET["host"])){
        $host = $general->get_escape($_GET["host"]);
    }
    
    $query = $virtualMachine->listar_todoFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], $producto, $cluster, $host, 0, 15000);
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                ->setTitle("Listado vCPU");


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'DC')
                ->setCellValue('B1', 'Cluster')
                ->setCellValue('C1', 'Host')
                ->setCellValue('D1', 'VM')
                ->setCellValue('E1', 'Edición')
                ->setCellValue('F1', 'Sockets')
                ->setCellValue('G1', 'Coresp/s')
                ->setCellValue('H1', 'CPUs');

    
    $i = 2;
    foreach($query as $row){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row["DC"])
                    ->setCellValue('B' . $i, $row["cluster"])
                    ->setCellValue('C' . $i, $row["host"])
                    ->setCellValue('D' . $i, $row["VM"])
                    ->setCellValue('E' . $i, $row["edicion"])
                    ->setCellValue('F' . $i, $row["sockets"])
                    ->setCellValue('G' . $i, $row["cores"])
                    ->setCellValue('H' . $i, $row["cpu"]);
        $i++;
    }

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="listadovCPU.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}