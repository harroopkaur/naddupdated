<?php
require_once("../configuracion/inicio.php");
require_once("../clases/clase_general.php");
require_once("../clases/clase_balance_SPLA.php");

//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>'false', 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = 'true';
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $balance2 = new Balance_SPLA($conn);
            
            $familia = "";
            if(isset($_POST["familia"])){
                $familia = $general->get_escape($_POST["familia"]);
            }
            
            $edicion = "";
            if(isset($_POST["edicion"])){
                $edicion = $general->get_escape($_POST["edicion"]);
            }

            $clientTotalOfficeCompras = 0;
            $clientTotalOfficeInstal = 0;
            $clientOfficeNeto = 0;

            $titulo = "";

            if ($familia != "Otros") {
                $titulo = $familia;
            } else {
                $titulo = $edicion;
            }

            if ($familia == "Windows") {
                if ($edicion != "Otros") {
                    $listar_Of = $balance2->listar_todo_familias6Sam($_SESSION['client_id'], $familia, $edicion);
                }
                else{
                    $listar_Of = $balance2->listar_todo_familias10Sam($_SESSION['client_id'], $familia);
                } 
            } else if ($familia == "Others") {
                $listar_Of = $balance2->listar_todo_familias7Sam($_SESSION['client_id'], $edicion);
            } else {
                if ($edicion != "Otros") {
                    $listar_Of = $balance2->listar_todo_familias1Sam($_SESSION['client_id'], $familia, $edicion);
                } else {
                $listar_Of = $balance2->listar_todo_familias9Sam($_SESSION['client_id'], $familia);
                }
            }

            foreach ($listar_Of as $reg_equipos) {
                $clientTotalOfficeCompras += $reg_equipos["compra"];
                $clientTotalOfficeInstal += $reg_equipos["instalaciones"];
                $clientOfficeNeto = $clientTotalOfficeCompras - $clientTotalOfficeInstal;
            }

            $tabla = "";
            foreach ($listar_Of as $reg_equipos) {
                $tabla .= '<tr>
                                <td>' . $reg_equipos["familia"] . '</td>
                                <td>' . $reg_equipos["office"] . '</td>
                                <td>' . $reg_equipos["version"] . '</td>
                                <td align="center">' . $reg_equipos["instalaciones"] . '</td>
                                <td align="center">' . round($reg_equipos["compra"], 0) . '</td>
                                <td align="center">' . round($reg_equipos["balance"], 0) . '</td>
                                <td align="center">' . round($reg_equipos["balancec"], 0) . '</td>
                        </tr>';
            }

            $ediciones = '<option value="" ';
            if ($edicion == "")
                $ediciones .= "selected='selected'";
            $ediciones .= '>Seleccione..</option>';
            if ($familia == "Windows") {
                $ediciones .= '<option value="Enterprise" ';
                if ($edicion == "Enterprise")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Enterprise</option>
                        <option value="Professional" ';
                if ($edicion == "Professional")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Professional</option>
                        <option value="Otros" ';
                if ($edicion == "Otros")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Otros</option>';
            }
            else if ($familia == "Office" || $familia == "Project" || $familia == "Visio") {
                $ediciones .= '<option value="Standard" ';
                if ($edicion == "Standard")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Standard</option>
                        <option value="Professional" ';
                if ($edicion == "Professional")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Professional</option>
                        <option value="Otros" ';
                if ($edicion == "Otros")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Otros</option>';
            }
            else if ($familia == "Windows Server" || $familia == "SQL") {
                $ediciones .= '<option value="Standard" ';
                if ($edicion == "Standard")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Standard</option>
                        <option value="Datacenter" ';
                if ($edicion == "Datacenter")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Datacenter</option>
                        <option value="Enterprise" ';
                if ($edicion == "Enterprise")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Enterprise</option>
                        <option value="Otros" ';
                if ($edicion == "Otros")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Otros</option>';
            }
            else {
                $ediciones .= '<option value="Visual" ';
                if ($edicion == "Visual")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Visual Studio</option>
                        <option value="Exchange" ';
                if ($edicion == "Exchange")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Exchange Server</option>
                        <option value="Sharepoint" ';
                if ($edicion == "Sharepoint")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Sharepoint Server</option>
                        <option value="Skype" ';
                if ($edicion == "Skype")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>Skype for Business</option>
                        <option value="System Center" ';
                if ($edicion == "System Center")
                    $ediciones .= "selected='selected'";
                $ediciones .= '>System Center</option>';
            }

            $array = array(0 => array('sesion'=>$sesion, 'mensaje'=>'', 'titulo' => $titulo, 'compra' => $clientTotalOfficeCompras, 
            'instalacion' => $clientTotalOfficeInstal, 'tabla' => $tabla, 'edicion' => $ediciones, 'neto' => $clientOfficeNeto, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
?>