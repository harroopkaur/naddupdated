<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_cliente.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_SKU.php");

$virtualMachine = new clase_SPLA_vCPU();
$clienteSPLA = new clase_SPLA_cliente();
$SKU = new clase_SPLA_SKU();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $customers = $clienteSPLA->clientes($_SESSION["client_id"], $_SESSION["client_empleado"]);   
            $listadoSKU = $SKU->SKU($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $clientes = "";
            if(isset($_POST["clientes"])){
                $clientes = $general->get_escape($_POST["clientes"]);
            }

            $producto = "";
            if(isset($_POST["producto"])){
                $producto = $general->get_escape($_POST["producto"]);
            }

            $pagina = 0;
            if(isset($_POST["pagina"]) && (filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false || $_POST["pagina"] == "ultima")){
                $pagina = $_POST["pagina"];
            }

            $limite = 0;
            if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                $limite     = $_POST["limite"];
            }

            $div = '';
            $i = 0;
            $total = $virtualMachine->appValidationTotal($_SESSION["client_id"], $_SESSION["client_empleado"], $clientes, $producto);
            $query = $virtualMachine->appValidationFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], $clientes, $producto, $pagina, $limite);
            foreach($query as $row){
            $div .= '<tr>
                        <input type="hidden" id="id' . $i . '" name="id[]" value="' . $row["id"] . '">
                        <input type="hidden" id="tipo' . $i . '" name="tipo[]" value="' . $row["tipo"] . '">
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["VM"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">
                            <select id="customers' . $i . '" name="customers[]" disabled>
                            <option value="">--Select--</option>';
                            foreach($customers as $row1){
                                $div .= '<option value="' . $row1["nombreCliente"] . '"';
                                if($row1["nombreCliente"] == $row["nombreCliente"]){ $div .= "selected='selected'"; } 
                                $div .= '>' . $row1["nombreCliente"] . '</option>';
                            }
                        $div .= '</select>
                        </td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["VMDate"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["hostDate"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["familia"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["edicion"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["sockets"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["cores"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["cpu"] . '</td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '"><input type="text" class="text-right" id="cantidad' . $i . '" name="cantidad[]" value="' . $row["Qty"] . '" maxlength="4" style="width:80px;" onfocus="$(\'#cantidad' . $i . '\').val(\'\')" disabled></td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">
                            <select id="SKU' . $i . '" name="SKU[]" disabled>
                            <option value="">--Select--</option>';
                            foreach($listadoSKU as $row1){
                                $div .= '<option value="' . $row1["SKU"] . '"';
                                if($row1["SKU"] == $row["SKU"]){ $div .= "selected='selected'"; } 
                                $div .= '>' . $row1["SKU"] . '</option>';
                            }
                            $div .= '</select>
                        </td>
                        <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '"><input type="text" class="text-right" id="type' . $i . '" name="type[]" value="' . $row["type"] . '" maxlength="70" style="width:100px;" onfocus="$(\'#type' . $i . '\').val(\'\')" disabled></td>
                        <script>
                            $("#cantidad' . $i . '").numeric(false);
                        </script>
                    </tr>';
                    $i++;
            }

            $ultimo  = "no";
            $primero = "no";

            $limiteNuevo = $limite * $pagina;
            if($limiteNuevo > $total || $pagina == "ultima"){
                    $limiteNuevo = $total;
                    $ultimo      = "si";
                    $pagina      = $virtualMachine->ultimaPaginaApps($_SESSION["client_id"], $_SESSION["client_empleado"], $clientes, $producto, $limite);
            }

            if($pagina == 1){
                    $primero = "si";
            }

            $sinPaginacion = "no";
            if($limite > $total){
                    $sinPaginacion = "si";
            }

            $nuevoInicio = (($pagina - 1) * $limite) + 1;
            if($nuevoInicio < 0){
                $nuevoInicio = 0;
            }
            $paginacion = $nuevoInicio .'-' . $limiteNuevo . ' of ' . $total;

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
            'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
            'sinPaginacion'=>$sinPaginacion, 'ultimaPagina'=>$pagina, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);