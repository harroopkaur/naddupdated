<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_spla_SKU.php");

$SKU = new clase_SPLA_SKU();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $producto = "";
            if(isset($_POST["producto"])){
                $producto = $general->get_escape($_POST["producto"]);
            }
            
            $familia = "";
            if(isset($_POST["familia"])){
                $familia = $general->get_escape($_POST["familia"]);
            }

            $pagina = 0;
            if(isset($_POST["pagina"]) && (filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false || $_POST["pagina"] == "ultima")){
                $pagina = $_POST["pagina"];
            }
            
            $limite = 0;
            if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                $limite     = $_POST["limite"];
            }

            $div = '';
            $i = 0;
            $total = $SKU->totalRegistrosSKU($_SESSION["client_id"], $_SESSION["client_empleado"], $producto, $familia);
            $query = $SKU->listar_todoFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], $producto, $familia, $pagina, $limite);
            foreach($query as $row){
            $div .= '<tr>
                    <input type="hidden" id="id' . $i . '" name="id[]" value="' . $row["id"] . '">
                    <td style="border: 1px solid;">' . $row["SKU"] . '</td>
                    <td style="border: 1px solid;">' . $row["description"] . '</td>
                    <td style="border: 1px solid;">' . $row["product"] . '</td>
                    <td style="border: 1px solid;">' . $row["partNumber"] . '</td>
                    <td style="border: 1px solid;" class="text-right">' . $row["price"] . '</td>
                </tr>';
                $i++;
            }

            $ultimo  = "no";
            $primero = "no";

            $limiteNuevo = $limite * $pagina;
            if($limiteNuevo > $total || $pagina == "ultima"){
                    $limiteNuevo = $total;
                    $ultimo      = "si";
                    $pagina      = $SKU->ultimaPaginaSKU($_SESSION["client_id"], $_SESSION["client_empleado"], $producto, $familia, $limite);
            }

            if($pagina == 1){
                    $primero = "si";
            }

            $sinPaginacion = "no";
            if($limite > $total){
                    $sinPaginacion = "si";
            }

            $nuevoInicio = (($pagina - 1) * $limite) + 1;
            if($nuevoInicio < 0){
                $nuevoInicio = 0;
            }
            $paginacion = $nuevoInicio .'-' . $limiteNuevo . ' of ' . $total;

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
            'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
            'sinPaginacion'=>$sinPaginacion, 'ultimaPagina'=>$pagina, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);