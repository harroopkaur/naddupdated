<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_SPLA.php");

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

$array = array(0=>array('resultado'=>false));
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $vert = 0;
            if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
                $vert = $_POST['vert']; 
            }
            
            $opcion = "";
            if(isset($_POST['opcion'])){
                $opcion = $general->get_escape($_POST['opcion']);
            }
            
            $ordenar = "";
            if(isset($_POST["ordenar"])){
                $ordenar = $general->get_escape($_POST["ordenar"]);
            }
            
            $direccion = "";
            if(isset($_POST["direccion"])){
                $direccion = $general->get_escape($_POST["direccion"]); 
            }


            $detalles1 = new Detalles_SPLA();
            $detalles2 = new Detalles_SPLA();
            $resumen   = new Resumen_SPLA();
            $tabla = '';
            if($vert == 0 || $vert == 1 || $vert == 2 || $vert == 3 || $vert == 8 || $vert == 82 || $vert == 83 || $vert == 84){
                $tabla .= '<table class="tablap" id="tablaMicrosoftDetalle">
                            <thead>
                                <tr style="background:#333; color:#fff;">
                                        <th valign="middle"><span>&nbsp;</span></th>
                                        <th valign="middle"><span>Nombre Equipo</span></th>
                                        <th valign="middle"><span>Tipo</span></th>
                                        <th valign="middle"><span>Sistema Operativo</span></th>
                                        <th valign="middle"><span>Activo AD</span></th>
                                        <th valign="middle"><span>LA Tool</span></th>
                                        <th valign="middle"><span>Usabilidad</span></th>
                                </tr>
                            </thead>
                            <tbody>';

                if($vert == 0 || $vert == 8){
                    $listar_equipos0 = $detalles2->listar_todog0Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], $ordenar, $direccion);
                    $i = 1;
                    foreach($listar_equipos0 as $reg_equipos0){
                        if($vert == 0){
                            $tipo = 1;
                            $tipoMaquina = 'Cliente';
                        }
                        else{
                            $tipo = 2;
                            $tipoMaquina = 'Servidor';
                        }
                        if($opcion == 1){
                            if(($reg_equipos0["rango"] == 1 || $reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3) && $reg_equipos0["tipo"] == $tipo){    
                                $tabla .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $reg_equipos0["equipo"] . '</td>
                                    <td>' . $tipoMaquina . '</td>
                                    <td>' . $reg_equipos0["os"] . '</td>
                                    <td>' . $reg_equipos0["ActivoAD"] . '</td>
                                    <td>' . $reg_equipos0["LaTool"] . '</td>
                                    <td>' . $reg_equipos0["usabilidad"] . '</td>
                                    </tr>';
                                 $i++;
                            }
                        }
                        else{
                            if($reg_equipos0["rango"] != 1 && $reg_equipos0["rango"] != 2 && $reg_equipos0["rango"] != 3 && $reg_equipos0["tipo"] == $tipo){
                                $tabla .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $reg_equipos0["equipo"] . '</td>
                                    <td>' . $tipoMaquina . '</td>
                                    <td>' . $reg_equipos0["os"] . '</td>
                                    <td>' . $reg_equipos0["ActivoAD"] . '</td>
                                    <td>' . $reg_equipos0["LaTool"] . '</td>
                                    <td>' . $reg_equipos0["usabilidad"] . '</td>
                                </tr>';
                                 $i++;
                            }
                        }   

                    }
                }
                else{
                    if($vert == 1){
                        $listar_equipos0=$detalles1->listar_todog1Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise', $ordenar, $direccion);
                        $tipo = 1;
                        $tipoMaquina = 'Cliente';
                    }
                    if($vert == 2){
                        $listar_equipos0=$detalles1->listar_todog1Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'professional', $ordenar, $direccion);
                        $tipo = 1;
                        $tipoMaquina = 'Cliente';
                    }
                    if($vert == 3){
                        $listar_equipos0=$detalles1->listar_todog2Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise','professional', $ordenar, $direccion);
                        $tipo = 1;
                        $tipoMaquina = 'Cliente';
                    }
                    if($vert == 82){
                        $listar_equipos0=$detalles1->listar_todog1Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise', $ordenar, $direccion);
                        $tipo = 2;
                        $tipoMaquina = 'Servidor';
                    }
                    if($vert == 83){
                        $listar_equipos0=$detalles1->listar_todog1Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'professional', $ordenar, $direccion);
                        $tipo = 2;
                        $tipoMaquina = 'Servidor';
                    }
                    if($vert == 84){
                        $listar_equipos0=$detalles1->listar_todog2Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise','professional', $ordenar, $direccion);
                        $tipo = 2;
                        $tipoMaquina = 'Servidor';
                    }
                    $i = 1;
                    foreach($listar_equipos0 as $reg_equipos0){
                        if($reg_equipos0["tipo"] == $tipo){
                            $tabla .= '<tr>
                                <td>' . $i . '</td>
                                <td>' . $reg_equipos0["equipo"] . '</td>
                                <td>' . $tipoMaquina . '</td>
                                <td>' . $reg_equipos0["os"] . '</td>
                                <td>' . $reg_equipos0["ActivoAD"] . '</td>
                                <td>' . $reg_equipos0["LaTool"] . '</td>
                                <td>' . $reg_equipos0["usabilidad"] . '</td>
                            </tr>';
                            $i++;
                        }
                    }
                }
            }
            else{
                $tabla .= '<table class="tablap" id="tablaMicrosoftDetalle">
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th valign="middle"><span>&nbsp;</span></th>
                            <th valign="middle"><span>Equipo</span></th>
                            <th valign="middle"><span>Tipo</span></th>
                            <th valign="middle"><span>Familia</span></th>
                            <th valign="middle"><span>Edici&oacute;n</span></th>
                            <th valign="middle"><span>Versi&oacute;n</span></th>
                            <th valign="middle"><span>Instalaci&oacute;n</span></th>
                            <th valign="middle"><span>Usabilidad</span></th>
                            <th valign="middle"><span>Observaci&oacute;n</span></th>
                        </tr>
                    </thead>
                    <tbody>';
                if($vert == 5){
                     $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office','', $ordenar, $direccion);
                }
                if($vert == 51){
                     $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office','Standard', $ordenar, $direccion);
                }
                if($vert == 52){
                     $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office','Professional', $ordenar, $direccion);
                }
                if($vert == 53){
                    $lista_calculo = $resumen->listar_datos7Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office', 'Professional', 'Standard', $ordenar, $direccion);
                } 
                if($vert == 6){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','', $ordenar, $direccion);
                }
                if($vert == 61){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','Standard', $ordenar, $direccion);
                }
                if($vert == 62){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','Professional', $ordenar, $direccion);
                }
                if($vert == 63){
                    $lista_calculo = $resumen->listar_datos7Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','Professional', 'Standard', $ordenar, $direccion);
                }
                if($vert == 7){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'visio','', $ordenar, $direccion);
                }
                if($vert == 71){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'visio','Standard', $ordenar, $direccion);
                }
                if($vert == 72){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'visio','Professional', $ordenar, $direccion);
                }
                if($vert == 73){
                    $lista_calculo = $resumen->listar_datos7Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'visio','Professional','Standard', $ordenar, $direccion);
                }
                if($vert == 9){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','', $ordenar, $direccion);
                }
                if($vert == 91){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Standard', $ordenar, $direccion);
                }
                if($vert == 92){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Datacenter', $ordenar, $direccion);
                }
                if($vert == 93){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Enterprise', $ordenar, $direccion);
                }
                if($vert == 94){
                    $lista_calculo = $resumen->listar_datos8Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Standard','Datacenter','Enterprise', $ordenar, $direccion);
                }
                if($vert == 10){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Visual Studio','', $ordenar, $direccion);
                }
                if($vert == 11){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Exchange Server','', $ordenar, $direccion);
                }
                if($vert == 12){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Sharepoint Server','', $ordenar, $direccion);
                }
                if($vert == 13){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'Skype for Business','', $ordenar, $direccion);
                }
                if($vert == 14){
                    $lista_calculo = $resumen->listar_datos6Ordenar($_SESSION['client_id'], $_SESSION["client_empleado"], 'System Center','', $ordenar, $direccion);
                }

                $i = 1;
                foreach($lista_calculo as $reg_calculo){
                    $tabla .= '<tr>
                        <td>' . $i . '</td>
                        <td>' . $reg_calculo["equipo"] . '</td>
                        <td>' . $reg_calculo["tipo"] . '</td>
                        <td>' . $reg_calculo["familia"] . '</td>
                        <td>' . utf8_encode($reg_calculo["edicion"]) . '</td>
                        <td>' . $reg_calculo["version"] . '</td>
                        <td>' . $reg_calculo["fecha_instalacion"] . '</td>
                        <td>' . $reg_calculo["rango"] . '</td>
                        <td>';
                        if($resumen->duplicado($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_calculo["equipo"], $reg_calculo["familia"], $reg_calculo["edicion"], $reg_calculo["version"]) > 1){
                            $tabla .= " Duplicado";
                        }
                        $tabla .= '</td>
                    </tr>';
                    $i++;
                }
            }
            $tabla .= '</tbody>
                </table>
            <script> 
                $(document).ready(function(){
                    $("#tablaMicrosoftDetalle").tablesorter();
                });
            </script>';
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);