<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_spla_cliente.php");
$clientesSPLA = new clase_SPLA_cliente();

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $inicioContrato = "";
                if(isset($_POST["inicioContrato"]) && $_POST["inicioContrato"] != ""){
                    $inicioContrato = $general->get_escape($general->reordenarFecha($_POST["inicioContrato"], "/", null, "mm/dd/YYYY"));
                }
                
                if(isset($_POST["inicioContrato"]) && $_POST["inicioContrato"] == "00/00/0000"){
                    $inicioContrato = "0000-00-00";
                }
                
                $cliente = "";
                if(isset($_POST["cliente"])){
                    $cliente = $general->get_escape($_POST["cliente"]);
                }
                
                $contrato = "";
                if(isset($_POST["contrato"])){
                    $contrato = $general->get_escape($_POST["contrato"]);
                }
                
                $pagina = 0;
                if(isset($_POST["pagina"]) && (filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false || $_POST["pagina"] == "ultima")){
                    $pagina = $_POST["pagina"];
                }
                
                $limite = 0;
                if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                    $limite     = $_POST["limite"];
                }
                
                $div = '';
                $i = 0;
                $total = $clientesSPLA->totalRegistrosClientes1($_SESSION["client_id"], $_SESSION["client_empleado"], $cliente, $contrato, $inicioContrato);
                $query = $clientesSPLA->listar_todoFiltrado1($_SESSION["client_id"], $_SESSION["client_empleado"], $cliente, $contrato, $inicioContrato, $pagina, $limite);
                foreach($query as $row){
                $div .= '<tr>
                            <input type="hidden" id="id' . $i . '" name="id[]" value="' . $row["id"] . '">
                            <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["nombreCliente"] . '</td>
                            <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["contrato"] . '</td>
                            <td style="border: 1px solid; width:100px;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '"><input type="text" class="pointer" id="inicioContrato' . $i . '" name="inicioContrato[]" value="' . $row["inicioContrato"] . '" disabled></td>
                            <td style="border: 1px solid; width:100px;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '"><input type="text" class="pointer" id="finContrato' . $i . '" name="finContrato[]" value="' . $row["finContrato"] . '" disabled></td>
                            <script>
                                $("#inicioContrato' . $i . '").datepicker();
                                $("#finContrato' . $i . '").datepicker();
                            </script>
                        </tr>';
                    $i++;
                }

                $ultimo  = "no";
                $primero = "no";

                $limiteNuevo = $limite * $pagina;
                if($limiteNuevo > $total || $pagina == "ultima"){
                        $limiteNuevo = $total;
                        $ultimo      = "si";
                        $pagina      = $clientesSPLA->ultimaPaginaCliente1($_SESSION["client_id"], $_SESSION["client_empleado"], $cliente, $contrato, $inicioContrato, $limite);
                }

                if($pagina == 1){
                        $primero = "si";
                }

                $sinPaginacion = "no";
                if($limite > $total){
                        $sinPaginacion = "si";
                }

                $nuevoInicio = (($pagina - 1) * $limite) + 1;
                if($nuevoInicio < 0){
                    $nuevoInicio = 0;
                }
                $paginacion = $nuevoInicio .'-' . $limiteNuevo . ' of ' . $total;

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
                'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
                'sinPaginacion'=>$sinPaginacion, 'ultimaPagina'=>$pagina, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);