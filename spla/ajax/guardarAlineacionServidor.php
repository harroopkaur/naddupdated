<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores_SPLA.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $moduloServidor = new moduloServidores_SPLA();

            $vert = 0;
            if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
                $vert = $_POST["vert"];
            }
            
            $clusterVirtual      = $_POST["clusterVirtual"];
            $hostVirtual         = $_POST["hostVirtual"];
            $nombreVirtual       = $_POST["nombreVirtual"];
            $familiaVirtual      = $_POST["familiaVirtual"];
            $edicionVirtual      = $_POST["edicionVirtualSelected"];
            $versionVirtual      = $_POST["versionVirtualSelected"];
            $tipoVirtual         = $_POST["tipoVirtual"];
            $procesadoresVirtual = $_POST["procesadoresVirtual"];
            $coresVirtual        = $_POST["coresVirtual"];
            $licSrvVirtual       = $_POST["licSrvVirtual"];
            $licProcVirtual      = $_POST["licProcVirtual"];
            $licCoreVirtual      = $_POST["licCoreVirtual"];
            
            if($vert == 0){
                $moduloServidor->eliminarAlineaionWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"]);

                $conteo = 0;
                for($i = 0; $i < count($nombreVirtual); $i++){
                    if(filter_var($procesadoresVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $procesadoresVirtual[$i] = 0;
                    }
                    if(filter_var($coresVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $coresVirtual[$i] = 0;
                    }
                    if(filter_var($licSrvVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $licSrvVirtual[$i] = 0;
                    }
                    if(filter_var($licProcVirtual[$i], FILTER_VALIDATE_INT) === false){
                       $licProcVirtual[$i] = 0; 
                    }
                    if(filter_var($licCoreVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $licCoreVirtual[$i] = 0;
                    }
                    if($moduloServidor->agregarAlineacionWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"], $general->get_escape($clusterVirtual[$i]), 
                    $general->get_escape($hostVirtual[$i]), $general->get_escape($nombreVirtual[$i]), $general->get_escape($familiaVirtual[$i]), 
                    $general->get_escape($edicionVirtual[$i]), $general->get_escape($versionVirtual[$i]), $general->get_escape($tipoVirtual[$i]), 
                    $procesadoresVirtual[$i], $coresVirtual[$i], $licSrvVirtual[$i], $licProcVirtual[$i], $licCoreVirtual[$i])){
                        $conteo++;
                    }
                }
            }
            else{
                $moduloServidor->eliminarAlineaionSqlServer($_SESSION["client_id"], $_SESSION["client_empleado"]);

                $conteo = 0;
                for($i = 0; $i < count($nombreVirtual); $i++){
                    if(filter_var($procesadoresVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $procesadoresVirtual[$i] = 0;
                    }
                    if(filter_var($coresVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $coresVirtual[$i] = 0;
                    }
                    if(filter_var($licSrvVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $licSrvVirtual[$i] = 0;
                    }
                    if(filter_var($licProcVirtual[$i], FILTER_VALIDATE_INT) === false){
                       $licProcVirtual[$i] = 0; 
                    }
                    if(filter_var($licCoreVirtual[$i], FILTER_VALIDATE_INT) === false){
                        $licCoreVirtual[$i] = 0;
                    }
                    if($moduloServidor->agregarAlineacionSQLServer($_SESSION["client_id"], $_SESSION["client_empleado"], $general->get_escape($clusterVirtual[$i]), 
                    $general->get_escape($hostVirtual[$i]), $general->get_escape($nombreVirtual[$i]), $general->get_escape($familiaVirtual[$i]), 
                    $general->get_escape($edicionVirtual[$i]), $general->get_escape($versionVirtual[$i]), $general->get_escape($tipoVirtual[$i]), 
                    $procesadoresVirtual[$i], $coresVirtual[$i], $licSrvVirtual[$i], $licProcVirtual[$i], $licCoreVirtual[$i])){
                        $conteo++;
                    }
                }
            }

            $result = 0;

            if($conteo == $i){
                $result = 1;
            }
            else if($conteo > 0){
                $result = 2;
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);