<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_SPLA_historico.php");

$historico = new clase_SPLA_historico();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $result = 0;
            $descripcion = "";
            if(isset($_POST["descripcion"])){
                $descripcion = $general->get_escape($_POST["descripcion"]);
            }
            
            $hist = $historico->existeHistorico($_SESSION["client_id"]);
            
            if($hist == 0){
                $exito = $historico->insertarCabecera($_SESSION["client_id"], $descripcion);
                $newId = $historico->obtenerUltId();
            } else{
                $newId = $historico->obtenerIdFecha($_SESSION["client_id"]);
                $exito = true;
            }
            
            if($exito == true){
                $historico->eliminarHistoricos($newId);
                $historico->insertarHistoricoCustomer($_SESSION["client_id"], $_SESSION["client_empleado"], $newId);
                $historico->insertarHistoricoVM($_SESSION["client_id"], $_SESSION["client_empleado"], $newId);
                $historico->insertarHistoricoApps($_SESSION["client_id"], $_SESSION["client_empleado"], $newId);
                $historico->insertarHistoricoCurrentSPLA($_SESSION["client_id"], $_SESSION["client_empleado"], $newId);
                $result = 1;
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);