<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_SPLA_historico.php");

$virtualMachine = new clase_SPLA_historico();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $VM = "";
            if(isset($_POST["VM"])){
                $VM = $general->get_escape($_POST["VM"]);
            }
            
            $fechaInicio = date("Y") . "-01-01";
            if(isset($_POST["fechaInicio"]) && $_POST["fechaInicio"] != ""){
                if($general->validarFecha($_POST["fechaInicio"], "/", "mm/dd/aaaa")){
                    $fechaInicio = $general->reordenarFecha($_POST["fechaInicio"], "/", "-", $formato = "mm/dd/YYYY");
                }
            }
            
            $fechaFin = date("Y-m-d");
            if(isset($_POST["fechaFin"]) && $_POST["fechaFin"] != ""){
                if($general->validarFecha($_POST["fechaFin"], "/", "mm/dd/aaaa")){
                    $fechaFin = $general->reordenarFecha($_POST["fechaFin"], "/", "-", $formato = "mm/dd/YYYY");
                }
            }

            $pagina = 1;
            if(isset($_POST["pagina"]) && (filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false || $_POST["pagina"] == "ultima")){
                $pagina = $_POST["pagina"];
            }

            $limite = 0;
            if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                $limite     = $_POST["limite"];
            }

            $div = '';
            $i = 0;
            $total = $virtualMachine->reportHistoricalVMTotal($_SESSION["client_id"], $VM, $fechaInicio, $fechaFin);
            $query = $virtualMachine->reportHistoricalVMPaginado($_SESSION["client_id"], $VM, $fechaInicio, $fechaFin, $pagina, $limite);
            $totalCost = 0;
            
            $mesInicio = date("n", strtotime($fechaInicio)); 
            $mesFin = date("n", strtotime($fechaFin));
            $yearInicio = date("Y", strtotime($fechaInicio)); 
            $yearFin = date("Y", strtotime($fechaFin));

            if($yearFin > $yearInicio){
                $mesFin += 12;
            }

            $mesFin++;
            
            $celdas = "";
            $arrayCeldas = array();
            $j = 0;
            for($index = $mesInicio; $index < $mesFin; $index++){
                $valor = $general->completarCeroNumeros($index);
                $campo = $valor . '/'.  $yearInicio;
                if($index > 12){
                    $campo = $valor . '/'.  $yearFin;
                }
                //$celdas .= '<th style="text-align:center; border: 1px solid #000000; cursor:pointer;">' . $campo . '</th>';
                $arrayCeldas[$j] = '<th style="text-align:center; border: 1px solid #000000; cursor:pointer;">' . $campo . '</th>';
                $j++;
            }
            
            $celdasReversed = array_reverse($arrayCeldas, false);
            for($z = 0; $z < count($arrayCeldas); $z++){
                $celdas .= $celdasReversed[$z];
            }   
            
            $div .= '<table id="listaReportingVM" class="tablap" style="width:1200px; margin:0 auto;">
                <thead>
                    <tr>
                        <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Host Name</th>
                        <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Host Date</th>
                        <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM Name</th>
                        <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM Date</th>
                        ' . $celdas . '
                    </tr>
                </thead>
                <tbody>';
            
            $totFechas = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            foreach($query as $row){
                $valores = "";
                $valoresArray = array();
                $totalArray = array();
                $j = 0;
                for($index = $mesInicio; $index < $mesFin; $index++){
                    $valor = $general->completarCeroNumeros($index);
                    $campo = $valor . '/'.  $yearInicio;
                    if($index > 12){
                        $campo = $valor . '/'.  $yearFin;
                    }
                    //$valores .= '<td style="border: 1px solid; text-align:right;">' . $row[$campo] . '</td>';
                    //$totFechas[$index] += intval($row[$campo]); 
                    $valoresArray[$j] = '<td style="border: 1px solid; text-align:right;">' . $row[$campo] . '</td>';
                    $totalArray[$j] = intval($row[$campo]);
                    $j++;
                }       
                
                $valoresReversed = array_reverse($valoresArray, false);
                $totalReversed = array_reverse($totalArray, false);
                
                for($z = 0; $z < count($valoresReversed); $z++){
                    $valores .= $valoresReversed[$z];
                    $totFechas[$z] += $totalReversed[$z];
                }
                
            $div .= '<tr>
                    <td style="border: 1px solid;">' . $row["host"] . '</td>
                    <td style="border: 1px solid;">' . $row["hostDate"] . '</td>
                    <td style="border: 1px solid; text-align:right;">' . $row["VM"] . '</td>
                    <td style="border: 1px solid; text-align:right;">' . $row["VMDate"] . '</td>
                    ' . $valores . '
                </tr>';
                $i++;
            }
            $div .= '<tr>
                    <td colspan="4" style="border: 1px solid;" class="bold">Total</td>';
                    //for($index = $mesInicio; $index < $mesFin; $index++){
                    for($index = 0; $index < count($arrayCeldas); $index++){
                        $div .= '<td style="border: 1px solid; text-align:right;">' . $totFechas[$index] . '</td>';
                    }
                $div .= '</tr> 
                    </tbody>
                </table>
                <script> 
                    $("#listaReportingVM").tablesorter();
                    $("#listaReportingVM").tableHeadFixer();
                </script>';
            

            $ultimo  = "no";
            $primero = "no";

            $limiteNuevo = $limite * $pagina;
            if($limiteNuevo > $total || $pagina == "ultima"){
                    $limiteNuevo = $total;
                    $ultimo      = "si";
                    $pagina      = $virtualMachine->ultimaPaginaHistoricalReportingVM($_SESSION["client_id"], $VM, $fechaInicio, $fechaFin, $limite);
            }

            if($pagina == 1){
                    $primero = "si";
            }

            $sinPaginacion = "no";
            if($limite > $total){
                    $sinPaginacion = "si";
            }

            $nuevoInicio = (($pagina - 1) * $limite) + 1;
            if($nuevoInicio < 0){
                $nuevoInicio = 0;
            }
            $paginacion = $nuevoInicio .'-' . $limiteNuevo . ' of ' . $total;

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
            'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
            'sinPaginacion'=>$sinPaginacion, 'ultimaPagina'=>$pagina, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);