<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores_SPLA.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $moduloServidor = new moduloServidores_SPLA();

            $vert = 0;
            if(isset($_POST["vert"]) && filter_var($_POST["vert"], FILTER_VALIDATE_INT) !== false){
                $vert = $_POST["vert"];
            }
            
            $nombreFisico       = $_POST["nombreFisico"];
            $familiaFisico      = $_POST["familiaFisico"];
            $edicionFisico      = $_POST["edicionFisicoSelected"];
            $versionFisico      = $_POST["versionFisicoSelected"];
            $tipoFisico         = $_POST["tipoFisico"];
            $procesadoresFisico = $_POST["procesadoresFisico"];
            $coresFisico        = $_POST["coresFisico"];
            $licSrvFisico       = $_POST["licSrvFisico"];
            $licProcFisico      = $_POST["licProcFisico"];
            $licCoreFisico      = $_POST["licCoreFisico"];

            if($vert == 0){
                $moduloServidor->eliminarWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"], $tipoFisico[0]);

                $conteo = 0;
                for($i = 0; $i < count($nombreFisico); $i++){
                    if(filter_var($procesadoresFisico[$i], FILTER_VALIDATE_INT) === false){
                        $procesadoresFisico[$i] = 0;
                    }
                    if(filter_var($coresFisico[$i], FILTER_VALIDATE_INT) === false){
                        $coresFisico[$i] = 0;
                    }
                    if(filter_var($licSrvFisico[$i], FILTER_VALIDATE_INT) === false){
                        $licSrvFisico[$i] = 0;
                    }
                    if(filter_var($licProcFisico[$i], FILTER_VALIDATE_INT) === false){
                       $licProcFisico[$i] = 0; 
                    }
                    if(filter_var($licCoreFisico[$i], FILTER_VALIDATE_INT) === false){
                        $licCoreFisico[$i] = 0;
                    }
                    
                    if($moduloServidor->agregarWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", $general->get_escape($nombreFisico[$i]), 
                    $general->get_escape($familiaFisico[$i]), $general->get_escape($edicionFisico[$i]), $general->get_escape($versionFisico[$i]), 
                    $general->get_escape($tipoFisico[$i]), $procesadoresFisico[$i], $coresFisico[$i], $licSrvFisico[$i], $licProcFisico[$i], $licCoreFisico[$i])){
                        $conteo++;
                    }
                }
            }
            else{
                $moduloServidor->eliminarSqlServer($_SESSION["client_id"], $_SESSION["client_empleado"], $tipoFisico[0]);

                $conteo = 0;
                for($i = 0; $i < count($nombreFisico); $i++){
                    if(filter_var($procesadoresFisico[$i], FILTER_VALIDATE_INT) === false){
                        $procesadoresFisico[$i] = 0;
                    }
                    if(filter_var($coresFisico[$i], FILTER_VALIDATE_INT) === false){
                        $coresFisico[$i] = 0;
                    }
                    if(filter_var($licSrvFisico[$i], FILTER_VALIDATE_INT) === false){
                        $licSrvFisico[$i] = 0;
                    }
                    if(filter_var($licProcFisico[$i], FILTER_VALIDATE_INT) === false){
                       $licProcFisico[$i] = 0; 
                    }
                    if(filter_var($licCoreFisico[$i], FILTER_VALIDATE_INT) === false){
                        $licCoreFisico[$i] = 0;
                    }
                    if($moduloServidor->agregarSQLServer($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", $general->get_escape($nombreFisico[$i]), 
                    $general->get_escape($familiaFisico[$i]), $general->get_escape($edicionFisico[$i]), $general->get_escape($versionFisico[$i]), 
                    $general->get_escape($tipoFisico[$i]), $procesadoresFisico[$i], $coresFisico[$i], $licSrvFisico[$i], $licProcFisico[$i], $licCoreFisico[$i])){
                        $conteo++;
                    }
                }
            }

            $result = 0;

            if($conteo == $i){
                $result = 1;
            }
            else if($conteo > 0){
                $result = 2;
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);