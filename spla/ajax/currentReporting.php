<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_cliente.php");

$virtualMachine = new clase_SPLA_vCPU();
$clienteSPLA = new clase_SPLA_cliente();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $customers = $clienteSPLA->clientes($_SESSION["client_id"], $_SESSION["client_empleado"]);   
            
            $clientes = "";
            if(isset($_POST["clientes"])){
                $clientes = $general->get_escape($_POST["clientes"]);
            }

            $producto = "";
            if(isset($_POST["producto"])){
                $producto = $general->get_escape($_POST["producto"]);
            }

            $pagina = 0;
            if(isset($_POST["pagina"]) && (filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false || $_POST["pagina"] == "ultima")){
                $pagina = $_POST["pagina"];
            }

            $limite = 0;
            if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                $limite     = $_POST["limite"];
            }

            $div = '';
            $i = 0;
            $total = $virtualMachine->currentReportingTotal($_SESSION["client_id"], $_SESSION["client_empleado"], $clientes, $producto);
            $query = $virtualMachine->currentReportingFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], $clientes, $producto, $pagina, $limite);
            $totalCost = 0;
            $totalSale = 0;
            foreach($query as $row){
                $totalCost += $row["total"];
                $totalSale += $row["totalSale"];
            $div .= '<tr>
                    <input type="hidden" id="id' . $i . '" name="id[]" value="' . $row["id"] . '">
                    <input type="hidden" id="tipo' . $i . '" name="tipo[]" value="' . $row["tipo"] . '">
                    <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["nombreCliente"] . '</td>
                    <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["familia"] . '</td>
                    <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["SKU"] . '</td>
                    <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["installed"] . '</td>
                    <td style="border: 1px solid; text-align:right;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["Qty"] . '</td>
                    <td style="border: 1px solid; text-align:right;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["price"] . '</td>
                    <td style="border: 1px solid; text-align:right;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '">' . $row["total"] . '</td>
                    <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '"><input type="text" style="width:50px;" id="QtySale' . $i . '" 
                    name="QtySale[]" maxlength="4" class="text-right" value="' . $row["QtySale"] . '" onfocus="$(\'#QtySale' . $i . '\').val(\'\')" onkeyup="actualizarTotal()" disabled></td>
                    <td style="border: 1px solid;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '"><input type="text" style="width:50px;" id="priceSale' . $i . '" 
                    name="priceSale[]" maxlength="11" class="text-right" value="' . $row["priceSale"] . '" onfocus="$(\'#priceSale' . $i . '\').val(\'\')" onkeyup="actualizarTotal()" disabled></td>
                    <td style="border: 1px solid; text-align:right;" class="';
                            if($row["existe"] == 1){ $div .= 'nuevoProducto'; } else if($row["existe"] == 2){ $div .= 'productoNoExiste'; } 
                            $div .= '"><input type="text" style="width:50px;" id="totalSale' . $i . '" 
                    name="totalSale[]" maxlength="11" class="text-right" value="' . $row["totalSale"] . '" disabled></td>
                    <script>
                        $("#QtySale' . $i . '").numeric(false);
                        $("#priceSale' . $i . '").numeric();
                    </script>
                </tr>';
                $i++;
            }
            $div .= '<tr>
                    <td style="border: 1px solid;" class="bold">Total</td>
                    <td colspan="5" style="border: 1px solid;"></td>
                    <td style="border: 1px solid; text-align:right;">' . $totalCost . '</td>
                    <td colspan="2" style="border: 1px solid; width:100px;"></td>
                    <td style="border: 1px solid; text-align:right;"><input type="text" style="width:50px;" id="totSale" 
                    name="totSale" maxlength="11" class="text-right" value="' . $totalSale . '" disabled></td>
                </tr>';
            

            $ultimo  = "no";
            $primero = "no";

            $limiteNuevo = $limite * $pagina;
            if($limiteNuevo > $total || $pagina == "ultima"){
                    $limiteNuevo = $total;
                    $ultimo      = "si";
                    $pagina      = $virtualMachine->ultimaPaginaReportingCurrent($_SESSION["client_id"], $_SESSION["client_empleado"], $clientes, $producto, $limite);
            }

            if($pagina == 1){
                    $primero = "si";
            }

            $sinPaginacion = "no";
            if($limite > $total){
                    $sinPaginacion = "si";
            }

            $nuevoInicio = (($pagina - 1) * $limite) + 1;
            if($nuevoInicio < 0){
                $nuevoInicio = 0;
            }
            $paginacion = $nuevoInicio .'-' . $limiteNuevo . ' of ' . $total;

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
            'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
            'sinPaginacion'=>$sinPaginacion, 'ultimaPagina'=>$pagina, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);