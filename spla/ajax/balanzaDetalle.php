<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $balance2= new Balance_SPLA();
                
                $familia = "";
                if(isset($_POST["familia"])){
                    $familia = $_POST["familia"];
                }
                
                $edicion = "";
                if(isset($_POST["edicion"])){
                    $edicion = $_POST["edicion"];
                }

                $clientTotalOfficeCompras  = 0;
                $clientTotalOfficeInstal   = 0;	
                $clientOfficeNeto          = 0;
                $total_1Inac               = 0;

                $titulo = "";

                if($familia != "Otros"){
                    $titulo = $familia;
                }
                else{
                     $titulo = $edicion;
                }

                if($familia == "Windows"){
                        if($edicion != "Otros"){
                                $listar_Of = $balance2->listar_todo_familias6($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion);
                        }
                        else{
                                $listar_Of = $balance2->listar_todo_familias10($_SESSION['client_id'], $_SESSION['client_empleado'], $familia);
                        }
                }
                else if($familia == "Others"){
                        $listar_Of = $balance2->listar_todo_familias7($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion);
                }
                else{
                        if($edicion != "Otros"){
                                $listar_Of = $balance2->listar_todo_familias1($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion);
                        }
                        else{
                                $listar_Of = $balance2->listar_todo_familias9($_SESSION['client_id'], $_SESSION['client_empleado'], $familia);
                        }
                }

                foreach($listar_Of as $reg_equipos){
                        $clientTotalOfficeCompras += $reg_equipos["compra"];
                        $clientTotalOfficeInstal  += $reg_equipos["instalaciones"];
                        $clientOfficeNeto          = $clientTotalOfficeCompras - $clientTotalOfficeInstal;
                }

                if($familia == "Windows" || $familia == "Windows Server"){
                    $detalles1 = new Detalles_SPLA();
                    if($edicion != ""){
                        $listar_equipos = $detalles1->listar_todoEdicion($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion);
                    }
                    else{
                        $listar_equipos = $detalles1->listar_todo($_SESSION['client_id'], $_SESSION['client_empleado']);
                    }

                    if ($listar_equipos) {
                        $totalInstalC = 0;
                        $totalInstalS = 0;
                        $totalInactC  = 0;
                        $totalInactS  = 0; 
                        foreach ($listar_equipos as $reg_equipos) {

                            if ($reg_equipos["tipo"] == 1) {//cliente
                                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                                    $totalInstalC++;
                                }
                                else{
                                    $totalInactC++;
                                }
                            } else {//server
                                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                                    $totalInstalS++;
                                }
                                else{
                                    $totalInactS++;
                                }
                            }
                        }
                        if($familia == "Windows"){
                            $clientTotalOfficeInstal = $totalInstalC;
                            $total_1Inac             = $totalInactC;
                        }
                        else{
                            $clientTotalOfficeInstal = $totalInstalS;
                            $total_1Inac             = $totalInactS;
                        }
                        $clientOfficeNeto = $clientTotalOfficeCompras - $clientTotalOfficeInstal;
                    }
                }

                $tabla = "";
                foreach($listar_Of as $reg_equipos){
                    $tabla .= '<tr>
                            <td>' . $reg_equipos["familia"] . '</td>
                            <td>' . $reg_equipos["office"] . '</td>
                            <td>' . $reg_equipos["version"] . '</td>
                            <td align="center">' . $reg_equipos["instalaciones"] . '</td>
                            <td align="center">' . round($reg_equipos["compra"], 0) . '</td>
                            <td align="center">' . round($reg_equipos["balance"], 0) . '</td>
                            <td align="center"';
                    if($reg_equipos["balancec"] < 0){
                        $tabla .= ' style="color:red;"';
                    }
                    $tabla .= '>' . round($reg_equipos["balancec"], 0) . '</td>
                    </tr>';
                }

                $ediciones = '<option value="" ';
                if($edicion == "") $ediciones .= "selected='selected'";
                $ediciones .= '>Seleccione..</option>';
                if($familia == "Windows"){
                        $ediciones .= '<option value="Enterprise" ';
                        if($edicion == "Enterprise") $ediciones .= "selected='selected'";
                        $ediciones .= '>Enterprise</option>
                        <option value="Professional" ';
                        if($edicion == "Professional") $ediciones .= "selected='selected'";
                        $ediciones .= '>Professional</option>
                        <option value="Otros" ';
                        if($edicion == "Otros") $ediciones .= "selected='selected'";
                        $ediciones .= '>Otros</option>';
                }
                else if($familia == "Office" || $familia == "Project" || $familia == "Visio"){
                        $ediciones .= '<option value="Standard" ';
                        if($edicion == "Standard") $ediciones .= "selected='selected'";
                        $ediciones .= '>Standard</option>
                        <option value="Professional" ';
                        if($edicion == "Professional") $ediciones .= "selected='selected'";
                        $ediciones .= '>Professional</option>
                        <option value="Otros" ';
                        if($edicion == "Otros") $ediciones .= "selected='selected'";
                        $ediciones .= '>Otros</option>';
                }
                else if($familia == "Windows Server" || $familia == "SQL"){
                        $ediciones .= '<option value="Standard" ';
                        if($edicion == "Standard") $ediciones .= "selected='selected'";
                        $ediciones .= '>Standard</option>
                        <option value="Datacenter" ';
                        if($edicion == "Datacenter") $ediciones .= "selected='selected'";
                        $ediciones .= '>Datacenter</option>
                        <option value="Enterprise" ';
                        if($edicion == "Enterprise") $ediciones .= "selected='selected'";
                        $ediciones .= '>Enterprise</option>
                        <option value="Otros" ';
                        if($edicion == "Otros") $ediciones .= "selected='selected'";
                        $ediciones .= '>Otros</option>';
                }
                else{
                        $ediciones .= '<option value="Visual" ';
                        if($edicion == "Visual") $ediciones .= "selected='selected'";
                        $ediciones .= '>Visual Studio</option>
                        <option value="Exchange" ';
                        if($edicion == "Exchange") $ediciones .= "selected='selected'";
                        $ediciones .= '>Exchange Server</option>
                        <option value="Sharepoint" ';
                        if($edicion == "Sharepoint") $ediciones .= "selected='selected'";
                        $ediciones .= '>Sharepoint Server</option>
                        <option value="Skype" ';
                        if($edicion == "Skype") $ediciones .= "selected='selected'";
                        $ediciones .= '>Skype for Business</option>
                        <option value="System Center" ';
                        if($edicion == "System Center") $ediciones .= "selected='selected'";
                        $ediciones .= '>System Center</option>';
                }

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'titulo'=>$titulo, 'compra'=>$clientTotalOfficeCompras, 
                'instalacion'=>$clientTotalOfficeInstal, 'tabla'=>$tabla, 'edicion'=>$ediciones, 'neto'=>$clientOfficeNeto, 
                'obsoleto'=>$total_1Inac, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);