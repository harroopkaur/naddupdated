<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_filepcs_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_tipo_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_cliente.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_SKU.php");


$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $resumenOf     = new Resumen_SPLA();
            $consolidadoOf = new Consolidado_SPLA();
            $archivosDespliegue = new clase_archivos_fabricantes();
            $detalles     = new Detalles_SPLA();
            $filec        = new Filepc_SPLA();
            $escaneo      = new Scaneo_SPLA();
            $procesadores = new ConsolidadoProcesadores_SPLA();
            $tipoEquipo   = new ConsolidadoTipoEquipo_SPLA();
            $servidores   = new moduloServidores_SPLA();
            $compras   = new Compras_SPLA();
            $balance  = new Balance_SPLA(); 
            $virtualMachine = new clase_SPLA_vCPU();
            $clienteSPLA = new clase_SPLA_cliente();
            $SKU = new clase_SPLA_SKU();

            $i = 17;
            $j = 0;
            if($consolidadoOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
           
            if($archivosDespliegue->eliminarArchivosFabricante($_SESSION["client_id"], $_SESSION["client_empleado"], 3)){
                $j++;
            }
            
            if($resumenOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($filec->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($detalles->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($escaneo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($procesadores->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($tipoEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarAlineaionWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarAlineaionSqlServer($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarWindowServer1($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarSqlServer1($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($compras->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($balance->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($virtualMachine->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($clienteSPLA->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($SKU->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            $result = 0;
            if($i > $j){
                $result = 2;
            } else if($i == $j){
                $result = 1;
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);