<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores_SPLA.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $moduloServidor = new moduloServidores_SPLA();

            $vert = 0;
            if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
                $vert = $_POST["vert"];
            }

            if($vert == 0){
                $tabla = $moduloServidor->windowServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
                $ediciones = $moduloServidor->obtenerEdicProducto(3, 5);
            }
            else{
                $tabla = $moduloServidor->sqlServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
                $ediciones = $moduloServidor->obtenerEdicProducto(3, 13);
            }

            //$versiones = $moduloServidor->obtenerVersiones();
            $body       = "";
            $clusterAux = "";
            $j = 0;
            foreach($tabla as $row){
                $hostAux    = "";
                if($clusterAux != $row["cluster"]){
                    $body .= '<tr style="border-bottom: 1px solid #ddd" id="rowVirtual' . $j . '">
                        <td><input type="checkbox" id="checkVirtual' . $j .'" name="checkVirtual[]" value="' . $j . '"></td>
                        <td><input type="text" id="clusterVirtual' . $j . '" name="clusterVirtual[]" value="' . $row["cluster"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="hostVirtual' . $j . '" name="hostVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="nombreVirtual' . $j . '" name="nombreVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="familiaVirtual' . $j . '" name="familiaVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="edicionVirtual' . $j . '" name="edicionVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="versionVirtual' . $j . '" name="versionVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="tipoVirtual' . $j . '" name="tipoVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="procesadoresVirtual' . $j . '" name="procesadoresVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="coresVirtual' . $j . '" name="coresVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licSrvVirtual' . $j .'" name="licSrvVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licProcVirtual' . $j . '" name="licProcVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licCoreVirtual' . $j . '" name="licCoreVirtual[]" value="" readonly></td>
                    </tr>
                    <input type="hidden" id="block' . $j . '" value="true">';

                    $body .= '<input type="hidden" id="edicionVirtualSelected' . $j . '" name="edicionVirtualSelected[]" value="" style="background-color:#C1C1C1;" readonly>
                              <input type="hidden" id="versionVirtualSelected' . $j . '" name="versionVirtualSelected[]" value="" style="background-color:#C1C1C1;" readonly>';

                    if($vert == 0){
                        $tablaHost = $moduloServidor->windowServerCluster($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual", $row["cluster"]);
                    }
                    else{
                        $tablaHost = $moduloServidor->sqlServerCluster($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual", $row["cluster"]);
                    }

                    foreach($tablaHost as $rowHost){
                        $versiones = $moduloServidor->obtenerVersionEquivalencia(3, $rowHost["familia"], $rowHost["edicion"]);
                        if($hostAux != $rowHost["host"]){
                            $hostAux = $rowHost["host"];
                            $j++;
                            $body .= '<tr style="border-bottom: 1px solid #ddd" id="rowVirtual' . $j .'">
                                <td><input type="checkbox" id="checkVirtual' . $j . '" name="checkVirtual[]" value="' . $j . '"></td>
                                <td><input type="text" id="clusterVirtual' . $j . '" name="clusterVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="hostVirtual' . $j . '" name="hostVirtual[]" value="' . $rowHost["host"] . '" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="nombreVirtual' . $j . '" name="nombreVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="familiaVirtual' . $j . '" name="familiaVirtual[]" value="' . $rowHost["familia"] . '" style="background-color:#C1C1C1;" readonly></td>
                                <td>
                                    <select id="edicionVirtual' . $j . '" name="edicionVirtual[]" onchange="selectEdicion(this.value, \'Virtual\', ' . $j . ')">';
                                        foreach($ediciones as $row1){
                                            $body .= '<option value="' . $row1["nombre"] . '"';
                                            if($rowHost["edicion"] == $row1["nombre"]){ 
                                                $body .= "selected='selected'";
                                            }
                                            $body .= '>' . $row1["nombre"] . '</option>';
                                        }
                                    $body .= '</select>
                                </td>
                                <td>
                                    <select id="versionVirtual' . $j . '" name="versionVirtual[]" onchange="selectVersion(this.value, \'Virtual\', ' . $j . ')">';
                                        foreach($versiones as $row1){
                                        $body .= '<option value="' . $row1["nombre"] . '"';
                                            if($rowHost["version"] == $row1["nombre"]){
                                                $body .= "selected='selected'";
                                            }
                                            $body .= '>' . $row1["nombre"] . '</option>';
                                        }
                                    $body .= '</select>
                                </td>
                                <td><input type="text" id="tipoVirtual' . $j . '" name="tipoVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" style="text-align:center" id="procesadoresVirtual' . $j . '" name="procesadoresVirtual[]" value="0" onclick="selProcCores(\'procesadoresVirtual\', ' . $j . ')" readonly></td>
                                <td><input type="text" style="text-align:center" id="coresVirtual' . $j . '" name="coresVirtual[]" value="0" onclick="selProcCores(\'coresVirtual\', ' . $j . ')" readonly></td>
                                <td><input type="text" style="text-align:center" id="licSrvVirtual' . $j .'" name="licSrvVirtual[]" value="0" onclick="seleccionar(\'licSrvVirtual\', ' . $j . ')" readonly></td>
                                <td><input type="text" style="text-align:center" id="licProcVirtual' . $j . '" name="licProcVirtual[]" value="0" onclick="seleccionar(\'licProcVirtual\', ' . $j . ')" readonly></td>
                                <td><input type="text" style="text-align:center" id="licCoreVirtual' . $j . '" name="licCoreVirtual[]" value="0" onclick="seleccionar(\'licCoreVirtual\', ' . $j . ')" readonly></td>
                            </tr>
                            <input type="hidden" id="block' . $j . '" value="false">
                            <script>
                                $("#procesadoresVirtual' . $j . '").numeric(false);
                                $("#coresVirtual' . $j . '").numeric(false);
                                $("#licSrvVirtual' . $j . '").numeric(false);
                                $("#licProcVirtual' . $j . '").numeric(false);
                                $("#licCoreVirtual' . $j . '").numeric(false);
                            </script>';

                            $body .= '<input type="hidden" id="edicionVirtualSelected' . $j . '" name="edicionVirtualSelected[]" value="" style="background-color:#C1C1C1;" readonly>
                                      <input type="hidden" id="versionVirtualSelected' . $j . '" name="versionVirtualSelected[]" value="" style="background-color:#C1C1C1;" readonly>';
                        }
                    }

                    $j++;

                    $body .= '<tr style="border-bottom: 1px solid #ddd" id="rowVirtual' .  $j . '">
                        <td><input type="checkbox" id="checkVirtual' . $j . '" name="checkVirtual[]" value="' . $j . '"></td>
                        <td><input type="text" id="clusterVirtual' . $j . '" name="clusterVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="hostVirtual' . $j . '" name="hostVirtual[]" value="' . $row["host"] . '" style="background-color:#C1C1C1;" readonly></td>    
                        <td><input type="text" id="nombreVirtual' . $j . '" name="nombreVirtual[]" value="' . $row["equipo"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="familiaVirtual' . $j . '" name="familiaVirtual[]" value="' . $row["familia"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="edicionVirtual' . $j . '" name="edicionVirtual[]" value="' . $row["edicion"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="versionVirtual' . $j . '" name="versionVirtual[]" value="' . $row["version"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="tipoVirtual' . $j . '" name="tipoVirtual[]" value="' . $row["tipo"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="procesadoresVirtual' . $j . '" name="procesadoresVirtual[]" value="' . $row["cpu"] . '" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="coresVirtual' . $j . '" name="coresVirtual[]" value="' . $row["cores"] . '" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licSrvVirtual' . $j .'" name="licSrvVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licProcVirtual' . $j . '" name="licProcVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licCoreVirtual' . $j . '" name="licCoreVirtual[]" value="" readonly></td>
                    </tr>
                    <input type="hidden" id="block' . $j . '" value="true">';

                    $body .= '<input type="hidden" id="edicionVirtualSelected' . $j . '" name="edicionVirtualSelected[]" value="" style="background-color:#C1C1C1;">
                              <input type="hidden" id="versionVirtualSelected' . $j . '" name="versionVirtualSelected[]" value="" style="background-color:#C1C1C1;">';
                    $clusterAux = $row["cluster"];
                }
                else{
                    $body .= '<tr style="border-bottom: 1px solid #ddd" id="rowVirtual' .  $j . '">
                        <td><input type="checkbox" id="checkVirtual' . $j . '" name="checkVirtual[]" value="' . $j . '"></td>
                        <td><input type="text" id="clusterVirtual' . $j . '" name="clusterVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="hostVirtual' . $j . '" name="hostVirtual[]" value="' . $row["host"] . '" style="background-color:#C1C1C1;" readonly></td>    
                        <td><input type="text" id="nombreVirtual' . $j . '" name="nombreVirtual[]" value="' . $row["equipo"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="familiaVirtual' . $j . '" name="familiaVirtual[]" value="' . $row["familia"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="edicionVirtual' . $j . '" name="edicionVirtual[]" value="' . $row["edicion"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="versionVirtual' . $j . '" name="versionVirtual[]" value="' . $row["version"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" id="tipoVirtual' . $j . '" name="tipoVirtual[]" value="' . $row["tipo"] . '" style="background-color:#C1C1C1;" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="procesadoresVirtual' . $j . '" name="procesadoresVirtual[]" value="' . $row["cpu"] . '" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="coresVirtual' . $j . '" name="coresVirtual[]" value="' . $row["cores"] . '" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licSrvVirtual' . $j .'" name="licSrvVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licProcVirtual' . $j . '" name="licProcVirtual[]" value="" readonly></td>
                        <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licCoreVirtual' . $j . '" name="licCoreVirtual[]" value="" readonly></td>
                    </tr>
                    <input type="hidden" id="block' . $j . '" value="true">';

                    $body .= '<input type="hidden" id="edicionVirtualSelected' . $j . '" name="edicionVirtualSelected[]" value="" style="background-color:#C1C1C1;" readonly>
                              <input type="hidden" id="versionVirtualSelected' . $j . '" name="versionVirtualSelected[]" value="" style="background-color:#C1C1C1;" readonly>';
                }

                $j++;
            }    

            $body .= '<script>$("#tablaVirtual").tableHeadFixer({"left" : 3});</script>';
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$body, 'newRow'=>$j, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);