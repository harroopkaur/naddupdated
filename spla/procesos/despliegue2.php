<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_filepcs_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_conversion.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_tipo_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");

$detalles     = new Detalles_SPLA();
$detalles2    = new Detalles_SPLA();
$tablac       = new TablaC();
$filec        = new Filepc_SPLA();
$filec2       = new Filepc_SPLA();
$filepcs      = new Filepc_SPLA();
$general      = new General();
$conversion   = new TablaC();
$conversion2  = new TablaC();
$escaneo      = new Scaneo_SPLA();
$escaneo2     = new Scaneo_SPLA();
$procesadores = new ConsolidadoProcesadores_SPLA();
$tipoEquipo   = new ConsolidadoTipoEquipo_SPLA();
$servidores   = new moduloServidores_SPLA();
$config       = new configuraciones();
$archivosDespliegue = new clase_archivos_fabricantes();

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$listaEdiciones = $config->listar_edicionTotal();
$listaVersiones = $config->listar_versionTotal();

$datosAux = array();
if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];


    // Validaciones
    if ($nombre_imagen != "") {
        $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
        $long = count($extension) - 1;
        if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
            $error2 = 1;
        }  // Permitir subir solo imagenes JPG,
    } else {
        $error2 = 2;
    }

    if ($error2 == 0) {
        $imagen = $_SESSION['client_id'] . $_SESSION['client_empleado'] . "_LAE_Output" . date("dmYHis") . ".csv";

        if (!$filec->cargar_archivo($imagen, $temporal_imagen)) {
            $error2 = 5;
        } else {
            if($general->obtenerSeparadorLAE_Output("archivos_csvf2/" . $imagen) === true){
                if (($fichero = fopen("archivos_csvf2/" . $imagen, "r")) !== false) {
                    $i = 1;
                    $iwhenCreated = $iDN = $iobjectClass = $icn = $iuserAccountControl = $ilastLogon = $ipwdLastSet = $ioperatingSystem
                    = $ioperatingSystemVersion = $ilastLogonTimestamp = -1;
                    $procesarLAE = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        for($k = 0; $k < count($datos); $k++){
                            if(trim(utf8_encode($datos[$k])) == "whenCreated"){
                                $iwhenCreated = $k;
                            }
                            
                            if(trim(utf8_encode($datos[$k])) == "DN"){
                                $iDN = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "objectClass"){
                                $iobjectClass = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "cn"){
                                $icn = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "userAccountControl"){
                                $iuserAccountControl = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "lastLogon"){
                                $ilastLogon = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "pwdLastSet"){
                                $ipwdLastSet = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "operatingSystem"){
                                $ioperatingSystem = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "operatingSystemVersion"){
                                $ioperatingSystemVersion = $k;
                            }

                            if(trim(utf8_encode($datos[$k])) == "lastLogonTimestamp"){
                                $ilastLogonTimestamp = $k;
                            }
                        }
                        /*if($i == 1 && ($datos[0] != "DN" || $datos[1] != "objectClass" || $datos[2] != "cn" || $datos[3] != "userAccountControl" 
                        || $datos[4] != "lastLogon" || $datos[5] != "pwdLastSet" || $datos[6] != "operatingSystem" || $datos[7] != "operatingSystemVersion"
                        || $datos[8] != "lastLogonTimestamp")){
                            $error2 = 3;
                            break;
                        }*/
                        $i++;
                        
                        break;
                    }
                }
                fclose($fichero);
                
                if($iwhenCreated == -1 && $iDN == -1 && $iobjectClass == -1 && $icn == -1 && $iuserAccountControl == -1 && $ilastLogon == -1 &&
                $ipwdLastSet == -1 && $ioperatingSystem == -1 && $ioperatingSystemVersion == -1 && $ilastLogonTimestamp == -1){
                    $procesarLAE = false;
                } else if($iwhenCreated == -1 || $iDN == -1 || $iobjectClass == -1 || $icn == -1 || $iuserAccountControl == -1 || $ilastLogon == -1 ||
                $ipwdLastSet == -1 || $ioperatingSystem == -1 || $ioperatingSystemVersion == -1 || $ilastLogonTimestamp == -1){
                    $error2 = 3;
                }
            } else{
                //$error2 = 6;
                $procesarLAE = false;
            }
        }
        
        if($error2 == 0){
            $filec2->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            if (($fichero = fopen("archivos_csvf2/" . $imagen, "r")) !== FALSE) {
                $i = 1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                
                while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                    if ($i > 1) {

                        $sistema = $datos[$ioperatingSystem];
                        
                        if ($conversion->codigo_existe($datos[$ioperatingSystem], 0)) {
                            $conversion2->datos2($datos[$ioperatingSystem]);
                            $sistema = $conversion2->os;
                        }
                        
                        if($datos[$iwhenCreated] != ""){
                            $fechaCreatedAux = explode(".", $datos[$iwhenCreated]);
                            $fechaCreated = substr($fechaCreatedAux[0], 0, 4) . "-" . substr($fechaCreatedAux[0], 4, 2) . "-" . substr($fechaCreatedAux[0], 6, 2);
                        }

                        /*if (!$filec->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], $sistema, $datos[7])) {
                            echo $filec->error;
                        }*/
                        
                        if($j == 0){
                            $insertarBloque = true;
                            $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                            . ":cn" . $j . ", :whenCreated" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                            . ":os" . $j . ", :lastlogontimes" . $j . ")";
                        } else {
                            $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                            . ":cn" . $j . ", :whenCreated" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                            . ":os" . $j . ", :lastlogontimes" . $j . ")";
                        } 
                        
                        $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                        $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                        $bloqueValores[":dn" . $j] = $general->truncarString($datos[$iDN], 250);
                        $bloqueValores[":objectclass" . $j] = $general->truncarString($datos[$iobjectClass], 250);
                        $bloqueValores[":cn" . $j] = $general->truncarString($datos[$icn], 250);
                        $bloqueValores[":whenCreated" . $j] = $fechaCreated;
                        $bloqueValores[":useracountcontrol" . $j] = $general->truncarString($datos[$iuserAccountControl], 250);
                        $bloqueValores[":lastlogon" . $j] = $general->truncarString($datos[$ilastLogon], 250);
                        $bloqueValores[":pwdlastset" . $j] = $general->truncarString($datos[$ipwdLastSet], 250);
                        $bloqueValores[":os" . $j] = $general->truncarString($sistema, 250);
                        $bloqueValores[":lastlogontimes" . $j] = $general->truncarString($datos[$ilastLogonTimestamp], 250);

                        if($j == $general->registrosBloque){
                            if(!$filec->insertarEnBloque($bloque, $bloqueValores)){ 
                                //echo $filec->error;
                            }
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }
                    $i++;
                    $exito2 = 1;
                }
                
                if($insertarBloque === true){
                    if(!$filec->insertarEnBloque($bloque, $bloqueValores)){
                        //echo $filec->error;
                    }
                }
                
                if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 10) === false){
                    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 10);
                }

                $archivosDespliegue->actualizarDespliegue2($_SESSION["client_id"], $_SESSION['client_empleado'], 10, $imagen);
            }
            
            
            //inicio actualizar filepcs
            $detalles2->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            $lista_todos_files = $filepcs->listar_todo($_SESSION['client_id'], $_SESSION['client_empleado']);

            if ($lista_todos_files) {
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                
                $fechaDespliegue = strtotime('now');
                
                if($general->validarFecha($_POST["fechaDespliegue"], "/", "dd/mm/aaaa")){
                    if($_SESSION["idioma"] == 1){
                        $fechaDespliegue = strtotime($general->reordenarFecha($_POST["fechaDespliegue"], "/", "-"));
                    } else if($_SESSION["idioma"] == 2){
                        $fechaDespliegue = strtotime($general->reordenarFecha($_POST["fechaDespliegue"], "/", "-", "mm/dd/YYYY"));
                    }
                }
                
                foreach ($lista_todos_files as $reg_f) {

                    $host = explode('.', $reg_f["cn"]);
                    if(filter_var($reg_f["cn"], FILTER_VALIDATE_IP)!== false){
                        $host[0] = $reg_f["cn"];
                    }

                    $sistema = $reg_f["os"];

                    if ($reg_f["lastlogon"] != "") {

                        $value = round(($reg_f["lastlogon"] - 116444735995904000) / 10000000);

                        $fecha1 = date('d/m/Y', $value);

                        $dias1 = NULL; //$general->daysDiff($value, $fechaDespliegue);
                    } else {
                        $dias1 = NULL;
                    }

                    if ($reg_f["pwdlastset"] != "") {

                        $value2 = round(($reg_f["pwdlastset"] - 116444735995904000) / 10000000);

                        $fecha2 = date('d/m/Y', $value2);

                        $dias2 = NULL; //$general->daysDiff($value2, $fechaDespliegue);
                    } else {
                        $dias2 = NULL;
                    }
                    if ($reg_f["lastlogontimes"] != "") {

                        $value3 = round(($reg_f["lastlogontimes"] - 116444735995904000) / 10000000);

                        $fecha3 = date('d/m/Y', $value3);

                        $dias3 = NULL; //$general->daysDiff($value3, $fechaDespliegue);
                    } else {
                        $dias3 = NULL;
                    }

                    $minimos = $general->minimo($dias1, $dias2, $dias3);

                    $minimor = round(abs($minimos), 0);

                    if ($minimor <= 30) {
                        $minimo = 1;
                    } else if ($minimor <= 60) {
                        $minimo = 2;
                    } else if ($minimor <= 90) {
                        $minimo = 3;
                    } else if ($minimor <= 365) {
                        $minimo = 4;
                    } else {
                        $minimo = 5;
                    }

                    if ($minimo < 4) {
                        $activo = 1;
                    } else {
                        $activo = 0;
                    }

                    $tipos = $general->search_server($reg_f["os"]);

                    $tipo = $tipos[0];
                    $familia = "";
                    if(strpos($sistema, "Windows Server") !== false || (strpos($sistema, "Windows") !== false && strpos($sistema, "Server") !== false)){
                        $familia = "Windows Server";
                    }
                    else if(strpos($sistema, "Windows") !== false){
                        $familia = "Windows"; 
                    }

                    $edicion = "";
                    foreach($listaEdiciones as $rowEdiciones){
                        if(trim($rowEdiciones["nombre"]) != ""){
                            if(strpos($sistema, trim($rowEdiciones["nombre"])) !== false && strlen(trim($rowEdiciones["nombre"])) > strlen($edicion)){
                                 $edicion = trim($rowEdiciones["nombre"]);    
                            }
                        }
                    }
                    if($edicion == "" || ($familia == "Windows Server" && $edicion == "Server") || $edicion == "Windows 2000 Server"){
                        $edicion = "Standard";
                    }

                    $version = "";
                    foreach($listaVersiones as $rowVersiones){
                        if(trim($rowVersiones["nombre"]) != ""){
                            if(strpos($sistema, trim($rowVersiones["nombre"])) !== false && strlen(trim($rowVersiones["nombre"])) > strlen($version)){
                                 $version = trim($rowVersiones["nombre"]);    
                            }
                        }
                    } 

                    if($familia == ""){
                        $edicion = "";
                        $version = "";
                    }

                    /*if ($detalles->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], addslashes($host[0]), $sistema, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimor, $activo, $tipo, $minimo)) {
                        $exito2 = 1;
                    } else {
                        echo $detalles->error;
                    }*/
                    
                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                        . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                        . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ", :whenCreated" . $j . ")";
                    } else {
                        $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                        . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                        . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ", :whenCreated" . $j . ")";
                    } 
                    
                    $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                    $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                    $bloqueValores[":equipo" . $j] = $host[0];
                    $bloqueValores[":os" . $j] = $sistema;
                    $bloqueValores[":familia" . $j] = $familia;
                    $bloqueValores[":edicion" . $j] = $edicion;
                    $bloqueValores[":version" . $j] = $version;
                    $bloqueValores[":dias1" . $j] = $dias1;
                    $bloqueValores[":dias2" . $j] = $dias2;
                    $bloqueValores[":dias3" . $j] = $dias3;
                    $bloqueValores[":minimo" . $j] = $minimor;
                    $bloqueValores[":activo" . $j] = $activo;
                    $bloqueValores[":tipo" . $j] = $tipo;
                    $bloqueValores[":rango" . $j] = $minimo;
                    $bloqueValores[":whenCreated" . $j] = $reg_f["whenCreated"];

                    if($j == $general->registrosBloque){
                        if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){ 
                            //echo $detalles->error;
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }//foreach
                
                if($insertarBloque === true){
                    if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){   
                        //echo $detalles->error;
                    }
                }
            }//if
            //fin actualizar filepcs
            //
            //inicio escaneo
            $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";

            $escaneo2->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            if (($fichero = fopen("archivos_csvf1/" . $imagen, "r")) !== FALSE) {
                $i = 1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                
                while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                    if ($i != 1) {
                        $host = explode('.', $datos[0]);
                        if(filter_var($datos[0], FILTER_VALIDATE_IP)!== false){
                            $host[0] = $datos[0];
                        }

                        /*if (!$escaneo->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], addslashes($host[0]), $datos[1], $datos[2])) {
                            echo $escaneo->error;
                        }*/
                        if($j == 0){
                        $insertarBloque = true;
                            $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                        } else {
                            $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                        } 

                        $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                        $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                        $bloqueValores[":equipo" . $j] = $host[0];
                        $bloqueValores[":status" . $j] = $datos[1];
                        $bloqueValores[":errors" . $j] = utf8_encode($datos[2]);

                        if($j == $general->registrosBloque){
                            if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo "Escaneo: " . $escaneo->error;
                            }
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }
                    $i++;
                }
                
                if($insertarBloque === true){
                    if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){   
                        echo "Escaneo: " . $escaneo->error;
                    }
                }
                $exito2 = 1;
            }
            //fin escaneo
            
            
            //inicio actualizar escaneo
            $lista_equipos_scaneados = $escaneo->listar_todo2($_SESSION['client_id'], $_SESSION['client_empleado']);
            if ($lista_equipos_scaneados) {
                foreach ($lista_equipos_scaneados as $reg_e) {
                    if($detalles->actualizar($_SESSION['client_id'], $_SESSION['client_empleado'], addslashes($reg_e["equipo"]), $reg_e["errors"])){
                        echo $detalles->error;
                    }
                }
            }
            //fin actualizar escaneo

            //inicio cargar consolidado procesadores
            $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Procesadores.csv";

            $procesadores->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            if (($fichero = fopen("archivos_csvf1/".$imagen, "r")) !== FALSE) {
                $i=1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                
                while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                    $datosAux[0] = "";
                    $datosAux[1] = "";
                    $datosAux[2] = "";
                    $datosAux[3] = "";  
                    $datosAux[4] = "";
                    $datosAux[5] = "";
                    $datosAux[6] = "";
                    if($i!=1){       
                        if(isset($datos[0]) && $datos[0] != 'Dato de Control' && isset($datos[6]) && $datos[6] != ""){
                            $datosAux[0] = $datos[0];

                            if(isset($datos[1])){
                                $datosAux[1] = $datos[1];
                            }

                            if(isset($datos[2])){
                                $datosAux[2] = $datos[2];
                            }

                            $datosAux[3] = 0;
                            if(isset($datos[3]) && filter_var($datos[3], FILTER_VALIDATE_INT) !== false){
                                $datosAux[3] = $datos[3];
                            }

                            $datosAux[4] = 0;
                            if(isset($datos[4]) && filter_var($datos[4], FILTER_VALIDATE_INT) !== false){
                                $datosAux[4] = $datos[4];
                            }

                            $datosAux[5] = 0;
                            if(isset($datos[5]) && filter_var($datos[5], FILTER_VALIDATE_INT) !== false){
                                $datosAux[5] = $datos[5];
                            }

                            if(isset($datos[6])){
                                $datosAux[6] = $datos[6];
                            }
                            
                            if($datosAux[0] != "" && isset($host[0]) && $host[0] != "" && $datosAux[2] != "" && $datosAux[3] != "" && $datosAux[4] != "" && $datosAux[5] != "" && $datosAux[6] != ""){
                                /*if(!$procesadores->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datosAux[0],$datosAux[1],$datosAux[2],$datosAux[3],$datosAux[4],$datosAux[5],$datosAux[6])){
                                    //echo $procesadores->error;
                                }*/
                                
                                if($j == 0){
                                    $insertarBloque = true;
                                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                    . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                    . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                                } else {
                                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                    . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                    . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                                } 

                                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                                $bloqueValores[":dato_control" . $j] = $datosAux[0];
                                $bloqueValores[":host_name" . $j] = $host[0];
                                $bloqueValores[":tipo_CPU" . $j] = $datosAux[2];
                                $bloqueValores[":cpu" . $j] = $datosAux[3];
                                $bloqueValores[":cores" . $j] = $datosAux[4];
                                $bloqueValores[":procesadores_logicos" . $j] = $datosAux[5];
                                $bloqueValores[":tipo_escaneo" . $j] = $datosAux[6];

                                if($j == $general->registrosBloque){
                                    if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){ 
                                        echo "Procesadores: " . $procesadores->error;
                                    }
                                    $bloque = "";
                                    $bloqueValores = array();
                                    $j = -1;
                                    $insertarBLoque = false; 
                                }
                                $j++;
                            }
                        }
                    }
                    $i++;
                }
                if($insertarBloque === true){
                    if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){   
                        echo "Procesadores: " . $procesadores->error;
                    }
                }
                $exito2 = 1;	
            }
            //fin cargar consolidado procesadores

            //inicio cargar consolidado tipo de equipo
            $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Tipo de Equipo.csv";

            $tipoEquipo->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            if (($fichero = fopen("archivos_csvf1/".$imagen, "r")) !== FALSE) {
                $i=1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                
                while (($datos = fgetcsv($fichero, 2000)) !== FALSE) {
                    if($i!=1){
                        $datosAux[0] = "";
                        $datosAux[1] = "";
                        $datosAux[2] = "";
                        $datosAux[3] = "";  
                        if(isset($datos[0])){
                            $datosAux[0] = $datos[0];
                        }
                        if($datosAux[0] != 'Dato de Control'){
                            if(isset($datos[1])){
                            $datosAux[1] = $datos[1];
                            }
                            if(isset($datos[2])){
                                $datosAux[2] = $datos[2];
                            }
                            if(isset($datos[3])){
                                $datosAux[3] = $datos[3];
                            }

                            if($j == 0){
                            $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dato_control" . $j] = $datosAux[0];
                            $bloqueValores[":host_name" . $j] = $host[0];
                            $bloqueValores[":fabricante" . $j] = $datosAux[2];
                            $bloqueValores[":modelo" . $j] = $datosAux[3];

                            if($j == $general->registrosBloque){
                                if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo "Tipo Equipo: " . $tipoEquipo->error;
                                }
                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                    }
                    $i++;	
                }
                
                if($insertarBloque === true){
                    if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){   
                        echo "Tipo Equipo: " . $tipoEquipo->error;
                    }
                }
                $exito2 = 1;
            }

            //$servidores->eliminarAlineaionWindowServer($_SESSION["client_id"], $_SESSION['client_empleado']);
            //$servidores->eliminarAlineaionSqlServer($_SESSION["client_id"], $_SESSION['client_empleado']);
            //$servidores->eliminarWindowServer1($_SESSION["client_id"], $_SESSION['client_empleado']);
            //$servidores->eliminarSqlServer1($_SESSION["client_id"], $_SESSION['client_empleado']);
            //fin cargar tipo de equipo
            
        }//cago
    }
}

$opcionDespliegue = "";
if(isset($_POST["opcionDespliegue"])){
    $opcionDespliegue = $general->get_escape($_POST["opcionDespliegue"]);
}