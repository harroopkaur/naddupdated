<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_cliente.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_SKU.php");

$virtualMachine = new clase_SPLA_vCPU();
$general = new General();
$clienteSPLA = new clase_SPLA_cliente();
$SKU = new clase_SPLA_SKU();

$exito = 0;
$insertar = 0;
if(isset($_POST["insertar"]) && $_POST["insertar"] == 1){
    $insertar = $_POST["insertar"];
    $id = $_POST["id"];
    $customers = $_POST["customers"];
    $Qty = $_POST["cantidad"];
    $SKUSPLA = $_POST["SKU"];
    $type = $_POST["type"];
    
    $j = 0;
    for($i = 0; $i < count($id); $i++){
        $identificador = 0;
        if(filter_var($id[$i], FILTER_VALIDATE_INT) !== false){
            $identificador = $id[$i];
        }
        
        $custom = $general->get_escape($customers[$i]);
        $cantidad = 0;
        if(filter_var($Qty[$i], FILTER_VALIDATE_INT) !== false){
            $cantidad = $Qty[$i];
        }
        
        $sk = $general->get_escape($SKUSPLA[$i]);
        $tipo = $general->get_escape($type[$i]);
        
        if($virtualMachine->actualizar($identificador, $custom, $cantidad, $sk, $tipo)){
            $j++;
        }
    }
    
    if($i == $j){
        $exito = 1;
    } else if($i > $j && $j > 0){
        $exito = 2;
    }
}

$clientes = $clienteSPLA->clientes($_SESSION["client_id"], $_SESSION["client_empleado"]);
$datacenter = $virtualMachine->datacenter($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listadoSKU = $SKU->SKU($_SESSION["client_id"], $_SESSION["client_empleado"]);
$VMName = $virtualMachine->VM($_SESSION["client_id"], $_SESSION["client_empleado"]);
$total = $virtualMachine->totalRegistrosvCPU1($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "");

if($total < 25){
    $pagina = $total;
}
else{
    $pagina = 25;
}

$listadovCPU = $virtualMachine->listar_todoFiltrado1($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "", 1, $pagina);
$inicio = 0;

if(count($listadovCPU) > 0){
    $inicio = 1;
}