<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_cliente.php");

$clienteSPLA = new clase_SPLA_cliente();
$general = new General();
$exito = 0;
$insertar = 0;

if(isset($_POST["insertar"]) && $_POST["insertar"] == 1){
    $insertar = $_POST["insertar"];
    $id = $_POST["id"];
    $inicioContrato = $_POST["inicioContrato"];
    $finContrato = $_POST["finContrato"];
    
    $j = 0;
    for($i = 0; $i < count($id); $i++){
        $identificador = 0;
        if(filter_var($id[$i], FILTER_VALIDATE_INT) !== false){
            $identificador = $id[$i];
        }
        
        $inicioFecha = "0000-00-00";
        if($general->validarFecha($inicioContrato[$i], "/", "mm/dd/aaaa")){
            $inicioFecha = $general->reordenarFecha($inicioContrato[$i], "/", "-", "mm/dd/YYYY");
        }
        
        $finFecha = "0000-00-00";
        if($general->validarFecha($finContrato[$i], "/", "mm/dd/aaaa")){
            $finFecha = $general->reordenarFecha($finContrato[$i], "/", "-", "mm/dd/YYYY");
        }
        
        if($clienteSPLA->actualizarFechas($identificador, $inicioFecha, $finFecha)){
            $j++;
        }
    }
    
    if($i == $j){
        $exito = 1;
    } else if($i > $j && $j > 0){
        $exito = 2;
    }
}

$clientes = $clienteSPLA->clientes($_SESSION["client_id"], $_SESSION["client_empleado"]);
$contratos = $clienteSPLA->contratos($_SESSION["client_id"], $_SESSION["client_empleado"]);
$fechaInicio = $clienteSPLA->fechasInicio($_SESSION["client_id"], $_SESSION["client_empleado"]);
$total = $clienteSPLA->totalRegistrosClientes1($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "");

if($total < 25){
    $pagina = $total;
}
else{
    $pagina = 25;
}

$listadoClientes = $clienteSPLA->listar_todoFiltrado1($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "", 1, $pagina);
$inicio = 0;
if(count($listadoClientes) > 0){
    $inicio = 1;
}