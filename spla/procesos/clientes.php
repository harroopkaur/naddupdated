<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_cliente.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$general = new General();
$cliente = new clase_SPLA_cliente();
$archivosDespliegue = new clase_archivos_fabricantes();
$log = new log();

$exito=0;
$error=0;

if(isset($_POST['insertar'])) {
    if(isset($_FILES['archivo']) && is_uploaded_file($_FILES['archivo']['tmp_name'])){
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "csv") && ($extension[$long] != "CSV")) { $error = 1; }  
            // Permitir subir solo imagenes JPG,
        }else{
            $error=2;	
        }

        if(!file_exists($GLOBALS['app_root'] . "/spla/archivos_csvf5/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
            mkdir($GLOBALS['app_root'] . "/spla/archivos_csvf5/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
        }

        if($general->obtenerSeparador($temporal_imagen) === true){
            if (($fichero = fopen($temporal_imagen, "r")) !== FALSE) {
                $i=1;
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    if($i == 1 && ($datos[0] != "Nombre Cliente" || $datos[1] != "Contrato #" || $datos[2] != "Inicio de contrato" || 
                    $datos[3] != "Fin de contrato" || $datos[4] != "Licencias Win" || $datos[5] != "Licencias SQL")){
                        $error = 3;
                        break;
                    } else{
                        break;
                    }
                    $i++;
                }
            }
        }else{
            $error = 1; //10 no es el error que debe ir hay que cambiarlo
        }
        
        if($error == 0) {
            $baseDireccion = "archivos_csvf5/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/";
            $nombreArchivo = "vCPU" . date("dmYHis") . ".csv";
            move_uploaded_file($_FILES['archivo']['tmp_name'], 'archivos_csvf5/' . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . '/' . $nombreArchivo); 

            if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 10) === false){
                $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 10);
            }

            $tipoDespliegue = 0;

            $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], 10, $nombreArchivo, $tipoDespliegue);

            $cliente->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

            if($general->obtenerSeparador($baseDireccion . $nombreArchivo) === true){
                if (($fichero = fopen($baseDireccion . $nombreArchivo, "r")) !== FALSE) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {$pruebas = explode(",", $datos[0], -1);                    
                        if($i > 1){
                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :nombreCliente" . $j . ", "
                                . ":contrato" . $j . ", :inicioContrato" . $j . ", :finContrato" . $j . ", :licenciasWin" . $j . ", :licenciasSQL" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :nombreCliente" . $j . ", "
                                . ":contrato" . $j . ", :inicioContrato" . $j . ", :finContrato" . $j . ", :licenciasWin" . $j . ", :licenciasSQL" . $j . ")";
                            } 
                
                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":nombreCliente" . $j] = $general->truncarString($datos[0], 70);
                            $bloqueValores[":contrato" . $j] = $general->truncarString($datos[1], 70);
                            $bloqueValores[":inicioContrato" . $j] = $general->reordenarFecha($datos[2], "/", "-");
                            $bloqueValores[":finContrato" . $j] = $general->reordenarFecha($datos[3], "/", "-");
                            $bloqueValores[":licenciasWin" . $j] = $general->truncarString($datos[4], 70);
                            $bloqueValores[":licenciasSQL" . $j] = $general->truncarString($datos[5], 70);
                            
                            if($j == $general->registrosBloque){
                                if(!$cliente->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo $cliente->error."<br>";
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $exito=1;	

                        $i++; 
                    }

                    if($insertarBloque === true){
                        if(!$cliente->insertarEnBloque($bloque, $bloqueValores)){    
                            echo $cliente->error."<br>";
                        }
                    }
                }
            }
          
            $exito=1;  
            
            $log->insertar(18, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
        }
    }else {
        $error = 4;
    }
}