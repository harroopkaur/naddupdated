<?php
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

$virtualMachine = new clase_SPLA_vCPU();
$general = new General();

$clusters = $virtualMachine->clusters($_SESSION["client_id"], $_SESSION["client_empleado"]);
$hosts = $virtualMachine->hosts($_SESSION["client_id"], $_SESSION["client_empleado"]);
$total = $virtualMachine->totalRegistrosvCPU($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "");

if($total < 25){
        $pagina = $total;
}
else{
        $pagina = 25;
}

$listadovCPU = $virtualMachine->listar_todoFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "", 0, $pagina);
$inicio = 0;
if(count($listadovCPU) > 0){
    $inicio = 1;
}