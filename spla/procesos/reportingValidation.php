<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");

$virtualMachine = new clase_SPLA_vCPU();
$general = new General();

$exito = 0;
$insertar = 0;

$listadoProductos = $virtualMachine->productos($_SESSION["client_id"], $_SESSION["client_empleado"]); 
$total = $virtualMachine->reportingValidationTotal($_SESSION["client_id"], $_SESSION["client_empleado"], "");

if($total < 25){
    $pagina = $total;
}
else{
    $pagina = 25;
}

$listadoResumen = $virtualMachine->reportingValidationFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], "", 1, $pagina);
$inicio = 0;
if(count($listadoResumen) > 0){
    $inicio = 1;
}