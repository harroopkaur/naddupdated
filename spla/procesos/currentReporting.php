<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_cliente.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_SKU.php");

$virtualMachine = new clase_SPLA_vCPU();
$general = new General();
$resumen = new Resumen_SPLA();
$clienteSPLA = new clase_SPLA_cliente();
$SKU = new clase_SPLA_SKU();

$exito = 0;
$insertar = 0;

if(isset($_POST["insertar"]) && $_POST["insertar"] == 1){
    $insertar = $_POST["insertar"];
    $id = $_POST["id"];
    $QtySale = $_POST["QtySale"];
    $priceSale = $_POST["priceSale"];
    $tipoApp = $_POST["tipo"];
    
    $j = 0;
    for($i = 0; $i < count($id); $i++){
        $identificador = 0;
        if(filter_var($id[$i], FILTER_VALIDATE_INT) !== false){
            $identificador = $id[$i];
        }
       
        $Qty = 0;
        if(filter_var($QtySale[$i], FILTER_VALIDATE_INT) !== false){
            $Qty = $QtySale[$i];
        }
        
        $price = 0;
        if(filter_var($priceSale[$i], FILTER_VALIDATE_FLOAT) !== false){
            $price = $priceSale[$i];
        }
        
        $tipo = $general->get_escape($tipoApp[$i]);
        
        if($tipo == "VM"){
            if($virtualMachine->actualizarPriceSale($identificador, $Qty, $price)){
                $j++;
            }
        } else{
            if($resumen->actualizarPriceSale($identificador, $Qty, $price)){
                $j++;
            }
        }
    }
    
    if($i == $j){
        $exito = 1;
    } else if($i > $j && $j > 0){
        $exito = 2;
    }
}

$clientes = $clienteSPLA->clientes($_SESSION["client_id"], $_SESSION["client_empleado"]);
//$listadoProductos = $virtualMachine->productos($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listadoSKU = $SKU->SKU($_SESSION["client_id"], $_SESSION["client_empleado"]);
$total = $virtualMachine->currentReportingTotal($_SESSION["client_id"], $_SESSION["client_empleado"], "", "");

if($total < 25){
    $pagina = $total;
}
else{
    $pagina = 25;
}

$listadoResumen = $virtualMachine->currentReportingFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", 1, $pagina);
$inicio = 0;
if(count($listadoResumen) > 0){
    $inicio = 1;
}