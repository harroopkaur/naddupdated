<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");

$compra_datos   = new Compras_SPLA();
$compra_datos2  = new Compras_SPLA();
$resumen_o      = new Resumen_SPLA();
$resumen_o2     = new Resumen_SPLA();
$resumen_s      = new Resumen_SPLA();
$resumen_s2     = new Resumen_SPLA();
$resumen_ot     = new Resumen_SPLA();
$resumen_ot2    = new Resumen_SPLA();
$balance_datos  = new Balance_SPLA();
$balance_datos2 = new Balance_SPLA();
$office         = new Equivalencias();
$office2        = new Equivalencias();
$general        = new General();
$detalle        = new Detalles_SPLA();
$nueva_funcion = new funcionesSam();

$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 10);

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if(isset($_POST['insertarRepoF']) && $_POST["insertarRepoF"] == 1) {

    if ($error == 0) {

        $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $balance_datos2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

        $tablaCompraRepo = array();
        if (isset($_POST["check"])) {
            $tablaCompraRepo = $_POST["check"];

            for ($i = 0; $i < count($tablaCompraRepo); $i++) {
                $newArray = explode("*", $tablaCompraRepo[$i]);
                $familia  = $general->get_escape($newArray[0]);
                $edicion  = $general->get_escape($newArray[1]);
                $version  = $general->get_escape($newArray[2]);
                $cantidad = 0;
                if(filter_var($newArray[3], FILTER_VALIDATE_FLOAT) !== false){
                    $cantidad = $newArray[3];
                }
                $precio = 0;
                if(filter_var($newArray[4], FILTER_VALIDATE_FLOAT) !== false){
                    $precio = $general->get_escape($newArray[4]);
                }
                if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], trim($familia), trim($edicion), trim($version), $cantidad, $precio)) {
                    echo $compra_datos->error;
                } else {

                    //$i++;

                    $exito = 1;
                }//exito*/
            }//WhILE
        }
    }

    if ($exito == 1) {

        $lista_compras = $compra_datos2->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $lista_office = $office->listar_todo(3); //3 es el id fabricante Microsoft
        if ($lista_office) {
            foreach ($lista_office as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;

                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
                
                if ($compra_datos2->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion) > 0) {
                    $compra = $compra_datos2->compra;
                    $precio = $compra_datos2->precio;
                }
                
                if (($newFamilia == 'Office') || ($newFamilia == 'Visio') || ($newFamilia == 'Project') || ($newFamilia == 'Visual Studio')) {
                    $instalaciones = $resumen_o->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);                    
                }//resumen offices

                if (($newFamilia == 'Exchange Server') || ($newFamilia == 'Sharepoint Server') || ($newFamilia == 'Skype for Business') /* ||($newFamilia=='Windows Server') */ || ($newFamilia == 'SQL Server')) {
                    $instalaciones = $resumen_s->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                }//resumen offices

                if (($newFamilia == 'System Center')) {
                    $instalaciones = $resumen_ot->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                }//resumen offices
                
                //inicio Windows OS
                if (($newFamilia == 'Windows')) {
                    $instalaciones = $detalle->listar_WindowsOS($_SESSION['client_id'], $_SESSION["client_empleado"], $newEdicion, $newVersion);
                }//fin Windows OS 
                
                //inicio Windows Server
                if (($newFamilia == 'Windows Server')) {
                    $instalaciones = $detalle->listar_WindowsServer($_SESSION['client_id'], $_SESSION["client_empleado"], $newEdicion, $newVersion);
                }//fin Windows Server 

                $balancec1 = $compra - $instalaciones;
                //$balancec2=$balancec1*$reg_o->precio;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                if (strpos($newFamilia, "Windows Server") !== false) {
                    $tipo = 2;
                } else if (strpos($newFamilia, "Windows") !== false) {
                    $tipo = 1;
                } else if ($balancec2 < 0) {
                    $tipo = 2;
                } else {
                    $tipo = 1;
                }
               
                //$balance_datos->insertar($_SESSION['client_id'],$newFamilia,$newEdicion, $newVersion,$reg_o->precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
            }//office
        }//office

        $exito2 = 1;
    }
}