<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_SPLA_historico.php");

$virtualMachine = new clase_SPLA_historico();
$general = new General();

$exito = 0;
$insertar = 0;

$fechaInicio = date("Y") . "-01-01";
$fechaFin = date("Y-m-d");

$listadoProductos = $virtualMachine->ProductoHistorical($_SESSION["client_id"]); 
$total = $virtualMachine->reportHistoricalAppsTotal($_SESSION["client_id"], "", $fechaInicio, $fechaFin);

if($total < 25){
    $pagina = $total;
}
else{
    $pagina = 25;
}

$listadoResumen = $virtualMachine->reportHistoricalAppsPaginado($_SESSION["client_id"], "", $fechaInicio, $fechaFin, 1, 25);
$inicio = 0;
if(count($listadoResumen) > 0){
    $inicio = 1;
}

$mesInicio = date("n", strtotime($fechaInicio)); 
$mesFin = date("n", strtotime($fechaFin));
$yearInicio = date("Y", strtotime($fechaInicio)); 
$yearFin = date("Y", strtotime($fechaFin));

if($yearFin > $yearInicio){
    $mesFin += 12;
}

$mesFin++;

$celdas = "";
$arrayCeldas = array();
$j = 0;
for($index = $mesInicio; $index < $mesFin; $index++){
    $valor = $general->completarCeroNumeros($index);
    $campo = $valor . '/'.  $yearInicio;
    if($index > 12){
        $campo = $valor . '/'.  $yearFin;
    }
    //$celdas .= '<th style="text-align:center; border: 1px solid #000000; cursor:pointer;">' . $campo . '</th>';
    $arrayCeldas[$j] = '<th style="text-align:center; border: 1px solid #000000; cursor:pointer;">' . $campo . '</th>';
    $j++;
}

$celdasReversed = array_reverse($arrayCeldas, false);
for($z = 0; $z < count($arrayCeldas); $z++){
    $celdas .= $celdasReversed[$z];
}   