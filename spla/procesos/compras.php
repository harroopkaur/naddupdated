<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");

$compra_datos   = new Compras_SPLA();
$compra_datos2  = new Compras_SPLA();
$resumen_o      = new Resumen_SPLA();
$resumen_o2     = new Resumen_SPLA();
$resumen_s      = new Resumen_SPLA();
$resumen_s2     = new Resumen_SPLA();
$resumen_ot     = new Resumen_SPLA();
$resumen_ot2    = new Resumen_SPLA();
$balance_datos  = new Balance_SPLA();
$balance_datos2 = new Balance_SPLA();
$office         = new Equivalencias();
$office2        = new Equivalencias();
$general        = new General();
$detalle        = new Detalles_SPLA();
$archivosDespliegue   = new clase_archivos_fabricantes();

$nueva_funcion = new funcionesSam($conn);
$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 10);

$exito=0;
$error=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;

if(isset($_POST['insertar']) && $_POST["insertar"] == 1) {
	
    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];

    // Validaciones
    $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
    $long = count($extension) - 1;
    if(($extension[$long] != "csv") && ($extension[$long] != "CSV")) { $error = 1; }  // Permitir subir solo imagenes JPG,

        if($error == 0) {

        $imagen=$_SESSION['client_id'] . $_SESSION["client_empleado"] . "_compraSPLA" . date("dmYHis") . ".csv";

        if(!$compra_datos->cargar_archivo($imagen, $temporal_imagen)) { $error = 5; } else{

            $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            $balance_datos2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

            if($general->obtenerSeparador("archivos_csvf4/" . $imagen) === true){
                if (($fichero = fopen("archivos_csvf4/".$imagen, "r")) !== FALSE) {
                    $i=1;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i == 1 && ($datos[0] != "Producto" || utf8_encode($datos[1]) != "Edición" || utf8_encode($datos[2]) != "Versión" || 
                        $datos[3] != "SKU" || $datos[4] != "Tipo" || $datos[5] != "Cantidades" || $datos[6] != "Precio Unitario" ||
                        utf8_encode($datos[7]) != "Asignación")){
                            $error = 3;
                            unlink("archivos_csvf4/".$imagen);
                            break;
                        }

                        if($datos[0] == "Sistema Operativo"){
                            $newFamilia = "Windows";
                        }
                        else{
                            $newFamilia = $general->get_escape($datos[0]);
                        }
                        
                        $cantidad = 0;
                        if(filter_var($datos[5], FILTER_VALIDATE_INT) !== false){
                            $cantidad = $datos[5];
                        }
                        
                        $precio = 0;
                        if(filter_var($datos[6], FILTER_VALIDATE_FLOAT) !== false){
                            $precio = $datos[6];
                        }

                        //if(!$compra_datos->insertar($_SESSION['client_id'],$newFamilia,$datos[1],$datos[2],$datos[3], 0)){
                        if(!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $general->get_escape($datos[1]), $general->get_escape($datos[2]),$cantidad, $precio)){
                            echo $compra_datos->error;
                        }
                        else{

                            $i++;

                            $exito=1;
                        }//exito
                    }//WhILE
                     
                    if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 10) === false){
                        $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 10);
                    }
                    $archivosDespliegue->actualizarCompras($_SESSION["client_id"], $_SESSION["client_empleado"], 10, $imagen);
                }//FICHERO
            }
            else{
                $error = 4;
            }
        }//cago
    }//is error
	
    if($exito==1){

        $lista_compras=$compra_datos2->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $lista_office=$office->listar_todo(3);//3 es el id fabricante Microsoft

        if($lista_office){
            foreach($lista_office as $reg_o){
                $instalaciones=0;
                $compra = 0;
                
                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
                
                if($compra_datos2->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion) > 0){
                    $compra=$compra_datos2->compra;
                    $precio=$compra_datos2->precio;
                }

                if(($newFamilia=='Office')||($newFamilia=='Visio')||($newFamilia=='Project')||($newFamilia=='Visual Studio')){
                    $instalaciones = $resumen_o->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                }//resumen offices

                if(($newFamilia=='Exchange Server')||($newFamilia=='Sharepoint Server')||($newFamilia=='Skype for Business') /*||($newFamilia=='Windows Server')*/ ||($newFamilia=='SQL Server')){
                    $instalaciones = $resumen_s->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                }//resumen offices

                if(($newFamilia=='System Center')){
                    if(strpos($newEdicion, "Data Warehouse") !== false){
                        $instalaciones = $resumen_ot->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                    }
                    else{
                        $instalaciones = $resumen_ot->listar_datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, "Data Warehouse", $newVersion);
                    }
                }//resumen offices
                
                //inicio Windows OS
                if(($newFamilia=='Windows')){
                    $instalaciones = $detalle->listar_WindowsOS($_SESSION['client_id'], $_SESSION["client_empleado"], $newEdicion , $newVersion);
                }//fin Windows OS 
                
                //inicio Windows Server
                if(($newFamilia=='Windows Server')){
                   $instalaciones = $detalle->listar_WindowsServer($_SESSION['client_id'], $_SESSION["client_empleado"], $newEdicion , $newVersion);
                }//fin Windows Server 

                if($precio == 0 || $precio == ""){
                    $precio = $reg_o["precio"];
                }
                
                $balancec1=$compra-$instalaciones;
                $balancec2=$balancec1*$precio;

                //if($balancec2<0){
                if(strpos($newFamilia, "Windows Server") !== false){
                    $tipo=2;	
                }
                else if(strpos($newFamilia, "Windows") !== false){
                    $tipo=1;	
                }
                else if($balancec2<0){
                    $tipo=2;	
                }else{
                    $tipo=1;	
                }

                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia,$newEdicion, $newVersion,$precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);


            }//office
        }//office
        $exito2=1;
    }	
}