<?php
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

$virtualMachine = new clase_SPLA_vCPU();
$general = new General();
$exito = 0;
$insertar = 0;

if(isset($_POST["insertar"]) && $_POST["insertar"] == 1){
    $insertar = $_POST["insertar"];
    $id = $_POST["id"];
    $type = $_POST["tipo"];
    $VM = $_POST["VM"];
    $licenciasWin = $_POST["licenciasWin"];
    $licenciasSQL = $_POST["licenciasSQL"];
    $j = 0;
    for($i = 0; $i < count($type); $i++){
        if($clienteSPLA->actualizar($id[$i], $general->get_escape($type[$i]),
        $general->get_escape($VM[$i]), $general->get_escape($licenciasWin[$i]), $general->get_escape($licenciasSQL[$i]))){
            $j++;
        }
    }
    
    if($i == $j){
        $exito = 1;
    } else if($i > $j && $j > 0){
        $exito = 2;
    }
}

$clusters = $virtualMachine->clusters($_SESSION["client_id"], $_SESSION["client_empleado"]);
$hosts = $virtualMachine->hosts($_SESSION["client_id"], $_SESSION["client_empleado"]);
$total = $virtualMachine->totalRegistrosResumen($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "");

if($total < 25){
        $pagina = $total;
}
else{
        $pagina = 25;
}

$listadoResumen = $virtualMachine->resumenFiltrado($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", "", 0, $pagina);
$inicio = 0;
if(count($listadoResumen) > 0){
    $inicio = 1;
}