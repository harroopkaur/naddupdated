<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_SPLA.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_SPLA.php");

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$balanceg  = new Balance_SPLA();
$balance4g = new Balance_SPLA();
$balance2g = new Balance_SPLA();
$balance3g = new Balance_SPLA();
$detalles1 = new Detalles_SPLA();
$detalles2 = new Detalles_SPLA();
$resumen   = new Resumen_SPLA();
$general = new General();

$total_1LAc  = 0;
$total_2DVc  = 0;
$total_1LAs  = 0;
$total_2DVs  = 0;
$total_1     = 0;

$total_1LAc2 = 0;
$total_2DVc2 = 0;
$total_1LAs2 = 0;
$total_2DVs2 = 0;
$total_2     = 0;


$total_1LAc3 = 0;
$total_2DVc3 = 0;
$total_1LAs3 = 0;
$total_2DVs3 = 0;
$total_3     = 0;
$tclient     = 0;
$tclient2    = 0;
$tclient3    = 0;
$tserver     = 0;
$tserver3    = 0;

$uso1        = 0;
$nouso1      = 0;
$uso2        = 0;
$nouso2      = 0;
$uso3        = 0;
$nouso3      = 0;

$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';

if(isset($_GET['vert']) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false){
    $vert = $_GET['vert'];
}
else{
    $vert = 0;
}

if($vert==0){
		
    $listar_equipos=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise');
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==1){//cliente
                $tclient=($tclient+1);
                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAc=($total_1LAc+1);
                }

                if($reg_equipos["rango"]==1){
                    $total_2DVc=($total_2DVc+1);

                    }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVc=($total_2DVc+1);

                }
                $total_1=($total_1+1);
            }
        }

        $uso1=$total_2DVc;
        $nouso1=$total_1-$uso1;
    }//1
		
    $listar_equipos2=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'professional');
    if($listar_equipos2){
        foreach($listar_equipos2 as $reg_equipos2){

            if($reg_equipos2["tipo"]==1){//cliente
                $tclient2=($tclient2+1);
                if($reg_equipos2["errors"]=='Ninguno'){
                    $total_1LAc2=($total_1LAc2+1);
                }

                if($reg_equipos2["rango"]==1){
                    $total_2DVc2=($total_2DVc2+1);

                }else if($reg_equipos2["rango"]==2 || $reg_equipos2["rango"]==3){
                    $total_2DVc2=($total_2DVc2+1);

                }
                $total_2=($total_2+1);
            }
        }
        
        $uso2=$total_2DVc2;
        $nouso2=$total_2-$uso2;
    }//2
		
    $listar_equipos3=$detalles1->listar_todog2($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise','professional');
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==1){//cliente
                    $tclient3=($tclient3+1);
                    if($reg_equipos3["errors"]=='Ninguno'){
                        $total_1LAc3=($total_1LAc3+1);
                    }

                    if($reg_equipos3["rango"]==1){
                        $total_2DVc3=($total_2DVc3+1);

                        }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                        $total_2DVc3=($total_2DVc3+1);

                    }
                $total_3=($total_3+1);
            }
        }

        $uso3=$total_2DVc3;
        $nouso3=$total_3-$uso3;
    }//3
}//ver 0

//ver 1
if($vert==1){
    $listar_equipos=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise');
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==1){//cliente
                $tclient=($tclient+1);
                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAc=($total_1LAc+1);
                }

                if($reg_equipos["rango"]==1){
                    $total_2DVc=($total_2DVc+1);

                    }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVc=($total_2DVc+1);

                }
                $total_1=($total_1+1);
            }
            
        }

        $uso1=$total_2DVc;
        $nouso1=$total_1-$uso1;
    }//1
}//ver 1

//ver 2
if($vert==2){
    $listar_equipos=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'professional');
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==1){//cliente
                $tclient=($tclient+1);
                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAc=($total_1LAc+1);
                }

                if($reg_equipos["rango"]==1){
                    $total_2DVc=($total_2DVc+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVc=($total_2DVc+1);

                }
                $total_1=($total_1+1);
            }
        }
        
        $uso1=$total_2DVc;
        $nouso1=$total_1-$uso1;
    }//1
}// ver 2

//ver 3
if($vert==3){
    $listar_equipos3=$detalles1->listar_todog2($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise','professional');
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==1){//cliente
                $tclient3=($tclient3+1);
                if($reg_equipos3["errors"]=='Ninguno'){
                    $total_1LAc3=($total_1LAc3+1);
                }

                if($reg_equipos3["rango"]==1){
                    $total_2DVc3=($total_2DVc3+1);

                    }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                    $total_2DVc3=($total_2DVc3+1);

                }
                $total_3=($total_3+1);
            }
        }
        $uso3=$total_2DVc3;
        $nouso3=$total_3-$uso3;
    }
}//ver 3

$total_i=0;
//ver 5
if($vert==5){
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office','');

   if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }		
}//ver 5

//ver 51
if($vert==51){
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office','Standard');

   if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }		
}//ver 51

//ver 52
if($vert==52){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office','Professional');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }	
}//ver 52

//ver 53
if($vert==53){	
    $lista_calculo=$balance4g->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], 'Office','Standard','Professional');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 53

//ver 6
if($vert==6){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 6

//ver 61
if($vert==61){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','Standard');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 61

//ver 62
if($vert==62){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','Professional');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 62

//ver 63
if($vert==63){	
    $lista_calculo=$balance4g->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], 'Project','Professional','Standard');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 63

//ver 7
if($vert==7){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Visio','');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 7

//ver 71
if($vert==71){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Visio','Standard');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 71

//ver 72
if($vert==72){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'Visio','Professional');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 72

//ver 73
if($vert==73){	
    $lista_calculo=$balance4g->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], 'Visio','Professional','Standard');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }	
}//ver 73

//ver 8
if($vert==8){	
    $listar_equipos=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise');
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==2){//cliente

                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAs=($total_1LAs+1);
                }

                $tserver=($tserver+1);

                if($reg_equipos["rango"]==1){
                    $total_2DVs=($total_2DVs+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVs=($total_2DVs+1);
                }

                $total_1=($total_1+1);
            }

        }

        $uso1=$total_2DVs;
        $nouso1=$total_1-$uso1;	
    }//1

    $listar_equipos2=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'professional');
    if($listar_equipos2){
        foreach($listar_equipos2 as $reg_equipos2){

            if($reg_equipos2["tipo"]==2){//cliente					
                if($reg_equipos2["errors"]=='Ninguno'){
                    $total_1LAs2=($total_1LAs2+1);
                }

                $tserver2=($tserver2+1);

                if($reg_equipos2["rango"]==1){
                    $total_2DVs2=($total_2DVs2+1);

                }else if($reg_equipos2["rango"]==2 || $reg_equipos2["rango"]==3){
                    $total_2DVs2=($total_2DVs2  +1);
                }

                $total_2=($total_2+1);
            }
        }
        $uso2=$total_2DVs2;
        $nouso2=$total_2-$uso2;
    }//2

    $listar_equipos3=$detalles1->listar_todog2($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise','professional');
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==2){//cliente					
                if($reg_equipos3["errors"]=='Ninguno'){
                    $total_1LAs3=($total_1LAs3+1);
                }

                $tserver3=($tserver3+1);

                if($reg_equipos3["rango"]==1){
                    $total_2DVs3=($total_2DVs3+1);

                }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                    $total_2DVs3=($total_2DVs3+1);
                }

                $total_3=($total_3+1);	
            }
        }
        $uso3=$total_2DVs3;
        $nouso3=$total_3-$uso3;


    }//3
}//ver 8

//ver 82
if($vert==82){		
    $listar_equipos=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise');
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==2){//cliente

                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAs=($total_1LAs+1);
                }

                $tserver=($tserver+1);

                if($reg_equipos["rango"]==1){
                    $total_2DVs=($total_2DVs+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVs=($total_2DVs+1);
                }

                $total_1=($total_1+1);
            }


        }

        $uso1=$total_2DVs;
        $nouso1=$total_1-$uso1;


    }//1
}//ver 82

//ver 83
if($vert==83){
				
    $listar_equipos=$detalles1->listar_todog1($_SESSION['client_id'], $_SESSION["client_empleado"], 'Professional');
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==2){//cliente

                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAs=($total_1LAs+1);
                }

                $tserver=($tserver+1);

                if($reg_equipos["rango"]==1){
                    $total_2DVs=($total_2DVs+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVs=($total_2DVs+1);
                }

                $total_1=($total_1+1);
            }


        }

        $uso1=$total_2DVs;
        $nouso1=$total_1-$uso1;


    }//1
}//ver 83

//ver 84
if($vert==84){
    $listar_equipos3=$detalles1->listar_todog2($_SESSION['client_id'], $_SESSION["client_empleado"], 'enterprise','professional');
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==2){//cliente

                if($reg_equipos3["errors"]=='Ninguno'){
                    $total_1LAs3=($total_1LAs3+1);
                }

                $tserver3=($tserver3+1);

                if($reg_equipos3["rango"]==1){
                    $total_2DVs3=($total_2DVs3+1);

                }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                    $total_2DVs3=($total_2DVs3+1);
                }

                $total_3=($total_3+1);
            }


        }

        $uso3=$total_2DVs3;
        $nouso3=$total_3-$uso3;

    }
}//ver 84

//ver 9
if($vert==9){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 9

//ver 91
if($vert==91){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Standard');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }
    }
}//ver 91

//ver 92
if($vert==92){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Datacenter');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 92

//ver 93
if($vert==93){	
    $lista_calculo=$balance4g->listar_todo_familias($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Enterprise');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 93

//ver 94
if($vert==94){
    $lista_calculo=$balance4g->listar_todo_familias3($_SESSION['client_id'], $_SESSION["client_empleado"], 'SQL Server','Standard','Datacenter','Enterprise');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 94

//ver 10
if($vert==10){
		
    $lista_calculo=$balance4g->listar_todo_familias4($_SESSION['client_id'], $_SESSION["client_empleado"], 'Visual Studio');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 10

//ver 11
if($vert==11){
		
    $lista_calculo=$balance4g->listar_todo_familias4($_SESSION['client_id'], $_SESSION["client_empleado"], 'Exchange Server');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 11

//ver 12
if($vert==12){
		
    $lista_calculo=$balance4g->listar_todo_familias4($_SESSION['client_id'], $_SESSION["client_empleado"], 'Sharepoint Server');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 12

//ver 13
if($vert==13){
		
    $lista_calculo=$balance4g->listar_todo_familias4($_SESSION['client_id'], $_SESSION["client_empleado"], 'Skype for Business');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 13

//ver 14
if($vert==14){
		
    $lista_calculo=$balance4g->listar_todo_familias4($_SESSION['client_id'], $_SESSION["client_empleado"], 'System Center');

    if($lista_calculo){
        foreach($lista_calculo as $reg_calculo){
            $total_i += $reg_calculo["instalaciones"];
        }	
    }
}//ver 14