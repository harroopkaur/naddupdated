<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_spla_vCPU.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$general = new General();
$virtualMachine = new clase_SPLA_vCPU();
$archivosDespliegue = new clase_archivos_fabricantes();
$log = new log();

$exito=0;
$error=0;

if(isset($_POST['insertar'])) {
    if(isset($_FILES['archivo']) && is_uploaded_file($_FILES['archivo']['tmp_name'])){
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "csv") && ($extension[$long] != "CSV")) { $error = 1; }  
            // Permitir subir solo imagenes JPG,
        }else{
            $error=2;	
        }

        if(!file_exists($GLOBALS['app_root'] . "/spla/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
            mkdir($GLOBALS['app_root'] . "/spla/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
        }

        if($general->obtenerSeparador($temporal_imagen) === true){
            if (($fichero = fopen($temporal_imagen, "r")) !== FALSE) {
                $i=1;
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    /*if($i == 1 && ($datos[0] != "VM" || $datos[3] != "CPUs" || $datos[4] != "Sockets" || 
                    $datos[5] != "Cores p/s" || $datos[18] != "Datacenter" || $datos[19] != "Cluster" || 
                    $datos[20] != "Host" || $datos[22] != "OS" )){*/
                    
                    if($i == 1 && ($datos[0] != "Datacenter" || $datos[1] != "Cluster" || $datos[2] != "Host" || 
                    $datos[3] != "VM" || $datos[4] != "OS" || $datos[5] != "Sockets" || 
                    $datos[6] != "Cores p/s" || $datos[7] != "CPUs" || $datos[8] != "Host Date" || $datos[9] != "VM Date")){
                        $error = 3;
                        break;
                    } else{
                        break;
                    }
                    $i++;
                }
            }
        }else{
            $error = 1; //10 no es el error que debe ir hay que cambiarlo
        }
        
        if($error == 0) {
            $baseDireccion = "archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/";
            $nombreArchivo = "vCPU" . date("dmYHis") . ".csv";
            move_uploaded_file($_FILES['archivo']['tmp_name'], $baseDireccion . $nombreArchivo); 

            if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 10) === false){
                $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 10);
            }

            $tipoDespliegue = 0;

            $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], 10, $nombreArchivo, $tipoDespliegue);

            $virtualMachine->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

            if($general->obtenerSeparador($baseDireccion . $nombreArchivo) === true){
                if (($fichero = fopen($baseDireccion . $nombreArchivo, "r")) !== FALSE) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {$pruebas = explode(",", $datos[0], -1);                    
                        if($i > 1){
                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :DC" . $j . ", :cluster" . $j . ", "
                                . ":host" . $j . ", :VM" . $j . ", :edicion" . $j . ", :sockets" . $j . ", :cores" . $j . ", "
                                . ":cpu" . $j . ", :hostDate" . $j . ", :VMDate" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :DC" . $j . ", :cluster" . $j . ", "
                                . ":host" . $j . ", :VM" . $j . ", :edicion" . $j . ", :sockets" . $j . ", :cores" . $j . ", "
                                . ":cpu" . $j . ", :hostDate" . $j . ", :VMDate" . $j . ")";
                            } 
                            
                            $host=explode('.', $datos[2]);
                            if(filter_var($datos[2], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[2];
                            }

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":DC" . $j] = $general->truncarString($datos[0], 70);
                            $bloqueValores[":cluster" . $j] = $general->truncarString($datos[1], 70);
                            $bloqueValores[":host" . $j] = $general->truncarString($host[0], 70);
                            $bloqueValores[":VM" . $j] = $general->truncarString($datos[3], 70);
                            $bloqueValores[":edicion" . $j] = $general->truncarString($datos[4], 70);
                            $bloqueValores[":sockets" . $j] = 0;
                            if(filter_var($datos[5], FILTER_VALIDATE_INT) !== false){
                                $bloqueValores[":sockets" . $j] = $datos[5];
                            }
                            
                            $bloqueValores[":cores" . $j] = 0;
                            if(filter_var($datos[6], FILTER_VALIDATE_INT) !== false){
                                $bloqueValores[":cores" . $j] = $datos[6];
                            }
                            
                            $bloqueValores[":cpu" . $j] = 0;
                            if(filter_var($datos[7], FILTER_VALIDATE_INT) !== false){
                                $bloqueValores[":cpu" . $j] = $datos[7];
                            }
                            
                            $bloqueValores[":hostDate" . $j] = null;
                            if($datos[8] != ""){
                                $bloqueValores[":hostDate" . $j] = $general->reordenarFecha($datos[8], "/");
                            }
                            
                            $bloqueValores[":VMDate" . $j] = null;
                            if($datos[9] != ""){
                                $bloqueValores[":VMDate" . $j] = $general->reordenarFecha($datos[9], "/");
                            }
                            
                            if($j == $general->registrosBloque){
                                if(!$virtualMachine->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo $virtualMachine->error."<br>";
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $exito=1;	

                        $i++; 
                    }

                    if($insertarBloque === true){
                        if(!$virtualMachine->insertarEnBloque($bloque, $bloqueValores)){    
                            echo $virtualMachine->error."<br>";
                        }
                    }
                }
            }
          
            $exito=1;  
            
            $log->insertar(17, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
        }
    }else {
        $error = 4;
    }
}