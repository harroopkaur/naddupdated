<div style="width:98%; padding:10px; overflow:hidden;">
    <?php 
    if($_SESSION["idioma"] == 1){
    ?>
        <h1 class="textog negro" style="margin:20px; text-align:center;">Administraci&oacute;n de Licenciamiento</h1>
        <p style="margin-top:20px; font-size:24px; text-align:center;">Administra tus activos de software en 3 simples pasos</p>
    <?php 
    } else{
    ?>
        <h1 class="textog negro" style="margin:20px; text-align:center;">SPLA Administration</h1>
        <p style="margin-top:20px; font-size:24px; text-align:center;">Step 1 & 2: Discovery & Validation - Customers</p>
    <?php
    }
    ?>
    <br>
    
    <?php 
    if($_SESSION["idioma"] == 1){
    ?>
        <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/imgGeneral.png" style="margin: 0 auto; width: 85%;display: block;"/>
    <?php 
    } else{
    ?>  
        <div style="width:100%; overflow:hidden; margin:0 auto;">
            <div style="width:33%; overflow:hidden; float:left;">
                <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/imgSPLADownload.png" style="margin: 0 auto; height:270px; width:auto; display: block;"/>
                <div class="botonesSPLA boton5 <?php if(!$moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 19)){ echo "hide"; } ?>" onclick="location.href='customersInput.php';">Customer Input</div>
            </div>
            <div style="width:33%; overflow:hidden; float:left;">
                <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/SPLAUploadCustomers.png" style="margin: 0 auto; height:270px; width:auto; display: block;"/>
                <div class="botonesSPLA boton5 <?php if(!$moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 19)){ echo "hide"; } ?>" onclick="location.href='customersInput.php';">Customer Input</div>
            </div>
            <div style="width:33%; overflow:hidden; float:left;">
                <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/SPLAValidateCustomers.png" style="margin: 0 auto; height:270px; width:auto; display: block;"/>
                <div class="botonesSPLA boton5 <?php if(!$moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 19)){ echo "hide"; } ?>" onclick="location.href='customersValidation.php';">Customer Validation</div>
            </div>
        </div>
    <?php
    }
    ?>
    <br>
</div>