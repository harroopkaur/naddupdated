<div style="width:98%; padding:10px; overflow:hidden;">
    <?php 
    if($_SESSION["idioma"] == 1){
    ?>
        <h1 class="textog negro" style="margin:20px; text-align:center;">Administraci&oacute;n de Licenciamiento</h1>
        <p style="margin-top:20px; font-size:24px; text-align:center;">Administra tus activos de software en 3 simples pasos</p>
    <?php 
    } else{
    ?>
        <h1 class="textog negro" style="margin:20px; text-align:center;">Software Provider Licensing Agreement</h1>
        <p style="margin-top:20px; font-size:24px; text-align:center;">Manage your Software services in 3 simple steps</p>
    <?php
    }
    ?>
    <br>
    
    <?php 
    if($_SESSION["idioma"] == 1){
    ?>
        <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/imgGeneral.png" style="margin: 0 auto; width: 85%;display: block;"/>
    <?php 
    } else{
    ?>  <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/portadaSPLA.png" style="margin: 0 auto; width: 90%;display: block;"/>
        
    <?php
    }
    ?>
    <br>
    
    <?php 
    if($_SESSION["idioma"] == 1){
    ?>
        <div style="float:right;"><div class="botones_m2" id="boton1" onClick="location.href='despliegue1.php';">Despliegue</div></div>
        <div style="float:right;"><div class="botones_m2Alterno boton1" id="limpiar">Limpiar Despliegue</div></div>
    <?php
    } else{
    ?>
        <div style="float:right;"><div class="botones_m2 boton5" onClick="location.href='customersInput.php';">Customer Input</div></div>
        <div style="float:right;"><div class="botones_m2Alterno boton5" id="limpiar">Clean Deployment</div></div>
        <div style="float:right;"><div class="botones_m2Alterno boton5" id="guardarHistorico">Save Historical</div></div>
    <?php
    }
    ?>
</div>

<script>
    $(document).ready(function(){
        $("#limpiar").click(function(){
            $("#fondo").show();
            $.post("ajax/limpiarDespliegue.php", { token : localStorage.licensingassuranceToken }, function (data) {
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>

                if(data[0].result === 0){
                    $.alert.open('warning', "Could not remove deployment");
                } else if(data[0].result === 1){
                    $.alert.open('info', "Successfully deleted deployment");
                } else{
                    $.alert.open('warning', "The deployment was not completely eliminated");
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#guardarHistorico").click(function(){
            $("#fondo").show();
            $.post("ajax/guardarHistorico.php", { token : localStorage.licensingassuranceToken }, function (data) {
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>

                if(data[0].result === 0){
                    $.alert.open('warning', "Failed to save history");
                } else if(data[0].result === 1){
                    $.alert.open('info', "Historically saved successfully");
                } 
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        });
    });
</script>