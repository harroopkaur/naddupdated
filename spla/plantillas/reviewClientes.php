<?php if($exito == 1 && $insertar == 1){ ?>
    <script>
        $.alert.open('info', 'Información actualizada con éxito', {'Aceptar' : 'Aceptar'});
    </script>
<?php } else if($exito == 2 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'Alert', 'No se pudo actualizar toda la información', {'Aceptar' : 'Aceptar'});
    </script>
<?php } else if($exito == 0 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'Alert', 'No se actualizó la información', {'Aceptar' : 'Aceptar'});
    </script>
<?php } ?>

<h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Clientes</p></h1>

<input type="hidden" id="pagina" value="1">

<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; line-height:30px;">Producto</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="producto" name="producto">
        <option value="">--Seleccione--</option>
        <option value="Windows Server">Windows Server</option>
        <option value="SQL Server">SQL Server</option>
    </select>
</div>
<br style="clear:both;"><br>

<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; line-height:30px;">Cliente</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="cliente" name="cliente">
        <option value="">--Seleccione--</option>
        <?php
        foreach($clientes as $row){
        ?>
            <option value="<?= $row["nombreCliente"] ?>"><?= $row["nombreCliente"] ?></option>
        <?php
        }
        ?>
    </select>
</div>
<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; margin-left:20px; line-height:30px;">Contrato</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="contrato" name="contrato">
        <option value="">--Seleccione--</option>
        <?php
        foreach($contratos as $row){
        ?>
            <option value="<?= $row["contrato"] ?>"><?= $row["contrato"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Exportar Excel</div>

<br>
<br>

<div style="float:left">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' de ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Mostrar</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">por p&aacute;gina</p>
</div>

<br>
<br>
<div style="clear: both;">
    <div style="overflow:hidden; padding-bottom:20px;">
        <!--<div class="botonesSAM boton5" id="asignarVS" style="float:right;">Asignar</div>-->
        <div class="botonesSAM boton5" id="editarClientes" style="float:right;">Editar</div>
        <!--<div class="botonesSAM boton5" id="borrarVS" style="float:right;">Borrar</div>
        <div class="botonesSAM boton5" id="agregarVS" style="float:right;">Agregar</div>-->
    </div>
    
    <form id="formClientes" name="formClientes" method="post" enctype="multipart/form-data" action="reviewClientes.php">
        <input type="hidden" id="insertar" name="insertar" value="1">
        <table id="listaCliente" style="width:100%; border: 1px solid;">
            <thead style="background-color:#06B6FF; color:#ffffff;">
                <tr>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Nombre del Cliente</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Contrato</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Inicio de Contrato</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Fin de Contrato</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Type</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Licencias Win</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Licencias SQL</th>
                </tr>
            </thead>
            <tbody id="tablaCliente">
            <?php
            $i = 0;
            foreach($listadoClientes as $row){
            ?>
                <tr>
                    <input type="hidden" id="id<?= $i ?>" name="id[]" value="<?= $row["id"] ?>">
                    <td style="border: 1px solid;"><?= $row["nombreCliente"] ?></td>
                    <td style="border: 1px solid;"><?= $row["contrato"] ?></td>
                    <td style="border: 1px solid;"><?= $row["inicioContrato"] ?></td>
                    <td style="border: 1px solid;"><?= $row["finContrato"] ?></td>
                    <td style="border: 1px solid;">
                        <select id="tipo<?= $i ?>" name="tipo[]" disabled="disabled">
                            <option value="" <?php if($row["type"] == ""){ echo "selected='selected'"; } ?>>--Seleccione--</option>
                            <option value="Managed" <?php if($row["type"] == "Managed"){ echo "selected='selected'"; } ?>>Managed</option>
                            <option value="Unmanaged" <?php if($row["type"] == "Unmanaged"){ echo "selected='selected'"; } ?>>Unmanaged</option>
                        </select>
                    </td>
                    <td style="border: 1px solid;"><input type="text" id="VM<?= $i ?>" name="VM[]" value="<?= $row["VM"] ?>" readonly></td>
                    <td style="border: 1px solid;">
                        <select id="licenciasWin<?= $i ?>" name="licenciasWin[]" disabled="disabled">
                            <option value="" <?php if($row["licenciasWin"] == ""){ echo "selected='selected'"; } ?>>--Seleccione--</option>
                            <option value="Si" <?php if($row["licenciasWin"] == "Si"){ echo "selected='selected'"; } ?>>Si</option>
                            <option value="No" <?php if($row["licenciasWin"] == "No"){ echo "selected='selected'"; } ?>>No</option>
                        </select>
                    </td>
                    <td style="border: 1px solid;">
                        <select id="licenciasSQL<?= $i ?>" name="licenciasSQL[]" disabled="disabled">
                            <option value="" <?php if($row["licenciasSQL"] == ""){ echo "selected='selected'"; } ?>>--Seleccione--</option>
                            <option value="Si" <?php if($row["licenciasSQL"] == "Si"){ echo "selected='selected'"; } ?>>Si</option>
                            <option value="No" <?php if($row["licenciasSQL"] == "No"){ echo "selected='selected'"; } ?>>No</option>
                        </select>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
            </tbody>
        </table>
    </form>
</div>
<br>
<div style="float:right;" class="botonesSAM boton5" id="guardar">Guardar</div>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='clientes.php'">Atras</div>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listaCliente").tablesorter();
      
        $("#producto, #cliente, #contrato").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadoCliente.php", { producto : $("#producto").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#limite").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadoCliente.php", { producto : $("#producto").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaCliente").empty();        
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#primero").click(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadoCliente.php", { producto : $("#producto").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#ultimo").click(function(){
            $("#fondo").show();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadoCliente.php", { producto : $("#producto").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#siguiente").click(function(){
            $("#fondo").show();
            pagina += 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadoCliente.php", { producto : $("#producto").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#atras").click(function(){
            $("#fondo").show();
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadoCliente.php", { producto : $("#producto").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });	

        $("#exportar").click(function(){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/listadoClientes.php?producto="+$("#producto").val()+"&cliente="+$("#cliente").val()+"&contrato="+$("#contrato").val();
        });
        
        $("#editarClientes").click(function(){
            for(i = 0; i < $("#tablaCliente tr").length; i++){
                $("#tipo" + i).prop("disabled", false);
                $("#VM" + i).prop("readonly", false);
                $("#licenciasWin" + i).prop("disabled", false);
                $("#licenciasSQL" + i).prop("disabled", false);
            }
        });
        
        $("#guardar").click(function(){
            for(i = 0; i < $("#tablaCliente tr").length; i++){
                if($("#tipo" + i).val() === ""){
                    $.alert.open('warning', "Alert", "Existen Type en blanco", {'Aceptar' : 'Aceptar'}, function() {
                        $("#tipo" + i).focus();
                    });
                    return false;
                }
                
                if($("#VM" + i).val() === ""){
                    $.alert.open('warning', "Alert", "Existen Virutal Machine en blanco", {'Aceptar' : 'Aceptar'}, function() {
                        $("#VM" + i).focus();
                    });
                    return false;
                }
                $("#formClientes").submit();              
            }
        });
    });
</script>