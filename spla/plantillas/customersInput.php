<input type="hidden" id="nombreArchivo">
<div style="width:98%; padding:20px; overflow:hidden;">                                
    <h1 class="textog negro" style="text-align:center; line-height:35px;">Upload <span style="color:#06B6FF;">Customer Information</span></h1>    
    
    <br>
    <br>
    
    <p>Please follow the steps below:</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1.-&nbsp;</span>Download <a class="link1" href="Customer Form.xlsx" target="_blank">Customers Form</a></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Complete the data of the form</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Select the <span class="bold">"Save As"</span> option and choose the file type <span class="bold">"CSV" (comma delimited) (*.csv)</span> </p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Upload saved CSV file</p><br>
    
    <br>

    <fieldset class="fieldset">
        <legend style=" font-weight:bold; margin-left:15px;">Customers</legend>

        <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="customersInput.php" >
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <div class="contenedor_archivo">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_file" disabled id="url_file" value="File Name...">
                    <input type="file" class="archivo" name="archivo" id="archivo" accept=".csv">
                    <input type="button" id="boton1" class="botones_m5" value="Search">
                </div>
                <div class="contenedor_boton_archivo">
                    <input name="insertar" type="button" id="insertar" value="File Upload" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                </div>
            </div>
        </form>

        <p style="text-align:center; color:#2D6BA4;">Find your file, and upload it by clicking on "Upload File"</p>
        <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Processing...</div>
        <?php if($exito=='1'){ ?>
            <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>');
            </script>
        <?php }else{ 
            if($error == 1){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorSPLAClientes($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getSeparadorSPLAClientes($_SESSION["idioma"]) ?>');
                </script>
            <?php
            }
            if($error == 2){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>');
                </script>
            <?php
            }
            if($error == 3){
            ?>
                <div class="error_archivo"><?= $general->getMensajeSPLAClientes($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeSPLAClientes($_SESSION["idioma"]) ?>');
                </script>
            <?php
            } 
            if($error == 4){
            ?>
                <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>');
                </script>
            <?php
            }
        } ?>
    </fieldset>

    <br>
    <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='customersValidation.php';">Customers Validation</div></div>
</div>

<script>
    $(document).ready(function(){
        $("#fechaD").datepicker();
        
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
</script>