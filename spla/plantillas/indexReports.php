<div style="width:98%; padding:10px; overflow:hidden;">
    <?php 
    if($_SESSION["idioma"] == 1){
    ?>
        <h1 class="textog negro" style="margin:20px; text-align:center;">Administraci&oacute;n de Licenciamiento</h1>
        <p style="margin-top:20px; font-size:24px; text-align:center;">Administra tus activos de software en 3 simples pasos</p>
    <?php 
    } else{
    ?>
        <h1 class="textog negro" style="margin:20px; text-align:center;">SPLA Administration</h1>
        <p style="margin-top:20px; font-size:24px; text-align:center;">Step 3: Reporting</p>
    <?php
    }
    ?>
    <br>
    
    <?php 
    if($_SESSION["idioma"] == 1){
    ?>
        <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/imgGeneral.png" style="margin: 0 auto; width: 85%;display: block;"/>
    <?php 
    } else{
    ?>  
        <div style="width:100%; overflow:hidden; margin:0 auto;">
            <div style="width:49.5%; overflow:hidden; float:left;">
                <div style="width:350px; margin:0 auto; overflow:hidden;">
                    <div style="float:left; width:200px;">
                        <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/SPLACurrentReport2.png" style="height:270px; width:auto; display: block;"/>
                        <div class="botonesSPLA boton5" style="margin-top:-20px;" onclick="location.href='currentReporting.php';">Current Reporting</div>
                    </div>
                    <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/imgCurrentReport.png" style="margin-top:68px; margin-left:-20px; float:left; width:140px; height:auto; display: block;"/>
                </div>
            </div>
            <div style="width:49.5%; overflow:hidden; float:left;">
                <div style="width:350px; margin:0 auto; overflow:hidden; float:left;">
                    <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/SPLAHistoricalReport2.png" style="margin:0 auto; height:270px; width:auto; display: block;"/>
                    <div style="width:350px; margin:0 auto; margin-top:-20px;overflow:hidden;">
                        <div class="float-lt" style="width:160px;"><div class="botonesSPLA boton5" onclick="location.href='historicalReportingApps.php';">Historical Apps</div></div>
                        <div class="float-rt" style="margin-left:15px; width:160px;"><div class="botonesSPLA boton5" onclick="location.href='historicalReportingVM.php';">Historical VM</div></div>
                    </div>
                </div>
                <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/imgHistoricalReport.png" style="margin-top:68px; margin-left:-120px; float:left; width:140px; height:auto; display: block; "/>                    
            </div>
        </div>
    <?php
    }
    ?>
    <br>
</div>