<input type="hidden" id="nombreArchivo">
<div style="width:98%; padding:20px; overflow:hidden;">                                
    
    <h1 class="textog negro" style="text-align:center; line-height:35px;">Deployment <span style="color:#06B6FF;">Tool</span></h1>
        
    <div>
        <p style="font-size:16px; color:#000; font-weight:300;">Dear Customer, please follow the steps below:</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Download file <a class="link1" href="<?= $GLOBALS["domain_root"] . "/" . $general->ToolLocal ?>"><?= $general->ToolLocal ?></a></p><br />
        <p class="float-lt"><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Download the list of computers to scan </p>
        <input name="listaPersonalizada" type="button" value="Create List VMs" class="botones_m5 float-lt" onclick="listaPersonalizada();"/><br style="clear:both"><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>In the previous step will generate a file called Equipos.zip, unzip the same and save it in the same folder in which are the components of the tool replacing the original file Equipos.txt with the new one.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.1 -&nbsp;</span>Confirm that the new file <span class="bold">Equipos.txt</span> was replaced corresponding opening it and confirm that the list of VMs is found below one separated by commas and not only the machine "Localhost"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Connect to the Domain Controller from where the tool will be executed</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Right click on the file "<span style="font-weight:bold;">LA_Tool.vbe</span>". Select the option "<span style="font-weight:bold;">Open with Command Prompt</span>" or "<span class="bold">Run with System Symbol</span>"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>A Command Prompt window will be displayed showing the progress of the computer scan execution. This window will close automatically after the scanning is complete. This may take a few minutes to complete, depending on the number of machines connected to the network and the connection speed</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>A file named "<span class="bold">LAE_Output.csv</span>" and a (1) folder named <span class="bold">Results</span> will be generated, within it will be a file named "<span class="bold">LAD_Output [dateoutdate] .rar</span>" with the execution date of the tool.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Upload the file "<span class="bold">LAD_Output.rar</span>". <span class="bold">Note:</span> The storage of the data may take time, please do not close this window and wait for the message "<span class="bold">File loaded successfully</span>"</p>
        <br>

        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">LAD_Output</legend>
            
            <form enctype="multipart/form-data" name="form_seg" id="form_seg" method="post" action="deployment.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionSegmentado" type="hidden" value="segmentado" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file2" value="File Name...">
                        <input type="file" class="archivo" name="archivo" id="archivo2" accept=".rar">
                        <input type="button" id="boton1" class="botones_m5" value="Search">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="File Upload" onclick="showhide('cargando');document.getElementById('form_seg').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>
            <p style="text-align:center; color:#2D6BA4;">Find your file, and upload it by clicking on "Upload File"</p>

            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Processing...</div>
            <?php
            if($exito==1){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }else{ ?>
            <?php }
            if($error== -1){ ?>
                <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error== -2){ ?>
                <div class="error_archivo">Wrong Password</div>
                <script>
                    $.alert.open('warning', 'Warning', 'Wrong Password', {'Ok' : 'Ok'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==1){ ?>
                <div class="error_archivo"><?= $general->getMensajeRar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==2){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error == 3){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 4){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 5){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', "<?=$consolidado->error?>", function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php } 
            else if($error == 6){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 7){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 8){
            ?>
                <div class="error_archivo"><?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 9){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 10){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 11){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 12){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 13){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 14){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 15){
            ?>
                <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 16){
            ?>
                <div class="error_archivo"><?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 17){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?>', function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            ?>
        </fieldset>

        <br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Upload the file "<span class="bold">LAE_Output.csv</span>". <span class="bold">Note:</span> The storage of the data may take time, please do not close this window and wait for the message "<span class="bold">File loaded successfully</span>"</p><br />
        
        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">LAE_Output</legend>
            
            <form enctype="multipart/form-data" name="form_seg2" id="form_seg2" method="post" action="deployment2.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionSegmentado" type="hidden" value="segmentado" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file3" value="File Name...">
                        <input type="file" class="archivo" name="archivo" id="archivo3" accept=".csv">
                        <input type="button" id="boton1" class="botones_m5" value="Search">
                        <input type="hidden" id="fechaDespliegueSegmentado" name="fechaDespliegue">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="File Upload" onclick="mostrarFechaDespliegue('segmentado');" class="botones_m5"/ />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Find your file, and upload it by clicking on "Upload File"</p>
            <div id="cargandoSegmentado" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Processing...</div>
            <?php if($exito2=='1' && $opcionDespliegue == "segmentado"){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>');
                </script>
            <?php }else{ ?>

            <?php }
            if($error2=='1' && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>');
                </script>
            <?php }
            if($error2=='2' && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>');
                </script>
            <?php }
            if($error2==3 && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', '<?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?>');
                </script>
            <?php }
            if($error2 > 5 && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', '<?=$consolidado->error?>');
                </script>
            <?php } ?>
        </fieldset>
    </div>
    
    <br>
    <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='appsValidation.php';">App Validation</div></div>
</div>

<!--inicio modal fecha despliegue-->
<div class="modal-personal modal-personal-md" id="modal-fechaDespliegue">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;"><p style="color:#06B6FF; display:inline">Deployment</p> Date</h1>
    </div>
    <div class="modal-personal-body text-center">
        <input type="hidden" id="fechaOpcion">
        <span class="bold">Date: </span><input type="text" id="fechaD" name="fechaD">
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirFechaDespliegue">Exit</div>
        <div class="boton1 botones_m2" style="float:right;" id="procesarLAE_Output">Process</div>
    </div>
</div>
<!--fin modal fecha despliegue-->
    
<!--inicio modal lista personalizada-->
<div class="modal-personal" id="modal-listaPersonalizada">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">List of <p style="color:#06B6FF; display:inline">VMs</p></h1>
    </div>

    <div class="modal-personal-body">
        <div style="width:400px; margin:0 auto;">
            <div style="float:left;">
                <span class="bold">Equipos:</span> 
            </div>
            <div style="float:left; margin-left:15px;">
                <textarea id="equipo" name="equipo" style="width:200%; height:300px; border:1px solid;" ></textarea>
            </div>
        </div>
    </div>

    <div class="modal-personal-footer">
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="$('#fondo1').hide(); $('#modal-listaPersonalizada').hide();">Exit</div></div>
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="generarListadoPersonal()">Generate File</div></div>
    </div>
</div>
<!--fin modal lista personalizada-->

<script>
    $(document).ready(function(){
        $("#fechaD").datepicker();
        
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
        
        $("#archivo1").change(function(e){
            $("#nombreArchivo").val("#url_file1");
            addArchivo(e);
        });
        
        $("#archivo2").change(function(e){
            $("#nombreArchivo").val("#url_file2");
            addArchivo(e);
        });
        
        $("#archivo3").change(function(e){
            $("#nombreArchivo").val("#url_file3");
            addArchivo(e);
        });
        
        //inicio fecha despliegue
        $("#salirFechaDespliegue").click(function(){
            $("#fechaOpcion").val("");
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        
        $("#procesarLAE_Output").click(function(){
            if($("#fechaD").val() === ""){
                $.alert.open('warning', "You must select the deployment date");
                return false;
            }
       
            if($("#fechaOpcion").val() === "completo"){
                showhide('cargando2');                
                $("#fechaDespliegueCompleto").val($("#fechaD").val());
                document.getElementById('form_e2').submit();
            } else if($("#fechaOpcion").val() === "segmentado"){
                showhide('cargandoSegmentado');
                $("#fechaDespliegueSegmentado").val($("#fechaD").val());
                document.getElementById('form_seg2').submit();
            }
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        //fin fecha despliegue
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
    
    function desplegarCompleto() {
        $("#principal").hide();
        $("#segmentado").hide();
        $("#completo").show();
        $("#despliegueSegmentado").hide();
        $("#despliegueCompleto").show();
        $("#opcionCompleto").val("completo");
        $("#LAEopcionCompleto").val("completo");
        $("#LAE-completo").show();
    }

    function desplegarSegmentado() {
        $("#principal").hide();
        $("#completo").hide();
        $("#segmentado").show();
        $("#despliegueCompleto").hide();
        $("#despliegueSegmentado").show();
        $("#opcionSegmentado").val("segmentado");
        $("#LAEopcionSegmentado").val("segmentado");
        $("#LAE-segmentado").show();
    }
    
    function mostrarFechaDespliegue(opcion){
        if(opcion === "completo" && $("#archivo1").val() === ""){
            $.alert.open('warning', '<?= $general->getMensajeLAE($_SESSION["idioma"]) ?>');
            return false;
        } else if(opcion === "segmentado" && $("#archivo3").val() === ""){
            $.alert.open('warning', '<?= $general->getMensajeLAE($_SESSION["idioma"]) ?>');
            return false;
        }
        
        $("#fechaOpcion").val(opcion);
        $("#fondo1").show();
        $("#modal-fechaDespliegue").show();
    }
    
    function listaPersonalizada(){
        $("#fondo1").show();
        $("#modal-listaPersonalizada").show();
        $("#equipo").focus();
    }
    
    function generarListadoPersonal(){
        $("#fondo1").hide();
        $("#modal-listaPersonalizada").hide();
        $("#fondo").show();
        
        if($("#equipo").val() === ""){
            $.alert.open('warning', '<?= $general->getMensajeEquipos($_SESSION["idioma"]) ?>', function() {
                $("#fondo1").show();
                $("#modal-listaPersonalizada").show();
            });
            return false;
        }    
        
        $.post("<?= $GLOBALS["domain_root"] ?>/usabilidad/ajax/generarLista.php", { equipo : $("#equipo").val(), token : localStorage.licensingassuranceToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
            $("#fondo").hide();
            if(data[0].result === 0){
                $.alert.open('warning', '<?= $general->getMensajeCarpNoCreada($_SESSION["idioma"]) ?>');
            } else if(data[0].result === 1){
                location.href = "<?= $GLOBALS["domain_root"] ?>/" + data[0].archivo;
                $("#equipo").val("");
            } else if(data[0].result === 2){
                $.alert.open('warning', '<?= $general->getMensajeEquipos($_SESSION["idioma"]) ?>');
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status);
        });
    }
</script>