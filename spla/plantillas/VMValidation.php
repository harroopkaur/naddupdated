<?php if($exito == 1 && $insertar == 1){ ?>
    <script>
        $.alert.open('info', 'Updated information successfully', {'Aceptar' : 'Aceptar'});
    </script>
<?php } else if($exito == 2 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'Failed to update all information', {'Aceptar' : 'Aceptar'});
    </script>
<?php } else if($exito == 0 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'The information was not updated', {'Aceptar' : 'Aceptar'});
    </script>
<?php } ?>
    
<h1 class="textog negro" style="margin:20px; text-align:center;">Listing <p style="color:#06B6FF; display:inline">Virtual Machines</p></h1>

<input type="hidden" id="pagina" value="1">

<div style="float:left;">
    <p class="bold text-right" style="width:80px; float:left; line-height:30px;">Customers</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="clientes" name="clientes">
        <option value="">--Seleccione--</option>
        <?php
        foreach($clientes as $row1){
        ?>
            <option value="<?= $row1["nombreCliente"] ?>"><?= $row1["nombreCliente"] ?></option>
        <?php
        }
        ?>
    </select>
</div>
<br style="clear:both;"><br>

<div style="float:left;">
    <p class="bold text-right" style="width:80px; float:left; line-height:30px;">Data Center</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="datacenter" name="datacenter">
        <option value="">--Seleccione--</option>
        <?php
        foreach($datacenter as $row){
        ?>
            <option value="<?= $row["DC"] ?>"><?= $row["DC"] ?></option>
        <?php
        }
        ?>
    </select>
</div>
<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; margin-left:20px; line-height:30px;">VM Name</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="VMName" name="VMName">
        <option value="">--Seleccione--</option>
        <?php
        foreach($VMName as $row){
        ?>
            <option value="<?= $row["VM"] ?>"><?= $row["VM"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<p class="bold" style="float:left; margin-left:30px; line-height:30px;">Black: </p><p style="float:left; margin-left:10px; line-height:30px;">Existing</p>
<p class="bold" style="float:left; margin-left:30px; line-height:30px;">Red: </p><p style="float:left; margin-left:10px; line-height:30px;">New</p>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Export Excel</div>

<br>
<br>

<div style="float:left">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' of ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ echo 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ echo 'display:none'; } ?>" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Show</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">per page</p>
</div>

<div class="botonesSAM boton5" id="editarVMs" style="float:right;">Edit</div>

<br>
<br>
<form id="formClientes" name="formClientes" method="post" enctype="multipart/form-data" action="VMValidation.php">
    <input type="hidden" id="insertar" name="insertar" value="1">
    <div style="clear: both; width:100%; max-height:400px; overflow:auto;">    
        <table id="listavCPU" class="tablap" style="width:1600px;">
            <thead style="background-color:#06B6FF; color:#ffffff;">
                <tr>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">DC</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Cluster</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Host</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM Name</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">OS</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Sockets</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Cores</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">CPUs</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Host Date</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM Date</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Customer</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Qty</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">SKU</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Type</th>
                </tr>
            </thead>
            <tbody id="tablavCPU">
            <?php
            $i = 0;
            foreach($listadovCPU as $row){
            ?>
                <tr>
                    <input type="hidden" id="id<?= $i ?>" name="id[]" value="<?= $row["id"] ?>">
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["DC"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["cluster"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["host"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["VM"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["edicion"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["sockets"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["cores"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["cpu"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["hostDate"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["VMDate"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>">
                        <select id="customers<?= $i ?>" name="customers[]" disabled>
                            <option value="">--Select--</option>
                            <?php
                            foreach($clientes as $row1){
                            ?>
                                <option value="<?= $row1["nombreCliente"] ?>" <?php if($row1["nombreCliente"] == $row["customers"]){ echo "selected='selected'"; } ?>><?= $row1["nombreCliente"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><input type="text" class="text-right" id="cantidad<?= $i ?>" name="cantidad[]" value="<?= $row["Qty"] ?>" maxlength="4" style="width:80px;" onfocus="$('#cantidad<?= $i ?>').val('')" disabled></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>">
                        <select id="SKU<?= $i ?>" name="SKU[]" disabled>
                            <option value="">--Select--</option>
                            <?php
                            foreach($listadoSKU as $row1){
                            ?>
                                <option value="<?= $row1["SKU"] ?>" <?php if($row1["SKU"] == $row["SKU"]){ echo "selected='selected'"; } ?>><?= $row1["SKU"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><input type="text" class="text-right" id="type<?= $i ?>" name="type[]" value="<?= $row["type"] ?>" maxlength="70" style="width:100px;" onfocus="$('#type<?= $i ?>').val('');" disabled></td>
                    <script>
                        $("#cantidad<?= $i ?>").numeric(false);
                    </script>
                </tr>
                <?php
                $i++;
            }
            ?>
            </tbody>
        </table>
    </div>
</form>
<br>

<div style="float:right;" class="botonesSAM boton5 hide" id="guardar">Save</div>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='VMInput.php'">Back</div>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listavCPU").tablesorter();
        $("#listavCPU").tableHeadFixer();
      
        $("#datacenter, #VMName, #clientes").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/VMList.php", { clientes : $("#clientes").val(), 
            datacenter : $("#datacenter").val(), VMName : $("#VMName").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#limite").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/VMList.php", { clientes : $("#clientes").val(), 
            datacenter : $("#datacenter").val(), VMName : $("#VMName").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablavCPU").empty();        
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#primero").click(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/VMList.php", { clientes : $("#clientes").val(), 
            datacenter : $("#datacenter").val(), VMName : $("#VMName").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#ultimo").click(function(){
            $("#fondo").show();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/VMList.php", { clientes : $("#clientes").val(), 
            datacenter : $("#datacenter").val(), VMName : $("#VMName").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#siguiente").click(function(){
            $("#fondo").show();
            pagina += 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/VMList.php", { clientes : $("#clientes").val(), 
            datacenter : $("#datacenter").val(), VMName : $("#VMName").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#atras").click(function(){
            $("#fondo").show();
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/VMList.php", { clientes : $("#clientes").val(), 
            datacenter : $("#datacenter").val(), VMName : $("#VMName").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });	

        $("#exportar").click(function(){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/VMList.php?clientes="+$("#clientes").val()+"&datacenter="+$("#datacenter").val()+"&VMName="+$("#VMName").val();
        });
        
        $("#editarVMs").click(function(){
            for(i = 0; i < $("#tablavCPU tr").length; i++){
                $("#customers" + i).prop("disabled", false);
                $("#cantidad" + i).prop("disabled", false);
                $("#SKU" + i).prop("disabled", false);
                $("#type" + i).prop("disabled", false);
                $("#guardar").show();
            }
        });
        
        $("#guardar").click(function(){
            $("#formClientes").submit();
        });
    });
</script>