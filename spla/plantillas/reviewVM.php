<h1 class="textog negro" style="margin:20px; text-align:center;">Listado <p style="color:#06B6FF; display:inline">Virtual Machines</p></h1>

<input type="hidden" id="pagina" value="1">

<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; line-height:30px;">Producto</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="producto" name="producto">
        <option value="">--Seleccione--</option>
        <option value="Windows Server">Windows Server</option>
        <option value="SQL Server">SQL Server</option>
    </select>
</div>
<br style="clear:both;"><br>

<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; line-height:30px;">Cluster</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="cluster" name="cluster">
        <option value="">--Seleccione--</option>
        <?php
        foreach($clusters as $row){
        ?>
            <option value="<?= $row["cluster"] ?>"><?= $row["cluster"] ?></option>
        <?php
        }
        ?>
    </select>
</div>
<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; margin-left:20px; line-height:30px;">Host</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="host" name="host">
        <option value="">--Seleccione--</option>
        <?php
        foreach($hosts as $row){
        ?>
            <option value="<?= $row["host"] ?>"><?= $row["host"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Exportar Excel</div>

<br>
<br>

<div style="float:left">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' de ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Mostrar</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">por p&aacute;gina</p>
</div>

<br>
<br>
<div style="clear: both;">
    <table id="listavCPU" style="width:100%; border: 1px solid;">
        <thead style="background-color:#06B6FF; color:#ffffff;">
            <tr>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">DC</th>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Cluster</th>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Host</th>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM</th>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Edición</th>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Sockets</th>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Coresp/s</th>
                <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">CPUs</th>
            </tr>
        </thead>
        <tbody id="tablavCPU">
        <?php
        $i = 0;
        foreach($listadovCPU as $row){
        ?>
            <tr>
                <td style="border: 1px solid;"><?= $row["DC"] ?></td>
                <td style="border: 1px solid;"><?= $row["cluster"] ?></td>
                <td style="border: 1px solid;"><?= $row["host"] ?></td>
                <td style="border: 1px solid;"><?= $row["VM"] ?></td>
                <td style="border: 1px solid;"><?= $row["edicion"] ?></td>
                <td style="border: 1px solid;"><?= $row["sockets"] ?></td>
                <td style="border: 1px solid;"><?= $row["cores"] ?></td>
                <td style="border: 1px solid;"><?= $row["cpu"] ?></td>
            </tr>
            <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
</div>
<br>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='vCPU.php'">Atras</div>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listavCPU").tablesorter();
      
        $("#producto, #cluster, #host").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadovCPU.php", { producto : $("#producto").val(), 
            cluster : $("#cluster").val(), host : $("#host").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#limite").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadovCPU.php", { producto : $("#producto").val(), 
            cluster : $("#cluster").val(), host : $("#host").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablavCPU").empty();        
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#primero").click(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadovCPU.php", { producto : $("#producto").val(), 
            cluster : $("#cluster").val(), host : $("#host").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#ultimo").click(function(){
            $("#fondo").show();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadovCPU.php", { producto : $("#producto").val(), 
            cluster : $("#cluster").val(), host : $("#host").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#siguiente").click(function(){
            $("#fondo").show();
            pagina += 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadovCPU.php", { producto : $("#producto").val(), 
            cluster : $("#cluster").val(), host : $("#host").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#atras").click(function(){
            $("#fondo").show();
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/listadovCPU.php", { producto : $("#producto").val(), 
            cluster : $("#cluster").val(), host : $("#host").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablavCPU").empty();
                $("#tablavCPU").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });	

        $("#exportar").click(function(){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/listadovCPU.php?producto="+$("#producto").val()+"&cluster="+$("#cluster").val()+"&host="+$("#host").val();
        });
    });
</script>