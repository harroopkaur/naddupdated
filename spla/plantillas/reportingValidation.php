<?php if($exito == 1 && $insertar == 1){ ?>
    <script>
        $.alert.open('info', 'Updated information successfully');
    </script>
<?php } else if($exito == 2 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'Failed to update all information');
    </script>
<?php } else if($exito == 0 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'The information was not updated');
    </script>
<?php } ?>

<h1 class="textog negro" style="margin:20px; text-align:center;">Summary <p style="color:#06B6FF; display:inline">Report</p></h1>

<input type="hidden" id="pagina" value="1">
<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; margin-left:20px; line-height:30px;">Product</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="producto" name="produto">
        <option value="">--Seleccione--</option>
        <?php
        foreach($listadoProductos as $row){
        ?>
            <option value="<?= $row["familia"] ?>"><?= $row["familia"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Export Excel</div>

<br>
<br>

<div style="float:left;">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' of ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Show</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">per page</p>  
</div>

<div class="botonesSAM boton5" id="editarApps" style="float:right;">Edit</div>

<br>
<br>
<div style="clear: both;">
    
    
    <form id="formApps" name="formApps" method="post" enctype="multipart/form-data" action="appsValidation.php">
        <input type="hidden" id="insertar" name="insertar" value="1">
        
        <style>
            table thead th{
                background-color:#06B6FF; 
                color:#ffffff;
            }
            
            table thead th.darkBlue{
                background-color:#4E89BE; 
                color:#ffffff;
                font-weight: bold;
            }
        </style>
        <table id="listaApps" class="tablap" style="width:80%; margin:0 auto;">
            <thead>
                <tr>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Product</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">SKU</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Qty</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Cost</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Total</th>
                </tr>
            </thead>
            <tbody id="tablaApps">
            <?php
            $i = 0;
            $totalCost = 0;
            foreach($listadoResumen as $row){
                $totalCost += $row["total"];
            ?>
                <tr>
                    <input type="hidden" id="id<?= $i ?>" name="id[]" value="<?= $row["id"] ?>">
                    <input type="hidden" id="tipo<?= $i ?>" name="tipo[]" value="<?= $row["tipo"] ?>">
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["familia"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["SKU"] ?></td>
                    <td style="border: 1px solid; text-align:right;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["Qty"] ?></td>
                    <td style="border: 1px solid; text-align:right;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["price"] ?></td>
                    <td style="border: 1px solid; text-align:right;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["total"] ?></td>
                </tr>
                <?php
                $i++;
            }
            ?>
                <tr>
                    <td colspan="4" style="border: 1px solid;" class="bold">Total</td>
                    <td style="border: 1px solid; text-align:right;"><?= $totalCost ?></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br>
<div style="float:right;" class="botonesSAM boton5 hide" id="guardar">Save</div>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='deployment.php'">Back</div>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listaCliente").tablesorter();
      
        $("#producto, #clientes").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/reportingValidation.php", { producto : $("#producto").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaApps").empty();
                $("#tablaApps").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#limite").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/reportingValidation.php", { producto : $("#producto").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaApps").empty();        
                $("#tablaApps").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#primero").click(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/reportingValidation.php", { producto : $("#producto").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaApps").empty();
                $("#tablaApps").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#ultimo").click(function(){
            $("#fondo").show();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/reportingValidation.php", { producto : $("#producto").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaApps").empty();
                $("#tablaApps").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#siguiente").click(function(){
            $("#fondo").show();
            pagina += 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/reportingValidation.php", { producto : $("#producto").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaApps").empty();
                $("#tablaApps").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#atras").click(function(){
            $("#fondo").show();
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/reportingValidation.php", { producto : $("#producto").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaApps").empty();
                $("#tablaApps").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });	

        $("#exportar").click(function(){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/reportingValidation.php?&producto="+$("#producto").val();
        });
        
        $("#editarApps").click(function(){
            for(i = 0; i < $("#tablaApps tr").length; i++){
                $("#customers" + i).prop("disabled", false);
                $("#SKU" + i).prop("disabled", false);
                $("#cantidad" + i).prop("readonly", false);
                $("#type" + i).prop("readonly", false);
            }
            
            $("#guardar").show();
        });
        
        $("#guardar").click(function(){
            $("#formApps").submit();              
        });
    });
</script>