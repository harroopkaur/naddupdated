<?php if($exito == 1 && $insertar == 1){ ?>
    <script>
        $.alert.open('info', 'Updated information successfully', {'Aceptar' : 'Aceptar'});
    </script>
<?php } else if($exito == 2 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'Failed to update all information', {'Aceptar' : 'Aceptar'});
    </script>
<?php } else if($exito == 0 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'The information was not updated', {'Aceptar' : 'Aceptar'});
    </script>
<?php } ?>

<h1 class="textog negro" style="margin:20px; text-align:center;"><span style="color:#06B6FF; display:inline">Customers</span> Listing Validation</h1>

<input type="hidden" id="pagina" value="1">

<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; line-height:30px;">Customers</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="cliente" name="cliente">
        <option value="">--Select--</option>
        <?php
        foreach($clientes as $row){
        ?>
            <option value="<?= $row["nombreCliente"] ?>"><?= $row["nombreCliente"] ?></option>
        <?php
        }
        ?>
    </select>
</div>
<br style="clear:both;"><br>

<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; line-height:30px;">Contract #</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="contrato" name="contrato">
        <option value="">--Select--</option>
        <?php
        foreach($contratos as $row){
        ?>
            <option value="<?= $row["contrato"] ?>"><?= $row["contrato"] ?></option>
        <?php
        }
        ?>
    </select>
</div>
<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; margin-left:20px; line-height:30px;">Start Date</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="inicioContrato" name="inicioContrato">
        <option value="">--Select--</option>
        <?php
        foreach($fechaInicio as $row){
        ?>
            <option value="<?= $row["inicioContrato"] ?>"><?= $row["inicioContrato"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<p class="bold" style="float:left; margin-left:30px; line-height:30px;">Black: </p><p style="float:left; margin-left:10px; line-height:30px;">Existing</p>
<p class="bold" style="float:left; margin-left:30px; line-height:30px;">Red: </p><p style="float:left; margin-left:10px; line-height:30px;">New</p>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Export Excel</div>

<br>
<br>

<div style="float:left">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' of ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ echo 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ echo 'display:none'; } ?>" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Show</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">per page</p>
</div>

<div class="botonesSAM boton5" id="editarClientes" style="float:right;">Edit</div>

<br>
<br>

<form id="formClientes" name="formClientes" method="post" enctype="multipart/form-data" action="customersValidation.php">
    <div style="clear: both; width:80%; max-height:400px; margin:0 auto;">
        <input type="hidden" id="insertar" name="insertar" value="1">
        <table id="listaCliente" class="tablap" style="width:100%; border: 1px solid;">
            <thead style="background-color:#06B6FF; color:#ffffff;">
                <tr>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Customer Name</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Contract</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Start Date</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">End Date</th>
                </tr>
            </thead>
            <tbody id="tablaCliente"> 
            <?php
            $i = 0;
            foreach($listadoClientes as $row){
            ?>
                <tr>
                    <input type="hidden" id="id<?= $i ?>" name="id[]" value="<?= $row["id"] ?>">
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["nombreCliente"] ?></td>
                    <td style="border: 1px solid;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><?= $row["contrato"] ?></td>
                    <td style="border: 1px solid; width:100px;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><input type="text" class="pointer" id="inicioContrato<?= $i ?>" name="inicioContrato[]" value="<?= $row["inicioContrato"] ?>" disabled></td>
                    <td style="border: 1px solid; width:100px;" class="<?php if($row["existe"] == 1){ echo 'nuevoProducto'; } else if($row["existe"] == 2){ echo 'productoNoExiste'; } ?>"><input type="text" class="pointer" id="finContrato<?= $i ?>" name="finContrato[]" value="<?= $row["finContrato"] ?>" disabled></td>
                    <script>
                        $("#inicioContrato<?= $i ?>").datepicker();
                        $("#finContrato<?= $i ?>").datepicker();
                    </script>
                </tr>
                <?php
                $i++;
            }
            ?>
            </tbody>
        </table>
    </div>
</form>
<br>
<div style="float:right;" class="botonesSAM boton5 hide" id="guardar">Save</div>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='customersInput.php'">Back</div>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listaCliente").tablesorter();
        $("#listaCliente").tableHeadFixer();
      
        $("#inicioContrato, #cliente, #contrato").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/customersList.php", { inicioContrato : $("#inicioContrato").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#limite").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/customersList.php", { inicioContrato : $("#inicioContrato").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaCliente").empty();        
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#primero").click(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/customersList.php", { inicioContrato : $("#inicioContrato").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#ultimo").click(function(){
            $("#fondo").show();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/customersList.php", { inicioContrato : $("#inicioContrato").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#siguiente").click(function(){
            $("#fondo").show();
            pagina += 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/customersList.php", { inicioContrato : $("#inicioContrato").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#atras").click(function(){
            $("#fondo").show();
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/customersList.php", { inicioContrato : $("#inicioContrato").val(), 
            cliente : $("#cliente").val(), contrato : $("#contrato").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaCliente").empty();
                $("#tablaCliente").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });	

        $("#exportar").click(function(){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/customersList.php?fechasInicio="+$("#inicioContrato").val()+"&cliente="+$("#cliente").val()+"&contrato="+$("#contrato").val();
        });
        
        $("#editarClientes").click(function(){
            for(i = 0; i < $("#tablaCliente tr").length; i++){
                $("#inicioContrato" + i).prop("disabled", false);
                $("#finContrato" + i).prop("disabled", false);
                $("#inicioContrato" + i).prop("readonly", true);
                $("#finContrato" + i).prop("readonly", true);
                $("#guardar").show();
            }
        });
        
        $("#guardar").click(function(){
            $("#formClientes").submit();
        });
    });
</script>