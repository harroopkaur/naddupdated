<style>
    ul, ol {
            list-style:none;
    }

    .nav {
        width:140px; /*Le establecemos un ancho*/
        margin:0 auto; /*Centramos automaticamente*/
        margin:5px;
        margin-left:10px;
        float:left;	
        width:100px;
        padding:10px;
        border-radius:10px;
        text-align:center;
        cursor:pointer;
        background:#7F7F7F;
	color:#fff;
        font-size:12px;
    }
        
    .nav:hover {
        border-radius:10px 10px 0px 0px;
    }

    .nav li a {
        background:#7F7F7F;
	color:#fff;
        text-decoration:none;
        display:block;
    }
    
    .nav.navActivado, .nav.navActivado li a{
	background:#D9D9D9 !important;
	color:#000 !important;
        font-weight: bold;
    }
    
    .nav.navActivado li a:hover{
	background:#7F7F7F !important;
    }
    
    .nav li a:hover {
        background-color:#434343;
    }

    .nav li ul {
        margin-top: 0px;
        margin-left: -10px;
        display:none;
        position:absolute;
        min-width:120px;
    }

    .nav li:hover > ul {
        display:block;
    }
    
    .navSelected{
        background-color:#ffffff;
        color:#000000;
    }
    
    /*inicio submenu*/
    .nav li ul li{
        position: relative;
    }
    
    .nav li ul li ul{
        right: -140px;
        top: 0;
    }
    /*fin submenu*/
</style>


<!--<div class="botones_m2 boton2 <?php if($menuSPLA==1){ echo 'activado3'; } ?>" style="margin-left:30px;" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/spla/';" title="Información General">General</div>
<ul class="nav <?php if($menuSPLA==2){ echo 'navActivado'; } ?>">
    <li><a href="">Customers</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/customersInput.php">Input</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/customersValidation.php">Validation</a></li>
        </ul>
    </li>
</ul>

<ul class="nav <?php if($menuSPLA==3){ echo 'navActivado';   } ?>">
    <li><a href="">Virtual Machines</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/VMInput.php">Input</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/VMValidation.php">Validation</a></li>
        </ul>
    </li>
</ul>

<ul class="nav <?php if($menuSPLA==4){ echo 'navActivado';   } ?>">
    <li><a href="">Applications</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/deployment.php">Input</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/appsValidation.php">Validation</a></li>
        </ul>
    </li>
</ul>

<ul class="nav <?php if($menuSPLA==5){ echo 'navActivado';   } ?>">
    <li><a href="">Reporting</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/currentReporting.php">Current</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/reportingValidation.php">Validation</a></li>
        </ul>
    </li>
</ul>

<ul class="nav <?php if($opcionm1==7){ echo 'navActivado';   } ?>">
    <li><a href="">Historical</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/historicalVM.php">Reporting</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/historicalApps.php">Other</a></li>
        </ul>
    </li>
</ul>

<ul class="nav <?php if($menuSPLA==6){ echo 'navActivado';   } ?>">
    <li><a href="">SKU</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/SKUInput.php">Input</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/SKUReview.php">Review</a></li>
        </ul>
    </li>
</ul>-->
<div class="botonesTopMenu boton2 <?php if($menuSPLA == 1){ echo 'activado3'; } ?>" style="margin-left:30px;" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/spla/';" title="Información General">General</div>
<div class="botones_m2 boton2 <?php if($menuSPLA==2){ echo 'activado3'; } ?>" onclick="location.href='indexCustomers.php'" title="">Customers</div>
<div class="botones_m2 boton2 <?php if($menuSPLA==3){ echo 'activado3'; } ?>" onclick="location.href='indexVM.php'" title="">Virtual Machines</div>
<div class="botones_m2 boton2 <?php if($menuSPLA==4){ echo 'activado3'; } ?>" onclick="location.href='indexApps.php'" title="">Applications</div>
<div class="botones_m2 boton2 <?php if($menuSPLA==5){ echo 'activado3'; } ?>" onclick="location.href='indexReports.php'" title="">Reporting</div>
<!--<div class="botones_m2 boton2 <?php if($menuSPLA==6){ echo 'activado3'; } ?>" onclick="location.href='indexSKU.php'" title="">SKU</div>-->
<ul class="nav <?php if($menuSPLA==6){ echo 'navActivado';   } ?>">
    <li><a href="">SKU</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/SKUInput.php">Input</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/spla/SKUReview.php">Review</a></li>
        </ul>
    </li>
</ul>