<input type="hidden" id="nombreArchivo">
<div style="width:98%; padding:20px; overflow:hidden;">                                
    
    <h1 class="textog negro" style="text-align:center; line-height:35px;">Despliegue <span style="color:#06B6FF;">Tool</span></h1>
        
    <div>
        <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] . "/" . $general->ToolLocal ?>"><?= $general->ToolLocal ?></a></p><br />
        <p class="float-lt"><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Descargar la lista de equipos a escanear</p>
        <input name="listaPersonalizada" type="button" value="Crear Lista VMs" class="botones_m5 float-lt" onclick="listaPersonalizada();"/><br style="clear:both"><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>En el paso anterior se generará un archivo llamado Equipos.zip, descomprimir el mismo y guardarlo en la misma carpeta en la que se encuentran los componentes de la herramienta reemplazando el archivo original Equipos.txt por el nuevo.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.1 -&nbsp;</span>Confirmar que el archivo nuevo <span class="bold">Equipos.txt</span> fue reemplazado correspondiente abriendo el mismo y confirmar que se encuentra la lista de VMs una debajo de otra separada por comas y no se encuentra solamente la máquina "Localhost"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Conectarse al Controlador de Dominio desde donde será ejecutada la herramienta</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
        <br>

        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">LAD_Output</legend>
            
            <form enctype="multipart/form-data" name="form_seg" id="form_seg" method="post" action="despliegue1.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionSegmentado" type="hidden" value="segmentado" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file2" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo2" accept=".rar">
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_seg').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>

            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <?php
            if($exito==1){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }else{ ?>
            <?php }
            if($error== -1){ ?>
                <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==1){ ?>
                <div class="error_archivo"><?= $general->getMensajeRar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==2){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error == 3){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 4){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 5){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php } 
            else if($error == 6){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 7){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 8){
            ?>
                <div class="error_archivo"><?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 9){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 10){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 11){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 12){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 13){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 14){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 15){
            ?>
                <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 16){
            ?>
                <div class="error_archivo"><?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 17){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            ?>
        </fieldset>

        <br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p><br />
        
        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">LAE_Output</legend>
            
            <form enctype="multipart/form-data" name="form_seg2" id="form_seg2" method="post" action="despliegue2.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionSegmentado" type="hidden" value="segmentado" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file3" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo3" accept=".csv">
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                        <input type="hidden" id="fechaDespliegueSegmentado" name="fechaDespliegue">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="mostrarFechaDespliegue('segmentado');" class="botones_m5"/ />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargandoSegmentado" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <?php if($exito2=='1' && $opcionDespliegue == "segmentado"){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>" . $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }else{ ?>

            <?php }
            if($error2=='1' && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error2=='2' && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error2==3 && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            if($error2 > 5 && $opcionDespliegue == "segmentado"){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?=$consolidado->error?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php } ?>
        </fieldset>
        
        <br />


        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='compras.php';">Compras</div></div>
    </div>
</div>

<!--inicio modal fecha despliegue-->
<div class="modal-personal modal-personal-md" id="modal-fechaDespliegue">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Fecha <p style="color:#06B6FF; display:inline">Despliegue</p></h1>
    </div>
    <div class="modal-personal-body text-center">
        <input type="hidden" id="fechaOpcion">
        <span class="bold">Fecha: </span><input type="text" id="fechaD" name="fechaD">
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirFechaDespliegue">Salir</div>
        <div class="boton1 botones_m2" style="float:right;" id="procesarLAE_Output">Procesar</div>
    </div>
</div>
<!--fin modal fecha despliegue-->
    
<!--inicio modal lista personalizada-->
<div class="modal-personal" id="modal-listaPersonalizada">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">VMs</p></h1>
    </div>

    <div class="modal-personal-body">
        <div style="width:400px; margin:0 auto;">
            <div style="float:left;">
                <span class="bold">Equipos:</span> 
            </div>
            <div style="float:left; margin-left:15px;">
                <textarea id="equipo" name="equipo" style="width:200%; height:300px; border:1px solid;" ></textarea>
            </div>
        </div>
    </div>

    <div class="modal-personal-footer">
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="$('#fondo1').hide(); $('#modal-listaPersonalizada').hide();">Salir</div></div>
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="generarListadoPersonal()">Generar Archivo</div></div>
    </div>
</div>
<!--fin modal lista personalizada-->

<script>
    $(document).ready(function(){
        $("#fechaD").datepicker();
        
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
        
        $("#archivo1").change(function(e){
            $("#nombreArchivo").val("#url_file1");
            addArchivo(e);
        });
        
        $("#archivo2").change(function(e){
            $("#nombreArchivo").val("#url_file2");
            addArchivo(e);
        });
        
        $("#archivo3").change(function(e){
            $("#nombreArchivo").val("#url_file3");
            addArchivo(e);
        });
        
        //inicio fecha despliegue
        $("#salirFechaDespliegue").click(function(){
            $("#fechaOpcion").val("");
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        
        $("#procesarLAE_Output").click(function(){
            if($("#fechaD").val() === ""){
                $.alert.open('warning', 'Alert', "Debe seleccionar la fecha del despliegue", {"Aceptar" : "Aceptar"});
                return false;
            }
       
            if($("#fechaOpcion").val() === "completo"){
                showhide('cargando2');                
                $("#fechaDespliegueCompleto").val($("#fechaD").val());
                document.getElementById('form_e2').submit();
            } else if($("#fechaOpcion").val() === "segmentado"){
                showhide('cargandoSegmentado');
                $("#fechaDespliegueSegmentado").val($("#fechaD").val());
                document.getElementById('form_seg2').submit();
            }
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        //fin fecha despliegue
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
    
    function desplegarCompleto() {
        $("#principal").hide();
        $("#segmentado").hide();
        $("#completo").show();
        $("#despliegueSegmentado").hide();
        $("#despliegueCompleto").show();
        $("#opcionCompleto").val("completo");
        $("#LAEopcionCompleto").val("completo");
        $("#LAE-completo").show();
    }

    function desplegarSegmentado() {
        $("#principal").hide();
        $("#completo").hide();
        $("#segmentado").show();
        $("#despliegueCompleto").hide();
        $("#despliegueSegmentado").show();
        $("#opcionSegmentado").val("segmentado");
        $("#LAEopcionSegmentado").val("segmentado");
        $("#LAE-segmentado").show();
    }
    
    function mostrarFechaDespliegue(opcion){
        if(opcion === "completo" && $("#archivo1").val() === ""){
            $.alert.open('warning', 'Alert', "Debe seleccionar el archivo LAE_Output.csv", {"Aceptar" : "Aceptar"});
            return false;
        } else if(opcion === "segmentado" && $("#archivo3").val() === ""){
            $.alert.open('warning', 'Alert', "Debe seleccionar el archivo LAE_Output.csv", {"Aceptar" : "Aceptar"});
            return false;
        }
        
        $("#fechaOpcion").val(opcion);
        $("#fondo1").show();
        $("#modal-fechaDespliegue").show();
    }
    
    function listaPersonalizada(){
        $("#fondo1").show();
        $("#modal-listaPersonalizada").show();
        $("#equipo").focus();
    }
    
    function generarListadoPersonal(){
        $("#fondo1").hide();
        $("#modal-listaPersonalizada").hide();
        $("#fondo").show();
        
        if($("#equipo").val() === ""){
            $.alert.open('warning', 'Alert', "No existen equipos", {"Aceptar" : "Aceptar"}, function() {
                $("#fondo1").show();
                $("#modal-listaPersonalizada").show();
            });
            return false;
        }    
        
        $.post("<?= $GLOBALS["domain_root"] ?>/usabilidad/ajax/generarLista.php", { equipo : $("#equipo").val(), token : localStorage.licensingassuranceToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
            $("#fondo").hide();
            if(data[0].result === 0){
                $.alert.open('warning', 'Alert', "No se pudo crear la carpeta del cliente", {"Aceptar" : "Aceptar"});
            } else if(data[0].result === 1){
                location.href = "<?= $GLOBALS["domain_root"] ?>/" + data[0].archivo;
                $("#equipo").val("");
            } else if(data[0].result === 2){
                $.alert.open('warning', 'Alert', "No existen equipos", {"Aceptar" : "Aceptar"});
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
        });
    }
</script>