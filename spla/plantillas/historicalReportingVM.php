<?php if($exito == 1 && $insertar == 1){ ?>
    <script>
        $.alert.open('info', 'Updated information successfully');
    </script>
<?php } else if($exito == 2 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'Failed to update all information');
    </script>
<?php } else if($exito == 0 && $insertar == 1){ ?>
    <script>
        $.alert.open('warning', 'The information was not updated');
    </script>
<?php } ?>

<h1 class="textog negro" style="margin:20px; text-align:center;">Historical <p style="color:#06B6FF; display:inline">Reporting VMs</p></h1>

<input type="hidden" id="pagina" value="1">

<div style="float:left;">
    <p class="bold text-right" style="width:80px; float:left; line-height:30px;">VMs</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="VM" name="VM">
        <option value="">--Seleccione--</option>
        <?php
        foreach($listadoVM as $row1){
        ?>
            <option value="<?= $row1["VM"] ?>"><?= $row1["VM"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="float:left;">
    <p class="bold text-right" style="width:80px; float:left; line-height:30px;">Start Date</p>
    <input type="text" style="height:30px; width:164px; margin-left:20px; border:2px solid; cursor:pointer;" id="fechaInicio" name="fechaInicio" readonly>
</div>
<div style="float:left;">
    <p class="bold text-right" style="width:70px; float:left; margin-left:20px; line-height:30px;">End Date</p>
    <input type="text" style="height:30px; width:164px; margin-left:20px; border:2px solid; cursor:pointer;" id="fechaFin" name="fechaFin" readonly>
</div>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="search">Search</div>

<br>
<br>

<div style="float:left;">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' of ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Show</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">per page</p>  
</div>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Export Excel</div>

<br>
<br>

<form id="formReportingVM" name="formApps" method="post" enctype="multipart/form-data" action="historicalReportingVM.php">
    <input type="hidden" id="insertar" name="insertar" value="1">
    <div id="tablaReportingVM" style="clear: both; width:100%; max-height:400px; overflow:auto;">
        <table id="listaReportingVM" class="tablap" style="width:1200px; margin:0 auto;">
            <thead>
                <tr>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Host Name</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">Host Date</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM Name</th>
                    <th style="text-align:center; border: 1px solid #000000; cursor:pointer;">VM Date</th>
                    <?= $celdas ?>
                </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            $totFechas = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            
            
            foreach($listadoResumen as $row){
                $valores = "";
                $valoresArray = array();
                $totalArray = array();
                $j = 0;
                for($index = $mesInicio; $index < $mesFin; $index++){
                    $valor = $general->completarCeroNumeros($index);
                    $campo = $valor . '/'.  $yearInicio;
                    if($index > 12){
                        $campo = $valor . '/'.  $yearFin;
                    }
                    //$valores .= '<td style="border: 1px solid; text-align:right;">' . $row[$campo] . '</td>';
                    //$totFechas[$index] += intval($row[$campo]); 
                    $valoresArray[$j] = '<td style="border: 1px solid; text-align:right;">' . $row[$campo] . '</td>';
                    $totalArray[$j] = intval($row[$campo]);
                    $j++;
                }               
                
                $valoresReversed = array_reverse($valoresArray, false);
                $totalReversed = array_reverse($totalArray, false);
                
                for($z = 0; $z < count($valoresReversed); $z++){
                    $valores .= $valoresReversed[$z];
                    $totFechas[$z] += $totalReversed[$z];
                }
            ?>
                <tr>
                    <td style="border: 1px solid;"><?= $row["host"] ?></td>
                    <td style="border: 1px solid;"><?= $row["hostDate"] ?></td>
                    <td style="border: 1px solid;"><?= $row["VM"] ?></td>
                    <td style="border: 1px solid;"><?= $row["VMDate"] ?></td>
                    <?= $valores ?>
                </tr>
                <?php
                $i++;
            }
            ?>
                <tr>
                    <td colspan="4" style="border: 1px solid;" class="bold">Total</td>
                    <?php 
                    //for($index = $mesInicio; $index < $mesFin; $index++){
                    for($index = 0; $index < count($arrayCeldas); $index++){
                    ?>
                        <td style="border: 1px solid; text-align:right;"><?= $totFechas[$index] ?></td>
                    <?php
                    }     
                    ?>
                </tr>
            </tbody>
        </table>
    </div>
</form>
<br>
<div style="float:right;" class="botonesSAM boton5" id="clear">Clear</div>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='indexReports.php'">Back</div>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listaReportingVM").tablesorter();
        $("#listaReportingVM").tableHeadFixer();
        
        $("#fechaInicio").datepicker();
        $("#fechaFin").datepicker();
        
        $("#clear").click(function(){
            $("#VM").val("");
            $("#fechaInicio").val("");
            $("#fechaFin").val("");
            $("#search").click();
        });
      
        $("#search").click(function(){
            if($("#fechaInicio").val() !== "" && $("#fechaFin").val() !== ""){
                arrayFechaIni = $("#fechaInicio").val().split("/");
                arrayFechaF = $("#fechaFin").val().split("/");
                fechaIni = new Date(arrayFechaIni[2], arrayFechaIni[0], arrayFechaIni[1]);
                fechaFin = new Date(arrayFechaF[2], arrayFechaF[0], arrayFechaF[1]);

                if(fechaIni > fechaFin){
                    $.alert.open("warning", "The start date should be less than the end date");
                    return false;
                }
                
                if(((parseInt(arrayFechaF[2]) - parseInt(arrayFechaIni[2])) === 1 && arrayFechaF[0] > arrayFechaIni[0]) 
                || (parseInt(arrayFechaF[2]) - parseInt(arrayFechaIni[2])) > 1){
                    $.alert.open("warning", "The difference in dates should not exceed 12 months");
                    return false;
                }
            } else if(($("#fechaInicio").val() === "" && $("#fechaFin").val() !== "") || ($("#fechaInicio").val() !== "" && $("#fechaFin").val() === "")){
                $.alert.open("warning", "You must select the start date and the end date");
                return false;
            }
            
            
            
            
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/historicalReportingVM.php", { VM : $("#VM").val(), 
            fechaInicio : $("#fechaInicio").val(), fechaFin : $("#fechaFin").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaReportingVM").empty();
                $("#tablaReportingVM").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#limite").change(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/historicalReportingVM.php", { VM : $("#VM").val(), 
            fechaInicio : $("#fechaInicio").val(), fechaFin : $("#fechaFin").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaReportingVM").empty();        
                $("#tablaReportingVM").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#primero").click(function(){
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/historicalReportingVM.php", { VM : $("#VM").val(),
            fechaInicio : $("#fechaInicio").val(), fechaFin : $("#fechaFin").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                $("#tablaReportingVM").empty();
                $("#tablaReportingVM").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#ultimo").click(function(){
            $("#fondo").show();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/historicalReportingVM.php", { VM : $("#VM").val(), 
            fechaInicio : $("#fechaInicio").val(), fechaFin : $("#fechaFin").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaReportingVM").empty();
                $("#tablaReportingVM").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#siguiente").click(function(){
            $("#fondo").show();
            pagina += 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/historicalReportingVM.php", { VM : $("#VM").val(), 
            fechaInicio : $("#fechaInicio").val(), fechaFin : $("#fechaFin").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaReportingVM").empty();
                $("#tablaReportingVM").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });

        $("#atras").click(function(){
            $("#fondo").show();
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/spla/ajax/historicalReportingVM.php", { VM : $("#VM").val(),
            fechaInicio : $("#fechaInicio").val(), fechaFin : $("#fechaFin").val(), pagina : $("#pagina").val(), limite : $("#limite").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaReportingVM").empty();
                $("#tablaReportingVM").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            $("#guardar").hide();
        });	

        $("#exportar").click(function(){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/historicalReportingVM.php?&VM="+$("#VM").val()+"&fechaInicio="+$("#fechaInicio").val()+"&fechaFin="+$("#fechaFin").val();
        });
    });
</script>