var filaLicencias  = 0;
var desactivarBtn  = 0;
var desactivarBtn1 = 0;
$(document).ready(function () {
    $('#contenedor').on('change', '.archivo', function () {
        $(this).parent().children(".url_file").val($(this).val());
    });

    $(".botones_m").click(function () {
        $('div.activado2:first').removeClass("activado2");
        $(this).addClass("activado2");
    });

    $(".botones_m3").click(function () {
        $('div.activado1:first').removeClass("activado1");
        $(this).addClass("activado1");
    });

    $(".botones_m2").click(function () {
        $('div.activado3:first').removeClass("activado3");
        $(this).addClass("activado3");
    });

    $(".botonesg").click(function () {
        $('div.activado4:first').removeClass("activado4");
        $(this).addClass("activado4");
    });

    /*inicio funciones Sam*/
    $("#btnSam").click(function () {
        desactivarBtn++;
        if(desactivarBtn === 1){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/sam.php", { token : localStorage.licensingassuranceToken }, function (data) {
                if(data[0].resultado === false){
                    location.href = "<?= $GLOBALS['domain_root'] ?>";
                    return false;
                }

                if(data[0].sesion === "false"){
                    $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                        location.href = "<?= $GLOBALS['domain_root'] ?>";
                        return false;
                    });
                }

                if(data[0].mensajeAlerta !== ""){
                    $.alert.open("info", data[0].mensajeAlerta, {"Aceptar" : "Aceptar"}, function() {
                    });
                }

                $("#contenedor_ver1").empty();
                $("#contenedor_ver2").empty();
                $("#contenedorCentral").empty();
                $("#panelControl").empty();
                $("#panelControl").append(data[0].panelControl);
                $("#contenedor_ver2").append(data[0].menuSuperior);
                $("#contenedor_ver1").append(data[0].menuLateral);
                $("#contenedorCentral").append(data[0].contenedorCentral);
                desactivarBtn = 0;
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    desactivarBtn = 0;
                });
            });
        }
    });
    /*fin funciones Sam*/
});

/*inicio lista equipos Microsoft Office*/
function listaWebtool(){
    $("#fondo").show();
    $.post("<?= $GLOBALS['domain_root'] ?>/usabilidad/listaEquiposWebtool.php", { token : localStorage.licensingassuranceToken }, function (data) {
        $("#fondo").hide();
        if(data[0].resultado === false){
            location.href = "<?= $GLOBALS['domain_root'] ?>";
            return false;
        }

        if(data[0].sesion === "false"){
            $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                location.href = "<?= $GLOBALS['domain_root'] ?>";
                return false;
            });
        }

        if(data[0].result === 0){
            $.alert.open("alert", "No se pudo crear la carpeta del cliente", {"Aceptar" : "Aceptar"}, function() {
            });
        }else if(data[0].result === 1){
            location.href = "<?= $GLOBALS['domain_root'] ?>/" + data[0].archivo;
        }
    }, "json")
    .fail(function( jqXHR ){
        $("#fondo").hide();
        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
        });
    });
}

function despliegue(opcion){
    $("#tituloDespliegue").empty();
    $("#servidor").empty();
    if(opcion === "solaris"){
        $("#tituloDespliegue").append("Despliegue Solaris");
        $("#servidor").append("Solaris");
        $("#incremento").val("false");
    }
    else if(opcion === "aix"){
        $("#tituloDespliegue").append("Despliegue AIX");
        $("#servidor").append("AIX");
        $("#incremento").val("false");
    }
    else if(opcion === "linux"){
        $("#tituloDespliegue").append("Despliegue Linux");
        $("#servidor").append("Linux");
        $("#incremento").val("false");
    } else if(opcion === "incremento solaris"){
        $("#tituloDespliegue").append("Despliegue Incremento Solaris");
        $("#servidor").append("Solaris");
        $("#incremento").val("true");
    }
    else if(opcion === "incremento aix"){
        $("#tituloDespliegue").append("Despliegue Incremento AIX");
        $("#servidor").append("AIX");
        $("#incremento").val("true");
    }
    else if(opcion === "incremento linux"){
        $("#tituloDespliegue").append("Despliegue Incremento Linux");
        $("#servidor").append("Linux");
        $("#incremento").val("true");
    }
    $("#opcionDespliegue").val(opcion);
    $(".exito_archivo").hide();
    $(".error_archivo").hide();
    $("#mensAdvCompras").hide();
}
    
function comprasRepo() {
    desactivarBtn++;
    if(desactivarBtn === 1){
        document.getElementById("compraRepo").submit();
        desactivarBtn = 0;
    }
}

function comprasRepoF() {
    $("#fondo1").show();
    $("#modal-contratos").show();
}

function buscarLicenciasContrato(){
    $("#modal-contratos").hide();
    $("#fondo1").hide();
    $("#fondo").show();
    $("#tokenContratos").val(localStorage.licensingassuranceToken);
    var formData = new FormData($("#contratoRepoF")[0]);	
    $.ajax({
        type: "POST",
        url:"../plantillas/licencias_repositorioF.php", 
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",  
        cache:false,
        success: function(data){
            $("#tablaLicencias").empty();
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>";
                return false;
            }
            if(data[0].sesion === "false"){
                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                    location.href = "<?= $GLOBALS['domain_root'] ?>";
                    return false;
                });
            }
            $("#fondo").hide();
            $("#tablaLicencias").append(data[0].tabla);
            $("#fondo1").show();
            $("#modal-licenciasCompras").show();

        }
    })
    .fail(function( jqXHR ){
        $("#fondo").hide();
        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
        });
    });
}

function guardarComprasRepoF() {
    desactivarBtn++;
    if(desactivarBtn === 1){
        document.getElementById("compraRepoF").submit();
        desactivarBtn++;
    }
}

function salirModalContratos() {
    for (i = 0; i < $("#tablaLicencias tr").length; i++) {
        $("#check" + i).attr("checked", true);
    }
    $("#modal-contratos").scrollTop(0);
    $("#fondo1").hide();
    $("#modal-contratos").hide();
    $("body").scrollTop(0);
}

function salirModalCompras() {
    for (i = 0; i < $("#tablaLicencias tr").length; i++) {
        $("#check" + i).attr("checked", true);
    }
    $("#modal-licenciasCompras").scrollTop(0);
    $("#modal-licenciasCompras").hide();
    $("#modal-contratos").show();
    $("body").scrollTop(0);
}

function chequearListContratos(){
    if ($("#checkContratosAll").prop("checked")) {
        for (i = 0; i < $("#tablaContratos tr").length; i++) {
            $("#checkContratos" + i).attr("checked", true);
        }
    } else {
        for (i = 0; i < $("#tablaContratos tr").length; i++) {
            $("#checkContratos" + i).removeAttr("checked");
        }
    }
}

function chequearListLicencias() {
    if ($("#checkAll").prop("checked")) {
        for (i = 0; i < $("#tablaLicencias tr").length; i++) {
            $("#check" + i).attr("checked", true);
        }
    } else {
        for (i = 0; i < $("#tablaLicencias tr").length; i++) {
            $("#check" + i).removeAttr("checked");
        }
    }
}

/*function ver_new(actual) {
    $('#contenedor_ver3').hide();
    $('#contenedor_ver2').hide();

    var url = '<?= $GLOBALS["domain_root"] ?>/ver1/sinpermiso.php';
    var pars = 'actual=' + actual;

    desactivarBtn++;
    if(desactivarBtn === 1){
        $.ajax({
            type: "POST",
            url: url,
            data: pars,
            success: function (data) {
                $('#contenedor_ver1').html(data);
                desactivarBtn = 0;
            }
        })
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                desactivarBtn = 0;
            });
        });
    }
}

function ver_new2(actual) {
    $(".botones_m2").click(function () {
        $('div.activado3:first').removeClass("activado3");
        $(this).addClass("activado3");
    });

    $("#fondoMenu1").show();
    var url = '<?= $GLOBALS["domain_root"] ?>/ver4/sinpermiso4.php';

    $("#panelControl").empty();
    $("#panelControl").append('<p class="blanco">1.-Selecciona fabricante</p><p class="blanco">2.-Nivel de Servicio</p><p class="blanco">3.-Completar los pasos</p>');

    desactivarBtn1++;
    if(desactivarBtn1 === 1){
        $.post(url, {actual : actual, direccion : '<?= $GLOBALS["app_root"] ?>/configuracion/inicio.php' }, function(data){                   
            $('#contenedor_ver4').html(data);
            $('#contenedor_ver3').show();

            var url = '<?= $GLOBALS["domain_root"] ?>/ver2/sinpermiso2.php';
            var pars = 'actual=' + actual;

            $.ajax({
                type: "POST",
                url: url,
                data: pars,
                success: function (data) {
                    $('#contenedor_ver2').html(data);
                    $('#contenedor_ver2').show();
                    $(".botones_m3").click(function (event) {
                        $('div.activado1:first').removeClass("activado1");
                        $(this).addClass("activado1");
                    });
                    desactivarBtn1 = 0;
                    $("#fondoMenu1").hide();
                }
            })
            .fail(function( jqXHR ){
                $("#fondoMenu1").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    desactivarBtn1 = 0;
                });
            });
        })
        .fail(function( jqXHR ){
            $("#fondoMenu1").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function(){ 
                desactivarBtn1 = 0;
            });
        });
    }
}

function ver_new3(actual) {
    $(".botones_m2").click(function () {
        $('div.activado3:first').removeClass("activado3");
        $(this).addClass("activado3");
    });

    $("#fondoMenu1").show();
    desactivarBtn1++;
    if(desactivarBtn1 === 1){
        var url = '<?= $GLOBALS["domain_root"] ?>/ver4/sinpermiso4.php';

        $.post(url, { actual : actual, direccion : '<?= $GLOBALS["app_root"] ?>/configuracion/inicio.php'}, function(data){
                $('#contenedor_ver4').html(data);
                desactivarBtn1 = 0;
                $("#fondoMenu1").hide();
        })
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                desactivarBtn1 = 0;
                $("#fondoMenu1").hide();
            });
        });
    }
}*/

function showhide(what) {
    var seccion = document.getElementById(what);

    if (seccion.style.display === 'none') {
        seccion.style.display = 'block';
        if ( $("#export").length > 0 ) {
            $('#export').show();
        }
    } else {
        seccion.style.display = 'none';
        if ( $("#export").length > 0 ) {
            $('#export').hide();
        }
    }
}