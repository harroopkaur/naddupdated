if(data[0].resultado === false){
    $.alert.open('error', "No coinciden los Tokens", {'Aceptar' : 'Aceptar'}, function() {
        location.href = "<?= $GLOBALS['domain_root'] ?>";
        return false;
    }); 
}
if(data[0].sesion === false){
    $.alert.open('error', "Error: " + data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
        location.href = "<?= $GLOBALS['domain_root'] ?>";
        return false;
    });                    
}