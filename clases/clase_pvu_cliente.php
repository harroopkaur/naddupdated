<?php
class pvuCliente extends General {
    public $error;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $tipoProcesador, $producto, $conteoProcesadores, $conteoCore, 
    $usaProcesadores, $usaCore, $pvu, $conteo, $totalPvu) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO pvuCliente (cliente, empleado, servidor, tipoProcesador, "
            . "producto, conteoProcesadores, conteoCore, usaProcesadores, usaCore, pvu, conteo, totalPvu) "
            . "VALUES (:cliente, :empleado, :equipo, :tipoProcesador, :producto, :conteoProcesadores, :conteoCore, :usaProcesadores, "
            . ":usaCore, :pvu, :conteo, :totalPvu)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':tipoProcesador'=>$tipoProcesador, 
            ':producto'=>$producto, ':conteoProcesadores'=>$conteoProcesadores, ':conteoCore'=>$conteoCore, ':usaProcesadores'=>$usaProcesadores, 
            ':usaCore'=>$usaCore, ':pvu'=>$pvu, ':conteo'=>$conteo, ':totalPvu'=>$totalPvu));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO pvuCliente (cliente, empleado, servidor, tipoProcesador, "
            . "producto, conteoProcesadores, conteoCore, usaProcesadores, usaCore, pvu, conteo, totalPvu) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente,$empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM pvuCliente WHERE cliente = :cliente");
            $sql->execute(array('cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $usaProcesadores, $usaCore) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE pvuCliente SET usaProcesadores = :usaProcesadores, usaCore = :usaCore "
            . "WHERE id = :id");
            $sql->execute(array(':id'=>$id,':usaProcesadores'=>$usaProcesadores, ':usaCore'=>$usaCore));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function procesarConsolidadoProcesadores($procesador){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT procesador.descripcion AS procesador,
                    modelo.descripcion AS modelo,
                    tablaPVU.pvu
                FROM tablaPVU
                    INNER JOIN detalleMaestra procesador ON tablaPVU.idProcesador = procesador.idDetalle AND
                    tablaPVU.idMaestraProcesador = procesador.idMaestra AND procesador.idMaestra = 8
                    INNER JOIN detalleMaestra modelo ON tablaPVU.idModeloServidor = modelo.idDetalle AND
                    tablaPVU.idMaestraModelo = modelo.idMaestra AND modelo.idMaestra = 9
                WHERE :procesador LIKE CONCAT('%', procesador.descripcion ,'%')
                AND (:procesador LIKE CONCAT('%', modelo.descripcion ,'%') OR modelo.descripcion = 'todos')");
            $sql->execute(array(':procesador'=>$procesador));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('procesador'=>'', 'modelo'=>'', 'pvu'=>'');
        }
    }
    
    function listadoPVUCliente($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
                . "FROM pvuCliente "
                . "WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}