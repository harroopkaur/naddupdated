<?php
class clase_licenciamiento_centralizador extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    private $idAgente;
    private $nombTabla;
    private $cabecera; 
    private $bloque;
    private $bloqueValores;
    private $insertarBloque;
    private $i;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $fechaIni, $fechaFin, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO licenciasCentralizador (cliente, fechaIni, fechaFin, fechaCreacion,
            serial) '
            . 'VALUES (:cliente, :fechaIni, :fechaFin, NOW(), :serial)');
            $sql->execute(array(':cliente'=>$cliente, ':fechaIni'=>$fechaIni, ':fechaFin'=>$fechaFin, ':serial'=>$serial));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar    
    function actualizar($id, $fechaIni, $fechaFin, $status) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE licenciasCentralizador SET '
            . 'fechaIni = :fechaIni, fechaFin = :fechaFin, status = :status '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':fechaIni'=>$fechaIni, ':fechaFin'=>$fechaFin, ':status'=>$status));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($idLicencia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE licenciasCentralizador SET '
            . 'status = 0 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$idLicencia));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function verificarSerial($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, fechaFin
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 1
                    AND fechaIni <= NOW() AND fechaFin >= NOW()
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function verificarSerial1($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 2
                    AND fechaIni <= NOW() AND fechaFin >= NOW()
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function verificarSerial2($email, $serial, $serialHDD) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, licenciasCentralizador.fechaFin
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 2
                    AND serialHDD = :serialHDD AND fechaIni <= NOW() AND fechaFin >= NOW()
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email, ':serialHDD'=>$serialHDD));
            $row = $sql->fetch();
            if($row["id"] == ""){
                $row["id"] = 0;
                $row["fechaFin"] = "1981-01-01";
            }
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function verificarSerial3($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, fechaFin, clientes.id AS cliente
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 2
                    AND fechaIni <= NOW() AND fechaFin >= NOW()
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01", "cliente"=>0);
        }
    }
    
    /*function verificarSerial4($serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id AS cliente
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 2
                    AND fechaIni < NOW() AND fechaFin > NOW()');
            $sql->execute(array(":serial"=>$serial));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("cliente"=>0);
        }
    }*/
    
    function verificarSerial5($serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, fechaFin, clientes.id AS cliente
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 1
                    AND fechaIni <= NOW() AND fechaFin >= NOW()');
            $sql->execute(array(":serial"=>$serial));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01", "cliente"=>0);
        }
    }
    
    function verificarSchedulingName($serial, $nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(scheduling.tx_schedule) AS cantidad
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 1
                    AND fechaIni <= NOW() AND fechaFin >= NOW()
                    INNER JOIN scheduling ON clientes.id = scheduling.cliente AND tx_descrip = :nombre');
            $sql->execute(array(":serial"=>$serial, ":nombre"=>$nombre));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function obtenerCliente($serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id AS cliente
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 2
                    AND fechaIni <= NOW() AND fechaFin >= NOW()');
            $sql->execute(array(":serial"=>$serial));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function serialActivado($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 2
                    AND fechaIni <= NOW() AND fechaFin >= NOW()
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function activarSerial($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE licenciasCentralizador SET status = 2 WHERE id = :id');
            $sql->execute(array(":id"=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function activarSerial1($id, $serialHDD) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE licenciasCentralizador SET serialHDD = :serialHDD, status = 2 WHERE id = :id');
            $sql->execute(array(":id"=>$id, ":serialHDD"=>$serialHDD));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function datos($idLicencia) {
        try{            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                serial,
                DATE_FORMAT(fechaIni, "%d/%m/%Y") AS fechaIni,
                DATE_FORMAT(fechaFin, "%d/%m/%Y") AS fechaFin,
                DATE_FORMAT(fechaCreacion, "%d/%m/%Y") AS fechaCreacion,
                status
            FROM licenciasCentralizador
            WHERE id = :id');
            $sql->execute(array(':id'=>$idLicencia));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "serial"=>"", "fechaIni"=>"", "fechaFin"=>"", "fechaCreacion"=>"", "status"=>0);
        }
    }
    
    function idCliente($serial) {
        try{            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id
                FROM clientes
                     INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente
                WHERE licenciasCentralizador.serial = :serial');
            $sql->execute(array(':serial'=>$serial));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listar_todo_paginado($cliente, $pagina) {
        try{
            $inicio = ($pagina * $this->limit_paginacion) - $this->limit_paginacion; 
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                serial,
                DATE_FORMAT(fechaIni, "%d/%m/%Y") AS fechaIni,
                DATE_FORMAT(fechaFin, "%d/%m/%Y") AS fechaFin,
                DATE_FORMAT(fechaCreacion, "%d/%m/%Y") AS fechaCreacion,
                status
            FROM licenciasCentralizador
            WHERE cliente = :cliente
            ORDER BY fechaIni ASC 
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM licenciasCentralizador WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    } 
    function serial_existe($serial, $id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM licenciasCentralizador WHERE serial = :serial AND id != :id');
            $sql->execute(array(':serial'=>$serial, ':id'=>$id));
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function datosAgente($centralizadorSerial, $tx_host_name, $tx_serial_disco){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id
                FROM agente
                    INNER JOIN clientes ON agente.cliente = clientes.id
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND
                    licenciasCentralizador.serial = :serial
                WHERE agente.tx_host_name = :tx_host_name AND agente.tx_serial_disco = :tx_serial_disco');
            $sql->execute(array(':serial'=>$centralizadorSerial, ':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "tx_host_name"=>"", "tx_serial_disco"=>"", "tx_ip"=>"");
        }
    }
    
    function datosAgente1($hostName, $serialCentralizador){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id
                FROM agente
                     INNER JOIN agenteRelCentralizador ON agente.id = agenteRelCentralizador.id_agente
                     INNER JOIN licenciasCentralizador ON agenteRelCentralizador.cliente = licenciasCentralizador.cliente
                WHERE agente.tx_host_name = :tx_host_name AND licenciasCentralizador.serial = :serialCentralizador');
            $sql->execute(array(':tx_host_name'=>$hostName, ':serialCentralizador'=>$serialCentralizador));
            return $sql->fetch();
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return array("id"=>0);
        }
    }
    
    function datosAgente2($tx_host_name, $tx_serial_disco, $serial){
        try{
            $this->conexion();         
            $sql = $this->conn->prepare('SELECT agente.id, agente.cliente, tx_host_name, tx_serial_disco, tx_ip '
                . 'FROM agente '
                . 'INNER JOIN licenciasCentralizador ON agente.cliente = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial '
                . 'WHERE tx_host_name = :tx_host_name AND tx_serial_disco = :tx_serial_disco');
            $sql->execute(array(':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco, ':serial'=>$serial));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "tx_host_name"=>"", "tx_serial_disco"=>"", "tx_ip"=>"");
        }
    }
    
    function datosAgente2Serveris($tx_host_name, $serial){
        try{
            $this->conexion();         
            $sql = $this->conn->prepare('SELECT agente.id, agente.cliente, tx_host_name, tx_serial_disco, tx_ip '
                . 'FROM agente '
                . 'INNER JOIN licenciasCentralizador ON agente.cliente = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial '
                . 'WHERE tx_host_name = :tx_host_name');
            $sql->execute(array(':tx_host_name'=>$tx_host_name, ':serial'=>$serial));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "tx_host_name"=>"", "tx_serial_disco"=>"", "tx_ip"=>"");
        }
    }
    
    Public Function insertarConfig($cliente, $description, $val){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO configCentralizador (cliente, description, val) 
            VALUES (:cliente, :description, :val)');
            $sql->execute(array(':cliente'=>$cliente, ':description'=>$description, ':val'=>$val));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    Public Function actualizarConfigAgenteInactivo($id, $val){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE configCentralizador SET val = :val WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':val'=>$val));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarAgente($tx_host_name, $tx_serial_disco, $tx_ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO agente (tx_host_name, tx_serial_disco, tx_ip) '
            . 'VALUES (:hostname, :serialDisk, :ipHost)');
            $sql->execute(array(':hostname'=>$tx_host_name, ':serialDisk'=>$tx_serial_disco, ':ipHost'=>$tx_ip));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarAgenteWeb($cliente, $tx_host_name, $tx_serial_disco, $tx_ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO agente (cliente, tx_host_name, tx_serial_disco, tx_ip) '
            . 'VALUES (:cliente, :tx_host_name, :tx_serial_disco, :tx_ip)');
            $sql->execute(array(':cliente'=>$cliente, ':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco, 
            ':tx_ip'=>$tx_ip));
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function ultIdAgenteWeb() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT max(id) AS id FROM agente');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function actualizarAgenteWeb($id_agente, $tx_serial_disco, $tx_ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE agente SET tx_serial_disco = :tx_serial_disco, tx_ip = :tx_ip WHERE id = :id_agente');
            $sql->execute(array(':tx_serial_disco'=>$tx_serial_disco, ':tx_ip'=>$tx_ip, ':id_agente'=>$id_agente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarRelAgenteWeb($cliente, $idAgente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO agenteRelCentralizador (cliente, id_agente) '
            . 'VALUES (:cliente, :idAgente)');
            $sql->execute(array(':cliente'=>$cliente, ':idAgente'=>$idAgente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function asociarAgenteWebScheduling($id, $agente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO agente_scheduling (id_scheduling, id_agente) '
            . 'VALUES (:id, :agente)');
            $sql->execute(array(':id'=>$id, ':agente'=>$agente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function elimAsocAgenteWebScheduling($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_scheduling WHERE id_scheduling = :id_scheduling');
            $sql->execute(array(':id_scheduling'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function ultIdAgente(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id FROM agente');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function eliminarDatos($idAgente, $tipo = "L"){
        if ($tipo == "L"){
            $this->eliminarAddRemove($idAgente);
            $this->eliminarDesinstalacion($idAgente);
            $this->eliminarLlave($idAgente);
            $this->eliminarProcesador($idAgente);
            $this->eliminarProceso($idAgente);
            $this->eliminarResultadoEscaneo($idAgente);
            $this->eliminarSQLData($idAgente);
            $this->eliminarSeguridad($idAgente);
            $this->eliminarSerialMaquina($idAgente);
            $this->eliminarServicio($idAgente);
            $this->eliminarSistemaOperativo($idAgente);
            $this->eliminarTipoEquipo($idAgente);
            $this->eliminarUsuarioEquipo($idAgente);
            $this->eliminarUsabilidadSoftware($idAgente);
            $this->eliminarMemoria($idAgente);
            $this->eliminarAutodesk($idAgente);
        } else if($tipo == "M"){
            $this->eliminarMeteringErrors($idAgente);
        }
        return true;
    }
            
    function eliminarAddRemove($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_add_remove WHERE id_agente = :id_agente'
            . '');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSerialMaquina($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_serial_maquina WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSQLData($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_sql_data WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarServicio($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_servicio WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarProcesador($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_procesador WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSistemaOperativo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_sistema_operativo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarTipoEquipo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_tipo_equipo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarUsuarioEquipo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_usuario_equipo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarProceso($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_proceso WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarDesinstalacion($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_desinstalacion WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSeguridad($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_seguridad WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarLlave($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_llave WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarResultadoEscaneo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_resultado_escaneo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarUsabilidadSoftware($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_usabilidad_software WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarMemoria($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_memoria WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarAutodesk($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_autodesk WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarMeteringErrors($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_metering_errors WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarResults() {
        $this->conexion();
        $query = "INSERT INTO " . $this->nombTabla . " (" . $this->cabecera . ") ";
        $query .= "VALUES " . $this->bloque;

        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($this->bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarHistorico($idAgente){
        $this->conexion();
        $query = "INSERT INTO agente_hist (id_agente, fe_ejeccn) VALUES (:id_agente, NOW())";

        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id_agente'=>$idAgente));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function procesar($datos, $idAgente, $opc){
        $this->cabeceraInsert($opc);
        
        $error = 0;
        $this->i = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        $this->idAgente = $idAgente;

        foreach($datos as $row){
            $this->montarData($row, $opc); 
             
            if ($this->i == 1000){
                if(!$this->insertarResults()){ 
                    echo "Results: " . $this->error;
                    $error = 1;
                }

                $this->bloque = "";
                $this->bloqueValores = array();
                $this->i = -1;
                $this->insertarBLoque = false; 
            }
            $this->i++;
        }

        if($this->insertarBloque === true){
            if(!$this->insertarResults()){ 
                echo "Results: " . $this->error;
                $error = 1;
            }
        }
        
        return $error;
    }
    
    function cabeceraInsert($opc){
        switch ($opc) {
            case "add_remove":
                $this->nombTabla = "agente_add_remove";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_regist, tx_editor, tx_verson, tx_dia_instlc, tx_softwr";
                break;
            case "serial_maquina":
                $this->nombTabla = "agente_serial_maquina";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_serial_maquna";
                break;
            case "sql_data":
                $this->nombTabla = "agente_sql_data";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_llave_regist, tx_edicon, tx_verson, tx_ruta_instlc";
                break;
            case "servicio":
                $this->nombTabla = "agente_servicio";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_nb_prodct, tx_estado";
                break;
            case "procesador":
                $this->nombTabla = "agente_procesador";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_tipo_cpu, tx_nu_cpu, tx_nu_cores,
                tx_procsd_logico, tx_tipo_escano";
                break;
            case "sistema_operativo":
                $this->nombTabla = "agente_sistema_operativo";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_sistma_opertv, tx_fe_creacn";
                break;
            case "tipo_equipo":
                $this->nombTabla = "agente_tipo_equipo";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_fabrcn, tx_modelo";
                break;
            case "usuario_equipo":
                $this->nombTabla = "agente_usuario_equipo";
                //$this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_domino, tx_usuario, tx_ip";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_domino, tx_usuario";
                break;
            case "proceso":
                $this->nombTabla = "agente_proceso";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_nb_procso, tx_ruta_procso";
                break;
            case "desinstalacion":
                $this->nombTabla = "agente_desinstalacion";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_descrp, tx_fe_desins, tx_usuaro_desins";
                break;
            case "seguridad":
                $this->nombTabla = "agente_seguridad";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_hot_name, tx_captin, tx_descrp, tx_fix_commnt, tx_hotfix_id, 
                tx_instll_date, tx_install_by, tx_install_on, tx_name, tx_servce_pack, tx_status";
                break;
            case "llave":
                $this->nombTabla = "agente_llave";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_hot_name, tx_tipo_llave, tx_llave";
                break;
            case "resultado_escaneo":
                $this->nombTabla = "agente_resultado_escaneo";
                $this->cabecera = "id_agente, tx_hot_name, tx_status, tx_error";
                break;
            case "usabilidad_software":
                $this->nombTabla = "agente_usabilidad_software";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_hot_name, tx_softwr, tx_last_exec";
                break;
            case "memoria":
                $this->nombTabla = "agente_memoria";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, dbl_memoria, dbl_memoria_disp";
                break;
            case "autodesk":
                $this->nombTabla = "agente_autodesk";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_regist, tx_producto, tx_verson, tx_serial";
                break;
            case "agente_metering_errors":
                $this->nombTabla = "agente_metering_errors";
                $this->cabecera = "id_agente, equipo, error, origen, descripcion";
                break;
            case "agente_metering_results":
                $this->nombTabla = "agente_metering_results";
                $this->cabecera = "id_agente, equipo, proceso, pid, creado, horaInicio, finalizado, horaFin, duracion";
                break;
        }
    }
    
    function montarData($row, $opc){
        switch ($opc) {
            case "add_remove":
                $this->datosAddRemove($row);
                break;
            case "serial_maquina":
                $this->datosSerialMaquina($row);
                break;
            case "sql_data":
                $this->datosSQLData($row);
                break;
            case "servicio":
                $this->datosServicio($row);
                break;
            case "procesador":
                $this->datosProcesador($row);
                break;
            case "sistema_operativo":
                $this->datosSistemaOperativo($row);
                break;
            case "tipo_equipo":
                $this->datosTipoEquipo($row);
                break;
            case "usuario_equipo":
                $this->datosUsuarioEquipo($row);
                break;
            case "proceso":
                $this->datosProceso($row);
                break;
            case "desinstalacion":
                $this->datosDesinstalacion($row);
                break;
            case "seguridad":
                $this->datosSeguridad($row);
                break;
            case "llave":
                $this->datosLlave($row);
                break;
            case "resultado_escaneo":
                $this->datosResultadoEscaneo($row);
                break;
            case "usabilidad_software":
                $this->datosResultadoUsabilidadSoftware($row);
                break;
            case "memoria":
                $this->datosResultadoMemoria($row);
                break;
            case "autodesk":
                $this->datosResultadoAutodesk($row);
                break;
            case "agente_metering_errors":
                $this->datosResultadoMeteringErrors($row);
                break;
            case "agente_metering_results":
                $this->datosResultadoMeteringResults($row);
                break;
        }       
    }
    
    function datosAddRemove($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_regist" . $this->i . ",
            :tx_editor" . $this->i . ", :tx_verson" . $this->i . ", :tx_dia_instlc" . $this->i . ", :tx_softwr" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_regist" . $this->i . ",
            :tx_editor" . $this->i . ", :tx_verson" . $this->i . ", :tx_dia_instlc" . $this->i . ", :tx_softwr" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_regist" . $this->i] = $row["tx_regist"];
        $this->bloqueValores[":tx_editor" . $this->i] = $row["tx_editor"];
        $this->bloqueValores[":tx_verson" . $this->i] = $row["tx_verson"];
        $this->bloqueValores[":tx_dia_instlc" . $this->i] = $row["tx_dia_instlc"];
        $this->bloqueValores[":tx_softwr" . $this->i] = $row["tx_softwr"];    
    }
    
    function datosSerialMaquina($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_serial_maquna" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_serial_maquna" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_serial_maquna" . $this->i] = $row["tx_serial_maquna"];
    }
    
    function datosSQLData($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_llave_regist" . $this->i . ", :tx_edicon" . $this->i . ", :tx_verson" . $this->i . ", :tx_ruta_instlc" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_llave_regist" . $this->i . ", :tx_edicon" . $this->i . ", :tx_verson" . $this->i . ", :tx_ruta_instlc" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_llave_regist" . $this->i] = $row["tx_llave_regist"];
        $this->bloqueValores[":tx_edicon" . $this->i] = $row["tx_edicon"];
        $this->bloqueValores[":tx_verson" . $this->i] = $row["tx_verson"];
        $this->bloqueValores[":tx_ruta_instlc" . $this->i] = $row["tx_ruta_instlc"];
    }
    
    function datosServicio($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_prodct" . $this->i . ", :tx_estado" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_prodct" . $this->i . ", :tx_estado" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_nb_prodct" . $this->i] = $row["tx_nb_prodct"];
        $this->bloqueValores[":tx_estado" . $this->i] = $row["tx_estado"];
    }
    
    function datosProcesador($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", 
            :tx_tipo_cpu" . $this->i . ", :tx_nu_cpu" . $this->i . ", :tx_nu_cores" . $this->i . ", :tx_procsd_logico" . $this->i . ", "
            . ":tx_tipo_escano" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", 
            :tx_tipo_cpu" . $this->i . ", :tx_nu_cpu" . $this->i . ", :tx_nu_cores" . $this->i . ", :tx_procsd_logico" . $this->i . ", "
            . ":tx_tipo_escano" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_tipo_cpu" . $this->i] = $row["tx_tipo_cpu"];
        $this->bloqueValores[":tx_nu_cpu" . $this->i] = $row["tx_nu_cpu"];
        $this->bloqueValores[":tx_nu_cores" . $this->i] = $row["tx_nu_cores"];
        $this->bloqueValores[":tx_procsd_logico" . $this->i] = $row["tx_procsd_logico"];
        $this->bloqueValores[":tx_tipo_escano" . $this->i] = $row["tx_tipo_escano"];
    }
    
    function datosSistemaOperativo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_sistma_opertv" . $this->i . ", :tx_fe_creacn" . $this->i . ")";
        } else {
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_sistma_opertv" . $this->i . ", :tx_fe_creacn" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_sistma_opertv" . $this->i] = $row["tx_sistma_opertv"];
        $this->bloqueValores[":tx_fe_creacn" . $this->i] = $row["tx_fe_creacn"];
    }
    
    function datosTipoEquipo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_fabrcn" . $this->i . ", :tx_modelo" . $this->i . ")";
        } else {
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_fabrcn" . $this->i . ", :tx_modelo" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_fabrcn" . $this->i] = $row["tx_fabrcn"];
        $this->bloqueValores[":tx_modelo" . $this->i] = $row["tx_modelo"];
    }
    
    function datosUsuarioEquipo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            /*$this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ", :tx_ip" . $this->i . ")";*/
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ")";
        } else {
            /*$this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ", :tx_ip" . $this->i . ")";*/
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_domino" . $this->i] = $row["tx_domino"];
        $this->bloqueValores[":tx_usuario" . $this->i] = $row["tx_usuario"];
        //$this->bloqueValores[":tx_ip" . $this->i] = $row["tx_ip"];
    }
    
    function datosProceso($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_procso" . $this->i . ", :tx_ruta_procso" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_procso" . $this->i . ", :tx_ruta_procso" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_nb_procso" . $this->i] = $row["tx_nb_procso"];
        $this->bloqueValores[":tx_ruta_procso" . $this->i] = $row["tx_ruta_procso"];
    }
    
    function datosDesinstalacion($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_descrp" . $this->i . ", :tx_fe_desins" . $this->i . ", :tx_usuaro_desins" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_descrp" . $this->i . ", :tx_fe_desins" . $this->i . ", :tx_usuaro_desins" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_descrp" . $this->i] = $row["tx_descrp"];
        $this->bloqueValores[":tx_fe_desins" . $this->i] = $row["tx_fe_desins"];
        $this->bloqueValores[":tx_usuaro_desins" . $this->i] = $row["tx_usuaro_desins"];
    }
    
    function datosSeguridad($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", 
            :tx_captin" . $this->i . ", :tx_descrp" . $this->i . ", :tx_fix_commnt" . $this->i . ", :tx_hotfix_id" . $this->i . ", 
            :tx_instll_date" . $this->i . ", :tx_install_by" . $this->i . ", :tx_install_on" . $this->i . ", :tx_name" . $this->i . ", "
            . ":tx_servce_pack" . $this->i . ", :tx_status" . $this->i . ")";
        } else {
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", 
            :tx_captin" . $this->i . ", :tx_descrp" . $this->i . ", :tx_fix_commnt" . $this->i . ", :tx_hotfix_id" . $this->i . ", 
            :tx_instll_date" . $this->i . ", :tx_install_by" . $this->i . ", :tx_install_on" . $this->i . ", :tx_name" . $this->i . ", "
            . ":tx_servce_pack" . $this->i . ", :tx_status" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_captin" . $this->i] = $row["tx_captin"];
        $this->bloqueValores[":tx_descrp" . $this->i] = $row["tx_descrp"];
        $this->bloqueValores[":tx_fix_commnt" . $this->i] = $row["tx_fix_commnt"];
        $this->bloqueValores[":tx_hotfix_id" . $this->i] = $row["tx_hotfix_id"];
        $this->bloqueValores[":tx_instll_date" . $this->i] = $row["tx_instll_date"];
        $this->bloqueValores[":tx_install_by" . $this->i] = $row["tx_install_by"];
        $this->bloqueValores[":tx_install_on" . $this->i] = $row["tx_install_on"];
        $this->bloqueValores[":tx_name" . $this->i] = $row["tx_name"];
        $this->bloqueValores[":tx_servce_pack" . $this->i] = $row["tx_servce_pack"];
        $this->bloqueValores[":tx_status" . $this->i] = $row["tx_status"];
    } 
    
    function datosLlave($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_tipo_llave" . $this->i . ", :tx_llave" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_tipo_llave" . $this->i . ", :tx_llave" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_tipo_llave" . $this->i] = $row["tx_tipo_llave"];
        $this->bloqueValores[":tx_llave" . $this->i] = $row["tx_llave"];
    }
    
    function datosResultadoEscaneo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_hot_name" . $this->i . ", :tx_status" . $this->i . ", "
            . ":tx_error" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_hot_name" . $this->i . ", :tx_status" . $this->i . ", "
            . ":tx_error" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_status" . $this->i] = $row["tx_status"];
        $this->bloqueValores[":tx_error" . $this->i] = $row["tx_error"];
    }
    
    function datosResultadoUsabilidadSoftware($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_softwr" . $this->i . ", :tx_last_exec" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_softwr" . $this->i . ", :tx_last_exec" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_softwr" . $this->i] = $row["tx_softwr"];
        $this->bloqueValores[":tx_last_exec" . $this->i] = $row["tx_last_exec"];
    }
    
    function datosResultadoMemoria($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":dbl_memoria" . $this->i . ", :dbl_memoria_disp" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":dbl_memoria" . $this->i . ", :dbl_memoria_disp" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":dbl_memoria" . $this->i] = $row["dbl_memoria"];
        $this->bloqueValores[":dbl_memoria_disp" . $this->i] = $row["dbl_memoria_disp"];
    }
    
    function datosResultadoAutodesk($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_regist" . $this->i . ", :tx_producto" . $this->i . ", :tx_verson" . $this->i . ", :tx_serial" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_regist" . $this->i . ", :tx_producto" . $this->i . ", :tx_verson" . $this->i . ", :tx_serial" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_regist" . $this->i] = $row["tx_regist"];
        $this->bloqueValores[":tx_producto" . $this->i] = $row["tx_producto"];
        $this->bloqueValores[":tx_verson" . $this->i] = $row["tx_verson"];
        $this->bloqueValores[":tx_serial" . $this->i] = $row["tx_serial"];
    }
    
    function datosResultadoMeteringErrors($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :equipo" . $this->i . ", :error" . $this->i . ", "
            . ":origen" . $this->i . ", :descripcion" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :equipo" . $this->i . ", :error" . $this->i . ", "
            . ":origen" . $this->i . ", :descripcion" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":equipo" . $this->i] = $row["equipo"];
        $this->bloqueValores[":error" . $this->i] = $row["nu_error"];
        $this->bloqueValores[":origen" . $this->i] = $row["origen"];
        $this->bloqueValores[":descripcion" . $this->i] = $row["descripcion"];
    }
    
    function datosResultadoMeteringResults($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :equipo" . $this->i . ", :proceso" . $this->i . ", "
            . ":pid" . $this->i . ", :creado" . $this->i . ", :inicio" . $this->i . ", :finalizado" . $this->i . ", "
            . ":fin" . $this->i . ", :duracion" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :equipo" . $this->i . ", :proceso" . $this->i . ", "
            . ":pid" . $this->i . ", :creado" . $this->i . ", :inicio" . $this->i . ", :finalizado" . $this->i . ", "
            . ":fin" . $this->i . ", :duracion" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":equipo" . $this->i] = $row["equipo"];
        $this->bloqueValores[":proceso" . $this->i] = $row["proceso"];
        $this->bloqueValores[":pid" . $this->i] = $row["pid"];
        $this->bloqueValores[":creado" . $this->i] = $row["creado"];
        $this->bloqueValores[":inicio" . $this->i] = $row["hora_inicio"];
        $this->bloqueValores[":finalizado" . $this->i] = $row["finalizado"];
        $this->bloqueValores[":fin" . $this->i] = $row["hora_fin"];
        $this->bloqueValores[":duracion" . $this->i] = $row["duracion"];
    }
    
    function listaAgente($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id, agente.tx_host_name, agente.tx_ip, IFNULL(MAX(agente_hist.fe_ejeccn), "") AS fe_ejeccn,
                IF(DATEDIFF(NOW(), IFNULL(MAX(agente_hist.fe_ejeccn), "1981-01-01")) > (SELECT val FROM agente_config WHERE id = 1), "Off", "On") AS estado
                FROM agenteRelCentralizador
                     INNER JOIN agente ON agenteRelCentralizador.id_agente = agente.id
                     LEFT JOIN agente_hist ON agente.id = agente_hist.id_agente
                WHERE agenteRelCentralizador.cliente = :cliente
                GROUP BY agente.id, agente.tx_host_name, agente.tx_ip');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function elimAgente($idAgente, $cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM agenteRelCentralizador WHERE id_agente = :idAgente AND cliente = :cliente');
            $sql->execute(array(':idAgente'=>$idAgente, ':cliente'=>$cliente));
            $row = $sql->fetch();
            if ($row["cantidad"] > 0){
                $sql = $this->conn->prepare('DELETE FROM agente WHERE id = :idAgente');
                $sql->execute(array(':idAgente'=>$idAgente));
                return true;
            } 
            return false;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function verifNombScheduling($nombre, $cliente, $tipo = "L"){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, COUNT(id) AS cantidad
                FROM scheduling
                WHERE tx_descrip = :nombre AND cliente = :cliente AND tx_tipo_scheduling = :tipo');
            $sql->execute(array(':nombre'=>$nombre, ':cliente'=>$cliente, ':tipo'=>$tipo));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "cantidad"=>0);
        }
    }
    
    function dataScheduling($nombre, $cliente, $tipo = "L"){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM scheduling
                WHERE tx_descrip = :nombre AND cliente = :cliente AND tx_tipo_scheduling = :tipo');
            $sql->execute(array(':nombre'=>$nombre, ':cliente'=>$cliente, ':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function agregarScheduling($cliente, $tx_descrip, $fe_fecha_inicio, $tx_schedule, $int_repeat_days, 
    $int_repeat_weeks, $tx_days, $tx_months, $num_days){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO scheduling (cliente, tx_descrip, fe_fecha_creacion, fe_fecha_inicio, tx_schedule,
            int_repeat_days, int_repeat_weeks, tx_days, tx_months, num_days) VALUES (:cliente, :tx_descrip, NOW(), 
            :fe_fecha_inicio, :tx_schedule, :int_repeat_days, :int_repeat_weeks, :tx_days, :tx_months, :num_days)');
            $sql->execute(array(':cliente'=>$cliente, ':tx_descrip'=>$tx_descrip, ':fe_fecha_inicio'=>$fe_fecha_inicio, 
            ':tx_schedule'=>$tx_schedule, ':int_repeat_days'=>$int_repeat_days, ':int_repeat_weeks'=>$int_repeat_weeks, 
            ':tx_days'=>$tx_days, ':tx_months'=>$tx_months, ':num_days'=>$num_days));
            return 1;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function updateScheduling($id, $tx_descrip, $fe_fecha_inicio, $tx_schedule, $int_repeat_days, 
    $in_repeat_weeks, $tx_days, $tx_months, $num_days){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE scheduling SET tx_descrip = :tx_descrip, fe_fecha_inicio = :fe_fecha_inicio, 
            tx_schedule = :tx_schedule, int_repeat_days = :int_repeat_days, in_repeat_weeks = :in_repeat_weeks, tx_days = :tx_days, 
            tx_months = :tx_months, num_days = :num_days, tiny_status = 1 WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':tx_descrip'=>$tx_descrip, ':fe_fecha_inicio'=>$fe_fecha_inicio, 
            ':tx_schedule'=>$tx_schedule, ':int_repeat_days'=>$int_repeat_days, ':in_repeat_weeks'=>$in_repeat_weeks, 
            ':tx_days'=>$tx_days, ':tx_months'=>$tx_months, ':num_days'=>$num_days));
            return 1;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
}
?>