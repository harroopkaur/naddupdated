<?php
class clase_visitas extends General{
    public $error;
    
    function insertar($ip, $pais, $modulo = "Home"){
        try{
            if($pais == ""){
                $pais = "Desconocido";
            }
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO visitas (ip, pais, modulo, fecha) VALUES (:ip, :pais, :modulo, NOW())');
            $sql->execute(array(':ip'=>$ip, ':pais'=>$pais, ':modulo'=>$modulo));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function visitasPorIP($pagina, $fechaIni, $fechaFin){
        try{
            $where =  "";
            $array = array();
            
            if($pagina != ""){
                $where .= "WHERE modulo = :modulo ";
                $array[":modulo"] = $pagina;
            }
            
            if($fechaIni != "" && $fechaFin != ""){
                if($where == ""){
                    $where .= "WHERE ";
                }
                
                $where .= "fecha BETWEEN :fechaIni AND :fechaFin ";
                $array[":fechaIni"] = $fechaIni . " 00:00:00";
                $array[":fechaFin"] = $fechaFin . " 23:59:59";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IP,
                    COUNT(IP) AS cantidad,
                    modulo
                FROM visitas
                ' . $where . '
                GROUP BY IP');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }
    
    function visitasPorPais($pagina, $fechaIni, $fechaFin){
        try{
            $where =  "";
            $array = array();
            
            if($pagina != ""){
                $where .= "WHERE modulo = :modulo ";
                $array[":modulo"] = $pagina;
            }
            
            if($fechaIni != "" && $fechaFin != ""){
                if($where == ""){
                    $where .= "WHERE ";
                }
                
                $where .= "fecha BETWEEN :fechaIni AND :fechaFin ";
                $array[":fechaIni"] = $fechaIni . " 00:00:00";
                $array[":fechaFin"] = $fechaFin . " 23:59:59";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT pais,
                    COUNT(pais) AS cantidad,
                    modulo
                FROM visitas
                ' . $where . '
                GROUP BY pais');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }
    
    function visitas($pagina, $fechaIni, $fechaFin){
        try{
            $where =  "";
            $array = array();
            
            if($pagina != ""){
                $where .= "WHERE modulo = :modulo ";
                $array[":modulo"] = $pagina;
            }
            
            if($fechaIni != "" && $fechaFin != ""){
                if($where == ""){
                    $where .= "WHERE ";
                }
                
                $where .= "fecha BETWEEN :fechaIni AND :fechaFin ";
                $array[":fechaIni"] = $fechaIni . " 00:00:00";
                $array[":fechaFin"] = $fechaFin . " 23:59:59";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IP, 
                    pais,
                    DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha,
                    modulo
                FROM visitas
                ' . $where);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }
    
    function visitasSPLA(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "SAM as a Service for SPLA"');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasSAMColomPanama(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "SAM as a Service"
                AND pais IN ("Colombia", "Panama")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasSAMEcuadCostaRicaDominicana(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "SAM as a Service"
                AND pais IN ("Ecuador", "Costa Rica", "Republica Dominicana")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasSAMMexicoBolivia(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "SAM as a Service"
                AND pais IN ("Mexico", "Bolivia")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasSAMGuateSalvaTriniHondurNicara(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "SAM as a Service"
                AND pais IN ("Guatemala", "El Salvador", "Trinidad", "Honduras", "Nicaragua")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasSAMChileArgentPeru(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "SAM as a Service"
                AND pais IN ("Chile", "Argentina", "Peru")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasAuditDefenseColombiaPanama(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "Audit Defense"
                AND pais IN ("Colombia", "Panama")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasAuditDefenseEcuadCostaRicaDominicana(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "Audit Defense"
                AND pais IN ("Ecuador", "Costa Rica", "Republica Dominicana")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasAuditDefenseMexicoBolivia(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "Audit Defense"
                AND pais IN ("Mexico", "Bolivia")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasAuditDefenseGuateSalvaTriniHondurNicara(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "Audit Defense"
                AND pais IN ("Guatemala", "El Salvador", "Trinidad", "Honduras", "Nicaragua")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasAuditDefenseChileArgentPeru(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "Audit Defense"
                AND pais IN ("Chile", "Argentina", "Peru")');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasDeploymentDiagnostic(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas 
                WHERE fecha BETWEEN CONCAT(CURDATE(), " 00:00:00") AND CONCAT(CURDATE(), " 23:59:59") AND modulo = "Deployment Diagnostic"');
            $sql->execute();
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
    
    function visitasSemanales($modulo, $in){
        try{
            $where = "";
            if($in != ""){
                $where = ' AND pais IN (' . $in . ')';
            }
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM visitas     
                WHERE fecha BETWEEN date_sub(CONCAT(CURDATE(), " 00:00:00"), INTERVAL 5 DAY) AND date_sub(CONCAT(CURDATE(), 
                " 23:59:59"), INTERVAL 1 DAY) AND modulo = :modulo' . $where);
            $sql->execute(array(":modulo"=>$modulo));
            return $sql->fetchAll();
        } catch(PDOException $e){
            return array();
        }
    }
}
