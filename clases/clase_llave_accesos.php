<?php
class clase_llave_accesos extends clase_general_licenciamiento{ 
    public $error = "";
    
    function insertar($serial, $accesos) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('INSERT INTO admin004 (serial, accesos) '
            . 'VALUES (:serial, :accesos)');
            $sql->execute(array(':serial'=>$serial, ':accesos'=>$accesos));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($serial, $accesos) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('UPDATE admin004 SET '
            . 'status = 1 '
            . 'WHERE serial = :serial AND accesos = :accesos');
            $sql->execute(array(':serial'=>$serial, ':accesos'=>$accesos));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarTodo($serial) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('UPDATE admin004 SET status = 0 WHERE serial = :serial');
            $sql->execute(array(':serial'=>$serial));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($serial, $accesos) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('UPDATE admin004 SET status = 0 WHERE serial = :serial AND accesos = :accesos');
            $sql->execute(array(':serial'=>$serial, ':accesos'=>$accesos));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listado($serial) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT accesos '
                . 'FROM admin004 '
                . 'WHERE serial = :serial AND status = 1');
            $sql->execute(array(':serial'=>$serial));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function serial_existe($serial, $accesos) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT * FROM admin004 WHERE serial = :serial AND accesos = :accesos');
            $sql->execute(array(':serial'=>$serial, ':accesos'=>$accesos));
            $row = $sql->fetch();
            if(count($row['serial']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
}