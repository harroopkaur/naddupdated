<?php
class balanceVMWare extends General{
    ########################################  Atributos  ########################################
    public  $lista = array();
    public  $listaNoIncluir = array();
    public  $error = NULL;
    var $id;
    var $cliente;
    var $officce;
    var $instalaciones;
    var $compra;
    var $balance;
    var $tipo;
    var $archivo;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $office, $version, $precio, $instalaciones, $compra, $balance, $balancec, $tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_VMWare(cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo) VALUES '
            . '(:cliente, :empleado, :familia, :office, :version, :precio, :instalaciones, :compra, :balance, :balancec, :tipo)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':office'=>$office, ':version'=>$version, ':precio'=>$precio, ':instalaciones'=>$instalaciones, ':compra'=>$compra, ':balance'=>$balance, ':balancec'=>$balancec, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM balance_VMWare WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function imprimirTablaExcelBalanza($tabla, $objPHPExcel, $i){
        foreach ($tabla as $reg_equipos) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $reg_equipos["familia"])
                ->setCellValue('B' . $i, $reg_equipos["office"])
                ->setCellValue('C' . $i, $reg_equipos["version"])
                ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                ->setCellValue('E' . $i, $reg_equipos["compra"])
                ->setCellValue('F' . $i, $reg_equipos["balance"])
                ->setCellValue('G' . $i, $reg_equipos["balancec"]);
            $i++;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function productosNoIncluir($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia
                FROM balance_VMWare
                WHERE cliente = :cliente AND empleado = :empleado
                    familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 != "Otros" AND idMaestra = 1)
                GROUP BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaNoIncluir = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM balance_VMWare
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_VMWare WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias2($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM balance_VMWare
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    /*function listar_todo_familiasGrafico($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_oracle WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND office = :edicion AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    function listar_todo_familias1Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_VMWareSam WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_VMWareSam WHERE archivo = :archivo AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familiasSAM($cliente, $empleado, $familia, $edicion) {        
        try{
            $this->conexion();
        //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia AND office = :edicion ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_VMWareSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias2SAM($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_VMWareSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csv2/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }

    function fechaSAM($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT fecha FROM encGrafVMWareSam WHERE cliente = :cliente AND empleado = :empleado');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            $this->fecha = $row["fecha"];
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function fechaSAMArchivo($archivo) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT fecha FROM encGrafVMWareSam WHERE archivo = :archivo');        
            $sql->execute(array(':archivo'=>$archivo));
            $row = $sql->fetch();
            return $row["fecha"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function balanceEjecutivo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia,
                    SUM(compra) AS totalCompras,
                    SUM(instalaciones) AS totalInstalaciones,
                    SUM(compra) - SUM(instalaciones) AS neto,
                    precio,
                    (SUM(compra) - SUM(instalaciones)) * precio AS total
                FROM balance_VMWare
                WHERE cliente = :cliente AND precio > 0
                GROUP BY familia
                ORDER BY familia');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function sobranteFaltante($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ABS(ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) < 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0)) AS faltante,
                    ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) > 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0) AS sobrante
                FROM balance_VMWare
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("faltante"=>0, "sobrante"=>0);
        }
    }
}