<?php
class ConsolidadoTipoEquipo extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $datoControl;
    var $hostName;
    var $fabricante;
    var $modelo;
    var $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $datoControl, $hostName, $fabricante, $modelo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoTipoEquipo (cliente, empleado, dato_control, host_name, fabricante, modelo) 
            VALUES(:cliente, :empleado, :datoControl, :hostName, :fabricante, :modelo)");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado, 'datoControl'=>$datoControl ,'hostName'=>$hostName, 'fabricante'=>$fabricante,'modelo'=>$modelo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoTipoEquipo (cliente, empleado, dato_control, host_name, fabricante, modelo) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM consolidadoTipoEquipo WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}