<?php
class usuarioEquipoMicrosoft extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $dominio, $usuario, $ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usuarioEquipoMicrosoft (cliente, empleado, dato_control, host_name, dominio, usuario, ip) '
            . 'VALUES (:cliente, :empleado, :dato_control, :host_name, :dominio, :usuario, :ip)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control, ':host_name'=>$host_name, ':dominio'=>$dominio, ':usuario'=>$usuario, ':ip'=>$ip));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usuarioEquipoMicrosoft (cliente, empleado, dato_control, host_name, dominio, usuario, ip) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $centroCosto) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE usuarioEquipoMicrosoft SET centroCosto = :centroCosto WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':centroCosto'=>$centroCosto));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM usuarioEquipoMicrosoft WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarUsuarioEquipo($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla1.id,
                    tabla1.host_name,
                    tabla1.usuario,
                    tabla1.centroCosto,
                    SUM(IFNULL(compras2.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                FROM (SELECT resumen_office2.cliente,
                        resumen_office2.empleado,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM resumen_office2
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY resumen_office2.equipo) AS tabla
                    LEFT JOIN compras2 ON tabla.cliente = compras2.cliente AND tabla.empleado = compras2.empleado
                    AND tabla.familia = compras2.familia AND tabla.edicion = compras2.edicion AND tabla.version = compras2.version
                    INNER JOIN versiones ON tabla.version = versiones.nombre
                    INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                    INNER JOIN productos ON tabla.familia = productos.nombre
                    INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                    INNER JOIN (SELECT usuarioEquipoMicrosoft.id,
                                    usuarioEquipoMicrosoft.cliente,
                                    usuarioEquipoMicrosoft.empleado,
                                    usuarioEquipoMicrosoft.host_name,
                                    usuarioEquipoMicrosoft.usuario,
                                    usuarioEquipoMicrosoft.centroCosto
                                FROM usuarioEquipoMicrosoft
                                WHERE usuarioEquipoMicrosoft.cliente = :cliente AND usuarioEquipoMicrosoft.empleado = :empleado
                                GROUP BY usuarioEquipoMicrosoft.host_name, usuarioEquipoMicrosoft.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente AND tabla.empleado = tabla1.empleado
                GROUP BY tabla1.host_name, tabla1.usuario
                ORDER BY tabla1.host_name');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function UsuarioXEquipo($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT host_name, usuario, centroCosto
                FROM usuarioEquipoMicrosoft
                WHERE cliente = :cliente AND empleado = :empleado AND centroCosto != ""
                GROUP BY host_name, usuario
                ORDER BY centroCosto, host_name, usuario');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoCentroCostoAsignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla1.centroCosto,
                    SUM(IFNULL(compras2.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                FROM (SELECT resumen_office2.cliente,
                        resumen_office2.empleado,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM resumen_office2
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY resumen_office2.equipo) AS tabla
                    LEFT JOIN compras2 ON tabla.cliente = compras2.cliente AND tabla.empleado = compras2.empleado
                    AND tabla.familia = compras2.familia AND tabla.edicion = compras2.edicion AND tabla.version = compras2.version
                    INNER JOIN versiones ON tabla.version = versiones.nombre
                    INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                    INNER JOIN productos ON tabla.familia = productos.nombre
                    INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                    INNER JOIN (SELECT usuarioEquipoMicrosoft.id,
                                    usuarioEquipoMicrosoft.cliente,
                                    usuarioEquipoMicrosoft.empleado,
                                    usuarioEquipoMicrosoft.host_name,
                                    usuarioEquipoMicrosoft.usuario,
                                    usuarioEquipoMicrosoft.centroCosto
                                FROM usuarioEquipoMicrosoft
                                WHERE usuarioEquipoMicrosoft.cliente = :cliente AND usuarioEquipoMicrosoft.empleado = :empleado
                                GROUP BY usuarioEquipoMicrosoft.host_name, usuarioEquipoMicrosoft.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente AND tabla.empleado = tabla1.empleado
                GROUP BY tabla1.centroCosto
                ORDER BY tabla1.host_name');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }    
}