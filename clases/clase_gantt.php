<?php
class gantt extends General{
    public  $error = NULL;
    
    // Insertar 
    function insertar($cliente, $descripcion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO gantt (cliente, descripcion, fecha) VALUES '
            . '(:cliente, :descripcion, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':descripcion'=>$descripcion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE gantt SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoPaginado($cliente, $inicio){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
            . 'FROM gantt '
            . 'WHERE cliente = :cliente AND estado = 1 '
            . 'ORDER BY fecha DESC '
            . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
            . 'FROM gantt '
            . 'WHERE cliente = :cliente AND estado = 1');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerUltId(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id '
            . 'FROM gantt');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function datosGantt($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.empresa,
                    gantt.cliente,
                    gantt.descripcion,
                    gantt.fecha
                FROM gantt
                    INNER JOIN clientes ON gantt.cliente = clientes.id
                WHERE gantt.id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('empresa'=>"", 'cliente'=>"", 'descripcion'=>"", 'fecha'=>'0000-00-00 00:00:00');
        }
    }
}