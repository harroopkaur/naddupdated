<?php
class TablaC extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $codigo;
    var $os;
    var $error = NULL;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($codigo, $os) {
        $query  = "INSERT INTO tabla_conversion (codigo, os) ";
        $query .= "VALUES (:codigo, :os)";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':codigo'=>$codigo, ':os'=>$os));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Actualizar
    function actualizar($id, $codigo, $os) {
        $query = "UPDATE tabla_conversion SET ";
        $query .= " codigo = :codigo, os = :os";
        $query .= " WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':codigo'=>$codigo, ':os'=>$os, ':id'=>$id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Eliminar
    function eliminar($id) {
        $query = "DELETE FROM tabla_conversion WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Obtener datos 
    function datos($id) {
        $query = "SELECT * FROM tabla_conversion WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id     = $usuario['id'];
            $this->codigo = $usuario['codigo'];
            $this->os     = $usuario['os'];
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Obtener datos 
    function datos2($codigo) {
        $query = "SELECT * FROM tabla_conversion WHERE codigo = :codigo";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':codigo'=>$codigo));
            $usuario = $sql->fetch();
            
            $this->id     = $usuario['id'];
            $this->codigo = $usuario['codigo'];
            $this->os     = $usuario['os'];
            
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo() {
        $query = "SELECT * FROM tabla_conversion WHERE id!=0 ORDER BY id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados
    function listar_todo_paginado($inicio, $fin) {
        $query = "SELECT * FROM tabla_conversion WHERE id!=0 ORDER BY  id LIMIT " . $inicio . ", " . $fin;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }

    // Contar el total de Usuarios
    function total() {
        $query = "SELECT COUNT(*) AS cantidad FROM tabla_conversion WHERE id != 0";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute();
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return 0;
        }
    }

    // Verificar existe
    function codigo_existe($codigo, $id) {
        $query = "SELECT * FROM tabla_conversion WHERE codigo = :codigo AND id != :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':codigo'=>$codigo, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row["codigo"]) > 0){
                return true;
            }
            else{
                return false;
            }
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
}
?>