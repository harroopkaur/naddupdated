<?php
class log extends General{ 
    public $error;
    
    function insertar($idModulo, $idOperacion, $cliente, $empleado, $IP, $comentario) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO logWebtool (idModulo, idOperacion, cliente, empleado, IP, comentario, fecha) VALUES
            (:idModulo, :idOperacion, :cliente, :empleado, :IP, :comentario, NOW())');
            $sql->execute(array(':idModulo'=>$idModulo, ':idOperacion'=>$idOperacion, ':cliente'=>$cliente, ':empleado'=>$empleado, ':IP'=>$IP, ':comentario'=>$comentario));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    
    function totalListadoLog(){
        
    }
    
    function listadoLogPaginado($idModulo, $idOperacion, $cliente, $empleado, $IP, $comentario, $fecha) {
        try{
            $array = array(':cliente'=>$cliente, ':empleado'=>"%" . $empleado . "%", ':IP'=>"%" . $IP . "%" ,
            ':comentario'=>"%" . $comentario . "%");
            $where = "";
            
            if($idModulo != ""){
                $array[":idModulo"] = $idModulo; 
                $where .= " AND logWebtool.idModulo = :idModulo ";
            }
            
            if($idOperacion != ""){
                $array[":idOperacion"] = $idOperacion; 
                $where .= " AND logWebtool.idOperacion = :idOperacion ";
            }
            
            if($fecha != 0 && $fecha != ""){
                $array[':fecha'] = "%" . $fecha . "%";
                $where .= ' AND DATE_FORMAT(logWebtool.fecha, "%d/%m/%Y") LIKE :fecha ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT modulo.descripcion AS modulo,
                    operacion.descripcion AS operacion,
                    CONCAT(empleados.nombre, " ", empleados.apellido) AS nombre,
                    logWebtool.IP,
                    logWebtool.comentario,
                    DATE_FORMAT(logWebtool.fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                FROM logWebtool
                    INNER JOIN detalleMaestra AS modulo ON logWebtool.idModulo = modulo.idDetalle AND modulo.idMaestra = 22
                    INNER JOIN detalleMaestra AS operacion ON logWebtool.idOperacion = operacion.idDetalle AND operacion.idMaestra = 23
                    INNER JOIN empleados ON logWebtool.empleado = empleados.id
                WHERE logWebtool.cliente = :cliente ' . $where . '
                AND CONCAT(empleados.nombre, " ", empleados.apellido) LIKE :empleado AND logWebtool.IP LIKE :IP 
                AND comentario LIKE :comentario 
                ORDER BY logWebtool.id DESC');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function fechaUltimoEscaneo($idModulo, $cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT DATE_FORMAT(MAX(logWebtool.fecha), "%d/%m/%Y") AS fecha
                FROM logWebtool
                WHERE idModulo = :idModulo AND idOperacion = 1 AND logWebtool.cliente = :cliente');
            $sql->execute(array(":idModulo"=>$idModulo, ":cliente"=>$cliente));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return "";
        }
    }
}