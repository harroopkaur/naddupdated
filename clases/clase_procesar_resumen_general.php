<?php
class clase_procesar_resumen_general extends General{
    
    protected $opcionDespliegue;
    protected $opcion;
    protected $idDiagnostic;
    protected $whereConsolidado;
    protected $arrayConsolidado;
    protected $campoResumen;
    protected $insertarBloque;
    protected $fabricante;
    protected $tabResumen;
    protected $tabConsolidado;
    protected $client_id;
    protected $client_empleado;
    protected $serialHDD;
    protected $idCorreo;
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO " . $this->tabResumen . " (" . $this->campoResumen . ", equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES " . $bloque;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($cliente) {
        $this->conexion();
        $query = "DELETE FROM " . $this->tabResumen . " WHERE cliente = :cliente";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarAppEscaneo($cliente, $idCorreo, $serialHDD) {
        $this->conexion();
        $query = "DELETE FROM appResumen_office WHERE cliente = :cliente AND idCorreo = :idCorreo AND serialHDD = :serialHDD";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':idCorreo'=>$idCorreo, ':serialHDD'=>$serialHDD));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function setTabResumen($tabResumen){
        $this->tabResumen = $tabResumen;
    }
    
    function sustituirCamposDespliegue($idMaestra, $idForaneo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :idMaestra AND (idForaneo = :idForaneo OR idForaneo IS NULL)
                ORDER BY prioridad');
            $sql->execute(array(':idMaestra'=>$idMaestra, ':idForaneo'=>$idForaneo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listarProductosMicrosoft($in = null, $out = null) {        
        try{
            $this->conexion();
            $agregar = $this->camposAgregar($in);
            $quitar = $this->camposQuitar($out);
                       
            $sql = $this->conn->prepare("SELECT *
                FROM (SELECT *
                        FROM " . $this->tabConsolidado . "
                        WHERE " . $this->whereConsolidado . $agregar . "
                        ORDER BY fecha_instalacion DESC) AS tabla
                WHERE tabla.id NOT IN (
                          SELECT id
                          FROM " . $this->tabConsolidado . "
                          WHERE " . $this->whereConsolidado . " AND (" . $quitar . ") )");
            $sql->execute($this->arrayConsolidado);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->tabConsolidado;
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function buscarVersion($software) {
        $version = "";
        try{
            //if (strpos($software, "Office 365") === FALSE){    
                $this->conexion();
                $sql = $this->conn->prepare("SELECT *
                        FROM versiones
                        WHERE id NOT IN (46) AND id NOT IN (SELECT id FROM versiones WHERE id > 140 AND id <= 1330) AND status = 1
                        ORDER BY nombre");
                $sql->execute();
                $tabla = $sql->fetchAll();
                
                $version = $this->buscarVersionTabla($tabla, $software);
            //}
            return $version;
        }catch(PDOException $e){
            echo "Listado Version".$this->error = $e->getMessage()."<br>";
            return $version;
        }
    }
    
    function buscarVersionTabla($tabla, $software){
        $version = "";
        foreach($tabla as $row){
            if (strpos($software, $row["nombre"]) !== false && strlen($row["nombre"]) > strlen($version)) {
                $version = $row["nombre"];
            }
        }
        
        return $version;
    }
    
    function filtrarProducto($productoIni, $version, $idForaneo, $idForaneo1){    
        $producto = $this->condicionesInicialesFiltrarProducto($productoIni, $version, $idForaneo);
        
        $tabla1 = $this->sustituirCamposDespliegue(3, $idForaneo1);
        foreach($tabla1 as $row){
            $resultado = strpos($producto, $row["descripcion"]);
            if($resultado !== FALSE){
                $producto = str_replace($row["descripcion"], $row["campo1"], $producto);
            }
        }
        
        $tabla2 = $this->sustituirCamposDespliegue(4, $idForaneo);
        foreach($tabla2 as $row){
            if(trim($producto) == trim($row["descripcion"])){
                $producto = $row["campo1"];
            }
        }
        
        return trim($producto);
    }
    
    function condicionesInicialesFiltrarProducto($producto, $version, $idForaneo){
        $tabla = $this->sustituirCamposDespliegue(2, $idForaneo);
        if($version != ""){
            if(strpos($producto, $version) !== FALSE){
                $producto = str_replace($version, "", $producto);
            }
        }        
        
        foreach($tabla as $row){
            $resultado = strpos($producto, $row["descripcion"]);
            if($resultado !== FALSE){
                $producto = str_replace($row["descripcion"], "", $producto);
            }
        }
        
        if(($idForaneo == 1 || $idForaneo == 2 || $idForaneo == 3 || $idForaneo == 4 || $idForaneo == 5 || $idForaneo == 6) && (trim($producto) == "" || is_numeric(trim($producto)) || strpos($producto, "Standard") !== FALSE)){
            $producto = "Standard";
        }
        
        return $producto;
    }
    
    function procesarResumenGeneral($client_id, $client_empleado, $tablaProductosMaestra, $opcionDespliegue, $opcion, 
    $idDiagnostic = 0, $idCorreo = 0, $serialHDD = ""){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->serialHDD = $serialHDD;
        $this->idDiagnostic = $idDiagnostic;
        $this->opcionDespliegue = $opcionDespliegue;
        $this->opcion = $opcion;
        $this->idCorreo = $idCorreo;
        
        if($this->opcion == "otros" || $this->opcion == "microsoft" || $this->opcion == "Cloud" || $this->opcion == "Diagnostic" || $this->opcion == "appEscaneo"){
            $this->fabricante = 3;
        }
        
        $this->cabeceraTablas();
        
        foreach($tablaProductosMaestra as $rowProductos){       
            if(strpos($rowProductos['descripcion'], 'Windows') !== false || strpos($rowProductos['descripcion'], 'SQL') !== false){
                continue;
            }
            $lista = $this->listarProductosMicrosoft($rowProductos["campo3"], $rowProductos["campo2"]);

            $this->cicloInsertar($lista, $rowProductos);

            $this->verificarRegistrosInsertar1();
        }
        //fin resumen office
    }
    
    function cabeceraTablas(){
        if($this->opcion == "otros"){
            $this->tabConsolidado = "consolidado_othersAux";
            $this->whereConsolidado = "cliente = :cliente AND empleado = :empleado";
            $this->arrayConsolidado = array(':cliente'=>$this->client_id, ':empleado'=>$this->client_empleado);
            $this->tabResumen = "resumen_others";
            $this->campoResumen = "cliente, empleado";
        } else if($this->opcion == "Cloud"){
            $this->tabConsolidado = "consolidado_MSCloud";
            $this->whereConsolidado = "idDiagnostic = :idDiagnostic";  
            $this->arrayConsolidado = array(':idDiagnostic'=>$this->idDiagnostic);
            $this->tabResumen = "resumen_MSCloud";
            $this->campoResumen = "idDiagnostic";
        } else if($this->opcion == "Diagnostic"){
            $this->tabConsolidado = "consolidado_SAMDiagnostic";
            $this->whereConsolidado = "idDiagnostic = :idDiagnostic"; 
            $this->arrayConsolidado = array(':idDiagnostic'=>$this->idDiagnostic);
            $this->tabResumen = "resumen_SAMDiagnostic";
            $this->campoResumen = "idDiagnostic";
        } else if($this->opcion == "microsoft"){
            $this->tabConsolidado = "consolidado_offices";
            if($this->opcionDespliegue != "completo" && $this->opcionDespliegue != "segmentado"){
                $this->tabConsolidado = "consolidado_officesAux";
            }
            
            $this->whereConsolidado = "cliente = :cliente AND empleado = :empleado";
            $this->arrayConsolidado = array(':cliente'=>$this->client_id, ':empleado'=>$this->client_empleado);
            $this->tabResumen = "resumen_office2";
            $this->campoResumen = "cliente, empleado";
        } else if($this->opcion == "appEscaneo"){
            $this->tabConsolidado = "appConsolidado_offices";
            $this->whereConsolidado = "cliente = :cliente AND idCorreo = :idCorreo AND serialHDD = :serialHDD";
            $this->arrayConsolidado = array(':cliente'=>$this->client_id, ':idCorreo'=>$this->idCorreo, ':serialHDD'=>$this->serialHDD);
            $this->tabResumen = "appResumen_office";
            $this->campoResumen = "cliente, idCorreo, serialHDD";
        }
    }
    
    function buscarCampos($campos, $j){
        $result = "";
        for($i = $j; $i < count($campos); $i++){
            if($i > $j){
                $result .= " OR ";
            }
            $result .= "sofware LIKE '%" . trim($campos[$i]) . "%'";
        }
        
        return $result;
    }
    
    function camposAgregar($in){
        $agregar = "";
        if($in != null){
            $inAux = explode(",", $in);
        }

        if(trim($inAux[0]) == "Office" || trim($inAux[0]) == "Visio" || trim($inAux[0]) == "Visual" || trim($inAux[0]) == "Microsoft Exchange"
        || trim($inAux[0]) == "SharePoint" || trim($inAux[0]) == "SQL"){
            $agregar .= " AND sofware LIKE '%" . trim($inAux[0]) . "%' AND (";
            $agregar .= $this->buscarCampos($inAux, 1);
            $agregar .= ")";
        } else{
            $agregar .= " AND (";
            $agregar .= $this->buscarCampos($inAux, 0);               
            $agregar .= ")";
        }
        
        return $agregar;
    }
    
    function camposQuitar($out){
        $quitar = "";
        if($out != null){
            $outAux = explode(",", $out);
        }
        $quitar .= $this->buscarCampos($outAux, 0);   
        return $quitar;
    }
    
    function cicloInsertar($lista, $rowProductos){
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;

        foreach($lista as $reg_r){
            $this->crearBloque($j);
            
            $this->crearBloqueValores($j, $reg_r["host_name"], $rowProductos["descripcion"], $reg_r["sofware"], 
            $reg_r["fecha_instalacion"], $rowProductos["idForaneo"], $this->fabricante);
            
            $j = $this->verificarRegistrosInsertar($j);

            $j++;
        }
    }
    
    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        } else if($this->opcion == "appEscaneo"){
            $inicioBloque = "(:cliente" . $j . ", :idCorreo" . $j . ", :serialHDD" . $j . ", ";
        }
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":equipo" . $j . ", :familia" . $j . ", "
            . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":equipo" . $j . ", :familia" . $j . ", "
            . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j, $hostName, $descripcion, $software, $fechaInstalacion, $idForaneo, $fabricante){
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else if($this->opcion == "appEscaneo"){
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":idCorreo" . $j] = $this->idCorreo;
            $this->bloqueValores[":serialHDD" . $j] = $this->serialHDD;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }
        
        $host = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($hostName))), 250);

        $version = $this->buscarVersion($software);

        $fecha_instalacion = $this->obtenerFechaInstalacion($fechaInstalacion);

        $this->bloqueValores[":equipo" . $j] = $host;
        $this->bloqueValores[":familia" . $j] = $descripcion;
        $this->bloqueValores[":edicion" . $j] = $this->filtrarProducto($software, $version, $idForaneo, $fabricante); //3 es el fabricante Microsoft
        $this->bloqueValores[":version" . $j] = $version;
        $this->bloqueValores[":fecha_instalacion" . $j] = $fecha_instalacion;
    }
    
    function obtenerFechaInstalacion($fechaInstalacion){
        $f3 = substr($fechaInstalacion, 6, 2);
        $f2 = substr($fechaInstalacion, 4, -2);
        $f1 = substr($fechaInstalacion, 0, 4);

        if($fechaInstalacion == 0){
            $fecha_instalacion ='0000-00-00';
        } else{
            $fecha_instalacion = $f1.'-'.$f2.'-'.$f3;
        }
        
        return $fecha_instalacion;
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function insertarGeneral(){
        if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        } 
    }     
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
}