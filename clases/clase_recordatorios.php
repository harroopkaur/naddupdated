<?php
class clase_recordatorios extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($modulo, $cliente, $correo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO recordatorios (modulo, cliente, correo) '
            . 'VALUES (:modulo, :cliente, :correo)');
            $sql->execute(array(':modulo'=>$modulo, ':cliente'=>$cliente, ':correo'=>$correo));
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar    
    function actualizar($id, $correo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE recordatorios SET '
            . 'correo = :correo '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':correo'=>$correo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar    
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM recordatorios WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_paginado($empresa, $email, $inicio) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT recordatorios.id, clientes.empresa, recordatorios.correo '
                . 'FROM recordatorios '
                    . 'INNER JOIN clientes ON recordatorios.cliente = clientes.id AND clientes.estado = 1 '
                . 'WHERE clientes.empresa LIKE :empresa AND recordatorios.correo LIKE :email '
                . 'ORDER BY clientes.empresa '
                . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute($array);
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de recordatorios
    function total($empresa, $email) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
                . 'FROM recordatorios '
                    . 'INNER JOIN clientes ON recordatorios.cliente = clientes.id AND clientes.estado = 1 '
                . 'WHERE clientes.empresa LIKE :empresa AND recordatorios.correo LIKE :email '
                . 'ORDER BY clientes.empresa ');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function datos($id) {
        try{
            $array = array(":id"=>$id);
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM recordatorios '
                . 'WHERE id = :id');
            $sql->execute($array);
            return $sql->fetch();
        }catch(PDOException $e){
            return array("id"=>0, "modulo"=>0, "cliente"=>0, "correo"=>"");
        }
    }
    
    function email_existe($modulo, $cliente, $email, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM recordatorios WHERE modulo = :modulo AND cliente = :cliente AND '
            . 'correo = :email AND id != :id');
            $sql->execute(array(':modulo'=>$modulo, ':cliente'=>$cliente, ':email'=>$email, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['correo']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    function emailsCliente($modulo, $cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT correo '
                . 'FROM recordatorios '
                . 'WHERE cliente = :cliente AND modulo = :modulo ');
            $sql->execute(array(':cliente'=>$cliente, ':modulo'=>$modulo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }
}
?>