<?php
class clase_procesar_eliminar_equipos extends General{
    private $tabEliminar;
    private $tablaElim;
    private $tablaElimAux;
   
    function eliminarEquipos($equipos, $arrayEquipos) {
        $this->conexion();
        $query = "DELETE FROM " . $this->tabEliminar . " WHERE cliente = :cliente AND (" . $equipos . ")";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($arrayEquipos);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function cabeceraTablas(){
        if($this->opcion == "microsoft"){
            $this->tablaElim = "filepcs2";
            $this->tablaElimAux = "filepcsAux";
        }
    }
    
    function abrirArchivo($ruta){
        $k = 0;
        $equiposElim = "";
        if (($fichero = fopen($ruta, "r")) !== false) {
            $arrayEquipos = array(':cliente'=>$this->client_id);
            while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {
                if($k > 0){
                    $equiposElim .= " OR ";
                }
                $equiposElim .= "cn = :equipos" . $k . " ";
                $arrayEquipos[":equipos" . $k] = $datos[0];
                $k++;
            }

            if($equiposElim != ""){
                $this->tabEliminar = $this->tablaElim;
                $this->eliminarEquipos($equiposElim, $arrayEquipos);
                //echo $this->error;
                $this->tabEliminar = $this->tablaElimAux;
                $this->eliminarEquipos($equiposElim, $arrayEquipos);
                //echo $this->error;
            }
        }
        fclose($fichero);
    }
    
    function procesarArchivo($ruta){
        if($this->obtenerSeparadorUniversal($ruta, 1, 1) === true){
            $this->abrirArchivo($ruta);
        }
    }
    
    function eliminarEquiposLAE($cliente, $opcion, $nombre){
        $this->client_id = $cliente;
        $this->opcion = $opcion;
        
        $ruta = $GLOBALS["app_root"] . "/" . $opcion . "/archivos_csvf3/" . $nombre;
        if($nombre != "" && file_exists($ruta)){
            $this->procesarArchivo($ruta);
        }
    }
}
