<?php
class Compras_f extends General{
    ########################################  Atributos  ########################################

    public $id;
    public $cliente;
    public $officce;
    public $instalaciones;
    public $compra;
    public $balance;
    public $tipo;
    public $precio;
    public $error = NULL;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $edicion, $version, $tipo, $compra, $precio, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO compras2 (cliente, empleado, familia, edicion, version, tipo, compra, precio, asignacion) '
            . 'VALUES (:cliente, :empleado, :familia, :edicion, :version, :tipo, :compra, :precio, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, 
            ':tipo'=>$tipo, ':compra'=>$compra, ':precio'=>$precio, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM compras2 WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    function listar_asignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT asignacion FROM compras2 WHERE cliente = :cliente AND empleado = :empleado GROUP BY asignacion ORDER BY asignacion");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function datos3($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras2 WHERE familia = :familia AND edicion = :edicion AND '
            . 'version = :version AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $usuario = $sql->fetch();
            
            $this->id      = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            $this->compra  = $usuario['compra'];
            $this->precio  = $usuario['precio'];
            $this->tipo    = $usuario['tipo'];
            if($usuario['compra'] == ""){
                $this->compra  = 0;
            }
            if($usuario['precio'] == ""){
                $this->precio = 0;
            }
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            $this->precio = 0;
            return false;
        }
    }

    function datos2($cliente, $empleado, $familia, $edicion) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras2 WHERE familia = :familia AND edicion = :edicion AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $usuario = $sql->fetch();
            
            $this->id      = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            $this->compra  = $usuario['compra'];
            $this->precio  = $usuario['precio'];
            $this->tipo    = $usuario['tipo'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras2 WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Contar el total de Usuarios
    function total($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM compras2 WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalCompras($cliente, $familia, $edicion, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        
        if($familia != "Others"){
            $where1 = " AND familia LIKE :familia ";
            $array[":familia"] = '%' . $familia . '%';
            
            if($familia == "Windows Server"){
                $where1 = " AND familia IN (:familia, 'Windows Terminal Server') ";
                $array[":familia"] = $familia;
            } else if($familia == "Windows"){
                $where1 = " AND familia = :familia ";
                $array[":familia"] = $familia;
                
                if($edicion == "Standard"){
                    $where1 .= " AND edicion NOT LIKE '%Embedded%' ";
                }
            }
        } else{
            $where1 = " AND familia IN ('Visual Studio', 'Exchange Server', 'Sharepoint Server', 'Skype for Business', 'System Center', 'Dynamics CRM') ";
        }
       
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        if ($edicion == "Professional"){
            $edicion = "Pro";
        }
        
        $array[":edicion"] = '%' . $edicion . '%';
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(SUM(IFNULL(compra, 0)), 0) AS compra '
            . 'FROM compras2 '
            . 'WHERE cliente = :cliente ' . $where1 . ' AND edicion LIKE :edicion ' . $where
            . 'ORDER BY id');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["compra"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalComprasOtros($cliente, $familia, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        
        $family = " AND familia = :familia ";
        if ($familia == "Windows Server"){
            $family = " AND familia IN (:familia, 'Windows Terminal Server') ";
        } 
        
        if($familia == "Windows"){
            $notIn = " AND edicion NOT IN ('Standard') AND NOT edicion LIKE '%Enterprise%' AND NOT edicion LIKE '%Pro%' ";
        } else if($familia == "Windows Server"){
            $notIn = " AND edicion NOT IN ('Standard') AND NOT edicion LIKE '%Enterprise%' AND NOT edicion LIKE '%Datacenter%' ";
        } else if($familia == "SQL Server"){      
            $notIn = " AND edicion NOT IN ('Standard') AND NOT edicion LIKE '%Enterprise%' ";            
        } else{
            $notIn = " AND edicion NOT IN ('Standard') AND NOT edicion LIKE '%Pro%' ";
        }
        
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        try{
            $this->conexion();
            /*$sql = $this->conn->prepare('SELECT IFNULL(SUM(IFNULL(compra, 0)), 0) AS compra '
            . 'FROM compras2 '
            . 'WHERE cliente = :cliente AND familia = :familia AND edicion NOT IN (' . $notIn . ') ' . $where
            . 'ORDER BY id');*/
            $sql = $this->conn->prepare('SELECT IFNULL(SUM(IFNULL(compra, 0)), 0) AS compra '
            . 'FROM compras2 '
            . 'WHERE cliente = :cliente ' . $family . $notIn . $where
            . 'ORDER BY id');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["compra"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalCompras1($cliente, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(SUM(IFNULL(compra, 0)), 0) AS compra '
            . 'FROM compras2 '
            . 'WHERE cliente = :cliente AND familia NOT IN ("Office", "Windows", "Windows Server", "SQL Server") ' . $where
            . 'ORDER BY id');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["compra"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function comprasAsignacion1($cliente, $empleado, $familia, $edicion, $asignacion){
        $where = "";
        $array = array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%');
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', ':asignacion'=>$asignacion);
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT compras2.familia,
                    compras2.edicion,
                    compras2.version,
                    compras2.asignacion,
                    IFNULL(COUNT(resumen_office2.familia), 0) AS instalaciones,
                    (compras2.compra - IFNULL(COUNT(resumen_office2.familia), 0)) AS balance,
                    (compras2.compra - IFNULL(COUNT(resumen_office2.familia), 0)) * compras2.precio AS balancec,
                    compras2.compra,
                    (compras2.compra - IFNULL(COUNT(resumen_office2.familia), 0)) AS disponible,
                    compras2.precio,
                    compras2.cantidadGAP,
                    compras2.balanceGAP
                FROM compras2
                    LEFT JOIN (SELECT  resumen_office2.cliente,
                            resumen_office2.empleado,
                            resumen_office2.familia,
                            resumen_office2.edicion,
                            resumen_office2.version,
                            detalles_equipo2.asignacion
                        FROM resumen_office2
                            INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                            AND resumen_office2.cliente = detalles_equipo2.cliente AND resumen_office2.empleado = detalles_equipo2.empleado
                        WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia = :familia
                        AND resumen_office2.edicion LIKE :edicion
                        GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                        ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) AS resumen_office2 ON compras2.cliente = resumen_office2.cliente AND
                    compras2.empleado = resumen_office2.empleado AND compras2.familia = resumen_office2.familia
                    AND compras2.edicion = resumen_office2.edicion AND compras2.version = resumen_office2.version
                    AND compras2.asignacion = resumen_office2.asignacion
                WHERE compras2.cliente = :cliente AND compras2.empleado = :empleado AND compras2.familia = :familia AND compras2.edicion LIKE :edicion
                GROUP BY compras2.familia, compras2.edicion, compras2.version, compras2.asignacion
                ORDER BY compras2.familia, compras2.edicion, compras2.version, compras2.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {

        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf4/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }

    function buscarPrecioRepo($cliente, $empleado, $fabricante, $familia, $edicion, $version) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT  detalleContrato.precio 
                FROM detalleContrato 
                     INNER JOIN contrato ON detalleContrato.`idContrato` = contrato.`idContrato`
                         INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                         INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                         INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE contrato.idCliente = :cliente AND contrato.empleado = :empleado AND contrato.idFabricante = :fabricante AND productos.`nombre` LIKE :familia
                AND ediciones.nombre LIKE :edicion AND versiones.nombre LIKE :version
                GROUP BY productos.nombre, ediciones.nombre, versiones.nombre');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':version'=>'%' . $version . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}