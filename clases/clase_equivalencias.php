<?php
class Equivalencias extends General{
    ########################################  Atributos  ########################################

    public $id;
    public $familia;
    public $edicion;
    public $version;
    public $precio;
    public $error = NULL;
    public $idFabricante;
    public $idFamilia;
    public $idEdicion;
    public $idVersion;
    public $fabricante;
    public $nombre;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($familia, $edicion, $version, $precio) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO tabla_equivalencia (familia, edicion, version, precio) '
            . 'VALUES (:familia, :edicion, :version, :precio)');
            $sql->execute(array(':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':precio'=>$precio));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertar1($fabricante, $familia, $edicion, $version, $precio) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO tabla_equivalencia (fabricante, familia, edicion, version, precio) '
            . 'VALUES (:fabricante, :familia, :edicion, :version, :precio)');
            $sql->execute(array(':fabricante'=>$fabricante, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':precio'=>$precio));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar
    function actualizar($id, $fabricante, $familia, $edicion, $version, $precio) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE tabla_equivalencia SET '
            . 'fabricante = :fabricante, familia = :familia, edicion = :edicion, version = :version, precio = :precio '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':fabricante'=>$fabricante, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':precio'=>$precio));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existeEquivalencia($fabricante, $familia, $edicion, $version) {
        try{ 
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
            . 'FROM tabla_equivalencia '
            . 'WHERE fabricante = :fabricante AND familia = :familia AND edicion = :edicion AND version = :version');
            $sql->execute(array(':fabricante'=>$fabricante, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            return 0;
        }
    }

    // Eliminar
    function eliminar($id) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM tabla_equivalencia WHERE id = :id');
            $sql->execute(array(':id'=>$id));
           return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos     
    function datos3($id) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM tabla_equivalencia WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            $this->id           = $usuario['id'];
            $this->idFabricante = $usuario['fabricante'];
            $this->idFamilia    = $usuario['familia'];
            $this->idEdicion    = $usuario['edicion'];
            $this->idVersion    = $usuario['version'];
            $this->precio       = $usuario['precio'];
           return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_completo() {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.id, ' 
                . 'fabricantes.`idFabricante`, '
                . 'fabricantes.`nombre` AS fabricante, '
                . 'productos.`idProducto`, '
                . 'TRIM(productos.`nombre`) AS familia, '
                . 'ediciones.`idEdicion`, '
                . 'TRIM(ediciones.`nombre`) AS edicion, '
                . 'versiones.id AS idVersion, '
                . 'TRIM(versiones.nombre) AS version, '
                . 'tabla_equivalencia.precio AS precio '
            . 'FROM tabla_equivalencia '
                . 'INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.`idFabricante` '
                . 'INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto` '
                . 'LEFT JOIN ediciones ON tabla_equivalencia.`edicion` = ediciones.`idEdicion` '
                . 'LEFT JOIN versiones ON tabla_equivalencia.`version` = versiones.`id` ' 
            . 'ORDER BY fabricante, familia, edicion, version');
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo($fabricante) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.id, ' 
                . 'fabricantes.`idFabricante`, '
                . 'fabricantes.`nombre` AS fabricante, '
                . 'productos.`idProducto`, '
                . 'TRIM(productos.`nombre`) AS familia, '
                . 'ediciones.`idEdicion`, '
                . 'TRIM(ediciones.`nombre`) AS edicion, '
                . 'versiones.id AS idVersion, '
                . 'TRIM(versiones.nombre) AS version, '
                . 'tabla_equivalencia.precio AS precio '
            . 'FROM tabla_equivalencia '
                . 'INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.`idFabricante` '
                . 'INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto` '
                . 'LEFT JOIN ediciones ON tabla_equivalencia.`edicion` = ediciones.`idEdicion` '
                . 'LEFT JOIN versiones ON tabla_equivalencia.`version` = versiones.`id` ' 
            . 'WHERE tabla_equivalencia.fabricante = :fabricante '
            . 'ORDER BY familia, edicion, version');
            $sql->execute(array(':fabricante'=>$fabricante));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_paginado($fab, $fam, $edi, $ver, $inicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.id,
                tabla_equivalencia.fabricante AS idFabricante,
                tabla_equivalencia.familia AS idFamilia,
                tabla_equivalencia.edicion AS idEdicion,
                tabla_equivalencia.version AS idVersion,
                fabricantes.nombre AS fabricante,
                productos.nombre AS familia,
                ediciones.nombre AS edicion,
                versiones.nombre AS version,
                tabla_equivalencia.precio
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id 
            WHERE fabricantes.nombre LIKE :fab AND productos.nombre LIKE :fam AND ediciones.nombre LIKE :edi AND versiones.nombre LIKE :ver  
            ORDER BY id 
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':fab'=>"%" . $fab . "%", ':fam'=>"%" . $fam . "%", ':edi'=>"%" . $edi . "%", ':ver'=>"%" . $ver . "%"));
            //$sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Contar el total de Usuarios
    function total($fab, $fam, $edi, $ver) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.`fabricante` = fabricantes.idFabricante
                INNER JOIN productos ON `tabla_equivalencia`.familia = productos.`idProducto`
                LEFT JOIN ediciones ON `tabla_equivalencia`.`edicion` = ediciones.`idEdicion`
                LEFT JOIN versiones ON `tabla_equivalencia`.`version` = versiones.id 
            WHERE fabricantes.nombre LIKE :fab AND productos.nombre LIKE :fam AND ediciones.nombre LIKE :edi AND versiones.nombre LIKE :ver');
            $sql->execute(array('fab'=>"%".$fab."%", 'fam'=>"%".$fam."%", 'edi'=>"%".$edi."%", 'ver'=>"%".$ver."%"));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return false;
        }
    }
    
    function listar_fabricantes() {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM fabricantes ' 
                . 'WHERE status = 1 '
                . 'ORDER BY nombre');
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_familias() {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM productos ' 
                . 'WHERE status = 1 '
                . 'ORDER BY nombre');
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_ediciones() {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM ediciones ' 
                . 'WHERE status = 1 '
                . 'ORDER BY nombre');
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_versiones() {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM versiones ' 
                . 'WHERE status = 1 '
                . 'ORDER BY nombre');
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function obtenerIdFabNombre($nombre){
        try {
            $this->conexion();
            $id = 0;
            $sql = $this->conn->prepare('SELECT idFabricante '
                . 'FROM fabricantes ' 
                . 'WHERE nombre = :nombre AND status = 1 '
                . 'ORDER BY nombre');
            $sql->execute(array(':nombre'=>$nombre));
            $row = $sql->fetch();
            if($row["idFabricante"] !== ""){
                $id = $row["idFabricante"];
            }
            return $id;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerOrden($fabricante, $familia, $edicion){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT (MAX(IFNULL(orden, 0)) + 1) AS orden '
                . 'FROM tabla_equivalencia ' 
                . 'WHERE fabricante = :fabricante AND familia = :familia AND edicion = :edicion');
            $sql->execute(array(':fabricante'=>$fabricante, ':familia'=>$familia, ':edicion'=>$edicion));
            $row = $sql->fetch();
            return $row["orden"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listadoPrecios($fecha){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, fabricante, familia, edicion, version, precio
                FROM tabla_equivalencia
                WHERE fechaActualizacion >= :fecha AND fabricante <= 18 AND familia <= 258 AND edicion <= 912 AND version <= 1002');
            $sql->execute(array(':fecha'=>$fecha));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
}