<?php
class clase_procesar_SQL_general extends clase_procesar_resumen_general{
    private $nDatoControl;
    private $nHostName;
    private $nEdicionSQL;
    private $nVersionSQL;
    private $procesarSQL;
    private $hostName;
    private $descripcion;
    private $fecha_instalacion;
    private $edicion;
    private $version;
    
    function procesarSQL($client_id, $client_empleado, $archivoSQL, $procesarSQL, $nDatoControl, $nHostName, $nEdicionSQL, 
    $nVersionSQL, $opcionDespliegue, $opcion, $idDiagnostic = 0, $idCorreo = 0, $serialHDD = ""){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->serialHDD = $serialHDD;
        $this->archivoSQL = $archivoSQL;
        $this->procesarSQL = $procesarSQL;
        $this->nDatoControl = $nDatoControl;
        $this->nHostName = $nHostName; 
        $this->nEdicionSQL = $nEdicionSQL; 
        $this->nVersionSQL = $nVersionSQL;
        $this->opcionDespliegue = $opcionDespliegue;
        $this->opcion = $opcion;
        $this->idDiagnostic = $idDiagnostic;
        $this->idCorreo = $idCorreo;
       
        $this->cabeceraTablas();
        
        if($this->obtenerSeparadorUniversal($this->archivoSQL, 2, 6) === true && $this->procesarSQL === true){
            if (($fichero = fopen($this->archivoSQL, "r")) !== false) {
                $this->cicloInsertarSQL($fichero);

                $this->verificarRegistrosInsertar1();
                
                fclose($fichero);
            }
        }
    }
    
    function cicloInsertarSQL($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {                 
            if($i > 2 && $datos[$this->nDatoControl] != "Dato de Control"){
                $this->crearBloque($j);
                
                $this->setValores($datos);
                
                $this->crearBloqueValoresSQL($j);

                $j = $this->verificarRegistrosInsertar($j);
                
                $j++;
            }
            
            $i++; 
        }
    }
   
    function crearBloqueValoresSQL($j){
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else if($this->opcion == "appEscaneo"){
             $this->bloqueValores[":cliente" . $j] = $this->client_id;
             $this->bloqueValores[":idCorreo" . $j] = $this->idCorreo;
             $this->bloqueValores[":serialHDD" . $j] = $this->serialHDD;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }

        $this->bloqueValores[":equipo" . $j] = $this->hostName;
        $this->bloqueValores[":familia" . $j] = $this->descripcion;
        $this->bloqueValores[":edicion" . $j] = $this->edicion;
        $this->bloqueValores[":version" . $j] = $this->version;
        $this->bloqueValores[":fecha_instalacion" . $j] = $this->fecha_instalacion;
    }
    
    function convertirVersionSQL($version){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT campo1 AS version
                FROM detalleMaestra
                WHERE idMaestra = 12 AND descripcion = :version');
            $sql->execute(array(':version'=>$version));
            $row = $sql->fetch();
            return $row["version"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("version"=>"");
        }
    }
    
    function setValores($datos){
        $this->hostName = "";
        $this->descripcion = "SQL Server";
        $this->fecha_instalacion = "0000-00-00";
        $this->edicion = "";
        $this->version = "";
        
        if(isset($datos[$this->nHostName])){
            $this->hostName = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($datos[$this->nHostName]))), 250);
        }  
        
        if(isset($datos[$this->nEdicionSQL])){
            $this->edicion = trim(str_replace("Edition", "", $this->truncarString($this->get_escape(utf8_encode($datos[$this->nEdicionSQL])), 250)));
        }

        if(isset($datos[$this->nVersionSQL])){
            $this->version = $this->convertirVersionSQL($this->truncarString($this->get_escape(utf8_encode($datos[$this->nVersionSQL])), 250));
        }    
    }
}
