<?php
class clase_procesar_actualizar_LAE extends General{
    private $client_id;
    private $client_empleado;
    private $serialHDD;
    private $opcionDespliegue;
    private $incremArchivo;
    private $opcion;
    private $idDiagnostic;
    private $tabLAE;
    private $tabLAEAux;
    private $campoLAE;
    private $arrayLAE;
    private $tabDetalle;
    private $campoDetalle;
    private $fDespliegue;
    private $fechaDespliegue;
    private $idioma;
    
    private $host;   
    private $sistema;
    private $dias1;
    private $dias2;
    private $dias3;
    private $minimo;
    private $minimor;
    private $activo;
    private $tipo;
    private $familia;
    private $edicion;
    private $version;
    
    private $idCorreo;
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabDetalle . " (" . $this->campoDetalle . ", equipo, 
            os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM " . $this->tabDetalle . " WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarAppEscaneo($cliente, $idCorreo, $serialHDD) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM appDetalles_equipo WHERE cliente = :cliente AND idCorreo = :idCorreo AND serialHDD = :serialHDD");
            $sql->execute(array(':cliente'=>$cliente, ':idCorreo'=>$idCorreo, ':serialHDD'=>$serialHDD));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarLAE($id, $archivo){
        $query = "UPDATE " . $this->tabDiagnosticos . " SET LAE = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        } 
    }
    
    function listar_todo() {
        $this->conexion();

        $query = "SELECT * FROM " . $this->tabLAE . " WHERE " . $this->campoLAE . " ORDER BY id";
        try { 
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($this->arrayLAE);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_edicionTotal() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idEdicion,
                nombre,
                status
            FROM ediciones
            WHERE status = 1');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function listar_versionTotal() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                nombre,
                status
            FROM versiones
            WHERE status = 1');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function procesarActualizarLAE($client_id, $client_empleado, $fDespliegue, $opcionDespliegue, $incremArchivo, $opcion, $idioma, 
    $idDiagnostic = 0, $idCorreo = 0, $serialHDD = ""){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->serialHDD = $serialHDD;
        $this->fDespliegue = $fDespliegue;
        $this->opcionDespliegue = $opcionDespliegue;
        $this->incremArchivo = $incremArchivo;
        $this->opcion = $opcion;
        $this->idioma = $idioma; 
        $this->idDiagnostic = $idDiagnostic;
        $this->idCorreo = $idCorreo; 
        
        $this->cabeceraTablas();
        
        if(($this->opcionDespliegue == "completo" || $this->opcionDespliegue == "segmentado" || $this->incremArchivo == 0) 
        && $this->opcion != "appEscaneo"){
            $this->eliminar($this->client_id);
        } 

        if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
            $lista_todos_files = $this->listar_todo();
        } else {
            $this->tabLAE = $this->tabLAEAux;
            $lista_todos_files = $this->listar_todo();
        }
                
        $this->cicloInsertar($lista_todos_files);

        $this->verificarRegistrosInsertar1();
    }
            
    function cabeceraTablas(){
        if($this->opcion == "Cloud"){
            $this->tabLAE = "filepcsMSCloud";
            $this->campoLAE = "idDiagnostic = :idDiagnostic";
            $this->arrayLAE = array(':idDiagnostic'=>$this->idDiagnostic);
            $this->tabDetalle = "detalles_equipoMSCloud";
            $this->campoDetalle = "idDiagnostic";
        } else if($this->opcion == "Diagnostic"){
            $this->tabLAE = "filepcsSAMDiagnostic";
            $this->campoLAE = "idDiagnostic = :idDiagnostic";
            $this->arrayLAE = array(':idDiagnostic'=>$this->idDiagnostic);
            $this->tabDetalle = "detalles_equipoSAMDiagnostic";
            $this->campoDetalle = "idDiagnostic";
        } else if($this->opcion == "microsoft"){
            $this->tabLAE = "filepcs2";
            $this->campoLAE = "cliente = :cliente AND empleado = :empleado";
            $this->arrayLAE = array(':cliente'=>$this->client_id, ':empleado'=>$this->client_empleado);
            $this->tabLAEAux = "filepcsAux";
            $this->tabDetalle = "detalles_equipo2";
            $this->campoDetalle = "cliente, empleado";
        } else if($this->opcion == "appEscaneo"){
            $this->tabLAE = "appFilepcs";
            $this->campoLAE = "cliente = :cliente AND idCorreo = :idCorreo AND serialHDD = :serialHDD";
            $this->arrayLAE = array(':cliente'=>$this->client_id, ':idCorreo'=>$this->idCorreo, ':serialHDD'=>$this->serialHDD);
            $this->tabDetalle = "appDetalles_equipo";
            $this->campoDetalle = "cliente, idCorreo, serialHDD";
        }
    }
    
    function cicloInsertar($tabla){
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        $this->fechaDespliegue = strtotime('now');       
        
        $this->buscarFecha();
        
        foreach ($tabla as $row) {               
            $this->crearBloque($j);

            $this->setValores($row);                
            $this->crearBloqueValores($j);

            $j = $this->verificarRegistrosInsertar($j);

            $j++;
        }
    }
    
    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        } else if($this->opcion == "appEscaneo"){
            $inicioBloque = "(:cliente" . $j . ", :idCorreo" . $j . ", :serialHDD" . $j . ", ";
        }
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":equipo" . $j . ", :os" . $j . ", "
            . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
            . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":equipo" . $j . ", :os" . $j . ", "
            . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
            . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j){
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else if($this->opcion == "appEscaneo"){
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":idCorreo" . $j] = $this->idCorreo;
            $this->bloqueValores[":serialHDD" . $j] = $this->serialHDD;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }
        
        $this->bloqueValores[":equipo" . $j] = $this->host;
        $this->bloqueValores[":os" . $j] = $this->sistema;
        $this->bloqueValores[":familia" . $j] = $this->familia;
        $this->bloqueValores[":edicion" . $j] = $this->edicion;
        $this->bloqueValores[":version" . $j] = $this->version;
        $this->bloqueValores[":dias1" . $j] = $this->dias1;
        $this->bloqueValores[":dias2" . $j] = $this->dias2;
        $this->bloqueValores[":dias3" . $j] = $this->dias3;
        $this->bloqueValores[":minimo" . $j] = $this->minimor;
        $this->bloqueValores[":activo" . $j] = $this->activo;
        $this->bloqueValores[":tipo" . $j] = $this->tipo;
        $this->bloqueValores[":rango" . $j] = $this->minimo;
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function insertarGeneral(){
        if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        } 
    }     
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
    
    function setValores($datos){
        $this->host = $this->extraerEquipo($datos["cn"]);   
        $this->sistema = $datos["os"];
        $this->dias1 = $this->obtenerDias($datos["lastlogon"]);
        $this->dias2 = $this->obtenerDias($datos["pwdlastset"]);
        $this->dias3 = $this->obtenerDias($datos["lastlogontimes"]);
        $this->obtenerMinimo();
        
        $tipos = $this->search_server($datos["os"]);
        $this->tipo = $tipos[0];
        
        $this->obtenerFamilia();
        $this->obtenerEdicion();
        $this->obtenerVersion();
        $this->limpFamEdiVersion();
    }
    
    function buscarFecha(){
        if($this->idioma == 1){
            if($this->validarFecha($this->fDespliegue, "/", "dd/mm/aaaa")){
                $this->fechaDespliegue = strtotime($this->reordenarFecha($this->fDespliegue, "/", "-"));
            }
        } else if($this->idioma == 2){
            if($this->validarFecha($this->fDespliegue, "/", "mm/dd/aaaa")){
                $this->fechaDespliegue = strtotime($this->cambiarfechasFormato($this->fDespliegue));
            }
        }
    }
    
    function obtenerDias($fecha){
        $dias = NULL;
        if ($fecha != "") {
            $value = round(($fecha - 116444735995904000) / 10000000);
            $dias = $this->daysDiff($value, $this->fechaDespliegue);
        } 
        
        return $dias;
    }
    
    
    function obtenerMinimo(){
        $minimos = $this->minimo($this->dias1, $this->dias2, $this->dias3);
        $this->minimor = round(abs($minimos), 0);

        if ($this->minimor <= 30) {
            $this->minimo = 1;
        } else if ($this->minimor <= 60) {
            $this->minimo = 2;
        } else if ($this->minimor <= 90) {
            $this->minimo = 3;
        } else if ($this->minimor <= 365) {
            $this->minimo = 4;
        } else {
            $this->minimo = 5;
        }

        if ($this->minimo < 4) {
            $this->activo = 1;
        } else {
            $this->activo = 0;
        }
    }
    
    function obtenerFamilia(){
        $this->familia = "";
        if(strpos($this->sistema, "Windows") !== false && strpos($this->sistema, "Server") !== false){
            $this->familia = "Windows Server";
        }
        else if(strpos($this->sistema, "Windows") !== false){
            $this->familia = "Windows"; 
        }
    }
    
    function obtenerEdicion(){
        $this->edicion = "";
        $listaEdiciones = $this->listar_edicionTotal();
        foreach($listaEdiciones as $rowEdiciones){
            if(trim($rowEdiciones["nombre"]) != "" && strpos($this->sistema, trim($rowEdiciones["nombre"])) !== false && 
            strlen(trim($rowEdiciones["nombre"])) > strlen($this->edicion)){
                 $this->edicion = trim($rowEdiciones["nombre"]);    
            }
        }

        if($this->edicion == "" || ($this->familia == "Windows Server" && $this->edicion == "Server") || $this->edicion == "Windows 2000 Server"){
            $this->edicion = "Standard";
        }
    }
    
    function obtenerVersion(){
        $this->version = "";
        $listaVersiones = $this->listar_versionTotal();
        foreach($listaVersiones as $rowVersiones){
            if(trim($rowVersiones["nombre"]) != "" && strpos($this->sistema, trim($rowVersiones["nombre"])) !== false && 
            strlen(trim($rowVersiones["nombre"])) > strlen($this->version)){
                 $this->version = trim($rowVersiones["nombre"]);    
            }
        } 
    }
    
    function limpFamEdiVersion(){
        if($this->familia == ""){
            $this->edicion = "";
            $this->version = "";
        }
    }
}
