<?php
class Ediciones extends General{
  
    function getEdicionNombre($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM ediciones
                WHERE nombre = :nombre');
            $sql->execute(array('nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>