<?php
class Balance_SPLA extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $officce;
    var $instalaciones;
    var $compra;
    var $balance;
    var $tipo;
    var $error = NULL;
    var $archivo;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $office, $version, $precio, $instalaciones, $compra, $balance, $balancec, $tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_SPLA (cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo) '
            . 'VALUES (:cliente, :empleado, :familia, :office, :version, :precio, :instalaciones, :compra, :balance, :balancec, :tipo)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':office'=>$office, ':version'=>$version, 
            ':precio'=>$precio, ':instalaciones'=>$instalaciones, ':compra'=>$compra, ':balance'=>$balance, ':balancec'=>$balancec, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function insertarSAM($archivo, $cliente, $familia, $office, $version, $precio, $instalaciones, $compra, $balance, $balancec, $tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_SPLASam (archivo, cliente, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo) '
            . 'VALUES(:archivo, :cliente, :familia, :office, :version, :precio, :instalaciones, :compra, :balance, :balancec, :tipo)');
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia, ':office'=>$office, ':version'=>$version, 
            ':precio'=>$precio, ':instalaciones'=>$instalaciones, ':compra'=>$compra, ':balance'=>$balance, ':balancec'=>$balancec, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function eliminarSAM($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM balance_SPLASam WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Obtener datos 
    /*function datos($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id            = $usuario['id'];
            $this->cliente       = $usuario['cliente'];
            $this->familia       = $usuario['familia'];
            $this->office        = $usuario['office'];
            $this->version       = $usuario['version'];
            $this->precio        = $usuario['precio'];
            $this->instalaciones = $usuario['instalaciones'];
            $this->compra        = $usuario['compra'];
            $this->balance       = $usuario['balance'];
            $this->balancec      = $usuario['balancec'];
            $this->tipo          = $usuario['tipo'];
            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    /*function datosSAM($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id            = $usuario['id'];
            $this->cliente       = $usuario['cliente'];
            $this->familia       = $usuario['familia'];
            $this->office        = $usuario['office'];
            $this->version       = $usuario['version'];
            $this->precio        = $usuario['precio'];
            $this->instalaciones = $usuario['instalaciones'];
            $this->compra        = $usuario['compra'];
            $this->balance       = $usuario['balance'];
            $this->balancec      = $usuario['balancec'];
            $this->tipo          = $usuario['tipo'];
            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    /*function datos2($cliente, $office) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE office = :office AND cliente = :cliente');
            $sql->execute(array(':office'=>$office, ':cliente'=>$cliente));
            $usuario = $sql->fetch();
            
            $this->id            = $usuario['id'];
            $this->cliente       = $usuario['cliente'];
            $this->office        = $usuario['office'];
            $this->familia       = $usuario['familia'];
            $this->version       = $usuario['version'];
            $this->precio        = $usuario['precio'];
            $this->instalaciones = $usuario['instalaciones'];
            $this->compra        = $usuario['compra'];
            $this->balance       = $usuario['balance'];
            $this->balancec      = $usuario['balancec'];
            $this->tipo          = $usuario['tipo'];
            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE id!=0 ORDER BY id');
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_cliente($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    /*function listar_todo_clienteSAM($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE cliente = :cliente ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios paginados
    /*function listar_todo_paginado($inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE id!=0 ORDER BY  id LIMIT ' . $inicio . ', ' . $fin);
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    function listar_todo_cliente1($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND '
            . '(familia LIKE "%Visio%" OR familia LIKE "%Project%" OR familia LIKE "%Visual%" OR familia LIKE "%System%" ' 
            . 'OR familia LIKE "%Exchange%" OR familia LIKE "%Sharepoint%" OR familia LIKE "%Skype%") ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_cliente2($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Contar el total de Usuarios
    /*function total() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM balance_SPLA WHERE id!=0');
            $sql->execute();
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_tipo($tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE tipo = :tipo ORDER BY id');
            $sql->execute(array(':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_office($cliente, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND office LIKE :edicion ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_office3($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND (office LIKE "%Professional%" OR office LIKE "%profesional%") ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Contar el total de Usuarios
    /*function total_office3($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM balance_SPLA WHERE cliente = :cliente AND (office LIKE "%Professional%" OR office LIKE "%profesional%") ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/

    // Obtener listado de todos 
    /*function listar_todo_office2($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND '
            . 'id NOT IN (SELECT id FROM balance_SPLA WHERE cliente = :cliente AND (office LIKE "%Professional%" '
            . 'OR office LIKE "%Standard%" OR office LIKE "%Profesional%")) ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Verificar si e-mail ya existe
    /*function cliente_existe($cliente, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM balance_SPLA WHERE cliente = :cliente AND id != :id');
            $sql->execute(array(':cliente'=>$cliente, ':id'=>$id));
            $row = $sql->fetch();
            if($row["cantidad"] > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND office LIKE :edicion ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familiasGrafico($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND office LIKE :edicion AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias1($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias1Sam($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias1SamArchivo($archivo, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE archivo = :archivo AND familia LIKE :familia AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias6($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND familia  NOT LIKE "%Windows Server%" AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias6Sam($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND familia  NOT LIKE "%Windows Server%" AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias6SamArchivo($archivo, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE archivo = :archivo AND familia LIKE :familia AND familia  NOT LIKE "%Windows Server%" AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias8($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" '
            . 'AND office NOT LIKE "%Datacenter%" AND office NOT LIKE "%Professional%" ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias9($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Datacenter%" AND '
            . 'office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias9Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Datacenter%" AND '
            . 'office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias9SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE archivo = :archivo AND familia LIKE :familia '
            . 'AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Datacenter%" AND '
            . 'office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias7($cliente, $empleado, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia  NOT LIKE "%Windows%" '
            . 'AND familia  NOT LIKE "%Office%" AND familia  NOT LIKE "%Project%" AND familia  NOT LIKE "%visio%" AND '
            . 'familia  NOT LIKE "%SQL%" AND familia LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias7Sam($cliente, $empleado, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE cliente = :cliente AND empleado = :empleado AND familia  NOT LIKE "%Windows%" '
            . 'AND familia  NOT LIKE "%Office%" AND familia  NOT LIKE "%Project%" AND familia  NOT LIKE "%visio%" AND '
            . 'familia  NOT LIKE "%SQL%" AND familia LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias7SamArchivo($archivo, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE archivo = :archivo AND familia  NOT LIKE "%Windows%" '
            . 'AND familia  NOT LIKE "%Office%" AND familia  NOT LIKE "%Project%" AND familia  NOT LIKE "%visio%" AND '
            . 'familia  NOT LIKE "%SQL%" AND familia LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias10($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias10Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias10SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLASam WHERE archivo = :archivo AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familiasSAM($cliente, $familia, $edicion, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia AND office LIKE :edicion ORDER BY version');
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias2($cliente, $empleado, $familia, $edicion, $edicion2) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND '
            . 'office NOT LIKE :edicion AND  office NOT LIKE :edicion2 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias2Grafico($cliente, $empleado, $familia, $edicion, $edicion2) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND  office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familias2SAM($cliente, $familia, $edicion, $edicion2, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia AND office NOT LIKE :edicion AND  office NOT LIKE :edicion2 ORDER BY version');
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias3($cliente, $empleado, $familia, $edicion, $edicion2, $edicion3) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND  office NOT LIKE :edicion3 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', 
            ':edicion2'=>'%' . $edicion2 . '%', ':edicion3'=>$edicion3));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias3Grafico($cliente, $empleado, $familia, $edicion, $edicion2, $edicion3) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND  office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND  office NOT LIKE :edicion3 AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', 
            ':edicion2'=>'%' . $edicion2 . '%', ':edicion3'=>$edicion3));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familias3SAM($cliente, $familia, $edicion, $edicion2, $edicion3, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia AND  office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND  office NOT LIKE '
            . ':ediicon3 ORDER BY version');
            $sql->execute(array(':arcgivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', 
            ':edicion2'=>'%' . $edicion2 . '%', ':edicion3'=>$edicion3));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias4($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY office,version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias4Grafico($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente empleado = :empleado AND familia = :familia '
            . 'AND instalaciones > 0 ORDER BY office,version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familias5($cliente, $familia, $tipo){ 
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_SPLA WHERE cliente = :cliente AND familia = :familia '
            . 'AND tipo = :tipo ORDER BY office,version');
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    /*function listar_todo_familias4SAM($cliente, $familia, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia ORDER BY office,version');
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csv2/" . $titulo_archivo;
            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
    
    function cabeceraReporteBalanza($myWorkSheet){
        $myWorkSheet->setCellValue('A1', 'Producto')
                    ->setCellValue('B1', 'Edición')
                    ->setCellValue('C1', 'Versión')
                    ->setCellValue('D1', 'Instalaciones')
                    ->setCellValue('E1', 'Compras')
                    ->setCellValue('F1', 'Neto')
                    ->setCellValue('G1', 'Precio');
    }
    
    function detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, $indice){
        $i = 2;
        foreach ($listar_Of as $reg_equipos) {
            $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                        ->setCellValue('B' . $i, $reg_equipos["office"])
                        ->setCellValue('C' . $i, $reg_equipos["version"])
                        ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                        ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                        ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                        ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
            $i++;
        }
        $objPHPExcel->addSheet($myWorkSheet, $indice);
    }
    
    function balanceEjecutivo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia,
                    SUM(compra) AS totalCompras,
                    SUM(instalaciones) AS totalInstalaciones,
                    SUM(compra) - SUM(instalaciones) AS neto,
                    precio,
                    (SUM(compra) - SUM(instalaciones)) * precio AS total
                FROM balance_SPLA
                WHERE cliente = :cliente AND precio > 0
                GROUP BY familia
                ORDER BY familia');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}