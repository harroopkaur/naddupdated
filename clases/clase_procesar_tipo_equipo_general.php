<?php
class clase_procesar_tipo_equipo_general extends General{
    private $client_id;
    private $client_empleado;
    private $archivoEquipos;
    private $procesarEquipos;
    private $lDatoControl;
    private $lHostName;
    private $lFabricante;
    private $lModelo;
    private $opcion; 
    private $idDiagnostic;
    private $bloque;
    private $bloqueValores;
    private $insertarBloque;
    private $tabTipoEquipo;
    private $campoTipoEquipo;
    private $datoControl;
    private $hostName;  
    private $fabricante;
    private $modelo;
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabTipoEquipo . " (" . $this->campoTipoEquipo . ", dato_control, host_name, fabricante, modelo) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM " . $this->tabTipoEquipo . " WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function setTabTipoEquipo($tabTipoEquipo){
        $this->tabTipoEquipo = $tabTipoEquipo;
    } 
    
    function procesarTipoEquipo($client_id, $client_empleado, $archivoEquipos, $procesarEquipos, $lDatoControl, 
    $lHostName, $lFabricante, $lModelo, $opcion, $idDiagnostic = 0){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->archivoEquipos = $archivoEquipos;
        $this->procesarEquipos = $procesarEquipos; 
        $this->lDatoControl = $lDatoControl; 
        $this->lHostName = $lHostName;
        $this->lFabricante = $lFabricante;
        $this->lModelo = $lModelo;
        $this->opcion = $opcion;
        $this->idDiagnostic = $idDiagnostic;
        
        $this->cabeceraTablas();
        
        if($this->obtenerSeparadorUniversal($this->archivoEquipos, 2, 4) === true && $this->procesarEquipos === true){
            if (($fichero = fopen($this->archivoEquipos, "r")) !== false) {
                $this->cicloInsertar($fichero);

                $this->verificarRegistrosInsertar1();
            
                fclose($fichero);
            }
        }
    }
            
    function cabeceraTablas(){
        if($this->opcion == "Cloud"){
            $this->tabTipoEquipo = "consolidadoTipoEquipoMSCloud";
            $this->campoTipoEquipo = "idDiagnostic";
        }else if($this->opcion == "Diagnostic"){
            $this->tabTipoEquipo = "consolidadoTipoEquipoSAMDiagnostic";
            $this->campoTipoEquipo = "idDiagnostic";
        } else if($this->opcion == "microsoft"){
            $this->tabTipoEquipo = "consolidadoTipoEquipo";
            $this->campoTipoEquipo = "cliente, empleado";
        }
    }
    
    function cicloInsertar($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {                 
            if($i > 2 && $datos[$this->lDatoControl] != 'Dato de Control'){
                $this->crearBloque($j);

                $this->setValores($datos);                
                $this->crearBloqueValores($j);

                $j = $this->verificarRegistrosInsertar($j);
                
                $j++;
            }
            
            $i++; 
        }
    }
    
    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        }
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":dato_control" . $j . ", "
            . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":dato_control" . $j . ", "
            . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j){
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }
        
        $this->bloqueValores[":dato_control" . $j] = $this->datoControl;
        $this->bloqueValores[":host_name" . $j] = $this->hostName;
        $this->bloqueValores[":fabricante" . $j] = $this->fabricante;
        $this->bloqueValores[":modelo" . $j] = $this->modelo;
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function insertarGeneral(){
        if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        } 
    }     
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
    
    function setValores($datos){
        $this->datoControl = "";
        $this->hostName = "";  
        $this->fabricante = "";
        $this->modelo = "";
        
        if(isset($datos[$this->lDatoControl])){
            $this->datoControl = $this->truncarString($this->get_escape(utf8_encode($datos[$this->lDatoControl])), 250);
        }

        if(isset($datos[$this->lHostName])){
            $this->hostName = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($datos[$this->lHostName]))), 250);   
        }

        if(isset($datos[$this->lFabricante])){
            $this->fabricante = $this->truncarString($this->get_escape(utf8_encode($datos[$this->lFabricante])), 250);
        }
        
        if(isset($datos[$this->lModelo])){
            $this->modelo = $this->truncarString($this->get_escape(utf8_encode($datos[$this->lModelo])), 250);
        }
    }
}
