<?php
class fabricantesActivos extends General{
    public $error;
    
    function agregarFabricanteCliente($cliente, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO fabricantesCliente (idFabricante, cliente) '
            . 'VALUES (:fabricante, :cliente)');
            $sql->execute(array(':fabricante'=>$fabricante, ':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarFabricanteCliente($cliente, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE fabricantesCliente SET estado = 0 WHERE idFabricante = :fabricante AND cliente = :cliente');
            $sql->execute(array(':fabricante'=>$fabricante, ':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function activarFabricanteCliente($cliente, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE fabricantesCliente SET estado = 1 WHERE idFabricante = :fabricante AND cliente = :cliente');
            $sql->execute(array(':fabricante'=>$fabricante, ':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existeFabricante($cliente, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM fabricantesCliente WHERE cliente = :cliente AND idFabricante = :fabricante AND estado = 1');
            $sql->execute(array(':cliente'=>$cliente, ':fabricante'=>$fabricante));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeFabricanteCliente($cliente, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM fabricantesCliente WHERE cliente = :cliente AND idFabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':fabricante'=>$fabricante));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listar_fabricantes($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla.idFabricante,
                    tabla.nombre
                FROM (SELECT fabricantes.idFabricante,
                        IF(fabricantes.nombre = "IBM", "Windows-IBM", IF(fabricantes.nombre = "Oracle", "Windows-Oracle", fabricantes.nombre)) AS nombre
                    FROM fabricantesCliente 
                        INNER JOIN fabricantes ON fabricantesCliente.idFabricante = fabricantes.idFabricante AND fabricantes.status = 1
                    WHERE fabricantesCliente.cliente = :cliente AND fabricantesCliente.estado = 1
                    ORDER BY fabricantes.nombre) tabla
                ORDER BY tabla.nombre');
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM fabricantesCliente 
                INNER JOIN fabricantes ON fabricantesCliente.idFabricante = fabricantes.idFabricante AND fabricantes.status = 1
            WHERE fabricantesCliente.cliente = :cliente AND fabricantesCliente.estado = 1
            ORDER BY fabricantes.nombre');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function gantt_inicio($cliente, $empleado) {
        try{
            $this->conexion();
            /*$query = "SELECT fabricantes.idFabricante,
                    fabricantes.nombre,
                    archivosFabricantes.fechaRegistro
                FROM fabricantesCliente
                    INNER JOIN fabricantes ON fabricantesCliente.idFabricante = fabricantes.idFabricante AND fabricantes.status = 1
                    LEFT JOIN archivosFabricantes ON fabricantesCliente.idFabricante = archivosFabricantes.fabricante AND archivosFabricantes.empleado = :empleado
                WHERE fabricantesCliente.cliente = :cliente AND fabricantesCliente.estado = 1
                ORDER BY fabricantes.nombre";*/
            $query = "SELECT fabricantes.idFabricante,
                    fabricantes.nombre,
                    archivosFabricantes.fechaRegistro,
                    IFNULL(ganttDetalle.P1, IFNULL(ganttDetalle.P2, IFNULL(ganttDetalle.P3, IFNULL(ganttDetalle.P4, '0000-00-00')))) AS presupuesto
                FROM fabricantesCliente
                    INNER JOIN fabricantes ON fabricantesCliente.idFabricante = fabricantes.idFabricante AND fabricantes.status = 1 AND fabricantes.idFabricante IN (1,2,3,4,5,6,7,8,10)
                    LEFT JOIN archivosFabricantes ON fabricantesCliente.idFabricante = archivosFabricantes.fabricante AND archivosFabricantes.empleado = :empleado
                    LEFT JOIN gantt ON fabricantesCliente.cliente = gantt.cliente AND gantt.id = (SELECT MAX(id) FROM gantt WHERE cliente = fabricantesCliente.cliente)
                    LEFT JOIN ganttDetalle ON gantt.id = ganttDetalle.idGantt
                WHERE fabricantesCliente.cliente = :cliente AND fabricantesCliente.estado = 1
                GROUP BY fabricantes.idFabricante
                ORDER BY fabricantes.nombre";
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function gantt_fechas($cliente, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT pagosFabricantesDetalle.idFabricante,
                    pagosFabricantesDetalle.aniversario,
                    pagosFabricantesDetalle.renovacion,
                    pagosFabricantesDetalle.monto
                FROM pagosFabricantesDetalle
                    INNER JOIN pagosFabricantes ON pagosFabricantesDetalle.idPago = pagosFabricantes.id AND pagosFabricantes.cliente = :cliente AND pagosFabricantes.estado = 1
                WHERE pagosFabricantesDetalle.idFabricante = :fabricante AND pagosFabricantesDetalle.estado = 1 AND pagosFabricantes.id = (SELECT MAX(id) FROM pagosFabricantes WHERE pagosFabricantes.cliente = :cliente AND pagosFabricantes.estado = 1)
                GROUP BY pagosFabricantesDetalle.idFabricante');
            $sql->execute(array(':cliente'=>$cliente, ':fabricante'=>$fabricante));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('idFabricante'=>'', 'aniversario'=>'', 'renovacion'=>'', 'monto'=>'');
        }
    }
}