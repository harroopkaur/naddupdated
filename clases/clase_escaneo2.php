<?php
class Scaneo_f extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $equipo;
    var $status;
    var $errors;
    var $error = NULL;
  
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $status, $errors) {
        $this->conexion();
        $query = "INSERT INTO escaneo_equipos2 (cliente, empleado, equipo, status, errors) ";
        $query .= "VALUES (:cliente, :empleado, :equipo, :status, :errors)";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':status'=>$status, ':errors'=>$errors));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO escaneo_equipos2 (cliente, empleado, equipo, status, errors) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        $this->conexion();
        $query = "DELETE FROM escaneo_equipos2 WHERE cliente = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM escaneo_equipos2 WHERE cliente = :cliente AND empleado = :empleado ORDER BY id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo2($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT id,
                cliente,
                empleado,
                equipo,
                status,
                errors
            FROM escaneo_equipos2
            WHERE cliente = :cliente AND empleado = :empleado AND errors = 'Ninguno'
            GROUP BY equipo

            UNION

            SELECT id,
                cliente,
                empleado,
                equipo,
                status,
                errors
            FROM escaneo_equipos2
            WHERE cliente = :cliente AND empleado = :empleado AND errors != '' AND NOT errors IS NULL
            AND equipo NOT IN (SELECT equipo FROM escaneo_equipos2 WHERE cliente = :cliente AND empleado = :empleado AND errors = 'Ninguno' GROUP BY equipo)
            GROUP BY equipo";
        //$query = "SELECT * FROM escaneo_equipos2 WHERE cliente = :cliente AND empleado = :empleado GROUP BY equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equiposNoDescubiertos($cliente, $empleado){
        $query = "SELECT *
            FROM escaneo_equipos2
            WHERE cliente = :cliente AND equipo NOT IN (SELECT equipo
                FROM escaneo_equipos2
                WHERE cliente = :cliente AND errors = 'Ninguno'
                GROUP BY equipo)
            GROUP BY equipo";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equiposNoDescubiertosAsignacion($cliente, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        }
        
        /*$query = "SELECT *
            FROM escaneo_equipos2
                INNER JOIN detalles_equipo2 ON escaneo_equipos2.cliente = detalles_equipo2.cliente AND escaneo_equipos2.equipo = detalles_equipo2.equipo " . $where . "
            WHERE escaneo_equipos2.cliente = :cliente AND escaneo_equipos2.equipo NOT IN (SELECT equipo
                FROM escaneo_equipos2
                WHERE cliente = :cliente AND errors = 'Ninguno'
                GROUP BY equipo)
            GROUP BY escaneo_equipos2.equipo";*/
        
        $query = "SELECT detalles_equipo2.equipo,
                IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                IFNULL(IF(detalles_equipo2.errors = '', 'No Escaneado', detalles_equipo2.errors), 'No Escaneado') AS errors
            FROM detalles_equipo2
                /*LEFT JOIN (SELECT escaneo_equipos2.cliente, escaneo_equipos2.equipo, escaneo_equipos2.errors
                    FROM escaneo_equipos2
                    WHERE escaneo_equipos2.cliente = :cliente AND escaneo_equipos2.equipo NOT IN (SELECT equipo
                             FROM escaneo_equipos2
                             WHERE cliente = :cliente AND errors = 'Ninguno'
                             GROUP BY equipo)
                    GROUP BY escaneo_equipos2.equipo) escaneo_equipos2 ON detalles_equipo2.cliente = escaneo_equipos2.cliente AND detalles_equipo2.equipo = escaneo_equipos2.equipo*/
             WHERE detalles_equipo2.cliente = :cliente AND (detalles_equipo2.errors != 'Ninguno' OR detalles_equipo2.errors IS NULL) " . $where;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equiposNoDescubiertosActivosAsignacion($cliente, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        }
        
        /*$query = "SELECT *
            FROM escaneo_equipos2
                INNER JOIN detalles_equipo2 ON escaneo_equipos2.cliente = detalles_equipo2.cliente AND escaneo_equipos2.equipo = detalles_equipo2.equipo " . $where . "
            WHERE escaneo_equipos2.cliente = :cliente AND escaneo_equipos2.equipo NOT IN (SELECT equipo
                FROM escaneo_equipos2
                WHERE cliente = :cliente AND errors = 'Ninguno'
                GROUP BY equipo)
            GROUP BY escaneo_equipos2.equipo";*/
        
        $query = "SELECT detalles_equipo2.equipo,
                IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                IFNULL(IF(detalles_equipo2.errors = '', 'No Escaneado', detalles_equipo2.errors), 'No Escaneado') AS errors
            FROM detalles_equipo2
                /*LEFT JOIN (SELECT escaneo_equipos2.cliente, escaneo_equipos2.equipo, escaneo_equipos2.errors
                    FROM escaneo_equipos2
                    WHERE escaneo_equipos2.cliente = :cliente AND escaneo_equipos2.equipo NOT IN (SELECT equipo
                             FROM escaneo_equipos2
                             WHERE cliente = :cliente AND errors = 'Ninguno'
                             GROUP BY equipo)
                    GROUP BY escaneo_equipos2.equipo) escaneo_equipos2 ON detalles_equipo2.cliente = escaneo_equipos2.cliente AND detalles_equipo2.equipo = escaneo_equipos2.equipo*/
            WHERE detalles_equipo2.cliente = :cliente AND (detalles_equipo2.errors != 'Ninguno' OR detalles_equipo2.errors IS NULL) 
            AND detalles_equipo2.rango IN (1, 2, 3) " . $where;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function totalNoDescubiertosActivosGraficoAsignacion($cliente, $tipo, $error, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':tipo'=>$tipo);

        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        }
        
        if($error != "Otros"){
            $where .= " AND detalles_equipo2.errors = :error ";
            $array[":error"] = $error;
        }
        else{
            $where .= " AND (detalles_equipo2.errors NOT IN ('Error 462 - The remote server machine does not exist or is unavailable',
            'Error 70 - Permission denied', 'Ping Sin Respuesta') OR escaneo_equipos2.errors IS NULL) ";
        }
       
        $query = "SELECT COUNT(detalles_equipo2.equipo) AS cantidad 
            FROM detalles_equipo2
                /*LEFT JOIN (SELECT escaneo_equipos2.cliente, escaneo_equipos2.equipo, escaneo_equipos2.errors
                    FROM escaneo_equipos2
                    WHERE escaneo_equipos2.cliente = :cliente AND escaneo_equipos2.equipo NOT IN (SELECT equipo
                             FROM escaneo_equipos2
                             WHERE cliente = :cliente AND errors = 'Ninguno' 
                             GROUP BY equipo)
                    GROUP BY escaneo_equipos2.equipo) escaneo_equipos2 ON detalles_equipo2.cliente = escaneo_equipos2.cliente AND detalles_equipo2.equipo = escaneo_equipos2.equipo*/
             WHERE detalles_equipo2.cliente = :cliente AND (detalles_equipo2.errors != 'Ninguno' OR detalles_equipo2.errors IS NULL) 
             AND detalles_equipo2.rango IN (1, 2, 3) AND detalles_equipo2.tipo = :tipo " . $where;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            $row = $sql->fetch(); 
           return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalNoDescubiertosGraficoAsignacion($cliente, $tipo, $error, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':tipo'=>$tipo);

        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        }
        
        if($error != "Otros"){
            $where .= " AND escaneo_equipos2.errors = :error ";
            $array[":error"] = $error;
        }
        else{
            $where .= " AND (detalles_equipo2.errors NOT IN ('Error 462 - The remote server machine does not exist or is unavailable',
            'Error 70 - Permission denied', 'Ping Sin Respuesta') OR detalles_equipo2.errors IS NULL) ";
        }
       
        $query = "SELECT COUNT(detalles_equipo2.equipo) AS cantidad 
            FROM detalles_equipo2
                /*LEFT JOIN (SELECT escaneo_equipos2.cliente, escaneo_equipos2.equipo, escaneo_equipos2.errors
                    FROM escaneo_equipos2
                    WHERE escaneo_equipos2.cliente = :cliente AND escaneo_equipos2.equipo NOT IN (SELECT equipo
                             FROM escaneo_equipos2
                             WHERE cliente = :cliente AND errors = 'Ninguno' 
                             GROUP BY equipo)
                    GROUP BY escaneo_equipos2.equipo) escaneo_equipos2 ON detalles_equipo2.cliente = escaneo_equipos2.cliente AND detalles_equipo2.equipo = escaneo_equipos2.equipo*/
             WHERE detalles_equipo2.cliente = :cliente AND (detalles_equipo2.errors != 'Ninguno' OR detalles_equipo2.errors IS NULL) 
             AND detalles_equipo2.tipo = :tipo " . $where;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            $row = $sql->fetch(); 
           return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    //inicio reporte SAM
    function listar_equiposNoDescubiertosSam($archivo){
        $query = "SELECT *
            FROM escaneo_equipos2Sam
            WHERE archivo = :archivo
            GROUP BY equipo";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        } 
    }
    //fin reporte SAM
}