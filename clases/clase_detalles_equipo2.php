<?php
class DetallesE_f extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $equipo;
    var $os;
    var $dias1;
    var $dias2;
    var $dias3;
    var $min;
    var $activo;
    var $tipo;
    var $rango;
    var $errors;
    var $error = NULL;
    var $archivo;
    var $ActivoAD;
    var $LaTool;
    var $usabilidad;
    var $SQLServer;
    var $office;
    var $visio;
    var $project;
    var $visual;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $os, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipo2 (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES (:cliente, :empleado, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo, 
            ':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipo2 (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function insertarSAM($archivo, $cliente, $equipo, $os, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detallesEquipo2Sam (archivo, cliente, equipo, os, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES (:archivo, :cliente, :equipo, :os, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)");
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':equipo'=>$equipo, ':os'=>$os, ':dias1'=>$dias1, 
            ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo, ':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Actualizar
    function actualizar($cliente, $empleado, $equipo, $errors) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo2 SET errors = :errors "
            . "WHERE equipo = :equipo AND cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':equipo'=>$equipo, ':cliente'=>$cliente, ':empleado'=>$empleado, ':errors'=>$errors));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function actualizarSAM($cliente, $equipo, $errors, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detallesEquipo2Sam SET errors = :errors "
            . "WHERE equipo = :equipo AND cliente = :cliente AND archivo = :archivo");
            $sql->execute(array(':equipo'=>$equipo, ':cliente'=>$cliente, ':archivo'=>$archivo, ':errors'=>$errors));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM detalles_equipo2 WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function limpiarAsignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo2 SET asignacion = null WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarAsignacion($cliente, $empleado, $equipo, $asignacion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo2 SET asignacion = :asignacion WHERE cliente = :cliente AND equipo = :equipo");
            $sql->execute(array(':cliente'=>$cliente, ':asignacion'=>$asignacion, ':equipo'=>$equipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_asignacion($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT asignacion FROM detalles_equipo2 WHERE cliente = :cliente GROUP BY asignacion ORDER BY asignacion");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND id != 0 AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo1($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND id != 0 ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo1Asignacion($cliente, $asignacion, $asignaciones) {
        try{
            $array = array(':cliente'=>$cliente,);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion "; 
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM detalles_equipo2 "
            . "WHERE cliente = :cliente " . $where 
            . " ORDER BY id");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listarDescubiertos($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT SUM(IF(tipo = 1 AND rango = 1 AND errors = 'Ninguno', 1, 0)) AS CActivoSi,
                    SUM(IF(tipo = 1 AND rango = 1 AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS CActivoNo,
                    SUM(IF(tipo = 1 AND rango IN (2,3) AND errors = 'Ninguno', 1, 0)) AS CProbableSi,
                    SUM(IF(tipo = 1 AND rango IN (2,3) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS CProbableNo,
                    SUM(IF(tipo = 1 AND rango IN (4,5) AND errors = 'Ninguno', 1, 0)) AS CObsoletoSi,
                    SUM(IF(tipo = 1 AND rango IN (4,5) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS CObsoletoNo,
                    SUM(IF(tipo = 2 AND rango = 1 AND errors = 'Ninguno', 1, 0)) AS SActivoSi,
                    SUM(IF(tipo = 2 AND rango = 1 AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS SActivoNo,
                    SUM(IF(tipo = 2 AND rango IN (2,3) AND errors = 'Ninguno', 1, 0)) AS SProbableSi,
                    SUM(IF(tipo = 2 AND rango IN (2,3) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS SProbableNo,
                    SUM(IF(tipo = 2 AND rango IN (4,5) AND errors = 'Ninguno', 1, 0)) AS SObsoletoSi,
                    SUM(IF(tipo = 2 AND rango IN (4,5) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS SObsoletoNo
                FROM detalles_equipo2
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('CActivoSi'=>0, 'CActivoNo'=>0, 'CProbableSi'=>0, 'CProbableNo'=>0, 'CObsoletoSi'=>0,
            'CObsoletoNo'=>0, 'SActivoSi'=>0, 'SActivoNo'=>0, 'SProbableSi'=>0, 'SProbableNo'=>0, 'SObsoletoSi'=>0, 
            'SObsoletoNo'=>0);
        }
    }
    
    function listarDescubiertosAsignacion($cliente, $asignacion, $asignaciones){
        try{
            $where = "";
            $array = array(':cliente'=>$cliente);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT SUM(IF(tipo = 1 AND rango = 1 AND errors = 'Ninguno', 1, 0)) AS CActivoSi,
                    SUM(IF(tipo = 1 AND rango = 1 AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS CActivoNo,
                    SUM(IF(tipo = 1 AND rango IN (2,3) AND errors = 'Ninguno', 1, 0)) AS CProbableSi,
                    SUM(IF(tipo = 1 AND rango IN (2,3) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS CProbableNo,
                    SUM(IF(tipo = 1 AND rango IN (4,5) AND errors = 'Ninguno', 1, 0)) AS CObsoletoSi,
                    SUM(IF(tipo = 1 AND rango IN (4,5) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS CObsoletoNo,
                    SUM(IF(tipo = 2 AND rango = 1 AND errors = 'Ninguno', 1, 0)) AS SActivoSi,
                    SUM(IF(tipo = 2 AND rango = 1 AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS SActivoNo,
                    SUM(IF(tipo = 2 AND rango IN (2,3) AND errors = 'Ninguno', 1, 0)) AS SProbableSi,
                    SUM(IF(tipo = 2 AND rango IN (2,3) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS SProbableNo,
                    SUM(IF(tipo = 2 AND rango IN (4,5) AND errors = 'Ninguno', 1, 0)) AS SObsoletoSi,
                    SUM(IF(tipo = 2 AND rango IN (4,5) AND (errors != 'Ninguno' OR errors IS NULL), 1, 0)) AS SObsoletoNo
                FROM detalles_equipo2
                WHERE detalles_equipo2.cliente = :cliente " . $where);
            $sql->execute($array);
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('CActivoSi'=>0, 'CActivoNo'=>0, 'CProbableSi'=>0, 'CProbableNo'=>0, 'CObsoletoSi'=>0,
            'CObsoletoNo'=>0, 'SActivoSi'=>0, 'SActivoNo'=>0, 'SProbableSi'=>0, 'SProbableNo'=>0, 'SObsoletoSi'=>0, 
            'SObsoletoNo'=>0);
        }
    }
    
    function listar_todoWindows($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND familia = 'Windows' ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_segmentado($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND id != 0 AND os LIKE '%Windows%' AND errors != '' ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_segmentado1($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND id != 0 AND errors != '' ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_segmentado1Asignacion($cliente, $asignacion, $asignaciones) {
        try{
            $array = array(':cliente'=>$cliente);
            if($asignacion != ""){
                $array[":asignacion"] = $asignacion;
                $where = " AND asignacion = :asignacion ";
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM detalles_equipo2 "
            . "WHERE cliente = :cliente AND errors != '' " . $where
            . " ORDER BY id");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todoEdicion($cliente, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND id != 0 AND os LIKE '%Windows%' AND os LIKE :edicion ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    /*function listar_todoSAM($cliente) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2Sam WHERE cliente = :cliente AND id != 0 AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todoSAMArchivo($archivo) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2Sam WHERE archivo = :archivo AND id != 0 AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todoSAMArchivo1($archivo) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2Sam WHERE archivo = :archivo AND id != 0 ORDER BY id");
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_todo_office($cliente, $empleado, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND edicion = :edicion "
            . "AND os LIKE '%Windows%' ORDER BY version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_WindowsOS($cliente, $empleado, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado "
            . "AND familia = 'Windows' AND edicion = :edicion AND version = :version AND activo = 1");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    function listar_WindowsServer($cliente, $empleado, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado "
            . "AND familia = 'Windows Server' AND edicion = :edicion AND version = :version AND activo = 1");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todog0($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND id != 0 ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todog0Ordenar($cliente, $empleado, $ordenar, $direccion) {
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND id != 0 ORDER BY " . $ordenar . " " . $direccion;
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todog0Asignacion($cliente, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        if ($asignacion != "") {
            $where = " AND asignacion = :asignacion";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }

        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                asignacion
            FROM detalles_equipo2 WHERE cliente = :cliente AND os LIKE '%windows%' " . $where;
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function ($cliente) {
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo2Sam WHERE cliente = :cliente AND os LIKE '%windows%' AND id != 0";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todog0SamDetalleArchivo($archivo) {
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo2Sam WHERE archivo = :archivo AND os LIKE '%windows%' AND id != 0";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todog1Ordenar($cliente, $empleado, $edicion, $ordenar, $direccion) {
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0 ORDER BY " . $ordenar . " " . $direccion;
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todog1SamDetalle($cliente, $edicion) {
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo2Sam WHERE cliente = :cliente AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0";        
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todog1SamDetalleArchivo($archivo, $edicion) {
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo2Sam WHERE archivo = :archivo AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0";        
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todog2Ordenar($cliente, $empleado, $edicion, $edicion2, $ordenar, $direccion) {
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad 
            FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id !=0 ORDER BY " . $ordenar . " " . $direccion;
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todog2SamDetalle($cliente, $edicion, $edicion2) {
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad 
            FROM detalles_equipo2Sam WHERE cliente = :cliente AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0"; 
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todog2SamDetalleArchivo($archivo, $edicion, $edicion2) {
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad 
            FROM detalles_equipo2Sam WHERE archivo = :archivo AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0"; 
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_optimizacion($cliente, $empleado, $ordenar, $direccion) {
        if ($ordenar == "") {
            $ordenar = "detalles_equipo2.id";
        }
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND detalles_equipo2.empleado = visio.empleado AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND detalles_equipo2.empleado = project.empleado AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND detalles_equipo2.empleado = visual.empleado AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.id
            ORDER BY " . $ordenar . " " . $direccion;
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacionAsignacion($cliente, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo " . $where . ") > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND 
            rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' " . $where . "
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacionServidor($cliente, $empleado, $ordenar, $direccion) {
        if ($ordenar == "") {
            $ordenar = "detalles_equipo2.id";
        }
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND 
                detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND 
                detalles_equipo2.cliente = sqlServer.cliente AND detalles_equipo2.empleado = sqlServer.empleado AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND 
            rango NOT IN (1) AND detalles_equipo2.familia = 'Windows Server'
            GROUP BY detalles_equipo2.id
            ORDER BY " . $ordenar . " " . $direccion;
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacionServidorAsignacion($cliente, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo " . $where . ") > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND 
                detalles_equipo2.cliente = sqlServer.cliente AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.cliente = :cliente AND 
            rango NOT IN (1) AND detalles_equipo2.familia = 'Windows Server' " . $where . "
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacion1($cliente, $empleado) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND detalles_equipo2.empleado = visio.empleado AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND detalles_equipo2.empleado = project.empleado AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND detalles_equipo2.empleado = visual.empleado AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacion1Servidor($cliente, $empleado) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND detalles_equipo2.cliente = sqlServer.cliente AND detalles_equipo2.empleado = sqlServer.empleado AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows Server'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_optimizacionSam($cliente) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2Sam AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
             FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND office.familia = 'Office'
                LEFT JOIN resumen_office2Sam AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2Sam AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND project.familia = 'Project'
                LEFT JOIN resumen_office2Sam AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_optimizacionSamArchivo($archivo) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2Sam AS detEquipo WHERE detEquipo.archivo = :archivo AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
             FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND office.familia = 'Office'
                LEFT JOIN resumen_office2Sam AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2Sam AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND project.familia = 'Project'
                LEFT JOIN resumen_office2Sam AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_optimizacionSamServidor($cliente) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2Sam AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND detalles_equipo2.cliente = sqlServer.cliente AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.cliente = :cliente AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows Server'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_optimizacionSamServidorArchivo($archivo) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2Sam AS detEquipo WHERE detEquipo.archivo = :archivo AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND detalles_equipo2.cliente = sqlServer.cliente AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows Server'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*inicio detalleEquipo en Resumen*/
    function listar_detalleEquipo($cliente, $empleado) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                GROUP_CONCAT(office.edicion) AS office,
                GROUP_CONCAT(visio.edicion) AS visio,
                GROUP_CONCAT(project.edicion) AS project,
                GROUP_CONCAT(visual.edicion) AS visual
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND detalles_equipo2.empleado = visio.empleado AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND detalles_equipo2.empleado = project.empleado AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND detalles_equipo2.empleado = visual.empleado AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
        
        /*$query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS edicion
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = :familia
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }*/
    }
    
    function listar_detalleEquipoAsignacion($cliente, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND 
            detalles_equipo2.familia = 'Windows' " . $where . "
            GROUP BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_detalleEquipoServidor($cliente, $empleado) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                GROUP_CONCAT(sqlServer.edicion) AS SQLServer
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND detalles_equipo2.cliente = sqlServer.cliente AND detalles_equipo2.empleado = sqlServer.empleado AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows Server'
            GROUP BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_detalleEquipoServidorAsignacion($cliente, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (". $asignacion . ") ";
        }
        
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND detalles_equipo2.cliente = sqlServer.cliente AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.cliente = :cliente AND 
            detalles_equipo2.familia = 'Windows Server' " . $where . "
            GROUP BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_detalleEquipo1($cliente, $empleado) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.cliente = visio.cliente AND detalles_equipo2.empleado = visio.empleado AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.cliente = project.cliente AND detalles_equipo2.empleado = project.empleado AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.cliente = visual.cliente AND detalles_equipo2.empleado = visual.empleado AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_detalleEquipo1Servidor($cliente, $empleado) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo2.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo2 AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND detalles_equipo2.cliente = sqlServer.cliente AND detalles_equipo2.empleado = sqlServer.empleado AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows Server'
            GROUP BY detalles_equipo2.id
            ORDER BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    /*fin detalleEquipo en Resumen*/

    /*function listar_todog0SAM($cliente, $archivo) {
        $query = "SELECT * FROM detallesEquipo2Sam WHERE archivo = :archivo AND cliente = :cliente AND os LIKE '%windows%' AND id != 0 ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todog1($cliente, $empleado, $edicion) {
        $query = "SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0 ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todog1Asignacion($cliente, $edicion, $asignacion, $asignaciones) {   
        $array = array();
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") "; 
        } else{
            $where = " AND detalles_equipo2.asignacion = :asignacion "; 
            $array[":asignacion"] = $asignacion;
        }
        
        if($edicion == "Standard"){
            /*$where .= " AND NOT detalles_equipo2.os LIKE '%Embedded%' ";  /*"AND NOT detalles_equipo2.os LIKE '%Foundation%' AND NOT detalles_equipo2.os LIKE '%Essentials%' "
            . "AND NOT detalles_equipo2.os LIKE '%Empresa%' AND NOT detalles_equipo2.os LIKE '%Business%' AND NOT "
            . "detalles_equipo2.os LIKE '%Datacenter%' AND NOT detalles_equipo2.os LIKE '%Storage%' AND NOT detalles_equipo2.os LIKE '%Enterprise%' ";
            $edicion = '';*/
        } else if($edicion == "Professional"){
            $edicion = 'Pro';
        }
        
        $array[':cliente'] = $cliente;
        $array[':edicion'] = '%' . $edicion . '%';
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                asignacion
            FROM detalles_equipo2 
            WHERE cliente = :cliente AND os LIKE '%windows%' AND os LIKE :edicion " . $where . "
            ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todog1SAM($cliente, $edicion, $archivo) {
        $query = "SELECT * FROM detallesEquipo2Sam WHERE archivo = :archivo AND cliente = :cliente AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0 ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    // Obtener listado de todos los Usuarios
    function listar_todog2($cliente, $empleado, $edicion, $edicion2) {
        $query = "SELECT * FROM detalles_equipo2 WHERE cliente = :cliente AND empleado = :empleado AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0 ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todog2Asignacion($cliente, $edicion, $edicion2, $asignacion, $asignaciones) {
        $where = "";
        $array = array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%');
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") "; 
        } else{
            $where = " AND detalles_equipo2.asignacion = :asignacion "; 
            $array[":asignacion"] = $asignacion;
        }
        
        if($edicion2 == "Professional"){
            $where .= " AND os NOT LIKE '%Profesional%' AND os NOT LIKE '%Pro%' ";
        }
        
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                asignacion "
        . "FROM detalles_equipo2 "
        . "WHERE cliente = :cliente AND os  LIKE '%windows%' AND "
        . "os NOT LIKE :edicion AND os NOT LIKE :edicion2 " . $where 
        . "ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todog3Asignacion($cliente, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") "; 
        } else{
            $where = " AND detalles_equipo2.asignacion = :asignacion "; 
            $array[":asignacion"] = $asignacion;
        }
        
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad "
        . "FROM detalles_equipo2 "
        . "WHERE cliente = :cliente AND os  LIKE '%windows%' AND (detalles_equipo2.os LIKE '%Foundation%' OR detalles_equipo2.os LIKE '%Essentials%' "
            . "OR detalles_equipo2.os LIKE '%Empresa%' OR detalles_equipo2.os LIKE '%Business%' OR detalles_equipo2.os LIKE '%Storage%') " . $where 
        . "ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todog4Asignacion($cliente, $edicion, $edicion2, $edicion3, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%');
        $where = "";
        
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") "; 
        } else{
            $where = " AND detalles_equipo2.asignacion = :asignacion "; 
            $array[":asignacion"] = $asignacion;
        }
        
        $where .= " AND detalles_equipo2.os NOT LIKE '%Datacenter%' "; 
        
        if($edicion3 == "Professional"){
            $edicion3 = "Pro";
        }
        
        $array[":edicion3"] = '%' . $edicion3 . '%';
        
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad "
        . "FROM detalles_equipo2 "
        . "WHERE cliente = :cliente AND os  LIKE '%windows%' AND "
        . "os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND os NOT LIKE :edicion3 " . $where 
        . "ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todog2SAM($cliente, $edicion, $edicion2, $archivo) {
        $query = "SELECT * FROM detallesEquipo2Sam WHERE archivo = :archivo AND cliente = :cliente AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0 ORDER BY id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>$edicion2, ':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    /*function fechaSAM($cliente, $empleado) {
        $fecha = "";
        $query = "SELECT fecha FROM encGrafSam WHERE cliente = :cliente AND empleado = :empleado";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }*/
    
    function fechaSAMArchivo($archivo) {
        $fecha = "";
        $query = "SELECT fecha FROM encGrafSam WHERE archivo = :archivo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }
    
    function listaProductosOSDesuso($cliente, $empleado, $familia){
        $query = "SELECT *
            FROM detalles_equipo2 
            WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listaProductosOSDesusoAsignacion($cliente, $familia, $asignacion, $asignaciones){
        $where = "";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT *
            FROM detalles_equipo2 
            WHERE cliente = :cliente AND familia = :familia " . $where;
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function cantidadProductosOSUso($cliente, $empleado, $familia){
        $query = "SELECT COUNT(id) AS cantidad
            FROM detalles_equipo2 
            WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
    
    function cantidadProductosInstalados($cliente, $empleado, $familia, $os){
        try{
            $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = :familia
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = :os AND office.edicion != ''";
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':os'=>$os));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
    
    function listaProductosDesuso($cliente, $empleado, $familia, $os){
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS familia,
                'Desuso' AS usabilidad
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = :familia
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = :os AND office.edicion != ''
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':os'=>$os));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listaProductosDesusoAsignacion($cliente, $familia, $os, $asignacion, $asignaciones){
        $where = "";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia, ':os'=>$os);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS familia,
                'Desuso' AS usabilidad
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                detalles_equipo2.cliente = office.cliente 
                AND office.familia = :familia
            WHERE detalles_equipo2.cliente = :cliente AND 
            rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = :os AND office.edicion != '' " . $where . "
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function cantidadProductosOtrosInstalados($cliente, $empleado, $familia, $os){
        $where = "";
        for($i = 0; $i < count($familia); $i++){
            $where .= " AND office.familia != '" . $familia[$i] . "' ";
        }
        $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado " . $where . " 
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = :os AND office.edicion != ''";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':os'=>$os));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
    
    function listaProductosOtrosDesuso($cliente, $empleado, $familia, $os){
        $where = "";
        for($i = 0; $i < count($familia); $i++){
            $where .= " AND office.familia != '" . $familia[$i] . "' ";
        }
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS familia,
                'Desuso' AS usabilidad
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado " . $where . " 
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = :os AND office.edicion != ''
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':os'=>$os));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listaProductosOtrosDesusoAsignacion($cliente, $familia, $os, $asignacion, $asignaciones){
        $where = "";
        for($i = 0; $i < count($familia); $i++){
            $where .= " AND office.familia != '" . $familia[$i] . "' ";
        }
        
        $where1 = "";
        $array = array(':cliente'=>$cliente, ':os'=>$os);
        if($asignacion != ""){
            $where1 = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where1 = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS familia,
                'Desuso' AS usabilidad
            FROM detalles_equipo2
                LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                detalles_equipo2.cliente = office.cliente " . $where . " 
            WHERE detalles_equipo2.cliente = :cliente AND 
            rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = :os AND office.edicion != '' " . $where1 . "
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosDesusoCliente($cliente, $empleado){
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Office'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Visio'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION 
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Project'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Visual Studio'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION 
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia NOT IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            ";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosDesusoClienteAsignacion($cliente, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                    detalles_equipo2.cliente = office.cliente 
                    AND office.familia = 'Office'
                WHERE detalles_equipo2.cliente = :cliente AND 
                rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = 'Windows' AND office.familia != '' " . $where . "
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                    detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado 
                    AND office.familia = 'Visio'
                WHERE detalles_equipo2.cliente = :cliente AND 
                rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = 'Windows' AND office.familia != '' " . $where . "
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION 
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                    detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado 
                    AND office.familia = 'Project'
                WHERE detalles_equipo2.cliente = :cliente AND 
                rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = 'Windows' AND office.familia != '' " . $where . "
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                    detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado 
                    AND office.familia = 'Visual Studio'
                WHERE detalles_equipo2.cliente = :cliente AND 
                rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = 'Windows' AND office.familia != '' " . $where . "
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            /*UNION 
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                    detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado 
                    AND office.familia NOT IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
                WHERE detalles_equipo2.cliente = :cliente AND 
                rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = 'Windows' AND office.familia != '' " . $where . "
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version*/";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosUsoCliente($cliente, $empleado){
        $query = "SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Office'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Visio'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Project'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Visual Studio'
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia NOT IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            ";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosDesusoServidor($cliente, $empleado){
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado =  office.empleado AND office.familia = 'SQL Server'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows Server' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosDesusoServidorAsignacion($cliente, $asignacion, $asignaciones){
        $where = "";
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND 
                    detalles_equipo2.cliente = office.cliente  
                    AND office.familia = 'SQL Server'
                WHERE detalles_equipo2.cliente = :cliente AND 
                rango NOT IN (1, 2, 3) AND detalles_equipo2.familia = 'Windows Server' AND office.familia != '' " . $where . "
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosUsoServidor($cliente, $empleado){
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2
                    LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado =  office.empleado AND office.familia = 'SQL Server'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = 'Windows Server' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listaEquiposOffice($cliente, $empleado){
        $query = "SELECT detalles_equipo2.equipo
            FROM detalles_equipo2
                /*LEFT JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = 'Office'*/
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND rango > 1 AND detalles_equipo2.familia = 'Windows'";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    //inicio reporte SAM
    function listaProductosOSDesusoSAM($archivo, $familia){
        $query = "SELECT *
            FROM detalles_equipo2Sam 
            WHERE archivo = :archivo AND familia = :familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listaProductosDesusoSAM($archivo, $familia, $os){
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS familia,
                'Desuso' AS usabilidad
            FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.archivo AND office.familia = :familia
            WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = :os AND office.edicion != ''
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia, ':os'=>$os));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listaProductosOtrosDesusoSAM($archivo, $familia, $os){
        $where = "";
        for($i = 0; $i < count($familia); $i++){
            $where .= " AND office.familia != '" . $familia[$i] . "' ";
        }
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS familia,
                'Desuso' AS usabilidad
            FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.archivo " . $where . " 
            WHERE detalles_equipo2.archivo = :archivo AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = :os AND office.edicion != ''
            GROUP BY detalles_equipo2.id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':os'=>$os));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosDesusoClienteSam($archivo){
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2Sam AS detalles_equipo2
                    LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.archivo AND office.familia = 'Office'
                WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2Sam AS detalles_equipo2
                    LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.archivo AND office.familia = 'Visio'
                WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION 
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2Sam AS detalles_equipo2
                    LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.archivo AND office.familia = 'Project'
                WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2Sam AS detalles_equipo2
                    LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.archivo AND office.familia = 'Visual Studio'
                WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            
            UNION 
            
            SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2Sam AS detalles_equipo2
                    LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.empleado AND office.familia NOT IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
                WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version
            ";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosDesusoServidorSam($archivo){
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo2.id,
                    `detalles_equipo2`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo2Sam AS detalles_equipo2
                    LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo =  office.archivo AND office.familia = 'SQL Server'
                WHERE detalles_equipo2.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo2.familia = 'Windows Server' AND office.familia != ''
                GROUP BY detalles_equipo2.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    
    function listar_detalleEquipoSam($archivo) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual
            FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.archivo = office.archivo AND office.familia = 'Office'
                LEFT JOIN resumen_office2Sam AS visio ON detalles_equipo2.equipo = visio.equipo AND detalles_equipo2.archivo = visio.archivo AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2Sam AS project ON detalles_equipo2.equipo = project.equipo AND detalles_equipo2.archivo = project.archivo AND project.familia = 'Project'
                LEFT JOIN resumen_office2Sam AS visual ON detalles_equipo2.equipo = visual.equipo AND detalles_equipo2.archivo = visual.archivo AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo2.archivo = :archivo AND detalles_equipo2.familia = 'Windows'
            GROUP BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_detalleEquipoServidorSam($archivo) {
        $query = "SELECT detalles_equipo2.id,
                `detalles_equipo2`.equipo,
                detalles_equipo2.edicion AS os,
                sqlServer.edicion AS SQLServer
            FROM detalles_equipo2Sam AS detalles_equipo2
                LEFT JOIN resumen_office2Sam AS sqlServer ON detalles_equipo2.equipo = sqlServer.equipo AND detalles_equipo2.archivo = sqlServer.archivo AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo2.archivo = :archivo AND detalles_equipo2.familia = 'Windows Server'
            GROUP BY detalles_equipo2.equipo";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    //fin reporte SAM
    
    function total_equiposMicrosoft($cliente, $empleado, $os, $familia){
        $query = "SELECT COUNT(office.familia) AS cantidad
            FROM detalles_equipo2
                INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND detalles_equipo2.empleado = office.empleado AND office.familia = :familia
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = :os
            GROUP BY office.familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':os'=>$os, ':familia'=>$familia));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function total_equiposMicrosoftAsignacion($cliente, $os, $familia, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':os'=>$os, ':familia'=>$familia);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        $query = "SELECT COUNT(familia) AS cantidad
            FROM (SELECT office.equipo,
                    office.familia 
                FROM detalles_equipo2
                    INNER JOIN resumen_office2 AS office ON detalles_equipo2.equipo = office.equipo AND detalles_equipo2.cliente = office.cliente AND office.familia = :familia
                WHERE detalles_equipo2.cliente = :cliente AND 
                detalles_equipo2.familia = :os " . $where . "
                GROUP BY office.equipo, office.familia) tabla";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            $row = $sql->fetch();
            $cantidad = $row["cantidad"];
            if ($cantidad == ""){
                $cantidad = 0;
            }
            return $cantidad;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalWindows($cliente, $empleado, $os){
        $query = "SELECT COUNT(id) AS cantidad
            FROM detalles_equipo2
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia = :os";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':os'=>$os, ':os'=>$os));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalWindowsAsignacion($cliente, $os, $edicion, $asignacion, $asignaciones){
        $familia = " AND detalles_equipo2.familia = :os ";
        $where = "";
        
        if($os == "Windows Server"){
            $familia = " AND detalles_equipo2.familia IN (:os, 'Windows Terminal Server') ";
        }else if($os == "Windows" && $edicion == "Standard"){
            $where .= " AND os NOT LIKE '%Embedded%' ";
        }
        
        if($asignacion != ""){
            $where .= " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where .= " AND asignacion IN (" . $asignacion . ")";
        }
        
        if($edicion == "Professional"){
            $edicion = "Pro";
        }
        
        $array = array(':cliente'=>$cliente, ':os'=>$os, ':edicion'=>'%' . $edicion . "%");
        
        $query = "SELECT rango,
                COUNT(id) AS instalaciones
            FROM detalles_equipo2
            WHERE detalles_equipo2.cliente = :cliente " . $familia . " AND detalles_equipo2.edicion LIKE :edicion " . $where . "
            GROUP BY rango";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function totalWindowsAsignacionOtros($cliente, $os, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':os'=>$os);
        $familia = "";
        
        if($os == "Windows"){
            $familia = " AND detalles_equipo2.familia = :os AND detalles_equipo2.edicion NOT LIKE '%Pro%' ";
            $notIn = " AND detalles_equipo2.edicion NOT IN ('Standard') AND detalles_equipo2.edicion NOT LIKE '%Enterprise%' AND detalles_equipo2.edicion NOT LIKE '%Pro%' ";
            //$notIn = "'Standard', 'Enterprise'";
        } else{
            $familia = " AND detalles_equipo2.familia IN (:os, 'Windows Terminal Server') ";
            $notIn = " AND detalles_equipo2.edicion NOT IN ('Standard') AND detalles_equipo2.edicion NOT LIKE '%Datacenter%' AND detalles_equipo2.edicion NOT LIKE '%Enterprise%' ";
            //$notIn = "'Standard', 'Datacenter', 'Enterprise'";
        }
        $array = array(':cliente'=>$cliente, ':os'=>$os);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        /*$query = "SELECT rango,
                COUNT(id) AS instalaciones
            FROM detalles_equipo2
            WHERE detalles_equipo2.cliente = :cliente " . $familia . " AND detalles_equipo2.edicion NOT IN (" . $notIn . ") " . $where . "
            GROUP BY rango";*/
        
        $query = "SELECT rango,
                COUNT(id) AS instalaciones
            FROM detalles_equipo2
            WHERE detalles_equipo2.cliente = :cliente " . $familia . $notIn . $where . "
            GROUP BY rango";
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function erroneus($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version
                FROM detalles_equipo2
                    INNER JOIN resumen_office2 ON detalles_equipo2.cliente = resumen_office2.cliente
                    AND detalles_equipo2.equipo = resumen_office2.equipo AND
                    (resumen_office2.familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') 
                    OR (resumen_office2.familia IN ('Skype for Business') AND resumen_office2.edicion IN ('Server')))
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.tipo = 1
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                
                UNION 
                
                SELECT resumen_office2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version
                FROM detalles_equipo2
                    INNER JOIN resumen_office2 ON detalles_equipo2.cliente = resumen_office2.cliente
                    AND detalles_equipo2.equipo = resumen_office2.equipo AND
                    (resumen_office2.familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                    OR (resumen_office2.familia IN ('Skype for Business') AND resumen_office2.edicion NOT IN ('Server')))
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.tipo = 2
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function erroneusCantidad($cliente, $tipo){
        try{
            $this->conexion();
            
            $where = " AND (resumen_office2.familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') "
                    . "OR (resumen_office2.familia IN ('Skype for Business') AND resumen_office2.edicion NOT IN ('Server'))) ";
            $where1 = " AND detalles_equipo2.tipo = 2 ";
            
            if($tipo == "servidor"){
                $where = " AND resumen_office2.familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') "
                    . "OR (resumen_office2.familia IN ('Skype for Business') AND resumen_office2.edicion IN ('Server'))) ";
                $where1 = " AND detalles_equipo2.tipo = 1 ";
            }
            
            $sql = $this->conn->prepare("SELECT familia,
                    COUNT(familia) AS cantidad
                FROM (SELECT resumen_office2.equipo,
                        IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM detalles_equipo2
                        INNER JOIN resumen_office2 ON detalles_equipo2.cliente = resumen_office2.cliente
                        AND detalles_equipo2.equipo = resumen_office2.equipo " . $where . "
                    WHERE detalles_equipo2.cliente = :cliente " . $where1 . "
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) tabla1
                GROUP BY familia");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function erroneus($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version
                FROM detalles_equipo2
                    INNER JOIN resumen_office2 ON detalles_equipo2.cliente = resumen_office2.cliente
                    AND detalles_equipo2.equipo = resumen_office2.equipo AND (resumen_office2.familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') "
                    . "OR resumen_office2.edicion LIKE '%Server%') "
                    . "AND resumen_office2.edicion NOT LIKE '%Endpoint Protection%' "
                    . "AND resumen_office2.edicion NOT LIKE '%Express%' AND resumen_office2.edicion NOT LIKE '%MUI%' "
                    . "AND resumen_office2.edicion NOT LIKE '%Update%' AND resumen_office2.edicion NOT LIKE '%Configuration Manager Client%'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.tipo = 1
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                
                UNION 
                
                SELECT resumen_office2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version
                FROM detalles_equipo2
                    INNER JOIN resumen_office2 ON detalles_equipo2.cliente = resumen_office2.cliente
                    AND detalles_equipo2.equipo = resumen_office2.equipo AND (resumen_office2.familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') "
                . "AND NOT resumen_office2.edicion LIKE '%Server%') AND resumen_office2.edicion NOT LIKE '%Endpoint Protection%' "
                . "AND resumen_office2.edicion NOT LIKE '%Express%' AND resumen_office2.edicion NOT LIKE '%MUI%' "
                . "AND resumen_office2.edicion NOT LIKE '%Update%' AND resumen_office2.edicion NOT LIKE '%Configuration Manager Client%'
                WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.tipo = 2
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version");
            
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function erroneusCantidad($cliente, $tipo){
        try{
            $this->conexion();
            
            $where = " AND (resumen_office2.familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                    AND NOT resumen_office2.edicion LIKE '%Server%') AND (resumen_office2.familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') "
                . "AND NOT resumen_office2.edicion LIKE '%Server%') AND resumen_office2.edicion NOT LIKE '%Endpoint Protection%' "
                . "AND resumen_office2.edicion NOT LIKE '%Express%' AND resumen_office2.edicion NOT LIKE '%MUI%' "
                . "AND resumen_office2.edicion NOT LIKE '%Update%' AND resumen_office2.edicion NOT LIKE '%Configuration Manager Client%' ";
            $where1 = " AND detalles_equipo2.tipo = 2 ";
            
            if($tipo == "servidor"){
                $where = " AND
                    (resumen_office2.familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') 
                    OR resumen_office2.edicion LIKE '%Server%') AND (resumen_office2.familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') "
                    . "OR resumen_office2.edicion LIKE '%Server%') "
                    . "AND resumen_office2.edicion NOT LIKE '%Endpoint Protection%' "
                    . "AND resumen_office2.edicion NOT LIKE '%Express%' AND resumen_office2.edicion NOT LIKE '%MUI%' "
                    . "AND resumen_office2.edicion NOT LIKE '%Update%' AND resumen_office2.edicion NOT LIKE '%Configuration Manager Client%' ";
                $where1 = " AND detalles_equipo2.tipo = 1 ";
            }
            
            $sql = $this->conn->prepare("SELECT familia,
                    COUNT(familia) AS cantidad
                FROM (SELECT resumen_office2.equipo,
                        IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM detalles_equipo2
                        INNER JOIN resumen_office2 ON detalles_equipo2.cliente = resumen_office2.cliente
                        AND detalles_equipo2.equipo = resumen_office2.equipo " . $where . "
                    WHERE detalles_equipo2.cliente = :cliente " . $where1 . "
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) tabla1
                GROUP BY familia");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equipos($cliente){
        try{
            $this->conexion();
            
            $sql = $this->conn->prepare("SELECT equipo
                FROM detalles_equipo2
                WHERE cliente = :cliente
                ORDER BY equipo");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}