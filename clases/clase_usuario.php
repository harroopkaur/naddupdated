<?php
class Usuario extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar un Usuario a la Base de Datos
    function insertar($login, $contrasena, $nombre, $apellido, $email, $tipo) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usuarios(login, contrasena, nombre, apellido, email, tipo, estado, fecha_registro, usuario_registro, fecha_actualizacion, usuario_actualizacion) '
            . 'VALUES(:login, :contrasena, :nombre, :apellido, :email, :tipo, 1, CURDATE(), ' . $_SESSION['usuario_id'] . ', CURDATE(), ' . $_SESSION['usuario_id'] . ')');
            $sql->execute(array(':login'=>$login, ':contrasena'=>$contrasena, ':nombre'=>$nombre, ':apellido'=>$apellido, ':email'=>$email, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar un Usuario a la Base de Datos identificado por su id
    function actualizar($id, $login, $nombre, $apellido, $email) {       
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE usuarios SET '
            . 'login = :login, nombre = :nombre, apellido = :apellido, email = :email, fecha_actualizacion = CURDATE(), usuario_actualizacion = ' . $_SESSION['usuario_id'] . ' '
            . 'WHERE id = :id');       
            $sql->execute(array(':id'=>$id, ':login'=>$login, ':nombre'=>$nombre, ':apellido'=>$apellido, ':email'=>$email));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar un Usuario a la Base de Datos identificado por su id
    function actualizar_clave($id, $clave) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE usuarios SET '
            . 'contrasena = :clave '
            . 'WHERE id = :id');       
            $sql->execute(array(':id'=>$id, ':clave'=>$clave));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar el Tipo de un Usuario (Estatus) a la Base de Datos identificado por su id
    function actualizar_tipo($id, $tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE usuarios SET '
            . 'tipo = :tipo, fecha_actualizacion = CURDATE(), usuario_actualizacion = ' . $_SESSION['usuario_id'] . ' '
            . 'WHERE id = :id');       
            $sql->execute(array(':id'=>$id, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar un Usuario de la Base de Datos identificado por su id
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM usuarios WHERE id = :id');       
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Desactivar un Usuario de la Base de Datos identificado por su id
    function desactivar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE usuarios SET estado = 0 WHERE id = :id');       
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Autenticar Usuario
    function autenticar($login, $contrasena) {
        if (($login != "") && ($contrasena != "")) {            
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT id, nombre, apellido, tipo, estado FROM usuarios WHERE login = :login AND contrasena = :contrasena AND estado != 0');       
                $sql->execute(array(':login'=>$login, ':contrasena'=>$contrasena));
                $rusuario = $sql->fetch();
                if(count($rusuario['nombre']) > 0){
                    $_SESSION['usuario_autorizado'] = true;
                    $_SESSION['usuario_id']         = $rusuario['id'];
                    $_SESSION['usuario_nombre']     = $rusuario['nombre'];
                    $_SESSION['usuario_apellido']   = $rusuario['apellido'];
                    $_SESSION['usuario_tipo']       = 1;
                    $_SESSION['usuario_estado']     = $rusuario['estado'];
                    $_SESSION['usuario_fecha']      = date('d/m/Y H:i:s');
                    $_SESSION['usuario_tiempo']     = time();
                    return true;
                }
                else{
                    $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Usuario o Contrase&ntilde;a vac&iacute;os';
            return false;
        }
    }

    // Imprimir datos de Session
    function session() {
        $cadena = "";
        $cadena .= $_SESSION['usuario_nombre'] . " " . $_SESSION['usuario_apellido'] . " - ";
        switch ($_SESSION['usuario_tipo']) {
            case 1: $cadena .= "Administrador";
                break;
        }
        return $cadena;
    }

    // Obtener listado de todos los Usuarios
    function datos($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE id = :id AND estado != 0 ORDER BY tipo, nombre');       
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id                    = $usuario['id'];
            $this->login                 = $usuario['login'];
            $this->contrasena            = $usuario['contrasena'];
            $this->nombre                = $usuario['nombre'];
            $this->apellido              = $usuario['apellido'];
            $this->email                 = $usuario['email'];
            $this->tipo                  = $usuario['tipo'];
            $this->estado                = $usuario['estado'];
            $this->fecha_registro        = $usuario['fecha_registro'];
            $this->usuario_registro      = $usuario['usuario_registro'];
            $this->fecha_actualizacion   = $usuario['fecha_actualizacion'];
            $this->usuario_actualizacion = $usuario['usuario_actualizacion'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE estado != 0 ORDER BY tipo, nombre');       
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados
    function listar_todo_paginado($inicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE estado != 0 ORDER BY tipo, nombre LIMIT ' . $inicio . ', ' . $this->limit_paginacion);       
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Contar el total de Usuarios
    function total() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM usuarios WHERE estado != 0');       
            $sql->execute();
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios desactivados
    function listar_desactivados() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE estado = 0 ORDER BY tipo, nombre');       
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Obtener listado de todos los Usuarios desactivados paginado
    function listar_desactivados_paginado($inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE estado=0 ORDER BY tipo, nombre LIMIT ' . $inicio . ', ' . $fin);       
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Contar el total de Usuarios desactivados
    function total_desactivados() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM usuarios WHERE estado = 0');       
            $sql->execute();
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios de un Tipo
    function listar_tipo($tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE estado != 0 AND tipo = :tipo ORDER BY tipo, nombre');       
            $sql->execute(array(':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Obtener listado de todos los Usuarios de un Tipo Paginados
    function listar_tipo_paginado($tipo, $inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE estado != 0 AND tipo = :tipo ORDER BY tipo, nombre LIMIT ' . $inicio . ', ' . $fin);       
            $sql->execute(array(':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Contar el total de Usuarios de un Tipo
    function total_tipo($tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM usuarios WHERE estado != 0 AND tipo = :tipo');       
            $sql->execute(array(':tipo'=>$tipo));
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Verificar si login ya existe
    function login_existe($login, $id) {
        try{
            $this->conexion();
            $result = false;
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE login = :login AND id != :id AND estado != 0');       
            $sql->execute(array(':login'=>$login, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row["login"]) > 0){
                $result = true;
            }
            return $result;
        }catch(PDOException $e){
            return false;
        }
    }

    // Verificar si e-mail ya existe
    function email_existe($email, $id) {
        try{
            $this->conexion();
            $result = false;
            $sql = $this->conn->prepare('SELECT * FROM usuarios WHERE email = :email AND id != :id AND estado != 0');       
            $sql->execute(array(':email'=>$email, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row["email"]) > 0){
                $result = true;
            }
            return $result;
        }catch(PDOException $e){
            return false;
        }
    }

    // Recuperar Contraseña de un Usuario
    function recuperar_contrasena($email) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT contrasena, nombre, apellido, email, login FROM usuarios WHERE email = :email');       
            $sql->execute(array(':email'=>$email));
            $row = $sql->fetch();
            if(count($row['nombre']) > 0){
                $this->nombre   = $usuario['nombre'];
                $this->apellido = $usuario['apellido'];
                $this->login    = $usuario['login'];
                $this->email    = $usuario['email'];
                $this->clave    = $usuario['contrasena'];
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
}
?>