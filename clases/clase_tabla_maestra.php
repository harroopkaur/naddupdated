<?php
class tablaMaestra extends General{
    public  $listaSustituir = array();
    public  $idForaneo;
    public  $sustituir      = "";
    public  $listaProductos = array();
    public  $listaCampos    = array();
    public  $lista = array();
    
    function sustituirCamposDespliegue($idMaestra, $idForaneo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :idMaestra AND (idForaneo = :idForaneo OR idForaneo IS NULL)
                ORDER BY prioridad');
            $sql->execute(array(':idMaestra'=>$idMaestra, ':idForaneo'=>$idForaneo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sustituirCamposDespliegue1($idMaestra, $idForaneo, $os) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :idMaestra AND idForaneo = :idForaneo AND campo2 = :os
                ORDER BY prioridad');
            $sql->execute(array(':idMaestra'=>$idMaestra, ':idForaneo'=>$idForaneo, ':os'=>$os));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sustituirCamposDespliegue2($idMaestra, $idForaneo, $os, $tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :idMaestra AND idForaneo = :idForaneo AND campo2 = :os AND campo3 = :tipo
                ORDER BY prioridad');
            $sql->execute(array(':idMaestra'=>$idMaestra, ':idForaneo'=>$idForaneo, ':os'=>$os, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
   
    //inicio filtrar Despliegue
    function buscarVersion($software) {
        $version = "";
        try{
            //if (strpos($software, "Office 365") === FALSE){    
                $this->conexion();
                $sql = $this->conn->prepare("SELECT *
                        FROM versiones
                        WHERE id NOT IN (46) AND id NOT IN (SELECT id FROM versiones WHERE id > 140 AND id <= 1330) AND status = 1
                        ORDER BY nombre");
                $sql->execute();
                $tabla = $sql->fetchAll();
                foreach($tabla as $row){
                    if (strpos($software, $row["nombre"]) !== false && strlen($row["nombre"]) > strlen($version)) {
                        $version = $row["nombre"];
                    }
                }
            //}
            return $version;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $version;
        }
    }
    
    function filtrarProducto($producto, $version, $idForaneo, $idForaneo1){    
        $tabla = $this->sustituirCamposDespliegue(2, $idForaneo);

        if($version != ""){
            if(strpos($producto, $version) !== FALSE){
                $producto = str_replace($version, "", $producto);
            }
        }        
        
        foreach($tabla as $row){
            $resultado = strpos($producto, $row["descripcion"]);
            if($resultado !== FALSE){
                $producto = str_replace($row["descripcion"], "", $producto);
            }
        }
        
        if(($idForaneo == 1 || $idForaneo == 2 || $idForaneo == 3 || $idForaneo == 4 || $idForaneo == 5 || $idForaneo == 6) && (trim($producto) == "" || is_numeric(trim($producto)) || strpos($producto, "Standard") !== FALSE)){
            $producto = "Standard";
        }
        
        $tabla1 = $this->sustituirCamposDespliegue(3, $idForaneo1);
        foreach($tabla1 as $row){
            $resultado = strpos($producto, $row["descripcion"]);
            if($resultado !== FALSE){
                $producto = str_replace($row["descripcion"], $row["campo1"], $producto);
            }
        }
        
        $tabla = $this->sustituirCamposDespliegue(4, $idForaneo);
        foreach($tabla as $row){
            if(trim($producto) == trim($row["descripcion"])){
                $producto = $row["campo1"];
            }
        }
        
        return trim($producto);
    }
    //fin filtrar Despliegue
    
    //inicio sustituir general
    function buscarSustituirMaestra($software, $idForaneo, $opcion){ //5 edicion, 4 version
        try{
            $this->sustituir = "";
            $where = "";
            if($idForaneo == 10 && $opcion == 5 && strpos($software, "Reader DC") !== FALSE){
                $this->sustituir = "Reader DC";
                return true;
            }   
            else if($idForaneo == 10 && $opcion == 5 && strpos($software, "Reader DC") === FALSE){
                $where = ' AND "' . $software . '" NOT LIKE "%Reader DC%"';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :opcion AND idForaneo = :idForaneo AND "' . $software . '" LIKE CONCAT("%", descripcion ,"%")' . $where);
            $sql->execute(array(':opcion'=>$opcion, ':idForaneo'=>$idForaneo));
            $result = $sql->fetch();
        
        $this->sustituir = $result["campo1"];
        /*if($this->sustituir == ""){
            $this->sustituir = $software;
        }*/
            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    } 
    
    function buscarSustituirMaestraCampo2($software, $idForaneo, $campo2, $opcion){ //5 edicion, 4 version
        try{
            $this->sustituir = "";
            $where = "";
            if($idForaneo == 10 && $opcion == 5 && strpos($software, "Reader DC") !== FALSE){
                $this->sustituir = "Reader DC";
                return true;
            }   
            else if($idForaneo == 10 && $opcion == 5 && strpos($software, "Reader DC") === FALSE){
                $where = ' AND "' . $software . '" NOT LIKE "%Reader DC%"';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :opcion AND idForaneo = :idForaneo AND campo2 = :campo2 AND "' . $software . '" LIKE CONCAT("%", descripcion ,"%")' . $where);
            $sql->execute(array(':opcion'=>$opcion, ':idForaneo'=>$idForaneo, ':campo2'=>$campo2));
            $result = $sql->fetch();
            $this->sustituir = $result["campo1"];
            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productosSalida($fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = 1 AND idForaneo = :fabricante
                GROUP BY campo1');
            $sql->execute(array(':fabricante'=>$fabricante));
            $this->lista = $sql->fetchAll();            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function todosProductosSalida($fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = 1 AND idForaneo = :fabricante');
            $sql->execute(array(':fabricante'=>$fabricante));
            $this->lista = $sql->fetchAll();            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productosSustituir($fabricante, $software){
        try{
            $this->conexion();
            $this->sustituir = "";
            $sql = $this->conn->prepare('SELECT detalleMaestra.idForaneo,
                detalleMaestra.descripcion,
                sustituir.descripcion AS sustituir
            FROM detalleMaestra
                INNER JOIN detalleMaestra sustituir ON detalleMaestra.idForaneo = sustituir.idDetalle AND sustituir.idMaestra = 1 AND sustituir.idForaneo = :fabricante
            WHERE  detalleMaestra.idMaestra = 6 AND :software LIKE CONCAT("%", detalleMaestra.descripcion ,"%")');
            $sql->execute(array(':fabricante'=>$fabricante, 'software'=>$software));
            $result = $sql->fetch();
            $this->idForaneo = $result["idForaneo"];
            $this->sustituir = $result["sustituir"];    
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productosSustituirComponente($fabricante, $software){
        try{
            $this->conexion();
            $this->sustituir = "";
            $sql = $this->conn->prepare('SELECT detalleMaestra.idForaneo,
                detalleMaestra.descripcion,
                sustituir.descripcion AS sustituir,
                grupo.descripcion AS grupo
            FROM detalleMaestra
                INNER JOIN detalleMaestra sustituir ON detalleMaestra.idForaneo = sustituir.idDetalle AND sustituir.idMaestra = 1 AND sustituir.idForaneo = :fabricante
                INNER JOIN detalleMaestra grupo ON detalleMaestra.campo3 = grupo.idDetalle AND grupo.idMaestra = 10
            WHERE  detalleMaestra.idMaestra = 6 AND TRIM(:software) = detalleMaestra.campo2');
            $sql->execute(array(':fabricante'=>$fabricante, 'software'=>$software));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("idForaneo"=>"", "descripcion"=>"", "sustituir"=>"");
        }
    }
    
    function listadoProductosMaestra($fabricante){
        try{
            $this->conexion();
            $this->sustituir = "";
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = 6 AND campo1 = :fabricante');
            $sql->execute(array(':fabricante'=>$fabricante));
            $this->listaProductos = $sql->fetchAll(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoProductosMaestraActivo($fabricante){
        try{
            $this->conexion();
            $this->sustituir = "";
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = 6 AND campo1 = :fabricante AND status = 1');
            $sql->execute(array(':fabricante'=>$fabricante));
            $this->listaProductos = $sql->fetchAll(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listaComponentesSAP($producto){
        try{
            $this->conexion();
            $query = "SELECT grupo.descripcion AS nombre
                FROM detalleMaestra
                    INNER JOIN detalleMaestra AS componente ON componente.idMaestra = 6 AND componente.campo1 = 5
                    INNER JOIN detalleMaestra AS grupo ON componente.campo3 = grupo.idDetalle AND grupo.idMaestra = 10
                    AND detalleMaestra.idDetalle = componente.idForaneo
                WHERE detalleMaestra.idMaestra = 1 AND detalleMaestra.idForaneo = 5 AND detalleMaestra.campo1 = :producto
                GROUP BY nombre"; 
                    /*"SELECT componente.descripcion AS nombre
                FROM detalleMaestra
                    INNER JOIN detalleMaestra AS componente ON componente.idMaestra = 6 AND componente.campo1 = 5 
                    AND detalleMaestra.idDetalle = componente.idForaneo
                WHERE detalleMaestra.idMaestra = 1 AND detalleMaestra.idForaneo = 5 AND detalleMaestra.campo1 = :producto";*/
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':producto'=>$producto));
            return $sql->fetchAll(); 
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoEncontrarCampo($fabricante){
        try{
            $this->conexion();
            $this->sustituir = "";
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = 7 AND idForaneo = :fabricante');
            $sql->execute(array(':fabricante'=>$fabricante));
            $this->listaCampos = $sql->fetchAll(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoEncontrarCampo1($fabricante){
        try{
            $this->conexion();
            $this->sustituir = "";
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = 7 AND idForaneo = :fabricante');
            $sql->execute(array(':fabricante'=>$fabricante));
            $this->listaCampos = $sql->fetchAll(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    //fin sustituir general
    
    function listar_todo_paginado($descripcion, $maestra, $inicio){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :maestra AND descripcion LIKE :descripcion
                ORDER BY descripcion
                LIMIT ' . $inicio . ", " . $this->limit_paginacion);
            $sql->execute(array(':descripcion'=>"%" . $descripcion . "%", ':maestra'=>$maestra));
            return $sql->fetchAll(); 
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($nombre, $maestra){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM detalleMaestra
                WHERE idMaestra = :maestra AND descripcion LIKE :nombre
                ORDER BY descripcion');
            $sql->execute(array(':maestra'=>$maestra, ':nombre'=>"%" . $nombre . "%"));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerUltIdDetalle($maestra){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(idDetalle) AS idDetalle
                FROM detalleMaestra
                WHERE idMaestra = :maestra');
            $sql->execute(array(':maestra'=>$maestra));
            $row = $sql->fetch();
            return $row["idDetalle"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerUltPrioridad($maestra, $idForaneo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(prioridad) AS prioridad
                FROM detalleMaestra
                WHERE idMaestra = :maestra AND idForaneo = :idForaneo');
            $sql->execute(array(':maestra'=>$maestra, ':idForaneo'=>$idForaneo));
            $row = $sql->fetch();
            return $row["prioridad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existe_nombre($nombre, $maestra, $idDetalle){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleMaestra
                WHERE idMaestra = :maestra AND idDetalle != :detalle AND descripcion = :nombre');
            $sql->execute(array(':maestra'=>$maestra, ':detalle'=>$idDetalle, ':nombre'=>$nombre));
            return count($sql->fetchAll());
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function insertarDetalle($nombre, $maestra, $idDetalle){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleMaestra (idDetalle, idMaestra, descripcion) VALUES '
            . '(:detalle, :maestra, :nombre)');
            $sql->execute(array(':maestra'=>$maestra, ':detalle'=>$idDetalle, ':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function modificarDetalle($nombre, $maestra, $idDetalle){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET descripcion = :nombre WHERE idDetalle = :detalle '
            . 'AND idMaestra = :maestra');
            $sql->execute(array(':maestra'=>$maestra, ':detalle'=>$idDetalle, ':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarDetalle($maestra, $idDetalle){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalleMaestra WHERE idDetalle = :detalle '
            . 'AND idMaestra = :maestra');
            $sql->execute(array(':maestra'=>$maestra, ':detalle'=>$idDetalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function registroEspecifico($maestra, $idDetalle){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalleMaestra WHERE idMaestra = :maestra AND idDetalle = :detalle');
            $sql->execute(array(':maestra'=>$maestra, ':detalle'=>$idDetalle));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function insertarPalabraIncluir($detalle, $palabra){
        $row = $this->registroEspecifico(6, $detalle);
        $campo = "";
        if($row["campo3"] != ""){
            $campo = trim($row["campo3"]) . ", ";
        }
        $campo .= trim($palabra);
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET campo3 = :campo WHERE idMaestra = 6 AND idDetalle = :detalle');
            $sql->execute(array(':campo'=>$campo, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function existePalabraIncluir($detalle, $palabra){
        $result = false;
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo3"]);
        for($i = 0; $i < count($palabras); $i++){
            if(trim($palabras[$i]) == trim($palabra)){
                $result = true;
                break;
            }
        }
            
        return $result;
    }
    
    function modificarPalabraIncluir($detalle, $palabra, $palabraOri){
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo3"]);
        for($i = 0; $i < count($palabras); $i++){
            if(trim($palabras[$i]) == trim($palabraOri)){
                $palabras[$i] = $palabra;
                break;
            }
        }
        
        $campo = "";
        for($i = 0; $i < count($palabras); $i++){
            if($i == 0){
                $campo = trim($palabras[$i]);
            } else{
                $campo .= ", " . trim($palabras[$i]);
            }
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET campo3 = :campo WHERE idMaestra = 6 AND idDetalle = :detalle');
            $sql->execute(array(':campo'=>$campo, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function eliminarPalabraIncluir($detalle, $palabra){
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo3"]);
        $count = count($palabras);
        $z = 0;
        for($i = 0; $i < $count; $i++){
            if(trim($palabras[$i]) == trim($palabra)){
                unset($palabras[$i]);
                $z = $i;
                break;
            }
        }
        
        $campo = "";
        for($i = 0; $i < $count; $i++){
            if($i == $z){
                continue;
            }
            if($campo == ""){
                $campo = trim($palabras[$i]);
            } else{
                $campo .= ", " . trim($palabras[$i]);
            }
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET campo3 = :campo WHERE idMaestra = 6 AND idDetalle = :detalle');
            $sql->execute(array(':campo'=>$campo, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function insertarPalabraExcluir($detalle, $palabra){
        $row = $this->registroEspecifico(6, $detalle);
        $campo = "";
        if($row["campo2"] != ""){
            $campo = trim($row["campo2"]) . ", ";
        }
        $campo .= trim($palabra);
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET campo2 = :campo WHERE idMaestra = 6 AND idDetalle = :detalle');
            $sql->execute(array(':campo'=>$campo, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function existePalabraExcluir($detalle, $palabra){
        $result = false;
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo2"]);
        for($i = 0; $i < count($palabras); $i++){
            if(trim($palabras[$i]) == trim($palabra)){
                $result = true;
                break;
            }
        }
            
        return $result;
    }
    
    function modificarPalabraExcluir($detalle, $palabra, $palabraOri){
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo2"]);
        for($i = 0; $i < count($palabras); $i++){
            if(trim($palabras[$i]) == trim($palabraOri)){
                $palabras[$i] = $palabra;
                break;
            }
        }
        
        $campo = "";
        for($i = 0; $i < count($palabras); $i++){
            if($i == 0){
                $campo = trim($palabras[$i]);
            } else{
                $campo .= ", " . trim($palabras[$i]);
            }
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET campo2 = :campo WHERE idMaestra = 6 AND idDetalle = :detalle');
            $sql->execute(array(':campo'=>$campo, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function eliminarPalabraExcluir($detalle, $palabra){
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo2"]);
        $count = count($palabras);
        $z = 0;
        for($i = 0; $i < $count; $i++){
            if(trim($palabras[$i]) == trim($palabra)){
                unset($palabras[$i]);
                $z = $i;
                break;
            }
        }
        
        $campo = "";
        for($i = 0; $i < $count; $i++){
            if($i == $z){
                continue;
            }
            if($campo == ""){
                $campo = trim($palabras[$i]);
            } else{
                $campo .= ", " . trim($palabras[$i]);
            }
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET campo2 = :campo WHERE idMaestra = 6 AND idDetalle = :detalle');
            $sql->execute(array(':campo'=>$campo, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function listadoModificarIncluir($detalle){
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo3"]);
        return $palabras;
    }
    
    function listadoModificarExcluir($detalle){
        $row = $this->registroEspecifico(6, $detalle);
        $palabras = explode(",", $row["campo2"]);
        return $palabras;
    }
    
    function listadoConvertir($idForaneo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalleMaestra WHERE idMaestra = 3 AND idForaneo = :idForaneo');
            $sql->execute(array(':idForaneo'=>$idForaneo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function existePalabraConvertir($idMaestra, $idForaneo, $palabra, $idDetalle){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
            . 'FROM detalleMaestra '
            . 'WHERE idMaestra = :idMaestra AND idForaneo = :idForaneo AND descripcion = :palabra AND idDetalle != :idDetalle');
            $sql->execute(array(':idMaestra'=>$idMaestra, ':idForaneo'=>$idForaneo, ':palabra'=>$palabra, ':idDetalle'=>$idDetalle));
            $row = $sql->fetch();
            //echo $idMaestra.", ".$idDetalle.", ". $idForaneo.", ". $palabra;
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function insertarPalabraConvertir($idMaestra, $idForaneo, $palabra, $convertir){
        $newId = $this->obtenerUltIdDetalle($idMaestra) + 1;
        $prioridad = $this->obtenerUltPrioridad($idMaestra, $idForaneo) + 1;
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleMaestra (idDetalle, idMaestra, descripcion, campo1, idForaneo, prioridad) '
            . 'VALUES (:idDetalle, :idMaestra, :descripcion, :campo1, :idForaneo, :prioridad)');
            $sql->execute(array(':idDetalle'=>$newId, ':idMaestra'=>$idMaestra, ':descripcion'=>$palabra, ':campo1'=>$convertir, 
            ':idForaneo'=>$idForaneo, ':prioridad'=>$prioridad));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function eliminarConvertir($detalle, $idMaestra){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalleMaestra WHERE idMaestra = :idMaestra AND idDetalle = :detalle');
            $sql->execute(array(':idMaestra'=>$idMaestra, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function modificarPalabraConvertir($maestra, $detalle, $descripcion, $convertir){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET descripcion = :descripcion, campo1 = :convertir WHERE idMaestra = :maestra AND idDetalle = :detalle');
            $sql->execute(array(':maestra'=>$maestra, ':descripcion'=>$descripcion, ':convertir'=>$convertir, ':detalle'=>$detalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }
    
    function listadoConvertirEdicion($idForaneo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT detalleMaestra.idDetalle AS idForaneoEdicion, 
                    edicion.idDetalle,
                    detalleMaestra.descripcion AS familia,
                    edicion.descripcion AS edicion,
                    edicion.campo1 AS edicionConvertida
                FROM detalleMaestra
                    INNER JOIN detalleMaestra AS edicion ON detalleMaestra.idDetalle = edicion.idForaneo AND edicion.idMaestra = 5
                WHERE detalleMaestra.idMaestra = 1 AND detalleMaestra.idForaneo = :idForaneo');
            $sql->execute(array(':idForaneo'=>$idForaneo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function existeEdicionConvertir($idForaneo, $idDetalle, $edicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM detalleMaestra
                    INNER JOIN detalleMaestra AS edicion ON detalleMaestra.idDetalle = edicion.idForaneo AND edicion.idMaestra = 5
                WHERE detalleMaestra.idMaestra = 1 AND detalleMaestra.idForaneo = :idForaneo AND detalleMaestra.idDetalle = :idDetalle AND edicion.descripcion = :edicion');
            $sql->execute(array(':idForaneo'=>$idForaneo, ':idDetalle'=>$idDetalle, ':edicion'=>$edicion));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function listadoConvertirVersion($idForaneo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT detalleMaestra.idDetalle AS idForaneoEdicion, 
                    edicion.idDetalle,
                    detalleMaestra.descripcion AS familia,
                    edicion.descripcion AS edicion,
                    edicion.campo1 AS edicionConvertida
                FROM detalleMaestra
                    INNER JOIN detalleMaestra AS edicion ON detalleMaestra.idDetalle = edicion.idForaneo AND edicion.idMaestra = 4
                WHERE detalleMaestra.idMaestra = 1 AND detalleMaestra.idForaneo = :idForaneo');
            $sql->execute(array(':idForaneo'=>$idForaneo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function convertirVersionSQL($version){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT campo1 AS version
                FROM detalleMaestra
                WHERE idMaestra = 12 AND descripcion = :version');
            $sql->execute(array(':version'=>$version));
            $row = $sql->fetch();
            return $row["version"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("version"=>"");
        }
    }
    
    function listadoTablaMaestra($idMaestra){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idDetalle, descripcion
                FROM detalleMaestra
                WHERE idMaestra = :idMaestra
                ORDER BY descripcion');
            $sql->execute(array(':idMaestra'=>$idMaestra));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}