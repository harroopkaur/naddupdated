<?php
class cotizacionLicensing extends General{
    function insertar($nombre, $email, $telefono, $horario, $nivelServicio, $pcs, $servidores, $fabricante, $otro){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO cotizacionesLicensing (nombreSolicitante, email, telefono, horario, '
            . 'nivelServicio, pcs, servidores, fabricantes, otro, fecha) VALUES (:nombre, :email, :telefono, :horario, :nivelServicio, '
            . ':pcs, :servidores, :fabricantes, :otro, NOW())');
            $sql->execute(array(':nombre'=>$nombre, ':email'=>$email, ':telefono'=>$telefono, ':horario'=>$horario, 
            ':nivelServicio'=>$nivelServicio, ':pcs'=>$pcs, ':servidores'=>$servidores, ':fabricantes'=>$fabricante, 
            ':otro'=>$otro));
            return 1;
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function actualizRespon($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE cotizacionesLicensing SET estado = 2 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return 1;
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function listar() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, nombreSolicitante, email, telefono, nivelServicio, pcs,
                servidores, fabricantes, otro, DATE_FORMAT(fecha, "%d/%m/%Y %h:%i:%s %p") AS fecha, estado
            FROM cotizacionesLicensing
            WHERE estado IN (1,2)
            ORDER BY fecha DESC');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function cotizacionEspecifica($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT cotizacionesLicensing.id, nombreSolicitante, email, telefono, horario, 
                nivelServicio.descripcion, pcs,
                servidores, fabricantes, otro, DATE_FORMAT(fecha, "%d/%m/%Y %h:%i:%s %p") AS fecha, cotizacionesLicensing.estado
            FROM cotizacionesLicensing
                INNER JOIN nivelServicio ON cotizacionesLicensing.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
            WHERE cotizacionesLicensing.estado IN (1,2) AND cotizacionesLicensing.id = :id
            ORDER BY fecha DESC');
            $sql->execute(array(':id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return array("id"=>"");
        }
    }
}

?>