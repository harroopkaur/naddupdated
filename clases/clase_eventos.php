<?php
class clase_eventos extends General{ 
    function insertar($evento, $nombre, $email, $telefono, $direccion, $empresa) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleEvento (evento, nombre, email, telefono, direccion, empresa) 
            VALUES (:evento, :nombre, :email, :telefono, :direccion, :empresa)');
            $sql->execute(array(':evento'=>$evento, ':nombre'=>$nombre, ':email'=>$email, ':telefono'=>$telefono, 
            ':direccion'=>$direccion, ':empresa'=>$empresa));
            return true;
        }catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }
}