<?php
class centroCostos extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $usuario, $centroCosto) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO centroCosto(cliente, empleado, equipo, usuario, centroCosto) '
            . 'VALUES (:cliente, :empleado, :equipo, :usuario, :centroCosto)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':usuario'=>$usuario, ':centroCosto'=>$centroCosto));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente, $empleado, $equipo, $usuario) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM centroCosto WHERE cliente = :cliente AND empleado = :empleado AND equipo = :equipo AND usuario = :usuario');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':usuario'=>$usuario));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $centroCosto){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE centroCosto SET centroCosto = :centroCosto WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':centroCosto'=>$centroCosto));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    //inicio metodo de costos todos los usuarios y fabricantes
    function listarUsuarioEquipoTodos($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(centroCosto.id, 0) AS id,
                    tabla2.host_name,
                    tabla2.usuario,
                    centroCosto.centroCosto,
                    SUM(tabla2.costo) AS costo
                FROM (SELECT tabla1.id,
                        tabla1.host_name,
                        tabla1.usuario,
                        SUM(IFNULL(compras2.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                    FROM (SELECT resumen_office2.cliente,
                            resumen_office2.empleado,
                            resumen_office2.equipo,
                            resumen_office2.familia,
                            resumen_office2.edicion,
                            resumen_office2.version
                        FROM resumen_office2
                        WHERE resumen_office2.cliente = :cliente
                        GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                        ORDER BY resumen_office2.equipo) AS tabla
                        LEFT JOIN compras2 ON tabla.cliente = compras2.cliente
                        AND tabla.familia = compras2.familia AND tabla.edicion = compras2.edicion AND tabla.version = compras2.version
                        INNER JOIN versiones ON tabla.version = versiones.nombre
                        INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                        INNER JOIN productos ON tabla.familia = productos.nombre
                        INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                        INNER JOIN (SELECT usuarioEquipoMicrosoft.id,
                                        usuarioEquipoMicrosoft.cliente,
                                        usuarioEquipoMicrosoft.empleado,
                                        usuarioEquipoMicrosoft.host_name,
                                        usuarioEquipoMicrosoft.usuario,
                                        usuarioEquipoMicrosoft.centroCosto
                                    FROM usuarioEquipoMicrosoft
                                    WHERE usuarioEquipoMicrosoft.cliente = :cliente
                                    GROUP BY usuarioEquipoMicrosoft.host_name, usuarioEquipoMicrosoft.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente
                    GROUP BY tabla1.host_name, tabla1.usuario

                    UNION

                    SELECT tabla1.id,
                        tabla1.host_name,
                        tabla1.usuario,
                        SUM(IFNULL(compras_adobe.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                    FROM (SELECT resumen_adobe.cliente,
                            resumen_adobe.empleado,
                            resumen_adobe.equipo,
                            resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version
                        FROM resumen_adobe
                        WHERE resumen_adobe.cliente = :cliente
                        GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version
                        ORDER BY resumen_adobe.equipo) AS tabla
                        LEFT JOIN compras_adobe ON tabla.cliente = compras_adobe.cliente
                        AND tabla.familia = compras_adobe.familia AND tabla.edicion = compras_adobe.edicion AND tabla.version = compras_adobe.version
                        INNER JOIN versiones ON tabla.version = versiones.nombre
                        INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                        INNER JOIN productos ON tabla.familia = productos.nombre
                        INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                        INNER JOIN (SELECT usuarioEquipoAdobe.id,
                                        usuarioEquipoAdobe.cliente,
                                        usuarioEquipoAdobe.empleado,
                                        usuarioEquipoAdobe.host_name,
                                        usuarioEquipoAdobe.usuario,
                                        usuarioEquipoAdobe.centroCosto
                                    FROM usuarioEquipoAdobe
                                    WHERE usuarioEquipoAdobe.cliente = :cliente
                                    GROUP BY usuarioEquipoAdobe.host_name, usuarioEquipoAdobe.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente
                    GROUP BY tabla1.host_name, tabla1.usuario

                    UNION

                    SELECT tabla1.id,
                        tabla1.host_name,
                        tabla1.usuario,
                        SUM(IFNULL(compras_ibm.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                    FROM (SELECT resumen_ibm.cliente,
                            resumen_ibm.empleado,
                            resumen_ibm.equipo,
                            resumen_ibm.familia,
                            resumen_ibm.edicion,
                            resumen_ibm.version
                        FROM resumen_ibm
                        WHERE resumen_ibm.cliente = :cliente
                        GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version
                        ORDER BY resumen_ibm.equipo) AS tabla
                        LEFT JOIN compras_ibm ON tabla.cliente = compras_ibm.cliente
                        AND tabla.familia = compras_ibm.familia AND tabla.edicion = compras_ibm.edicion AND tabla.version = compras_ibm.version
                        INNER JOIN versiones ON tabla.version = versiones.nombre
                        INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                        INNER JOIN productos ON tabla.familia = productos.nombre
                        INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                        INNER JOIN (SELECT usuarioEquipoIbm.id,
                                        usuarioEquipoIbm.cliente,
                                        usuarioEquipoIbm.empleado,
                                        usuarioEquipoIbm.host_name,
                                        usuarioEquipoIbm.usuario,
                                        usuarioEquipoIbm.centroCosto
                                    FROM usuarioEquipoIbm
                                    WHERE usuarioEquipoIbm.cliente = :cliente
                                    GROUP BY usuarioEquipoIbm.host_name, usuarioEquipoIbm.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente
                    GROUP BY tabla1.host_name, tabla1.usuario) tabla2
                    LEFT JOIN centroCosto ON centroCosto.cliente = :cliente AND tabla2.host_name = centroCosto.equipo AND tabla2.usuario = centroCosto.usuario
                GROUP BY tabla2.host_name, tabla2.usuario
                ORDER BY tabla2.host_name');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    //fin metodo de costos todos los usuarios y fabricantes
    
    function UsuarioXEquipo($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT equipo AS host_name, usuario, centroCosto
                FROM centroCosto
                WHERE cliente = :cliente AND centroCosto != ""
                GROUP BY equipo, usuario
                ORDER BY centroCosto, equipo, usuario');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoCentroCostoAsignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla4.id,
                        tabla4.host_name,
                        tabla4.centroCosto,
                        SUM(tabla4.costo) AS costo
                 FROM (SELECT tabla3.id,
                        tabla3.host_name,
                        tabla3.centroCosto,
                        tabla3.costo
                 FROM (SELECT IFNULL(centroCosto.id, 0) AS id,
                                     tabla2.host_name,
                                     tabla2.usuario,
                                     centroCosto.centroCosto,
                                     SUM(tabla2.costo) AS costo
                 FROM (SELECT tabla1.id,
                    tabla1.host_name,
                    tabla1.usuario,
                    SUM(IFNULL(compras2.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                FROM (SELECT resumen_office2.cliente,
                        resumen_office2.empleado,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM resumen_office2
                    WHERE resumen_office2.cliente = :cliente
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY resumen_office2.equipo) AS tabla
                    LEFT JOIN compras2 ON tabla.cliente = compras2.cliente
                    AND tabla.familia = compras2.familia AND tabla.edicion = compras2.edicion AND tabla.version = compras2.version
                    INNER JOIN versiones ON tabla.version = versiones.nombre
                    INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                    INNER JOIN productos ON tabla.familia = productos.nombre
                    INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                    INNER JOIN (SELECT usuarioEquipoMicrosoft.id,
                                    usuarioEquipoMicrosoft.cliente,
                                    usuarioEquipoMicrosoft.empleado,
                                    usuarioEquipoMicrosoft.host_name,
                                    usuarioEquipoMicrosoft.usuario
                                FROM usuarioEquipoMicrosoft
                                WHERE usuarioEquipoMicrosoft.cliente = :cliente
                                GROUP BY usuarioEquipoMicrosoft.host_name, usuarioEquipoMicrosoft.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente
                GROUP BY tabla1.host_name, tabla1.usuario

                UNION

                SELECT tabla1.id,
                    tabla1.host_name,
                    tabla1.usuario,
                    SUM(IFNULL(compras_adobe.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                FROM (SELECT resumen_adobe.cliente,
                        resumen_adobe.empleado,
                        resumen_adobe.equipo,
                        resumen_adobe.familia,
                        resumen_adobe.edicion,
                        resumen_adobe.version
                    FROM resumen_adobe
                    WHERE resumen_adobe.cliente = :cliente
                    GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version
                    ORDER BY resumen_adobe.equipo) AS tabla
                    LEFT JOIN compras_adobe ON tabla.cliente = compras_adobe.cliente
                    AND tabla.familia = compras_adobe.familia AND tabla.edicion = compras_adobe.edicion AND tabla.version = compras_adobe.version
                    INNER JOIN versiones ON tabla.version = versiones.nombre
                    INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                    INNER JOIN productos ON tabla.familia = productos.nombre
                    INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                    INNER JOIN (SELECT usuarioEquipoAdobe.id,
                                    usuarioEquipoAdobe.cliente,
                                    usuarioEquipoAdobe.empleado,
                                    usuarioEquipoAdobe.host_name,
                                    usuarioEquipoAdobe.usuario
                                FROM usuarioEquipoAdobe
                                WHERE usuarioEquipoAdobe.cliente = :cliente
                                GROUP BY usuarioEquipoAdobe.host_name, usuarioEquipoAdobe.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente
                GROUP BY tabla1.host_name, tabla1.usuario

                UNION

                SELECT tabla1.id,
                    tabla1.host_name,
                    tabla1.usuario,
                    SUM(IFNULL(compras_ibm.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                FROM (SELECT resumen_ibm.cliente,
                        resumen_ibm.empleado,
                        resumen_ibm.equipo,
                        resumen_ibm.familia,
                        resumen_ibm.edicion,
                        resumen_ibm.version
                    FROM resumen_ibm
                    WHERE resumen_ibm.cliente = :cliente
                    GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version
                    ORDER BY resumen_ibm.equipo) AS tabla
                    LEFT JOIN compras_ibm ON tabla.cliente = compras_ibm.cliente
                    AND tabla.familia = compras_ibm.familia AND tabla.edicion = compras_ibm.edicion AND tabla.version = compras_ibm.version
                    INNER JOIN versiones ON tabla.version = versiones.nombre
                    INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                    INNER JOIN productos ON tabla.familia = productos.nombre
                    INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                    INNER JOIN (SELECT usuarioEquipoIbm.id,
                                    usuarioEquipoIbm.cliente,
                                    usuarioEquipoIbm.empleado,
                                    usuarioEquipoIbm.host_name,
                                    usuarioEquipoIbm.usuario
                                FROM usuarioEquipoIbm
                                WHERE usuarioEquipoIbm.cliente = :cliente
                                GROUP BY usuarioEquipoIbm.host_name, usuarioEquipoIbm.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente
                GROUP BY tabla1.host_name, tabla1.usuario) tabla2
                      LEFT JOIN centroCosto ON centroCosto.cliente = :cliente AND tabla2.host_name = centroCosto.equipo AND tabla2.usuario = centroCosto.usuario
                GROUP BY tabla2.host_name, tabla2.usuario
                ORDER BY tabla2.host_name) tabla3
                GROUP BY host_name) tabla4
                GROUP BY tabla4.centroCosto');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }    
}