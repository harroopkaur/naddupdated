<?php
class clase_procesar_escaneo_general extends General{
    private $client_id;
    private $client_empleado;
    private $archivoEscaneo;
    private $procesarEscaneo;
    private $jHostname;
    private $jStatus;
    private $jError;
    private $opcion;
    private $idDiagnostic;
    private $bloque;
    private $bloqueValores;
    private $insertarBloque;
    private $tabEscaneo;
    private $campoEscaneo;
    private $hostName;
    private $status;
    private $errors;
    
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO " . $this->tabEscaneo . " (" . $this->campoEscaneo . ", equipo, status, errors) ";
        $query .= "VALUES " . $bloque;
        
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente) {
        $this->conexion();
        $query = "DELETE FROM " . $this->tabEscaneo . " WHERE cliente = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function setTabEscaneo($tabEscaneo){
        $this->tabEscaneo = $tabEscaneo;
    } 
    
    function procesarEscaneo($client_id, $client_empleado, $archivoEscaneo, $procesarEscaneo, $jHostname, $jStatus, $jError, 
    $opcion, $idDiagnostic = 0){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->archivoEscaneo = $archivoEscaneo;
        $this->procesarEscaneo = $procesarEscaneo;
        $this->jHostname = $jHostname;
        $this->jStatus = $jStatus; 
        $this->jError = $jError; 
        $this->opcion = $opcion;
        $this->idDiagnostic = $idDiagnostic;
       
        $this->cabeceraTablas();
        
        if($this->obtenerSeparadorUniversal($this->archivoEscaneo, 2, 3) === true && $this->procesarEscaneo === true){
            if (($fichero = fopen($this->archivoEscaneo, "r")) !== false) {
                $this->cicloInsertar($fichero);

                $this->verificarRegistrosInsertar1();
                
                fclose($fichero);
            }
        }
    }
            
    function cabeceraTablas(){
        if($this->opcion == "Cloud"){
            $this->tabEscaneo = "escaneo_equiposMSCloud";
            $this->campoEscaneo = "idDiagnostic";
        }else if($this->opcion == "Diagnostic"){
            $this->tabEscaneo = "escaneo_equiposSAMDiagnostic";
            $this->campoEscaneo = "idDiagnostic";
        } else if($this->opcion == "microsoft"){
            $this->tabEscaneo = "escaneo_equipos2";
            $this->campoEscaneo = "cliente, empleado";
        }
    }
    
    function cicloInsertar($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {                 
            if($i > 2 && $datos[$this->jHostname] != "Hostname" &&  $datos[$this->jStatus] != "Status" && $datos[$this->jError] != "Error"){
                $this->crearBloque($j);

                $this->setValores($datos);                
                $this->crearBloqueValores($j);

                $j = $this->verificarRegistrosInsertar($j);
                
                $j++;
            }
            
            $i++; 
        }
    }
    
    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        }
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":equipo" . $j . ", :status" . $j . ", "
            . ":errors" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":equipo" . $j . ", :status" . $j . ", "
            . ":errors" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j){
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }
      
        $this->bloqueValores[":equipo" . $j] = $this->hostName;
        $this->bloqueValores[":status" . $j] = $this->status;
        $this->bloqueValores[":errors" . $j] = $this->errors;
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function insertarGeneral(){
        if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        } 
    }     
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
    
    function setValores($datos){
        $this->hostName = "";
        $this->status = "";
        $this->errors = "";

        if(isset($datos[$this->jHostname])){
            $this->hostName = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($datos[$this->jHostname]))), 250);
        }

        if(isset($datos[$this->jStatus])){
            $this->status = $this->truncarString($this->get_escape(utf8_encode($datos[$this->jStatus])), 250); 
        }

        if(isset($datos[$this->jError])){
            $this->errors = $this->truncarString($this->get_escape(utf8_encode($datos[$this->jError])), 250);
        }
    }
}
