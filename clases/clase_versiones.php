<?php
class Versiones extends General{

    function getVersionNombre($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM versiones
                WHERE nombre = :nombre');
            $sql->execute(array('nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>