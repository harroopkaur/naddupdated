<?php
class pagos_fabricantes extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $descripcion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO pagosFabricantes (cliente, descripcion, fechaReg) VALUES '
            . '(:cliente, :descripcion, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':descripcion'=>$descripcion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // actualizar 
    function actualizar($id, $descripcion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE pagosFabricantes SET descripcion = :descripcion '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':descripcion'=>$descripcion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE pagosFabricantes SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listado($cliente, $inicio){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM pagosFabricantes '
                . 'WHERE cliente = :cliente AND estado = 1 '
                . 'ORDER BY id '
                . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
                . 'FROM pagosFabricantes '
                . 'WHERE cliente = :cliente AND estado = 1 '
                . 'ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function ult_id(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id '
                . 'FROM pagosFabricantes');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function datosPago($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM pagosFabricantes '
                . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('id'=>'', 'cliente'=>'', 'descripcion'=>'', 'fechaReg'=>'', 'estado'=>'');
        }
    }
}