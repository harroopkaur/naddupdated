<?php
class ResumenUsuariosSAP extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $usuarios, $componente, $familia, $edicion, $version, $diasUltAcceso) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumenUsuariosModuloSap (cliente, empleado, usuarios, componente, familia, edicion, version, diasUltAcceso) '
            . 'VALUES (:cliente, :empleado, :usuarios, :componente, :familia, :edicion, :version, :diasUltAcceso)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':usuarios'=>$usuarios, ':componente'=>$componente, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':diasUltAcceso'=>$diasUltAcceso));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumenUsuariosModuloSap (cliente, empleado, usuarios, componente, familia, edicion, version, diasUltAcceso) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarDepartCargo($id, $departamento, $cargo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE resumenUsuariosModuloSap SET departamento = :departamento, cargo = :cargo '
            . 'WHERE id = :id');        
            $sql->execute(array(':id'=>$id, ':departamento'=>$departamento, ':cargo'=>$cargo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumenUsuariosModuloSap WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumenUsuariosModuloSap WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT usuarios,
                    componente,
                    familia,
                    edicion,
                    version,
                    diasUltAcceso
                FROM resumenUsuariosModuloSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenUsuariosModuloSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datosAgrupado($cliente, $empleado, $familia) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenUsuariosModuloSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}