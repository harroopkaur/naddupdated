<?php
class detalleCotizacionProv extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    
    function insertar($idCotizacion, $descripcion, $cantidad){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleCotProveedor (idCotizacion, descripcion, cantidad) VALUES '
            . '(:idCotizacion, :descripcion, :cantidad)');
            $sql->execute(array(':idCotizacion'=>$idCotizacion, ':descripcion'=>$descripcion, ':cantidad'=>$cantidad));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function detalleCotizacion($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM detalleCotProveedor
                WHERE idCotizacion = :id AND estatus = 1');
            $sql->execute(array(':id'=>$id));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}