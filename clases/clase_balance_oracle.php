<?php
class balanceOracle extends General{
    ########################################  Atributos  ########################################
    public  $lista = array();
    public  $listaNoIncluir = array();
    public  $error = NULL;
    var $id;
    var $cliente;
    var $officce;
    var $instalaciones;
    var $compra;
    var $balance;
    var $tipo;
    var $archivo;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $office, $version, $precio, $instalaciones, $compra, $balance, $balancec, $tipo, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_oracle(cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo, asignacion) VALUES '
            . '(:cliente, :empleado, :familia, :office, :version, :precio, :instalaciones, :compra, :balance, :balancec, :tipo, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':office'=>$office, ':version'=>$version, ':precio'=>$precio, ':instalaciones'=>$instalaciones, ':compra'=>$compra, ':balance'=>$balance, ':balancec'=>$balancec, ':tipo'=>$tipo, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM balance_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarBalanzaAgregar($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balanzaGeneral.familia,
                    balanzaGeneral.edicion,
                    balanzaGeneral.version,
                    SUM(balanzaGeneral.instalacion) AS instalacion,
                    SUM(balanzaGeneral.compra) AS compra,
                    balanzaGeneral.asignacion
                FROM (SELECT balanza.familia,
                        balanza.edicion,
                        balanza.version,
                        0 AS instalacion,
                        balanza.compra,
                        balanza.asignacion
                    FROM (SELECT productos.familia,
                            productos.edicion,
                            productos.version,
                            compras_oracle.compra,
                            compras_oracle.asignacion
                        FROM (SELECT productos.nombre AS familia,
                                ediciones.nombre AS edicion,
                                versiones.nombre AS version
                            FROM tabla_equivalencia
                                INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                                INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                                LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                                LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                            WHERE tabla_equivalencia.fabricante = 4
                            ORDER BY familia, edicion, version
                        ) AS productos
                        LEFT JOIN compras_oracle ON productos.familia = compras_oracle.familia AND productos.edicion = compras_oracle.edicion
                        AND productos.version = compras_oracle.version AND compras_oracle.cliente = :cliente AND compras_oracle.empleado = :empleado
                        GROUP BY productos.familia, productos.edicion, productos.version, compras_oracle.asignacion
                    ) balanza

                    UNION

                    SELECT resumen.familia,
                        resumen.edicion,
                        resumen.version,
                        COUNT(resumen.familia) AS instalaciones,
                        0 AS compra,
                        resumen.asignacion
                    FROM (SELECT resumen_oracle.familia,
                            resumen_oracle.edicion,
                            resumen_oracle.version,
                            detalles_equipo_oracle.asignacion
                        FROM resumen_oracle
                            INNER JOIN detalles_equipo_oracle ON resumen_oracle.cliente = detalles_equipo_oracle.cliente AND
                            resumen_oracle.empleado = detalles_equipo_oracle.empleado AND resumen_oracle.equipo = detalles_equipo_oracle.equipo
                       WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado
                       GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version
                    ) AS resumen
                    GROUP BY resumen.familia, resumen.edicion, resumen.version, resumen.asignacion
                ) balanzaGeneral
                GROUP BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion
                ORDER BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function obtenerPrecio($cliente, $empleado, $familia, $edicion, $version){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(compras_oracle.precio, IFNULL(productos.precio, 0)) AS precio
                FROM (SELECT productos.nombre AS familia,
                        ediciones.nombre AS edicion,
                        versiones.nombre AS version,
                        tabla_equivalencia.precio
                    FROM tabla_equivalencia
                        INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                        INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                        LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                        LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                    WHERE tabla_equivalencia.fabricante = 4
                    ORDER BY familia, edicion, version
                    ) AS productos
                    LEFT JOIN compras_oracle ON productos.familia = compras_oracle.familia AND productos.edicion = compras_oracle.edicion
                    AND productos.version = compras_oracle.version AND compras_oracle.cliente = :cliente AND compras_oracle.empleado = :empleado
                WHERE productos.familia = :familia AND productos.edicion = :edicion AND productos.version = :version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia,
            ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            $precio = 0;
            if($row["precio"] != ""){
                $precio = $row["precio"];
            }
            return $precio;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function balanzaAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        $where = "";
        if($edicion == ""){
            $where .= " AND (balance_oracle.office = '' OR balance_oracle.office IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND balance_oracle.office = :edicion ";
            $array[':edicion'] = $edicion;
        }
        
        if($asignacion != ""){
            $where .= " AND balance_oracle.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where .= " AND balance_oracle.asignacion IN (" . $asignacion . ") ";
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_oracle.id,
                    balance_oracle.familia,
                    balance_oracle.office,
                    balance_oracle.version,
                    balance_oracle.asignacion,
                    balance_oracle.instalaciones,
                    (IFNULL(balance_oracle.compra, 0) - balance_oracle.instalaciones) AS balance,
                    (IFNULL(balance_oracle.compra, 0) - balance_oracle.instalaciones) * balance_oracle.precio AS balancec,
                    balance_oracle.compra,
                    (IFNULL(balance_oracle.compra, 0) - balance_oracle.instalaciones) AS disponible,
                    balance_oracle.precio
                FROM balance_oracle
                WHERE balance_oracle.cliente = :cliente AND balance_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ' . $where . '
                GROUP BY balance_oracle.familia, balance_oracle.office, balance_oracle.version, balance_oracle.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function balanzaAsignacion1($cliente, $familia, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        
        if($asignacion != ""){
            $where = " AND balance_oracle.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND balance_oracle.asignacion IN (" . $asignacion . ") ";
        }
               
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_oracle.id,
                    balance_oracle.familia,
                    balance_oracle.office,
                    balance_oracle.version,
                    balance_oracle.asignacion,
                    balance_oracle.instalaciones,
                    (balance_oracle.compra - balance_oracle.instalaciones) AS balance,
                    (balance_oracle.compra - balance_oracle.instalaciones) * balance_oracle.precio AS balancec,
                    balance_oracle.compra,
                    (balance_oracle.compra - balance_oracle.instalaciones) AS disponible,
                    balance_oracle.precio
                FROM balance_oracle
                WHERE balance_oracle.cliente = :cliente AND balance_oracle.familia = :familia ' . $where . '
                GROUP BY balance_oracle.familia, balance_oracle.office, balance_oracle.version, balance_oracle.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function imprimirTablaExcelBalanza($tabla, $objPHPExcel, $i){
        foreach ($tabla as $reg_equipos) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $reg_equipos["familia"])
                ->setCellValue('B' . $i, $reg_equipos["office"])
                ->setCellValue('C' . $i, $reg_equipos["version"])
                ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                ->setCellValue('E' . $i, $reg_equipos["compra"])
                ->setCellValue('F' . $i, $reg_equipos["balance"])
                ->setCellValue('G' . $i, $reg_equipos["balancec"]);
            $i++;
        }
    }

    // Obtener listado de todos los Usuarios
    function productosNoIncluir($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia
                FROM balance_oracle
                WHERE cliente = :cliente AND empleado = :empleado AND 
                    familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 != "Otros" AND idMaestra = 1)
                GROUP BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaNoIncluir = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_ibm WHERE cliente = :cliente AND familia = :familia AND office = :edicion ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_oracle
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_oracle WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias2($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_oracle WHERE cliente = :cliente AND familia = :familia ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_oracle
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familiasGrafico($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_oracle WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND office = :edicion AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias1Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_oracleSam WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_oracleSam WHERE archivo = :archivo AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familiasSAM($cliente, $empleado, $familia, $edicion) {        
        try{
            $this->conexion();
        //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia AND office = :edicion ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_oracleSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias2SAM($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_oracleSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csv2/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
    
    function balanceEjecutivo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia,
                    SUM(compra) AS totalCompras,
                    SUM(instalaciones) AS totalInstalaciones,
                    SUM(compra) - SUM(instalaciones) AS neto,
                    precio,
                    (SUM(compra) - SUM(instalaciones)) * precio AS total
                FROM balance_oracle
                WHERE cliente = :cliente AND precio > 0
                GROUP BY familia
                ORDER BY familia');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function sobranteFaltante($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ABS(ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) < 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0)) AS faltante,
                    ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) > 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0) AS sobrante
                FROM balance_oracle
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("faltante"=>0, "sobrante"=>0);
        }
    }
}