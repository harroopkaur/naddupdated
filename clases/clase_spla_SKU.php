<?php
class clase_SPLA_SKU extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO SPLASKU (cliente, empleado, SKU, description, product, partNumber, price) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM SPLASKU WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productos($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT description FROM SPLASKU WHERE cliente = :cliente AND empleado = :empleado GROUP BY description ORDER BY description');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function familias($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT product FROM SPLASKU WHERE cliente = :cliente AND empleado = :empleado GROUP BY product ORDER BY product');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function SKU($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT SKU FROM SPLASKU WHERE cliente = :cliente AND empleado = :empleado GROUP BY SKU ORDER BY SKU');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function totalRegistrosSKU($cliente,$empleado, $descripcion, $producto){
        try{
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            if($descripcion != ""){
                $array[":description"] = $descripcion;
                $where = " AND description = :description ";
            }
            
            if($producto != ""){
                $array[":product"] = $producto;
                $where = " AND product = :product ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(id) AS cantidad '
                . 'FROM SPLASKU '
                . 'WHERE cliente = :cliente AND empleado = :empleado ' . $where);
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listar_todoFiltrado($cliente,$empleado, $descripcion, $producto, $pagina, $limite){
        try{          
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaSKU($cliente, $empleado, $descripcion, $producto, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            if($descripcion != ""){
                $array[":description"] = $descripcion;
                $where = " AND description = :description ";
            }
            
            if($producto != ""){
                $array[":product"] = $producto;
                $where = " AND product = :product ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM SPLASKU '
                . 'WHERE cliente = :cliente AND empleado = :empleado ' . $where 
                . 'LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function ultimaPaginaSKU($cliente, $empleado, $descripcion, $producto, $limite) {
        return ceil($this->totalRegistrosSKU($cliente,$empleado, $descripcion, $producto) / $limite);
    }
}