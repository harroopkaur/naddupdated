<?php
class DetallesOracleSolaris extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $os, $memoria, $cpu, $core) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_OracleSolaris (cliente, empleado, equipo, os, memoria, cpu, core) '
            . 'VALUES (:cliente, :empleado, :equipo, :os, :memoria, :cpu, :core)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':os'=>$os, ':memoria'=>$memoria, ':cpu'=>$cpu, ':core'=>$core));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_OracleSolaris (cliente, empleado, equipo, os, memoria, cpu, core) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalles_equipo_OracleSolaris WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }

    /*function fechaSAM($cliente) {
        $this->conexion();
        $fecha = "";
        $query = "SELECT fecha FROM encGrafOracleSolarisSam WHERE cliente = :cliente";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }*/
}