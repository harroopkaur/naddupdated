<?php
class cotizacionProveedor extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
  
    function insertar($cliente, $empleado, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO cotizacionProveedor (cliente, empleado, fabricante, fecha) VALUES '
            . '(:cliente, :empleado, :fabricante, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function obtenerNroCotizacion(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(MAX(id), 0) nro '
                . 'FROM cotizacionProveedor');
            $sql->execute();
            $row = $sql->fetch();
            return $row["nro"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listadoCotizaciones($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT cotizacionProveedor.id,
                    respCotizProv.id AS idCotProv,
                    DATE_FORMAT(cotizacionProveedor.fecha, "%d/%m/%Y") AS fechaCot,
                    fabricantes.nombre AS fabricante,
                    respCotizProv.nroCotizacion,
                    respCotizProv.proveedor,
                    IF(cotizacionProveedor.estatus = 1, "Pendiente", "Respondido") AS estatus,
                    respCotizProv.archivo
                FROM cotizacionProveedor
                    LEFT JOIN respCotizProv ON cotizacionProveedor.id = respCotizProv.idCotizacionProv AND respCotizProv.estatus = 1
                    INNER JOIN fabricantes ON cotizacionProveedor.fabricante = fabricantes.idFabricante
                WHERE cotizacionProveedor.estatus NOT IN (0) AND cotizacionProveedor.cliente = :cliente AND cotizacionProveedor.empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoCotizacionesId($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT cotizacionProveedor.id,
                    respCotizProv.id AS idCotProv,
                    DATE_FORMAT(cotizacionProveedor.fecha, "%d/%m/%Y") AS fechaCot,
                    fabricantes.nombre AS fabricante,
                    respCotizProv.nroCotizacion,
                    respCotizProv.proveedor,
                    cotizacionProveedor.estatus,
                    respCotizProv.archivo,
                    CONCAT(IFNULL(empleados.nombre, ""), " ", IFNULL(empleados.apellido, "")) AS nombre,
                    clientes.empresa
                FROM cotizacionProveedor
                    LEFT JOIN respCotizProv ON cotizacionProveedor.id = respCotizProv.idCotizacionProv AND respCotizProv.estatus = 1
                    INNER JOIN fabricantes ON cotizacionProveedor.fabricante = fabricantes.idFabricante
                    INNER JOIN empleados ON cotizacionProveedor.empleado = empleados.id
                    INNER JOIN clientes ON empleados.cliente = clientes.id
                WHERE cotizacionProveedor.estatus NOT IN (0) AND cotizacionProveedor.id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function CotizacionesEspecificaId($id, $idCotProv){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT cotizacionProveedor.id,
                    respCotizProv.id AS idCotProv,
                    DATE_FORMAT(cotizacionProveedor.fecha, "%d/%m/%Y") AS fechaCot,
                    fabricantes.nombre AS fabricante,
                    respCotizProv.nroCotizacion,
                    respCotizProv.proveedor,
                    cotizacionProveedor.estatus,
                    respCotizProv.archivo
                FROM cotizacionProveedor
                    LEFT JOIN respCotizProv ON cotizacionProveedor.id = respCotizProv.idCotizacionProv AND respCotizProv.estatus = 1 AND respCotizProv.id = :idCotProv
                    INNER JOIN fabricantes ON cotizacionProveedor.fabricante = fabricantes.idFabricante
                WHERE cotizacionProveedor.estatus NOT IN (0) AND cotizacionProveedor.id = :id');
            $sql->execute(array(':id'=>$id, ':idCotProv'=>$idCotProv));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoCotizacionesPendientes(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT CONCAT(clientes.nombre, " ", clientes.apellido) AS cliente,
                    clientes.empresa,
                    cotizacionProveedor.id,
                    DATE_FORMAT(cotizacionProveedor.fecha, "%d/%m/%Y") AS fechaCot,
                    fabricantes.nombre AS fabricante,
                    "recibido" AS estatus,
                    "" AS archivo
                FROM cotizacionProveedor
                    INNER JOIN fabricantes ON cotizacionProveedor.fabricante = fabricantes.idFabricante
                    INNER JOIN clientes ON cotizacionProveedor.cliente = clientes.id
                WHERE cotizacionProveedor.estatus NOT IN (0)');
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoCompletoCotizaciones(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT CONCAT(clientes.nombre, " ", clientes.apellido) AS cliente,
                    clientes.empresa,
                    cotizacionProveedor.id,
                    respCotizProv.id AS idCotProv,
                    DATE_FORMAT(cotizacionProveedor.fecha, "%d/%m/%Y") AS fechaCot,
                    fabricantes.nombre AS fabricante,
                    respCotizProv.nroCotizacion,
                    respCotizProv.proveedor,
                    IF(cotizacionProveedor.estatus = 2, "Enviado", "Por Responder") AS estatus,
                    respCotizProv.archivo
                FROM cotizacionProveedor
                    LEFT JOIN respCotizProv ON cotizacionProveedor.id = respCotizProv.idCotizacionProv AND respCotizProv.estatus = 1
                    INNER JOIN fabricantes ON cotizacionProveedor.fabricante = fabricantes.idFabricante
                    INNER JOIN clientes ON cotizacionProveedor.cliente = clientes.id
                WHERE cotizacionProveedor.estatus NOT IN (0)
                ORDER BY cotizacionProveedor.fecha DESC');
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function eliminar($id, $idRespCotizacion){
        try{
            $this->conexion();
            if($idRespCotizacion == ""){
                $sql = $this->conn->prepare('UPDATE cotizacionProveedor SET estatus = 0 WHERE id = :id');
                $sql->execute(array(':id'=>$id));
                
                $sql = $this->conn->prepare('UPDATE respCotizProv SET estatus = 0 WHERE idCotizacionProv = :id');
                $sql->execute(array(':id'=>$id));
            }
            else{
                $sql = $this->conn->prepare('UPDATE respCotizProv SET estatus = 0 WHERE id = :id');
                $sql->execute(array(':id'=>$idRespCotizacion));
                
                $sql = $this->conn->prepare('UPDATE cotizacionProveedor SET estatus = 1 WHERE id = :id');
                $sql->execute(array(':id'=>$id));
            }
         
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}