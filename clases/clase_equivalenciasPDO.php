<?php
class EquivalenciasPDO extends General{
        
    /*function listar_todo_paginado($fab, $fam, $edi, $ver, $inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.id,
                tabla_equivalencia.fabricante AS idFabricante,
                tabla_equivalencia.familia AS idFamilia,
                tabla_equivalencia.edicion AS idEdicion,
                tabla_equivalencia.version AS idVersion,
                fabricantes.nombre AS fabricante,
                productos.nombre AS familia,
                ediciones.nombre AS edicion,
                versiones.nombre AS version,
                tabla_equivalencia.precio
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id 
            WHERE fabricantes.nombre LIKE :fab AND productos.nombre LIKE :fam AND ediciones.nombre LIKE :edi AND versiones.nombre LIKE :ver  
            ORDER BY id 
            LIMIT ' . $inicio . ', ' . $fin);
            $sql->execute(array('fab'=>"%" . $fab . "%", 'fam'=>"%" . $fam . "%", 'edi'=>"%" . $edi . "%", 'ver'=>"%" . $ver . "%"));
            //$sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }*/
    
    /*function total($fab, $fam, $edi, $ver) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.`fabricante` = fabricantes.idFabricante
                INNER JOIN productos ON `tabla_equivalencia`.familia = productos.`idProducto`
                LEFT JOIN ediciones ON `tabla_equivalencia`.`edicion` = ediciones.`idEdicion`
                LEFT JOIN versiones ON `tabla_equivalencia`.`version` = versiones.id 
            WHERE fabricantes.nombre LIKE :fab AND productos.nombre LIKE :fam AND ediciones.nombre LIKE :edi AND versiones.nombre LIKE :ver');
            $sql->execute(array('fab'=>"%".$fab."%", 'fam'=>"%".$fam."%", 'edi'=>"%".$edi."%", 'ver'=>"%".$ver."%"));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return false;
        }
    }*/
    
    function ProductoxNombre($fabricante, $notIn = array()){
        try{
            $this->conexion();
            $where = "";
            if(count($notIn) > 0){
                /*for($i = 0; $i < count($notIn); $i++){
                    $where .= " AND productos.nombre NOT IN ('" . $notIn[$i] . "')";
                }*/
                foreach($notIn as $row){
                    $where .= " AND productos.nombre NOT IN ('" . $row["familia"] . "')";
                }
            }
            $sql = $this->conn->prepare('SELECT productos.`idProducto`,
                productos.`nombre`
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.`fabricante` = fabricantes.idFabricante
                INNER JOIN productos ON `tabla_equivalencia`.familia = productos.`idProducto` AND productos.status = 1
            WHERE tabla_equivalencia.fabricante = :fabricante ' . $where . '
            GROUP BY productos.`idProducto`
            ORDER BY productos.nombre');
            $sql->execute(array('fabricante'=>$fabricante));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function edicionesProducto($fabricante, $producto){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
                ediciones.nombre
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.`fabricante` = fabricantes.idFabricante
                INNER JOIN productos ON `tabla_equivalencia`.familia = productos.`idProducto`
                INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
            WHERE tabla_equivalencia.fabricante = :fabricante AND tabla_equivalencia.familia = :familia
            GROUP BY ediciones.idEdicion
            ORDER BY ediciones.nombre');
            $sql->execute(array('fabricante'=>$fabricante, 'familia'=>$producto));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function edicionesProductoxNombre($fabricante, $producto){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
                ediciones.nombre
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.`fabricante` = fabricantes.idFabricante
                INNER JOIN productos ON `tabla_equivalencia`.familia = productos.`idProducto` AND productos.status = 1
                INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion AND ediciones.status = 1
            WHERE tabla_equivalencia.fabricante = :fabricante AND productos.nombre IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1 AND idForaneo = :fabricante1)
            GROUP BY ediciones.idEdicion
            ORDER BY ediciones.nombre');
            $sql->execute(array(':fabricante'=>$fabricante, ':familia'=>$producto, ':fabricante1'=>$fabricante));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }  
    
    function versionesEdicion($fabricante, $producto, $edicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT versiones.id,
                versiones.nombre
            FROM tabla_equivalencia
                INNER JOIN fabricantes ON tabla_equivalencia.`fabricante` = fabricantes.idFabricante
                INNER JOIN productos ON `tabla_equivalencia`.familia = productos.`idProducto`
                INNER JOIN ediciones ON `tabla_equivalencia`.`edicion` = ediciones.`idEdicion`
                INNER JOIN versiones ON tabla_equivalencia.version = versiones.id
            WHERE tabla_equivalencia.fabricante = :fabricante AND tabla_equivalencia.familia = :familia AND tabla_equivalencia.edicion = :edicion
            GROUP BY versiones.id
            ORDER BY versiones.nombre');
            $sql->execute(array('fabricante'=>$fabricante, 'familia'=>$producto, 'edicion'=>$edicion));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
}