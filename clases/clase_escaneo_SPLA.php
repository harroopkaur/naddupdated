<?php
class Scaneo_SPLA extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $equipo;
    var $status;
    var $errors;
    var $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $status, $errors) {
        $this->conexion();
        $query = "INSERT INTO escaneo_equipos_SPLA (cliente, empleado, equipo, status, errors) ";
        $query .= "VALUES (:cliente, :empleado, :equipo, :status, :errors)";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':status'=>$status, ':errors'=>$errors));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO escaneo_equipos_SPLA (cliente, empleado, equipo, status, errors) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        $this->conexion();
        $query = "DELETE FROM escaneo_equipos_SPLA WHERE cliente = :cliente AND empleado = :empleado";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    /*function datos($id) {
        $this->conexion();
        $query = "SELECT * FROM escaneo_equipos_SPLA WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->equipo = $usuario['equipo'];
            $this->status = $usuario['status'];
            $this->errors = $usuario['errors'];
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM escaneo_equipos_SPLA WHERE cliente = :cliente AND empleado = :empleado ORDER BY id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo2($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT id,
                    cliente,
                    empleado,
                    equipo,
                    status,
                    errors
                FROM escaneo_equipos_SPLA
                WHERE cliente = :cliente AND empleado = :empleado AND errors = 'Ninguno'
                GROUP BY equipo

                UNION

                SELECT id,
                    cliente,
                    empleado,
                    equipo,
                    status,
                    errors
                FROM escaneo_equipos_SPLA
                WHERE cliente = :cliente AND empleado = :empleado AND errors != '' AND NOT errors IS NULL
                AND equipo NOT IN (SELECT equipo FROM escaneo_equipos_SPLA WHERE cliente = :cliente AND empleado = :empleado AND errors = 'Ninguno' GROUP BY equipo)
                GROUP BY equipo";
            //$query = "SELECT * FROM escaneo_equipos_SPLA WHERE cliente = :cliente AND empleado = :empleado GROUP BY equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados
    /*function listar_todo_paginado($cliente, $inicio, $fin) {
        $this->conexion();
        $query = "SELECT * FROM escaneo_equipos_SPLA WHERE cliente = :cliente ORDER BY  id LIMIT " . $inicio . ", " . $fin;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf3/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}