<?php
class clase_empresa_licenciamiento extends clase_general_licenciamiento{ 
    public $error = "";
    
    function insertar($nombEmpresa, $usuario, $email, $cargo, $pais) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('INSERT INTO admin001 (nombreEmpresa, usuario, email, cargo, fechaCreacion, '
            . 'pais) '
            . 'VALUES (:nombEmpresa, :usuario, :email, :cargo, NOW(), :pais)');
            $sql->execute(array(':nombEmpresa'=>$nombEmpresa, ':usuario'=>$usuario, 
            ':email'=>$email, ':cargo'=>$cargo, ':pais'=>$pais));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $nombEmpresa, $usuario, $email, $cargo, $pais, $status) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('UPDATE admin001 SET '
            . 'nombreEmpresa = :nombEmpresa, usuario = :usuario, email = :email, cargo = :cargo, pais = :pais, '
            . 'status = :status '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':nombEmpresa'=>$nombEmpresa, ':usuario'=>$usuario, 
            ':email'=>$email, ':cargo'=>$cargo, ':pais'=>$pais, ':status'=>$status));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('UPDATE admin001 SET '
            . 'status = 0 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function datos($id){
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT nombreEmpresa, '
                    . 'usuario, '
                    . 'email, '
                    . 'cargo, '
                    . 'fechaCreacion, '
                    . 'status, '
                    . 'pais,'
                    . 'status '
                . 'FROM admin001 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $row = $sql->fetch();
            return $row;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('nombreEmpresa'=>"", 'usuario'=>"", 'email'=>"", 'cargo'=>"", 'fechaCreacion'=>"0000-00-00", 
            'status'=>0, 'pais'=>0);
        }
    }
    
    function ultimo_id(){
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT MAX(id) AS id FROM admin001');
            $sql->execute();
            $row = $sql->fetch();
            return $row['id'];
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT *
            FROM admin001
            ORDER BY nombreEmpresa ASC');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todo_paginado($pagina) {
        try{
            $inicio = $pagina - 1; 
            
            $this->conexion();
            $sql = $this->conex->prepare('SELECT *
            FROM admin001
            ORDER BY nombreEmpresa ASC 
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total() {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT COUNT(*) AS cantidad FROM admin001');
            $sql->execute();
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function listar_todo_paginado($empresa, $email, $fecha, $estado, $inicio) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $fecha;
                $where = " AND DATE(fechaCreacion) = :fecha ";
            }
            
            if ($estado >= 0){
                $array[":status"] = $estado;
                $where = " AND status = :status ";
            }
            
            $this->conexion();
            $sql = $this->conex->prepare('SELECT *
            FROM admin001
            WHERE nombreEmpresa LIKE :empresa AND email LIKE :email ' . $where . '
            ORDER BY nombreEmpresa ASC 
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute($array);
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($empresa, $email, $fecha, $estado) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $fecha;
                $where = " AND DATE(fechaCreacion) = :fecha ";
            }
           
            if ($estado >= 0){
                $array[":status"] = $estado;
                $where = " AND status = :status ";
            }
            
            $this->conexion();
            $sql = $this->conex->prepare('SELECT COUNT(*) AS cantidad '
                . 'FROM admin001 '
                . 'WHERE nombreEmpresa LIKE :empresa AND email LIKE :email ' . $where);
            $sql->execute($array);
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function email_existe($email, $id) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT * FROM admin001 WHERE email = :email AND id != :id');
            $sql->execute(array(':email'=>$email, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['email']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    function empresa_existe($nombre, $id) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT * FROM admin001 WHERE nombreEmpresa = :nombre AND id != :id');
            $sql->execute(array(':nombre'=>$nombre, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['nombreEmpresa']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
}