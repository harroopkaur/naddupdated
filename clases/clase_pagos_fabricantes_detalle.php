<?php
class pagos_fabricantes_detalle extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($idPago, $idFabricante, $aniversario, $renovacion, $monto) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO pagosFabricantesDetalle (idPago, idFabricante, aniversario, renovacion, monto) VALUES '
            . '(:idPago, :idFabricante, :aniversario, :renovacion, :monto)');
            $sql->execute(array(':idPago'=>$idPago, ':idFabricante'=>$idFabricante, ':aniversario'=>$aniversario, ':renovacion'=>$renovacion, ':monto'=>$monto));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // actualizar 
    function actualizar($id, $aniversario, $renovacion, $monto) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE pagosFabricantesDetalle SET aniversario = :aniversario, renovacion = :renovacion, monto = :monto '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':aniversario'=>$aniversario, ':renovacion'=>$renovacion, ':monto'=>$monto));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE pagosFabricantesDetalle SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function pagoDetalle($idPago) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT pagosFabricantesDetalle.id, '
                    . 'pagosFabricantesDetalle.idFabricante, '
                    . 'pagosFabricantesDetalle.aniversario, '
                    . 'pagosFabricantesDetalle.renovacion, '
                    . 'pagosFabricantesDetalle.monto, '
                    . 'fabricantes.nombre '
                . 'FROM pagosFabricantesDetalle '
                    . 'INNER JOIN fabricantes ON pagosFabricantesDetalle.idFabricante = fabricantes.idFabricante '
                . 'WHERE pagosFabricantesDetalle.idPago = :idPago AND pagosFabricantesDetalle.estado = 1');
            $sql->execute(array(':idPago'=>$idPago));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}