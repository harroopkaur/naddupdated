<?php
class resumenVMWare extends General{
    public  $lista = array();
    public  $error = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $licenseKey, $producto, $edicion, $version, $uso, $metrica, $capacidad, $etiqueta, $asignado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_VMWare (cliente, empleado, licenseKey, familia, edicion, version, uso, metrica, capacidad, etiqueta, asignado) VALUES '
            . '(:cliente, :empleado, :licenseKey, :producto, :edicion, :version, :uso, :metrica, :capacidad, :etiqueta, :asignado)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':licenseKey'=>$licenseKey, ':producto'=>$producto, ':edicion'=>$edicion, ':version'=>$version, ':uso'=>$uso, ':metrica'=>$metrica, ':capacidad'=>$capacidad, ':etiqueta'=>$etiqueta, ':asignado'=>$asignado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumen_VMWare WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumen_VMWare WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos5($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
        from resumen_VMWare
        WHERE resumen_VMWare.cliente = :cliente AND empleado = :empleado AND resumen_VMWare.familia = :familia
        GROUP BY resumen_VMWare.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_datos5Sam($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
        FROM resumen_VMWareSam
        WHERE resumen_VMWareSam.cliente = :cliente AND resumen_VMWareSam.familia = :familia
        GROUP BY resumen_VMWareSam.id
        ORDER BY resumen_VMWareSam.familia, resumen_VMWareSam.edicion, resumen_VMWareSam.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5SamArchivo($archivo, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
        FROM resumen_VMWareSam
        WHERE resumen_VMWareSam.archivo = :archivo AND resumen_VMWareSam.familia = :familia
        GROUP BY resumen_VMWareSam.id
        ORDER BY resumen_VMWareSam.familia, resumen_VMWareSam.edicion, resumen_VMWareSam.version");
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
                FROM resumen_VMWare
                WHERE resumen_VMWare.cliente = :cliente AND empleado = :empleado AND resumen_VMWare.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_VMWare.edicion = :edicion
                GROUP BY resumen_VMWare.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos5Agrupado($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT resumen_VMWare.familia,
                            resumen_VMWare.edicion,
                            resumen_VMWare.version
                        FROM resumen_VMWare
                        WHERE resumen_VMWare.cliente = :cliente AND empleado = :empleado AND resumen_VMWare.familia = :familia
                        GROUP BY resumen_VMWare.id) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_VMWare.familia,
                        resumen_VMWare.edicion,
                        resumen_VMWare.version
                    FROM resumen_VMWare
                    WHERE resumen_VMWare.cliente = :cliente AND empleado = :empleado AND resumen_VMWare.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_VMWare.edicion = :edicion
                    GROUP BY resumen_VMWare.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datosAgrupado($cliente, $empleado, $familia) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_VMWare.familia,
                        resumen_VMWare.edicion,
                        resumen_VMWare.version
                    FROM resumen_VMWare
                    WHERE resumen_VMWare.cliente = :cliente AND resumen_VMWare.empleado = :empleado AND resumen_VMWare.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY resumen_VMWare.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function graficoBalanza($cliente, $familia){
        try{      
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion($cliente, $familia, $edicion){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.edicion = :edicion
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion1($cliente, $familia){
        try{  
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia = :familia
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
}