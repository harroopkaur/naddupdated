<?php
class comprasOracle extends General{
    ########################################  Atributos  ########################################
    public  $listado = array();
    public  $row = array();
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $edicion, $version, $compra, $precio, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO compras_oracle (cliente, empleado, familia, edicion, version, compra, precio, asignacion) '
            . 'VALUES (:cliente, :empleado, TRIM(:familia), TRIM(:edicion), TRIM(:version), :compra, :precio, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':compra'=>$compra, ':precio'=>$precio, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM compras_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    function datos3($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_oracle WHERE familia = :familia AND edicion = :edicion '
            . 'AND version = :version AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->row = $sql->fetch();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_oracle WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function totalCompras($cliente, $familia, $edicion, $asignacion, $asignaciones) {
        $where = " AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);

        if($edicion == ""){
            $where .= " AND (edicion = '' OR edicion IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND edicion = :edicion ";
            $array[':edicion'] = $edicion;
        }

        if($asignacion != ""){
            $where .= " AND asignacion = :asignacion ";
            $array[':asignacion'] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where .= " AND asignacion IN (" . $asignacion . ") ";
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(SUM(IFNULL(compra, 0)), 0) AS compra '
            . 'FROM compras_oracle '
            . 'WHERE cliente = :cliente ' . $where
            . 'ORDER BY id');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["compra"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function comprasAsignacion($cliente, $familia, $edicion, $asignacion){
        $where = " AND compras_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
        $where1 = " AND resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        
        if($edicion == ""){
            $where .= " AND (compras_oracle.edicion = '' OR compras_oracle.edicion IS NULL) ";
            $where1 .= " AND (resumen_oracle.edicion = '' OR resumen_oracle.edicion IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND compras_oracle.edicion = :edicion ";
            $where1 .= " AND resumen_oracle.edicion = :edicion ";
            $array[':edicion'] = $edicion;
        }

        if($asignacion != ""){
            $where .= " AND compras_oracle.asignacion = :asignacion ";
            $array[':asignacion'] = $asignacion;
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT compras_oracle.familia,
                    compras_oracle.edicion AS office,
                    compras_oracle.version,
                    compras_oracle.asignacion,
                    IFNULL(COUNT(resumen_oracle.familia), 0) AS instalaciones,
                    (compras_oracle.compra - IFNULL(COUNT(resumen_oracle.familia), 0)) AS balance,
                    (compras_oracle.compra - IFNULL(COUNT(resumen_oracle.familia), 0)) * compras_oracle.precio AS balancec,
                    compras_oracle.compra,
                    (compras_oracle.compra - IFNULL(COUNT(resumen_oracle.familia), 0)) AS disponible,
                    compras_oracle.precio /*,
                    compras_oracle.cantidadGAP,
                    compras_oracle.balanceGAP*/
                FROM compras_oracle
                    LEFT JOIN (SELECT  resumen_oracle.cliente,
                            resumen_oracle.empleado,
                            resumen_oracle.familia,
                            resumen_oracle.edicion,
                            resumen_oracle.version,
                            detalles_equipo_oracle.asignacion
                        FROM resumen_oracle
                            INNER JOIN detalles_equipo_oracle ON resumen_oracle.equipo = detalles_equipo_oracle.equipo
                            AND resumen_oracle.cliente = detalles_equipo_oracle.cliente
                        WHERE resumen_oracle.cliente = :cliente ' . $where1 . '
                        GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version
                        ORDER BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version) AS resumen_oracle ON compras_oracle.cliente = resumen_oracle.cliente AND compras_oracle.familia = resumen_oracle.familia
                    AND compras_oracle.edicion = resumen_oracle.edicion AND compras_oracle.version = resumen_oracle.version
                    AND compras_oracle.asignacion = resumen_oracle.asignacion
                WHERE compras_oracle.cliente = :cliente ' . $where . '
                GROUP BY compras_oracle.familia, compras_oracle.edicion, compras_oracle.version, compras_oracle.asignacion
                ORDER BY compras_oracle.familia, compras_oracle.edicion, compras_oracle.version, compras_oracle.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf4/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}