<?php
class moduloServidores_SPLA extends General{
    
    function windowServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla.cluster,
                    tabla.host,
                    tabla.equipo,
                    tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    tabla.tipo,
                    tabla.cpu,
                    tabla.cores,
                    tabla.licSrv,
                    tabla.licProc,
                    tabla.licCore
                FROM (SELECT "" AS cluster,
                        "" AS host,
                        filepcs2.cn AS equipo,
                        "Windows Server" AS familia,
                        ObtenerEdicion(TRIM(REPLACE(filepcs2.os, "Windows Server", ""))) AS edicion,
                        ObtenerVersion(TRIM(REPLACE(TRIM(REPLACE(filepcs2.os, "Windows Server", "")), ObtenerEdicion(TRIM(REPLACE(filepcs2.os, "Windows Server", ""))),""))) AS version,
                        IF(consolidadoTipoEquipo.modelo LIKE "%Virtual%", "Virtual", "Fisico") AS tipo,
                        IFNULL(consolidadoProcesadores.cpu, 0) AS cpu,
                        IFNULL(consolidadoProcesadores.cores, 0) AS cores,
                        0 AS licSrv,
                        0 As licProc,
                        0 AS licCore
                    FROM filepcs_SPLA AS filepcs2
                        LEFT JOIN consolidadoTipoEquipo_SPLA AS consolidadoTipoEquipo ON filepcs2.cn = substring_index(consolidadoTipoEquipo.host_name,".",1) AND consolidadoTipoEquipo.cliente = :cliente AND consolidadoTipoEquipo.empleado = :empleado
                        LEFT JOIN consolidadoProcesadores_SPLA AS consolidadoProcesadores ON consolidadoTipoEquipo.host_name = consolidadoProcesadores.host_name AND consolidadoProcesadores.cpu > 0 AND consolidadoProcesadores.cliente = :clienteProc AND consolidadoProcesadores.empleado = :empleado
                    WHERE filepcs2.os LIKE "%Windows Server%" AND filepcs2.cliente = :clienteFile AND filepcs2.empleado
                    GROUP BY filepcs2.cn, consolidadoProcesadores.cpu, consolidadoProcesadores.cores) AS tabla
                WHERE tabla.tipo = :tipo');
            $sql->execute(array(':clienteProc'=>$cliente, ':clienteFile'=>$cliente, ':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function SQLServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla.cluster,
                    tabla.host,
                    tabla.equipo,
                    tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    tabla.tipo,
                    tabla.cpu,
                    tabla.cores,
                    tabla.licSrv,
                    tabla.licProc,
                    tabla.licCore
                FROM (SELECT "" AS cluster,
                        "" AS host,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        IF(consolidadoTipoEquipo.modelo LIKE "%Virtual%", "Virtual", "Fisico") AS tipo,
                        consolidadoProcesadores.cpu,
                        consolidadoProcesadores.cores,
                        0 AS licSrv,
                        0 As licProc,
                        0 AS licCore
                    FROM resumen_SPLA AS resumen_office2 
                        LEFT JOIN consolidadoTipoEquipo_SPLA AS consolidadoTipoEquipo ON resumen_office2.equipo = substring_index(consolidadoTipoEquipo.host_name,".",1) AND consolidadoTipoEquipo.cliente = :cliente AND consolidadoTipoEquipo.empleado = :empleado
                        LEFT JOIN consolidadoProcesadores_SPLA AS consolidadoProcesadores ON consolidadoTipoEquipo.host_name = consolidadoProcesadores.host_name AND consolidadoProcesadores.cliente = :clienteProc AND consolidadoProcesadores.empleado = :empleado
                    WHERE resumen_office2.familia LIKE "%SQL%" AND resumen_office2.cliente = :clienteResumen AND resumen_office2.empleado = :empleado
                    GROUP BY equipo, consolidadoProcesadores.cpu, consolidadoProcesadores.cores) AS tabla
                WHERE tabla.tipo = :tipo');
            
            $sql->execute(array(':clienteProc'=>$cliente, ':clienteResumen'=>$cliente, ':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeWindowServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM windowServerClientes_SPLA
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeWindowServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM alineacionWindowsServidoresSPLA
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientes_SPLA
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerClienteSam($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientesSPLASam
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerClienteSamArchivo($archivo, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientesSPLASam
                WHERE archivo = :archivo AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':archivo'=>$archivo, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerCluster($cliente, $empleado, $tipo, $cluster){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientes_SPLA
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo AND cluster = :cluster
                GROUP BY host
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo, ':cluster'=>$cluster));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionWindowsServidoresSPLA
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerAlineacionSam($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionWindowsServidoresSPLASam
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerAlineacionSamArchivo($archivo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionWindowsServidoresSPLASam
                WHERE archivo = :archivo');
            $sql->execute(array(':archivo'=>$archivo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientes_SPLA
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerClienteSam($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientesSPLA_Sam
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerClienteSamArchivo($archivo, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientesSPLA_Sam
                WHERE archivo = :archivo AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':archivo'=>$archivo, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerCluster($cliente, $empleado, $tipo, $cluster){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientes_SPLA
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo AND cluster = :cluster
                GROUP BY host
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo, ':cluster'=>$cluster));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionSqlServidoresSPLA
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerAlineacionSam($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionSqlServidoresSPLASam
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerAlineacionSamArchivo($archivo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionSqlServidoresSPLASam
                WHERE archivo = :archivo');
            $sql->execute(array(':archivo'=>$archivo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeSqlServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM sqlServerClientes_SPLA
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeSqlServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM alineacionSqlServidoresSPLA
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarWindowServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $tipo, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO windowServerClientes_SPLA (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :tipo, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':tipo'=>$tipo, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarAlineacionWindowServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $tipo, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionWindowsServidoresSPLA (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :tipo, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':tipo'=>$tipo, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarSQLServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $tipo, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO sqlServerClientes_SPLA (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :tipo, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':tipo'=>$tipo, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarAlineacionSQLServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $tipo, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionSqlServidoresSPLA (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :tipo, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':tipo'=>$tipo, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarWindowServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM windowServerClientes_SPLA WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarWindowServer1($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM windowServerClientes_SPLA WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarAlineaionWindowServer($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM alineacionWindowsServidoresSPLA WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarSqlServer1($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM sqlServerClientes_SPLA WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarSqlServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM sqlServerClientes_SPLA WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarAlineaionSqlServer($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM alineacionSqlServidoresSPLA WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function obtenerEdicProducto($fabricante, $producto){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
                    ediciones.nombre
                FROM `tabla_equivalencia`
                    INNER JOIN ediciones ON `tabla_equivalencia`.`edicion` = ediciones.idEdicion AND ediciones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.`familia` = :producto
                GROUP BY idEdicion');
            $sql->execute(array(':fabricante'=>$fabricante, ':producto' => $producto));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function obtenerEdicProductoNombre($fabricante, $producto){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
                    ediciones.nombre
                FROM (SELECT tabla_equivalencia.fabricante,
                        productos.nombre AS familia,
                        tabla_equivalencia.`edicion` AS idEdicion
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`) AS `tabla_equivalencia`
                    INNER JOIN ediciones ON `tabla_equivalencia`.`idEdicion` = ediciones.idEdicion AND ediciones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.`familia` = :producto
                GROUP BY idEdicion');
            $sql->execute(array(':fabricante'=>$fabricante, ':producto' => $producto));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function obtenerVersionEquivalencia($fabricante, $producto, $edicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT versiones.id,
                    versiones.nombre
                FROM (SELECT tabla_equivalencia.`fabricante`,
                        productos.`idProducto` AS idProducto,
                        productos.nombre AS familia,
                        ediciones.`idEdicion` AS idEdicion,
                        ediciones.nombre AS edicion,
                        versiones.id AS idVersion,
                        versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.`edicion` = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.`version` = versiones.id) AS tabla_equivalencia
                    INNER JOIN versiones ON `tabla_equivalencia`.`idVersion` = versiones.`id` AND versiones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.`familia` = :producto AND tabla_equivalencia.`edicion` = :edicion');
            $sql->execute(array(':fabricante'=>$fabricante, ':producto' => $producto, ':edicion'=>$edicion));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
  
    function balanzaServer($cliente, $empleado, $tipoServidor){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.familia,
                    tabla_equivalencia.edicion,
                    tabla_equivalencia.version,
                    IFNULL(SUM(IFNULL(balance_office2.instalaciones, 0)), 0) AS instalaciones,
                    ObtenerLicenciaServidoresSPLA(:idCliente, :empleado, tabla_equivalencia.familia, "Fisico", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Fisico,
                    ObtenerLicenciaServidoresSPLA(:cliente, :empleado, tabla_equivalencia.familia, "Virtual", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Virtual
                FROM (SELECT productos.nombre AS familia,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.version = versiones.id) AS tabla_equivalencia
                    LEFT JOIN balance_SPLA AS balance_office2 ON tabla_equivalencia.familia = balance_office2.familia AND
                    tabla_equivalencia.edicion = balance_office2.office AND
                    tabla_equivalencia.version = balance_office2.version AND balance_office2.cliente = :clienteBalanza AND balance_office2.empleado = :empleado
                WHERE tabla_equivalencia.familia = :tipoServidor
                GROUP BY tabla_equivalencia.familia, tabla_equivalencia.edicion, tabla_equivalencia.version');
            $sql->execute(array(':idCliente'=>$cliente, ':cliente'=>$cliente, ':clienteBalanza'=>$cliente, ':empleado'=>$empleado, ':tipoServidor'=>$tipoServidor));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function balanzaServerSam($cliente, $empleado, $tipoServidor){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.familia,
                    tabla_equivalencia.edicion,
                    tabla_equivalencia.version,
                    IFNULL(SUM(IFNULL(balance_office2.instalaciones, 0)), 0) AS instalaciones,
                    ObtenerLicenciaServidoresSPLASam(:idCliente, :empleado, tabla_equivalencia.familia, "Fisico", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Fisico,
                    ObtenerLicenciaServidoresSPLASam(:cliente, :empleado, tabla_equivalencia.familia, "Virtual", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Virtual
                FROM (SELECT productos.nombre AS familia,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.version = versiones.id) AS tabla_equivalencia
                    LEFT JOIN balance_SPLASam AS balance_office2 ON tabla_equivalencia.familia = balance_office2.familia AND
                    tabla_equivalencia.edicion = balance_office2.office AND
                    tabla_equivalencia.version = balance_office2.version AND balance_office2.cliente = :clienteBalanza AND balance_office2.empleado = :empleado
                WHERE tabla_equivalencia.familia = :tipoServidor
                GROUP BY tabla_equivalencia.familia, tabla_equivalencia.edicion, tabla_equivalencia.version');
            $sql->execute(array(':idCliente'=>$cliente, ':cliente'=>$cliente, ':clienteBalanza'=>$cliente, ':empleado'=>$empleado, ':tipoServidor'=>$tipoServidor));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function balanzaServerSamArchivo($archivo, $tipoServidor){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.familia,
                    tabla_equivalencia.edicion,
                    tabla_equivalencia.version,
                    IFNULL(SUM(IFNULL(balance_office2.instalaciones, 0)), 0) AS instalaciones,
                    ObtenerLicenciaServidoresSPLASamArchivo(:archivo, tabla_equivalencia.familia, "Fisico", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Fisico,
                    ObtenerLicenciaServidoresSPLASamArchivo(:archivo, tabla_equivalencia.familia, "Virtual", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Virtual
                FROM (SELECT productos.nombre AS familia,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.version = versiones.id) AS tabla_equivalencia
                    LEFT JOIN balance_SPLASam AS balance_office2 ON tabla_equivalencia.familia = balance_office2.familia AND
                    tabla_equivalencia.edicion = balance_office2.office AND
                    tabla_equivalencia.version = balance_office2.version AND balance_office2.archivo = :archivo
                WHERE tabla_equivalencia.familia = :tipoServidor
                GROUP BY tabla_equivalencia.familia, tabla_equivalencia.edicion, tabla_equivalencia.version');
            $sql->execute(array(':archivo'=>$archivo, ':tipoServidor'=>$tipoServidor));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return false;
        }
    }
}