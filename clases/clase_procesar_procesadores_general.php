<?php
class clase_procesar_procesadores_general extends General{
    private $client_id;
    private $client_empleado;
    private $kDatoControl;
    private $kHostName;
    private $kTipoCPU;
    private $kCPUs;
    private $kCores;
    private $kProcesadoresLogicos;
    private $kTipoEscaneo;
    private $procesarProcesadores;
    private $opcion;
    private $idDiagnostic;
    private $bloque;
    private $bloqueValores;
    private $insertarBloque;
    private $tabProcesador;
    private $campoProcesador;
    private $datoControl;
    private $hostName;
    private $tipo_CPU;
    private $cpu;
    private $cores;
    private $procesadores_logicos;
    private $tipo_escaneo;
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabProcesador . " (" . $this->campoProcesador . ", dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM " . $this->tabProcesador . " WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function setTabProcesador($tabProcesador){
        $this->tabProcesador = $tabProcesador;
    } 
    
    function procesarProcesadores($client_id, $client_empleado, $archivoProcesadores, $procesarProcesadores, $kDatoControl,
    $kHostName, $kTipoCPU, $kCPUs, $kCores, $kProcesadoresLogicos, $kTipoEscaneo, $opcion, $idDiagnostic = 0){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->archivoProcesadores = $archivoProcesadores;
        $this->procesarProcesadores = $procesarProcesadores;
        $this->kDatoControl = $kDatoControl;
        $this->kHostName = $kHostName; 
        $this->kTipoCPU = $kTipoCPU;
        $this->kCPUs = $kCPUs;
        $this->kCores = $kCores;
        $this->kProcesadoresLogicos = $kProcesadoresLogicos;
        $this->kTipoEscaneo = $kTipoEscaneo;
        $this->opcion = $opcion;
        $this->idDiagnostic = $idDiagnostic;
        
        $this->cabeceraTablas();
        
        if($this->obtenerSeparadorUniversal($this->archivoProcesadores, 2, 7) === true && $this->procesarProcesadores === true){
            if (($fichero = fopen($this->archivoProcesadores, "r")) !== false) {
                $this->cicloInsertar($fichero);

                $this->verificarRegistrosInsertar1();
            
                fclose($fichero);
            }
        }
    }
            
    function cabeceraTablas(){
        if($this->opcion == "Cloud"){
            $this->tabProcesador = "consolidadoProcesadoresMSCloud";
            $this->campoProcesador = "idDiagnostic";
        }else if($this->opcion == "Diagnostic"){
            $this->tabProcesador = "consolidadoProcesadoresSAMDiagnostic";
            $this->campoProcesador = "idDiagnostic";
        } else if($this->opcion == "microsoft"){
            $this->tabProcesador = "consolidadoProcesadores";
            $this->campoProcesador = "cliente, empleado";
        }
    }
    
    function cicloInsertar($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {                 
            if($i > 2 && $datos[$this->kDatoControl] != 'Dato de Control' && isset($datos[$this->kTipoEscaneo]) && $datos[$this->kTipoEscaneo] != ""){
                $this->crearBloque($j);

                $this->setValores($datos);
                $this->crearBloqueValores($j);

                $j = $this->verificarRegistrosInsertar($j);
                
                $j++;
            }
            
            $i++; 
        }
    }
    
    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        }
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":dato_control" . $j . ", "
            . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
            . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":dato_control" . $j . ", "
            . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
            . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j){
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }
                
        $this->bloqueValores[":dato_control" . $j] = $this->datoControl;
        $this->bloqueValores[":host_name" . $j] = $this->hostName;
        $this->bloqueValores[":tipo_CPU" . $j] = $this->tipo_CPU;
        $this->bloqueValores[":cpu" . $j] = $this->cpu;
        $this->bloqueValores[":cores" . $j] = $this->cores;
        $this->bloqueValores[":procesadores_logicos" . $j] = $this->procesadores_logicos;
        $this->bloqueValores[":tipo_escaneo" . $j] = $this->tipo_escaneo;
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function insertarGeneral(){
        if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        } 
    }     
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
    
    function setValores($datos){
        $this->datoControl = "";
        $this->hostName = "";
        $this->tipo_CPU = "";
        $this->cpu = 0;
        $this->cores = 0;
        $this->procesadores_logicos = 0;
        $this->tipo_escaneo = "";

        if(isset($datos[$this->kDatoControl])){
            $this->datoControl = $this->truncarString($this->get_escape(utf8_encode($datos[$this->kDatoControl])), 250);
        }

        if(isset($datos[$this->kHostName])){
            $this->hostName = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($datos[$this->kHostName]))), 250);  
        }

        if(isset($datos[$this->kTipoCPU])){
            $this->tipo_CPU = $this->truncarString($this->get_escape(utf8_encode($datos[$this->kTipoCPU])), 250);
        }

        if(isset($datos[$this->kCPUs]) && filter_var($datos[$this->kCPUs], FILTER_VALIDATE_INT) !== false){
            $this->cpu = $datos[$this->kCPUs];
        }

        if(isset($datos[$this->kCores]) && filter_var($datos[$this->kCores], FILTER_VALIDATE_INT) !== false){
            $this->cores = $datos[$this->kCores];
        }

        if(isset($datos[$this->kProcesadoresLogicos]) && filter_var($datos[$this->kProcesadoresLogicos], FILTER_VALIDATE_INT) !== false){
            $this->procesadores_logicos = $datos[$this->kProcesadoresLogicos];
        }

        if(isset($datos[$this->kTipoEscaneo])){
            $this->tipo_escaneo = $this->truncarString($this->get_escape(utf8_encode($datos[$this->kTipoEscaneo])), 250);
        }    
    }
}
