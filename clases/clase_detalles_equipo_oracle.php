<?php
class DetallesOracle extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $os, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_oracle (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) '
            . 'VALUES (:cliente, :empleado, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo,':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    /*function insertarSAM($archivo, $cliente, $equipo, $os, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_oracleSam (cliente, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) VALUES '
            . 'VALUES (:cliente, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)');        
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo,':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/

    // Actualizar
    function actualizar($cliente, $empleado, $equipo, $errors) {       
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalles_equipo_oracle SET errors = :errors WHERE equipo = :equipo AND cliente = :cliente AND empleado = :empleado');        
            $sql->execute(array(':errors'=>$errors, ':equipo'=>$equipo, ':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo actualizar el registro";
            return false;
        }
    }
    
    function actualizarAsignacion($cliente, $empleado, $equipo, $asignacion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo_oracle SET asignacion = :asignacion WHERE cliente = :cliente AND empleado = :empleado "
            .  "AND equipo = :equipo");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':asignacion'=>$asignacion, ':equipo'=>$equipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalles_equipo_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_oracle WHERE cliente = :cliente AND empleado = :empleado AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_todoAsignacion($cliente, $asignacion, $asignaciones) {        
        try {
            $array = array(':cliente'=>$cliente);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_oracle WHERE cliente = :cliente AND os LIKE "%Windows%" ' . $where . ' ORDER BY id');
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return array();
        }
    }
    
    function listar_todo1($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_oracle WHERE cliente = :cliente ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return array();
        }
    }
    
    function listar_todo1Asignacion($cliente, $asignacion, $asignaciones) {        
        try {
            $array = array(':cliente'=>$cliente);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_oracle WHERE cliente = :cliente ' . $where . ' ORDER BY id');
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todoSAM($cliente) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_oracleSam WHERE cliente = :cliente AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }*/
    
    function listar_todoSAMArchivo($archivo) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_oracleSam WHERE archivo = :archivo AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':archivo'=>$archivo));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }

    function listar_optimizacion($cliente, $empleado, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(OracleApplicationServer.edicion) AS OracleApplicationServer,
                        GROUP_CONCAT(OracleEnterpriseRepository.edicion) AS OracleEnterpriseRepository,
                        GROUP_CONCAT(PlumtreePortal.edicion) AS PlumtreePortal,
                        GROUP_CONCAT(ServiceOrientedArchitecture.edicion) AS ServiceOrientedArchitecture,
                        GROUP_CONCAT(TUXEDO.edicion) AS TUXEDO,
                        GROUP_CONCAT(WebLogicBEA.edicion) AS WebLogicBEA,
                        GROUP_CONCAT(WebLogicOracle.edicion) AS WebLogicOracle,
                        GROUP_CONCAT(OracleDataBase.edicion) AS OracleDataBase,
                        GROUP_CONCAT(OracleEnterpriseManager.edicion) AS OracleEnterpriseManager,
                        GROUP_CONCAT(BusinessProcessManagement.edicion) AS BusinessProcessManagement,
                        GROUP_CONCAT(OracleEBS.edicion) AS OracleEBS,
                        GROUP_CONCAT(OracleFormsReports.edicion) AS OracleFormsReports,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_oracle AS detEquipo WHERE detEquipo.cliente = :cliente1 AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_oracle AS detalles_equipo2
                      LEFT JOIN resumen_oracle AS OracleApplicationServer ON detalles_equipo2.equipo = OracleApplicationServer.equipo AND detalles_equipo2.cliente = OracleApplicationServer.cliente AND detalles_equipo2.empleado = OracleApplicationServer.empleado AND OracleApplicationServer.familia = 'Oracle Application Server'
                      LEFT JOIN resumen_oracle AS OracleEnterpriseRepository ON detalles_equipo2.equipo = OracleEnterpriseRepository.equipo AND detalles_equipo2.cliente = OracleEnterpriseRepository.cliente AND detalles_equipo2.empleado = OracleEnterpriseRepository.empleado AND OracleEnterpriseRepository.familia = 'Oracle Enterprise Repository'
                      LEFT JOIN resumen_oracle AS PlumtreePortal ON detalles_equipo2.equipo = PlumtreePortal.equipo AND detalles_equipo2.cliente = PlumtreePortal.cliente AND detalles_equipo2.empleado = PlumtreePortal.empleado AND PlumtreePortal.familia = 'Plumtree Portal'
                      LEFT JOIN resumen_oracle AS ServiceOrientedArchitecture ON detalles_equipo2.equipo = ServiceOrientedArchitecture.equipo AND detalles_equipo2.cliente = ServiceOrientedArchitecture.cliente AND detalles_equipo2.empleado = ServiceOrientedArchitecture.empleado AND ServiceOrientedArchitecture.familia = 'Service Oriented Architecture'
                      LEFT JOIN resumen_oracle AS TUXEDO ON detalles_equipo2.equipo = TUXEDO.equipo AND detalles_equipo2.cliente = TUXEDO.cliente AND detalles_equipo2.empleado = TUXEDO.empleado AND TUXEDO.familia = 'TUXEDO'
                      LEFT JOIN resumen_oracle AS WebLogicBEA ON detalles_equipo2.equipo = WebLogicBEA.equipo AND detalles_equipo2.cliente = WebLogicBEA.cliente AND detalles_equipo2.empleado = WebLogicBEA.empleado AND WebLogicBEA.familia = 'WebLogic BEA'
                      LEFT JOIN resumen_oracle AS WebLogicOracle ON detalles_equipo2.equipo = WebLogicOracle.equipo AND detalles_equipo2.cliente = WebLogicOracle.cliente AND detalles_equipo2.empleado = WebLogicOracle.empleado AND WebLogicOracle.familia = 'WebLogic Oracle'
                      LEFT JOIN resumen_oracle AS OracleDataBase ON detalles_equipo2.equipo = OracleDataBase.equipo AND detalles_equipo2.cliente = OracleDataBase.cliente AND detalles_equipo2.empleado = OracleDataBase.empleado AND OracleDataBase.familia = 'Oracle DataBase'
                      LEFT JOIN resumen_oracle AS OracleEnterpriseManager ON detalles_equipo2.equipo = OracleEnterpriseManager.equipo AND detalles_equipo2.cliente = OracleEnterpriseManager.cliente AND detalles_equipo2.empleado = OracleEnterpriseManager.empleado AND OracleEnterpriseManager.familia = 'Oracle Enterprise Manager'
                      LEFT JOIN resumen_oracle AS BusinessProcessManagement ON detalles_equipo2.equipo = BusinessProcessManagement.equipo AND detalles_equipo2.cliente = BusinessProcessManagement.cliente AND detalles_equipo2.empleado = BusinessProcessManagement.empleado AND BusinessProcessManagement.familia = 'Business Process Management'
                      LEFT JOIN resumen_oracle AS OracleEBS ON detalles_equipo2.equipo = OracleEBS.equipo AND detalles_equipo2.cliente = OracleEBS.cliente AND detalles_equipo2.empleado = OracleEBS.empleado AND OracleEBS.familia = 'Oracle EBS'
                      LEFT JOIN resumen_oracle AS OracleFormsReports ON detalles_equipo2.equipo = OracleFormsReports.equipo AND detalles_equipo2.cliente = OracleFormsReports.cliente AND detalles_equipo2.empleado = OracleFormsReports.empleado AND OracleFormsReports.familia = 'Oracle Forms & Reports'
                 WHERE detalles_equipo2.cliente=:cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = :os
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute(array(':cliente'=>$cliente, ':cliente1'=>$cliente, ':empleado'=>$empleado, ':os'=>$os));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function listar_optimizacionAsignacion($cliente, $os, $asignacion, $asignaciones) {
        try {
            $array = array(':cliente'=>$cliente, ':os'=>$os);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array = array(':cliente'=>$cliente, ':os'=>$os, ':asignacion'=>$asignacion);
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(OracleApplicationServer.edicion) AS OracleApplicationServer,
                        GROUP_CONCAT(OracleEnterpriseRepository.edicion) AS OracleEnterpriseRepository,
                        GROUP_CONCAT(PlumtreePortal.edicion) AS PlumtreePortal,
                        GROUP_CONCAT(ServiceOrientedArchitecture.edicion) AS ServiceOrientedArchitecture,
                        GROUP_CONCAT(TUXEDO.edicion) AS TUXEDO,
                        GROUP_CONCAT(WebLogicBEA.edicion) AS WebLogicBEA,
                        GROUP_CONCAT(WebLogicOracle.edicion) AS WebLogicOracle,
                        GROUP_CONCAT(OracleDataBase.edicion) AS OracleDataBase,
                        GROUP_CONCAT(OracleEnterpriseManager.edicion) AS OracleEnterpriseManager,
                        GROUP_CONCAT(BusinessProcessManagement.edicion) AS BusinessProcessManagement,
                        GROUP_CONCAT(OracleEBS.edicion) AS OracleEBS,
                        GROUP_CONCAT(OracleFormsReports.edicion) AS OracleFormsReports,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_oracle AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo " . $where . ") > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_oracle AS detalles_equipo2
                      LEFT JOIN resumen_oracle AS OracleApplicationServer ON detalles_equipo2.equipo = OracleApplicationServer.equipo AND detalles_equipo2.cliente = OracleApplicationServer.cliente AND OracleApplicationServer.familia = 'Oracle Application Server'
                      LEFT JOIN resumen_oracle AS OracleEnterpriseRepository ON detalles_equipo2.equipo = OracleEnterpriseRepository.equipo AND detalles_equipo2.cliente = OracleEnterpriseRepository.cliente AND OracleEnterpriseRepository.familia = 'Oracle Enterprise Repository'
                      LEFT JOIN resumen_oracle AS PlumtreePortal ON detalles_equipo2.equipo = PlumtreePortal.equipo AND detalles_equipo2.cliente = PlumtreePortal.cliente AND PlumtreePortal.familia = 'Plumtree Portal'
                      LEFT JOIN resumen_oracle AS ServiceOrientedArchitecture ON detalles_equipo2.equipo = ServiceOrientedArchitecture.equipo AND detalles_equipo2.cliente = ServiceOrientedArchitecture.cliente AND ServiceOrientedArchitecture.familia = 'Service Oriented Architecture'
                      LEFT JOIN resumen_oracle AS TUXEDO ON detalles_equipo2.equipo = TUXEDO.equipo AND detalles_equipo2.cliente = TUXEDO.cliente AND TUXEDO.familia = 'TUXEDO'
                      LEFT JOIN resumen_oracle AS WebLogicBEA ON detalles_equipo2.equipo = WebLogicBEA.equipo AND detalles_equipo2.cliente = WebLogicBEA.cliente AND WebLogicBEA.familia = 'WebLogic BEA'
                      LEFT JOIN resumen_oracle AS WebLogicOracle ON detalles_equipo2.equipo = WebLogicOracle.equipo AND detalles_equipo2.cliente = WebLogicOracle.cliente AND WebLogicOracle.familia = 'WebLogic Oracle'
                      LEFT JOIN resumen_oracle AS OracleDataBase ON detalles_equipo2.equipo = OracleDataBase.equipo AND detalles_equipo2.cliente = OracleDataBase.cliente AND OracleDataBase.familia = 'Oracle DataBase'
                      LEFT JOIN resumen_oracle AS OracleEnterpriseManager ON detalles_equipo2.equipo = OracleEnterpriseManager.equipo AND detalles_equipo2.cliente = OracleEnterpriseManager.cliente AND OracleEnterpriseManager.familia = 'Oracle Enterprise Manager'
                      LEFT JOIN resumen_oracle AS BusinessProcessManagement ON detalles_equipo2.equipo = BusinessProcessManagement.equipo AND detalles_equipo2.cliente = BusinessProcessManagement.cliente AND BusinessProcessManagement.familia = 'Business Process Management'
                      LEFT JOIN resumen_oracle AS OracleEBS ON detalles_equipo2.equipo = OracleEBS.equipo AND detalles_equipo2.cliente = OracleEBS.cliente AND OracleEBS.familia = 'Oracle EBS'
                      LEFT JOIN resumen_oracle AS OracleFormsReports ON detalles_equipo2.equipo = OracleFormsReports.equipo AND detalles_equipo2.cliente = OracleFormsReports.cliente AND OracleFormsReports.familia = 'Oracle Forms & Reports'
                 WHERE detalles_equipo2.cliente=:cliente AND rango NOT IN (1) AND detalles_equipo2.familia = :os " . $where . "
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }
    
    /*function listar_optimizacionSam($cliente, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(OracleApplicationServer.edicion) AS OracleApplicationServer,
                        GROUP_CONCAT(OracleEnterpriseRepository.edicion) AS OracleEnterpriseRepository,
                        GROUP_CONCAT(PlumtreePortal.edicion) AS PlumtreePortal,
                        GROUP_CONCAT(ServiceOrientedArchitecture.edicion) AS ServiceOrientedArchitecture,
                        GROUP_CONCAT(TUXEDO.edicion) AS TUXEDO,
                        GROUP_CONCAT(WebLogicBEA.edicion) AS WebLogicBEA,
                        GROUP_CONCAT(WebLogicOracle.edicion) AS WebLogicOracle,
                        GROUP_CONCAT(OracleDataBase.edicion) AS OracleDataBase,
                        GROUP_CONCAT(OracleEnterpriseManager.edicion) AS OracleEnterpriseManager,
                        GROUP_CONCAT(BusinessProcessManagement.edicion) AS BusinessProcessManagement,
                        GROUP_CONCAT(OracleEB.edicion) AS OracleEBS,
                        GROUP_CONCAT(OracleFormsReports.edicion) AS OracleFormsReports,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_oracleSam AS detEquipo WHERE detEquipo.cliente = :cliente1 AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_oracleSam AS detalles_equipo2
                      LEFT JOIN resumen_oracleSam AS OracleApplicationServer ON detalles_equipo2.equipo = OracleApplicationServer.equipo AND detalles_equipo2.cliente = OracleApplicationServer.cliente AND OracleApplicationServer.familia = 'Oracle Application Server'
                      LEFT JOIN resumen_oracleSam AS OracleEnterpriseRepository ON detalles_equipo2.equipo = OracleEnterpriseRepository.equipo AND detalles_equipo2.cliente = OracleEnterpriseRepository.cliente AND OracleEnterpriseRepository.familia = 'Oracle Enterprise Repository'
                      LEFT JOIN resumen_oracleSam AS PlumtreePortal ON detalles_equipo2.equipo = PlumtreePortal.equipo AND detalles_equipo2.cliente = PlumtreePortal.cliente AND PlumtreePortal.familia = 'Plumtree Portal'
                      LEFT JOIN resumen_oracleSam AS ServiceOrientedArchitecture ON detalles_equipo2.equipo = ServiceOrientedArchitecture.equipo AND detalles_equipo2.cliente = ServiceOrientedArchitecture.cliente AND ServiceOrientedArchitecture.familia = 'Service Oriented Architecture'
                      LEFT JOIN resumen_oracleSam AS TUXEDO ON detalles_equipo2.equipo = TUXEDO.equipo AND detalles_equipo2.cliente = TUXEDO.cliente AND TUXEDO.familia = 'TUXEDO'
                      LEFT JOIN resumen_oracleSam AS WebLogicBEA ON detalles_equipo2.equipo = WebLogicBEA.equipo AND detalles_equipo2.cliente = WebLogicBEA.cliente AND WebLogicBEA.familia = 'WebLogic BEA'
                      LEFT JOIN resumen_oracleSam AS WebLogicOracle ON detalles_equipo2.equipo = WebLogicOracle.equipo AND detalles_equipo2.cliente = WebLogicOracle.cliente AND WebLogicOracle.familia = 'WebLogic Oracle'
                      LEFT JOIN resumen_oracleSam AS OracleDataBase ON detalles_equipo2.equipo = OracleDataBase.equipo AND detalles_equipo2.cliente = OracleDataBase.cliente AND OracleDataBase.familia = 'Oracle DataBase'
                      LEFT JOIN resumen_oracleSam AS OracleEnterpriseManager ON detalles_equipo2.equipo = OracleEnterpriseManager.equipo AND detalles_equipo2.cliente = OracleEnterpriseManager.cliente AND OracleEnterpriseManager.familia = 'Oracle Enterprise Manager'
                      LEFT JOIN resumen_oracleSam AS BusinessProcessManagement ON detalles_equipo2.equipo = BusinessProcessManagement.equipo AND detalles_equipo2.cliente = BusinessProcessManagement.cliente AND BusinessProcessManagement.familia = 'Business Process Management'
                      LEFT JOIN resumen_oracleSam AS OracleEBS ON detalles_equipo2.equipo = OracleEBS.equipo AND detalles_equipo2.cliente = OracleEBS.cliente AND OracleEBS.familia = 'Oracle EBS'
                      LEFT JOIN resumen_oracleSam AS OracleFormsReports ON detalles_equipo2.equipo = OracleFormsReports.equipo AND detalles_equipo2.cliente = OracleFormsReports.cliente AND OracleFormsReports.familia = 'Oracle Forms & Reports'
                 WHERE detalles_equipo2.cliente=:cliente AND rango NOT IN (1) AND detalles_equipo2.familia = :os
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute(array(':cliente'=>$cliente, ':cliente1'=>$cliente, ':os'=>$os));
            if($os == "Windows"){
                $this->listaCliente = $sql->fetchAll();
            }
            else{
                $this->listaServidor = $sql->fetchAll();
            }
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/
    
    function listar_optimizacionSamArchivo($archivo, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(OracleApplicationServer.edicion) AS OracleApplicationServer,
                        GROUP_CONCAT(OracleEnterpriseRepository.edicion) AS OracleEnterpriseRepository,
                        GROUP_CONCAT(PlumtreePortal.edicion) AS PlumtreePortal,
                        GROUP_CONCAT(ServiceOrientedArchitecture.edicion) AS ServiceOrientedArchitecture,
                        GROUP_CONCAT(TUXEDO.edicion) AS TUXEDO,
                        GROUP_CONCAT(WebLogicBEA.edicion) AS WebLogicBEA,
                        GROUP_CONCAT(WebLogicOracle.edicion) AS WebLogicOracle,
                        GROUP_CONCAT(OracleDataBase.edicion) AS OracleDataBase,
                        GROUP_CONCAT(OracleEnterpriseManager.edicion) AS OracleEnterpriseManager,
                        GROUP_CONCAT(BusinessProcessManagement.edicion) AS BusinessProcessManagement,
                        GROUP_CONCAT(OracleEB.edicion) AS OracleEBS,
                        GROUP_CONCAT(OracleFormsReports.edicion) AS OracleFormsReports,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_oracleSam AS detEquipo WHERE detEquipo.archivo = :archivo AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_oracleSam AS detalles_equipo2
                      LEFT JOIN resumen_oracleSam AS OracleApplicationServer ON detalles_equipo2.equipo = OracleApplicationServer.equipo AND detalles_equipo2.cliente = OracleApplicationServer.cliente AND OracleApplicationServer.familia = 'Oracle Application Server'
                      LEFT JOIN resumen_oracleSam AS OracleEnterpriseRepository ON detalles_equipo2.equipo = OracleEnterpriseRepository.equipo AND detalles_equipo2.cliente = OracleEnterpriseRepository.cliente AND OracleEnterpriseRepository.familia = 'Oracle Enterprise Repository'
                      LEFT JOIN resumen_oracleSam AS PlumtreePortal ON detalles_equipo2.equipo = PlumtreePortal.equipo AND detalles_equipo2.cliente = PlumtreePortal.cliente AND PlumtreePortal.familia = 'Plumtree Portal'
                      LEFT JOIN resumen_oracleSam AS ServiceOrientedArchitecture ON detalles_equipo2.equipo = ServiceOrientedArchitecture.equipo AND detalles_equipo2.cliente = ServiceOrientedArchitecture.cliente AND ServiceOrientedArchitecture.familia = 'Service Oriented Architecture'
                      LEFT JOIN resumen_oracleSam AS TUXEDO ON detalles_equipo2.equipo = TUXEDO.equipo AND detalles_equipo2.cliente = TUXEDO.cliente AND TUXEDO.familia = 'TUXEDO'
                      LEFT JOIN resumen_oracleSam AS WebLogicBEA ON detalles_equipo2.equipo = WebLogicBEA.equipo AND detalles_equipo2.cliente = WebLogicBEA.cliente AND WebLogicBEA.familia = 'WebLogic BEA'
                      LEFT JOIN resumen_oracleSam AS WebLogicOracle ON detalles_equipo2.equipo = WebLogicOracle.equipo AND detalles_equipo2.cliente = WebLogicOracle.cliente AND WebLogicOracle.familia = 'WebLogic Oracle'
                      LEFT JOIN resumen_oracleSam AS OracleDataBase ON detalles_equipo2.equipo = OracleDataBase.equipo AND detalles_equipo2.cliente = OracleDataBase.cliente AND OracleDataBase.familia = 'Oracle DataBase'
                      LEFT JOIN resumen_oracleSam AS OracleEnterpriseManager ON detalles_equipo2.equipo = OracleEnterpriseManager.equipo AND detalles_equipo2.cliente = OracleEnterpriseManager.cliente AND OracleEnterpriseManager.familia = 'Oracle Enterprise Manager'
                      LEFT JOIN resumen_oracleSam AS BusinessProcessManagement ON detalles_equipo2.equipo = BusinessProcessManagement.equipo AND detalles_equipo2.cliente = BusinessProcessManagement.cliente AND BusinessProcessManagement.familia = 'Business Process Management'
                      LEFT JOIN resumen_oracleSam AS OracleEBS ON detalles_equipo2.equipo = OracleEBS.equipo AND detalles_equipo2.cliente = OracleEBS.cliente AND OracleEBS.familia = 'Oracle EBS'
                      LEFT JOIN resumen_oracleSam AS OracleFormsReports ON detalles_equipo2.equipo = OracleFormsReports.equipo AND detalles_equipo2.cliente = OracleFormsReports.cliente AND OracleFormsReports.familia = 'Oracle Forms & Reports'
                 WHERE detalles_equipo2.archivo=:archivo AND rango NOT IN (1) AND detalles_equipo2.familia = :os
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute(array(':archivo'=>$archivo, ':os'=>$os));
            if($os == "Windows"){
                $this->listaCliente = $sql->fetchAll();
            }
            else{
                $this->listaServidor = $sql->fetchAll();
            }
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    /*function fechaSAM($cliente) {
        $this->conexion();
        $fecha = "";
        $query = "SELECT fecha FROM encGrafOracleSam WHERE cliente = :cliente";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }*/
    
    function fechaSAMArchivo($archivo) {
        $this->conexion();
        $fecha = "";
        $query = "SELECT fecha FROM encGrafOracleSam WHERE archivo = :archivo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }
    
    function cantidadProductosOSUso($cliente, $empleado, $familia){
        $query = "SELECT COUNT(id) AS cantidad
            FROM detalles_equipo_oracle 
            WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function cantidadProductosInstalados($cliente, $empleado, $familia){
        try{
            $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo_oracle
                INNER JOIN resumen_oracle AS office ON detalles_equipo_oracle.equipo = office.equipo AND detalles_equipo_oracle.cliente = office.cliente AND detalles_equipo_oracle.empleado = office.empleado AND office.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
            WHERE detalles_equipo_oracle.cliente = :cliente AND detalles_equipo_oracle.empleado = :empleado AND office.familia != ''";
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function productosUsoCliente($cliente, $empleado){
        $query = "SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_oracle
                INNER JOIN resumen_oracle AS office ON detalles_equipo_oracle.equipo = office.equipo AND detalles_equipo_oracle.cliente = office.cliente AND detalles_equipo_oracle.empleado = office.empleado AND office.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = 'Application' AND idMaestra = 1)
            WHERE detalles_equipo_oracle.cliente = :cliente AND detalles_equipo_oracle.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_oracle
                INNER JOIN resumen_oracle AS office ON detalles_equipo_oracle.equipo = office.equipo AND detalles_equipo_oracle.cliente = office.cliente AND detalles_equipo_oracle.empleado = office.empleado AND office.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = 'Database' AND idMaestra = 1)
            WHERE detalles_equipo_oracle.cliente = :cliente AND detalles_equipo_oracle.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_oracle
                INNER JOIN resumen_oracle AS office ON detalles_equipo_oracle.equipo = office.equipo AND detalles_equipo_oracle.cliente = office.cliente AND detalles_equipo_oracle.empleado = office.empleado AND office.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = 'Middleware' AND idMaestra = 1)
            WHERE detalles_equipo_oracle.cliente = :cliente AND detalles_equipo_oracle.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            ";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}