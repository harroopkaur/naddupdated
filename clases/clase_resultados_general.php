<?php
class clase_resultados_general extends General{
    private $id;
    private $opcion;
    private $tabla;
    private $tablaResumen;
    private $tablaTipoEquipo;
    private $tablaProcesadores;
    private $campo;
    private $campoIgualar;
    private $campoIgualarTipoEquipo;
    private $campoIgualarProcesadores; 
    private $campoIgualarProcesadores1;
    private $array;
    private $edicion;
    private $edicionValor;
    
    function actualizarSAMDiagnostic($id, $nombre, $email, $archivo){
        $query = "UPDATE SAMDiagnostic SET nombre = :nombre, email = :email, resultado = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre, ':email'=>$email, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        } 
    }
    
    function actualizarMSCloud($id, $nombre, $email, $archivo){
        $query = "UPDATE MSCloud SET nombre = :nombre, email = :email, archivo = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre, ':email'=>$email, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        } 
    }
    
    function listar_todo1Asignacion($opcion, $id, $asignacion = "", $asignaciones = array()){
        try{
            $where = "";
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion "; 
                $this->array[":asignacion"] = $asignacion;
            } else{
                if($opcion != "Cloud" && $opcion != "Diagnostic"){
                    $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                    $where = " AND asignacion IN (" . $asignacion . ") ";
                }
            }
            
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM " . $this->tabla . " "
            . "WHERE " . $this->campo . $where . " "
            . "ORDER BY id");
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
        
    function totalEquiposActivos($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM " . $this->tabla . "
                WHERE " . $this->campo . " AND rango IN (1, 2, 3)");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquiposLevantados($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM " . $this->tabla . "
                WHERE " . $this->campo . " AND rango IN (1, 2, 3) AND errors = 'Ninguno'");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquipos($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM " . $this->tabla . "
                WHERE " . $this->campo);
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function equiposNoEscaneados($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM " . $this->tabla . "
                WHERE " . $this->campo . " AND equipo NOT IN (
                    SELECT equipo FROM " . $this->tabla . " 
                    WHERE " . $this->campo . " AND errors = 'Ninguno' GROUP BY equipo)
                AND NOT errors IS NULL");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquiposUso($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM " . $this->tabla . "
                WHERE " . $this->campo . " AND rango IN (1, 2, 3)");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalAplicaciones($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM (SELECT resumen.id
                FROM " . $this->tabla . " 
                    INNER JOIN " . $this->tablaResumen . " ON " . $this->tabla . ".equipo = " . $this->tablaResumen . ".equipo
                    AND " . $this->tablaResumen . ".familia IN ('Office', 'Visio', 'Project', 'Visual Studio') AND " . $this->campoIgualar . "
                WHERE " . $this->tabla . "." . $this->campo . " AND " . $this->tabla . ".rango IN (1)
                GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version) tabla");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function unusedSW($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.familia) AS cantidad
                FROM (SELECT " . $this->tablaResumen . ".familia
                    FROM " . $this->tabla . "
                        INNER JOIN " . $this->tablaResumen . " ON " . $this->campoIgualar . "
                        AND " . $this->tabla . ".equipo = " . $this->tablaResumen . ".equipo AND
                        " . $this->tablaResumen . ".familia IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
                    WHERE " . $this->tabla . "." . $this->campo . "
                    AND " . $this->tabla . ".rango NOT IN (1)
                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version) tabla");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function softwareDuplicate($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.equipo,
                    tabla.familia,
                    tabla.edicion,
                    COUNT(tabla.equipo) AS cantidad
                FROM (SELECT " . $this->tablaResumen . ".equipo,
                        " . $this->tablaResumen . ".familia,
                        " . $this->tablaResumen . ".edicion
                    FROM " . $this->tablaResumen . "
                    WHERE " . $this->tablaResumen . "." . $this->campo . "
                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion,
                    " . $this->tablaResumen . ".version) tabla
                GROUP BY tabla.equipo, tabla.familia
                HAVING COUNT(tabla.equipo) > 1");
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function erroneus($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(equipo) AS cantidad
                FROM (SELECT equipo
                    FROM (SELECT " . $this->tablaResumen . ".equipo,
                            " . $this->tablaResumen . ".familia,
                            " . $this->tablaResumen . ".edicion,
                            " . $this->tablaResumen . ".version
                        FROM " . $this->tabla . "
                            INNER JOIN " . $this->tablaResumen . " ON " . $this->campoIgualar . "
                            AND " . $this->tabla . ".equipo = " . $this->tablaResumen . ".equipo AND
                            (" . $this->tablaResumen . ".familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                            OR " . $this->tablaResumen . ".edicion LIKE '%Server%')
                        WHERE " . $this->tabla . "." . $this->campo . " AND " . $this->tabla . ".tipo = 1

                        UNION

                        SELECT " . $this->tablaResumen . ".equipo,
                               " . $this->tablaResumen . ".familia,
                               " . $this->tablaResumen . ".edicion,
                               " . $this->tablaResumen . ".version
                        FROM " . $this->tabla . "
                            INNER JOIN " . $this->tablaResumen . " ON " . $this->campoIgualar . "
                            AND " . $this->tabla . ".equipo = " . $this->tablaResumen . ".equipo AND
                            (" . $this->tablaResumen . ".familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                            AND NOT " . $this->tablaResumen . ".edicion LIKE '%Server%')
                        WHERE " . $this->tabla . "." . $this->campo . " AND " . $this->tabla . ".tipo = 2) tabla
                    GROUP BY equipo, familia, edicion, version) tabla1");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalOffice($opcion, $id){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT " . $this->tablaResumen . ".equipo
                    FROM " . $this->tablaResumen . " 
                        INNER JOIN " . $this->tabla . " ON " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo 
                        AND " . $this->campoIgualar . "
                    WHERE " . $this->tablaResumen . "." . $this->campo . " AND " . $this->tablaResumen . ".familia LIKE '%Office%'
                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version
                    ORDER BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version) tabla");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){           
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalOfficeEdicion($opcion, $id, $edicion){
        try{            
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = true;
            $this->edicionValor = $edicion;
            $this->setDatos();
            
            if($edicion == "Professional"){
                $edicion1 = $this->obtenerCampoResumenProfesional($this->tablaResumen);
            }
            else{
                $edicion1 = " AND " . $this->tablaResumen . ".edicion LIKE :edicion ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT " . $this->tablaResumen . ".equipo
                    FROM " . $this->tablaResumen . " 
                        INNER JOIN " . $this->tabla . " ON " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo 
                        AND " . $this->campoIgualar . "
                    WHERE " . $this->tablaResumen . "." . $this->campo . " AND " . $this->tablaResumen . ".familia LIKE '%Office%' " . $edicion1 . "
                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version
                    ORDER BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version) tabla");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalOffice365($opcion, $id){
        try{            
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->setDatos();
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT " . $this->tablaResumen . ".equipo
                    FROM " . $this->tablaResumen . " 
                        INNER JOIN " . $this->tabla . " ON " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo 
                        AND " . $this->campoIgualar . "
                    WHERE " . $this->tablaResumen . "." . $this->campo . " AND " . $this->tablaResumen . ".familia LIKE '%Office%' AND " . $this->tablaResumen . ".version = 365 
                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version
                    ORDER BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version) tabla");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalVisioProjectEdicion($opcion, $id, $edicion){
        try{            
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = true;
            $this->edicionValor = $edicion;
            $this->setDatos();
            
            if($edicion == "Professional"){
                $edicion1 = $this->obtenerCampoResumenProfesional($this->tablaResumen);
            }
            else{
                $edicion1 = " AND " . $this->tablaResumen . ".edicion LIKE :edicion ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT " . $this->tablaResumen . ".equipo
                    FROM " . $this->tablaResumen . " 
                        INNER JOIN " . $this->tabla . " ON " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo 
                        AND " . $this->campoIgualar . "
                    WHERE " . $this->tablaResumen . "." . $this->campo . " AND " . $this->tablaResumen . ".familia IN ('Visio', 'Project') " . $edicion1 . " AND NOT " . $this->tablaResumen . ".edicion LIKE '%365%'
                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version
                    ORDER BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version) tabla");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function windowServer($opcion, $id, $tipo = "Fisico"){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
            $this->conexion();
            $sql = $this->conn->prepare('SELECT "" AS cluster,
                    "" AS host,
                    ' . $this->tabla . '.equipo AS equipo,
                    "Windows Server" AS familia,
                    ' . $this->tabla . '.edicion,
                    ' . $this->tabla . '.version,
                    "No" AS MSDN,
                    IF(' . $this->tablaTipoEquipo . '.modelo LIKE "%Virtual%", "Virtual", "Fisico") AS tipo,
                    "" AS centroCosto,
                    IFNULL(' . $this->tablaProcesadores . '.cpu, 0) AS cpu,
                    IFNULL(' . $this->tablaProcesadores . '.cores, 0) AS cores,
                    0 AS licSrv,
                    0 As licProc,
                    0 AS licCore
                FROM ' . $this->tabla . '
                        LEFT JOIN ' . $this->tablaTipoEquipo . ' ON ' . $this->tabla . '.equipo = ' . $this->tablaTipoEquipo . '.host_name AND ' . $this->campoIgualarTipoEquipo . '
                        LEFT JOIN ' . $this->tablaProcesadores . ' ON ' . $this->tablaTipoEquipo . '.host_name = ' . $this->tablaProcesadores . '.host_name AND ' . $this->tablaProcesadores . '.cpu > 0 AND ' . $this->campoIgualarProcesadores . '
                    WHERE ' . $this->tabla . '.os LIKE "%Windows Server%" AND ' . $this->tabla . '.' . $this->campo . ' AND (IF(' . $this->tablaTipoEquipo . '.modelo LIKE "%Virtual%", "Virtual", "Fisico")) = "' . $tipo . '" AND ' . $this->tabla . '.rango IN (1, 2, 3) AND ' . $this->tabla . '.errors = "Ninguno"
                    GROUP BY ' . $this->tabla . '.equipo, ' . $this->tablaProcesadores . '.cpu, ' . $this->tablaProcesadores . '.cores');
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function MSCloudDB($opcion, $id, $tipo, $version){
        $this->id =  $id;
        $this->opcion = $opcion;
        $this->edicion = false;
        $this->setDatos();
            
        if($version == "newer"){
            $where = " AND " . $this->tablaResumen . ".version > 2008 ";
        } else{
            $where = " AND (" . $this->tablaResumen . ".version <= 2008 OR " . $this->tablaResumen . ".version = '' OR " . $this->tablaResumen . ".version IS NULL) ";
        }
        
        if($tipo == "basic"){
            $where .= " AND " . $this->tablaResumen . ".edicion LIKE '%Standard%' AND " . $this->tablaProcesadores . ".cores <= 8 ";
        } else if($tipo == "standard"){
            $where .= " AND " . $this->tablaResumen . ".edicion LIKE '%Standard%' AND " . $this->tablaProcesadores . ".cores > 8 ";
        } else if($tipo == "premium"){
            $where .= " AND " . $this->tablaResumen . ".edicion LIKE '%Enterprise%' ";
        }
       
        $query = "SELECT COUNT(tabla.familia) AS cantidad
            FROM (SELECT " . $this->tablaResumen . ".familia
                FROM (SELECT " . $this->tablaResumen . ".idDiagnostic,
                        " . $this->tablaResumen . ".equipo,
                        " . $this->tablaResumen . ".familia,
                        " . $this->tablaResumen . ".edicion,
                        MAX(" . $this->tablaResumen . ".version) AS version
                    FROM " . $this->tablaResumen . "
                    WHERE " . $this->tablaResumen . "." . $this->campo . " AND " . $this->tablaResumen . ".familia = 'SQL Server'
                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version) " . $this->tablaResumen . "
                        INNER JOIN " . $this->tablaProcesadores . " ON " . $this->campoIgualarProcesadores1 . "
                        AND " . $this->tablaResumen . ".equipo = " . $this->tablaProcesadores . ".host_name
                WHERE " . $this->tablaResumen . "." . $this->campo . " AND " . $this->tablaResumen . ".familia = 'SQL Server' " . $where . " 
                GROUP BY " . $this->tablaResumen . ".equipo) tabla";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function MSCloudVM($opcion, $id, $tipo, $version){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();
        
            if($version == "newer"){
                $where = " AND " . $this->tabla . ".version > 2008 ";
            } else if($version == "older"){
                $where = " AND " . $this->tabla . ".version <= 2008 ";
            }
            
            if($tipo == "basic"){
                $where1 = " AND " . $this->tablaProcesadores . ".cores <= 8 ";
            } else if($tipo == "low"){
                $where1 = " AND " . $this->tablaProcesadores . ".cores > 8 AND " . $this->tablaProcesadores . ".cores <= 16 ";
            } else if($tipo == "standard"){
                $where1 = " AND " . $this->tablaProcesadores . ".cores > 16 ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT " . $this->tabla . ".equipo
                    FROM " . $this->tabla . "
                        INNER JOIN " . $this->tablaTipoEquipo . " ON " . $this->tabla . ".equipo = " . $this->tablaTipoEquipo . ".host_name
                        AND " . $this->campoIgualarTipoEquipo . "
                        INNER JOIN " . $this->tablaProcesadores . " ON " . $this->tablaTipoEquipo . ".host_name = " . $this->tablaProcesadores . ".host_name
                        AND " . $this->tablaProcesadores . ".cpu > 0 AND " . $this->campoIgualarProcesadores . "
                    WHERE " . $this->tabla . "." . $this->campo . " AND " . $this->tabla . ".os LIKE '%Windows Server%' " . $where . "
                    AND " . $this->tablaTipoEquipo . ".modelo LIKE '%Virtual%' " . $where1 . " AND " . $this->tabla . ".rango IN (1, 2, 3)
                    AND " . $this->tabla . ".errors = 'Ninguno'
                    GROUP BY " . $this->tabla . ".equipo) tabla");
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function productosSinSoporte($opcion, $id, $tipo = "todo"){
        $this->id =  $id;
        $this->opcion = $opcion;
        $this->edicion = false;
        $this->setDatos();
            
        $this->conexion();
        if($tipo == "cliente"){
            $where = " AND " . $this->tablaResumen . ".familia NOT IN ('SQL Server') ";
            $where1 = " AND " . $this->tabla . ".familia = 'Windows' ";
        } else if ($tipo == "servidor"){
            $where = " AND " . $this->tablaResumen . ".familia IN ('SQL Server') ";
            $where1 = " AND " . $this->tabla . ".familia = 'Windows Server' ";
        } else{
            $where = "";
            $where1 = "";
        }
        
        $query = "SELECT COUNT(tabla.id) AS cantidad
            FROM (SELECT " . $this->tablaResumen . ".id
                FROM " . $this->tablaResumen . "
                    INNER JOIN detalleMaestra ON " . $this->tablaResumen . ".familia = detalleMaestra.descripcion AND
                    " . $this->tablaResumen . ".edicion = detalleMaestra.campo1 AND " . $this->tablaResumen . ".version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE " . $this->tablaResumen . "." . $this->campo . $where . "
                GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version
                
                UNION 
                
                SELECT " . $this->tabla . ".id
                FROM " . $this->tabla . "
                    INNER JOIN detalleMaestra ON " . $this->tabla . ".familia = detalleMaestra.descripcion AND
                    " . $this->tabla . ".edicion = detalleMaestra.campo1 AND " . $this->tabla . ".version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE " . $this->tabla . "." . $this->campo . $where1 . "
                GROUP BY " . $this->tabla . ".equipo, " . $this->tabla . ".familia, " . $this->tabla . ".edicion, " . $this->tabla . ".version) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($this->array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function productosSinSoporte1($idDiagnostic, $opcion, $tipo = "todo"){
        $this->conexion();
        if($tipo == "cliente"){
            $where = " AND " . $this->tablaResumen . ".familia NOT IN ('SQL Server') ";
            $where1 = " AND " . $this->tabla . ".familia = 'Windows' ";
        }else if ($opcion == "servidor"){
            $where = " AND " . $this->tablaResumen . ".familia IN ('SQL Server') ";
            $where1 = " AND " . $this->tabla . ".familia = 'Windows Server' ";
        }else{
            $where = "";
            $where1 = "";
        }
        
        $this->conexion();
        $query = "SELECT tabla.familia,
                tabla.edicion,
                    COUNT(tabla.id) AS cantidad
            FROM (SELECT " . $this->tablaResumen . ".familia,
                    " . $this->tablaResumen . ".edicion,
                    " . $this->tablaResumen . ".id
                FROM " . $this->tablaResumen . "
                    INNER JOIN detalleMaestra ON " . $this->tablaResumen . ".familia = detalleMaestra.descripcion AND
                    " . $this->tablaResumen . ".edicion = detalleMaestra.campo1 AND " . $this->tablaResumen . ".version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE " . $this->tablaResumen . "." . $this->campo . $where . "
                GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version
                
                UNION 
                
                SELECT " . $this->tabla . ".familia,
                    " . $this->tabla . ".edicion,
                    " . $this->tabla . ".id
                FROM " . $this->tabla . "
                    INNER JOIN detalleMaestra ON " . $this->tabla . ".familia = detalleMaestra.descripcion AND
                    " . $this->tabla . ".edicion = detalleMaestra.campo1 AND " . $this->tabla . ".version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE " . $this->tabla . "." . $this->campo . $where1 . "
                GROUP BY " . $this->tabla . ".equipo, " . $this->tabla . ".familia, " . $this->tabla . ".edicion, " . $this->tabla . ".version) tabla
            GROUP BY tabla.familia";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function setDatos(){

        if($this->opcion == "microsoft"){
            $this->tabla = "detalles_equipo2";
            $this->tablaResumen = "resumen_office2";
            $this->tablaTipoEquipo = "consolidadoTipoEquipo";
            $this->tablaProcesadores = "consolidadoProcesadores";
            $this->campo = 'cliente = :cliente ';
            $this->campoIgualar = $this->tablaResumen . ".cliente = " . $this->tabla . ".cliente ";
            $this->campoIgualarTipoEquipo = $this->tabla . ".cliente = " . $this->tablaTipoEquipo . ".cliente ";
            $this->campoIgualarProcesadores = $this->tabla . ".cliente = " . $this->tablaProcesadores . ".cliente ";
            $this->campoIgualarProcesadores1 = $this->tablaResumen . ".cliente = " . $this->tablaProcesadores . ".cliente ";
            $this->array = array(':cliente'=>$this->id);
        } else if($this->opcion == "Diagnostic"){
            $this->tabla = "detalles_equipoSAMDiagnostic";
            $this->tablaResumen = "resumen_SAMDiagnostic";
            $this->tablaTipoEquipo = "consolidadoTipoEquipoSAMDiagnostic";
            $this->tablaProcesadores = "consolidadoProcesadoresSAMDiagnostic";
            $this->campo = 'idDiagnostic = :idDiagnostic ';
            $this->campoIgualar = $this->tablaResumen . ".idDiagnostic = " . $this->tabla . ".idDiagnostic ";
            $this->campoIgualarTipoEquipo = $this->tabla . ".idDiagnostic = " . $this->tablaTipoEquipo . ".idDiagnostic ";
            $this->campoIgualarProcesadores = $this->tabla . ".idDiagnostic = " . $this->tablaProcesadores . ".idDiagnostic ";
            $this->campoIgualarProcesadores1 = $this->tablaResumen . ".idDiagnostic = " . $this->tablaProcesadores . ".idDiagnostic ";
            $this->array = array(':idDiagnostic'=>$this->id);
        } else if($this->opcion == "Cloud"){
            $this->tabla = "detalles_equipoMSCloud";
            $this->tablaResumen = "resumen_MSCloud";
            $this->tablaTipoEquipo = "consolidadoTipoEquipoMSCloud";
            $this->tablaProcesadores = "consolidadoProcesadoresMSCloud";
            $this->campo = 'idDiagnostic = :idDiagnostic ';
            $this->campoIgualar = $this->tablaResumen . ".idDiagnostic = " . $this->tabla . ".idDiagnostic ";
            $this->campoIgualarTipoEquipo = $this->tabla . ".idDiagnostic = " . $this->tablaTipoEquipo . ".idDiagnostic ";
            $this->campoIgualarProcesadores = $this->tabla . ".idDiagnostic = " . $this->tablaProcesadores . ".idDiagnostic ";
            $this->campoIgualarProcesadores1 = $this->tablaResumen . ".idDiagnostic = " . $this->tablaProcesadores . ".idDiagnostic ";
            $this->array = array(':idDiagnostic'=>$this->id);
        } else if($this->opcion == "appEscaneo"){
            $this->tabla = "appDetalles_equipo";
            $this->tablaResumen = "appResumen_office";
            $this->tablaTipoEquipo = "";
            $this->tablaProcesadores = "";
            $this->campo = 'cliente = :cliente ';
            $this->campoIgualar = $this->tablaResumen . ".cliente = " . $this->tabla . ".cliente ";
            $this->campoIgualarTipoEquipo = ""; //$this->tabla . ".idDiagnostic = " . $this->tablaTipoEquipo . ".idDiagnostic ";
            $this->campoIgualarProcesadores = ""; //$this->tabla . ".idDiagnostic = " . $this->tablaProcesadores . ".idDiagnostic ";
            $this->campoIgualarProcesadores1 = ""; //$this->tablaResumen . ".idDiagnostic = " . $this->tablaProcesadores . ".idDiagnostic ";
            $this->array = array(':cliente'=>$this->id);
        }
        
        if($this->edicion == true){
            $this->array[":edicion"] = "%" . $this->edicionValor . "%";
        }
    }
    
    function listaProductosDesusoAsignacion($opcion, $id, $familia, $os, $asignacion = "", $asignaciones = array()){
        $this->id =  $id;
        $this->opcion = $opcion;
        $this->edicion = false;
        $this->setDatos();     
        $where = "";
        
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $this->array[":asignacion"] = $asignacion;
        } else{
            if($opcion != "Cloud" && $opcion != "Diagnostic"){
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
        }
        
        $this->array[":familia"] = $familia;
        $this->array[":os"] = $os;
        
        $query = "SELECT " . $this->tabla . ".id,
                " . $this->tabla . ".equipo,
                " . $this->tablaResumen . ".edicion AS familia,
                'Desuso' AS usabilidad
            FROM " . $this->tabla . "
                LEFT JOIN " . $this->tablaResumen . " ON " . $this->tabla . ".equipo = " . $this->tablaResumen . ".equipo AND "
                . $this->campoIgualar . " " 
                . "AND " . $this->tablaResumen . ".familia = :familia
            WHERE " . $this->tabla . "." . $this->campo . " AND 
            rango NOT IN (1, 2, 3) AND " . $this->tabla . ".familia = :os AND " . $this->tablaResumen . ".edicion != '' " . $where . "
            GROUP BY " . $this->tabla . ".id";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function softwareDuplicateGeneral($opcion, $id, $asignacion = "", $asignaciones = array(), $tipo = null){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();     
            
            $where = "";
            if($asignacion != ""){
                $where = " AND detalles_equipo2.asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                if($opcion != "Cloud" && $opcion != "Diagnostic"){
                    $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                    $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") ";
                }
            }
            
            $where1 = "";
            if ($tipo != null && $tipo == "cliente"){
                $where1 = " INNER JOIN " . $this->tabla . " ON " . $this->campoIgualar . " AND 
                " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo AND " . $this->tabla . ".tipo = 1 ";
            } else if($tipo != null && $tipo == "servidor") {
                $where1 = " INNER JOIN " . $this->tabla . " ON " . $this->campoIgualar . " AND 
                " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo AND " . $this->tabla . ".tipo = 2 ";
            } else if($tipo == null) {
                $where1 = " INNER JOIN " . $this->tabla . " ON " . $this->campoIgualar . " AND 
                " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo AND " . $this->tabla . ".tipo IN (1, 2) ";
            }
        
            $this->conexion();
            $sql = $this->conn->prepare("SELECT " . $this->tablaResumen . ".equipo,
                    IF(" . $this->tabla . ".tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    " . $this->tablaResumen . ".familia,
                    " . $this->tablaResumen . ".edicion,
                    " . $this->tablaResumen . ".version
                FROM " . $this->tablaResumen . "
                    INNER JOIN (SELECT tabla.equipo,
                                    tabla.familia,
                                    tabla.edicion,
                                    tabla.version
                                FROM (SELECT " . $this->tablaResumen . ".equipo,
                                        " . $this->tablaResumen . ".familia,
                                        " . $this->tablaResumen . ".edicion,
                                        " . $this->tablaResumen . ".version
                                    FROM " . $this->tablaResumen . "
                                    WHERE " . $this->tablaResumen . "." . $this->campo . " 
                                    GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion,
                                    " . $this->tablaResumen . ".version) tabla
                                GROUP BY tabla.equipo, tabla.familia
                                HAVING COUNT(tabla.equipo) > 1) tabla1 ON " . $this->tablaResumen . ".equipo = tabla1.equipo AND
                                " . $this->tablaResumen . ".familia = tabla1.familia
                    " . $where1 . "
                WHERE " . $this->tablaResumen . "." . $this->campo . " " . $where . "
                GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version");
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function erroneusGeneral($opcion, $id, $asignacion = "", $asignaciones = array()){
        try{
            $this->id =  $id;
            $this->opcion = $opcion;
            $this->edicion = false;
            $this->setDatos();     
            $where = "";
            $query = "";

            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $this->array[":asignacion"] = $asignacion;
            } else{
                if($opcion != "Cloud" && $opcion != "Diagnostic"){
                    $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                    $where = " AND asignacion IN (" . $asignacion . ") ";
                }
            }
        
            $this->conexion();
            $sql = $this->conn->prepare($query .= "SELECT " . $this->tablaResumen . ".equipo,
                    IF(" . $this->tabla . ".tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    " . $this->tablaResumen . ".familia,
                    " . $this->tablaResumen . ".edicion,
                    " . $this->tablaResumen . ".version
                FROM " . $this->tabla . "
                    INNER JOIN " . $this->tablaResumen . " ON " . $this->campoIgualar . "
                    AND " . $this->tabla . ".equipo = " . $this->tablaResumen . ".equipo AND
                    (" . $this->tablaResumen . ".familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center') 
                    OR " . $this->tablaResumen . ".edicion LIKE '%Server%') AND " . $this->tablaResumen . ".edicion NOT LIKE '%Endpoint Protection%' 
                    AND " . $this->tablaResumen . ".edicion NOT LIKE '%Express Edition%' AND " . $this->tablaResumen . ".edicion NOT LIKE '%MUI%' 
                    AND " . $this->tablaResumen . ".edicion NOT LIKE '%Update%' 
                WHERE " . $this->tabla . "." . $this->campo . " AND " . $this->tabla . ".tipo = 1 " . $where . "
                GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version "
                . ""
                . " UNION "
                . ""
                . "SELECT " . $this->tablaResumen . ".equipo,
                    IF(" . $this->tabla . ".tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    " . $this->tablaResumen . ".familia,
                    " . $this->tablaResumen . ".edicion,
                    " . $this->tablaResumen . ".version
                FROM " . $this->tabla . "
                    INNER JOIN " . $this->tablaResumen . " ON " . $this->campoIgualar . "
                    AND " . $this->tabla . ".equipo = " . $this->tablaResumen . ".equipo AND
                    (" . $this->tablaResumen . ".familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                    AND NOT " . $this->tablaResumen . ".edicion LIKE '%Server%') AND " . $this->tablaResumen . ".edicion NOT LIKE '%MUI%' "
                    . "AND " . $this->tablaResumen . ".edicion NOT LIKE '%Update%'
                WHERE " . $this->tabla . "." . $this->campo . " AND " . $this->tabla . ".tipo = 2 " . $where . "
                GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion, " . $this->tablaResumen . ".version");
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Asignacion($opcion, $id, $familia, $edicion, $asignacion = "", $asignaciones = array(), $dup = "Si") {
        $this->id =  $id;
        $this->opcion = $opcion;
        $this->edicion = false;
        $this->setDatos();
        
        $this->conexion();
        $where = "";
        $this->array[':familia'] = '%' . $familia . '%';
        $this->array[':edicion'] = '%' . $edicion . '%';
        
        if($asignacion == ""){
            if($opcion != "Cloud" && $opcion != "Diagnostic" && $opcion != "appEscaneo"){
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ")";
            }
        } else{
            $where = " AND asignacion = :asignacion ";
            $ths->array[":asignacion"] = $asignacion;
        }
        
        if($edicion == "Professional"){
            $edicion = $this->obtenerCampoResumenProfesional($this->tablaResumen);
        }
        else{
            $edicion = " AND " . $this->tablaResumen . ".edicion LIKE :edicion ";
        }
        
        $groupOrderBy = "";
        $version = $this->tablaResumen . ".version, ";
        if($dup == "Si"){
            $groupOrderBy = ", " . $this->tablaResumen . ".version";
            $version = "MAX(" . $this->tablaResumen . ".version) AS version, ";
        }

        $query = "SELECT " . $this->tablaResumen . ".id,
                        " . $this->tablaResumen . ".equipo,
                        IF(" . $this->tabla . ".os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        " . $this->tablaResumen . ".familia,
                        " . $this->tablaResumen . ".edicion,
                        " . $version . "
                        DATE_FORMAT(MAX(" . $this->tablaResumen . ".fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN " . $this->tabla . ".rango = 1 THEN
                                 'En Uso'
                            WHEN " . $this->tabla . ".rango = 2 OR " . $this->tabla . ".rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
		FROM " . $this->tablaResumen . " 
                    INNER JOIN " . $this->tabla . " ON " . $this->tablaResumen . ".equipo = " . $this->tabla . ".equipo 
                    AND " . $this->campoIgualar . " " . $where . "
		WHERE " . $this->tablaResumen . "." . $this->campo . " AND " . $this->tablaResumen . ".familia LIKE :familia " . $edicion . "
                GROUP BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion" . $groupOrderBy . "
                ORDER BY " . $this->tablaResumen . ".equipo, " . $this->tablaResumen . ".familia, " . $this->tablaResumen . ".edicion" . $groupOrderBy;
       
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($this->array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}
