<?php
class clase_nadd extends General{
    public $error = null;

    function listar_datos($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idCorreo, serialHDD, LAD, DATE_FORMAT(fechaCarga, "%d/%m/%Y") AS fechaCarga
                FROM appEscaneo
                WHERE cliente = :cliente
                ORDER BY serialHDD');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos_correo($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, correo
                FROM appEscaneoCorreo
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function borrarArchivo($archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE appEscaneo SET LAD = null WHERE LAD = :archivo');
            $sql->execute(array(':archivo'=>$archivo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function borrarCorreos($correo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM appEscaneoCorreo WHERE id = :correo');
            $sql->execute(array(':correo'=>$correo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarAppEscaneo($cliente, $correo, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM appEscaneo WHERE cliente = :cliente AND idCorreo = :correo AND serialHDD = :serial');
            $sql->execute(array(':cliente'=>$cliente, ':correo'=>$correo, ':serial'=>$serial));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    function listaManageCustomer($data){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                                         FROM clientes_nadd WHERE reseller_id=:reseller');
            $sql->execute(array(':reseller'=>$data));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }    
    }
    function EditarCustormeManager($editcustom){
        //print_r($editcustom);exit;
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes_nadd SET nombreEmpresa = :nombreEmpresa, correo = :correo WHERE id = :ID');
            $sql->execute(array(':nombreEmpresa'=>$editcustom['editCustomer'],':correo'=>$editcustom['editEmail'],':ID'=>$editcustom['editID']));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    function VerificarEmailEditar($data){
        //print_r($data);exit;
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                                         FROM clientes_nadd WHERE correo=:reseller AND id!=:id');
        //print_r($sql);exit;
            $sql->execute(array(':reseller'=>$data["editEmail"],":id"=>$data["editID"]));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        } 
    }
    function VerificarEmail($data){
        //print_r($data);exit;
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                                         FROM clientes_nadd WHERE correo=:reseller');
            $sql->execute(array(':reseller'=>$data["CrearEmail"]));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }    
    }
    function CrearCustormeManager($data){
        #print_r($data);exit;
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO clientes_nadd (nombreEmpresa,correo,fechaRegistro,reseller_id) VALUES (:nombreEmpresa,:correo,:fechaRegistro,:reseller_id)');
            $sql->execute(array( ':nombreEmpresa'=>$data['CrearCustomer'],':correo'=>$data['CrearEmail'],':fechaRegistro'=>date('Y-m-d h:m:s'),":reseller_id"=>$data["client_NADD_id"]));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    function CrearCustormeManagerEmail($data){
        print_r($data);exit;
        foreach($data as $row){
            $todo=explode('[",]',$row);
            
        }
        $todo=explode('[",]',$data["correosEnviar"]);
        

        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO clientes_nadd_correos (clase_nadd_id,correo) VALUES (:clase_nadd_id,:correo)');
            $sql->execute(array( ':clase_nadd_id'=>$data['customer'],':correo'=>$data['correosEnviar']));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    function eliminarCustormeManager($data){
        //print_r($data);exit;
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM clientes_nadd WHERE id = :ID ');
            $sql->execute(array( ':ID'=>$data['editID']));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}