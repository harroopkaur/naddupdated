<?php
class POC extends General{ 
    function listar() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM poc
            ORDER BY fecha DESC, nombre ASC');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function nombreArchivo($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM poc
            WHERE id = :id
            ORDER BY fecha DESC, nombre ASC');
            $sql->execute(array('id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminar($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM poc WHERE id = :id');
            $sql->execute(array('id'=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}