<?php
class filepcsOracle extends General{
    public  $error;
    public  $listado;

    // Insertar 
    function insertar($cliente, $empleado, $dn, $objectclass, $cn, $useracountcontrol, $lastlogon, $pwdlastset, $os, $lastlogontimes) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO filepcs_oracle (cliente, empleado, dn, objectclass, cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) VALUES '
            . '(:cliente, :empleado, :dn, :objectclass, :cn, :useracountcontrol, :lastlogon, :pwdlastset, :os, :lastlogontimes)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dn'=>$dn, ':objectclass'=>$objectclass, ':cn'=>$cn, ':useracountcontrol'=>$useracountcontrol, ':lastlogon'=>$lastlogon, ':pwdlastset'=>$pwdlastset, ':os'=>$os, ':lastlogontimes'=>$lastlogontimes));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    //Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM filepcs_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' =>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM filepcs_oracle WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf2/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}