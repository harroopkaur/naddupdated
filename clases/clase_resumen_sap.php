<?php
class ResumenSAP extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $compSoft, $familia, $edicion, $version) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumenServidorSap (cliente, empleado, equipo, compSoft, familia, edicion, version) '
            . 'VALUES (:cliente, :empleado, :equipo, :compSoft, :familia, :edicion, :version)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':compSoft'=>$compSoft, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumenServidorSap WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{       
            $this->conexion();
            $sql = $this->conn->prepare("SELECT id, 
                    equipo,
                    compSoft AS componente,
                    familia,
                    edicion,
                    version,
                    '' AS diasUltAcceso
                FROM resumenServidorSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND edicion LIKE :edicion
                
                UNION
                
                SELECT id, 
                    usuarios AS equipo,
                    componente,
                    familia,
                    edicion,
                    version,
                    diasUltAcceso
                FROM resumenUsuariosModuloSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND edicion LIKE :edicion");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Todos($cliente, $empleado, $familia) {        
        try{      
            $this->conexion();
            $sql = $this->conn->prepare("SELECT id, 
                    equipo,
                    compSoft AS componente,
                    familia,
                    edicion,
                    version,
                    '' AS diasUltAcceso,
                    '' AS departamento,
                    '' AS cargo
                FROM resumenServidorSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia
                
                UNION
                
                SELECT id, 
                    usuarios AS equipo,
                    componente,
                    familia,
                    edicion,
                    version,
                    diasUltAcceso,
                    departamento,
                    cargo
                FROM resumenUsuariosModuloSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Total($cliente, $empleado, $familia, $edicion) {        
        try{       
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.id) AS total
                FROM (SELECT id
                FROM resumenServidorSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND edicion LIKE :edicion
                
                UNION
                
                SELECT id
                FROM resumenUsuariosModuloSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND edicion LIKE :edicion) tabla");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            $row = $sql->fetch();
            return $row["total"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("total"=>0);
        }
    }
    
    function listar_datos6Paginado($cliente, $empleado, $familia, $edicion, $inicio, $limite) {        
        try{       
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.id, 
                    tabla.equipo,
                    tabla.componente,
                    tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    tabla.diasUltAcceso,
                    tabla.departamento,
                    tabla.cargo
                FROM (SELECT id, 
                    equipo,
                    compSoft AS componente,
                    familia,
                    edicion,
                    version,
                    '' AS diasUltAcceso,
                    '' AS departamento,
                    '' AS cargo
                FROM resumenServidorSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND edicion LIKE :edicion
                
                UNION
                
                SELECT id, 
                    usuarios AS equipo,
                    componente,
                    familia,
                    edicion,
                    version,
                    diasUltAcceso,
                    departamento,
                    cargo
                FROM resumenUsuariosModuloSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND edicion LIKE :edicion) tabla
                LIMIT " . $inicio . ", " . $limite);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos5Sam($cliente, $empleado, $familia) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT equipo,
                    compSoft AS componente,
                    familia,
                    edicion,
                    version,
                    '' AS diasUltAcceso
                FROM resumenServidorSapSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                
                UNION
                
                SELECT usuarios AS equipo,
                    componente,
                    familia,
                    edicion,
                    version,
                    diasUltAcceso
                FROM resumenUsuariosModuloSapSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos5SamArchivo($archivo, $familia) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT equipo,
                    compSoft AS componente,
                    familia,
                    edicion,
                    version,
                    '' AS diasUltAcceso
                FROM resumenServidorSapSam
                WHERE archivo = :archivo AND familia = :familia
                
                UNION
                
                SELECT usuarios AS equipo,
                    componente,
                    familia,
                    edicion,
                    version,
                    diasUltAcceso
                FROM resumenUsuariosModuloSapSam
                WHERE archivo = :archivo AND familia = :familia");
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{   
            $this->conexion();
            $query = "SELECT familia,
                    edicion,
                    version,
                    COUNT(*) AS cantidad
                FROM resumenServidorSap
                WHERE cliente = :cliente AND empleado = :empleado AND resumenServidorSap.familia = :familia AND edicion LIKE :edicion
                GROUP BY familia, edicion
                
                UNION

                SELECT
                    familia,
                    edicion,
                    version,
                    COUNT(*) AS cantidad
                FROM resumenUsuariosModuloSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND edicion LIKE :edicion
                GROUP BY familia, edicion";
            /*$sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenServidorSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version

                UNION

                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenUsuariosModuloSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");*/
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoTodos($cliente, $empleado, $familia) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenServidorSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion

                UNION

                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenUsuariosModuloSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion");            
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}