<?php
class clase_validar_procesoLAE_general extends General{
    private $temp;
    public $nombreArchivo;
    
    //inicio variables LAE
    public $iDN;
    public $iobjectClass;
    public $icn;
    public $iuserAccountControl;
    public $ilastLogon;
    public $ipwdLastSet;
    public $ioperatingSystem;
    public $ioperatingSystemVersion;
    public $ilastLogonTimestamp;
    public $iWhenCreated;
    //fin variables LAE
    
    private $opcionDespliegue;
    private $opcion;
    public $procesarLAE;
    public $archivoLAE;
    
    function validar_archivo($name, $ext){
        $error = 0;
        $nombre = $name;
     
        // Validaciones
        if($nombre != ""){
            $extension = explode(".", $nombre);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != $ext) && ($extension[$long] != strtoupper($ext))) { 
                $error = 1; 
            }  
        }else{
            $error = 2;	
        }
        
        return $error;
    }   
   
    function validarLAE($archivoLAE, $opcionDespliegue, $opcion){
        $error = 0;
        $this->opcionDespliegue = $opcionDespliegue;
        $this->opcion = $opcion;
        
        if($this->obtenerSeparadorUniversal($archivoLAE, 1, 10) === true){
            if (($fichero = fopen($archivoLAE, "r")) !== false) {
                $this->iDN = $this->iobjectClass = $this->icn = $this->iuserAccountControl = $this->ilastLogon = 
                $this->ipwdLastSet = $this->ioperatingSystem = $this->ioperatingSystemVersion = $this->ilastLogonTimestamp = 
                $this->iWhenCreated = -1;
                $this->procesarLAE = true;
                $this->cicloCabecera($fichero, 1, 1);
                fclose($fichero);

                $error = $this->validarResultadosLAE();
            }
        } else{
            //$error = 10;
            $this->procesarLAE = false;
        }
        
        return $error;
    }
    
    //inicio proceso general de validacion
    function cicloCabecera($fichero, $fila, $opcion){
        $i = 1;
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {
            if($i == $fila){
                $this->ejecutarValidacion($opcion, $datos);
            } else if($i > $fila){
                break;
            }

            $i++;
        }
    }
    
    function ejecutarValidacion($opcion, $datos){ 
        //opcion = 1 AddRemove
        if($opcion == 1){
            $this->validCabLAE($datos);
        }
    }
    
    function validCabLAE($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->iDN = $this->asignarColumnasi($this->iDN, $datos[$k], $k, "DN");
            $this->iobjectClass = $this->asignarColumnasi($this->iobjectClass, $datos[$k], $k, "objectClass");
            $this->icn = $this->asignarColumnasi($this->icn, $datos[$k], $k, "cn");
            $this->iuserAccountControl = $this->asignarColumnasi($this->iuserAccountControl, $datos[$k], $k, "userAccountControl");
            $this->ilastLogon = $this->asignarColumnasi($this->ilastLogon, $datos[$k], $k, "lastLogon");
            $this->ipwdLastSet = $this->asignarColumnasi($this->ipwdLastSet, $datos[$k], $k, "pwdLastSet");
            $this->ioperatingSystem = $this->asignarColumnasi($this->ioperatingSystem, $datos[$k], $k, "operatingSystem");
            $this->ioperatingSystemVersion = $this->asignarColumnasi($this->ioperatingSystemVersion, $datos[$k], $k, "operatingSystemVersion");
            $this->ilastLogonTimestamp = $this->asignarColumnasi($this->ilastLogonTimestamp, $datos[$k], $k, "lastLogonTimestamp");
            $this->iWhenCreated = $this->asignarColumnasi($this->iWhenCreated, $datos[$k], $k, "whenCreated");
        }
    }
    
    function validarResultadosLAE(){
        $error = 0;
        if($this->iDN == -1 && $this->iobjectClass == -1 &&  $this->icn == -1 && $this->iuserAccountControl == -1 && 
        $this->ilastLogon == -1 && $this->ipwdLastSet == -1 && $this->ioperatingSystem == -1 && $this->ioperatingSystemVersion == -1 && 
        $this->ilastLogonTimestamp == -1 && $this->iWhenCreated == -1){
            $this->procesarLAE = false;
        } else if($this->iDN == -1 || $this->iobjectClass == -1 ||  $this->icn == -1 || $this->iuserAccountControl == -1 || 
        $this->ilastLogon == -1 || $this->ipwdLastSet == -1 || $this->ioperatingSystem == -1 || $this->ioperatingSystemVersion == -1 || 
        $this->ilastLogonTimestamp == -1 || $this->iWhenCreated == -1){
            $error = 3;
        }
        
        return $error;
    }
}
