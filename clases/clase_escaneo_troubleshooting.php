<?php
class escaneoTroubleshoot extends General{
    ########################################  Atributos  ########################################
    public $error;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $status, $errors) {
        $this->conexion();
        $query = "INSERT INTO escaneo_troubleshoot (cliente, empleado, equipo, status, errors) ";
        $query .= "VALUES (:cliente, :empleado, :equipo, :status, :errors)";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':status'=>$status, ':errors'=>$errors));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO escaneo_troubleshoot (cliente, empleado, equipo, status, errors) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        $this->conexion();
        $query = "DELETE FROM escaneo_troubleshoot WHERE cliente = :cliente AND empleado = :empleado";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM escaneo_troubleshoot WHERE cliente = :cliente AND empleado = :empleado ORDER BY equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo2($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM escaneo_troubleshoot WHERE cliente = :cliente AND empleado = :empleado GROUP BY equipo ORDER BY equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equiposNoDescubiertos($cliente, $empleado){
        $query = "SELECT *
            FROM escaneo_troubleshoot
            WHERE cliente = :cliente AND errors != 'Ninguno'
            GROUP BY equipo
            ORDER BY equipo";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
        
    }
}