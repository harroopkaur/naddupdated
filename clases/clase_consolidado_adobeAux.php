<?php
class consolidadoAdobeAux extends General{
    public  $lista        = array();
    public  $listaEdicion = array();
    public  $listaVersion = array();
    public  $edicion      = "";
    public  $version      = "";
    public  $error        = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $registro, $editor, $version, $fecha_instalacion, $software) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_adobeAux (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) VALUES '
            . '(:cliente, :empleado, :dato_control, :host_name, :registro, :editor, :version, :fecha_instalacion, :software)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control, ':host_name'=>$host_name, ':registro'=>$registro, ':editor'=>$editor, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion, ':software'=>$software));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_adobeAux (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_adobeAux WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductosAdobe($cliente, $empleado, $in = null, $out = null) {        
        try{
            $this->conexion();
            
            $agregar = "";
            if($in != null){
                $inAux = explode(",", $in);
            }
            
            if(trim($inAux[0]) == "Adobe After Effects" && isset($inAux[1]) && (trim($inAux[1] == "Presets" || trim($inAux[1] == "Third")))){
                $agregar .= " AND (";
            
                for($i = 0; $i < count($inAux); $i++){
                    if($i > 0){
                        $agregar .= " AND ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            }else {
                $agregar .= " AND (";
            
                for($i = 0; $i < count($inAux); $i++){
                    if($i > 0){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            }
                
            $quitar = "";
            if($out != ""){
                $outAux = explode(",", $out);
            
                $quitar = "WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_adobeAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (";
            
                for($i = 0; $i < count($outAux); $i++){
                    if($i > 0){
                        $quitar .= " OR ";
                    }

                    $quitar .= "sofware LIKE '%" . trim($outAux[$i]) . "%'"; 
                }
                $quitar .= ")";
            }
            
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				FROM consolidado_adobeAux
				WHERE cliente = :cliente AND empleado = :empleado " . $agregar . "
				ORDER BY fecha_instalacion DESC) AS tabla "
			. $quitar);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}