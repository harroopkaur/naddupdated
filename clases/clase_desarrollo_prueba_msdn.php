<?php
class desarrolloPruebaMSDN extends General{
    ########################################  Atributos  ########################################
    public $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($idDesarrolloPruebaVS, $cliente, $empleado, $equipo, $tipo, $familia, $edicion, $version, $fechaInstalacion, 
    $usuario = null, $equipoUsuario = null, $msdn = null) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO desarrolloPruebaMSDN (idDesarrolloPruebaVS, cliente, empleado, equipo, tipo, "
            . "familia, edicion, version, fechaInstalacion, usuario, equipoUsuario, msdn) VALUES (:idDesarrolloPruebaVS, :cliente, :empleado, "
            . ":equipo, :tipo, :familia, :edicion, :version, :fechaInstalacion, :usuario, :equipoUsuario, :msdn)");
            $sql->execute(array(':idDesarrolloPruebaVS'=>$idDesarrolloPruebaVS, ':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':tipo'=>$tipo, 
            ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':fechaInstalacion'=>$fechaInstalacion, 
            ':usuario'=>$usuario, ':equipoUsuario'=>$equipoUsuario, ':msdn'=>$msdn));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $usuario, $equipoUsuario, $msdn){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE desarrolloPruebaMSDN SET usuario = :usuario, equipoUsuario = :equipoUsuario, msdn = :msdn "
            . "WHERE id = :id");
            $sql->execute(array(':id'=>$id, ':usuario'=>$usuario, ':equipoUsuario'=>$equipoUsuario, ':msdn'=>$msdn));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM desarrolloPruebaMSDN WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarId($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM desarrolloPruebaMSDN WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existeDesarrolloPruebaMSDN($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad "
            . "FROM desarrolloPruebaMSDN WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeRegistroMSDN($cliente, $empleado, $equipo, $tipo, $familia, $edicion, $version, 
    $fechaInstalacion, $usuario, $equipoUsuario){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad "
            . "FROM desarrolloPruebaMSDN "
            . "WHERE cliente = :cliente AND empleado = :empleado AND equipo = :equipo "
            . "AND tipo = :tipo AND familia = :familia AND edicion = :edicion AND version = :version "
            . "AND fechaInstalacion = :fechaInstalacion AND usuario = :usuario AND equipoUsuario = :equipoUsuario");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':tipo'=>$tipo, 
            ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':fechaInstalacion'=>$fechaInstalacion, 
            ':usuario'=>$usuario, ':equipoUsuario'=>$equipoUsuario));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listadoDesarrolloPruebaMSDN($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM desarrolloPruebaMSDN WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    //inicio reporte SAM
    function listadoDesarrolloPruebaMSDNSam($archivo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM desarrolloPruebaMSDNSam WHERE archivo = :archivo");
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    //fin reporte SAM
}