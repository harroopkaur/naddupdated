<?php
class clase_dashboard extends General{
    function insertar($cliente, $presentacion, $posicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO dashboard (cliente, presentacion, numero) '
            . 'VALUES (:cliente, :presentacion, :posicion)');
            $sql->execute(array(':cliente'=>$cliente, ':presentacion'=>$presentacion, 
            ':posicion'=>$posicion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($cliente, $presentacion, $posicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE dashboard SET presentacion = :presentacion WHERE cliente = :cliente '
                    . 'AND numero = :posicion');
            $sql->execute(array(':cliente'=>$cliente, ':presentacion'=>$presentacion, 
            ':posicion'=>$posicion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($cliente, $presentacion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM dashboard WHERE cliente = :cliente '
                    . 'AND presentacion = :presentacion');
            $sql->execute(array(':cliente'=>$cliente, ':presentacion'=>$presentacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existePresentacion($cliente, $posicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad 
                FROM dashboard
                WHERE cliente = :cliente AND numero = :posicion');
            $sql->execute(array(':cliente'=>$cliente, ':posicion'=>$posicion));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function presentaciones($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * 
                FROM dashboard
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function contenedorGrafico($contGrafico1, $contGrafico2, $titulo, $fechaEscaneo, $alcance, $sobrante,
    $faltante, $fechaProxima, $asignaciones){
        $contenedor = "<div class='contenedorDashboard'>
                <div class='contVentanaDashboard'>
                    <div class='ventanaSuperior'>
                        <div class='contTituloDashborad float-lt'>" . $titulo . "</div>";
                        
                        if ($asignaciones != ""){
                            $contenedor .= "<div class='glyphIconMethod float-rt'>
                                <select class='glyphIconCombo' onchange=\"buscarGraficos('" . $titulo . "', this.value);\">"
                                . $asignaciones .   
                                "</select>
                            </div>";
                        }
                    $contenedor .= "</div>

                    <div class='ventanaInferior'>
                        <div class='contInteriorVentana'>
                            <div class='contGraficoVentana'>
                                <div id='" . $contGrafico1 . "' class='tamGraficoDashboard'></div>
                            </div>

                            <div class='contGraficoVentana'>
                                <div id='" . $contGrafico2 . "' class='tamGraficoDashboard'></div>
                            </div>

                            <div class='contInfoVentana'>
                                <div class='contDivInfo'>Ultimo Escaneo: " . $fechaEscaneo . "</div>

                                <div id='alcance" . $titulo . "' class='contDivInfo'>Alcance: " . $alcance . " %</div>

                                <div id='sobrante" . $titulo . "' class='contDivInfo'>Sobrante: " . $sobrante . "</div>

                                <div id='faltante" . $titulo . "' class='contDivInfo'>Faltante: " . $faltante . "</div>

                                <div class='contDivInfo'>Fecha Próxima: " . $fechaProxima . "</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br>";
        return $contenedor;
    }
    
    function contenedorInfo($titulo, $fechaEscaneo, $sobrante, $faltante, $fechaProxima, $pos){
        $float = "float-lt";
        if ($pos == 1){
            $float = "float-rt";
        }
        
        $contenedor = "<div class='contVentanaDashboard1 " . $float . "'>
                <div class='ventanaSuperior1'>
                    <div class='contTituloDashborad'>" . $titulo . "</div>
                </div>

                <div class='ventanaInferior1'>
                    <div class='contInteriorVentana1'>
                        <div class='contInfoVentana1'>
                            <div class='contDivInfo'>Ultimo Escaneo: " . $fechaEscaneo . "</div>

                            <div class='contDivInfo'>Alcance:0%</div>

                            <div class='contDivInfo'>Sobrante:" . $sobrante . "</div>

                            <div class='contDivInfo'>Faltante:" . $faltante . "</div>

                            <div class='contDivInfo'>Fecha Próxima: " . $fechaProxima . "</div>
                        </div>
                    </div>
                </div>
            </div>";
        return $contenedor;
    }
}
