<?php
class clase_SPLA_cliente extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO SPLACliente (cliente, empleado, nombreCliente, contrato, inicioContrato, finContrato, licenciasWin, licenciasSQL) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $type, $VM, $licenciasWin, $licenciasSQL) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE SPLACliente SET type = :type, VM = :VM, licenciasWin = :licenciasWin, licenciasSQL = :licenciasSQL WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':type'=>$type, ':VM'=>$VM, ':licenciasWin'=>$licenciasWin, ':licenciasSQL'=>$licenciasSQL));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarFechas($id, $inicioContrato, $finContrato) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE SPLACliente SET inicioContrato = :inicioContrato, finContrato = :finContrato WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':inicioContrato'=>$inicioContrato, ':finContrato'=>$finContrato));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM SPLACliente WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todoFiltrado($cliente, $empleado, $producto, $nombreCliente, $contrato, $pagina, $limite) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM SPLACliente '
                . 'WHERE cliente = :cliente AND empleado = :empleado AND nombreCliente LIKE :nombreCliente AND contrato LIKE :contrato '
                . 'ORDER BY nombreCliente '
                . 'LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, 
            ':nombreCliente'=>"%" . $nombreCliente . "%", ':contrato'=>"%" . $contrato . "%"));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todoFiltrado1($cliente, $empleado, $nombreCliente, $contrato, $fecha, $pagina, $limite) {
        try{
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaCliente1($cliente, $empleado, $nombreCliente, $contrato, $fecha, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $where = "";
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            
            if($nombreCliente != ""){
                $array[':nombreCliente'] = $nombreCliente;
                $where .= ' WHERE nombreCliente = :nombreCliente ';
            }
            
            if($contrato != ""){
                $array[':contrato'] = $contrato;
                if($where != ""){
                    $where .= ' AND ';
                } else{
                    $where .= ' WHERE ';
                }
                $where .= ' contrato = :contrato ';
            }
            
            if($fecha != ""){
                $array[':fecha'] = $fecha;
                if($where != ""){
                    $where .= ' AND ';
                } else{
                    $where .= ' WHERE ';
                }
                $where .= ' inicioContrato = :fecha ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla.id,
                    tabla.nombreCliente,
                    tabla.contrato,
                    tabla.inicioContrato,
                    tabla.finContrato,
                    SUM(existe) AS existe
                FROM (SELECT SPLACliente.id,
                        nombreCliente,
                        contrato, 
                        DATE_FORMAT(inicioContrato, "%m/%d/%Y") AS inicioContrato,
                        DATE_FORMAT(finContrato, "%m/%d/%Y") AS finContrato,
                        1 AS existe
                    FROM SPLACliente 
                    WHERE cliente = :cliente AND empleado = :empleado

                    UNION

                    SELECT 0 AS id,
                        nombreCliente,
                        contrato,
                        DATE_FORMAT(inicioContrato, "%m/%d/%Y") AS inicioContrato,
                        DATE_FORMAT(finContrato, "%m/%d/%Y") AS finContrato,
                        2 AS existe
                    FROM historicoCustomerSPLA
                        INNER JOIN historicoSPLA ON historicoCustomerSPLA.idHistorico = historicoSPLA.id AND
                        historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha)
                    FROM historicoSPLA
                    WHERE cliente = :cliente)) tabla
                ' . $where . '
                GROUP BY tabla.nombreCliente, tabla.contrato
                ORDER BY tabla.nombreCliente, tabla.contrato '
                . 'LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function totalRegistrosClientes($cliente, $empleado, $producto, $nombreCliente, $contrato){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(id) AS cantidad '
                . 'FROM SPLACliente '
                . 'WHERE cliente = :cliente AND empleado = :empleado AND nombreCliente LIKE :nombreCliente '
                . 'AND contrato LIKE :contrato');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, 
            ':nombreCliente'=>"%" . $nombreCliente . "%", ':contrato'=>"%" . $contrato . "%"));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalRegistrosClientes1($cliente, $empleado, $nombreCliente, $contrato, $fecha){
        try{
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            
            if($nombreCliente != ""){
                $array[':nombreCliente'] = $nombreCliente;
                $where .= ' WHERE nombreCliente = :nombreCliente ';
            }
            
            if($contrato != ""){
                $array[':contrato'] = $contrato;
                if($where != ""){
                    $where .= ' AND ';
                } else{
                    $where .= ' WHERE ';
                }
                $where .= ' contrato = :contrato ';
            }
            
            if($fecha != ""){
                $array[':fecha'] = $fecha;
                if($where != ""){
                    $where .= ' AND ';
                } else{
                    $where .= ' WHERE ';
                }
                $where .= ' inicioContrato = :fecha ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(tabla1.id) AS cantidad
                FROM (SELECT tabla.id,
                        tabla.nombreCliente,
                        tabla.contrato
                    FROM (SELECT SPLACliente.id,
                            nombreCliente,
                            contrato,
                            DATE_FORMAT(inicioContrato, "%m/%d/%Y") AS inicioContrato,
                            DATE_FORMAT(finContrato, "%m/%d/%Y") AS finContrato,
                            1 AS existe
                        FROM SPLACliente
                        WHERE cliente = :cliente AND empleado = :empleado

                        UNION

                        SELECT 0 AS id,
                            nombreCliente,
                            contrato,
                            DATE_FORMAT(inicioContrato, "%m/%d/%Y") AS inicioContrato,
                            DATE_FORMAT(finContrato, "%m/%d/%Y") AS finContrato,
                            2 AS existe
                        FROM historicoCustomerSPLA
                            INNER JOIN historicoSPLA ON historicoCustomerSPLA.idHistorico = historicoSPLA.id AND
                            historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha)
                        FROM historicoSPLA
                        WHERE cliente = :cliente)) tabla
                    ' . $where . '
                    GROUP BY tabla.nombreCliente, tabla.contrato
                    ORDER BY tabla.nombreCliente, tabla.contrato) tabla1');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function fechasInicio($cliente, $empleado){
        try{            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT DATE_FORMAT(inicioContrato, "%m/%d/%Y") AS inicioContrato '
                . 'FROM SPLACliente '
                . 'WHERE cliente = :cliente AND empleado = :empleado '
                . 'GROUP BY inicioContrato');
            $sql->execute(array(":cliente"=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function clientes($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT nombreCliente '
                . 'FROM SPLACliente '
                . 'WHERE cliente = :cliente AND empleado = :empleado '
                . 'GROUP BY nombreCliente '
                . 'ORDER BY nombreCliente');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function contratos($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT contrato '
                . 'FROM SPLACliente '
                . 'WHERE cliente = :cliente AND empleado = :empleado '
                . 'GROUP BY contrato '
                . 'ORDER BY contrato');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function ultimaPaginaCliente($cliente, $empleado, $producto, $nombreCliente, $contrato, $limite) {
        return ceil($this->totalRegistrosClientes($cliente,$empleado, $producto, $nombreCliente, $contrato) / $limite);
    }
    
    function ultimaPaginaCliente1($cliente, $empleado, $nombreCliente, $contrato, $fecha, $limite) {
        return ceil($this->totalRegistrosClientes1($cliente,$empleado, $nombreCliente, $contrato, $fecha) / $limite);
    }
}