<?php
class clase_centralizador_web extends General{
    public $error; 
    
    Public Function dataCentralizador($empresa, $agente, $hostname, $ip, $fecha, $status, $inicio){
        try{
            $inicio = ($inicio * $this->limit_paginacion) - $this->limit_paginacion;
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT clientes.empresa, agente.id, agente.tx_host_name, agente.tx_ip,
                    MAX(agente_hist.fe_ejeccn) AS fe_ejeccn,
                    CASE
                        WHEN DATEDIFF(CURDATE(), IFNULL(MAX(agente_hist.fe_ejeccn), '1981-01-01')) > (SELECT val FROM agente_config WHERE id = 1) THEN
                            'Off'
                        ELSE
                            'On'
                    END AS estado
                FROM agente
                    LEFT JOIN agente_hist ON agente.id = agente_hist.id_agente
                    INNER JOIN clientes ON agente.cliente = clientes.id AND clientes.estado = 1 AND CURDATE() <= clientes.fecha2
                WHERE clientes.empresa LIKE :empresa AND agente.id LIKE :agente AND agente.tx_host_name LIKE :hostname
                AND agente.tx_ip LIKE :ip AND agente_hist.fe_ejeccn LIKE :fecha AND CASE
                        WHEN DATEDIFF(CURDATE(), IFNULL(agente_hist.fe_ejeccn, '1981-01-01')) > (SELECT val FROM agente_config WHERE id = 1) THEN
                            'Off'
                        ELSE
                            'On'
                    END LIKE :status
                GROUP BY agente.id, agente.tx_host_name, agente.tx_ip
                LIMIT " . $inicio . ", " . $this->limit_paginacion);
            $sql->execute(array(':empresa'=>'%' . $empresa . '%', ':agente'=>'%' . $agente . '%', ':hostname'=>'%' . $hostname . '%', 
            ':ip'=>'%' . $ip . '%', ':fecha'=>'%' . $fecha . '%', ':status'=>'%' . $status . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    Public Function total($empresa, $agente, $hostname, $ip, $fecha, $status){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(clientes.empresa) AS cantidad
                FROM agente
                    LEFT JOIN agente_hist ON agente.id = agente_hist.id_agente
                    INNER JOIN clientes ON agente.cliente = clientes.id AND clientes.estado = 1 AND CURDATE() <= clientes.fecha2
                WHERE clientes.empresa LIKE :empresa AND agente.id LIKE :agente AND agente.tx_host_name LIKE :hostname
                AND agente.tx_ip LIKE :ip AND agente_hist.fe_ejeccn LIKE :fecha AND CASE
                        WHEN DATEDIFF(CURDATE(), IFNULL(agente_hist.fe_ejeccn, '1981-01-01')) > (SELECT val FROM agente_config WHERE id = 1) THEN
                            'Off'
                        ELSE
                            'On'
                    END LIKE :status
                GROUP BY agente.id, agente.tx_host_name, agente.tx_ip");
            $sql->execute(array(':empresa'=>'%' . $empresa . '%', ':agente'=>'%' . $agente . '%', ':hostname'=>'%' . $hostname . '%', 
            ':ip'=>'%' . $ip . '%', ':fecha'=>'%' . $fecha . '%', ':status'=>'%' . $status . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    Public Function dataCentralizadorCliente($cliente, $empresa, $agente, $hostname, $ip, $fecha, $status, $inicio){
        try{
            $inicio = ($inicio * $this->limit_paginacion) - $this->limit_paginacion;
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT clientes.empresa, agente.id, agente.tx_host_name, agente.tx_ip,
                    MAX(agente_hist.fe_ejeccn) AS fe_ejeccn,
                    CASE
                        WHEN DATEDIFF(CURDATE(), IFNULL(MAX(agente_hist.fe_ejeccn), '1981-01-01')) > (SELECT val FROM agente_config WHERE id = 1) THEN
                            'Off'
                        ELSE
                            'On'
                    END AS estado
                FROM agente
                    LEFT JOIN agente_hist ON agente.id = agente_hist.id_agente
                    INNER JOIN clientes ON agente.cliente = clientes.id AND clientes.estado = 1 AND CURDATE() <= clientes.fecha2
                WHERE clientes.id = :cliente AND clientes.empresa LIKE :empresa AND agente.id LIKE :agente AND agente.tx_host_name LIKE :hostname
                AND agente.tx_ip LIKE :ip AND agente_hist.fe_ejeccn LIKE :fecha AND CASE
                        WHEN DATEDIFF(CURDATE(), IFNULL(agente_hist.fe_ejeccn, '1981-01-01')) > (SELECT val FROM agente_config WHERE id = 1) THEN
                            'Off'
                        ELSE
                            'On'
                    END LIKE :status
                GROUP BY agente.id, agente.tx_host_name, agente.tx_ip
                LIMIT " . $inicio . ", " . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente, ':empresa'=>'%' . $empresa . '%', ':agente'=>'%' . $agente . '%', ':hostname'=>'%' . $hostname . '%', 
            ':ip'=>'%' . $ip . '%', ':fecha'=>'%' . $fecha . '%', ':status'=>'%' . $status . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    Public Function totalCliente($cliente, $empresa, $agente, $hostname, $ip, $fecha, $status){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT SUM(tabla.cantidad) AS cantidad 
                FROM (SELECT 1 AS cantidad
                FROM agente
                    LEFT JOIN agente_hist ON agente.id = agente_hist.id_agente
                    INNER JOIN clientes ON agente.cliente = clientes.id AND clientes.estado = 1 AND CURDATE() <= clientes.fecha2
                WHERE clientes.id = :cliente AND clientes.empresa LIKE :empresa AND agente.id LIKE :agente AND agente.tx_host_name LIKE :hostname
                AND agente.tx_ip LIKE :ip AND agente_hist.fe_ejeccn LIKE :fecha AND CASE
                        WHEN DATEDIFF(CURDATE(), IFNULL(agente_hist.fe_ejeccn, '1981-01-01')) > (SELECT val FROM agente_config WHERE id = 1) THEN
                            'Off'
                        ELSE
                            'On'
                    END LIKE :status
                GROUP BY agente.id, agente.tx_host_name, agente.tx_ip) AS tabla");
            $sql->execute(array(':cliente'=>$cliente, ':empresa'=>'%' . $empresa . '%', ':agente'=>'%' . $agente . '%', ':hostname'=>'%' . $hostname . '%', 
            ':ip'=>'%' . $ip . '%', ':fecha'=>'%' . $fecha . '%', ':status'=>'%' . $status . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    Public Function deleteAgent($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente WHERE id = :id_agente');
            $sql->execute(array(':id_agente'=>$id_agente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    Public Function dataAddRemove($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_add_remove.tx_dato_contrl, agente_add_remove.tx_host_name, tx_regist, tx_editor, tx_verson,
                    tx_dia_instlc, tx_softwr
                FROM agente_add_remove
                    INNER JOIN agente ON agente_add_remove.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataDesinstalaciones($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_desinstalacion.tx_dato_contrl, agente_desinstalacion.tx_host_name, 
                    tx_descrp, tx_fe_desins, tx_usuaro_desins
                FROM agente_desinstalacion
                    INNER JOIN agente ON agente_desinstalacion.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataLlaves($cliente){ 
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_llave.tx_dato_contrl, tx_hot_name, tx_tipo_llave, tx_llave
                FROM agente_llave
                    INNER JOIN agente ON agente_llave.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataProcesador($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_procesador.tx_dato_contrl, agente_procesador.tx_host_name, tx_tipo_cpu, tx_nu_cpu, tx_nu_cores,
                    tx_procsd_logico, tx_tipo_escano
                FROM agente_procesador
                    INNER JOIN agente ON agente_procesador.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataProceso($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_proceso.tx_dato_contrl, agente_proceso.tx_host_name, tx_nb_procso, tx_ruta_procso
                FROM agente_proceso
                    INNER JOIN agente ON agente_proceso.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataResultadosEscaneo($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_resultado_escaneo.tx_hot_name, tx_status, tx_error
                FROM agente_resultado_escaneo
                    INNER JOIN agente ON agente_resultado_escaneo.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataSeguridad($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tx_dato_contrl, tx_hot_name, tx_captin, tx_descrp, tx_fix_commnt, tx_hotfix_id,
                tx_instll_date, tx_install_by, tx_install_on, tx_name, tx_servce_pack, tx_status
            FROM agente_seguridad
                     INNER JOIN agente ON agente_seguridad.id_agente = agente.id AND nagente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataSerialMaquina($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_serial_maquina.tx_dato_contrl, agente_serial_maquina.tx_host_name, tx_serial_maquna
                FROM agente_serial_maquina
                    INNER JOIN agente ON agente_serial_maquina.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataServicio($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_servicio.tx_dato_contrl, agente_servicio.tx_host_name, tx_nb_prodct, tx_estado
                FROM agente_servicio
                    INNER JOIN agente ON agente_servicio.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataSistemaOperativo($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_sistema_operativo.tx_dato_contrl, agente_sistema_operativo.tx_host_name, tx_sistma_opertv, tx_fe_creacn
                FROM agente_sistema_operativo
                    INNER JOIN agente ON agente_sistema_operativo.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataSQL($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_sql_data.tx_dato_contrl, agente_sql_data.tx_host_name, tx_llave_regist, tx_edicon, tx_verson, tx_ruta_instlc
                FROM agente_sql_data
                    INNER JOIN agente ON agente_sql_data.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataTipoEquipo($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_tipo_equipo.tx_dato_contrl, agente_tipo_equipo.tx_host_name, tx_fabrcn, tx_modelo
                FROM agente_tipo_equipo
                    INNER JOIN agente ON agente_tipo_equipo.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function dataUsuarioEquipo($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT agente_usuario_equipo.tx_dato_contrl, agente_usuario_equipo.tx_host_name, agente_usuario_equipo.tx_domino, agente_usuario_equipo.tx_usuario, agente_usuario_equipo.tx_ip
                FROM agente_usuario_equipo
                    INNER JOIN agente ON agente_usuario_equipo.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    Public Function dataUsabilidadSoftware($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tx_dato_contrl, tx_hot_name, tx_softwr, tx_last_exec
                FROM agente_usabilidad_software
                    INNER JOIN agente ON agente_usabilidad_software.id_agente = agente.id AND agente.cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    Public Function config($id){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT id, descripcion, val
            FROM agente_config
            WHERE id = :id");
            $sql->execute(array(":id"=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('id'=>0, 'descripcion'=>"", 'val'=>"");
        }
    }
    
    Public Function configCliente($cliente){
        try{          
            $this->conexion();
            $sql = $this->conn->prepare("SELECT id, descripcion, val
            FROM agente_config
            WHERE cliente = :cliente");
            $sql->execute(array(":cliente"=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('id'=>0, 'descripcion'=>"", 'val'=>"");
        }
    }

    Public Function actualizarConfig($id, $val){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE agente_config SET val = :val WHERE id = :id");
            $sql->execute(array(':id'=>$id, ':val'=>$val));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    Public Function selectConfigCliente($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM agente_config WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "cliente"=>0, "descripcion"=>"", "val"=>"");
        }
    }
    
    Public Function insertarConfigCliente($cliente, $val){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO agente_config (cliente, descripcion, val) VALUES (:cliente, 'Limit Agent Idle', :val)");
            $sql->execute(array(':cliente'=>$cliente, ':val'=>$val));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    Public Function actualizarConfigCliente($cliente, $val){
        try{
            $this->conexion();
            
            $row = $this->selectConfigCliente($cliente);
            if($row["cliente"] > 0){
                $sql = $this->conn->prepare("UPDATE agente_config SET val = :val WHERE cliente = :cliente");
                $sql->execute(array(':id'=>$id, ':val'=>$val));
                return true;
            } else{
                return $this->insertarConfigCliente($cliente, $val);
            }
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    Public Function listaScheduling($empresa, $scheduling, $fechaCreacion, $estado, $inicio){        
        try{
            $inicio = ($inicio * $this->limit_paginacion) - $this->limit_paginacion;
                    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT clientes.empresa,
                    scheduling.id, tx_descrip, fe_fecha_creacion,
                    CASE
                        WHEN tiny_status = 1 THEN
                            'Active'
                        ELSE
                            'Inactive'
                    END AS estado
                FROM scheduling
                    INNER JOIN clientes ON scheduling.cliente = clientes.id AND clientes.estado = 1 AND clientes.fecha2 > CURDATE()
                WHERE clientes.empresa LIKE :empresa AND tx_descrip LIKE :scheduling AND fe_fecha_creacion LIKE :fechaCreacion AND 
                tiny_status LIKE :estado
                LIMIT " . $inicio . ", " . $this->limit_paginacion);
            $sql->execute(array(':empresa'=>'%' . $empresa . '%', ':scheduling'=>'%' . $scheduling . '%', 
            ':fechaCreacion'=>'%' . $fechaCreacion . '%', ':estado'=>'%' . $estado . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    Public Function listaSchedulingClient($cliente, $scheduling, $fechaCreacion, $fechaInicio, $estado, $inicio){        
        try{
            $inicio = ($inicio * $this->limit_paginacion) - $this->limit_paginacion;
                    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT clientes.empresa,
                    scheduling.id, tx_descrip, DATE_FORMAT(fe_fecha_creacion, '%m/%d/%Y') AS fe_fecha_creacion,
                    DATE_FORMAT(fe_fecha_inicio, '%m/%d/%Y') AS fe_fecha_inicio,
                    CASE
                        WHEN tiny_status = 1 THEN
                            'Active'
                        ELSE
                            'Inactive'
                    END AS estado
                FROM scheduling
                    INNER JOIN clientes ON scheduling.cliente = clientes.id AND clientes.estado = 1 AND clientes.fecha2 > CURDATE()
                WHERE clientes.id = :cliente AND tx_descrip LIKE :scheduling AND fe_fecha_creacion LIKE :fechaCreacion AND 
                fe_fecha_inicio LIKE :fechaInicio AND tiny_status LIKE :estado
                LIMIT " . $inicio . ", " . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente, ':scheduling'=>'%' . $scheduling . '%', 
            ':fechaCreacion'=>'%' . $fechaCreacion . '%', ':fechaInicio'=>'%' . $fechaInicio . '%', ':estado'=>'%' . $estado . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    Public Function totalScheduling($empresa, $scheduling, $fechaCreacion, $estado){        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(scheduling.id) AS cantidad
                FROM scheduling
                    INNER JOIN clientes ON scheduling.cliente = clientes.id AND clientes.estado = 1 AND clientes.fecha2 > CURDATE()
                WHERE clientes.empresa LIKE :empresa AND tx_descrip LIKE :scheduling AND fe_fecha_creacion LIKE :fechaCreacion AND 
                tiny_status LIKE :estado");
            $sql->execute(array(':empresa'=>'%' . $empresa . '%', ':scheduling'=>'%' . $scheduling . '%', 
            ':fechaCreacion'=>'%' . $fechaCreacion . '%', ':estado'=>'%' . $estado . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    Public Function totalSchedulingClient($cliente, $scheduling, $fechaCreacion, $fechaInicio, $estado){        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(scheduling.id) AS cantidad
                FROM scheduling
                    INNER JOIN clientes ON scheduling.cliente = clientes.id AND clientes.estado = 1 AND clientes.fecha2 > CURDATE()
                WHERE clientes.id = :cliente AND tx_descrip LIKE :scheduling AND fe_fecha_creacion LIKE :fechaCreacion AND 
                fe_fecha_inicio LIKE :fechaInicio AND tiny_status LIKE :estado");
            $sql->execute(array(':cliente'=>$cliente, ':scheduling'=>'%' . $scheduling . '%', 
            ':fechaCreacion'=>'%' . $fechaCreacion . '%', ':fechaInicio'=>'%' . $fechaInicio . '%', ':estado'=>'%' . $estado . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    Public Function configScheduling($cliente, $tx_descrip, $fe_fecha_inicio, $tx_schedule, $int_repeat_days, 
    $int_repeat_weeks, $tx_days, $tx_months, $num_days, $tipoScheduling, $status){
        try{
            $array = array(':cliente'=>$cliente, ':tx_descrip'=>$tx_descrip, ':fe_fecha_inicio'=>$fe_fecha_inicio, 
            ':tx_schedule'=>$tx_schedule, ':tipoScheduling'=>$tipoScheduling, ':status'=>$status);
            $campos = "";
            $valores = "";

            if (filter_var($int_repeat_days, FILTER_VALIDATE_INT) !== false){
                $campos .= ", int_repeat_days";
                $valores .= ", :int_repeat_days";
                $array[":int_repeat_days"] = $int_repeat_days;
            }

            if (filter_var($int_repeat_weeks, FILTER_VALIDATE_INT) !== false){
                $campos .= ", int_repeat_weeks";
                $valores .= ", :int_repeat_weeks";
                $array[":int_repeat_weeks"] = $int_repeat_weeks;
            }

            if ($tx_days != ""){ 
                $campos .= ", tx_days";
                $valores .= ", :tx_days";
                $array[":tx_days"] = $tx_days;
            }

            if ($tx_months != ""){
                $campos .= ", tx_months";
                $valores .= ", :tx_months";
                $array[":tx_months"] = $tx_months;
            }

            if ($num_days != ""){
                $campos .= ", num_days";
                $valores .= ", :num_days";
                $array[":num_days"] = $num_days;
            }
                
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO scheduling (cliente, tx_descrip, fe_fecha_creacion, fe_fecha_inicio, "
            . "tx_schedule " . $campos . ", tx_tipo_scheduling, tiny_status) VALUES (:cliente, :tx_descrip, CURDATE(), :fe_fecha_inicio, "
            . ":tx_schedule" . $valores . ", :tipoScheduling, :status)");
            $sql->execute($array);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    Public Function updateConfigScheduling($id, $cliente, $tx_descrip, $fe_fecha_inicio, $tx_schedule, $int_repeat_days, 
    $int_repeat_weeks, $tx_days, $tx_months, $num_days, $tipoScheduling, $status){
        try{
            $array = array(':id'=>$id, ':cliente'=>$cliente, ':tx_descrip'=>$tx_descrip, ':fe_fecha_inicio'=>$fe_fecha_inicio, 
            ':tx_schedule'=>$tx_schedule, ':tipoScheduling'=>$tipoScheduling, ':status'=>$status);
            $valores = "";

            if (filter_var($int_repeat_days, FILTER_VALIDATE_INT) !== false){
                $valores .= ", int_repeat_days = :int_repeat_days";
                $array[":int_repeat_days"] = $int_repeat_days;
            }

            if (filter_var($int_repeat_weeks, FILTER_VALIDATE_INT) !== false){
                $valores .= ", int_repeat_weeks = :int_repeat_weeks";
                $array[":int_repeat_weeks"] = $int_repeat_weeks;
            }

            if ($tx_days != ""){ 
                $valores .= ", tx_days = :tx_days";
                $array[":tx_days"] = $tx_days;
            }

            if ($tx_months != ""){
                $valores .= ", tx_months = :tx_months";
                $array[":tx_months"] = $tx_months;
            }

            if ($num_days != ""){
                $valores .= ", num_days = :num_days";
                $array[":num_days"] = $num_days;
            }
                
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE scheduling SET cliente = :cliente, tx_descrip = :tx_descrip, fe_fecha_inicio = :fe_fecha_inicio, 
            tx_schedule = :tx_schedule, tx_tipo_scheduling = :tipoScheduling, tiny_status = :status " . $valores . " WHERE id = :id");
            $sql->execute($array);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    Public Function idScheduling($cliente, $tx_descrip){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT id FROM scheduling WHERE tx_descrip = :tx_descrip");
            $sql->execute(array(':cliente'=>$cliente, ':tx_descrip'));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    Public Function eliminarScheduling($id_scheduling){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM scheduling WHERE id = :id_scheduling");
            $sql->execute(array(':id_scheduling'=>$id_scheduling));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    Public Function eliminarSchedulingAgente($id_scheduling){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM agente_scheduling WHERE id_scheduling = :id_scheduling");
            $sql->execute(array(':id_scheduling'=>$id_scheduling));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    Public Function agregarSchedulingAgente($id_scheduling, $id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO agente_scheduling (id_scheduling, id_agente) VALUES "
                . "(:id_scheduling, :id_agente)");
            $sql->execute(array(':id_scheduling'=>$id_scheduling, ':id_agente'=>$id_agente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    Public Function infoScheduling($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM scheduling WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "cliente"=>0, "tx_descrip"=>"", "fe_fecha_creacion"=>"1981-01-01", "fe_fecha_inicio"=>"1981-01-01",
            "tx_schedule"=>"", "int_repeat_days"=>null, "int_repeat_weeks"=>null, "tx_days"=>null, "tx_months"=>null,
            "num_days"=>null, "tiny_status"=>0);
        }
    }

    Public Function listaAgenteScheduling($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT id_agente FROM agente_scheduling WHERE id_scheduling = :id");
            $sql->execute(array(':id'=>$id));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}
