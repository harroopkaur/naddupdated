<?php
class consolidadoProcesadoresIBM extends General{
    public $error;
    
    // Insertar 
    function insertar($cliente, $empleado, $datoControl, $hostName, $tipoCpu, $cpu, $cores, $procesadoresLogicos, $tipoEscaneo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoProcesadoresIBM (cliente, empleado, dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) 
            VALUES(:cliente, :empleado, :datoControl, :hostName, :tipoCpu, :cpu, :cores, :procesadoresLogicos, :tipoEscaneo)");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado, 'datoControl'=>$datoControl , 'hostName'=>$hostName, 'tipoCpu'=>$tipoCpu, 'cpu'=>$cpu, 'cores'=>$cores, 'procesadoresLogicos'=>$procesadoresLogicos, 'tipoEscaneo'=>$tipoEscaneo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidadoProcesadoresIBM (cliente, empleado, dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente,$empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM consolidadoProcesadoresIBM WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoProcesadores($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT consolidadoProcesadoresIBM.cliente,
                    consolidadoProcesadoresIBM.empleado,
                    consolidadoProcesadoresIBM.host_name,
                    consolidadoProcesadoresIBM.tipo_CPU,
                    CONCAT(IFNULL(resumen_ibm.familia, ''), ' ', IFNULL(resumen_ibm.edicion, ''), ' ', IFNULL(resumen_ibm.version, '')) AS producto,
                    consolidadoProcesadoresIBM.cpu,
                    consolidadoProcesadoresIBM.cores
                FROM consolidadoProcesadoresIBM
                    INNER JOIN resumen_ibm ON consolidadoProcesadoresIBM.cliente = resumen_ibm.cliente
                    AND consolidadoProcesadoresIBM.empleado = resumen_ibm.empleado AND
                    consolidadoProcesadoresIBM.host_name = resumen_ibm.equipo
                WHERE consolidadoProcesadoresIBM.cliente = :cliente AND consolidadoProcesadoresIBM.empleado = :empleado AND cpu > 0
                GROUP BY consolidadoProcesadoresIBM.cliente, consolidadoProcesadoresIBM.empleado, consolidadoProcesadoresIBM.host_name, 
                consolidadoProcesadoresIBM.tipo_CPU, producto");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}