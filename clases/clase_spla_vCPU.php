<?php
class clase_SPLA_vCPU extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO SPLAVirtualMachine (cliente, empleado, DC, cluster, host, VM, edicion, sockets, cores, cpu, hostDate, VMDate) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $customers, $Qty, $SKU, $type) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE SPLAVirtualMachine SET customers = :customers, Qty = :Qty, SKU = :SKU, type = :type WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':customers'=>$customers, ':Qty'=>$Qty, ':SKU'=>$SKU, ':type'=>$type));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarPriceSale($id, $QtySale, $priceSale) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE SPLAVirtualMachine SET QtySale = :QtySale, priceSale = :priceSale WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':QtySale'=>$QtySale, ':priceSale'=>$priceSale));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM SPLAVirtualMachine WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todoFiltrado($cliente, $empleado, $producto, $cluster, $host, $pagina, $limite) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM SPLAVirtualMachine '
                . 'WHERE cliente = :cliente AND empleado = :empleado AND edicion LIKE :producto AND cluster LIKE :cluster '
                . 'AND host LIKE :host '
                . 'ORDER BY DC '
                . 'LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':producto'=>"%" . $producto . "%", 
            ':cluster'=>"%" . $cluster . "%", ':host'=>"%" . $host . "%"));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todoFiltrado1($cliente, $empleado, $customers, $datacenter, $VM, $pagina, $limite) {
        try{
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginavCPU1($cliente, $empleado, $customers, $datacenter, $VM, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            
            if($customers != ""){
                $array[":customers"] = $customers;
                $where .= " WHERE customers = :customers ";
            }
            
            if($datacenter != ""){
                $array[":datacenter"] = $datacenter;
                if($where != ""){
                    $where .= " AND ";
                } else{
                    $where .= " WHERE ";
                }
                $where .= " DC = :datacenter ";
            }
            
            if($VM != ""){
                $array[":VM"] = $VM;
                if($where != ""){
                    $where .= " AND ";
                } else{
                    $where .= " WHERE ";
                }
                $where .= " VM = :VM ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, DC, cluster, host, VM, edicion, sockets, cores, cpu, hostDate, 
                    VMDate, customers, Qty, SKU, type, SUM(existe) AS existe
                FROM (SELECT id, DC, cluster, host, VM, edicion, sockets, cores, cpu, hostDate, 
                        VMDate, customers, Qty, SKU, type, 1 AS existe
                    FROM SPLAVirtualMachine
                    WHERE cliente = :cliente AND empleado = :empleado

                    UNION

                    SELECT 0 AS id, DC, cluster, host, VM, edicion, sockets, cores, cpu, hostDate, 
                        VMDate, customers, Qty, SKU, type, 2 AS existe
                    FROM historicoVMSPLA
                         INNER JOIN historicoSPLA ON historicoVMSPLA.idHistorico = historicoSPLA.id
                         AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla
                ' . $where . '
                GROUP BY VM '
                . 'LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function totalRegistrosvCPU($cliente, $empleado, $producto, $cluster, $host){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(id) AS cantidad '
                . 'FROM SPLAVirtualMachine '
                . 'WHERE cliente = :cliente AND empleado = :empleado AND edicion LIKE :producto AND cluster LIKE :cluster '
                . 'AND host LIKE :host');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':producto'=>"%" . $producto . "%", 
            ':cluster'=>"%" . $cluster . "%", ':host'=>"%" . $host . "%"));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalRegistrosvCPU1($cliente, $empleado, $clientes, $datacenter, $VM){
        try{
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            
            if($clientes != ""){
                $array[":customers"] = $clientes;
                if($where != ""){
                    $where .= " AND ";
                } else{
                    $where .= " WHERE ";
                }
                
                $where .= " customers = :customers ";
            }
            
            if($datacenter != ""){
                $array[":datacenter"] = $datacenter;
                if($where != ""){
                    $where .= " AND ";
                } else{
                    $where .= " WHERE ";
                }
                $where .= " DC = :datacenter ";
            }
            
            if($VM != ""){
                $array[":VM"] = $VM;
                if($where != ""){
                    $where .= " AND ";
                } else{
                    $where .= " WHERE ";
                }
                $where .= " VM = :VM ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(VM) AS cantidad
                FROM (SELECT VM
                FROM (SELECT id, DC, cluster, host, VM, edicion, sockets, cores, cpu, hostDate, 
                        VMDate, customers, Qty, SKU, type, 1 AS existe
                    FROM SPLAVirtualMachine
                    WHERE cliente = :cliente AND empleado = :empleado

                    UNION

                    SELECT 0 AS id, DC, cluster, host, VM, edicion, sockets, cores, cpu, hostDate, 
                        VMDate, customers, Qty, SKU, type, 2 AS existe
                    FROM historicoVMSPLA
                         INNER JOIN historicoSPLA ON historicoVMSPLA.idHistorico = historicoSPLA.id
                         AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla
                ' . $where . '
                GROUP BY VM) tabla1');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function clusters($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT cluster '
                . 'FROM SPLAVirtualMachine '
                . 'WHERE cliente = :cliente AND empleado = :empleado '
                . 'GROUP BY cluster '
                . 'ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function datacenter($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT DC '
                . 'FROM SPLAVirtualMachine '
                . 'WHERE cliente = :cliente AND empleado = :empleado '
                . 'GROUP BY DC '
                . 'ORDER BY DC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function hosts($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT host '
                . 'FROM SPLAVirtualMachine '
                . 'WHERE cliente = :cliente AND empleado = :empleado '
                . 'GROUP BY host '
                . 'ORDER BY host');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function VM($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT VM '
                . 'FROM SPLAVirtualMachine '
                . 'WHERE cliente = :cliente AND empleado = :empleado '
                . 'GROUP BY VM '
                . 'ORDER BY VM');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function ultimaPaginavCPU($cliente, $empleado, $producto, $cluster, $host, $limite) {
        return ceil($this->totalRegistrosvCPU($cliente,$empleado, $producto, $cluster, $host) / $limite);
    }
    
    function ultimaPaginavCPU1($cliente, $empleado, $clientes, $datacenter, $VM, $limite) {
        return ceil($this->totalRegistrosvCPU1($cliente,$empleado, $clientes, $datacenter, $VM) / $limite);
    }
    
    function ultimaPaginaApps($cliente, $empleado, $clientes, $producto, $limite) {
        return ceil($this->appValidationTotal($cliente,$empleado, $clientes, $producto) / $limite);
    }
    
    function ultimaPaginaReportingCurrent($cliente, $empleado, $clientes, $producto, $limite) {
        return ceil($this->currentReportingTotal($cliente,$empleado, $clientes, $producto) / $limite);
    }
    
    function ultimaPaginaReportingValidation($cliente, $empleado, $producto, $limite) {
        return ceil($this->reportingValidationTotal($cliente,$empleado, $producto) / $limite);
    }
    
    function totalRegistrosResumen($cliente, $empleado, $producto, $cluster, $host){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM SPLAVirtualMachine
                    INNER JOIN detalles_equipo_SPLA ON SPLAVirtualMachine.host = detalles_equipo_SPLA.equipo AND
                    SPLAVirtualMachine.cliente = detalles_equipo_SPLA.cliente AND SPLAVirtualMachine.empleado = detalles_equipo_SPLA.empleado
                    INNER JOIN SPLACliente ON SPLAVirtualMachine.VM = SPLACliente.VM AND
                    SPLAVirtualMachine.cliente = SPLACliente.cliente AND SPLAVirtualMachine.empleado = SPLACliente.empleado
                WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado AND 
                SPLAVirtualMachine.edicion LIKE :producto AND SPLAVirtualMachine.cluster LIKE :cluster AND SPLAVirtualMachine.host LIKE :host');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':producto'=>'%' . $producto . '%', ':cluster'=>'%' . $cluster . '%', ':host'=>'%' . $host . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function resumenFiltrado($cliente, $empleado, $producto, $cluster, $host, $pagina, $limite){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT DC,
                    cluster,
                    host,
                    SPLAVirtualMachine.VM,
                    SPLAVirtualMachine.edicion,
                    sockets,
                    cores,
                    cpu,
                    whenCreated,
                    nombreCliente,
                    inicioContrato,
                    finContrato
                FROM SPLAVirtualMachine
                    INNER JOIN detalles_equipo_SPLA ON SPLAVirtualMachine.host = detalles_equipo_SPLA.equipo AND
                    SPLAVirtualMachine.cliente = detalles_equipo_SPLA.cliente AND SPLAVirtualMachine.empleado = detalles_equipo_SPLA.empleado
                    INNER JOIN SPLACliente ON SPLAVirtualMachine.VM = SPLACliente.VM AND
                    SPLAVirtualMachine.cliente = SPLACliente.cliente AND SPLAVirtualMachine.empleado = SPLACliente.empleado
                WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado AND 
                SPLAVirtualMachine.edicion LIKE :producto AND SPLAVirtualMachine.cluster LIKE :cluster AND SPLAVirtualMachine.host LIKE :host '
                . 'LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':producto'=>'%' . $producto . '%', ':cluster'=>'%' . $cluster . '%', ':host'=>'%' . $host . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function appValidationTotal($cliente, $empleado, $customer, $producto){
        try{
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            if($customer != ""){
                $array[":customer"] = $customer;
                $where .= 'WHERE nombreCliente = :customer '; 
            }
            
            if($producto != ""){
                $array[":producto"] = $producto;
                if($where == ""){
                    $where = "WHERE ";
                } else{
                    $where = " AND ";
                }
                $where .= ' familia = :producto ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(VM) AS cantidad
                FROM (SELECT VM, familia, edicion
                    FROM (SELECT id, tipo, VM, nombreCliente, VMDate, hostDate, familia, edicion, sockets, cores, cpu, Qty, SKU, type, 1 AS existe
                        FROM (SELECT SPLAVirtualMachine.id,
                                "VM" AS tipo,
                                SPLAVirtualMachine.VM,
                                SPLAVirtualMachine.customers AS nombreCliente,
                                DATE_FORMAT(SPLAVirtualMachine.VMDate, "%m/%d/%Y") AS VMDate,
                                DATE_FORMAT(SPLAVirtualMachine.hostDate, "%m/%d/%Y") AS hostDate,
                                SPLAVirtualMachine.edicion AS familia,
                                "" AS edicion,
                                SPLAVirtualMachine.sockets,
                                SPLAVirtualMachine.cores,
                                SPLAVirtualMachine.cpu,
                                SPLAVirtualMachine.Qty,
                                SPLAVirtualMachine.SKU,
                                SPLAVirtualMachine.type
                            FROM SPLAVirtualMachine
                            WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                            GROUP BY SPLAVirtualMachine.VM, SPLAVirtualMachine.host

                            UNION

                            SELECT resumen_SPLA.id,
                                "App" AS tipo,
                                SPLAVirtualMachine.VM,
                                SPLAVirtualMachine.customers AS nombreCliente,
                                DATE_FORMAT(SPLAVirtualMachine.VMDate, "%m/%d/%Y") AS VMDate,
                                DATE_FORMAT(SPLAVirtualMachine.hostDate, "%m/%d/%Y") AS hostDate,
                                resumen_SPLA.familia,
                                resumen_SPLA.edicion,
                                SPLAVirtualMachine.sockets,
                                SPLAVirtualMachine.cores,
                                SPLAVirtualMachine.cpu,
                                resumen_SPLA.Qty,
                                resumen_SPLA.SKU,
                                resumen_SPLA.type
                            FROM SPLAVirtualMachine
                                INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                                SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                            WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                            GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla

                            UNION

                            SELECT 0 AS id, tipo, VM, customers AS nombreCliente, DATE_FORMAT(VMDate, "%m/%d/%Y") AS VMDate, DATE_FORMAT(hostDate, "%m/%d/%Y") AS hostDate, prod AS familia, edicion, sockets, cores, cpu, Qty, SKU, type, 1 AS existe
                            FROM historicoAppsSPLA
                                 INNER JOIN historicoSPLA ON historicoAppsSPLA.idHistorico = historicoSPLA.id
                                 AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla1
                    ' . $where . '
                    GROUP BY VM, familia, edicion) tabla2');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function appValidationFiltrado($cliente, $empleado, $customer, $producto, $pagina, $limite){
        try{
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaApps($cliente, $empleado, $customer, $producto, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            if($customer != ""){
                $array[":customer"] = $customer;
                $where .= 'WHERE nombreCliente = :customer '; 
            }
            
            if($producto != ""){
                $array[":producto"] = $producto;
                if($where == ""){
                    $where = "WHERE ";
                } else{
                    $where = " AND ";
                }
                $where .= ' familia = :producto ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, tipo, VM, nombreCliente, VMDate, hostDate, familia, 
                    edicion, sockets, cores, cpu, Qty, SKU, type, SUM(existe) AS existe
                FROM (SELECT id, tipo, VM, nombreCliente, VMDate, hostDate, familia, edicion, sockets, cores, 
                        cpu, Qty, SKU, type, 1 AS existe
                    FROM (SELECT SPLAVirtualMachine.id,
                            "VM" AS tipo,
                            SPLAVirtualMachine.VM,
                            SPLAVirtualMachine.customers AS nombreCliente,
                            DATE_FORMAT(SPLAVirtualMachine.VMDate, "%m/%d/%Y") AS VMDate,
                            DATE_FORMAT(SPLAVirtualMachine.hostDate, "%m/%d/%Y") AS hostDate,
                            SPLAVirtualMachine.edicion AS familia,
                            "" AS edicion,
                            SPLAVirtualMachine.sockets,
                            SPLAVirtualMachine.cores,
                            SPLAVirtualMachine.cpu,
                            SPLAVirtualMachine.Qty,
                            SPLAVirtualMachine.SKU,
                            SPLAVirtualMachine.type
                        FROM SPLAVirtualMachine
                        WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                        GROUP BY SPLAVirtualMachine.VM, SPLAVirtualMachine.host

                        UNION

                        SELECT resumen_SPLA.id,
                            "App" AS tipo,
                            SPLAVirtualMachine.VM,
                            SPLAVirtualMachine.customers AS nombreCliente,
                            DATE_FORMAT(SPLAVirtualMachine.VMDate, "%m/%d/%Y") AS VMDate,
                            DATE_FORMAT(SPLAVirtualMachine.hostDate, "%m/%d/%Y") AS hostDate,
                            resumen_SPLA.familia,
                            resumen_SPLA.edicion,
                            SPLAVirtualMachine.sockets,
                            SPLAVirtualMachine.cores,
                            SPLAVirtualMachine.cpu,
                            resumen_SPLA.Qty,
                            resumen_SPLA.SKU,
                            resumen_SPLA.type
                        FROM SPLAVirtualMachine
                            INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                            SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                        WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                        GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla

                        UNION

                        SELECT 0 AS id, tipo, VM, customers AS nombreCliente, DATE_FORMAT(VMDate, "%m/%d/%Y") AS VMDate, 
                            DATE_FORMAT(hostDate, "%m/%d/%Y") AS hostDate, prod AS familia, edicion, sockets, cores, cpu, 
                            Qty, SKU, type, 2 AS existe
                        FROM historicoAppsSPLA
                             INNER JOIN historicoSPLA ON historicoAppsSPLA.idHistorico = historicoSPLA.id
                             AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla1
                ' . $where . '
                GROUP BY VM, familia, edicion
                LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productos($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia
                FROM resumen_SPLA
                WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado
                GROUP BY familia
                
                UNION
                
                SELECT edicion AS familia
                FROM SPLAVirtualMachine
                WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                GROUP BY edicion
                ORDER BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function currentReportingFiltrado($cliente, $empleado, $customer, $sku, $pagina, $limite){
        try{
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaReportingCurrent($cliente, $empleado, $customer, $sku, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            if($customer != ""){
                $array[":customer"] = $customer;
                if($where == ""){
                    $where = "WHERE ";
                } else{
                    $where = " AND ";
                }
                $where .= ' nombreCliente = :customer '; 
            }
            
            if($sku != ""){
                $array[":SKU"] = $sku;
                if($where == ""){
                    $where = "WHERE ";
                } else{
                    $where = " AND ";
                }
                $where .= ' SKU LIKE :SKU ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, tipo, SKU, nombreCliente, familia, installed, Qty, price, 
                total, QtySale, priceSale, totalSale, SUM(existe) AS existe
                FROM (SELECT tabla.id,
                        tabla.tipo,
                        tabla.SKU,
                        tabla.nombreCliente,
                        tabla.familia,
                        SUM(tabla.installed) AS installed,
                        SUM(tabla.Qty) AS Qty,
                        SUM(tabla.price) AS price,
                        SUM(tabla.total) AS total,
                        SUM(tabla.QtySale) AS QtySale,
                        SUM(tabla.priceSale) AS priceSale,
                        SUM(tabla.totalSale) AS totalSale,
                        1 AS existe
                    FROM (SELECT SPLAVirtualMachine.id,
                            "VM" AS tipo,
                            SPLAVirtualMachine.SKU,
                            SPLAVirtualMachine.customers AS nombreCliente,
                            SPLAVirtualMachine.edicion AS familia,
                            1 AS installed,
                            SPLAVirtualMachine.Qty,
                            IFNULL(SPLASKU.price, 0) AS price,
                            ROUND(SPLAVirtualMachine.Qty * IFNULL(SPLASKU.price, 0), 0) AS total,
                            SPLAVirtualMachine.QtySale,
                            SPLAVirtualMachine.priceSale,
                            ROUND(SPLAVirtualMachine.QtySale * SPLAVirtualMachine.priceSale) AS totalSale
                        FROM SPLAVirtualMachine
                             LEFT JOIN SPLASKU ON SPLAVirtualMachine.SKU = SPLASKU.SKU
                        WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado

                        UNION

                        SELECT resumen_SPLA.id,
                            "App" AS tipo,
                            resumen_SPLA.SKU,
                            SPLAVirtualMachine.customers AS nombreCliente,
                            TRIM(CONCAT(resumen_SPLA.familia, " ", IFNULL(resumen_SPLA.edicion, ""), " ", IFNULL(resumen_SPLA.version, ""))) AS familia,
                            COUNT(resumen_SPLA.familia) AS installed,
                            resumen_SPLA.Qty,
                            IFNULL(SPLASKU.price, 0) AS price,
                            ROUND(resumen_SPLA.Qty * IFNULL(SPLASKU.price, 0), 0) AS total,
                            resumen_SPLA.QtySale,
                            resumen_SPLA.priceSale,
                            ROUND(resumen_SPLA.QtySale * resumen_SPLA.priceSale) AS totalSale
                        FROM SPLAVirtualMachine
                            INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                            SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                            LEFT JOIN SPLASKU ON resumen_SPLA.SKU = SPLASKU.SKU
                        WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                        GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla
                    GROUP BY tabla.nombreCliente, tabla.familia, tabla.SKU

                    UNION

                    SELECT 0 AS id, tipo, SKU, customers AS nombreCliente, prod AS familia, inst AS installed, 
                        Qty, price, Qty * price AS total, QtySale, priceSale, QtySale * priceSale AS totalSale, 2 AS existe
                    FROM historicoCurrentSPLA
                         INNER JOIN historicoSPLA ON historicoCurrentSPLA.idHistorico = historicoSPLA.id
                         AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla1
                ' . $where . '
                GROUP BY familia, SKU
                LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function currentReportingTotal($cliente, $empleado, $customer, $sku){
        try{
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
            if($customer != ""){
                $array[":customer"] = $customer;
                if($where == ""){
                    $where = "WHERE ";
                } else{
                    $where = " AND ";
                }
                $where .= ' nombreCliente = :customer '; 
            }
            
            if($sku != ""){
                $array[":SKU"] = $sku;
                if($where == ""){
                    $where = "WHERE ";
                } else{
                    $where = " AND ";
                }
                $where .= ' SKU = :SKU ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(familia) AS cantidad
                FROM (SELECT familia
                    FROM (SELECT tabla.id,
                            tabla.tipo,
                            tabla.SKU,
                            tabla.nombreCliente,
                            tabla.familia,
                            SUM(tabla.installed) AS installed,
                            SUM(tabla.Qty) AS Qty,
                            SUM(tabla.price) AS price,
                            SUM(tabla.total) AS total,
                            SUM(tabla.QtySale) AS QtySale,
                            SUM(tabla.priceSale) AS priceSale,
                            SUM(tabla.totalSale) AS totalSale
                        FROM (SELECT SPLAVirtualMachine.id,
                                "VM" AS tipo,
                                SPLAVirtualMachine.SKU,
                                SPLAVirtualMachine.customers AS nombreCliente,
                                SPLAVirtualMachine.edicion AS familia,
                                1 AS installed,
                                SPLAVirtualMachine.Qty,
                                IFNULL(SPLASKU.price, 0) AS price,
                                ROUND(SPLAVirtualMachine.Qty * IFNULL(SPLASKU.price, 0), 0) AS total,
                                SPLAVirtualMachine.QtySale,
                                SPLAVirtualMachine.priceSale,
                                ROUND(SPLAVirtualMachine.QtySale * SPLAVirtualMachine.priceSale) AS totalSale
                            FROM SPLAVirtualMachine
                                 LEFT JOIN SPLASKU ON SPLAVirtualMachine.SKU = SPLASKU.SKU
                            WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado

                            UNION

                            SELECT resumen_SPLA.id,
                                "App" AS tipo,
                                resumen_SPLA.SKU,
                                SPLAVirtualMachine.customers AS nombreCliente,
                                TRIM(CONCAT(resumen_SPLA.familia, " ", IFNULL(resumen_SPLA.edicion, ""), " ", IFNULL(resumen_SPLA.version, ""))) AS familia,
                                COUNT(resumen_SPLA.familia) AS installed,
                                resumen_SPLA.Qty,
                                IFNULL(SPLASKU.price, 0) AS price,
                                ROUND(resumen_SPLA.Qty * IFNULL(SPLASKU.price, 0), 0) AS total,
                                resumen_SPLA.QtySale,
                                resumen_SPLA.priceSale,
                                ROUND(resumen_SPLA.QtySale * resumen_SPLA.priceSale) AS totalSale
                            FROM SPLAVirtualMachine
                                INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                                SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                                LEFT JOIN SPLASKU ON resumen_SPLA.SKU = SPLASKU.SKU
                            WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                            GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla
                        GROUP BY tabla.nombreCliente, tabla.familia, tabla.SKU

                        UNION

                        SELECT 0 AS id, tipo, SKU, customers AS nombreCliente, prod AS familia, inst AS installed, Qty, price, Qty * price AS total, QtySale, priceSale, QtySale * priceSale AS totalSale
                        FROM historicoCurrentSPLA
                             INNER JOIN historicoSPLA ON historicoCurrentSPLA.idHistorico = historicoSPLA.id
                             AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla1
                ' . $where . '
                GROUP BY familia, SKU) tabla2');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function reportingValidationFiltrado($cliente, $empleado, $producto, $pagina, $limite){
        try{
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaReportingValidation($cliente, $empleado, $producto, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
                       
            if($producto != ""){
                $array[":producto"] = "%" . $producto . "%";
                $where .= ' WHERE familia LIKE :producto ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, tipo, familia, SKU, Qty, price, total, SUM(existe) AS existe
                FROM (SELECT tabla.id,
                        tabla.tipo,
                        tabla.familia,
                        tabla.SKU,
                        SUM(tabla.Qty) AS Qty,
                        SUM(tabla.price) AS price,
                        SUM(tabla.total) AS total,
                        1 AS existe
                    FROM (SELECT SPLAVirtualMachine.id,
                            "VM" AS tipo,
                            SPLAVirtualMachine.edicion AS familia,
                            SPLAVirtualMachine.SKU,
                            SPLAVirtualMachine.Qty,
                            IFNULL(SPLASKU.price, 0) AS price,
                            ROUND(SPLAVirtualMachine.Qty * IFNULL(SPLASKU.price, 0), 0) AS total
                        FROM SPLAVirtualMachine
                             LEFT JOIN SPLASKU ON SPLAVirtualMachine.SKU = SPLASKU.SKU
                        WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado

                        UNION

                        SELECT resumen_SPLA.id,
                            "App" AS tipo,
                            TRIM(CONCAT(resumen_SPLA.familia, " ", IFNULL(resumen_SPLA.edicion, ""), " ", IFNULL(resumen_SPLA.version, ""))) AS familia,
                            resumen_SPLA.SKU,
                            resumen_SPLA.Qty,
                            IFNULL(SPLASKU.price, 0) AS price,
                            ROUND(resumen_SPLA.Qty * IFNULL(SPLASKU.price, 0), 0) AS total
                        FROM SPLAVirtualMachine
                            INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                            SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                            LEFT JOIN SPLASKU ON resumen_SPLA.SKU = SPLASKU.SKU
                        WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                        GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla
                    GROUP BY tabla.familia, tabla.SKU

                    UNION

                    SELECT 0 AS id,
                        tipo,
                        prod AS familia,
                        SKU,
                        Qty,
                        price,
                        Qty * price AS total,
                        2 AS existe
                    FROM historicoCurrentSPLA
                         INNER JOIN historicoSPLA ON historicoCurrentSPLA.idHistorico = historicoSPLA.id
                         AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla1
                GROUP BY familia
                LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function reportingValidationTotal($cliente, $empleado, $producto){
        try{
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            $where = "";
                       
            if($producto != ""){
                $array[":producto"] = "%" . $producto . "%";
                $where .= ' WHERE familia LIKE :producto ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(familia) AS cantidad
                FROM (SELECT familia
                    FROM (SELECT tabla.id,
                        tabla.tipo,
                        tabla.familia,
                        tabla.SKU,
                        SUM(tabla.Qty) AS Qty,
                        SUM(tabla.price) AS price,
                        SUM(tabla.total) AS total,
                        1 AS existe
                        FROM (SELECT SPLAVirtualMachine.id,
                                "VM" AS tipo,
                                SPLAVirtualMachine.edicion AS familia,
                                SPLAVirtualMachine.SKU,
                                SPLAVirtualMachine.Qty,
                                IFNULL(SPLASKU.price, 0) AS price,
                                ROUND(SPLAVirtualMachine.Qty * IFNULL(SPLASKU.price, 0), 0) AS total
                            FROM SPLAVirtualMachine
                                 LEFT JOIN SPLASKU ON SPLAVirtualMachine.SKU = SPLASKU.SKU
                            WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado

                            UNION

                            SELECT resumen_SPLA.id,
                                "App" AS tipo,
                                TRIM(CONCAT(resumen_SPLA.familia, " ", IFNULL(resumen_SPLA.edicion, ""), " ", IFNULL(resumen_SPLA.version, ""))) AS familia,
                                resumen_SPLA.SKU,
                                resumen_SPLA.Qty,
                                IFNULL(SPLASKU.price, 0) AS price,
                                ROUND(resumen_SPLA.Qty * IFNULL(SPLASKU.price, 0), 0) AS total
                            FROM SPLAVirtualMachine
                                INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                                SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                                LEFT JOIN SPLASKU ON resumen_SPLA.SKU = SPLASKU.SKU
                            WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                            GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla
                        GROUP BY tabla.familia, tabla.SKU

                        UNION

                        SELECT 0 AS id,
                            tipo,
                            prod AS familia,
                            SKU,
                            Qty,
                            price,
                            Qty * price AS total,
                            2 AS existe
                        FROM historicoCurrentSPLA
                             INNER JOIN historicoSPLA ON historicoCurrentSPLA.idHistorico = historicoSPLA.id
                             AND historicoSPLA.cliente = :cliente AND historicoSPLA.fecha = (SELECT MAX(fecha) FROM historicoSPLA WHERE cliente = :cliente)) tabla1
                    ' . $where . '
                    GROUP BY familia) tabla2');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
}