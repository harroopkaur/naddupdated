<?php
class resumenOracle extends General{
    public  $lista = array();
    public  $error = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_oracle (cliente, empleado, equipo, familia, edicion, version) VALUES '
            . '(:cliente, :empleado, :equipo, :familia, :edicion, :version)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumen_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumen_oracle WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos5($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_oracle.id,
                resumen_oracle.cliente,
                resumen_oracle.equipo,
                IF(filepcs_oracle.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_oracle.familia,
                resumen_oracle.edicion,
                resumen_oracle.version,
                /*DATE_FORMAT(resumen_oracle.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,*/
                CASE
                    WHEN detalles_equipo_oracle.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_oracle.rango = 2 OR detalles_equipo_oracle.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_oracle
            INNER JOIN detalles_equipo_oracle ON resumen_oracle.equipo = detalles_equipo_oracle.equipo
            INNER JOIN filepcs_oracle ON resumen_oracle.equipo = filepcs_oracle.cn
        WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado AND resumen_oracle.familia = :familia
        GROUP BY resumen_oracle.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia)); 
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_datos5Sam($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_oracleSam.id,
                resumen_oracleSam.cliente,
                resumen_oracleSam.equipo,
                IF(detalles_equipo_oracleSam.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_oracleSam.familia,
                resumen_oracleSam.edicion,
                resumen_oracleSam.version,
                /*DATE_FORMAT(resumen_oracleSam.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,*/
                /*CASE
                    WHEN detalles_equipo_oracleSam.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_oracleSam.rango = 2 OR detalles_equipo_oracleSam.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_oracleSam
            INNER JOIN detalles_equipo_oracleSam ON resumen_oracleSam.equipo = detalles_equipo_oracleSam.equipo
        WHERE resumen_oracleSam.cliente = :cliente AND resumen_oracleSam.familia = :familia
        GROUP BY resumen_oracleSam.id
        ORDER BY resumen_oracleSam.familia, resumen_oracleSam.edicion, resumen_oracleSam.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5SamArchivo($archivo, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_oracleSam.id,
                resumen_oracleSam.cliente,
                resumen_oracleSam.equipo,
                IF(detalles_equipo_oracleSam.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_oracleSam.familia,
                resumen_oracleSam.edicion,
                resumen_oracleSam.version,
                /*DATE_FORMAT(resumen_oracleSam.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,*/
                CASE
                    WHEN detalles_equipo_oracleSam.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_oracleSam.rango = 2 OR detalles_equipo_oracleSam.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_oracleSam
            INNER JOIN detalles_equipo_oracleSam ON resumen_oracleSam.equipo = detalles_equipo_oracleSam.equipo
        WHERE resumen_oracleSam.archivo = :archivo AND resumen_oracleSam.familia = :familia
        GROUP BY resumen_oracleSam.id
        ORDER BY resumen_oracleSam.familia, resumen_oracleSam.edicion, resumen_oracleSam.version");
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_oracle.id,
                    resumen_oracle.cliente,
                    resumen_oracle.equipo,
                    IF(filepcs_oracle.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_oracle.familia,
                    resumen_oracle.edicion,
                    resumen_oracle.version,
                    /*DATE_FORMAT(resumen_oracle.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,*/
                    CASE
                        WHEN detalles_equipo_oracle.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_oracle.rango = 2 OR detalles_equipo_oracle.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_oracle
                    INNER JOIN detalles_equipo_oracle ON resumen_oracle.equipo = detalles_equipo_oracle.equipo
                    INNER JOIN filepcs_oracle ON resumen_oracle.equipo = filepcs_oracle.cn
                WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado AND resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_oracle.edicion = :edicion
                GROUP BY resumen_oracle.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Asignacion($cliente, $familia, $edicion, $asignacion, $asignaciones) {        
        try{     
            $where = "";
            $array = array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion);
                
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_oracle.id,
                    resumen_oracle.cliente,
                    resumen_oracle.equipo,
                    IF(detalles_equipo_oracle.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_oracle.familia,
                    resumen_oracle.edicion,
                    resumen_oracle.version,
                    /*DATE_FORMAT(resumen_oracle.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,*/
                    CASE
                        WHEN detalles_equipo_oracle.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_oracle.rango = 2 OR detalles_equipo_oracle.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_oracle
                    INNER JOIN detalles_equipo_oracle ON resumen_oracle.equipo = detalles_equipo_oracle.equipo 
                    AND resumen_oracle.cliente = detalles_equipo_oracle.cliente " . $where . "
                WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_oracle.edicion = :edicion
                GROUP BY resumen_oracle.id");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_datos5Agrupado($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT resumen_oracle.familia,
                            resumen_oracle.edicion,
                            resumen_oracle.version
                        FROM resumen_oracle
                            INNER JOIN detalles_equipo_oracle ON resumen_oracle.equipo = detalles_equipo_oracle.equipo
                            INNER JOIN filepcs_oracle ON resumen_oracle.equipo = filepcs_oracle.cn
                        WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.familia = :familia
                        GROUP BY resumen_oracle.id) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_oracle.familia,
                        resumen_oracle.edicion,
                        resumen_oracle.version
                    FROM resumen_oracle
                        INNER JOIN detalles_equipo_oracle ON resumen_oracle.equipo = detalles_equipo_oracle.equipo
                        INNER JOIN filepcs_oracle ON resumen_oracle.equipo = filepcs_oracle.cn
                    WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado AND resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_oracle.edicion = :edicion
                    GROUP BY resumen_oracle.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones) {        
        try{    
            $array = array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_oracle.familia,
                        resumen_oracle.edicion,
                        resumen_oracle.version
                    FROM resumen_oracle
                        INNER JOIN detalles_equipo_oracle ON resumen_oracle.equipo = detalles_equipo_oracle.equipo 
                        AND resumen_oracle.cliente = detalles_equipo_oracle.cliente " . $where . " 
                    WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_oracle.edicion = :edicion
                    GROUP BY resumen_oracle.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function graficoBalanza($cliente, $empleado, $familia){
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente AND detalles_equipo_oracle.empleado = resumen_oracle.empleado
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function graficoBalanzaEdicion($cliente, $empleado, $familia, $edicion){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente AND detalles_equipo_oracle.empleado = resumen_oracle.empleado
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.edicion = :edicion
                    AND resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function graficoBalanzaEdicion1($cliente, $empleado, $familia){
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente AND detalles_equipo_oracle.empleado = resumen_oracle.empleado
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia = :familia
                    AND resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function duplicado($cliente, $equipo, $familia, $edicion, $version){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.familia) AS cantidad
                FROM (SELECT resumen_oracle.familia
                    FROM resumen_oracle
                    WHERE resumen_oracle.cliente = :cliente AND empleado = :empleado AND resumen_oracle.equipo = :equipo 
                    AND resumen_oracle.familia = :familia AND resumen_oracle.edicion = :edicion AND resumen_oracle.version = :version
                    GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version) tabla");
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    } 
    
    /*function duplicadoSam($cliente, $equipo, $familia, $edicion, $version){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM resumen_oracleSam
                WHERE resumen_oracleSam.cliente = :cliente AND resumen_oracleSam.equipo = :equipo AND resumen_oracleSam.familia = :familia 
                AND resumen_oracleSam.edicion = :edicion AND resumen_oracleSam.version = :version");
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/   
    
    function duplicadoSamArchivo($archivo, $equipo, $familia, $edicion, $version){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM resumen_oracleSam
                WHERE resumen_oracleSam.archivo = :archivo AND resumen_oracleSam.equipo = :equipo AND resumen_oracleSam.familia = :familia 
                AND resumen_oracleSam.edicion = :edicion AND resumen_oracleSam.version = :version");
            $sql->execute(array(':archivo'=>$archivo, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }  
    
    function graficoBalanzaAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        try{    
            $where = " resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
            $array = array(':cliente'=>$cliente, ':familia'=>$familia);
           
            if($edicion == ""){
                $where .= " AND (resumen_oracle.edicion = '' OR resumen_oracle.edicion IS NULL) ";
            } else if($edicion != "Todo"){
                $where .= " AND resumen_oracle.edicion = :edicion ";
                $array[':edicion'] = $edicion;
            }
            
            if($asignacion != ""){
                $where1 = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where1 = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT IFNULL(COUNT(tabla.familia), 0) AS conteo,
                        tabla.rango
                FROM (SELECT resumen_oracle.familia,
                        detalles_equipo_oracle.rango
                    FROM resumen_oracle
                        INNER JOIN detalles_equipo_oracle ON resumen_oracle.cliente = detalles_equipo_oracle.cliente
                        AND resumen_oracle.equipo = detalles_equipo_oracle.equipo " . $where1 . "
                    WHERE " . $where . "
                    AND resumen_oracle.cliente = :cliente 
                    GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version) tabla
                GROUP BY tabla.rango");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}