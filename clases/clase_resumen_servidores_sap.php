<?php
class ResumenServidoresSAP extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $compSoft, $familia, $edicion, $version) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumenServidorSap (cliente, empleado, equipo, compSoft, familia, edicion, version) '
            . 'VALUES (:cliente, :empleado, :equipo, :compSoft, :familia, :edicion, :version)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':compSoft'=>$compSoft, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumenServidorSap (cliente, empleado, equipo, compSoft, familia, edicion, version) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumenServidorSap WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumenServidorSap WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT equipo,
                    compSoft,
                    familia,
                    edicion,
                    version
                FROM resumenServidorSap
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenServidorSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datosAgrupado($cliente, $empleado, $familia) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumenServidorSap
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}