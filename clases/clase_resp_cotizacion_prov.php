<?php
class respCotizacionProv extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
   
    function insertar($idCotizacion, $nroCotizacion, $proveedor, $archivo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO respCotizProv (idCotizacionProv, nroCotizacion, proveedor, archivo) VALUES '
            . '(:idCotizacion, :nroCotizacion, :proveedor, :archivo)');
            $sql->execute(array(':idCotizacion'=>$idCotizacion, ':nroCotizacion'=>$nroCotizacion, ':proveedor'=>$proveedor, ':archivo'=>$archivo));
            
            $sql = $this->conn->prepare('UPDATE cotizacionProveedor SET estatus = 2 WHERE id = :idCotizacion');
            $sql->execute(array(':idCotizacion'=>$idCotizacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}