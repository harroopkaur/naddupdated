<?php
class DetallesUnixSolaris extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $os, $memoria, $cpu, $tipoCPU, $core) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_UnixSolaris (cliente, empleado, equipo, os, memoria, cpu, versionCPU, core) '
            . 'VALUES (:cliente, :empleado, :equipo, :os, :memoria, :cpu, :tipoCPU, :core)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':os'=>$os, ':memoria'=>$memoria, ':cpu'=>$cpu, ':tipoCPU'=>$tipoCPU, ':core'=>$core));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalles_equipo_UnixSolaris WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
}