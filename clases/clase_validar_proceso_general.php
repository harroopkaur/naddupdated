<?php
class clase_validar_proceso_general extends General{
    private $temp;
    public $nombreArchivo;
    
    public $archivoConsolidado;
    public $archivoEscaneo;
    public $archivoProcesadores;
    public $archivoEquipos;
    public $archivoUsuarioEquipos;
    public $archivoSQL;
    
    //inicio variables AddRemove
    public $procesarAddRemove; 
    public $iDatoControl;
    public $iHostName;
    public $iRegistro;
    public $iEditor;
    public $iVersion;
    public $iDiaInstalacion;
    public $iSoftware;
    //fin variables AddRemove
    
    //inicio variables Escaneo
    public $procesarEscaneo;
    public $jHostname;
    public $jStatus;
    public $jError;
    //fin variables Escaneo
    
    //inicio variables Procesadores
    public $procesarProcesadores;
    public $kDatoControl;
    public $kHostName;
    public $kTipoCPU;
    public $kCPUs;
    public $kCores;
    public $kProcesadoresLogicos;
    public $kTipoEscaneo;
    //fin variables Procesadores
    
    //inicio variables Tipo Equipo
    public $procesarEquipos;
    public $lDatoControl;
    public $lHostName;
    public $lFabricante;
    public $lModelo;
    //fin variables Tipo Equipo
    
    //inicio variables Usuario Equipo
    public $procesarUsuarioEquipos;
    public $mDatoControl; 
    public $mHostName;
    public $mDominio;
    public $mUsuario;
    public $mIP;
    //fin variables Usuario Equipo
    
    //inicio variables SQL
    public $procesarSQL;
    public $nDatoControl;
    public $nHostName;
    public $nEdicionSQL;
    public $nVersionSQL;
    //fin variables SQL
    
    //inicio variables Otros
    public $oNombreEquipo;
    public $oSoftware;
    public $oVersion;
    public $oFechaInstal;
    public $oFabricante;
    public $oInstalAdmin;
    public $oInstalRed;
    
    private $nombreEquipo;
    private $software; 
    private $version;
    private $fechaInstal; 
    private $fabricante; 
    private $instalAdmin; 
    private $instalRed;
    //fin variables Otros
    
    //inicio varibles Consolidado SO
    public $procesarSO;
    public $archivoConsolidadoSO;
    public $pDatoControl;
    public $pHostName;
    public $pSistemaOperativo;
    public $pFechaCreacion;
    //fin variables Consolidado SO
    
    function validar_archivo($name, $ext){
        $error = 0;
        $nombre = $name;
     
        // Validaciones
        if($nombre != ""){
            $extension = explode(".",$nombre);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != $ext) && ($extension[$long] != strtoupper($ext))) { 
                $error = 1; 
            }  
        }else{
            $error = 2;	
        }
        
        return $error;
    }
    
    function existenCarpetasLAD($nombFab){
        if(!file_exists($GLOBALS['app_root'] . "/" . $nombFab . "/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'])){
            mkdir($GLOBALS['app_root'] . "/" . $nombFab . "/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'], 0755, true);
        }

        if(!file_exists($GLOBALS['app_root'] . "/" . $nombFab . "/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/")){
            mkdir($GLOBALS['app_root'] . "/" . $nombFab . "/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/", 0755, true);
        }
    }
    
    function validarArchivosRar($temporal, $fabricante, $passLAD, $incremento = false){
        $error = 0;
        $this->temp = $temporal;

        $rar_file = rar_open($this->temp, $passLAD);
            
        $entries = rar_list($rar_file);   
        
        if($fabricante == "microsoft"){
            $direcEntry = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/";
            if($incremento == true){
                $direcEntry .= "incremento/";
            }
       
            $error = $this->validarRarMicrosoft($entries, $direcEntry);
        } else if($fabricante == "Cloud" || $fabricante == "Diagnostic" || $fabricante == "appEscaneo"){
            $direcEntry = "archivosLAD/";
                   
            $error = $this->validarRarDiagnosticos($entries, $direcEntry);
        } 
       
        rar_close($rar_file);
        
        return $error;
    }
    
    /*function validarArchivosRar($temporal, $fabricante, $passLAD, $incremento = false){
        $error = 0;
        $this->temp = $temporal;
                
        $rar_file = rar_open($this->temp, $passLAD) or die("Can't open Rar archive");

        $entries = rar_list($rar_file);   

        if($fabricante == "microsoft"){
            $direcEntry = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/";
            if($incremento == true){
                $direcEntry .= "incremento/";
            }

            $error = $this->validarRarMicrosoft($entries, $direcEntry);
        } else if($fabricante == "Cloud" || $fabricante == "Diagnostic"){
            $direcEntry = "archivosLAD/";

            $error = $this->validarRarDiagnosticos($entries, $direcEntry);
        } 

        rar_close($rar_file);
        
        return $error;
    }*/
    
    function validarRarMicrosoft($entries, $direcEntry){
        $error = 0;
        $archivosExtraer = 6;
        $archivosExtraidos = 0;
        foreach ($entries as $entry) {
            if($entry->getName() == "Consolidado Addremove.csv" || $entry->getName() == "Consolidado Procesadores.csv"
            || $entry->getName() == "Consolidado Tipo de Equipo.csv" || $entry->getName() == "Consolidado Usuario-Equipo.csv"
            || $entry->getName() == "Resultados_Escaneo.csv" || $entry->getName() == "Consolidado SQL.csv"){
                $result = $this->validarExtraccion($entry, $direcEntry);
                $archivosExtraidos += $result;
            }
        }

        if($archivosExtraer > $archivosExtraidos){
            $error = 8;
        }
        
        return $error;
    }
    
    function validarRarDiagnosticos($entries, $direcEntry){
        $error = 0;
        $archivosExtraer = 7;
        $archivosExtraidos = 0;
        foreach ($entries as $entry) {
            if($entry->getName() == "Consolidado Addremove.csv" || $entry->getName() == "Consolidado Procesadores.csv"
            || $entry->getName() == "Consolidado Tipo de Equipo.csv" || $entry->getName() == "Consolidado Usuario-Equipo.csv" 
            || $entry->getName() == "Resultados_Escaneo.csv" || $entry->getName() == "Consolidado SQL.csv" 
            || $entry->getName() == "Consolidado Sistema Operativo.csv"){
                $result = $this->validarExtraccion($entry, $direcEntry);
                $archivosExtraidos += $result;
            }
        }

        if($archivosExtraer > $archivosExtraidos){
            $error = 8;
        }
        
        return $error;
    }
    
    function validarExtraccion($entry, $direcEntry){
        $result = 0;
        if($entry->extract($direcEntry)){
            $result = 1; 
        }
        
        return $result;
    }
    
    function copiarArchivosLAD($fabricante, $client_id = 0, $client_empleado = 0, $incremento = false){
                
        if($fabricante == "microsoft"){
            $dir = "archivos_csvf1/" . $client_id . "/" . $client_empleado . "/";
        } else if($fabricante == "Cloud" || $fabricante == "Diagnostic" || $fabricante == "appEscaneo"){
            $dir = "archivosLAD/";
        }
        
        if($incremento == true){
            $dir .= "incremento/";
        }
        
        $fecha = date("dmYHis");
        
        $this->archivoConsolidado = "";
        $this->archivoProcesadores = "";
        $this->archivoEquipos = "";
        $this->archivoEscaneo = "";
        $this->archivoSQL = "";
        
        if($fabricante ==  "microsoft"){
            $this->archivoUsuarioEquipos = "";
        }
                   
        $this->copiarLAD($dir, $fecha, $fabricante);
    }
    
    function copiarLAD($dir, $fecha, $fabricante){
        $this->nombreArchivo = 'LAD_Output' . $fecha . '.rar';
        move_uploaded_file($this->temp, $dir . $this->nombreArchivo); 
        copy($dir . "Consolidado Addremove.csv", $dir . "Consolidado Addremove" . $fecha . ".csv");
        copy($dir . "Consolidado Procesadores.csv", $dir . "Consolidado Procesadores" . $fecha . ".csv");
        copy($dir . "Consolidado Tipo de Equipo.csv", $dir . "Consolidado Tipo de Equipo" . $fecha . ".csv");
        copy($dir . "Resultados_Escaneo.csv", $dir . "Resultados_Escaneo" . $fecha . ".csv");
        copy($dir . "Consolidado SQL.csv", $dir . "Consolidado SQL" . $fecha . ".csv");
        
        $this->archivoConsolidado = $dir . "Consolidado Addremove" . $fecha . ".csv";
        $this->archivoProcesadores = $dir . "Consolidado Procesadores" . $fecha . ".csv";
        $this->archivoEquipos = $dir . "Consolidado Tipo de Equipo" . $fecha . ".csv";
        $this->archivoEscaneo = $dir . "Resultados_Escaneo" . $fecha . ".csv";
        $this->archivoSQL = $dir . "Consolidado SQL" . $fecha . ".csv";        
        
        if($fabricante == "microsoft"){
            copy($dir . "Consolidado Usuario-Equipo.csv", $dir . "Consolidado Usuario-Equipo" . $fecha . ".csv");
            $this->archivoUsuarioEquipos = $dir . "Consolidado Usuario-Equipo" . $fecha . ".csv";
        } else if($fabricante == "appEscaneo"){
            copy($dir . "Consolidado Sistema Operativo.csv", $dir . "Consolidado Sistema Operativo" . $fecha . ".csv");
            $this->archivoConsolidadoSO = $dir . "Consolidado Sistema Operativo" . $fecha . ".csv";
        }
    }    
    
    //inicio proceso general de validacion
    function cicloCabecera($fichero, $fila, $opcion){
        $i = 1;
       
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {
            if($i == $fila){
                $this->ejecutarValidacion($opcion, $datos);
            } else if($i > $fila){
                break;
            }

            $i++;
        }
    }
    
    function ejecutarValidacion($opcion, $datos){ 
        //opcion = 1 AddRemove
        if($opcion == -1){
            $this->validCabOtros($datos);
        } else if($opcion == 1){
            $this->validCabAddRemove($datos);
        } else if($opcion == 2){
            $this->validCabEscaneo($datos);
        } else if($opcion == 3){
            $this->validCabProcesadores($datos);
        } else if($opcion == 4){
            $this->validCabTipoEquipo($datos);
        } else if($opcion == 5){
            $this->validCabUsuarioEquipo($datos);
        } else if($opcion == 6){
            $this->validCabSQL($datos);
        } else if($opcion == 7){
            $this->validCabSO($datos);
        }
    }
    //fin proceso general de validacion
    
    //inicio validaciones AddRemove
    function validarConsolidadoAddRemove($archivoConsolidado){
        $error = 0;
        
        if($this->obtenerSeparadorUniversal($archivoConsolidado, 2, 7) === true){
            if (($fichero = fopen($archivoConsolidado, "r")) !== false) {
                $this->iDatoControl = $this->iHostName = $this->iRegistro = $this->iEditor = $this->iVersion = $this->iDiaInstalacion = $this->iSoftware = -1;
                $this->procesarAddRemove = true;
                $this->cicloCabecera($fichero, 2, 1);
                fclose($fichero);

                $error = $this->validarResultadosAddRemove();
            }
        } else{
            //$error = 10;
            $this->procesarAddRemove = false;
        }
        
        return $error;
    }
    
    function validCabAddRemove($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->iDatoControl = $this->asignarColumnasi($this->iDatoControl, $datos[$k], $k, "Dato de Control");
            $this->iHostName = $this->asignarColumnasi($this->iHostName, $datos[$k], $k, "HostName");
            $this->iRegistro = $this->asignarColumnasi($this->iRegistro, $datos[$k], $k, "Registro");
            $this->iEditor = $this->asignarColumnasi($this->iEditor, $datos[$k], $k, "Editor");
            $this->iVersion = $this->asignarColumnasi1($this->iVersion, $datos[$k], $k, "Version", "Versión");
            $this->iDiaInstalacion = $this->asignarColumnasi1($this->iDiaInstalacion, $datos[$k], $k, "Día de Instalacion", "Día de Instalación");
            $this->iSoftware = $this->asignarColumnasi($this->iSoftware, $datos[$k], $k, "Software");
        }
    }
    
    function validarResultadosAddRemove(){
        $error = 0;
        if($this->iDatoControl == -1 && $this->iHostName == -1 && $this->iRegistro == -1 && $this->iEditor == -1 && 
        $this->iVersion == -1 && $this->iDiaInstalacion == -1 && $this->iSoftware == -1){
            $this->procesarAddRemove = false;
        } else if($this->iDatoControl == -1 || $this->iHostName == -1 || $this->iRegistro == -1 || $this->iEditor == -1 || 
        $this->iVersion == -1 || $this->iDiaInstalacion == -1 || $this->iSoftware == -1){
            $error = 3;
        }
        return $error;
    }
    //fin validaciones AddRemove
    
    //inicio validaciones Escaneo
    function validarEscaneo($archivoEscaneo){
        $error = 0;
        
        if($this->obtenerSeparadorUniversal($archivoEscaneo, 2, 3) === true){
            if (($fichero = fopen($archivoEscaneo, "r")) !== false) {
                $this->jHostname = $this->jStatus = $this->jError = -1;
                $this->procesarEscaneo = true;
                $this->cicloCabecera($fichero, 2, 2);
                fclose($fichero);

                $error = $this->validarResultadosEscaneo();
            }
        } else{
            $this->procesarEscaneo = false;
        }
        
        return $error;
    }
    
    function validCabEscaneo($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->jHostname = $this->asignarColumnasi($this->jHostname, $datos[$k], $k, "Hostname");
            $this->jStatus = $this->asignarColumnasi($this->jStatus, $datos[$k], $k, "Status");
            $this->jError = $this->asignarColumnasi($this->jError, $datos[$k], $k, "Error");
        }
    }
    
    function validarResultadosEscaneo(){
        $error = 0;
        if($this->jHostname == -1 && $this->jStatus == -1 && $this->jError == -1){
            $this->procesarEscaneo = false;
        } else if($this->jHostname == -1 || $this->jStatus == -1 || $this->jError == -1){
            $error = 4;
        }
        
        return $error;
    }
    //fin validaciones Escaneo
    
    //inicio validaciones Consolidado Procesadores
    function validarProcesadores($archivoProcesadores){
        $error = 0;
        
        if($this->obtenerSeparadorUniversal($archivoProcesadores, 2, 7) === true){
            if (($fichero = fopen($archivoProcesadores, "r")) !== false) {
                $this->kDatoControl = $this->kHostName = $this->kTipoCPU = $this->kCPUs = $this->kCores = $this->kProcesadoresLogicos = $this->kTipoEscaneo = -1;
                $this->procesarProcesadores = true;
                $this->cicloCabecera($fichero, 2, 3);
                fclose($fichero);

                $error = $this->validarResultadosProcesadores();
            }
        } else{
            $this->procesarProcesadores = false;
        }
        
        return $error;
    }
    
    function validCabProcesadores($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->kDatoControl = $this->asignarColumnasi($this->kDatoControl, $datos[$k], $k, "Dato de Control");
            $this->kHostName = $this->asignarColumnasi($this->kHostName, $datos[$k], $k, "HostName");
            $this->kTipoCPU = $this->asignarColumnasi($this->kTipoCPU, $datos[$k], $k, "Tipo de CPU");
            $this->kCPUs = $this->asignarColumnasi($this->kCPUs, $datos[$k], $k, "CPUs");
            $this->kCores = $this->asignarColumnasi($this->kCores, $datos[$k], $k, "Cores");
            $this->kProcesadoresLogicos = $this->asignarColumnasi1($this->kProcesadoresLogicos, $datos[$k], $k, "Procesadores Logicos", "Procesadores Lógicos");
            $this->kTipoEscaneo = $this->asignarColumnasi2($this->kTipoEscaneo, $datos[$k], $k, "Tipo de Escaneo");
        }
    }
    
    function validarResultadosProcesadores(){
        $error = 0;
        
        if($this->kDatoControl == -1 && $this->kHostName == -1 && $this->kTipoCPU == -1 && $this->kCPUs == -1 && 
        $this->kCores == -1 && $this->kProcesadoresLogicos == -1 && $this->kTipoEscaneo == -1){
            $this->procesarProcesadores = false;
        } else if($this->kDatoControl == -1 || $this->kHostName == -1 || $this->kTipoCPU == -1 || $this->kCPUs == -1 || 
        $this->kCores == -1 || $this->kProcesadoresLogicos == -1 || $this->kTipoEscaneo == -1){
            $error = 6;
        }
        
        return $error;
    }
    //fin validaciones Consolidado Procesadores
    
    //inicio validaciones tipo de equipo
    function validarTipoEquipo($archivoEquipos){
        $error = 0;
        
        if($this->obtenerSeparadorUniversal($archivoEquipos, 2, 4) === true){
            if (($fichero = fopen($archivoEquipos, "r")) !== false) {
                $this->lDatoControl = $this->lHostName = $this->lFabricante = $this->lModelo = -1;
                $this->procesarEquipos = true;
                $this->cicloCabecera($fichero, 2, 4);
                fclose($fichero);

                $error = $this->validarResultadosTipoEquipo();
            }
        } else{
            $this->procesarEquipos = false;
        }
        
        return $error;
    }
    
    function validCabTipoEquipo($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->lDatoControl = $this->asignarColumnasi($this->lDatoControl, $datos[$k], $k, "Dato de Control");
            $this->lHostName = $this->asignarColumnasi($this->lHostName, $datos[$k], $k, "HostName");
            $this->lFabricante = $this->asignarColumnasi($this->lFabricante, $datos[$k], $k, "Fabricante");
            $this->lModelo = $this->asignarColumnasi($this->lModelo, $datos[$k], $k, "Modelo");
        }
    }
    
    function validarResultadosTipoEquipo(){
        $error = 0;
        
        if($this->lDatoControl == -1 && $this->lHostName == -1 && $this->lFabricante == -1 && $this->lModelo == -1){
            $this->procesarEquipos = false;
        } else if($this->lDatoControl == -1 || $this->lHostName == -1 || $this->lFabricante == -1 || $this->lModelo == -1){
            $error = 7;
        }
        
        return $error;
    }
    //fin validaciones tipo de equipo
    
    //inicio validaciiones usuario equipo
    function validarUsuarioEquipo($archivoUsuarioEquipos){
        $error = 0;
        
        if($this->obtenerSeparadorUniversal($archivoUsuarioEquipos, 2, 5) === true){
            if (($fichero = fopen($archivoUsuarioEquipos, "r")) !== false) {
                $this->mDatoControl = $this->mHostName = $this->mDominio = $this->mUsuario = $this->mIP = -1;
                $this->procesarUsuarioEquipos = true;
                $this->cicloCabecera($fichero, 2, 5);
                fclose($fichero);

                $error = $this->validarResultadosUsuarioEquipo();
            }
        } else{
            $this->procesarUsuarioEquipos = false;
        }
        
        return $error;
    }
    
    function validCabUsuarioEquipo($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->mDatoControl = $this->asignarColumnasi($this->mDatoControl, $datos[$k], $k, "Dato de Control"); 
            $this->mHostName = $this->asignarColumnasi($this->mHostName, $datos[$k], $k, "HostName");
            $this->mDominio = $this->asignarColumnasi($this->mDominio, $datos[$k], $k, "Dominio");
            $this->mUsuario = $this->asignarColumnasi($this->mUsuario, $datos[$k], $k, "Usuario");
            $this->mIP = $this->asignarColumnasi($this->mIP, $datos[$k], $k, "IP");
        }
    }
    
    function validarResultadosUsuarioEquipo(){
        $error = 0;
        
        if($this->mDatoControl == -1 && $this->mHostName == -1 && $this->mDominio == -1 && $this->mUsuario == -1 && $this->mIP == -1){
            $this->procesarUsuarioEquipos = false;
        } else if($this->mDatoControl == -1 || $this->mHostName == -1 || $this->mDominio == -1 || $this->mUsuario == -1 || $this->mIP == -1){
            $error = 9;
        }
        
        return $error;
    }
    //fin validaciones usuario equipo
    
    //inicio validaciones SQL
    function validarSQL($archivoSQL){
        $error = 0;
        
        if($this->obtenerSeparadorUniversal($archivoSQL, 2 ,6) === true){
            if (($fichero = fopen($archivoSQL, "r")) !== false) {
                $this->nDatoControl = $this->nHostName = $this->nEdicionSQL = $this->nVersionSQL = -1;
                $this->procesarSQL = true;
                $this->cicloCabecera($fichero, 2, 6);
                fclose($fichero);

                $error = $this->validarResultadosSQL();
            }
        } else{
            $this->procesarSQL = false;
        }

        return $error;
    }
    
    function validCabSQL($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->nDatoControl = $this->asignarColumnasi($this->nDatoControl, $datos[$k], $k, "Dato de Control");
            $this->nHostName = $this->asignarColumnasi($this->nHostName, $datos[$k], $k, "HostName");
            $this->nEdicionSQL = $this->asignarColumnasi1($this->nEdicionSQL, $datos[$k], $k, "Edicion de SQL:", "Edición de SQL:");
            $this->nVersionSQL = $this->asignarColumnasi1($this->nVersionSQL, $datos[$k], $k, "Version de SQL:", "Versión de SQL:");
        }
    }
    
    function validarResultadosSQL(){
        $error = 0;
        
        if($this->nDatoControl == -1 && $this->nHostName == -1 && $this->nEdicionSQL == -1 && $this->nVersionSQL == -1){
            $this->procesarSQL = false;
        } else if($this->nDatoControl == -1 || $this->nHostName == -1 || $this->nEdicionSQL == -1 || $this->nVersionSQL == -1){
            $error = 16;
        }
        
        return $error;
    }
    //fin validaciones SQL
    
    //inicio validaciones Consolidado SO
    function validarSO($archivoSO){
        $error = 0;
        
        if($this->obtenerSeparadorUniversal($archivoSO, 2 ,4) === true){
            if (($fichero = fopen($archivoSO, "r")) !== false) {
                $this->pDatoControl = $this->pHostName = $this->pSistemaOperativo = $this->pFechaCreacion = -1;
                $this->procesarSO = true;
                $this->cicloCabecera($fichero, 2, 7);
                fclose($fichero);

                $error = $this->validarResultadosSO();
            }
        } else{
            $this->procesarSO = false;
        }
        
        return $error;
    }
    
    function validCabSO($datos){
        for($k = 0; $k < count($datos); $k++){
            $this->pDatoControl = $this->asignarColumnasi($this->pDatoControl, $datos[$k], $k, "Dato de Control");
            $this->pHostName = $this->asignarColumnasi($this->pHostName, $datos[$k], $k, "HostName");
            $this->pSistemaOperativo = $this->asignarColumnasi($this->pSistemaOperativo, $datos[$k], $k, "Sistema Operativo");
            $this->pFechaCreacion = $this->asignarColumnasi($this->pFechaCreacion, $datos[$k], $k, "Fecha de Creacion");
        }
    }
    
    function validarResultadosSO(){
        $error = 0;
        
        if($this->pDatoControl == -1 && $this->pHostName == -1 && $this->pSistemaOperativo == -1 && $this->pFechaCreacion == -1){
            $this->procesarSO = false;
        } else if($this->pDatoControl == -1 || $this->pHostName == -1 || $this->pSistemaOperativo == -1 || $this->pFechaCreacion == -1){
            $error = 17;
        }
        
        return $error;
    }
    //fin validaciones Consolidado SO
    
    //inicio validaciones Otros
    function validarConsolidadoOtros($archivoConsolidado, $nombreEquipo, $software, $version, $fechaInstal, $fabricante, 
    $instalAdmin, $instalRed){
        $this->nombreEquipo = $nombreEquipo;
        $this->software = $software; 
        $this->version = $version;
        $this->fechaInstal = $fechaInstal; 
        $this->fabricante = $fabricante; 
        $this->instalAdmin = $instalAdmin; 
        $this->instalRed = $instalRed;
        
        if($this->obtenerSeparadorUniversal($archivoConsolidado, 1, 7) === true){
            if (($fichero = fopen($archivoConsolidado, "r")) !== false) {
                $this->oNombreEquipo = $this->oSoftware = $this->oVersion = $this->oFechaInstal = $this->oFabricante = $this->oInstalAdmin = $this->oInstalRed = -1;
                $this->cicloCabecera($fichero, 1, -1);
                fclose($fichero);

                $this->validarResultadosAddRemove();
            }
        } 
    }
    
    function validCabOtros($datos){
        for($k = 0; $k < count($datos); $k++){ 
            $this->oNombreEquipo = $this->asignarColumnasi($this->oNombreEquipo, $datos[$k], $k, $this->nombreEquipo);
            $this->oSoftware = $this->asignarColumnasi($this->oSoftware, $datos[$k], $k, $this->software);
            $this->oVersion = $this->asignarColumnasi($this->oVersion, $datos[$k], $k, $this->version);
            $this->oFechaInstal = $this->asignarColumnasi($this->oFechaInstal, $datos[$k], $k, $this->fechaInstal);
            $this->oFabricante = $this->asignarColumnasi($this->oFabricante, $datos[$k], $k, $this->fabricante);
            $this->oInstalAdmin = $this->asignarColumnasi($this->oInstalAdmin, $datos[$k], $k, $this->instalAdmin);
            $this->oInstalRed = $this->asignarColumnasi($this->oInstalRed, $datos[$k], $k, $this->instalRed);
        }
    }
    //fin validaciones Otros
}
