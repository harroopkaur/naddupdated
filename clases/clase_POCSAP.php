<?php
class POCSAP extends General{ 
    function listar() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM pocSAP
            ORDER BY fecha DESC, nombre ASC');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function nombreArchivo($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM pocSAP
            WHERE id = :id
            ORDER BY fecha DESC, nombre ASC');
            $sql->execute(array('id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminar($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM pocSAP WHERE id = :id');
            $sql->execute(array('id'=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}