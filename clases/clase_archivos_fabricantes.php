<?php
class clase_archivos_fabricantes extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $fabricante) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO archivosFabricantes (cliente, empleado, fabricante, fechaRegistro) VALUES '
            . '(:cliente, :empleado, :fabricante, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    //Update
    function actualizarDespliegue1($cliente, $empleado, $fabricante, $archivo, $tipoDespliegue) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosFabricantes SET archivoDespliegue1 = :archivo, tipoDespliegue = :tipoDespliegue, fechaRegistro = NOW() '
            . 'WHERE cliente = :cliente AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo, ':tipoDespliegue'=>$tipoDespliegue));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarDespliegue2($cliente, $empleado, $fabricante, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosFabricantes SET archivoDespliegue2 = :archivo WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarDespliegue3($cliente, $empleado, $fabricante, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosFabricantes SET archivoDespliegue3 = :archivo WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarCompras($cliente, $empleado, $fabricante, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosFabricantes SET archivoCompras = :archivo WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existeArchivos($cliente, $empleado, $fabricante){
        try{
            $this->conexion();
            $result = false;
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM archivosFabricantes WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante));
            $query = $sql->fetch();
            if($query["cantidad"] > 0){
                $result = true;
            }
            return $result;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function archivosDespliegue($cliente, $empleado, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM archivosFabricantes WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function eliminarArchivosFabricante($cliente, $empleado, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM archivosFabricantes WHERE cliente = :cliente '
            . 'AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':fabricante'=>$fabricante));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}