<?php
class DetallesAdobe extends General{
    ########################################  Atributos  ########################################

    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    public  $fecha;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $os, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_adobe (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) '
            . 'VALUES (:cliente, :empleado, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo,':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipo_adobe (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function insertarSAM($archivo, $cliente, $equipo, $os, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_adobeSam (cliente, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) VALUES '
            . 'VALUES (:cliente, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)');        
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo,':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/

    // Actualizar
    function actualizar($cliente, $empleado, $equipo, $errors) {       
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalles_equipo_adobe SET errors = :errors WHERE equipo = :equipo AND cliente = :cliente AND empleado = :empleado');        
            $sql->execute(array(':errors'=>$errors, ':equipo'=>$equipo, ':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo actualizar el registro";
            return false;
        }
    }
    
    function actualizarAsignacion($cliente, $empleado, $equipo, $asignacion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo_adobe SET asignacion = :asignacion WHERE cliente = :cliente AND empleado = :empleado "
            .  "AND equipo = :equipo");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':asignacion'=>$asignacion, ':equipo'=>$equipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function limpiarAsignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo_adobe SET asignacion = null WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalles_equipo_adobe WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_asignacion($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT asignacion FROM detalles_equipo_adobe WHERE cliente = :cliente AND empleado = :empleado GROUP BY asignacion ORDER BY asignacion");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_adobe WHERE cliente = :cliente AND empleado = :empleado AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_todo1($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_adobe WHERE cliente = :cliente ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_todo1Asignacion($cliente, $asignacion, $asignaciones) {        
        try {
            $array = array(':cliente'=>$cliente);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_adobe WHERE cliente = :cliente ' . $where . ' ORDER BY id');
            $sql->execute($array);
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }

    /*function listar_todoSAM($cliente) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_adobeSam WHERE cliente = :cliente AND id!=0 AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }*/
    
    function listar_todoSAMArchivo($archivo) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_adobeSam WHERE archivo = :archivo AND id!=0 AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':archivo'=>$archivo));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_todoSAMArchivo1($archivo) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_adobeSam WHERE archivo = :archivo AND id!=0 ORDER BY id');
            $sql->execute(array(':archivo'=>$archivo));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }

    function listar_optimizacion($cliente, $empleado, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo_adobe.id,
                        `detalles_equipo_adobe`.equipo,
                        AdobeAcrobat.edicion AS acrobat,
                        AdobeCreativeCloud.edicion AS cloud,
                        AdobeCreativeSuite.edicion AS suite,
                        CASE
                            WHEN detalles_equipo_adobe.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_adobe AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_adobe.equipo AND detEquipo.os = detalles_equipo_adobe.os) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_adobe
                      LEFT JOIN resumen_adobe AS AdobeAcrobat ON detalles_equipo_adobe.equipo = AdobeAcrobat.equipo AND detalles_equipo_adobe.cliente = AdobeAcrobat.cliente AND detalles_equipo_adobe.empleado = AdobeAcrobat.empleado AND AdobeAcrobat.familia = 'Adobe Acrobat'
		      LEFT JOIN resumen_adobe AS AdobeCreativeCloud ON detalles_equipo_adobe.equipo = AdobeCreativeCloud.equipo AND detalles_equipo_adobe.cliente = AdobeCreativeCloud.cliente AND detalles_equipo_adobe.empleado = AdobeCreativeCloud.empleado AND AdobeCreativeCloud.familia = 'Adobe Creative Cloud'
                      LEFT JOIN resumen_adobe AS AdobeCreativeSuite ON detalles_equipo_adobe.equipo = AdobeCreativeSuite.equipo AND detalles_equipo_adobe.cliente = AdobeCreativeSuite.cliente AND detalles_equipo_adobe.empleado = AdobeCreativeSuite.empleado AND AdobeCreativeSuite.familia = 'Adobe Creative Suite'
                 WHERE detalles_equipo_adobe.cliente = :cliente1 AND detalles_equipo_adobe.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo_adobe.familia = :os
                 GROUP BY detalles_equipo_adobe.id");        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cliente1'=>$cliente, ':os'=>$os));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function listar_optimizacionAsignacion($cliente, $os, $asignacion, $asignaciones){
        $where = "";
        $array = array(':cliente'=>$cliente, ':os'=>$os);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $where1 = " AND detEquipo.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
            $where1 = " AND detEquipo.asignacion IN (" . $asignacion . ") ";
        }
        
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo_adobe.id,
                        `detalles_equipo_adobe`.equipo,
                        AdobeAcrobat.edicion AS acrobat,
                        AdobeCreativeCloud.edicion AS cloud,
                        AdobeCreativeSuite.edicion AS suite,
                        CASE
                            WHEN detalles_equipo_adobe.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_adobe AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_adobe.equipo AND detEquipo.os = detalles_equipo_adobe.os " . $where1 . ") > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_adobe
                      LEFT JOIN resumen_adobe AS AdobeAcrobat ON detalles_equipo_adobe.equipo = AdobeAcrobat.equipo AND detalles_equipo_adobe.cliente = AdobeAcrobat.cliente AND AdobeAcrobat.familia = 'Adobe Acrobat'
		      LEFT JOIN resumen_adobe AS AdobeCreativeCloud ON detalles_equipo_adobe.equipo = AdobeCreativeCloud.equipo AND detalles_equipo_adobe.cliente = AdobeCreativeCloud.cliente AND AdobeCreativeCloud.familia = 'Adobe Creative Cloud'
                      LEFT JOIN resumen_adobe AS AdobeCreativeSuite ON detalles_equipo_adobe.equipo = AdobeCreativeSuite.equipo AND detalles_equipo_adobe.cliente = AdobeCreativeSuite.cliente AND AdobeCreativeSuite.familia = 'Adobe Creative Suite'
                 WHERE detalles_equipo_adobe.cliente = :cliente AND rango NOT IN (1) AND detalles_equipo_adobe.familia = :os " . $where . "
                 GROUP BY detalles_equipo_adobe.id");        
            $sql->execute($array);
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    /*function listar_optimizacionSam($cliente, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo_adobeSam.id,
                        `detalles_equipo_adobeSam`.equipo,
                        AdobeAcrobat.edicion AS acrobat,
                        AdobeCreativeCloud.edicion AS cloud,
                        AdobeCreativeSuite.edicion AS suite,
                        CASE
                            WHEN detalles_equipo_adobeSam.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo_adobeSam.rango = 2 OR detalles_equipo_adobeSam.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_adobeSam AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_adobeSam.equipo AND detEquipo.os = detalles_equipo_adobeSam.os) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_adobeSam 
                      LEFT JOIN resumen_adobeSam AS AdobeAcrobat ON detalles_equipo_adobeSam.equipo = AdobeAcrobat.equipo AND detalles_equipo_adobeSam.cliente = AdobeAcrobat.cliente AND AdobeAcrobat.familia = 'Adobe Acrobat'
		      LEFT JOIN resumen_adobeSam AS AdobeCreativeCloud ON detalles_equipo_adobeSam.equipo = AdobeCreativeCloud.equipo AND detalles_equipo_adobeSam.cliente = AdobeCreativeCloud.cliente AND AdobeCreativeCloud.familia = 'Adobe Creative Cloud'
                      LEFT JOIN resumen_adobeSam AS AdobeCreativeSuite ON detalles_equipo_adobeSam.equipo = AdobeCreativeSuite.equipo AND detalles_equipo_adobeSam.cliente = AdobeCreativeSuite.cliente AND AdobeCreativeSuite.familia = 'Adobe Creative Suite'
                 WHERE detalles_equipo_adobeSam.cliente = :cliente1 AND rango NOT IN (1) AND detalles_equipo_adobeSam.familia = :os
                 GROUP BY detalles_equipo_adobeSam.id");        
            $sql->execute(array(':cliente'=>$cliente, ':cliente1'=>$cliente, ':os'=>$os));
            if($os == "Windows"){
                $this->listaCliente = $sql->fetchAll();
            }
            else{
                $this->listaServidor = $sql->fetchAll();
            }
            
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/
    
    function listar_optimizacionSamArchivo($archivo, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo_adobeSam.id,
                        `detalles_equipo_adobeSam`.equipo,
                        AdobeAcrobat.edicion AS acrobat,
                        AdobeCreativeCloud.edicion AS cloud,
                        AdobeCreativeSuite.edicion AS suite,
                        CASE
                            WHEN detalles_equipo_adobeSam.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo_adobeSam.rango = 2 OR detalles_equipo_adobeSam.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_adobeSam AS detEquipo WHERE detEquipo.archivo = :archivo AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_adobeSam.equipo AND detEquipo.os = detalles_equipo_adobeSam.os) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_adobeSam 
                      LEFT JOIN resumen_adobeSam AS AdobeAcrobat ON detalles_equipo_adobeSam.equipo = AdobeAcrobat.equipo AND detalles_equipo_adobeSam.cliente = AdobeAcrobat.cliente AND AdobeAcrobat.familia = 'Adobe Acrobat'
		      LEFT JOIN resumen_adobeSam AS AdobeCreativeCloud ON detalles_equipo_adobeSam.equipo = AdobeCreativeCloud.equipo AND detalles_equipo_adobeSam.cliente = AdobeCreativeCloud.cliente AND AdobeCreativeCloud.familia = 'Adobe Creative Cloud'
                      LEFT JOIN resumen_adobeSam AS AdobeCreativeSuite ON detalles_equipo_adobeSam.equipo = AdobeCreativeSuite.equipo AND detalles_equipo_adobeSam.cliente = AdobeCreativeSuite.cliente AND AdobeCreativeSuite.familia = 'Adobe Creative Suite'
                 WHERE detalles_equipo_adobeSam.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo_adobeSam.familia = :os
                 GROUP BY detalles_equipo_adobeSam.id");        
            $sql->execute(array(':archivo'=>$archivo, ':os'=>$os));
            if($os == "Windows"){
                $this->listaCliente = $sql->fetchAll();
            }
            else{
                $this->listaServidor = $sql->fetchAll();
            }
            
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    /*function fechaSAM($cliente) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT fecha FROM encGrafAdobeSam WHERE cliente = :cliente');        
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["fecha"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/
    
    function fechaSAMArchivo($archivo) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT fecha FROM encGrafAdobeSam WHERE archivo = :archivo');        
            $sql->execute(array(':archivo'=>$archivo));
            $row = $sql->fetch();
            return $row["fecha"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function cantidadProductosOSUso($cliente, $empleado, $familia){
        $query = "SELECT COUNT(id) AS cantidad
            FROM detalles_equipo_adobe 
            WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function cantidadProductosInstalados($cliente, $empleado, $familia){
        try{
            $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo_adobe
                INNER JOIN resumen_adobe AS office ON detalles_equipo_adobe.equipo = office.equipo AND detalles_equipo_adobe.cliente = office.cliente AND detalles_equipo_adobe.empleado = office.empleado AND office.familia = :familia
            WHERE detalles_equipo_adobe.cliente = :cliente AND detalles_equipo_adobe.empleado = :empleado AND office.edicion != ''";
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function cantidadProductosOtrosInstalados($cliente, $empleado, $familia){
        $where = "";
        for($i = 0; $i < count($familia); $i++){
            $where .= " AND office.familia != '" . $familia[$i] . "' ";
        }
        $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo_adobe
                LEFT JOIN resumen_adobe AS office ON detalles_equipo_adobe.equipo = office.equipo AND detalles_equipo_adobe.cliente = office.cliente AND detalles_equipo_adobe.empleado = office.empleado " . $where . " 
            WHERE detalles_equipo_adobe.cliente = :cliente AND detalles_equipo_adobe.empleado = :empleado AND office.edicion != ''";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function productosUsoCliente($cliente, $empleado){
        $query = "SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_adobe
                INNER JOIN resumen_adobe AS office ON detalles_equipo_adobe.equipo = office.equipo AND detalles_equipo_adobe.cliente = office.cliente AND detalles_equipo_adobe.empleado = office.empleado AND office.familia = 'Adobe Acrobat'
            WHERE detalles_equipo_adobe.cliente = :cliente AND detalles_equipo_adobe.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_adobe
                INNER JOIN resumen_adobe AS office ON detalles_equipo_adobe.equipo = office.equipo AND detalles_equipo_adobe.cliente = office.cliente AND detalles_equipo_adobe.empleado = office.empleado AND office.familia = 'Adobe Creative Cloud'
            WHERE detalles_equipo_adobe.cliente = :cliente AND detalles_equipo_adobe.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_adobe
                INNER JOIN resumen_adobe AS office ON detalles_equipo_adobe.equipo = office.equipo AND detalles_equipo_adobe.cliente = office.cliente AND detalles_equipo_adobe.empleado = office.empleado AND office.familia = 'Adobe Creative Suite'
            WHERE detalles_equipo_adobe.cliente = :cliente AND detalles_equipo_adobe.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_adobe
                INNER JOIN resumen_adobe AS office ON detalles_equipo_adobe.equipo = office.equipo AND detalles_equipo_adobe.cliente = office.cliente AND detalles_equipo_adobe.empleado = office.empleado AND office.familia NOT IN ('Adobe Acrobat', 'Adobe Creative Cloud', 'Adobe Creative Suite')
            WHERE detalles_equipo_adobe.cliente = :cliente AND detalles_equipo_adobe.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            ";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equipos($cliente){
        try{
            $this->conexion();
            
            $sql = $this->conn->prepare("SELECT equipo
                FROM detalles_equipo_adobe
                WHERE cliente = :cliente
                ORDER BY equipo");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}