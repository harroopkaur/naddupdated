<?php

class ConsolidadoProcesadores extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $datoControl;
    var $hostName;
    var $tipoCpu;
    var $cpu;
    var $cores;
    var $procesadoresLogicos;
    var $tipoEscaneo;
    var $error;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $datoControl, $hostName, $tipoCpu, $cpu, $cores, $procesadoresLogicos, $tipoEscaneo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoProcesadores (cliente, empleado, dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) 
            VALUES(:cliente, :empleado, :datoControl, :hostName, :tipoCpu, :cpu, :cores, :procesadoresLogicos, :tipoEscaneo)");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado, 'datoControl'=>$datoControl , 'hostName'=>$hostName, 'tipoCpu'=>$tipoCpu, 'cpu'=>$cpu, 'cores'=>$cores, 'procesadoresLogicos'=>$procesadoresLogicos, 'tipoEscaneo'=>$tipoEscaneo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoProcesadores (cliente, empleado, dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente,$empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM consolidadoProcesadores WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}
