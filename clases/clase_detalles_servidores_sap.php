<?php
class DetallesSAP extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $compSoft, $descripcion) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleServidorSap (cliente, empleado, equipo, compSoft, descripcion) '
            . 'VALUES (:cliente, :empleado, :equipo, :compSoft, :descripcion)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':compSoft'=>$compSoft, ':descripcion'=>$descripcion));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalleServidorSap WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listarProductos($cliente, $empleado, $software) {
    //function listarProductos($cliente, $empleado, $software, $noIncluir) {
        try{
            $this->conexion();
            /*$notIn = "";
            if($noIncluir != ""){
                $data = explode(",", $noIncluir);
                for($i=0; $i < count($data); $i++){
                    $notIn .= " AND descripcion NOT LIKE '%" . $data[$i] . "%'";
                }
            }
            $sql = $this->conn->prepare('SELECT *
            FROM detalleServidorSap
            WHERE cliente = :cliente AND empleado = :empleado AND descripcion LIKE :descripcion ' . $notIn);*/
            $sql = $this->conn->prepare('SELECT *
            FROM detalleServidorSap
            WHERE cliente = :cliente AND empleado = :empleado AND compSoft = :componente');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':componente'=>$software));
            /*$this->lista = $sql->fetchAll();
            return true;*/
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            //return false;
            return array();
        }
    }
}