<?php
class pvu extends General{
    public $error;
    
    // Insertar 
    function insertar($idProcesador, $idModeloServidor, $pvu) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO tablaPVU (idProcesador, idModeloServidor, pvu) 
            VALUES(:idProcesador, :idModeloServidor, :pvu)");
            $sql->execute(array(':idProcesador'=>$idProcesador, ':idModeloServidor'=>$idModeloServidor, ':pvu'=>$pvu));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $idProcesador, $idModeloServidor, $pvu) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE tablaPVU SET idProcesador = :idProcesador, idModeloServidor = :idModeloServidor, pvu = :pvu 
            WHERE id = :id");
            $sql->execute(array(':id'=>$id,':idProcesador'=>$idProcesador, ':idModeloServidor'=>$idModeloServidor, ':pvu'=>$pvu));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM tablaPVU WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_paginado($procesador, $modelo, $pvu, $pagina){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tablaPVU.id, tablaPVU.idProcesador,
                    procesador.descripcion AS procesador,
                    tablaPVU.idModeloServidor,
                    modelo.descripcion AS modelo,
                    tablaPVU.pvu
                FROM tablaPVU
                    INNER JOIN detalleMaestra procesador ON tablaPVU.idProcesador = procesador.idDetalle AND 
                    tablaPVU.idMaestraProcesador = procesador.idMaestra AND procesador.idMaestra = 8
                    INNER JOIN detalleMaestra modelo ON tablaPVU.idModeloServidor = modelo.idDetalle AND 
                    tablaPVU.idMaestraModelo = modelo.idMaestra AND modelo.idMaestra = 9
                WHERE procesador.descripcion LIKE :procesador AND modelo.descripcion LIKE :modelo AND tablaPVU.pvu LIKE :pvu
                LIMIT " . $pagina . ", " . $this->limit_paginacion);
            $sql->execute(array(':procesador'=>"%" . $procesador . "%", ':modelo'=>"%" . $modelo . "%", ':pvu'=>"%" . $pvu . "%"));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($procesador, $modelo, $pvu){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM tablaPVU
                    INNER JOIN detalleMaestra procesador ON tablaPVU.idProcesador = procesador.idDetalle AND 
                    tablaPVU.idMaestraProcesador = procesador.idMaestra AND procesador.idMaestra = 8
                    INNER JOIN detalleMaestra modelo ON tablaPVU.idModeloServidor = modelo.idDetalle AND 
                    tablaPVU.idMaestraModelo = modelo.idMaestra AND modelo.idMaestra = 9
                WHERE procesador.descripcion LIKE :procesador AND modelo.descripcion LIKE :modelo AND tablaPVU.pvu LIKE :pvu");
            $sql->execute(array(':procesador'=>"%" . $procesador . "%", ':modelo'=>"%" . $modelo . "%", ':pvu'=>"%" . $pvu . "%"));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existe_pvu($idProcesador, $idModelo, $id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM tablaPVU
                WHERE id != :id AND idProcesador = :idProcesador AND idModeloServidor = :idModelo");
            $sql->execute(array(':id'=>$id, ':idProcesador'=>$idProcesador, ':idModelo'=>$idModelo));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function pvuEspecifico($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tablaPVU.id, tablaPVU.idProcesador,
                    procesador.descripcion AS procesador,
                    tablaPVU.idModeloServidor,
                    modelo.descripcion AS modelo,
                    tablaPVU.pvu
                FROM tablaPVU
                    INNER JOIN detalleMaestra procesador ON tablaPVU.idProcesador = procesador.idDetalle AND 
                    tablaPVU.idMaestraProcesador = procesador.idMaestra AND procesador.idMaestra = 8
                    INNER JOIN detalleMaestra modelo ON tablaPVU.idModeloServidor = modelo.idDetalle AND 
                    tablaPVU.idMaestraModelo = modelo.idMaestra AND modelo.idMaestra = 9
                WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('idProcesador'=>'', 'procesador'=>'', 'idModeloServidor'=>'', 'modelo'=>'', 'pvu'=>'');
        }
    }
}