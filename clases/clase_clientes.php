<?php
class Clientes extends General{
    ########################################  Atributos  ########################################
    private $listado = array();
    public  $error = NULL;
    public  $id;
    public  $nombre;
    public  $apellido;
    public  $correo;
    public  $telefono;
    public  $empresa;
    public  $pais;
    public  $login;
    public  $clave;
    public  $estado;
    public  $fecha_registro;
    public  $fecha1;
    public  $fecha2;
    public  $nivelServicio;
    public  $html_header;
    public  $html_footer;
    public  $img_header;
    public  $img_footer;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($correo, $telefono, $empresa, $pais, $nivelServicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO clientes(correo, telefono, empresa, pais, estado, fecha_registro, nivelServicio) '
            . 'VALUES (:correo, :telefono, :empresa, :pais, 1, CURDATE(), :nivelServicio)');
            $sql->execute(array(':correo'=>$correo, ':telefono'=>$telefono, 
            ':empresa'=>$empresa, ':pais'=>$pais, ':nivelServicio'=>$nivelServicio));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar    
    function actualizar($id, $correo, $telefono, $empresa, $pais, $nivelServicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'correo = :correo, telefono = :telefono, empresa = :empresa, pais = :pais, '
            . 'nivelServicio = :nivelServicio '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':correo'=>$correo, ':telefono'=>$telefono, 
            ':empresa'=>$empresa, ':pais'=>$pais, ':nivelServicio'=>$nivelServicio));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarCantCorreoCantDespliegue($id, $cantCorreo, $cantDespliegue) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'cantidadCorreosDominio = :cantCorreo, cantidadDespliegueEquipos = :cantDespliegue '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':cantCorreo'=>$cantCorreo, ':cantDespliegue'=>$cantDespliegue));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function suscripcionNADD($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT cantidadCorreosDominio, cantidadDespliegueEquipos '
            . 'FROM clientes '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidadCorreosDominio'=>0, 'cantidadDespliegueEquipos'=>0);
        }
    }
    
    function seleccionarHeaderFooterNADD($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT img_header, img_footer '
                . ' FROM clientes'
                . ' WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('img_header'=>'', 'img_footer'=>'');
        }
    }

    function actualizarNadd($id, $imgheader, $imgfooter, $htmlheader, $htmlfooter) {
        try{
            $this->conexion();

            if($imgheader){
                $sql = $this->conn->prepare('UPDATE clientes SET '
                    . ' img_header = :img_header'
                    . ' WHERE id = :id');
                $sql->execute(array(':id'=>$id, ':img_header'=>$imgheader));
            }

            if($imgfooter){
                $sql = $this->conn->prepare('UPDATE clientes SET '
                    . ' img_footer = :img_footer'
                    . ' WHERE id = :id');
                $sql->execute(array(':id'=>$id, ':img_footer'=>$imgfooter));
            }

            $sql = $this->conn->prepare('UPDATE clientes SET '
                . " html_header = '$htmlheader', html_footer = '$htmlfooter' "
                . ' WHERE id = '.$id);
            $sql->execute();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar
    function actualizar2($id, $estado, $fecha1, $fecha2) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'estado = :estado, fecha1 = :fecha1, fecha2 = :fecha2 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':estado'=>$estado, ':fecha1'=>$fecha1, ':fecha2'=>$fecha2));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar clave    
    function actualizar_clave($id, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'clave = :clave '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':clave'=>$clave));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarDashboard($id, $dashboard) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'dashboard = :dashboard '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':dashboard'=>$dashboard));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Autorizar    
    function autorizar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'estado = 1 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar    
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarDashboard($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET dashboard = NULL WHERE id = :id');
            $sql->execute(array(':id'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Desactivar     
    function desactivar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'estado = 0 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Autenticar Usuario
    function autenticar($correo, $clave) {
        if (($correo != "") && ($clave != "")) {
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT clientes.id, nombre, apellido, correo, empresa, clientes.estado, nivelServicio.descripcion '
                . 'FROM clientes '
                    . 'INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1 '
                . 'WHERE userName = :correo AND clave = :clave AND clientes.estado != 0');
                $sql->execute(array(':correo'=>$correo, ':clave'=>$clave));
                $rusuario = $sql->fetch();
                
                if(count($rusuario['nombre']) > 0){
                    $_SESSION['client_autorizado'] = true;
                    $_SESSION['client_id']         = $rusuario['id'];
                    $_SESSION['client_nombre']     = $rusuario['nombre'];
                    $_SESSION['client_apellido']   = $rusuario['apellido'];
                    $_SESSION['client_email']      = $rusuario['correo'];
                    $_SESSION['client_estado']     = $rusuario['estado'];
                    $_SESSION['client_empresa']    = $rusuario['empresa'];
                    $_SESSION['client_tiempo']     = time();
                    $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                    $_SESSION['nivelServicio']     = $rusuario['descripcion'];

                    return true;
                }
                else{
                    $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Usuario o Contrase&ntilde;a vac&iacute;os';
            return false;
        }
    }
    
    function autenticarEmpleado($correo, $clave) {
        if (($correo != "") && ($clave != "")) {
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT empleados.id, empleados.cliente, empleados.nombre, empleados.apellido, empleados.correo, clientes.empresa, empleados.estado, nivelServicio.descripcion
                    FROM empleados
                        INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                        INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
                    WHERE empleados.userName = :correo AND empleados.clave = :clave AND empleados.estado = 1');
                $sql->execute(array(':correo'=>$correo, ':clave'=>$clave));
                $rusuario = $sql->fetch();
                
                if(count($rusuario['nombre']) > 0){
                    $_SESSION['client_autorizado'] = true;
                    $_SESSION['client_id']         = $rusuario['cliente'];
                    $_SESSION['client_empleado']   = $rusuario['id'];
                    $_SESSION['client_nombre']     = $rusuario['nombre'];
                    $_SESSION['client_apellido']   = $rusuario['apellido'];
                    $_SESSION['client_email']      = $rusuario['correo'];
                    $_SESSION['client_estado']     = $rusuario['estado'];
                    $_SESSION['client_empresa']    = $rusuario['empresa'];
                    $_SESSION['client_tiempo']     = time();
                    $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                    $_SESSION['nivelServicio']     = $rusuario['descripcion'];

                    return true;
                }
                else{
                    $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Usuario o Contrase&ntilde;a vac&iacute;os';
            return false;
        }
    }
    
    function autenticarCentralizer($usuario, $clave) {
        if (($usuario != "") && ($clave != "")) {
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT empleados.id, empleados.cliente, empleados.nombre, empleados.apellido, empleados.correo, clientes.empresa, empleados.estado, nivelServicio.descripcion
                    FROM empleados
                        INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                        INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
                    WHERE empleados.userCent = :usuario AND empleados.passCent = SHA2(:clave, 512) AND empleados.estado = 1');
                $sql->execute(array(':usuario'=>$usuario, ':clave'=>$clave));
                $rusuario = $sql->fetch();
                
                if(count($rusuario['nombre']) > 0){
                    $_SESSION['client_autorizado'] = true;
                    $_SESSION['client_id']         = $rusuario['cliente'];
                    $_SESSION['client_empleado']   = $rusuario['id'];
                    $_SESSION['client_nombre']     = $rusuario['nombre'];
                    $_SESSION['client_apellido']   = $rusuario['apellido'];
                    $_SESSION['client_email']      = $rusuario['correo'];
                    $_SESSION['client_estado']     = $rusuario['estado'];
                    $_SESSION['client_empresa']    = $rusuario['empresa'];
                    $_SESSION['client_tiempo']     = time();
                    $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                    $_SESSION['nivelServicio']     = $rusuario['descripcion'];

                    return true;
                }
                else{
                    $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Usuario o Contrase&ntilde;a vac&iacute;os';
            return false;
        }
    }

    // Imprimir datos de Session
    function session() {
        $cadena = "";
        $cadena .= $_SESSION['client_nombre'] . " " . $_SESSION['client_apellido'] . " - Bienvenido";

        return $cadena;
    }

    // Obtener listado de todos los Usuarios   
    function datos($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            if(count($usuario['empresa']) > 0){
                $this->id             = $usuario['id'];
                $this->nombre         = $usuario['nombre'];
                $this->apellido       = $usuario['apellido'];
                $this->correo         = $usuario['correo'];
                $this->telefono       = $usuario['telefono'];
                $this->pais           = $usuario['pais'];
                $this->empresa        = $usuario['empresa'];
                $this->login          = $usuario['userName'];
                $this->clave          = $usuario['clave'];
                $this->estado         = $usuario['estado'];
                $this->fecha_registro = $usuario['fecha_registro'];
                $this->fecha1         = $usuario['fecha1'];
                $this->fecha2         = $usuario['fecha2'];
                $this->nivelServicio  = $usuario['nivelServicio'];
                $this->img_header     = $usuario['img_header'];
                $this->img_footer     = $usuario['img_footer'];
                $this->html_footer    = $usuario['html_footer'];
                $this->html_header    = $usuario['html_header'];
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    function ultimo_id(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id FROM clientes');
            $sql->execute();
            $row = $sql->fetch();
            return $row['id'];
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE estado = 1 ORDER BY empresa');
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados    
    function listar_todo_paginado($empresa, $email, $fecha, $estado, $inicio) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND fecha_registro = :fecha ";
            }
            
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM clientes '
                . 'WHERE empresa LIKE :empresa AND correo LIKE :email ' . $where . ''
                . 'ORDER BY  empresa '
                . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute($array);
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de Usuarios
    function total($empresa, $email, $fecha, $estado) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND fecha_registro = :fecha ";
            }
           
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
                . 'FROM clientes '
                . 'WHERE empresa LIKE :empresa AND correo LIKE :email ' . $where);
            $sql->execute($array);
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios desactivados
    function listar_desactivados() {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE estado = 0 ORDER BY empresa');
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Obtener listado de todos los Usuarios desactivados paginado
    function listar_desactivados_paginado($inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE estado = 0 ORDER BY empresa LIMIT ' . $inicio . ', ' . $fin);
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de Usuarios desactivados
    function total_desactivados() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM clientes WHERE estado = 0');
            $sql->execute();
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Verificar si e-mail ya existe
    function email_existe($email, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE correo = :email AND id != :id');
            $sql->execute(array(':email'=>$email, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['correo']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    function login_existe($login, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE userName = :login AND id != :id');
            $sql->execute(array(':login'=>$login, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['userName']) > 0){
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function empresa_existe($nombre, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE empresa = :nombre AND id != :id');
            $sql->execute(array(':nombre'=>$nombre, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['empresa']) > 0){
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }

    // Recuperar Contraseña de un Usuario
    function recuperar_contrasena($login) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clave, nombre, apellido, correo FROM clientes WHERE userName = :login');
            $sql->execute(array(':login'=>$login));
            $row = $sql->fetch();
            if(count($row['nombre']) > 0){
                $this->nombre   = $usuario['nombre'];
                $this->apellido = $usuario['apellido'];
                $this->correo   = $usuario['correo'];
                $this->clave    = $usuario['clave'];
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function dashboard($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT dashboard
                FROM clientes
                WHERE clientes.id = :cliente AND clientes.estado = 1');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["dashboard"];            
        }catch(PDOException $e){
            return array("dashboard"=>"");
        }
    }
}
?>