<?php
class email {

    function recuperar_clave($nombre, $apellido, $login, $correo, $clave) {
        $arrhtml_mail = file('plantillas/email_clave.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#LOGIN#", $login, $html_mail);
        $html_mail = str_replace("#CLAVE#", $clave, $html_mail);

        $contenido = $html_mail;
        $asunto = "Trial LA Tool - Seguridad Clave";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail($correo, $asunto, $contenido, $headers);
        
    }

    function recuperar_clave2($nombre, $apellido, $email, $clave) {
        $arrhtml_mail = file('../plantillas/email_clave.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#EMAIL#", $email, $html_mail);
        $html_mail = str_replace("#CLAVE#", $clave, $html_mail);

        $contenido = $html_mail;
        $asunto = "Trial LA Tool - Seguridad Clave";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail($email, $asunto, $contenido, $headers);
    }

    function enviar_suscripcionr($correo, $nombre) {
        $arrhtml_mail = file('plantillas/email_suscripcionr.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);

        $contenido = $html_mail;
        $asunto = " Trial LA Tool- Registro";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail($correo, $asunto, $contenido, $headers);
    }

    function enviar_suscripcion($correo, $nombre, $apellido, $login) {
        $arrhtml_mail = file('plantillas/email_suscripcion.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        $html_mail = str_replace("#CORREO#", $correo, $html_mail);
        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#LOGIN#", $login, $html_mail);

        $contenido = $html_mail;
        $asunto = " Trial LA Tool- Registro";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail('info@licensingassurance.com', $asunto, $contenido, $headers);
    }
    
    function enviar_alerta_renovacion($correo, $nombre, $apellido, $contrato) {
        $copy_email   = "<m_acero_n@hotmail.com>, <paola@licensingassurance.com>, <gabriel@licensingassurance.com>";
        $arrhtml_mail = file('../../plantillas/email_renovacion.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#CONTRATO#", $contrato, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode("Licensing Assurance Anualidad/Renovación");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        $headers .= "cc: {$copy_email}";
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_alerta_renovacion1($correo, $nombre, $apellido, $contrato) {
        //$correo = "m_acero_n@hotmail.com";
        $copy_email   = "<paola@licensingassurance.com>, <gabriel@licensingassurance.com>, <Floranaromero@licensingassurance.com>, <glencthgonzalez@licensingassurance.com>, <miguelangelmolina@licensingassurance.com>
 ";
        $arrhtml_mail = file('/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/plantillas/email_renovacion.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#CONTRATO#", $contrato, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode("Licensing Assurance Anualidad/Renovación");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "cc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_cotizacionLiensing($mensaje) {
        $arrhtml_mail = file('../../plantillas/email_cotizacionLicensing.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#COTIZACION#", utf8_decode("Cotización Licensing Assurance"), $html_mail);
        $html_mail = str_replace("#MENSAJE#", utf8_decode($mensaje), $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode("Cotización Licensing Assurance");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        return mail("Dimitri@licensingassurance.com", $asunto, $contenido, $headers);
    }

    function mail_file($to, $from, $subject, $body, $file, $nameFile){
        $copy_email   = "<m_acero_n@hotmail.com>, <paola@licensingassurance.com>";
        $boundary = md5(rand());
        
        $headers = array(
            "MIME-Version: 1.0",
            "Content-Type: multipart/mixed; boundary=\"{$boundary}\"",
            "From: {$from}",
            "cc: {$copy_email}"
        );
            
        $message = array(
            "--{$boundary}",
            "Content-Type: text/plain",
            "Content-Transfer-Encoding: 7-bit",
            "",
            chunk_split($body),
            "--{$boundary}",      
            "Content-Type: {mime_content_type($file)}; name=\"{$nameFile}\"",
            "Content-Disposition: attachment; filename=\"{$nameFile}\"",
            "Content-Transfer-Encoding: base64",
            "",
            chunk_split(base64_encode(file_get_contents($file))),
            "--{$boundary}--",
            
        );
            
        return mail($to, $subject, implode("\r\n", $message), implode("\r\n", $headers));
    }
    
    function enviar_app_correo($correo, $usuario, $copy_email = "") {
        $arrhtml_mail = file('../../plantillas/email_app_escaneo.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $contenido = $html_mail;
        $asunto = utf8_decode($usuario . " invites you to download the NADD application");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info.la@licensingassurance.com>\r\n";
        
        if ($copy_email != ""){
            $headers .= "Bcc: {$copy_email}";
        }
        
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_reporteVisitas($correo, $copy_email, $tabla, $titulo) {
        $arrhtml_mail = file('/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/plantillas/email_visitas.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#TABLA#", $tabla, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode($titulo);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "cc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_reporteVisitasSemanal($correo, $copy_email, $tabla, $titulo) {
        $arrhtml_mail = file('/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/plantillas/email_visitasSemanal.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#TABLA#", $tabla, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode($titulo);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "cc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_asistenciaEvento($correo, $copy_email, $nombre, $email, $telefono, $pais, $empresa, $titulo) {
        $arrhtml_mail = file('../plantillas/email_asistenciaEvento.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#NOMBRE#", $nombre, $html_mail);
        $html_mail = str_replace("#CORREO#", $email, $html_mail);
        $html_mail = str_replace("#TELEFONO#", $telefono, $html_mail);
        $html_mail = str_replace("#PAIS#", $pais, $html_mail);
        $html_mail = str_replace("#EMPRESA#", $empresa, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode($titulo);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "cc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_webinar($correo, $usuario) {
        $copy_email   = $correo;
        //$copy_email .= ", <m_acero_n@hotmail.com>";
        $arrhtml_mail = file('../plantillas/email_webinar.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $contenido = $html_mail;
        $asunto = "Link para Webinar";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        //$headers .= "cc: {$copy_email}";
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    public function send_ical_event_email($email_from, $email_to, $subject, $html_message, $start_date, $end_date, $description) {
 
        //Create unique identifier
        $cal_uid = date('Ymd').'T'.date('His')."-".rand()."@licensingassurance.com";
 
        //Create Mime Boundry
        //Sirve para separar los tipos de contenidos (iCalendar y el cuerpo del mensaje)
        $mime_boundary = "--Metting--".md5(time());
 
        //Create Email Headers
        $headers = "From: Nombre <".$email_from.">\n";
        $headers .= "Reply-To: Nombre <".$email_from.">\n";
 
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
        $headers .= "Content-class: urn:content-classes:calendarmessage\n";
 
        //Create Email Body (HTML)
        $message = "--$mime_boundary\n";
        $message .= "Content-Type: text/html; charset=UTF-8\n";
        $message .= "Content-Transfer-Encoding: 8bit\n\n";
 
        $message .= $html_message;
        $message .= "\n";
        $message .= "--$mime_boundary\n";
 
        //Create ICAL Content (Google rfc 2445 for details and examples of usage)
        $ical = 'BEGIN:VCALENDAR
PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN
VERSION:2.0
METHOD:REQUEST
BEGIN:VEVENT
ORGANIZER:MAILTO:'.$email_from.'
DTSTART:'.gmdate('Ymd\THis\Z', strtotime($start_date)).'
DTEND:'.gmdate('Ymd\THis\Z', strtotime($end_date)).'
TRANSP:OPAQUE
SEQUENCE:0
UID:'.$cal_uid.'
DTSTAMP:'.gmdate('Ymd\THis\Z', time()).'
DESCRIPTION:'.$description.'
SUMMARY:'.$subject.'
PRIORITY:5
CLASS:PUBLIC
END:VEVENT
END:VCALENDAR';
 
        $message .= 'Content-Type: text/calendar;name="meeting.ics";method=REQUEST;charset=utf-8\n';
        $message .= "Content-Transfer-Encoding: 8bit\n\n";
        $message .= $ical;            
 
        //SEND MAIL
        $mail_sent = @mail($email_to, $subject, $message, $headers );
 
        if($mail_sent)     {
            return true;
        } else {
            return false;
        }   
 
    }
    
    function enviar_email_diagnostic_apps($DiagnosticApp, $tipoEmail, $nombre, $email, $telefono, $pais, 
    $compania, $numEquipos, $mensaje, $asunto, $reporte) {
        //$correo = "m_acero_n@hotmail.com";
        $correo = "lucelvimelo@licensingassurance.com";
        
        $html_mail = $this->diagnostic_pantilla($DiagnosticApp, $tipoEmail, $nombre, $email, $telefono, 
        $pais, $compania, $numEquipos, $mensaje, $asunto, $reporte);

        $contenido = $html_mail;
        
        $asunto = $tipoEmail . " - " . $diagnosticApp . " Diagnostic";
        if ($tipoEmail == "Ask for Proposals"){
            $correo = "info@licensingassurance.com";
            $asunto = "Request Quote from LA Diagnostic APP - " . $diagnosticApp;
        }

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: " . $diagnosticApp . " Diagnostic Apps <info@licensingassurance.com>\r\n";
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function diagnostic_pantilla($app, $tipoEmail, $nombre, $email, $telefono, $pais, $compania, $numEquipos, 
    $mensaje, $asunto, $reporte){
        $arrhtml_mail = file('../plantillas/email_diagnostic_apps.php');
        
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#APP#", utf8_decode($app . " Diagnostic"), $html_mail);
        $html_mail = str_replace("#TIPOEMAIL#", $tipoEmail, $html_mail);
        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#EMAIL#", $email, $html_mail);
        $html_mail = str_replace("#TELEFONO#", $telefono, $html_mail);
        $html_mail = str_replace("#PAIS#", utf8_decode($pais), $html_mail);
        $html_mail = str_replace("#COMPANIA#", utf8_decode($compania), $html_mail);
        
        if ($tipoEmail == "Ask for Proposals"){
            $html_mail = str_replace("#CAMPOOPCIONAL#", "# Devices on the company", $html_mail);
            $html_mail = str_replace("#VALOROPCIONAL#", $numEquipos, $html_mail);
        } else if ($tipoEmail == "Contact Us"){
            $html_mail = str_replace("#CAMPOOPCIONAL#", "Subject", $html_mail);
            $html_mail = str_replace("#VALOROPCIONAL#", $asunto, $html_mail);
        } else if ($tipoEmail == "Purchase Details"){
            $html_mail = str_replace("#CAMPOOPCIONAL#", "Report & Detail", $html_mail);
            $html_mail = str_replace("#VALOROPCIONAL#", $reporte, $html_mail);
        }
        
        $html_mail = str_replace("#MENSAJE#", utf8_decode($mensaje), $html_mail);
        
        return $html_mail;
    }
    
    function enviar_preguntaSPLA($correo, $copy_email, $tema, $descripcion, $email) {
        $arrhtml_mail = file('../../plantillas/email_preguntaSPLA.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#DESCRIPCION#", $descripcion, $html_mail);
        $html_mail = str_replace("#EMAIL#", $email, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode($tema);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info.la@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "Bcc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
}