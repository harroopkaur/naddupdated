<?php
class comprasIbm extends General{
    ########################################  Atributos  ########################################
    public  $listado = array();
    public  $row = array();
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $edicion, $version, $compra, $precio, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO compras_ibm (cliente, empleado, familia, edicion, version, compra, precio, asignacion) '
            . 'VALUES (:cliente, :empleado, TRIM(:familia), TRIM(:edicion), TRIM(:version), :compra, :precio, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':compra'=>$compra, ':precio'=>$precio, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM compras_ibm WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    function datos3($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_ibm WHERE familia = :familia AND edicion = :edicion '
            . 'AND version = :version AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->row = $sql->fetch();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function totalCompras($cliente, $familia, $edicion, $asignacion, $asignaciones) {
        $where = " AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);

        if($edicion == ""){
            $where .= " AND (edicion = '' OR edicion IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND edicion = :edicion ";
            $array[':edicion'] = $edicion;
        }

        if($asignacion != ""){
            $where .= " AND asignacion = :asignacion ";
            $array[':asignacion'] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where .= " AND asignacion IN (" . $asignacion . ") ";
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(SUM(IFNULL(compra, 0)), 0) AS compra '
            . 'FROM compras_ibm '
            . 'WHERE cliente = :cliente ' . $where
            . 'ORDER BY id');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["compra"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function comprasAsignacion($cliente, $familia, $edicion, $asignacion){
        $where = " AND compras_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
        $where1 = " AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        
        if($edicion == ""){
            $where .= " AND (compras_ibm.edicion = '' OR compras_ibm.edicion IS NULL) ";
            $where1 .= " AND (resumen_ibm.edicion = '' OR resumen_ibm.edicion IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND compras_ibm.edicion = :edicion ";
            $where1 .= " AND resumen_ibm.edicion = :edicion ";
            $array[':edicion'] = $edicion;
        }

        if($asignacion != ""){
            $where .= " AND compras_ibm.asignacion = :asignacion ";
            $array[':asignacion'] = $asignacion;
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT compras_ibm.familia,
                    compras_ibm.edicion AS office,
                    compras_ibm.version,
                    compras_ibm.asignacion,
                    IFNULL(COUNT(resumen_ibm.familia), 0) AS instalaciones,
                    (compras_ibm.compra - IFNULL(COUNT(resumen_ibm.familia), 0)) AS balance,
                    (compras_ibm.compra - IFNULL(COUNT(resumen_ibm.familia), 0)) * compras_ibm.precio AS balancec,
                    compras_ibm.compra,
                    (compras_ibm.compra - IFNULL(COUNT(resumen_ibm.familia), 0)) AS disponible,
                    compras_ibm.precio /*,
                    compras_ibm.cantidadGAP,
                    compras_ibm.balanceGAP*/
                FROM compras_ibm
                    LEFT JOIN (SELECT  resumen_ibm.cliente,
                            resumen_ibm.empleado,
                            resumen_ibm.familia,
                            resumen_ibm.edicion,
                            resumen_ibm.version,
                            detalles_equipo_ibm.asignacion
                        FROM resumen_ibm
                            INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo
                            AND resumen_ibm.cliente = detalles_equipo_ibm.cliente
                        WHERE resumen_ibm.cliente = :cliente ' . $where1 . '
                        GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version
                        ORDER BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version) AS resumen_ibm ON compras_ibm.cliente = resumen_ibm.cliente AND compras_ibm.familia = resumen_ibm.familia
                    AND compras_ibm.edicion = resumen_ibm.edicion AND compras_ibm.version = resumen_ibm.version
                    AND compras_ibm.asignacion = resumen_ibm.asignacion
                WHERE compras_ibm.cliente = :cliente ' . $where . '
                GROUP BY compras_ibm.familia, compras_ibm.edicion, compras_ibm.version, compras_ibm.asignacion
                ORDER BY compras_ibm.familia, compras_ibm.edicion, compras_ibm.version, compras_ibm.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_ibm WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf4/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}