<?php
class Resumen_f extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $tipo;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;
    var $rango;
    
    #######################################  Operaciones  #######################################

    function convertirFecha($fecha) {
        $valor = explode("-", $fecha);
        $fecha = $valor[2] . "/" . $valor[1] . "/" . $valor[0];
        return $fecha;
    }

    // Insertar 
    function insertar($cliente, $empleado, $equipo, $familia, $edicion, $version, $fecha_instalacion) {
        $this->conexion();
        $query = "INSERT INTO resumen_office2(cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES(:cliente, :empleado, :equipo, :familia, :edicion, :version, :fecha_instalacion)";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>'TRIM(' . $familia . ')',
            ':edicion'=>'TRIM(' . $edicion . ')', ':version'=>'TRIM(' . $version . ')', ':fecha_instalacion'=>$fecha_instalacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        $this->conexion();
        $query = "DELETE FROM resumen_office2 WHERE cliente = :cliente AND empleado = :empleado";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    /*function datos($id) {
        $this->conexion();
        $query = "SELECT id,
                    cliente,
                    equipo,
                    familia,
                    edicion,
                    version,
                    DATE_FORMAT(fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion
                FROM resumen_office2 WHERE id = :id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->equipo = $usuario['equipo'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            $this->fecha_instalacion = $usuario['fecha_instalacion'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function datos3($id, $cliente, $equipo, $tipo, $familia, $edicion, $version, $fecha, $rango) {
        $this->id = $id;
        $this->cliente = $cliente;
        $this->equipo = $equipo;
        $this->tipo = $tipo;
        $this->familia = $familia;
        $this->edicion = $edicion;
        $this->version = $version;
        $this->fecha_instalacion = $fecha;
        $this->rango = $rango;
        return true;
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM resumen_office2 WHERE cliente = :cliente AND empleado = :empleado ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function duplicado($cliente, $empleado, $equipo, $familia) {
        $this->conexion();
        $query = "SELECT COUNT(equipo) AS cantidad
            FROM (SELECT equipo 
                FROM resumen_office2
                WHERE resumen_office2.cliente = :cliente AND empleado = :empleado AND resumen_office2.equipo = :equipo AND resumen_office2.familia = :familia 
                GROUP BY equipo, familia, edicion, version) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    /*function duplicadoSam($cliente, $equipo, $familia, $edicion, $version) {
        $query = "SELECT COUNT(*) AS cantidad
                    FROM resumen_office2Sam AS resumen_office2
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.equipo = :equipo AND resumen_office2.familia = :familia 
                    AND resumen_office2.edicion = :edicion AND resumen_office2.version = :version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function duplicadoSamArchivo($archivo, $equipo, $familia) {
        $this->conexion();
        $query = "SELECT COUNT(tabla.equipo) AS cantidad
                    FROM (SELECT equipo
                        FROM resumen_office2Sam AS resumen_office2
                        WHERE resumen_office2.archivo = :archivo AND resumen_office2.equipo = :equipo AND resumen_office2.familia = :familia) 
                        GROUP BY equipo, familia, edicion, version) AS tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':equipo'=>$equipo, ':familia'=>$familia));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios paginados
    /*function listar_todo_paginado($cliente, $inicio, $fin) {
        $query = "SELECT * FROM resumen_office2 WHERE cliente = :cliente ORDER BY id LIMIT " . $inicio . ", " . $fin;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todas las Inventarios
    /*function listar_datos($cliente) {
        $query = "select edicion, version, count(*) from resumen_office2 WHERE cliente = :cliente group by edicion, version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    function listar_datos6($cliente, $empleado, $familia, $edicion) {
        $this->conexion();
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        detalles_equipo2.rango
		from resumen_office2 
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    INNER JOIN filepcs2 ON resumen_office2.equipo = filepcs2.cn
		WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion LIKE :edicion
		GROUP BY GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                ORDER BY resumen_office2.version DESC, resumen_office2.fecha_instalacion DESC";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos7($cliente, $empleado, $familia, $edicion, $edicion1) {
        $this->conexion();
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        detalles_equipo2.rango
                    from resumen_office2 
                        INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_office2.equipo = filepcs2.cn
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE :edicion1:
                    GROUP BY GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY resumen_office2.version DESC, resumen_office2.fecha_instalacion DESC";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos8($cliente, $empleado, $familia, $edicion, $edicion1, $edicion2) {
        $this->conexion();
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        detalles_equipo2.rango
                    from resumen_office2 
                        INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_office2.equipo = filepcs2.cn
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE :edicion1 AND resumen_office2.edicion NOT LIKE :edicion2
                    GROUP BY GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY resumen_office2.version DESC, resumen_office2.fecha_instalacion DESC";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos6Ordenar($cliente, $empleado, $familia, $edicion, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        if ($ordenar == "fecha_instalacion") {
            $ordenar = "resumen_office2." . $ordenar;
        }

        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
		from resumen_office2 
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
		WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion LIKE :edicion
                /*GROUP BY resumen_office2.id*/
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                ORDER BY " . $ordenar . " " . $direccion;
       
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Asignacion($cliente, $familia, $edicion, $asignacion, $asignaciones, $dup = "Si") {
        $this->conexion();
        $array = array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%');
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        } else{
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        }
        
        if($edicion == "Professional"){
            $edicion = " AND (resumen_office2.edicion LIKE :edicion OR resumen_office2.edicion LIKE '%ProPlus%' OR resumen_office2.edicion LIKE '%Profesional%') ";
        }
        else{
            $edicion = " AND resumen_office2.edicion LIKE :edicion ";
        }

        $groupOrderBy = "";
        $version = "resumen_office2.version, ";
        if($dup == "Si"){
            $groupOrderBy = ", resumen_office2.version";
            $version = "MAX(resumen_office2.version) AS version, ";
        }
        
        $query = "SELECT resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        " . $version . "
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango,
                        asignacion
		FROM resumen_office2 
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo 
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . "
		WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia " . $edicion . "
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy . "
                ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy;
       
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    //inicio grafico Detalle
    function listar_datos6AsignacionGrafico($cliente, $familia, $edicion, $asignacion, $asignaciones, $dup = "Si") {
        $this->conexion();
        $where = "";
        $array = array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%');
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        } else{
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        }

        if ($edicion == "Professional"){
            $edicion = " AND (resumen_office2.edicion LIKE :edicion OR resumen_office2.edicion LIKE '%ProPlus%' OR resumen_office2.edicion LIKE '%Profesional%') ";
        }
        else{
            $edicion = " AND resumen_office2.edicion LIKE :edicion ";
        }
        
        $groupOrderBy = "";
        $version = "resumen_office2.version ";
        if($dup == "Si"){
            $groupOrderBy = ", resumen_office2.version";
            $version = "MAX(resumen_office2.version) AS version ";
        }

        $query = "SELECT tabla.office,
                tabla.version,
                COUNT(tabla.office) AS instalaciones
            FROM (SELECT resumen_office2.edicion AS office,
                    " . $version . "
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . "
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia " . $edicion . "
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy . "
                ORDER BY resumen_office2.id) tabla
            GROUP BY tabla.office, tabla.version
            ORDER BY tabla.office, tabla.version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos7AsignacionGrafico($cliente, $familia, $edicion, $edicion1, $asignacion, $asignaciones, $dup = "Si") {
        $this->conexion();
        $where = "";
        $array = array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%');
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        } else{
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        }
        
        $edicionPro = "";
        if($edicion1 == "Professional"){
            $edicionPro = " AND NOT resumen_office2.edicion LIKE '%Profesional%' ";
        }
        
        $groupOrderBy = "";
        $version = "resumen_office2.version ";
        if($dup == "Si"){
            $groupOrderBy = ", resumen_office2.version";
            $version = "MAX(resumen_office2.version) AS version ";
        }
        
        $query = "SELECT tabla.office,
                tabla.version,
                COUNT(tabla.office) AS instalaciones
            FROM (SELECT resumen_office2.edicion AS office,
                    " . $version . "
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . "
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND NOT resumen_office2.edicion LIKE :edicion AND NOT resumen_office2.edicion LIKE :edicion1
                " . $edicionPro . "
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy . "
                ORDER BY resumen_office2.id) tabla
            GROUP BY tabla.office, tabla.version
            ORDER BY tabla.office, tabla.version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos8AsignacionGrafico($cliente, $familia, $edicion, $edicion1, $edicion2, $asignacion, $asignaciones, $dup = "Si") {
        $this->conexion();
        $where = "";
        $array = array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', 
        ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%');
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ")";
        } else{
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        }
        
        $groupOrderBy = "";
        $version = "resumen_office2.version ";
        if($dup == "Si"){
            $groupOrderBy = ", resumen_office2.version";
            $version = "MAX(resumen_office2.version) AS version ";
        }

        $query = "SELECT tabla.office,
                tabla.version,
                COUNT(tabla.office) AS instalaciones
            FROM (SELECT resumen_office2.edicion AS office,
                    " . $version . "
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . "
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND 
                NOT resumen_office2.edicion LIKE :edicion AND NOT resumen_office2.edicion LIKE :edicion1 AND NOT resumen_office2.edicion LIKE :edicion2
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy . "
                ORDER BY resumen_office2.id) tabla
            GROUP BY tabla.office, tabla.version
            ORDER BY tabla.office, tabla.version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    //fin grafico Detalle

    /*function listar_datos6SamDetalle($cliente, $familia, $edicion) {
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(resumen_office2.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
		from resumen_office2Sam AS resumen_office2
                    INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
		WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion LIKE :edicion
		GROUP BY resumen_office2.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_datos6SamDetalleArchivo($archivo, $familia, $edicion) {
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
		from resumen_office2Sam AS resumen_office2
                    INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
		WHERE resumen_office2.archivo = :archivo AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion LIKE :edicion
		GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos7Ordenar($cliente, $empleado, $familia, $edicion, $edicion1, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_office2 
                        INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_office2.equipo = filepcs2.cn
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE :edicion1
                    /*GROUP BY resumen_office2.id*/
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos7Asignacion($cliente, $familia, $edicion, $edicion1, $asignacion, $asignaciones, $dup = "Si") {
        $this->conexion();
        $array = array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', 
        ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%');
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        } else{
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        }
        
        $edicionPro = "";
        if($edicion1 == "Professional"){
            $edicionPro = " AND NOT resumen_office2.edicion LIKE '%Profesional%' ";
        }
        
        $groupOrderBy = "";
        $version = "resumen_office2.version, ";
        if($dup == "Si"){
            $groupOrderBy = ", resumen_office2.version";
            $version = "MAX(resumen_office2.version) AS version, ";
        }

        $query = "SELECT resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        " . $version . "
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango,
                        asignacion
		FROM resumen_office2 
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo 
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . "
		WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND NOT resumen_office2.edicion LIKE :edicion AND NOT resumen_office2.edicion LIKE :edicion1 " . $edicionPro . "
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy . "
                ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_datos7SamDetalle($cliente, $familia, $edicion, $edicion1) {
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(resumen_office2.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_office2Sam AS resumen_office2
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE edicion1
                    GROUP BY resumen_office2.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_datos7SamDetalleArchivo($archivo, $familia, $edicion, $edicion1) {
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_office2Sam AS resumen_office2
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    WHERE resumen_office2.archivo = :archivo AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE edicion1
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos8Ordenar($cliente, $empleado, $familia, $edicion, $edicion1, $edicion2, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                         CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_office2 
                        INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_office2.equipo = filepcs2.cn
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE :edicion1 AND resumen_office2.edicion NOT LIKE :edicion2
                    /*GROUP BY resumen_office2.id*/
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_datos8SamDetalle($cliente, $familia, $edicion, $edicion1, $edicion2) {
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(resumen_office2.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                         CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_office2Sam AS resumen_office2
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE :edicion1 AND resumen_office2.edicion NOT LIKE :edicion2
                    GROUP BY resumen_office2.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }    
    }*/
    
    function listar_datos8SamDetalleArchivo($archivo, $familia, $edicion, $edicion1, $edicion2) {
        $query = "select resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                         CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_office2Sam AS resumen_office2
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    WHERE resumen_office2.archivo = :archivo AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE :edicion1 AND resumen_office2.edicion NOT LIKE :edicion2
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }    
    }

    function listar_datos8Asignacion($cliente, $familia, $edicion, $edicion1, $edicion2, $asignacion, $asignaciones, $dup = "Si") {
        $this->conexion();
        $array = array(':cliente'=>$cliente, ':familia'=>'%' . $familia . '%', 
            ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%');
        
        if($asignacion == ""){
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        } else{
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        }
        
        $groupOrderBy = "";
        $version = "resumen_office2.version, ";
        if($dup == "Si"){
            $groupOrderBy = ", resumen_office2.version";
            $version = "MAX(resumen_office2.version) AS version, ";
        }
        
        $query = "SELECT resumen_office2.id,
                        resumen_office2.cliente,
                        resumen_office2.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        " . $version . "
                        DATE_FORMAT(MAX(resumen_office2.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango,
                        asignacion
		FROM resumen_office2 
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo 
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . "
		WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND resumen_office2.edicion NOT LIKE :edicion AND resumen_office2.edicion NOT LIKE :edicion1 AND resumen_office2.edicion NOT LIKE :edicion2
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy . "
                ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion" . $groupOrderBy;
       
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}