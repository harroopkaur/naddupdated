<?php
class clase_procesar_actualizar_escaneo extends General{
    private $tabDetalle;
    private $campoDetalle;
    private $arrayDetalle;
    private $tabEscaneo;
    private $campoEscaneo;
    private $arrayEscaneo;
    private $opcion;
    private $idDiagnostic;
    private $client_id;
    
    function actualizarDetalle() {
        try{
            $this->conexion();
           
            $sql = $this->conn->prepare("UPDATE " . $this->tabDetalle . " SET errors = :errors "
            . "WHERE equipo = :equipo AND " . $this->campoDetalle);
            $sql->execute($this->arrayDetalle);
         
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarEquipo(){
        $this->conexion();
        $query = "UPDATE " . $this->tabDetalle . " SET dn = :dn, objectclass = :objectclass, useracountcontrol = :useracountcontrol, 
        lastlogon = :lastlogon, pwdlastset = :pwdlastset, os = :os, lastlogontimes = :lastlogontimes WHERE cliente = :cliente 
        AND cn = :cn";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$this->client_id, ':dn'=>$this->dn, ':objectclass'=>$this->objectclass, ':cn'=>$this->cn, 
            ':useracountcontrol'=>$this->useracountcontrol, ':lastlogon'=>$this->lastlogon, ':pwdlastset'=>$this->pwdlastset, 
            ':os'=>$this->os,':lastlogontimes'=>$this->lastlogontimes));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function listar_todo2() {
        $this->conexion();
        $query = "SELECT id,
                equipo,
                status,
                errors
            FROM " . $this->tabEscaneo . "
            WHERE " . $this->campoEscaneo . " AND errors = 'Ninguno'
            GROUP BY equipo

            UNION

            SELECT id,
                equipo,
                status,
                errors
            FROM " . $this->tabEscaneo . "
            WHERE " . $this->campoEscaneo . " AND errors != '' AND NOT errors IS NULL
            AND equipo NOT IN (SELECT equipo FROM " . $this->tabEscaneo . " WHERE " . $this->campoEscaneo . " AND errors = 'Ninguno' GROUP BY equipo)
            GROUP BY equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($this->arrayEscaneo);
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function procesarActualizacionEscaneo($client_id, $opcion, $idDiagnostic = 0){
        $this->client_id = $client_id;
        $this->opcion = $opcion;
        $this->idDiagnostic = $idDiagnostic;
        
        $this->cabeceraTablas();
        
        $lista_equipos_scaneados = $this->listar_todo2();
        foreach ($lista_equipos_scaneados as $reg_e) {
            $this->arrayDetalle[":equipo"] = $reg_e["equipo"];
            $this->arrayDetalle[":errors"] = $reg_e["errors"];
            $this->actualizarDetalle();
        }
    }
    
    function cabeceraTablas(){
        if($this->opcion == "Cloud"){
            $this->tabEscaneo = "escaneo_equiposMSCloud";
            $this->campoEscaneo = "idDiagnostic = :idDiagnostic";
            $this->arrayEscaneo = array(':idDiagnostic'=>$this->idDiagnostic);
            $this->tabDetalle = "detalles_equipoMSCloud";
            $this->campoDetalle = "idDiagnostic = :idDiagnostic";
            $this->arrayDetalle[":idDiagnostic"] = $this->idDiagnostic;
        }else if($this->opcion == "Diagnostic"){
            $this->tabEscaneo = "escaneo_equiposSAMDiagnostic";
            $this->campoEscaneo = "idDiagnostic = :idDiagnostic";
            $this->arrayEscaneo = array(':idDiagnostic'=>$this->idDiagnostic);
            $this->tabDetalle = "detalles_equipoSAMDiagnostic";
            $this->campoDetalle = "idDiagnostic = :idDiagnostic";
            $this->arrayDetalle[":idDiagnostic"] = $this->idDiagnostic;
        } else if($this->opcion == "microsoft"){
            $this->tabEscaneo = "escaneo_equipos2";
            $this->campoEscaneo = "cliente = :cliente";
            $this->arrayEscaneo = array(':cliente'=>$this->client_id);
            $this->tabDetalle = "detalles_equipo2";
            $this->campoDetalle = "cliente = :cliente";
            $this->arrayDetalle[":cliente"] = $this->client_id;
        }
    }
}
