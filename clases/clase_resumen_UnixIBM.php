<?php
class resumenUnixIBM extends General{
    public  $lista = array();
    public  $error = NULL;
    
    /*function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumen_UnixSolaris WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_UnixSolaris.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixSolaris
                     INNER JOIN `detalles_equipo_UnixSolaris` ON resumen_UnixSolaris.equipo = `detalles_equipo_UnixSolaris`.equipo
                WHERE resumen_UnixSolaris.cliente = :cliente AND resumen_UnixSolaris.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_UnixSolaris.id
                
                UNION 
                
                SELECT resumen_UnixAIX.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixAIX
                     INNER JOIN `detalles_equipo_UnixAIX` ON resumen_UnixAIX.equipo = `detalles_equipo_UnixAIX`.equipo
                WHERE resumen_UnixAIX.cliente = :cliente AND resumen_UnixAIX.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_UnixAIX.id
                
                UNION
                
                SELECT resumen_UnixLinux.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixLinux
                     INNER JOIN `detalles_equipo_UnixLinux` ON resumen_UnixLinux.equipo = `detalles_equipo_UnixLinux`.equipo
                WHERE resumen_UnixLinux.cliente = :cliente AND resumen_UnixLinux.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_UnixLinux.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_datos5Sam($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_UnixSolarisSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixSolarisSam
                     INNER JOIN detalles_equipo_UnixSolarisSam ON resumen_UnixSolarisSam.equipo = detalles_equipo_UnixSolarisSam.equipo
                WHERE resumen_UnixSolarisSam.cliente = :cliente AND familia = :familia
                GROUP BY resumen_UnixSolarisSam.id

                UNION

                SELECT resumen_UnixAIXSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixAIXSam
                     INNER JOIN detalles_equipo_UnixAIXSam ON resumen_UnixAIXSam.equipo = detalles_equipo_UnixAIXSam.equipo
                WHERE resumen_UnixAIXSam.cliente = :cliente AND familia = :familia
                GROUP BY resumen_UnixAIXSam.id

                UNION

                SELECT resumen_UnixLinuxSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixLinuxSam
                     INNER JOIN detalles_equipo_UnixLinuxSam ON resumen_UnixLinuxSam.equipo = detalles_equipo_UnixLinuxSam.equipo
                WHERE resumen_UnixLinuxSam.cliente = :cliente AND familia = :familia
                GROUP BY resumen_UnixLinuxSam.id");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5SamArchivo($archivo, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_UnixSolarisSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixSolarisSam
                     INNER JOIN detalles_equipo_UnixSolarisSam ON resumen_UnixSolarisSam.equipo = detalles_equipo_UnixSolarisSam.equipo
                WHERE resumen_UnixSolarisSam.archivo = :archivo AND familia = :familia
                GROUP BY resumen_UnixSolarisSam.id

                UNION

                SELECT resumen_UnixAIXSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixAIXSam
                     INNER JOIN detalles_equipo_UnixAIXSam ON resumen_UnixAIXSam.equipo = detalles_equipo_UnixAIXSam.equipo
                WHERE resumen_UnixAIXSam.archivo = :archivo AND familia = :familia
                GROUP BY resumen_UnixAIXSam.id

                UNION

                SELECT resumen_UnixLinuxSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixLinuxSam
                     INNER JOIN detalles_equipo_UnixLinuxSam ON resumen_UnixLinuxSam.equipo = detalles_equipo_UnixLinuxSam.equipo
                WHERE resumen_UnixLinuxSam.archivo = :archivo AND familia = :familia
                GROUP BY resumen_UnixLinuxSam.id");
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_UnixSolaris.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixSolaris
                     INNER JOIN `detalles_equipo_UnixSolaris` ON resumen_UnixSolaris.equipo = `detalles_equipo_UnixSolaris`.equipo
                WHERE resumen_UnixSolaris.cliente = :cliente AND resumen_UnixSolaris.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                GROUP BY resumen_UnixSolaris.id
                
                UNION 
                
                SELECT resumen_UnixAIX.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixAIX
                     INNER JOIN `detalles_equipo_UnixAIX` ON resumen_UnixAIX.equipo = `detalles_equipo_UnixAIX`.equipo
                WHERE resumen_UnixAIX.cliente = :cliente AND resumen_UnixAIX.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                GROUP BY resumen_UnixAIX.id
                
                UNION
                
                SELECT resumen_UnixLinux.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_UnixLinux
                     INNER JOIN `detalles_equipo_UnixLinux` ON resumen_UnixLinux.equipo = `detalles_equipo_UnixLinux`.equipo
                WHERE resumen_UnixLinux.cliente = :cliente AND resumen_UnixLinux.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                GROUP BY resumen_UnixLinux.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos5Agrupado($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT familia,
                            edicion,
                            version
                        FROM resumen_UnixSolaris
                        WHERE cliente = :cliente empleado = :empleado AND familia = :familia
                        GROUP BY id) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixSolaris
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version

                UNION

                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixAIX
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version
                
                UNION

                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixLinux
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datosAgrupado($cliente, $empleado, $familia) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixSolaris
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version
                
                UNION
                
                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixAIX
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version
                
                UNION 
                
                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixLinux
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function graficoBalanza($cliente, $familia){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion($cliente, $familia, $edicion){
        try{  
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.edicion = :edicion
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion1($cliente, $familia){
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia = :familia
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
}