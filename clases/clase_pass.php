<?php
class clase_pass extends General {
    private $semilla = "84k=hu*ekDHEe34=rf/m.72*Qsdp43**";
    private $iv = 74859372;
    public $error;
    
    function encriptar($valor) { 
        $encrip = mcrypt_encrypt(MCRYPT_BLOWFISH, $this->semilla, $valor, MCRYPT_MODE_CBC, $this->iv); 
        return base64_encode($encrip); 
    } 
    
    function desencriptar($valor){        
        $decrip = mcrypt_decrypt(MCRYPT_BLOWFISH, $this->semilla, base64_decode($valor), MCRYPT_MODE_CBC, $this->iv);
        return $decrip; 
    }
    
    function insertar($idDetalle, $descripcion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleMaestra (idDetalle, idMaestra, descripcion, campo1) '
            . 'VALUES (:idDetalle, 25, :descripcion, CURDATE())');
            $sql->execute(array(':idDetalle'=>$idDetalle, ':descripcion'=>$descripcion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
       
    function actualizar($idDetalle, $descripcion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET descripcion = :descripcion WHERE idDetalle = :idDetalle '
            . 'AND idMaestra = 25');
            $sql->execute(array(':idDetalle'=>$idDetalle, ':descripcion'=>$descripcion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($idDetalle) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalleMaestra WHERE idDetalle = :idDetalle AND idMaestra = 25');
            $sql->execute(array(':idDetalle'=>$idDetalle));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function ultId() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(MAX(idDetalle), 0) AS idDetalle
                FROM detalleMaestra  
                WHERE detalleMaestra.idMaestra = 25');
            $sql->execute();
            $row = $sql->fetch();
            return $row["idDetalle"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function total() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total
                FROM detalleMaestra  
                WHERE detalleMaestra.idMaestra = 25');
            $sql->execute();
            $row = $sql->fetch();
            return $row["total"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idDetalle, descripcion, DATE_FORMAT(campo1, "%d/%m/%Y") campo1
                FROM detalleMaestra  
                WHERE detalleMaestra.idMaestra = 25');
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }
    
    function listar_todo_paginado($inicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idDetalle, descripcion, DATE_FORMAT(campo1, "%d/%m/%Y") campo1
                FROM detalleMaestra  
                WHERE detalleMaestra.idMaestra = 25
                LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }
    
    function datos($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idDetalle, descripcion, DATE_FORMAT(campo1, "%d/%m/%Y") campo1
                FROM detalleMaestra  
                WHERE detalleMaestra.idMaestra = 25 AND detalleMaestra.idDetalle = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("idDetalle"=>"", "descripcion"=>"", "campo1"=>"00/00/0000");
        }
    }
    
    function pass_existe($pass, $id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM detalleMaestra  
                WHERE detalleMaestra.idMaestra = 25 AND detalleMaestra.idDetalle != :id AND 
                detalleMaestra.descripcion = :descripcion');
            $sql->execute(array(':id'=>$id, ':descripcion'=>$pass));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
}
