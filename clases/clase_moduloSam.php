<?php
class ModuloSam extends General{
    //atributos 
    public  $lista;
    public  $result;
    public  $error;
    
    function insertar($idCliente, $idPermiso){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO permisoSam (idCliente, idPermiso) VALUES '
            . '(:idCliente, :idPermiso)');
            $sql->execute(array(':idCliente'=>$idCliente, ':idPermiso'=>$idPermiso));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarPermisoEmpleado($idEmpleado, $idPermiso){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO permisoSam (idEmpleado, idPermiso) VALUES '
            . '(:idEmpleado, :idPermiso)');
            $sql->execute(array(':idEmpleado'=>$idEmpleado, ':idPermiso'=>$idPermiso));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    //verify module
    function existe_permiso($idCliente, $idPermiso) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM permisoSam WHERE idCliente = :idCliente AND idPermiso = :idPermiso');
            $sql->execute(array(':idCliente'=>$idCliente, ':idPermiso'=>$idPermiso));
            $result = $sql->fetch();
            return $result["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function existe_permisoEmpleado($idEmpleado, $idPermiso) {
        try{
            $resultado = false;
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM permisoSam WHERE idEmpleado = :idEmpleado AND idPermiso = :idPermiso');
            $sql->execute(array(':idEmpleado'=>$idEmpleado, ':idPermiso'=>$idPermiso));
            $result = $sql->fetch();
            if($result["cantidad"] > 0){
                $resultado = true;
            }
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // delete
    function eliminar($idCliente, $idPermiso) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM permisoSam WHERE idCliente = :idCliente AND idPermiso = :idPermiso');
            $sql->execute(array(':idCliente'=>$idCliente, ':idPermiso'=>$idPermiso));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarPermisoEmpleado($idEmpleado, $idPermiso) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM permisoSam WHERE idEmpleado = :idEmpleado AND idPermiso = :idPermiso');
            $sql->execute(array(':idEmpleado'=>$idEmpleado, ':idPermiso'=>$idPermiso));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    //close
}