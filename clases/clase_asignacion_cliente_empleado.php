<?php
class asignacionClienteEmpleado extends General{
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($asignacion, $cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO asigClienteEmple (idAsignacion, cliente, empleado) VALUES (:asignacion, :cliente, :empleado)');
            $sql->execute(array(':asignacion'=>$asignacion, ':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function actualizar($asignacion, $empleado, $estado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE asigClienteEmple SET estado = :estado WHERE idAsignacion = :asignacion AND empleado = :empleado');
            $sql->execute(array(':asignacion'=>$asignacion, ':empleado'=>$empleado, ':estado'=>$estado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function anularAsignaciones($empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE asigClienteEmple SET estado = 0 WHERE empleado = :empleado');
            $sql->execute(array(':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_asignacion($empleado) {
        try{
            $this->conexion();           
            $sql = $this->conn->prepare('SELECT *
                FROM asigClienteEmple
                WHERE empleado = :empleado AND asigClienteEmple.estado = 1');
            $sql->execute(array(':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeAsignacion($asignacion, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM asigClienteEmple
            WHERE idAsignacion = :asignacion AND empleado = :empleado');
            $sql->execute(array(':asignacion'=>$asignacion, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function obtenerCliente($empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT cliente
            FROM empleados
            WHERE id = :empleado');
            $sql->execute(array(':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado["cliente"];
        }catch(PDOException $e){
            return 0;
        }
    }
}