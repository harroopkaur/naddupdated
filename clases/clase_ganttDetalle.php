<?php
class ganttDetalle extends General{
    public  $error = NULL;
    
    // Insertar 
    function insertar($idGantt, $L1, $L2, $L3, $L4, $TS1, $TS2, $TS3, $TS4, $RS1, $RS2, $RS3, $RS4, $RP1, $RP2, 
    $RP3, $RP4, $Opt1, $Opt2, $Opt3, $Opt4, $Mit1, $Mit2, $Mit3, $Mit4, $Reno1, $Reno2, $Reno3, $Reno4, $TU1, 
    $TU2, $TU3, $TU4, $Rev1, $Rev2, $Rev3, $Rev4, $P1, $P2, $P3, $P4) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO ganttDetalle (idGantt, L1, L2, L3, L4, TS1, TS2, TS3, TS4, RS1, '
            . 'RS2, RS3, RS4, RP1, RP2, RP3, RP4, Opt1, Opt2, Opt3, Opt4, Mit1, Mit2, Mit3, Mit4, Reno1, Reno2, '
            . 'Reno3, Reno4, TU1, TU2, TU3, TU4, Rev1, Rev2, Rev3, Rev4, P1, P2, P3, P4) VALUES '
            . '(:idGantt, :L1, :L2, :L3, :L4, :TS1, :TS2, :TS3, :TS4, :RS1, :RS2, :RS3, :RS4, :RP1, :RP2, :RP3, '
            . ':RP4, :Opt1, :Opt2, :Opt3, :Opt4, :Mit1, :Mit2, :Mit3, :Mit4, :Reno1, :Reno2, :Reno3, :Reno4, '
            . ':TU1, :TU2, :TU3, :TU4, :Rev1, :Rev2, :Rev3, :Rev4, :P1, :P2, :P3, :P4)');
            $sql->execute(array(':idGantt'=>$idGantt, ':L1'=>$L1, ':L2'=>$L2, ':L3'=>$L3, ':L4'=>$L4, ':TS1'=>$TS1, 
            ':TS2'=>$TS2, ':TS3'=>$TS3, ':TS4'=>$TS4, ':RS1'=>$RS1, ':RS2'=>$RS2, ':RS3'=>$RS3, ':RS4'=>$RS4, 
            ':RP1'=>$RP1, ':RP2'=>$RP2, ':RP3'=>$RP3, ':RP4'=>$RP4, ':Opt1'=>$Opt1, ':Opt2'=>$Opt2, ':Opt3'=>$Opt3, 
            ':Opt4'=>$Opt4, ':Mit1'=>$Mit1, ':Mit2'=>$Mit2, ':Mit3'=>$Mit3, ':Mit4'=>$Mit4, ':Reno1'=>$Reno1, 
            ':Reno2'=>$Reno2, ':Reno3'=>$Reno3, ':Reno4'=>$Reno4, ':TU1'=>$TU1, ':TU2'=>$TU2, ':TU3'=>$TU3, ':TU4'=>$TU4, 
            ':Rev1'=>$Rev1, ':Rev2'=>$Rev2, ':Rev3'=>$Rev3, ':Rev4'=>$Rev4, ':P1'=>$P1, ':P2'=>$P2, ':P3'=>$P3, ':P4'=>$P4));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($idGantt, $L1, $L2, $L3, $L4, $TS1, $TS2, $TS3, $TS4, $RS1, $RS2, $RS3, $RS4, $RP1, $RP2, 
    $RP3, $RP4, $Opt1, $Opt2, $Opt3, $Opt4, $Mit1, $Mit2, $Mit3, $Mit4, $Reno1, $Reno2, $Reno3, $Reno4, $TU1, 
    $TU2, $TU3, $TU4, $Rev1, $Rev2, $Rev3, $Rev4, $P1, $P2, $P3, $P4) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE ganttDetalle SET L1 = :L1, L2 = :L2, L3 = :L3, L4 = :L4, TS1 = :TS1, '
            . 'TS2 = :TS2, TS3 = :TS3, TS4 = :TS4, RS1 = :RS1, RS2 = :RS2, RS3 = :RS3, RS4 = :RS4, RP1 = :RP1, RP2 = :RP2, '
            . 'RP3 = :RP3, RP4 = :RP4, Opt1 = :Opt1, Opt2 = :Opt2, Opt3 = :Opt3, Opt4 = :Opt4, Mit1 = :Mit1, '
            . 'Mit2 = :Mit2, Mit3 = :Mit3, Mit4 = :Mit4, Reno1 = :Reno1, Reno2 = :Reno2, Reno3 = :Reno3, Reno4 = :Reno4, '
            . 'TU1 = :TU1, TU2 = :TU2, TU3 = :TU3, TU4 = :TU4, Rev1 = :Rev1, Rev2 = :Rev2, Rev3 = :Rev3, Rev4 = :Rev4, '
            . 'P1 = :P1, P2 = :P2, P3 = :P3, P4 = :P4 WHERE idGantt = :idGantt');
            $sql->execute(array(':idGantt'=>$idGantt, ':L1'=>$L1, ':L2'=>$L2, ':L3'=>$L3, ':L4'=>$L4, ':TS1'=>$TS1, 
            ':TS2'=>$TS2, ':TS3'=>$TS3, ':TS4'=>$TS4, ':RS1'=>$RS1, ':RS2'=>$RS2, ':RS3'=>$RS3, ':RS4'=>$RS4, 
            ':RP1'=>$RP1, ':RP2'=>$RP2, ':RP3'=>$RP3, ':RP4'=>$RP4, ':Opt1'=>$Opt1, ':Opt2'=>$Opt2, ':Opt3'=>$Opt3, 
            ':Opt4'=>$Opt4, ':Mit1'=>$Mit1, ':Mit2'=>$Mit2, ':Mit3'=>$Mit3, ':Mit4'=>$Mit4, ':Reno1'=>$Reno1, 
            ':Reno2'=>$Reno2, ':Reno3'=>$Reno3, ':Reno4'=>$Reno4, ':TU1'=>$TU1, ':TU2'=>$TU2, ':TU3'=>$TU3, ':TU4'=>$TU4, 
            ':Rev1'=>$Rev1, ':Rev2'=>$Rev2, ':Rev3'=>$Rev3, ':Rev4'=>$Rev4, ':P1'=>$P1, ':P2'=>$P2, ':P3'=>$P3, ':P4'=>$P4));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function datosGanttDetalle($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM ganttDetalle
                WHERE idGantt = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array(':idGantt'=>'', ':L1'=>'', ':L2'=>'', ':L3'=>'', ':L4'=>'', ':TS1'=>'', ':TS2'=>'', 
            ':TS3'=>'', ':TS4'=>'', ':RS1'=>'', ':RS2'=>'', ':RS3'=>'', ':RS4'=>'', ':RP1'=>'', ':RP2'=>'', 
            ':RP3'=>'', ':RP4'=>'', ':Opt1'=>'', ':Opt2'=>'', ':Opt3'=>'', ':Opt4'=>'', ':Mit1'=>'', ':Mit2'=>'', 
            ':Mit3'=>'', ':Mit4'=>'', ':Reno1'=>'', ':Reno2'=>'', ':Reno3'=>'', ':Reno4'=>'', ':TU1'=>'', 
            ':TU2'=>'', ':TU3'=>'', ':TU4'=>'', ':Rev1'=>'', ':Rev2'=>'', ':Rev3'=>'', ':Rev4'=>'', ':P1'=>'', 
            ':P2'=>'', ':P3'=>'', ':P4'=>'');
        }
    }
    
    function datosGanttDetalleUlt($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM ganttDetalle
                WHERE idGantt = (SELECT MAX(id) AS id FROM gantt WHERE cliente = :cliente AND estado = 1)');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array(':idGantt'=>'', ':L1'=>'', ':L2'=>'', ':L3'=>'', ':L4'=>'', ':TS1'=>'', ':TS2'=>'', 
            ':TS3'=>'', ':TS4'=>'', ':RS1'=>'', ':RS2'=>'', ':RS3'=>'', ':RS4'=>'', ':RP1'=>'', ':RP2'=>'', 
            ':RP3'=>'', ':RP4'=>'', ':Opt1'=>'', ':Opt2'=>'', ':Opt3'=>'', ':Opt4'=>'', ':Mit1'=>'', ':Mit2'=>'', 
            ':Mit3'=>'', ':Mit4'=>'', ':Reno1'=>'', ':Reno2'=>'', ':Reno3'=>'', ':Reno4'=>'', ':TU1'=>'', 
            ':TU2'=>'', ':TU3'=>'', ':TU4'=>'', ':Rev1'=>'', ':Rev2'=>'', ':Rev3'=>'', ':Rev4'=>'', ':P1'=>'', 
            ':P2'=>'', ':P3'=>'', ':P4'=>'');
        }
    }
}