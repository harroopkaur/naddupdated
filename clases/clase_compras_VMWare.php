<?php
class comprasVMWare extends General{
    ########################################  Atributos  ########################################
    public  $listado = array();
    public  $row = array();
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $accountNumber, $accountName, $product, $licenseKey, $Qty, $unitMeasure, 
    $folderNamePath, $licenseKeyNotes, $type, $number, $poNumber, $orderDate, $orderQty, $supportLevel, 
    $licenseCoverageEndDate) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO contratosVMWare (cliente, empleado, accountNumber, accountName, '
            . 'product, licenseKey, Qty, unitMeasure, folderNamePath, licenseKeyNotes, type, number, poNumber, ' 
            . 'orderDate, orderQty, supportLevel, licenseCoverageEndDate) '
            . 'VALUES (:cliente, :empleado, :accountNumber, :accountName, :product, :licenseKey, :Qty, :unitMeasure, '
            . ':folderNamePath, :licenseKeyNotes, :type, :number, :poNumber, :orderDate, :orderQty, :supportLevel, :licenseCoverageEndDate)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':accountNumber'=>$accountNumber, ':accountName'=>$accountName, 
            ':product'=>$product, ':licenseKey'=>$licenseKey, ':Qty'=>$Qty, ':unitMeasure'=>$unitMeasure, ':folderNamePath'=>$folderNamePath, 
            ':licenseKeyNotes'=>$licenseKeyNotes, ':type'=>$type, ':number'=>$number, ':poNumber'=>$poNumber, ':orderDate'=>$orderDate, 
            ':orderQty'=>$orderQty, ':supportLevel'=>$supportLevel, ':licenseCoverageEndDate'=>$licenseCoverageEndDate));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarDetalle($cliente, $empleado, $producto, $edicion, $version, $llave, $cantidad, $medida, $tipo, $numero, $fechaOrden, 
    $soporte, $fechaFinCobertura) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleContrato_VMWare (cliente, empleado, producto, edicion, version, '
            . 'llave, cantidad, medida, tipo, numero, fechaOrden, soporte, fechaFinCobertura) VALUES '
            . '(:cliente, :empleado, :producto, :edicion, :version, :llave, :cantidad, :medida, :tipo, :numero, :fechaOrden, '
            . ':soporte, :fechaFinCobertura)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':producto'=>$producto, ':edicion'=>$edicion, ':version'=>$version, 
            ':llave'=>$llave, ':cantidad'=>$cantidad, ':medida'=>$medida, ':tipo'=>$tipo, ':numero'=>$numero, ':fechaOrden'=>$fechaOrden,
            ':soporte'=>$soporte, ':fechaFinCobertura'=>$fechaFinCobertura));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM contratosVMWare WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarDetalle($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalleContrato_VMWare WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    function listarDetalleCompra($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, '
                . 'cliente, '
                . 'producto, '
                . 'edicion, '
                . 'version, '
                . 'llave, '
                . 'cantidad, '
                . 'medida, '
                . 'tipo, '
                . 'numero, '
                . 'DATE_FORMAT(fechaOrden, "%d/%m/%Y") AS fechaOrden, '
                . 'soporte, '
                . 'DATE_FORMAT(fechaFinCobertura, "%d/%m/%Y") AS fechaFinCobertura '
            . 'FROM detalleContrato_VMWare WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarDetalleCompraSam($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, '
                . 'cliente, '
                . 'producto, '
                . 'edicion, '
                . 'version, '
                . 'llave, '
                . 'cantidad, '
                . 'medida, '
                . 'tipo, '
                . 'numero, '
                . 'DATE_FORMAT(fechaOrden, "%d/%m/%Y") AS fechaOrden, '
                . 'soporte, '
                . 'DATE_FORMAT(fechaFinCobertura, "%d/%m/%Y") AS fechaFinCobertura '
            . 'FROM detalleContrato_VMWareSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function datos3($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS compra FROM detalleContrato_VMWare WHERE producto = :familia '
            . 'AND edicion = :edicion AND version = :version AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->row = $sql->fetch();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_oracle WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf4/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }

    function listarProductos($cliente, $empleado, $software, $noIncluir) {
        try{
            $this->conexion();
            $notIn = "";
            if($noIncluir != ""){
                $data = explode(",", $noIncluir);
                for($i=0; $i < count($data); $i++){
                    $notIn .= " AND product NOT LIKE '%" . $data[$i] . "%'";
                }
            }
            $sql = $this->conn->prepare('SELECT contratosVMWare.`id`,
                    contratosVMWare.`cliente`,
                    contratosVMWare.accountNumber,
                    contratosVMWare.accountName,
                    contratosVMWare.`product`,
                    contratosVMWare.`licenseKey`,
                    contratosVMWare.`Qty`,
                    contratosVMWare.`unitMeasure`,
                    contratosVMWare.`folderNamePath`,
                    contratosVMWare.`licenseKeyNotes`,
                    contratosVMWare.`type`,
                    contratosVMWare.`number`,
                    contratosVMWare.`poNumber`,
                    contratosVMWare.`orderDate`,
                    contratosVMWare.`orderQty`,
                    contratosVMWare.`supportLevel`,
                    contratosVMWare.`licenseCoverageEndDate`
                FROM contratosVMWare
                WHERE cliente = :cliente AND empleado = :empleado AND product LIKE :product AND 
                licenseCoverageEndDate = (SELECT MAX(licenseCoverageEndDate) FROM contratosVMWare c WHERE c.licenseKey = contratosVMWare.`licenseKey`)' . $notIn . '
                GROUP BY licenseKey');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':product'=>"%" . $software . "%"));
            $this->listado = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function transformarFecha($fecha) {
        $result = "";
        if ($fecha != "") {
            $valor1 = explode("/", $fecha);
            $result = $valor1[2] . "/" . $valor1[1] . "/" . $valor1[0];
        }
        return $result;
    }
}