<?php
class detTrasladoLicencia extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($idTrasladoLicencia, $idDetalleContrato, $disponible, $cantTraslado, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO trasladoDetLicencias (idTrasladoLicencia, idDetalleContrato, disponible, cantTraslado, asignacion) '
            . 'VALUES (:idTrasladoLicencia, :idDetalleContrato, :disponible, :cantTraslado, :asignacion)');
            $sql->execute(array(':idTrasladoLicencia'=>$idTrasladoLicencia, ':idDetalleContrato'=>$idDetalleContrato, ':disponible'=>$disponible, ':cantTraslado'=>$cantTraslado, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function actualizar($id, $cantTraslado, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE trasladoDetLicencias SET cantTraslado = :cantTraslado, asignacion = :asignacion, estado = 1 WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':cantTraslado'=>$cantTraslado, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE trasladoDetLicencias SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarIdTraslado($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE trasladoDetLicencias SET estado = 0 WHERE idTrasladoLicencia = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function dataTraslado($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT trasladoDetLicencias.id,
                    detalleContrato.idDetalleContrato,
                    contrato.numero,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    trasladoDetLicencias.disponible,
                    detalleContrato.asignacion,
                    trasladoDetLicencias.cantTraslado,
                    trasladoDetLicencias.asignacion AS asigTraslado
                FROM trasladoDetLicencias
                    INNER JOIN detalleContrato ON trasladoDetLicencias.idDetalleContrato = detalleContrato.idDetalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE trasladoDetLicencias.idTrasladoLicencia = :id AND trasladoDetLicencias.estado = 1');
            $sql->execute(array(':id'=>$id));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}