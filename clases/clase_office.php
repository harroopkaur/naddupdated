<?php
class Office extends General{
    ########################################  Atributos  ########################################

    public $id;
    public $familia;
    public $edicion;
    public $version;
    public $precio;
    public $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($familia, $edicion, $version, $precio) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO lista_officce (familia, edicion, version, precio) '
            . 'VALUES (:familia, :edicion, :version, :precio)');
            $sql->execute(array(':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':precio'=>$precio));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar
    function actualizar($id, $familia, $edicion, $version, $precio) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE lista_officce SET '
            . 'familia = :familia, edicion = :edicion, version = :version, precio = :precio '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':precio'=>$precio));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($id) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM lista_officce WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    function datos($id) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM lista_officce WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            if(count($usuario["id"]) > 0){
                $this->id      = $usuario['id'];
                $this->familia = $usuario['familia'];
                $this->edicion = $usuario['edicion'];
                $this->version = $usuario['version'];
                $this->precio  = $usuario['precio'];
            }
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo() {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM lista_officce WHERE id!=0 ORDER BY id');
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados
    function listar_todo_paginado($inicio) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM lista_officce WHERE id!=0 ORDER BY id LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Contar el total de Usuarios
    function total() {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM lista_officce WHERE id!=0');
            $sql->execute();
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
}