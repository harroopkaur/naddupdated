<?php
class clientes_dominios extends General{
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO clientes_dominios (cliente, dominio) VALUES (:cliente, :nombre)');
            $sql->execute(array(':cliente'=>$cliente, ':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function actualizar($id, $nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes_dominios SET dominio = :nombre WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function activarDesactivar($id, $estado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes_dominios SET estado = :estado WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':estado'=>$estado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_dominios($cliente, $nombre, $inicio) {
        try{
            $this->conexion();          
            $sql = $this->conn->prepare('SELECT *
            FROM clientes_dominios
            WHERE dominio LIKE :nombre AND cliente = :cliente AND estado = 1
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente, ':nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function totalDominios($cliente, $nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM clientes_dominios
            WHERE cliente = :cliente AND dominio LIKE :nombre');
            $sql->execute(array(':cliente'=>$cliente, ':nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeDominio($cliente, $nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM clientes_dominios
            WHERE cliente = :cliente AND dominio = :nombre');
            $sql->execute(array(':cliente'=>$cliente, ':nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function idDominio($cliente, $nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id
            FROM clientes_dominios
            WHERE cliente = :cliente AND dominio = :nombre');
            $sql->execute(array(':cliente'=>$cliente, ':nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado["id"];
        }catch(PDOException $e){
            return 0;
        }
    }
}