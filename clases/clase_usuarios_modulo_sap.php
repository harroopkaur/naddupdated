<?php
class usuariosModuloSAP extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaCliente;
    public  $listaServidor;
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $usuarios, $componente, $nombreComponente, $diasUltAcceso) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usuariosModuloSap (cliente, empleado, usuarios, componente, nombreComponente, diasUltAcceso) '
            . 'VALUES (:cliente, :empleado, :usuarios, :componente, :nombreComponente, :diasUltAcceso)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':usuarios'=>$usuarios, ':componente'=>$componente, ':nombreComponente'=>$nombreComponente, ':diasUltAcceso'=>$diasUltAcceso));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usuariosModuloSap (cliente, empleado, usuarios, componente, nombreComponente, diasUltAcceso) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM usuariosModuloSap WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    //function listarProductos($cliente, $empleado, $software, $noIncluir) {
    function listarProductos($cliente, $empleado) {
        try{
            $this->conexion();
            /*$notIn = "";
            if($noIncluir != ""){
                $data = explode(",", $noIncluir);
                for($i=0; $i < count($data); $i++){
                    $notIn .= " AND nombreComponente NOT LIKE '%" . $data[$i] . "%'";
                }
            }
            $sql = $this->conn->prepare('SELECT *
            FROM usuariosModuloSap
            WHERE cliente = :cliente AND empleado = :empleado AND nombreComponente LIKE :nombreComponente ' . $notIn);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':nombreComponente'=>"%" . $software . "%"));*/
            $sql = $this->conn->prepare('SELECT usuarios, componente, diasUltAcceso
                FROM usuariosModuloSap
                    INNER JOIN detalleMaestra ON usuariosModuloSap.componente = detalleMaestra.campo2 AND detalleMaestra.idMaestra = 6 AND detalleMaestra.campo1 = 5 AND detalleMaestra.idForaneo = 127
                WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}