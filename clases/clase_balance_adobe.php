<?php
class balanceAdobe extends General{
    ########################################  Atributos  ########################################

    public  $lista = array();
    public  $listaNoIncluir = array();
    public  $error = NULL;
    var $id;
    var $cliente;
    var $officce;
    var $instalaciones;
    var $compra;
    var $balance;
    var $tipo;
    var $archivo;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $office, $version, $precio, $instalaciones, $compra, $balance, $balancec, $tipo, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_adobe(cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo, asignacion) VALUES '
            . '(:cliente, :empleado, :familia, :office, :version, :precio, :instalaciones, :compra, :balance, :balancec, :tipo, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':office'=>$office, 
            ':version'=>$version, ':precio'=>$precio, ':instalaciones'=>$instalaciones, ':compra'=>$compra, 
            ':balance'=>$balance, ':balancec'=>$balancec, ':tipo'=>$tipo, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM balance_adobe WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function imprimirTablaExcelBalanza($tabla, $objPHPExcel, $i){
        foreach ($tabla as $reg_equipos) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $reg_equipos["familia"])
                ->setCellValue('B' . $i, $reg_equipos["office"])
                ->setCellValue('C' . $i, $reg_equipos["version"])
                ->setCellValue('D' . $i, $reg_equipos["asignacion"])
                ->setCellValue('E' . $i, $reg_equipos["instalaciones"])
                ->setCellValue('F' . $i, $reg_equipos["compra"])
                ->setCellValue('G' . $i, $reg_equipos["balance"])
                ->setCellValue('H' . $i, $reg_equipos["balancec"]);
            $i++;
        }
    }

    // Obtener listado de todos los Usuarios
    function productosNoIncluir($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia
                FROM balance_adobe
                WHERE cliente = :cliente AND empleado = :empleado AND
                    familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 != "Otros" AND idMaestra = 1)
                GROUP BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaNoIncluir = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productosNoIncluirSam($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia
                FROM balance_adobeSam
                WHERE cliente = :cliente AND empleado = :empleado AND
                    familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 != "Otros" AND idMaestra = 1)
                GROUP BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaNoIncluir = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarBalanzaAgregar($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balanzaGeneral.familia,
                    balanzaGeneral.edicion,
                    balanzaGeneral.version,
                    SUM(balanzaGeneral.instalacion) AS instalacion,
                    SUM(balanzaGeneral.compra) AS compra,
                    balanzaGeneral.asignacion
                FROM (SELECT balanza.familia,
                        balanza.edicion,
                        balanza.version,
                        0 AS instalacion,
                        IFNULL(balanza.compra, 0) AS compra,
                        balanza.asignacion
                    FROM (SELECT productos.familia,
                            productos.edicion,
                            productos.version,
                            compras_adobe.compra,
                            compras_adobe.asignacion
                        FROM (SELECT productos.nombre AS familia,
                                ediciones.nombre AS edicion,
                                versiones.nombre AS version
                            FROM tabla_equivalencia
                                INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                                INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                                LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                                LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                            WHERE tabla_equivalencia.fabricante = 1
                            ORDER BY familia, edicion, version
                        ) AS productos
                        LEFT JOIN compras_adobe ON productos.familia = compras_adobe.familia AND productos.edicion = compras_adobe.edicion
                        AND productos.version = compras_adobe.version AND compras_adobe.cliente = :cliente AND compras_adobe.empleado = :empleado
                        GROUP BY productos.familia, productos.edicion, productos.version, compras_adobe.asignacion
                    ) balanza

                    UNION

                    SELECT resumen.familia,
                        resumen.edicion,
                        resumen.version,
                        COUNT(resumen.familia) AS instalaciones,
                        0 AS compra,
                        resumen.asignacion
                    FROM (SELECT resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version,
                            detalles_equipo_adobe.asignacion
                        FROM resumen_adobe
                            INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND
                            resumen_adobe.empleado = detalles_equipo_adobe.empleado AND resumen_adobe.equipo = detalles_equipo_adobe.equipo
                       WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado
                       GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version
                    ) AS resumen
                    GROUP BY resumen.familia, resumen.edicion, resumen.version, resumen.asignacion
                ) balanzaGeneral
                GROUP BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion
                ORDER BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function obtenerPrecio($cliente, $empleado, $familia, $edicion, $version){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(compras_adobe.precio, IFNULL(productos.precio, 0)) AS precio
                FROM (SELECT productos.nombre AS familia,
                        ediciones.nombre AS edicion,
                        versiones.nombre AS version,
                        tabla_equivalencia.precio
                    FROM tabla_equivalencia
                        INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                        INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                        LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                        LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                    WHERE tabla_equivalencia.fabricante = 3
                    ORDER BY familia, edicion, version
                    ) AS productos
                    LEFT JOIN compras_adobe ON productos.familia = compras_adobe.familia AND productos.edicion = compras_adobe.edicion
                    AND productos.version = compras_adobe.version AND compras_adobe.cliente = :cliente AND compras_adobe.empleado = :empleado
                WHERE productos.familia = :familia AND productos.edicion = :edicion AND productos.version = :version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia,
            ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            $precio = 0;
            if($row["precio"] != ""){
                $precio = $row["precio"];
            }
            return $precio;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function balanzaAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente);
        
        if($familia != "Otros"){
            $where = " AND balance_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
            $array[':familia'] = $familia;
        } else{
            $where = " AND balance_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
        }
        
        if($familia == "Otros" && $edicion != "Todo"){
            $where .= " AND balance_adobe.familia = :familia ";
            $array[':familia'] = $edicion;
        } else if($edicion == ""){
            $where .= " AND (balance_adobe.office = '' OR balance_adobe.office IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND balance_adobe.office = :edicion ";
            $array[':edicion'] = $edicion;
        }
            
        if($asignacion != ""){
            $where .= " AND balance_adobe.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where .= " AND balance_adobe.asignacion IN (" . $asignacion . ") ";
        }
               
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_adobe.id,
                    balance_adobe.familia,
                    balance_adobe.office,
                    balance_adobe.version,
                    balance_adobe.asignacion,
                    balance_adobe.instalaciones,
                    (IFNULL(balance_adobe.compra, 0) - balance_adobe.instalaciones) AS balance,
                    (IFNULL(balance_adobe.compra, 0) - balance_adobe.instalaciones) * balance_adobe.precio AS balancec,
                    balance_adobe.compra,
                    (IFNULL(balance_adobe.compra, 0) - balance_adobe.instalaciones) AS disponible,
                    balance_adobe.precio
                FROM balance_adobe
                WHERE balance_adobe.cliente = :cliente ' . $where . '
                GROUP BY balance_adobe.familia, balance_adobe.office, balance_adobe.version, balance_adobe.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function balanzaAsignacion1($cliente, $familia, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        
        if($asignacion != ""){
            $where = " AND balance_adobe.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND balance_adobe.asignacion IN (" . $asignacion . ") ";
        }
               
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_adobe.id,
                    balance_adobe.familia,
                    balance_adobe.office,
                    balance_adobe.version,
                    balance_adobe.asignacion,
                    balance_adobe.instalaciones,
                    (balance_adobe.compra - balance_adobe.instalaciones) AS balance,
                    (balance_adobe.compra - balance_adobe.instalaciones) * balance_adobe.precio AS balancec,
                    balance_adobe.compra,
                    (balance_adobe.compra - balance_adobe.instalaciones) AS disponible,
                    balance_adobe.precio
                FROM balance_adobe
                WHERE balance_adobe.cliente = :cliente AND balance_adobe.familia = :familia ' . $where . '
                GROUP BY balance_adobe.familia, balance_adobe.office, balance_adobe.version, balance_adobe.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia AND office = :edicion ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_adobe
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familiasSAM($cliente, $empleado, $familia, $edicion) {        
        try{
            $this->conexion();
        //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia AND office = :edicion ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_adobeSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_adobeSam WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_adobeSam WHERE archivo = :archivo AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias2($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_adobe
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias2SAM($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_adobeSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familiasGrafico($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia) AND office = :edicion AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csv2/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
    
    function balanceEjecutivo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia,
                    SUM(compra) AS totalCompras,
                    SUM(instalaciones) AS totalInstalaciones,
                    SUM(compra) - SUM(instalaciones) AS neto,
                    precio,
                    (SUM(compra) - SUM(instalaciones)) * precio AS total
                FROM balance_adobe
                WHERE cliente = :cliente AND precio > 0
                GROUP BY familia
                ORDER BY familia');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function sobranteFaltante($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ABS(ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) < 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0)) AS faltante,
                    ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) > 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0) AS sobrante
                FROM balance_adobe
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("faltante"=>0, "sobrante"=>0);
        }
    }
}