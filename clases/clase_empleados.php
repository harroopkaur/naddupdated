<?php
class empleados extends General{
    public $id;
    public $idEmpleado;
    public $nombre;
    public $apellido;
    public $correoEmpleado;
    public $correo;
    public $telefono;
    public $pais;
    public $empresa;
    public $login;
    public $clave;
    public $estado;
    public $fecha_registro;
    public $fecha1;
    public $fecha2;
    public $nivelServicio;
    public $userNADD;
    public $passNADD;
    public $fechaIniNADD;
    public $fechaFinNADD;
    public $userCent;
    public $passCent;
    public $error = null;
    public $listado;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($nombre, $apellido, $correo, $cliente, $login, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO empleados (nombre, apellido, correo, userName, clave, estado, fecha_registro, cliente) '
            . 'VALUES (:nombre, :apellido, :correo, :login, :clave, 1, CURDATE(), :cliente)');
            $sql->execute(array(':nombre'=>$nombre, ':apellido'=>$apellido, ':correo'=>$correo, ':login'=>$login, ':clave'=>$clave, ':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar    
    function actualizar($id, $nombre, $apellido, $correo, $cliente, $login, $clave, $estado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'nombre = :nombre, apellido = :apellido, cliente = :cliente, correo = :correo, userName = :login, clave = :clave, estado = :estado '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre, ':apellido'=>$apellido, ':cliente'=>$cliente, 
            ':correo'=>$correo, ':login'=>$login, ':clave'=>$clave, ':estado'=>$estado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarNADD($id, $login, $clave, $fechaInicio, $fechaFin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'userNADD = :login, passNADD = SHA2(:clave, 512), fechaIniNADD = :fechaInicio, fechaFinNADD = :fechaFin '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':login'=>$login, ':clave'=>$clave, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarNADD1($id, $login, $fechaInicio, $fechaFin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'userNADD = :login, fechaIniNADD = :fechaInicio, fechaFinNADD = :fechaFin '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':login'=>$login, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function verificarPassNADD($id, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad 
                FROM empleados WHERE id = :id AND passNADD = :clave');
            $sql->execute(array(':id'=>$id, ':clave'=>$clave));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 1;
        }
    }
    
    function autenticarNADD($login, $clave) {
        if (($login != "") && ($clave != "")) {
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT empleados.id, empleados.cliente, empleados.nombre, empleados.apellido, empleados.correo, clientes.empresa, empleados.estado, nivelServicio.descripcion
                    FROM empleados
                        INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                        INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
                    WHERE empleados.userNADD = :login AND empleados.passNADD = SHA2(:clave, 512) AND empleados.estado = 1 AND empleados.fechaFinNADD >= CURDATE()');
                $sql->execute(array(':login'=>$login, ':clave'=>$clave));
                $rusuario = $sql->fetch();
                
                if(count($rusuario['nombre']) > 0){
                    $_SESSION['client_NADD_autorizado'] = true;
                    $_SESSION['client_NADD_id']         = $rusuario['cliente'];
                    $_SESSION['client_NADD_empleado']   = $rusuario['id'];
                    $_SESSION['client_NADD_nombre']     = $rusuario['nombre'];
                    $_SESSION['client_NADD_apellido']   = $rusuario['apellido'];
                    $_SESSION['client_NADD_email']      = $rusuario['correo'];
                    $_SESSION['client_NADD_estado']     = $rusuario['estado'];
                    $_SESSION['client_NADD_empresa']    = $rusuario['empresa'];
                    $_SESSION['client_NADD_tiempo']     = time();
                    $_SESSION['client_NADD_fecha']      = date('d/m/Y H:i:s');
                    return true;
                }
                else{
                    $this->error = 'Invalid user or password';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Invalid user or password'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Invalid user or password';
            return false;
        }
    }
    
    function verificarPassCent($id, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad 
                FROM empleados WHERE id = :id AND passCent = :clave');
            $sql->execute(array(':id'=>$id, ':clave'=>$clave));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 1;
        }
    }
    
    function actualizarCent($id, $login, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'userCent = :login, passCent = SHA2(:clave, 512) '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':login'=>$login, ':clave'=>$clave));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarCent1($id, $login) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'userCent = :login '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':login'=>$login));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actUltAcceso($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET ultAcceso = CURDATE() WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar clave    
    function actualizar_clave($id, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'clave = :clave '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':clave'=>$clave));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Autorizar    
    function autorizar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'estado = 1 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar    
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarCredCent($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET userCent = null, passCent = null WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Desactivar     
    function desactivar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE empleados SET '
            . 'estado = 0 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Autenticar Usuario
    function autenticar($userName, $clave) {
        if (($userName != "") && ($clave != "")) {
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT clientes.id,
                        empleados.id AS idEmpleado,
                        empleados.nombre,
                        empleados.apellido,
                        empleados.correo AS correoEmpleado,
                        clientes.correo,
                        clientes.empresa,
                        empleados.estado,
                        nivelServicio.descripcion
                    FROM empleados
                        INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                        INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
                    WHERE empleados.userName = :userName AND empleados.clave = :clave AND empleados.estado = 1');
                $sql->execute(array(':userName'=>$userName, ':clave'=>$clave));
                $rusuario = $sql->fetch();
                
                if(count($rusuario['nombre']) > 0){
                    $_SESSION['client_autorizado'] = true;
                    $_SESSION['client_id']         = $rusuario['id'];
                    $_SESSION['empleado_id']       = $rusuario['idEmpleado'];
                    $_SESSION['client_nombre']     = $rusuario['nombre'];
                    $_SESSION['client_apellido']   = $rusuario['apellido'];
                    $_SESSION['client_email']      = $rusuario['correo'];
                    $_SESSION['empleado_email']    = $rusuario['correoEmpleado'];
                    $_SESSION['client_estado']     = $rusuario['estado'];
                    $_SESSION['client_empresa']    = $rusuario['empresa'];
                    $_SESSION['client_tiempo']     = time();
                    $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                    $_SESSION['nivelServicio']     = $rusuario['descripcion'];
                    return true;
                }
                else{
                    $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Usuario o Contrase&ntilde;a vac&iacute;os';
            return false;
        }
    }

    // Imprimir datos de Session
    function session() {
        $cadena = "";
        $cadena .= $_SESSION['client_nombre'] . " " . $_SESSION['client_apellido'] . " - Bienvenido";

        return $cadena;
    }

    // Obtener listado de todos los Usuarios   
    function datos($idEmpleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id,
                    empleados.id AS idEmpleado,
                    empleados.nombre,
                    empleados.apellido,
                    empleados.correo AS correoEmpleado,
                    clientes.correo,
                    clientes.telefono,
                    clientes.pais,
                    clientes.empresa,
                    empleados.userName,
                    empleados.clave,
                    empleados.estado,
                    empleados.fecha_registro,
                    clientes.fecha1,
                    clientes.fecha2,
                    nivelServicio.descripcion,
                    empleados.userNADD,
                    empleados.passNADD,
                    empleados.fechaIniNADD,
                    empleados.fechaFinNADD,
                    empleados.userCent,
                    empleados.passCent
                FROM empleados
                    INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                    INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
                WHERE empleados.id = :idEmpleado');
            $sql->execute(array('idEmpleado'=>$idEmpleado));
            $usuario = $sql->fetch();
            if(count($usuario['nombre']) > 0){
                $this->id             = $usuario['id'];
                $this->idEmpleado     = $usuario['idEmpleado'];
                $this->nombre         = $usuario['nombre'];
                $this->apellido       = $usuario['apellido'];
                $this->correoEmpleado = $usuario['correoEmpleado'];
                $this->correo         = $usuario['correo'];
                $this->telefono       = $usuario['telefono'];
                $this->pais           = $usuario['pais'];
                $this->empresa        = $usuario['empresa'];
                $this->login          = $usuario['userName'];
                $this->clave          = $usuario['clave'];
                $this->estado         = $usuario['estado'];
                $this->fecha_registro = $usuario['fecha_registro'];
                $this->fecha1         = $usuario['fecha1'];
                $this->fecha2         = $usuario['fecha2'];
                $this->userNADD       = $usuario['userNADD'];
                $this->passNADD       = $usuario['passNADD'];
                $this->fechaIniNADD   = $usuario['fechaIniNADD'];
                $this->fechaFinNADD   = $usuario['fechaFinNADD'];
                $this->userCent       = $usuario['userCent'];
                $this->passCent       = $usuario['passCent'];
                $this->nivelServicio  = $usuario['descripcion'];
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    function ultimo_id(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id FROM empleados');
            $sql->execute();
            $row = $sql->fetch();
            return $row['id'];
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id,
                    empleados.id AS idEmpleado,
                    empleados.nombre,
                    empleados.apellido,
                    empleados.correo AS correoEmpleado,
                    clientes.correo,
                    clientes.telefono,
                    clientes.pais,
                    clientes.empresa,
                    empleados.userName,
                    empleados.estado,
                    empleados.fecha_registro,
                    clientes.fecha1,
                    clientes.fecha2,
                    nivelServicio.descripcion
                FROM empleados
                    INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                    INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
                ORDER BY clientes.empresa, empleados.nombre, empleados.apellido');
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Obtener listado de todos los Usuarios paginados    
    function listar_todo_paginado($nombre, $empresa, $email, $fecha, $estado, $inicio) {
        try{
            $array = array(":nombre"=>"%" . $nombre . "%", ":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND empleados.fecha_registro = :fecha ";
            }
            
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND empleados.estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id,
                    empleados.id AS idEmpleado,
                    empleados.nombre,
                    empleados.apellido,
                    empleados.correo AS correoEmpleado,
                    clientes.correo,
                    clientes.telefono,
                    clientes.pais,
                    clientes.empresa,
                    empleados.userName,
                    empleados.estado,
                    empleados.fecha_registro,
                    clientes.fecha1,
                    clientes.fecha2,
                    nivelServicio.descripcion
                FROM empleados
                    INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                    INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1 
                WHERE CONCAT(empleados.nombre, " ", empleados.apellido) LIKE :nombre AND clientes.empresa LIKE :empresa AND empleados.correo LIKE :email ' . $where . '
                ORDER BY clientes.empresa, empleados.nombre, empleados.apellido
                LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute($array);
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de Usuarios
    function total($nombre, $empresa, $email, $fecha, $estado) {
        try{
            $array = array(":nombre"=>"%" . $nombre . "%", ":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND empleados.fecha_registro = :fecha ";
            }
            
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND empleados.estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(empleados.id) AS cantidad
                FROM empleados
                    INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                    INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1 
                WHERE CONCAT(empleados.nombre, " ", empleados.apellido) LIKE :nombre AND clientes.empresa LIKE :empresa AND empleados.correo LIKE :email ' . $where);
            $sql->execute($array);
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function listar_todo_paginadoCliente($cliente, $nombre, $empresa, $email, $fecha, $estado, $inicio) {
        try{
            $array = array(":cliente"=>$cliente, ":nombre"=>"%" . $nombre . "%", ":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND empleados.fecha_registro = :fecha ";
            }
            
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND empleados.estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id,
                    empleados.id AS idEmpleado,
                    empleados.nombre,
                    empleados.apellido,
                    empleados.correo AS correoEmpleado,
                    clientes.correo,
                    clientes.telefono,
                    clientes.pais,
                    clientes.empresa,
                    empleados.userName,
                    empleados.estado,
                    empleados.fecha_registro,
                    clientes.fecha1,
                    clientes.fecha2,
                    nivelServicio.descripcion
                FROM empleados
                    INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                    INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1 
                WHERE clientes.id = :cliente AND CONCAT(empleados.nombre, " ", empleados.apellido) LIKE :nombre AND clientes.empresa LIKE :empresa AND empleados.correo LIKE :email ' . $where . '
                ORDER BY clientes.empresa, empleados.nombre, empleados.apellido
                LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute($array);
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }
    
    function totalCliente($cliente, $nombre, $empresa, $email, $fecha, $estado) {
        try{
            $array = array(":cliente"=>$cliente, ":nombre"=>"%" . $nombre . "%", ":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND empleados.fecha_registro = :fecha ";
            }
            
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND empleados.estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(empleados.id) AS cantidad
                FROM empleados
                    INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                    INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1 
                WHERE clientes.id = :cliente AND CONCAT(empleados.nombre, " ", empleados.apellido) LIKE :nombre AND clientes.empresa LIKE :empresa AND empleados.correo LIKE :email ' . $where);
            $sql->execute($array);
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios desactivados
    function listar_desactivados() {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM empleados WHERE estado = 0');
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Obtener listado de todos los Usuarios desactivados paginado
    function listar_desactivados_paginado($inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM empleados WHERE estado = 0 LIMIT ' . $inicio . ', ' . $fin);
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de Usuarios desactivados
    function total_desactivados() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM clientes WHERE estado=0');
            $sql->execute();
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Verificar si e-mail ya existe
    function email_existe($email, $idEmpleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM empleados WHERE correo = :email AND id != :id');
            $sql->execute(array(':email'=>$email, ':id'=>$idEmpleado));
            $row = $sql->fetch();
            if(count($row['correo']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    function login_existe($login, $idEmpleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM empleados WHERE userName = :login AND id != :idEmpleado');
            $sql->execute(array(':login'=>$login, ':idEmpleado'=>$idEmpleado));
            $row = $sql->fetch();
            if(count($row['userName']) > 0){
                return true;
            }
            else{
                return false;
            }            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function loginNADD_existe($login, $idEmpleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM empleados WHERE userNADD = :login AND id != :idEmpleado');
            $sql->execute(array(':login'=>$login, ':idEmpleado'=>$idEmpleado));
            $row = $sql->fetch();
            if(count($row['userName']) > 0){
                return true;
            }
            else{
                return false;
            }            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function loginCent_existe($login, $idEmpleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM empleados WHERE userCent = :login AND id != :idEmpleado');
            $sql->execute(array(':login'=>$login, ':idEmpleado'=>$idEmpleado));
            $row = $sql->fetch();
            if(count($row['userName']) > 0){
                return true;
            }
            else{
                return false;
            }            
        }catch(PDOException $e){
            return false;
        }
    }

    // Recuperar Contraseña de un Usuario
    function recuperar_contrasena($login) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clave, nombre, apellido, correo FROM empleados WHERE userName = :login');
            $sql->execute(array(':login'=>$login));
            $usuario = $sql->fetch();
            if(count($usuario['nombre']) > 0){
                $this->nombre   = $usuario['nombre'];
                $this->apellido = $usuario['apellido'];
                $this->correo   = $usuario['correo'];
                $this->clave    = $usuario['clave'];
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function mostrarUltAcceso($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ultAcceso FROM empleados WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            return $usuario["ultAcceso"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('ultAcceso'=>'0000-00-00');
            
        }
    }
}