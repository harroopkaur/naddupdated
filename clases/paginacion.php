<?php
class Paginacion extends General{
	private $total;
	private $pagina;
	private $condicion; //nombre de la funcion en javascript para la paginacion
	
	public function __construct($total, $pagina, $condicion) {
		$this->total     = ceil($total / $this->limit_paginacion);
		$this->pagina    = $pagina;
		$this->condicion = $condicion;
	}
	
	private function get_paginacion() {
		$pag = "";
		if($this->total > 5){
			$inicio = $this->pagina - 2;
			$control = $this->pagina + 5;
			$limit = $this->total + 5;
			if($inicio == 0){
				$inicio = $this->pagina - 1;
				$control = $this->pagina + 5 - 1;
				$limit = $this->total + 5 - 1;
			}
			else if($inicio == -1){
				$inicio = $this->pagina;
			}
			else{
				$control = $this->pagina + 5 - 2;
				$limit = $this->total + 5 - 2;
			}
			
			if($control >= $limit - 1){
				$inicio = $this->pagina - 3;
				$control = $control - 1;
			}
			if($control >= $limit - 1 && $limit - 1 == $this->total + 2){
				$inicio = $this->pagina - 4;
				$control = $control - 1;
			}
			
			if ($inicio >= 2){
				$nueva_pagina = $this->pagina - 1;
				//$pag .= '<li><a href="#" aria-label="Anterior" onclick="' . $this->condicion . '('. $nueva_pagina .')"><span aria-hidden="true">&laquo;</span></a></li>';
                                $pag .= "<a class='paginator' style='font-size:14;text-decoration:none; margin-right:5px;' href='#' onclick='" . $this->condicion . "(". $nueva_pagina .")'>&lsaquo;</a>";
			}
		}
		else{
			$inicio  = 1;
			$control = $this->total + 1;
			$limit   = $this->total + 2;
		}
		if ($this->total > 1) {
			if($control > $limit){
				$control = $this->total;
			}			
			for ($i = $inicio; $i < $control; $i++) {
				/*$pag .= "<li ';
				if($this->pagina == $i){ $pag .= "class='active'"; }
				$pag .= '><a href="#" onclick="' . $this->condicion . '('. $i .')">'.$i.'</a></li>';*/
                            if($this->pagina == $i){ 
                                $pag .= "<span class='paginator_check' onclick='" . $this->condicion . "(". $i .")'>" . $i . "</span> ";
                            }
                            else{
                                $pag .= "<a href='#' class='paginator' style='margin-right:5px;' onclick='" . $this->condicion . "(". $i .")'>" . $i . "</a>";
                            }
			}
		}
		if ($control < $this->total + 1){
                    $nueva_pagina = $this->pagina + 1;
			/*$pag .= '<li><a href="#" aria-label="Siguiente" onclick="' . $this->condicion . '('. $nueva_pagina .')"> 
				<span aria-hidden="true">&raquo;</span></a></li>';*/
                    $pag .= "<a class='paginator' style='font-size:14;text-decoration:none' href='#' onclick='" . $this->condicion . "(". $nueva_pagina .")'>&rsaquo;</a>";
		}
		return $pag;
	}
	public function get_pag() {
		return $this->get_paginacion();
	}
}


