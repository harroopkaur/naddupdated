<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$tablaMaestra = new tablaMaestra();
$validator = new validator("form1");
$general = new General();

$modificar = 0;
$fab = 0;
if(isset($_GET["fab"]) && filter_var($_GET["fab"], FILTER_VALIDATE_INT) !== false){
    $fab = $_GET["fab"];
}

$idDetalle = 0;
if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $idDetalle = $_GET["id"];
}

$palabra = "";
if(isset($_GET["palabra"])){
    $palabra = $general->get_escape($_GET["palabra"]);
}

if (isset($_POST['modificar']) && $_POST["modificar"] == 1) {
    // Validaciones
    $modificar = 1;

    if(isset($_POST["fab"]) && filter_var($_POST["fab"], FILTER_VALIDATE_INT) !== false){
        $fab = $_POST["fab"];
    }

    if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
        $idDetalle = $_POST["id"];
    }
    
    $palabra = $general->get_escape($_POST["palabra"]);
    
    $palabraOri = $general->get_escape($_POST["palabraOri"]);
    
    if ($tablaMaestra->existePalabraIncluir($idDetalle, $palabra)) {
        $exito = 2;
    } else{
        if ($tablaMaestra->modificarPalabraIncluir($idDetalle, $palabra, $palabraOri)) {
            $exito = 1;
        } 
    }
}

$listado = $tablaMaestra->listadoModificarIncluir($idDetalle);
$count = count($listado);

$validator->create_message("msj_palabra", "palabra", " Obligatorio", 0);