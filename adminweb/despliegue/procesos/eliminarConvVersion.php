<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");

// Objetos
$tablaMaestra = new tablaMaestra();
$general = new General();
$exito = 0;

if(isset($_POST["id"]) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
    $fab = 0;
    if(isset($_POST["fab"]) && filter_var($_POST["fab"], FILTER_VALIDATE_INT) !== false){
        $fab = $_POST["fab"];
    }
    
    if($tablaMaestra->eliminarConvertir($_POST['id'], 4)){
        $exito = 1;   
    }
}