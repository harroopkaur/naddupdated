<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$tablaMaestra = new tablaMaestra();
$validator = new validator("form1");
$general = new General();

$fab = 0;
if(isset($_GET["fab"]) && filter_var($_GET["fab"], FILTER_VALIDATE_INT) !== false){
    $fab = $_GET["fab"];
}

$exito = 0;
$agregar = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    $agregar = 1;
    if(isset($_POST["fab"]) && filter_var($_POST["fab"], FILTER_VALIDATE_INT) !== false){
        $fab = $_POST["fab"];
    }

    $familia = 0;
    if(isset($_POST["familia"]) && filter_var($_POST["familia"], FILTER_VALIDATE_INT) !== false){
        $familia = $_POST["familia"];
    }
    
    $descripcion = "";
    if(isset($_POST["descripcion"])){
        $descripcion = $general->get_escape($_POST["descripcion"]);
    }
    
    $palabra = "";
    if(isset($_POST["palabra"])){
        $palabra = $general->get_escape($_POST["palabra"]);
    }
    
    /*if ($tablaMaestra->existeEdicionConvertir($fab, $familia, $descripcion) > 0) {
        $exito = 2;
    } else{*/
        if ($tablaMaestra->insertarPalabraConvertir(5, $familia, $descripcion, $palabra)) {
            $exito = 1;
        } 
    //}
}

$tablaMaestra->todosProductosSalida($fab);
$listado = $tablaMaestra->lista;

$validator->create_message("msj_familia", "familia", " Obligatorio", 0);
$validator->create_message("msj_descripcion", "descripcion", " Obligatorio", 0);