<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");

// Objetos
$tablaMaestra  = new tablaMaestra();
$general = new General();

$fab = 0;
if(isset($_GET["fab"]) && filter_var($_GET["fab"], FILTER_VALIDATE_INT) !== false){
    $fab = $_GET["fab"];
}

if($fab == 1 || $fab == 3){
    $tablaMaestra->listadoProductosMaestra($fab);
    $listado = $tablaMaestra->listaProductos; 
} else{
    $listado = $tablaMaestra->listadoConvertirEdicion($fab); 
}

$count = count($listado);