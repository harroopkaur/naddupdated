<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$tablaMaestra = new tablaMaestra();
$validator = new validator("form1");
$general = new General();

$modificar = 0;
$fab = 0;
if(isset($_GET["fab"]) && filter_var($_GET["fab"], FILTER_VALIDATE_INT) !== false){
    $fab = $_GET["fab"];
}

$idDetalle = 0;
if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $idDetalle = $_GET["id"];
}

if (isset($_POST['modificar']) && $_POST["modificar"] == 1) {
    // Validaciones
    $modificar = 1;

    if(isset($_POST["fab"]) && filter_var($_POST["fab"], FILTER_VALIDATE_INT) !== false){
        $fab = $_POST["fab"];
    }

    if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
        $idDetalle = $_POST["id"];
    }
    
    $descripcion = "";
    if(isset($_POST["descripcion"])){
        $descripcion = $general->get_escape($_POST["descripcion"]);
    }
    
    $palabra = "";
    if(isset($_POST["palabra"])){
        $palabra = $general->get_escape($_POST["palabra"]);
    }
    
    /*if ($tablaMaestra->existeEdicionConvertir($fab, $familia, $descripcion) > 0) {
        $exito = 2;
    } else{*/
        if ($tablaMaestra->modificarPalabraConvertir(4, $idDetalle, $descripcion, $palabra)) {
            $exito = 1;
        } 
    //}
}

$tablaMaestra->todosProductosSalida($fab);
$listado = $tablaMaestra->lista;

$rowVersion = $tablaMaestra->registroEspecifico(4, $idDetalle);

$validator->create_message("msj_familia", "familia", " Obligatorio", 0);
$validator->create_message("msj_descripcion", "descripcion", " Obligatorio", 0);