<?php
if ($agregar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro no se pudo insertar', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/convertirEdicion.php?fab=<?= $fab ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 1) {
    ?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/convertirEdicion.php?fab=<?= $fab ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 2) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Ya existe es palabra', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/insertarConvEdicion.php?fab=<?= $fab ?>';
            }
        });
    </script>
    <?php
}
?>
    
<fieldset class='fieldset'>
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Despliegue</span></legend>    
        
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="fab" id="fab" value="<?= $fab ?>" />
        <?php $validator->print_script(); ?>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Familia:</th>
                <td align="left">
                    <select id="familia" name="familia">
                        <option value="">--Seleccione--</option>
                        <?php 
                        foreach($listado as $row){
                        ?>
                            <option value="<?= $row["idDetalle"] ?>"><?= $row["descripcion"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_familia") ?></font></div>
                </td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Edición:</th>
                <td align="left">
                    <input type="text" id="descripcion" name="descripcion" maxlength="70">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_descripcion") ?></font></div>
                </td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Convertir:</th>
                <td align="left">
                    <input type="text" id="palabra" name="palabra" maxlength="70">
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="INSERTAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>