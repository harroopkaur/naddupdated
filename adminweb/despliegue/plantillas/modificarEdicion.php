<?php
if ($modificar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro no se pudo modificar', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/convertirEdicion.php?fab=<?= $fab ?>';
            }
        });
    </script>
    <?php
} else if ($modificar == 1 && $exito == 1) {
    ?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/convertirEdicion.php?fab=<?= $fab ?>';
            }
        });
    </script>
    <?php
} else if ($modificar == 1 && $exito == 2) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Ya existe es palabra', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/modificarEdicion.php?fab=<?= $fab ?>&id=<?= $idDetalle ?>';
            }
        });
    </script>
    <?php
}
?>
    
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Despliegue</span></legend> 
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="modificar" id="modificar" value="1" />
        <input type="hidden" name="fab" id="fab" value="<?= $fab ?>">
        <input type="hidden" name="id" id="id" value="<?= $idDetalle ?>">
        <?php $validator->print_script(); ?>

        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Familia:</th>
                <td align="left">
                    <select id="familia" name="familia" disabled="disabled">
                        <option value="">--Seleccione--</option>
                        <?php 
                        foreach($listado as $row){
                        ?>
                            <option value="<?= $row["idDetalle"] ?>" <?php if($row["idDetalle"] == $rowEdicion["idForaneo"]){ echo "selected='selected'"; } ?>><?= $row["descripcion"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_familia") ?></font></div>
                </td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Edición:</th>
                <td align="left">
                    <input type="text" id="descripcion" name="descripcion" value="<?= $rowEdicion["descripcion"] ?>" maxlength="70">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_descripcion") ?></font></div>
                </td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Convertir:</th>
                <td align="left">
                    <input type="text" id="palabra" name="palabra" value="<?= $rowEdicion["campo1"] ?>" maxlength="70">
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>