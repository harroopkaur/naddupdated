<?php
if ($modificar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro no se pudo modificar', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/excluir.php?fab=<?= $fab ?>';
            }
        });
    </script>
    <?php
} else if ($modificar == 1 && $exito == 1) {
    ?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/excluir.php?fab=<?= $fab ?>';
            }
        });
    </script>
    <?php
} else if ($modificar == 1 && $exito == 2) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Ya existe es palabra', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/despliegue/modificarExcluir.php?fab=<?= $fab ?>&id=<?= $idDetalle ?>';
            }
        });
    </script>
    <?php
}
?>
    
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Despliegue</span></legend> 
    
    <?php 
    if($palabra == ""){
    ?>
        <table style="width:60%;" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
            <thead>
                <tr>
                    <th>Palabra</th>
                    <th>Modificar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody id="bodyTable">
                <?php 
                for($i = 0; $i < $count; $i++){
                ?>
                    <tr>
                        <td align="left"><?= $listado[$i] ?></td>
                        <td class="text-center" style="width:80px;"><a href="#" onclick="modificar(<?= $idDetalle ?>, '<?= $listado[$i] ?>');"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" border="0" alt="Editar" title="Editar" /></a></td>
                        <td class="text-center" style="width:80px;"><a href="#" onclick="eliminar('<?= $listado[$i] ?>');"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    
        <form id="formEliminar" name="formEliminar" method="post"  enctype="multipart/form-data" action="eliminarExcluir.php">
            <input type="hidden" name="fab" id="fabEliminar" value="<?= $fab ?>">
            <input type="hidden" name="id" id="idEliminar" value="<?= $idDetalle ?>">
            <input type="hidden" name="palabra" id="palabraEliminar">
        </form>
    <?php
    } else{
    ?>
        <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="modificar" id="modificar" value="1" />
        <input type="hidden" name="fab" id="fab" value="<?= $fab ?>">
        <input type="hidden" name="id" id="id" value="<?= $idDetalle ?>">
        <input type="hidden" name="palabraOri" id="palabraOri" value="<?= $palabra?> ">
        <?php $validator->print_script(); ?>
                
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Familia:</th>
                <td align="left">
                    <input type="text" id="palabra" name="palabra" value="<?= $palabra ?>" maxlength="150">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_palabra") ?></font></div>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
    <?php
    }
    ?>
</fieldset>

<script>    
    function modificar(idDetalle, palabra){
        location.href = "modificarExcluir.php?fab=<?= $fab ?>&id=" + idDetalle + "&palabra=" + palabra;
    }
    
    function eliminar(palabra){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#palabraEliminar").val(palabra);
                $("#formEliminar").submit();
            }
        });        
    }
</script>