<table style="margin:0 auto; margin-top:45px; margin-bottom:45px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">    
    <?php 
    if($fab == 1 || $fab == 3){
    ?>
        <thead>
            <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                <th  align="center" valign="middle" ><strong class="til">Familia</strong></th>
                <th  align="center" valign="middle" ><strong class="til">Incluir</strong></th>
                <th  align="center" valign="middle" class="til" style="width:70px;"><strong>Modificar</strong></th>
            </tr> 
        </thead>
        <tbody id="bodyTable">
            <?php foreach ($listado as $registro) { ?>
                <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                    <td align="left">
                    <?= $registro["descripcion"] ?>
                    </td>
                    <td align="left">
                    <?= $registro["campo3"] ?>
                    </td>
                    <td  align="center">
                        <a href="modificarIncluir.php?fab=<?= $fab ?>&id=<?= $registro["idDetalle"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                    </td>
                </tr>
            <?php } ?>

        </tbody>
    <?php 
    } else{
    ?>
        <thead>
            <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                <th  align="center" valign="middle" ><strong class="til">Familia</strong></th>
                <th  align="center" valign="middle" ><strong class="til">Edición</strong></th>
                <th  align="center" valign="middle" ><strong class="til">Convertir</strong></th>
                <th  align="center" valign="middle" class="til" style="width:70px;"><strong>Modificar</strong></th>
                <th  align="center" valign="middle" class="til" style="width:70px;"><strong>Eliminar</strong></th>
            </tr> 
        </thead>
        <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                <?= $registro["familia"] ?>
                </td>
                <td align="left">
                <?= $registro["edicion"] ?>
                </td>
                <td align="left">
                <?= $registro["edicionConvertida"] ?>
                </td>
                <td  align="center">
                    <a href="modificarEdicion.php?fab=<?= $fab ?>&id=<?= $registro["idDetalle"] ?>&foraneo=<?= $registro["idForaneoEdicion"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" border="0" alt="Editar" title="Editar" /></a>
                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro["idDetalle"] ?>);"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>

        </tbody>
    <?php
    }
    ?>
</table>

<?php
if($count == 0){ ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay registros</div>
<?php } ?>