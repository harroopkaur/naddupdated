<table style="margin:0 auto; margin-top:45px; margin-bottom:45px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til">Descripción</strong></th>
            <th  align="center" valign="middle" ><strong class="til">Convertir</strong></th>
            <th  align="center" valign="middle" class="til" style="width:70px;"><strong>Modificar</strong></th>
            <th  align="center" valign="middle" class="til" style="width:70px;"><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                <?= $registro["descripcion"] ?>
                </td>
                <td align="left">
                <?= $registro["campo1"] ?>
                </td>
                <td  align="center">
                    <a href="modificarConvertir.php?fab=<?= $fab ?>&id=<?= $registro["idDetalle"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" border="0" alt="Editar" title="Editar" /></a>
                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro["idDetalle"] ?>);"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>

    </tbody>
</table>

<form id="formEliminar" name="formEliminar" method="post"  enctype="multipart/form-data" action="eliminarConvertir.php">
    <input type="hidden" name="fab" id="fabEliminar" value="<?= $fab ?>">
    <input type="hidden" name="id" id="idEliminar">
</form>

<?php
if($count == 0){ ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay registros</div>
<?php } ?>

<script>    
    function modificar(idDetalle, palabra){
        location.href = "modificarExcluir.php?fab=<?= $fab ?>&id=" + idDetalle + "&palabra=" + palabra;
    }
    
    function eliminar(idDetalle){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#idEliminar").val(idDetalle);
                $("#formEliminar").submit();
            }
        });        
    }
</script>