<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$configuraciones = new configuraciones();
$validator = new validator("form1");
$general = new General();

$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    if ($error == 0) {
        if($configuraciones->existeFabricante($_POST['fabricante']) > 0){
            $error = 1;
        }
        else{
            if ($configuraciones->insertarFabricante($general->get_escape($_POST['fabricante']))) {
                $exito = 1;
            } else {
                $error = 5;
            }
        } 
    }
}

$validator->create_message("msj_fabricante", "fabricante", " Obligatorio", 0);