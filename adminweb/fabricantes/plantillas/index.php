<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>

<table style="width:40%; margin:0 auto; margin-top:45px; margin-bottom:45px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroFabricante" name="filtroFabricante" style="width:auto;" value=""></th>
            <th  colspan="2" align="center" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer">Buscar</div></th>
        </tr>
        
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til" id="fabricante" name="fabricante">Fabricante</strong></th>
            <th  align="center" valign="middle" class="til" style="width:70px;"><strong>Modificar</strong></th>
            <th  align="center" valign="middle" class="til" style="width:70px;"><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                <?= $registro["nombre"] ?>
                </td>
                <td  align="center">
                    <a href="modificar.php?id=<?= $registro["idFabricante"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>

                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro['idFabricante'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>

                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php
if($count == 0){ 
?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay fabricantes</div>
<?php 
} 
?>

<script>
    $(document).ready(function(){
        $("#filtroFabricante").keyup(function(e){
            if(e.keyCode == 13){
                $.post("ajax/tablaFabricante.php", { fabricante : $("#filtroFabricante").val(), token : localStorage.licensingassuranceToken }, function(data){
                    if(data[0].resultado === true){
                        $("#bodyTable").empty();
                        $("#bodyTable").append(data[0].tabla);
                    }
                    else{
                        location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                    }
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
        
        $("#buscar").click(function(){
            $.post("ajax/tablaFabricante.php", { fabricante : $("#filtroFabricante").val(), token : localStorage.licensingassuranceToken }, function(data){
                if(data[0].resultado === true){
                    $("#bodyTable").empty();
                    $("#bodyTable").append(data[0].tabla);
                }
                else{
                    location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
    });
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>