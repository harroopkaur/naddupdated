<?php
if($exito == true){      
?>
<script type="text/javascript">
    $.alert.open('alert', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
        if (button === 'Aceptar'){
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/fabricantes/';
        }
    });
</script>
<?php
}
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Fabricante</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                echo "Ocurrió un error al insertar";
            } 
            else if($error == 1){
            ?>
                <script>
                    $.alert.open('alert', 'Ya existe ese fabricante', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                        }
                    });
                </script>
            <?php
            }
            ?></font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Fabricante:</th>
                <td align="left">
                    <input type="text" id="fabricante" name="fabricante" maxlength="70">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fabricante") ?></font></div>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="INSERTAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>