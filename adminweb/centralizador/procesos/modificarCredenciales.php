<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");

// Objetos
$clientes = new Clientes();
$empleados = new empleados();
$general = new General();
$validator = new validator("form1");

//procesos
$error = 0;
$exito = 0;

$id = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id = $_GET['id']; 
}

$id_user = 0;
if(isset($_GET['idEmp']) && filter_var($_GET['idEmp'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['idEmp']; 
}

$empleados->datos($id_user);
$empresas = $clientes->listar_todo();

$autorizado = $empleados->estado; 
if (isset($_POST['insertar']) && filter_var($_POST['idEmp'], FILTER_VALIDATE_INT)  !== false && isset($_POST["login"]) && isset($_POST["clave"])) {   
    if ($error == 0) {        
       
        if ($empleados->verificarPassCent($_POST['idEmp'], $general->get_escape($_POST["clave"])) == 0){
            if ($empleados->actualizarCent($_POST['idEmp'], $general->get_escape($_POST['login']), $general->get_escape($_POST["clave"]))) {
                $exito = 1;
            } else {
                $error = 3;
            }
        } else{
            if ($empleados->actualizarCent1($_POST['idEmp'], $general->get_escape($_POST['login']))) {
                $exito = 1;
            } else {
                $error = 3;
            }
        }
    }
}

$validator->create_message("msj_login", "login", " Obligatorio", 0);
$validator->create_message("msj_clave", "clave", " Obligatorio", 0);