<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_licenciamiento_centralizador.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$licencias = new clase_licenciamiento_centralizador();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$id = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id = $_GET["id"];
}

$idLicencia = 0;
if (isset($_GET["idLicencia"]) && is_numeric($_GET["idLicencia"])){
    $idLicencia = $_GET["idLicencia"];
}

if (isset($_POST['insertar']) && isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        $autorizado = 0;
        if(isset($_POST["modif"]) && ($_POST["modif"] == 1 || $_POST["modif"] == 2)){
            $autorizado = $_POST["modif"];
        }
        
        if ($licencias->actualizar($_POST["idLicencia"], $general->reordenarFecha($_POST['fechaIni'], "/", "-"), $general->reordenarFecha($_POST['fechaFin'], "/", "-"), 
        $autorizado)) {
            $idLicencia = $_POST["idLicencia"];
            $exito = 1;
        } else {
            $exito = 0;
        }
    }
}

$validator->create_message("msj_fechaIni", "fechaIni", " Obligatorio", 0);
$validator->create_message("msj_fechaFin", "fechaFin", " Obligatorio", 0);
$validator->create_message("msj_serial", "serial", " Obligatorio", 0);

$row = $licencias->datos($idLicencia);
$autorizado = $row["status"];