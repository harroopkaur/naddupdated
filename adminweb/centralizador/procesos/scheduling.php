<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_centralizador_web.php");

// Objetos
$centralizador = new clase_centralizador_web();
$general  = new General();

$empresa = "";
if(isset($_GET["emp"])){
    $empresa = $general->get_escape($_GET["emp"]);
}

$nombreScheduling = "";
if(isset($_GET["Sche"])){
    $nombreScheduling = $general->get_escape($_GET["Sche"]);
}

$fechaCreacion = "";
if(isset($_GET["fec"])){
    $fechaCreacion = $general->get_escape($_GET["fec"]);
}

$estadoSche = "";
if(isset($_GET["est"])){
    $estadoSche = $general->get_escape($_GET["est"]);
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '&emp=' . $empresa . '&Sche=' . $nombreScheduling . '&fec=' . $fechaCreacion . '&est=' . $estadoSche;
} else {
    $start_record = 1;
    $parametros   = '';
}

$listado = $centralizador->listaScheduling($empresa, $nombreScheduling, $fechaCreacion, $estadoSche, $start_record);
$count   = $centralizador->totalScheduling($empresa, $nombreScheduling, $fechaCreacion, $estadoSche);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i   = $pag->get_total_pages();