<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

//clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");

// Objetos
$empleados = new empleados();
$general = new General();
//procesos
$exito = false;

if (isset($_POST['idEmp']) && filter_var($_POST['idEmp'], FILTER_VALIDATE_INT) !== false) {
    $exito = $empleados->eliminarCredCent($_POST['idEmp']);
    
    $id = 0;
    if(isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
        $id = $_POST["id"];
    }
}