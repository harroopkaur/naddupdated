<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_centralizador_web.php");

// Objetos
$centralizador = new clase_centralizador_web();
$general  = new General();

$empresa = "";
if(isset($_GET["emp"])){
    $empresa = $general->get_escape($_GET["emp"]);
}

$agente = "";
if(isset($_GET["age"])){
    $agente = $general->get_escape($_GET["age"]);
}

$hostname = "";
if(isset($_GET["host"])){
    $hostname = $general->get_escape($_GET["host"]);
}

$IP = "";
if(isset($_GET["IP"])){
    $IP = $general->get_escape($_GET["IP"]);
}

$lastDataSent = "";
if(isset($_GET["dataSent"])){
    $lastDataSent = $general->get_escape($_GET["dataSent"]);
}

$status = "";
if(isset($_GET["status"])){
    $status = $general->get_escape($_GET["status"]);
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '&emp=' . $empresa . '&age=' . $agente . '&host=' . $hostname . '&IP=' . $IP .
    '&dataSent=' . $lastDataSent . '&status=' . $status;
} else {
    $start_record = 1;
    $parametros   = '';
}

$listado = $centralizador->dataCentralizador($empresa, $agente, $hostname, $IP, $lastDataSent, $status, 1, $start_record);
$count   = $centralizador->total($empresa, $agente, $hostname, $IP, $lastDataSent, $status);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i   = $pag->get_total_pages();