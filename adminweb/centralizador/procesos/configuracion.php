<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_centralizador_web.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$centralizador = new clase_centralizador_web();
$general = new General();
$validator = new validator("form1");
$diasAgente = $centralizador->config(1);

//procesos
$actualizar = 0;
$error = 0;
$exito = 0;

$dias = 7;
if (isset($_POST['actualizar']) && filter_var($_POST['dias'], FILTER_VALIDATE_INT) !== false) {
    $dias = $_POST["dias"];
    $actualizar = 1;
    if ($error == 0) {
        if ($centralizador->actualizarConfig(1, $dias)) {
            $exito = 1;
        } else {
            $error = 3;
        }
    }
}

$validator->create_message("msj_dias", "dias", " Obligatorio", 0);