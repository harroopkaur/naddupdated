<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_licenciamiento_centralizador.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$licencias = new clase_licenciamiento_centralizador();
$clientes = new Clientes();
$validator = new validator("form1");
$general = new General();

$id = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id = $_GET["id"];
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = ''; //filtro adicional si se le coloca input para filtrar por columnas
} else {
    $start_record = 1;
    $parametros   = '';
}

$listado = $licencias->listar_todo_paginado($id, $start_record);
$count   = $licencias->total($id);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i   = $pag->get_total_pages();

$clientes->datos($id);