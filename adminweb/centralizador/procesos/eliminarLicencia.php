<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

//clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_licenciamiento_centralizador.php");

// Objetos
$licencias = new clase_licenciamiento_centralizador();
$general = new General();

//procesos
$exito = false;
if (isset($_POST['idLicencia']) && filter_var($_POST['idLicencia'], FILTER_VALIDATE_INT) !== false) {
    $exito = $licencias->eliminar($_POST['idLicencia']);
    $id = $_POST["id"];
}