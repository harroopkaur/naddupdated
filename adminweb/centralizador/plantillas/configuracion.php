<?php
if ($actualizar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro actualizado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/';
            }
        });
    </script>
    <?php
} else if ($actualizar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'No se pudo actualizar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/';
            }
        });
    </script>
    <?php
}
?>                                
                               
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de la Configuración</span></legend>
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="actualizar" id="actualizar" value="1" />
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
             echo $centralizador->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="400" align="left" valign="top">Días que han pasado para considerar al agente inactivo:</th>
                <td align="left"><input name="dias" id="dias" type="text" value="<?= $diasAgente["val"] ?>" size="30" maxlength="3" style="width:50px;" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_dias") ?></font></div></td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input name="actualizar" type="button" id="actualizar" value="ACTUALIZAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
       $("#dias").numeric(false);
    });
</script>