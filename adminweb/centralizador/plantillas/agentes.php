<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminarAgente.php">
    <input type="hidden" id="id" name="id">
</form>

<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroEmpresa" name="filtroEmpresa" style="width:120px;" value="<?= $empresa ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroAgente" name="filtroAgente" style="width:120px;" value="<?= $agente ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroHostname" name="filtroHostname" style="width:120px;" value="<?= $hostname ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroIP" name="filtroIP" style="width:120px;" value="<?= $IP ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroLastDataSent" name="filtroLastDataSent" style="width:120px;" value="<?= $lastDataSent ?>" readonly></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroStatus" name="filtroStatus" style="width:120px;" value="<?= $status ?>"></th>
            <th  align="center" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer">Buscar</div></th>
        </tr>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="midle" ><strong class="til">Empresa</strong></th>
            <th  align="center" valign="midle" ><strong class="til">Agente</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Hostname</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>IP</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Última Data Enviada</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Status</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left"><?= $registro['empresa'] ?></td>
                <td  align="left"><?= $registro['id'] ?></td>
                <td><?= $registro['tx_host_name'] ?></td>
                <td><?= $registro['tx_ip'] ?></td>
                <td  align="center"><?= $general->muestrafecha($registro['fe_ejeccn']) ?></td>
                <td><?= $registro['estado'] ?></td>
                <td align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div id="paginador" style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>

<?php 
if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay Agentes</div>
<?php 
}
?>

<script>
    $(document).ready(function(){
        $("#filtroLastDataSent").datepicker();
        
        $("#filtroEmpresa").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroAgente").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroHostname").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroIP").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroLastDataSent").click(function(){
            $("#filtroLastDataSent").val("");
        });
        
        $("#filtroStatus").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#buscar").click(function(){
            buscarData();
        }); 
    });
    
    function buscarData(){
        $.post("ajax/agentes.php", { empresa : $("#filtroEmpresa").val(), agente : $("#filtroAgente").val(), 
        hostname : $("#filtroHostname").val(), IP : $("#filtroIP").val(), lastDataSent : $("#filtroLastDataSent").val(), 
        status : $("#filtroStatus").val(), pagina : 1, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb";
                return false;
            }
            $("#bodyTable").empty();
            $("#paginador").empty();
            $("#bodyTable").append(data[0].tabla);
            $("#paginador").append(data[0].paginador);
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>