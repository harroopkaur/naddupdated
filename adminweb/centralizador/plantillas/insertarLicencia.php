<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/licencias.php?id=<?= $id ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/';
            }
        });
    </script>
<?php
} 
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de la Licencia del Producto</span></legend>

        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id ?>" />
        <?php $validator->print_script(); ?>

        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
             echo $llaves->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="150" align="left" valign="top">Inicio Acceso:</th>
                <td colspan="2" align="left"><input name="fechaIni" id="fechaIni" type="text" value="" size="30" maxlength="20" readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaIni") ?></font></div></td>
            </tr>

            <tr>
                <th width="150" align="left" valign="top">Fin Acceso:</th>
                <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="" size="30" maxlength="20" readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
            </tr>

            <tr>
                <th width="150" align="left" valign="top">Serial:</th>
                <td width="100" align="left"><input name="serial" id="serial" type="text" value="" size="30" maxlength="20"  readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_serial") ?></font></div></td>
                <td align="left"><input name="generar" type="button" id="generar" value="Generar" class="boton" /></td>
            </tr>
        </table>

        <div style="width:77px; margin:0 auto;">
            <input style="margin:0 auto; margin-top:10px;" name="insertar" type="button" id="insertar" value="CREAR" onclick="validate();" class="boton" />
        </div>
    </fieldset>
</form>  
    
<script>
    $(document).ready(function(){
        $("#cantidad").numeric(false);
        $("#fechaIni").datepicker();
        $("#fechaFin").datepicker();
        
        $("#cantidad").click(function(){
           $("#cantidad").val(""); 
        });
        
        $("#generar").click(function(){
           $.post("ajax/generarSerial.php", { token : localStorage.licensingassuranceToken }, function(data){
                if(data[0].resultado === true){
                    $("#serial").val(data[0].llaveGenerada);
                }
                else{
                    $.alert.open('alert', "No se pudo generar el serial por favor intente de nuevo", {'Aceptar' : 'Aceptar'});
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            }); 
        });
    });
</script>