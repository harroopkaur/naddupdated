<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro actualizado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/scheduling.php';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $error > 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'No se pudo actualizar el registro', {'Aceptar': 'Aceptar'});
    </script>
    <?php
}
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data" action="actualizarScheduling.php">
    <input type="hidden" name="insertar" id="insertar" value="1" />
    <input type="hidden" name="id" id="id" value="<?= $id ?>" />
    
    <table class="tablap2" style="width:500px; margin:0 auto; margin-top:20px;">
        <tr>
            <th width="150" align="left" valign="top">Empresa:</th>
            <td align="left">
                <select name="empresa" id="empresa">
                    <?php
                    foreach($empresas as $row){
                    ?>
                        <option value="<?= $row["id"] ?>" <?php if($row["id"] == $datosScheduling["cliente"]){ echo "selected='selected'"; } ?>><?= $row["empresa"] ?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th width="150" align="left" valign="top">Nombre de la Tarea:</th>
            <td align="left"><input name="nombreTarea" id="nombreTarea" type="text" value="<?= $datosScheduling["tx_descrip"] ?>" size="30" maxlength="250" />
            </td>
        </tr>
    </table>
    
    <br>
    <div class="contentSchedule">
        <fieldset class="fieldsetSchedule">
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Schedule</span></legend>
            <input type="radio" name="schedule" id="scheduleDiario" value="Daily" <?php if($datosScheduling["tx_schedule"] == "Daily") echo "checked"; ?>>Diario<br>
            <input type="radio" name="schedule" id="scheduleSemanal" value="Weekly" <?php if($datosScheduling["tx_schedule"] == "Weekly") echo "checked"; ?>>Semanal<br>          
            <input type="radio" name="schedule" id="scheduleMensual" value="Monthly" <?php if($datosScheduling["tx_schedule"] == "Monthly") echo "checked"; ?>>Mensual<br>          
        </fieldset>

        <fieldset class="fieldsetInicio">
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Inicio</span></legend>
            <label>Día de Inicio</label><input type="text" name="fechaInicio" id="fechaInicio" readonly value="<?= $fecha ?>"><br>
            <label>Hora de Inicio</label><input type="time" name="horaInicio" id="horaInicio" value="<?= $hora ?>">         
        </fieldset>
    </div>
   
    <fieldset class="fieldsetDaily <?php if($datosScheduling["tx_schedule"] != "Daily") echo "hide"; ?>" id="horarioDia">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Diario</span></legend>
        <span>
            Repetir cada:
        </span>
        <input type="text" name="repetirDia" id="repetirDia" style="width:50px;" value="<?= $datosScheduling["int_repeat_days"] ?>">
    </fieldset>
    
    <fieldset class="fieldsetWeekly <?php if($datosScheduling["tx_schedule"] != "Weekly") echo "hide"; ?>" id="horarioSemana">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Semanal</span></legend>
        <div>
            <span>
                Repetir cada:
            </span>
            <input type="text" name="repetirSemana" id="repetirSemana" style="width:50px;" value="<?= $datosScheduling["int_repeat_weeks"] ?>">

            <span>
                Semana en:
            </span>
        </div>
        
        <span class="cont-monthly-label">
            Día:
        </span>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="dia[]" id="dia1" value="Sunday" <?php if(strpos($datosScheduling["tx_days"], 'Sunday') !== false){ echo "checked"; } ?>>Domingo<br><br>
            <input type="checkbox" name="dia[]" id="dia2" value="Monday" <?php if(strpos($datosScheduling["tx_days"], 'Monday') !== false){ echo "checked"; } ?>>Lunes<br><br>
            <input type="checkbox" name="dia[]" id="dia3" value="Tuesday" <?php if(strpos($datosScheduling["tx_days"], 'Tuesday') !== false){ echo "checked"; } ?>>Martes<br><br>      
            <input type="checkbox" name="dia[]" id="dia4" value="Wednesday" <?php if(strpos($datosScheduling["tx_days"], 'Wednesday') !== false){ echo "checked"; } ?>>Miércoles
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="dia[]" id="dia5" value="Thursday" <?php if(strpos($datosScheduling["tx_days"], 'Thursday') !== false){ echo "checked"; } ?>>Jueves<br><br>
            <input type="checkbox" name="dia[]" id="dia6" value="Friday" <?php if(strpos($datosScheduling["tx_days"], 'Friday') !== false){ echo "checked"; } ?>>Viernes<br><br>
            <input type="checkbox" name="dia[]" id="dia7" value="Saturday" <?php if(strpos($datosScheduling["tx_days"], 'Saturday') !== false){ echo "checked"; } ?>>Sábado
        </div>
    </fieldset>
    
    <fieldset class="fieldsetMonthly <?php if($datosScheduling["tx_schedule"] != "Monthly") echo "hide"; ?>" id="horarioMes">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Mensual</span></legend>
        <span class="cont-monthly-label">
            Meses:
        </span>
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month1" value="January" <?php if(strpos($datosScheduling["tx_months"], 'January') !== false){ echo "checked"; } ?>>Enero<br><br>
            <input type="checkbox" name="month[]" id="month2" value="February" <?php if(strpos($datosScheduling["tx_months"], 'February') !== false){ echo "checked"; } ?>>Febrero<br><br>
            <input type="checkbox" name="month[]" id="month3" value="March" <?php if(strpos($datosScheduling["tx_months"], 'March') !== false){ echo "checked"; } ?>>Marzo<br><br>      
            <input type="checkbox" name="month[]" id="month4" value="April" <?php if(strpos($datosScheduling["tx_months"], 'April') !== false){ echo "checked"; } ?>>Abril
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month5" value="May" <?php if(strpos($datosScheduling["tx_months"], 'May') !== false){ echo "checked"; } ?>>Mayo<br><br>
            <input type="checkbox" name="month[]" id="month6" value="June" <?php if(strpos($datosScheduling["tx_months"], 'June') !== false){ echo "checked"; } ?>>Junio<br><br>
            <input type="checkbox" name="month[]" id="month7" value="July" <?php if(strpos($datosScheduling["tx_months"], 'July') !== false){ echo "checked"; } ?>>Julio<br><br>    
            <input type="checkbox" name="month[]" id="month8" value="August" <?php if(strpos($datosScheduling["tx_months"], 'August') !== false){ echo "checked"; } ?>>Agosto
        </div>
        
        <div class="float-lt" style="margin-left:20px; padding:10px;">
            <input type="checkbox" name="month[]" id="month9" value="September" <?php if(strpos($datosScheduling["tx_months"], 'September') !== false){ echo "checked"; } ?>>Septiembre<br><br>
            <input type="checkbox" name="month[]" id="month10" value="October" <?php if(strpos($datosScheduling["tx_months"], 'October') !== false){ echo "checked"; } ?>>Octubre<br><br>
            <input type="checkbox" name="month[]" id="month11" value="November" <?php if(strpos($datosScheduling["tx_months"], 'November') !== false){ echo "checked"; } ?>>Noviembre<br><br>       
            <input type="checkbox" name="month[]" id="month12" value="December" <?php if(strpos($datosScheduling["tx_months"], 'December') !== false){ echo "checked"; } ?>>Diciembre
        </div>
       
        <br style="clear:both;"><br>
        <span class="cont-monthly-label">
            Día:
        </span>
        
        <?= $listaCheck ?>
    </fieldset>
    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <td colspan="2" align="center"><input name="crear" type="button" id="crear" value="ACTUALIZAR" onclick="validar();" class="boton" /></td>
        </tr>
    </table>
 </form>
    
<script>
    $(document).ready(function(){   
        $("#repetirDia").numeric(false);
        $("#repetirSemana").numeric(false);
        $("#fechaInicio").datepicker();
        
        $("#crear").click(function(){
            if (validar() === false){
                return false;
            }
            
            $("#form1").submit();
        });
        
        $("#fechaInicio").click(function(){
            $("#fechaInicio").val("");
        });
        
        $("#scheduleDiario").click(function(){
            ocultarCampos();
            $("#horarioDia").show();
        });
        
        $("#scheduleSemanal").click(function(){
            ocultarCampos();
            $("#horarioSemana").show();
        });
        
        $("#scheduleMensual").click(function(){
            ocultarCampos();
            $("#horarioMes").show();
        });
    });
    
    function ocultarCampos(){
        $("#horarioDia").hide();
        $("#horarioSemana").hide();
        $("#horarioMes").hide();
        reiniciarCampos();
    }
    
    function reiniciarCampos(){
        $("#repetirDia").val("");
        $("#repetirSemana").val("");
        
        for(index = 1; index < 8; index++){
            $("#dia" + index).prop("checked", false);
        }
        
        for(index = 1; index < 13; index++){
            $("#month" + index).prop("checked", false);
        }
        
        for(index = 1; index < 32; index++){
            $("#day" + index).prop("checked", false);
        }
    }
    
    
    function validar(){
        result = true;
        
        if ($("#nombreTarea").val() === ""){
            $.alert.open('warning', "Debe llenar el nombre de la tarea", {'Aceptar' : 'Aceptar'}, function(){
                $("#nombreTarea").focus();
            });
            return false;
        }
        
        if ($("#fechaInicio").val() === ""){
            $.alert.open('warning', "Debe llenar la fecha de inicio", {'Aceptar' : 'Aceptar'}, function(){
                $("#fechaInicio").focus();
            });
            return false;
        }
        
        if ($("#horaInicio").val() === ""){
            $.alert.open('warning', "Debe llenar la hora de inicio", {'Aceptar' : 'Aceptar'}, function(){
                $("#horaInicio").focus();
            });
            return false;
        }
        
        if ($("#scheduleDiario").prop("checked") && $("#repetirDia").val() === ""){
            $.alert.open('warning', "Debe llenar cada cuanto se repite la tarea", {'Aceptar' : 'Aceptar'}, function(){
                $("#repetirDia").focus();
            });
            return false;
        } else if ($("#scheduleSemanal").prop("checked")){
            if ($("#repetirSemana").val() === ""){
                $.alert.open('warning', "Debe llenar cada cuanto se repite la tarea", {'Aceptar' : 'Aceptar'}, function(){
                    $("#repetirSemana").focus();
                });
                return false;
            } 
            
            check = 0;
            for(index = 1; index < 8; index++){
                if($("#dia" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "Debe seleccionar al menos día de la semana", {'Aceptar' : 'Aceptar'});
                return false;
            }
        } else if($("#scheduleMensual").prop("checked")){
            check = 0;
            for(index = 1; index < 13; index++){
                if($("#month" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "Debe seleccionar al menos un mes", {'Aceptar' : 'Aceptar'});
                return false;
            }
            
            check = 0;
            for(index = 1; index < 32; index++){
                if($("#day" + index).prop("checked")){
                    check++;
                }
            }
            
            if(check === 0){
                $.alert.open('warning', "Debe seleccionar al menos un día del mes", {'Aceptar' : 'Aceptar'});
                return false;
            }
        }
        
        return true;
    }
</script>