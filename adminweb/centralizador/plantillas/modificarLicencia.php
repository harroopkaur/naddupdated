<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro actualizado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/licencias.php?id=<?= $id ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/';
            }
        });
    </script>
    <?php
} 
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de la Licencia del Producto</span></legend>

            <input type="hidden" name="insertar" id="insertar" value="1" />
            <input type="hidden" name="id" id="id" value="<?= $id ?>" />
            <input type="hidden" name="idLicencia" id="idLicencia" value="<?= $idLicencia ?>" />
            <input type="hidden" name="modif" id="modif" value="<?= $autorizado ?>">
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $llaves->error;
            } ?></font>
            </div>
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
                <tr>
                    <th width="150" align="left" valign="top">Inicio Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaIni" id="fechaIni" type="text" value="<?= $row["fechaIni"] ?>" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaIni") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Fin Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="<?= $row["fechaFin"] ?>" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Serial:</th>
                    <td width="100" align="left"><input name="serial" id="serial" type="text" value="<?= $row["serial"] ?>" size="30" maxlength="20"  readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_serial") ?></font></div></td>
                    <td align="left"></td>
                </tr>
                
                <tr>
                    <th width="80" align="left" valign="top">Autorizado:</th>
                    <td align="left">
                        <input type="checkbox" id="autorizado" name="autorizado" <?php if($row["status"] == 1 || $row["status"] == 2){ echo 'checked="checked" disabled="disabled"'; } ?>>
                    </td>
                </tr>
            </table>
            
            <div style="width:77px; margin:0 auto;">
                <input style="margin:0 auto; margin-top:10px;" name="insertar" type="button" id="insertar" value="MODIFICAR" onclick="validate();" class="boton" />
            </div>
    </fieldset>
</form>  
    
<script>
    $(document).ready(function(){
        $("#fechaIni").datepicker();
        $("#fechaFin").datepicker();
        
        $("#autorizado").click(function(){
            if($("#autorizado").prop("disabled")){
                return false;
            }
            
            if($("#autorizado").prop("checked")){
                $("#modif").val(1);
            } else{
                $("#modif").val(0);
            }
        });
    });
</script>