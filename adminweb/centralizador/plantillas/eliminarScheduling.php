<?php if($exito == true){  ?>
    <script type="text/javascript">
        $.alert.open('Registro eliminado con éxito', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/scheduling.php';
            }
        });
    </script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('Registro no ha sido eliminado', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/centralizador/scheduling.php';
            }
        });
    </script>
<?php
}
?>