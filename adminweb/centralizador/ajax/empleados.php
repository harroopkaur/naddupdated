<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $empleados = new empleados();

            $id = 0;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["id"];
            }
            
            $nombre = "";
            if(isset($_POST["nombre"])){
                $nombre = $general->get_escape($_POST["nombre"]);
            }
            
            $empresa = "";
            if(isset($_POST["empresa"])){
                $empresa = $general->get_escape($_POST["empresa"]);
            }

            $email = "";
            if(isset($_POST["email"])){
                $email = $general->get_escape($_POST["email"]);
            }

            $fecha = "";
            if(isset($_POST["fecha"])){
                $fecha = $general->get_escape($_POST["fecha"]);
            }

            $estado = "-1";
            if(isset($_POST["estado"])){
                $estado = $general->get_escape($_POST["estado"]);
            }

            $start_record = 0;
            if(isset($_POST["pagina"]) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false){
                $start_record = $_POST["pagina"];
            }

            $listado = $empleados->listar_todo_paginadoCliente($id, $nombre, $empresa, $email, $fecha, $estado, $start_record);
            $count   = $empleados->totalCliente($id, $nombre, $empresa, $email, $fecha, $estado);
            $condiciones  =  '&id='. $id .'&nom=' . $nombre . '&emp=' . $empresa . '&ema=' . $email . '&fec=' . $fecha . '&est=' . $estado;

            $pag = new paginator($count, $general->limit_paginacion, 'credenciales.php?', $condiciones);
            $i = $pag->get_total_pages();

            $tabla = "";
            foreach ($listado as $registro) {
                $tabla .= '<tr onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                    <td align="left">' . $registro["nombre"] . ' ' . $registro["apellido"] . '</td>
                    <td align="left">' . $registro["correoEmpleado"] . '</td>
                    <td align="left">' . $registro["empresa"] . '</td>
                    <td align="left">' . $general->muestrafecha($registro['fecha_registro']) . '</td>
                    <td align="left">';
                        if($registro['estado'] == 1){ $tabla .= 'Autorizado'; }else{ $tabla .= 'No autorizado'; } 
                    $tabla .= '</td>
                    <td  align="center">
                        <a href="modificarCredenciales.php?id=' . $id . '&idEmp=' . $registro["idEmpleado"] . '"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                    </td>
                    <td align="center">
                        <a href="#" onclick="eliminar(' . $id . ', ' . $registro['idEmpleado'] . ')"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                    </td>
                </tr>';
            }

            $paginador = $pag->print_paginatorAjax("");
            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$paginador, 'resultado'=>true));
        }  
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);