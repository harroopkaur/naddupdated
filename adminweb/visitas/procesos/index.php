<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_visitas.php");

// Objetos
$visitas = new clase_visitas();
$general  = new General();

//procesos
$pagina = "";
$fechaIni = "";
$fechaIni1 = "";

if(isset($_GET["pagina"])){
    $pagina = $general->get_escape($_GET["pagina"]);
}

if(isset($_GET["fechaIni"]) && $_GET["fechaIni"] != ""){
    $fechaIni1 = $_GET["fechaIni"];
    $fechaIni = $general->reordenarFecha($_GET["fechaIni"], "/", "-");
}

$fechaFin = "";
$fechaFin1 = "";
if(isset($_GET["fechaFin"]) && $_GET["fechaFin"] != ""){
    $fechaFin1 = $_GET["fechaFin"];
    $fechaFin = $general->reordenarFecha($_GET["fechaFin"], "/", "-");
}

$listado = $visitas->visitas($pagina, $fechaIni, $fechaFin);
$grafico = $visitas->visitasPorIP($pagina, $fechaIni, $fechaFin);
$total = 0;
foreach($grafico as $row){
    $total += $row["cantidad"];
}

$grafico1 = $visitas->visitasPorPais($pagina, $fechaIni, $fechaFin);
$total1 = 0;
foreach($grafico1 as $row){
    $total1 += $row["cantidad"];
}

$count = count($listado);