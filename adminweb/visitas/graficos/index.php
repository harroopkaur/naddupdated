<script type="text/javascript">
    $(function () {
        Highcharts.setOptions({
            colors: ['#DCDCDC', '#99BFDC', '#243C67', '#00AFF0', '#006FC0', '#95DEF8', '#579BCC', 
            '#344970', '#047FB3', '#05B5FE', '#81816A']
        });

        $('#container1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Visitas por IP'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> IP: <b>{point.y:.0f}</b> de <b><?= $total ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: 'Visitas por IP',
                colorByPoint: true,
                data: [
                    <?php 
                    $i = 0;
                    foreach($grafico as $row){
                    ?>
                        {
                            name: '<?= $row["IP"] ?>',
                            y: <?= $row["cantidad"] ?>,
                        },
                    <?php
                        $i++;
                    }
                    ?>
                ]
            }]
        });
        
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Visitas por Pais'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> IP: <b>{point.y:.0f}</b> de <b><?= $total1 ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: 'Visitas por IP',
                colorByPoint: true,
                data: [
                    <?php 
                    $i = 0;
                    foreach($grafico1 as $row){
                    ?>
                        {
                            name: '<?= $row["pais"] ?>',
                            y: <?= $row["cantidad"] ?>,
                        },
                    <?php
                        $i++;
                    }
                    ?>
                ]
            }]
        });
    });
</script>