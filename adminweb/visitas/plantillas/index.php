<div style="float:left;"><span><strong>Página </strong></span>
    <select id="pagina" name="pagina">
        <option value="">--Seleccione--</option>
        <option value="Home" <?php if($pagina == "Home"){ echo "selected='selected'"; } ?>>Home</option>
        <option value="About Us" <?php if($pagina == "About Us"){ echo "selected='selected'"; } ?>>About Us</option>
        <option value="Services" <?php if($pagina == "Services"){ echo "selected='selected'"; } ?>>Services</option>
        <option value="SAM as a Service" <?php if($pagina == "SAM as a Service"){ echo "selected='selected'"; } ?>>SAM as a Service</option>
        <option value="Audit Defense" <?php if($pagina == "Audit Defense"){ echo "selected='selected'"; } ?>>Audit Defense</option>
        <option value="SAM as a Service for SPLA" <?php if($pagina == "SAM as a Service for SPLA"){ echo "selected='selected'"; } ?>>SAM as a Service for SPLA</option>
        <option value="Deployment Diagnostic" <?php if($pagina == "Deployment Diagnostic"){ echo "selected='selected'"; } ?>>Deployment Diagnostic</option>
        <option value="Overview" <?php if($pagina == "Overview"){ echo "selected='selected'"; } ?>>Overview</option>
        <option value="Articles" <?php if($pagina == "Articles"){ echo "selected='selected'"; } ?>>Articles</option>
    </select>
</div>
<div style="float:left; margin-left:15px;"><span><strong>Fecha Inicio </strong></span><input type="text" id="fechaIni" name="fechaIni" value="<?= $fechaIni1 ?>" readonly></div>
<div style="float:left; margin-left:15px;"><span><strong>Fecha Fin </strong></span><input type="text" id="fechaFin" name="fechaFin" value="<?= $fechaFin1 ?>" readonly></div>
<div style="float:left; margin-left:15px; margin-top:-5px;"><div class="botones_m2Alterno boton1" id="buscar">Buscar</div></div>
<div style="float:right; margin-top:-5px;"><div class="botones_m2Alterno boton1" onclick="window.open('reportes/index.php?pagina=<?= $pagina ?>&fechaIni=<?= $fechaIni1 ?>&fechaFin=<?= $fechaFin1 ?>');">Exportar</div></div>
<br><br><br>

<div id="container1" style="height:400px; width:45%; margin:10px; float:left"></div>
<div id="container2" style="height:400px; width:45%; margin:10px; float:right"></div>

<br style="clear:both;"><br>
<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="midle" ><strong class="til">IP</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Página</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>País</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Fecha Registro</strong></th>
        </tr> 
    </thead>
    
    <tbody>
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left"><?= $registro['IP'] ?></td>
                <td  align="left"><?= $registro['modulo'] ?></td>
                <td  align="left"><?= $registro['pais'] ?></td>
                <td  align="center"><?= $registro['fecha'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php 
if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay Visitas</div>
<?php 
}
?>
<script>
    $(document).ready(function(){
        $("#fechaIni").datepicker();
        $("#fechaFin").datepicker();
        
        $("#buscar").click(function(){
            if(($("#fechaIni").val() != "" && $("#fechaFin").val() == "") || 
            ($("#fechaFin").val() != "" && $("#fechaIni").val() == "")){
                $.alert.open('warning', "Alerta", "Debe seleccionar la fecha de inicio y la fecha fin", {'Aceptar' : 'Aceptar'});
                return false;
            }
            
            if($('#pagina').val() === "" && $("#fechaIni").val() === "" && $("#fechaFin").val() === ""){
                location.href = "index.php?pagina=" + $('#pagina').val() + "&fechaIni=" + $("#fechaIni").val() + "&fechaFin=" + $("#fechaFin").val();
            } else if ($('#pagina').val() === ""){
                location.href = "index.php?fechaIni=" + $("#fechaIni").val() + "&fechaFin=" + $("#fechaFin").val();
            } else if($("#fechaIni").val() === "" && $("#fechaFin").val() === ""){
                location.href = "index.php?pagina=" + $('#pagina').val();
            }
            
        });
    });
</script>