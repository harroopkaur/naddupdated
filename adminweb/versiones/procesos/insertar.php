<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$configuraciones = new configuraciones();
$validator = new validator("form1");
$general = new General();

$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    if($configuraciones->existeVersion($_POST['version']) > 0){
            $error = 1;
    }
    else{

        if ($error == 0) {
            if ($configuraciones->insertarVersion($general->get_escape($_POST['version']))) {
                $exito = 1;
            } else {
                $error = 5;
            }
        }
    }
}

$validator->create_message("msj_version", "version", " Obligatorio", 0);