<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$passLAD = new clase_pass();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

if (isset($_POST['insertar']) && isset($_POST['pass'])) {
    // Validaciones
    $agregar = 1;
    $encrip = $passLAD->encriptar($general->get_escape($_POST['pass']));
   
    if ($passLAD->pass_existe($encrip, 0)) {
        $error = 2;
    }  // pass duplicado
   
    if ($error == 0) {
        $idDetalle = $passLAD->ultId() + 1;
        if ($passLAD->insertar($idDetalle, $encrip)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_pass", "pass", " Obligatorio", 0);