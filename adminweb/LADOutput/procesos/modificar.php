<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$passLAD = new clase_pass();
$general = new General();
$validator = new validator("form1");

//procesos
$error = 0;
$exito = 0;
$modificar = 0;

$idDetalle = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id = $_GET['id']; 
}

$row = $passLAD->datos($id);

if (isset($_POST['insertar']) && isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {   
    $modificar = 1;
    $encrip = $passLAD->encriptar($general->get_escape($_POST['pass']));
   
    $id = $_POST['id']; 
    if ($passLAD->pass_existe($encrip, $id)) {
        $error = 2;
    }  // pass duplicado
    
    if ($error == 0) {               
        if ($passLAD->actualizar($id, $encrip)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_pass", "pass", " Obligatorio", 0);