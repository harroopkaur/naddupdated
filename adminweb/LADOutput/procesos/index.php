<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");

// Objetos
$passLAD = new clase_pass();
$general   = new General();

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '';
} else {
    $start_record = 0;
    $parametros   = '';
}

$listado = $passLAD->listar_todo_paginado($start_record);
$count = $passLAD->total();

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i = $pag->get_total_pages();