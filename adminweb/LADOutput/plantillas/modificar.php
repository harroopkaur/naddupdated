<?php
if ($exito == 1 && $modificar == 1 && $error == 0) { ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/LADOutput/';
            }
        });
    </script>
<?php
} else if ($modificar == 1 && $exito == 0 && $error == 0){
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alerta', 'No se pudo agregar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/LADOutput/';
            }
        });
    </script>
<?php  
} else if ($error == 2){
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alerta', 'Ya existe la contraseña', {'Aceptar': 'Aceptar'});
    </script>
<?php  
} else if ($error == 5){
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alerta', '<?= $passLAD->error ?>', {'Aceptar': 'Aceptar'});
    </script>
<?php  
}     
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Modificar Datos del Empleado</span></legend>
    
    <form id="form1" name="form1" method="post" enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id ?>">
        <?php $validator->print_script(); ?>
        <div class="error_prog"><font color="#FF0000"><?php 
        if ($error == 2) {
            echo "Ya existe la contraseña";
        } else if ($error == 5) {
            echo $passLAD->error;
        } ?></font>
        </div>
 
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Nombre:</th>
                <td align="left"><input name="pass" id="pass" type="text" value="<?= trim($passLAD->desencriptar($row["descripcion"]), chr(0)) ?>" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pass") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>