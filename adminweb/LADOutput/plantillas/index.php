<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>

<table style="width:60%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="midle" ><strong class="til">Contraseña</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Fecha Creación</strong></th>
            <th  align="center" valign="middle" class="til" ></th>
            <th  align="center" valign="middle" class="til" ></th>
        </tr> 
    </thead>
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left"><?= $passLAD->desencriptar($registro['descripcion']) ?></td>
                <td  align="left"><?= $registro['campo1'] ?></td>
                <td  align="center">
                    <a href="modificar.php?id=<?= $registro['idDetalle'] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>

                </td>
                <td align="center">
                    <a href="#" onclick="eliminar(<?= $registro['idDetalle'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>

    </tbody>
</table>
<div id="paginador" style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>
<?php 
    if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay Contraseñas</div>
<?php } ?>

<script>        
    function eliminar(id){
        $.alert.open('confirm', 'Desea anular el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>