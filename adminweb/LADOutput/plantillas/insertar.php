<?php
if ($agregar == 1 && $exito == 1 && $error == 0) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/LADOutput/';
            }
        });
    </script>
<?php  
} else if ($agregar == 1 && $exito == 0 && $error == 0){
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alerta', 'No se pudo agregar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/LADOutput/';
            }
        });
    </script>
<?php  
} else if ($error == 2){
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alerta', 'Ya existe la contraseña', {'Aceptar': 'Aceptar'});
    </script>
<?php  
}
?>
    
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de la Contraseña</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <?php $validator->print_script(); ?>
        <div class="error_prog">
            <font color="#FF0000">
                <?php if ($error == 2) {
                    echo "Ya existe la contraseña";
                } else if ($error == 5) {
                    echo $passLAD->error;
                } ?>
            </font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Contraseña:</th>
                <td align="left">
                    <input name="pass" id="pass" type="text" value="" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pass") ?></font></div>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="CREAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>