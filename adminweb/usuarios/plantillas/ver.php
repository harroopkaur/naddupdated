<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos del Usuario</span></legend>
    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <th width="100" align="left" valign="middle">Login:</th>
            <td align="left" valign="middle"><?= $usuario->login ?></td>
        </tr>
        <tr>
            <th width="100" align="left" valign="middle">Nombre:</th>
            <td align="left" valign="middle"><?= $usuario->nombre ?></td>
        </tr>
        <tr>
            <th width="100" align="left" valign="middle">Apellido:</th>
            <td align="left" valign="middle"><?= $usuario->apellido ?></td>
        </tr>
        <tr>
            <th width="100" align="left" valign="middle">E-mail:</th>
            <td align="left" valign="middle"><?= $usuario->email ?></td>
        </tr>
        <tr>
            <th width="100" align="left" valign="middle">Tipo:</th>
            <td align="left" valign="middle"><?php if ($usuario->tipo == 1) {
                echo "Administrador";
            } ?></td>
        </tr>
    </table>
</fieldset>