 <?php if ($exito == true) { ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/usuarios/';
            }
        });
    </script>
<?php
}
?>
        
<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="modificar" id="modificar" value="1" />
    <?php $validator->print_script(); ?>
    
    <input type="hidden" name="id" id="id" value="<?= $_SESSION['usuario_id'] ?>">
    <div class="error_prog"><font color="#FF0000"><?php if ($error == 3) {
            echo $usuario->error;
        } ?></font>
    </div>
    
    
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left: 15px;"><span class="bold">Modificar Clave</span></legend>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="105" align="left" valign="middle">Clave Nueva:</th>
                <td width="845" align="left"><input name="clave1" id="clave1" type="password" value=""  maxlength="250" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_clave1") ?></font></div></td>
            </tr>
            <tr>
                <th width="105" align="left" valign="middle">Repetir clave:</th>
                <td align="left"><input name="clave2" id="clave2" type="password" value=""  maxlength="250" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_clave2") ?> <?php if ($error == 1) {
                    echo "Las claves no coinciden";
                } ?></font></div></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </fieldset>
</form>