<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>

<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til">Nombre</strong></th>
            <th align="center" valign="middle" class="til" ><strong>Email</strong></th>
            <th align="center" valign="middle" class="til" ><strong>Fecha Registro</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Estado</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Cambio de Clave</strong></th>
            <th align="center" valign="middle" class="til" ><strong>Modificar</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    
    <tbody>
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="center"><a href="ver.php?id=<?= $registro['id'] ?>">
                    <?= $registro['nombre'] . ' ' . $registro['apellido'] ?>
                    </a></td>
                <td  align="center"><?= $registro['email'] ?></td>
                <td  align="center"><?= $general->muestrafecha($registro['fecha_registro']) ?></td>
                <td  align="center">
                    <?php if ($registro['estado'] == 1) { echo 'Autorizado'; }else{ echo 'No autorizado'; } ?>
                </td>
                <td align="center" valign="middle"><?php if ($registro['id'] == $_SESSION['usuario_id']) { ?> <a href="clave.php"><img src="<?= $GLOBALS['domain_root'] ?>/imagenes/png/glyphicons_203_lock.png" width="21" height="26" /></a>  <?php  } else {?>  -   <?php }?></td>
                <td  align="center">
                    <?php if ($registro['estado'] != 0) { ?>
                        <a href="modificar.php?id=<?= $registro['id'] ?>"><img src="<?= $GLOBALS['domain_root'] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                    <?php } else {
                        echo '&nbsp;';
                    } ?>
                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS['domain_root'] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>

<?php if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay Usuarios</div>
<?php } ?>
                                
<script>
    function eliminar(id){
        $.alert.open('confirm', '"¿Está seguro que desea eliminar este registro', {'Si': 'Si', 'No': 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>