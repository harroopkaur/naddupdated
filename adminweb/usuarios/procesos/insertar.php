<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware
//
// Clases
require($GLOBALS["app_root"] . "/clases/clase_general.php");
require($GLOBALS["app_root"] . "/clases/clase_usuario.php");
require($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$usuario = new Usuario();
$general = new General();
$validator = new validator("form1");

$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
    // Validaciones
    if ($usuario->login_existe($_POST['login'], 0)) {
        $error = 1;
    }  // Login duplicado
    if ($usuario->email_existe($_POST['email'], 0)) {
        $error = 2;
    }  // Email duplicado
    if (strlen($_POST['contrasena']) < 6) {
        $error = 3;
    }  // Contrase�a m�nima de 6 caracteres

    if ($error == 0) {
        if ($usuario->insertar($_POST['login'], $_POST['contrasena'], $general->get_escape($_POST['nombre']), 
        $general->get_escape($_POST['apellido']), $general->get_escape($_POST['email']), 1)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_login", "login", " Obligatorio", 0);
$validator->create_message("msj_contrasena", "contrasena", " Obligatorio", 0);
$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);
$validator->create_message("msj_apellido", "apellido", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);