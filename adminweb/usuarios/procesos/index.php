<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario.php");

// Objetos
$usuario = new Usuario();
$general = new General();

if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros = ""; //filtro adicional si se le coloca input para filtrar por columnas
} else {
    $start_record = 0;
    $parametros = "";
}

// Listar Usuarios
$listado = $usuario->listar_todo_paginado($start_record);
$count = $usuario->total();

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i = $pag->get_total_pages();