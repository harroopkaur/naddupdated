<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware
//
// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$general   = new General();
$usuario   = new Usuario();
$usuario2  = new Usuario();
$validator = new validator("form1");

$id_usuario = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_usuario = $general->get_escape($_GET['id']); 
}
$usuario->datos($id_usuario);

$error = 0;
$exito = 0;

if (isset($_POST['modificar']) && filter_var($_POST['modificar'], FILTER_VALIDATE_INT) !== false && $_POST["modificar"] == 1) {
    // Validaciones
    if(isset($_POST["id"]) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && isset($_POST['email']) && 
    filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false && isset($_POST['login']) && isset($_POST['nombre']) && 
    isset($_POST['apellido'])){
        if ($usuario2->login_existe($_POST['login'], $_POST['id'])) {
            $error = 1;
        }  // Login duplicado
        if ($usuario2->email_existe($_POST['email'], $_POST['id'])) {
            $error = 2;
        }  // Email duplicado

        if ($error == 0) {
            if ($usuario2->actualizar($_POST['id'], $_POST['login'], $general->get_escape($_POST['nombre']), 
            $general->get_escape($_POST['apellido']), $general->get_escape($_POST['email']))) {
                $exito = 1;
            } else {
                $error = 3;
            }
        }
    }
}

$validator->create_message("msj_login", "login", " Obligatorio", 0);
$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);
$validator->create_message("msj_apellido", "apellido", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);