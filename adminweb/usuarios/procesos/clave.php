<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware
//
// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$usuario   = new Usuario();
$usuario2  = new Usuario();
$validator = new validator("form1");
$general = new General();

$exito = 0;
$error = 0;

if (isset($_POST['modificar']) && isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    // Validaciones
    if(isset($_POST["clave1"]) && isset($_POST["clave2"])){
        if ($_POST['clave1'] != $_POST['clave2']) {
            $error = 1;
        }
        if ($error == 0) {
            if ($usuario->actualizar_clave($_POST['id'], $_POST['clave1'])) {
                $exito = 1;
            } else {
                $error = 3;
            }
        }
    }
}

$validator->create_message("msj_clave1", "clave1", " Obligatorio", 0);
$validator->create_message("msj_clave2", "clave2", " Obligatorio", 0);