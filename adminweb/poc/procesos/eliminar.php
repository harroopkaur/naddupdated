<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_POC.php");

// Objetos
$poc = new POC();
$general = new General();
$exito = 0;

$id = 0;
if(isset($_POST["id"]) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
    $id = $_POST["id"];
}

$arrayArchivo = $poc->nombreArchivo($id);
if($poc->eliminar($id)){
    unlink($GLOBALS["app_root"] . "\../POC/archivosPOC/".$arrayArchivo["nombre"]);
    $exito = 1;
};
?>