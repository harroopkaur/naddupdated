<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminarSap.php">
    <input type="hidden" id="id" name="id">
</form>
<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til" id="archivo" name="archivo">Archivo</strong></th>
            <th  align="center" valign="middle" class="til" style="width:80px;"><strong>Descargar</strong></th>
            <th  align="center" valign="middle" class="til" style="width:80px;"><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                <?= $registro["nombre"] ?>
                </td>
                <td  align="center">
                    <a href="#"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_181_download_alt.png" width="24" height="21" border="0" alt="Editar" title="Descargar" onclick="descargar('<?= $registro["nombre"] ?>')"/></a>
                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
if($count == 0){ ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay archivos</div>
<?php } ?>

<script>
    function descargar(archivo){
        window.open("<?= $GLOBALS["domain_root1"] ?>/POC/archivosPOCSAP/" + archivo);
    }
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>