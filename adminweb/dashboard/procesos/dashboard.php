<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_dashboard.php");

// Objetos
$clientes = new Clientes();
$general = new General();
$dash = new clase_dashboard();

$urlDashboard = $GLOBALS["domain_root"] . "/dashboard/";

//procesos
$insertar = 0;
$exito = 0;


$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$urlPresentacion = $GLOBALS["domain_root"] . "/dashboard/" . $id_user;

$clientes->datos($id_user);

if ($id_user > 0 && !file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user)){
    mkdir($GLOBALS["app_root"] . "/dashboard/" . $id_user, 0755);
}

if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    $insertar = 1;
    $id_user = $_POST['id'];
    
    $urlPresentacion = $GLOBALS["domain_root"] . "/dashboard/" . $id_user;
    
    if(isset($_FILES['archivo']) && is_uploaded_file($_FILES['archivo']['tmp_name'])){
        $imgDashboard = $general->get_escape($_POST["dashboard"]);
        $nombArchivo = $_FILES['archivo']['name'];
        $temp = $_FILES['archivo']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = "dashboard".$fecha . "." . $ext;
       
        if ($clientes->actualizarDashboard($id_user, $nombre)) {
            move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $nombre); 

            if($imgDashboard != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $imgDashboard)){
                unlink($GLOBALS["app_root"] . "/dashboard/" . $imgDashboard);
            }

            $exito++;
        } 
    }
    
    if(isset($_FILES['archivo1']) && is_uploaded_file($_FILES['archivo1']['tmp_name'])){
        $imgPresentacion1 = $general->get_escape($_POST["presentacion1"]);
        $nombArchivo = $_FILES['archivo1']['name'];
        $temp = $_FILES['archivo1']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion1".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 1) > 0){
            if ($dash->actualizar($id_user, $nombre, 1)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 

                if($imgPresentacion1 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion1)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion1);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 1)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion1 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion1)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion1);
                }
                $exito++;
            }
        }
    }
    
    if(isset($_FILES['archivo2']) && is_uploaded_file($_FILES['archivo2']['tmp_name'])){
        $imgPresentacion2 = $general->get_escape($_POST["presentacion2"]);
        $nombArchivo = $_FILES['archivo2']['name'];
        $temp = $_FILES['archivo2']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion2".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 2) > 0){
            if ($dash->actualizar($id_user, $nombre, 2)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion2 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion2)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion2);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 2)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion2 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion2)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion2);
                }
                $exito++;
            }
        }
    }
    
    if(isset($_FILES['archivo3']) && is_uploaded_file($_FILES['archivo3']['tmp_name'])){
        $imgPresentacion3 = $general->get_escape($_POST["presentacion3"]);
        $nombArchivo = $_FILES['archivo3']['name'];
        $temp = $_FILES['archivo3']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion3".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 3) > 0){
            if ($dash->actualizar($id_user, $nombre, 3)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion3 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion3)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion3);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 3)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion3 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion3)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion3);
                }
                $exito++;
            }
        }
    }
    
    if(isset($_FILES['archivo4']) && is_uploaded_file($_FILES['archivo4']['tmp_name'])){
        $imgPresentacion4 = $general->get_escape($_POST["presentacion4"]);
        $nombArchivo = $_FILES['archivo4']['name'];
        $temp = $_FILES['archivo4']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion4".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 4) > 0){
            if ($dash->actualizar($id_user, $nombre, 4)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion4 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion4)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion4);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 4)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion4 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion4)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion4);
                }
                $exito++;
            }
        }
    }
    
    if(isset($_FILES['archivo5']) && is_uploaded_file($_FILES['archivo5']['tmp_name'])){
        $imgPresentacion5 = $general->get_escape($_POST["presentacion5"]);
        $nombArchivo = $_FILES['archivo5']['name'];
        $temp = $_FILES['archivo5']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion5".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 5) > 0){
            if ($dash->actualizar($id_user, $nombre, 5)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion5 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion5)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion5);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 5)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion5 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion5)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion5);
                }
                $exito++;
            }
        }
    }
    
    if(isset($_FILES['archivo6']) && is_uploaded_file($_FILES['archivo6']['tmp_name'])){
        $imgPresentacion6 = $general->get_escape($_POST["presentacion6"]);
        $nombArchivo = $_FILES['archivo6']['name'];
        $temp = $_FILES['archivo6']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion6".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 6) > 0){
            if ($dash->actualizar($id_user, $nombre, 6)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion6 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion6)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion6);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 6)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion6 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion6)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion6);
                }
                $exito++;
            }
        }
    }
    
    if(isset($_FILES['archivo7']) && is_uploaded_file($_FILES['archivo7']['tmp_name'])){
        $imgPresentacion7 = $general->get_escape($_POST["presentacion7"]);
        $nombArchivo = $_FILES['archivo7']['name'];
        $temp = $_FILES['archivo7']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion7".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 7) > 0){
            if ($dash->actualizar($id_user, $nombre, 7)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion7 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion7)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion7);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 7)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion7 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion7)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion7);
                }
                $exito++;
            }
        }
    }
    
    if(isset($_FILES['archivo8']) && is_uploaded_file($_FILES['archivo8']['tmp_name'])){
        $imgPresentacion8 = $general->get_escape($_POST["presentacion8"]);
        $nombArchivo = $_FILES['archivo8']['name'];
        $temp = $_FILES['archivo8']['tmp_name'];
        $fecha = date("dmYHis");
            
        $info = new SplFileInfo($nombArchivo);
        $ext = $info->getExtension();
        $nombre = $nombArchivo; //"presentacion8".$fecha . "." . $ext;
       
        if ($dash->existePresentacion($id_user, 8) > 0){
            if ($dash->actualizar($id_user, $nombre, 8)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion8 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion8)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion8);
                }
                $exito++;
            } 
        } else{
            if ($dash->insertar($id_user, $nombre, 8)) {
                move_uploaded_file($temp, $GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $nombre); 
                
                if($imgPresentacion8 != "" && file_exists ($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion8)){
                    unlink($GLOBALS["app_root"] . "/dashboard/" . $id_user . "/" . $imgPresentacion8);
                }
                $exito++;
            }
        }
    }
}

$dirDashboard = $clientes->dashboard($id_user);
$dirPres = $dash->presentaciones($id_user);

$dirPresentacion1 = "";
$dirPresentacion2 = "";
$dirPresentacion3 = "";
$dirPresentacion4 = "";
$dirPresentacion5 = "";
$dirPresentacion6 = "";
$dirPresentacion7 = "";
$dirPresentacion8 = "";

foreach($dirPres as $row){
    if($row["numero"] == 1){
        $dirPresentacion1 = $row["presentacion"];
    }
    
    if($row["numero"] == 2){
        $dirPresentacion2 = $row["presentacion"];
    }
    
    if($row["numero"] == 3){
        $dirPresentacion3 = $row["presentacion"];
    }
    
    if($row["numero"] == 4){
        $dirPresentacion4 = $row["presentacion"];
    }
    
    if($row["numero"] == 5){
        $dirPresentacion5 = $row["presentacion"];
    }
    
    if($row["numero"] == 6){
        $dirPresentacion6 = $row["presentacion"];
    }
    
    if($row["numero"] == 7){
        $dirPresentacion7 = $row["presentacion"];
    }
    
    if($row["numero"] == 8){
        $dirPresentacion8 = $row["presentacion"];
    }
}