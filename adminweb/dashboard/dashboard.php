<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/adminweb/dashboard/procesos/dashboard.php");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/head.php");
?>

<section class="contenedor">
    <div class="contenedorMenuAdmin">
        <?php include_once($GLOBALS["app_root"] . "/adminweb/plantillas/titulo.php"); ?>

        <br><br>

        <?php
        $opcionm1 = 14;
        include_once($GLOBALS["app_root"] . "/adminweb/plantillas/menu1.php");
        ?>
    </div>

    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno">
                <?php
                    $menuDashboard = 1;
                    include_once($GLOBALS["app_root"] . "/adminweb/dashboard/plantillas/menu.php");
                ?>
            </div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root"] . "/adminweb/dashboard/plantillas/dashboard.php") ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/pie.php");
?>