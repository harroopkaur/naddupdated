<?php 
if($insertar == 1){
?>
    <script type="text/javascript">
        $.alert.open('info', 'Info', '<?= $exito ?> archivos cargados', {'Aceptar': 'Aceptar'}, function(button) {
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/dashboard';
        });
    </script>
<?php 
}
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Modificar Datos del Dashboard</span></legend>
    
    <form id="form1" name="form1" method="post" enctype="multipart/form-data" action="dashboard.php">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id_user ?>">
        <input type="hidden" id="dashboard" name="dashboard" value="<?= $dirDashboard ?>">
        <input type="hidden" id="presentacion1" name="presentacion1" value="<?= $dirPresentacion1 ?>">
        <input type="hidden" id="presentacion2" name="presentacion2" value="<?= $dirPresentacion2 ?>">
        <input type="hidden" id="presentacion3" name="presentacion3" value="<?= $dirPresentacion3 ?>">
        <input type="hidden" id="presentacion4" name="presentacion4" value="<?= $dirPresentacion4 ?>">
        <input type="hidden" id="presentacion5" name="presentacion5" value="<?= $dirPresentacion5 ?>">
        <input type="hidden" id="presentacion6" name="presentacion6" value="<?= $dirPresentacion6 ?>">
        <input type="hidden" id="presentacion7" name="presentacion7" value="<?= $dirPresentacion7 ?>">
        <input type="hidden" id="presentacion8" name="presentacion8" value="<?= $dirPresentacion8 ?>">
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="120" align="left" valign="top">Empresa:</th>
                <td align="left"><input name="empresa" id="empresa" type="text" value="<?= $clientes->empresa ?>" size="30" maxlength="70"  readonly/>
                </td>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Dashboard:</th>
                <td width="300" align="left"><input name="archivo" id="archivo" type="file" accept=".jpg, .png"/>
                </td>
                <td align="left"><img id="imgDashboard" src="<?php if($dirDashboard != ""){ echo $urlDashboard . $dirDashboard; } ?>"  style='width:30px; height:auto;'>
                </td>
                <?php if($dirDashboard != ""){ ?>
                    <td><a id="elim0" href="#" onclick="eliminarDashboard('<?= $dirDashboard ?>', 'elim0')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 1:</th>
                <td width="300" align="left"><input name="archivo1" id="archivo1" type="file" accept=".pdf"/></td>
                <td id="presen1" align="left"><?= $dirPresentacion1 ?><!--<img id="imgPresentacion1" src="<?php //if($dirPresentacion1 != ""){ echo $urlDashboard . $dirPresentacion1; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion1 != ""){ ?>
                    <td><a id="elim1" href="#" onclick="eliminar('<?= $dirPresentacion1 ?>', 'presen1', 'elim1')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 2:</th>
                <td width="300" align="left"><input name="archivo2" id="archivo2" type="file" accept=".pdf"/></td>
                <td id="presen2" align="left"><?= $dirPresentacion2 ?><!--<img id="imgPresentacion2" src="<?php //if($dirPresentacion2 != ""){ echo $urlDashboard . $dirPresentacion2; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion2 != ""){ ?>
                    <td><a id="elim2" href="#" onclick="eliminar('<?= $dirPresentacion2 ?>', 'presen2', 'elim2')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 3:</th>
                <td width="300" align="left"><input name="archivo3" id="archivo3" type="file" accept=".pdf"/></td>
                <td id="presen3" align="left"><?= $dirPresentacion3 ?><!--<img id="imgPresentacion3" src="<?php //if($dirPresentacion3 != ""){ echo $urlDashboard . $dirPresentacion3; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion3 != ""){ ?>
                    <td><a id="elim3" href="#" onclick="eliminar('<?= $dirPresentacion3 ?>', 'presen3', 'elim3')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 4:</th>
                <td width="300" align="left"><input name="archivo4" id="archivo4" type="file" accept=".pdf"/></td>
                <td id="presen4" align="left"><?= $dirPresentacion4 ?><!--<img id="imgPresentacion4" src="<?php //if($dirPresentacion4 != ""){ echo $urlDashboard . $dirPresentacion4; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion4 != ""){ ?>
                    <td><a id="elim4" href="#" onclick="eliminar('<?= $dirPresentacion4 ?>', 'presen4', 'elim4')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 5:</th>
                <td width="300" align="left"><input name="archivo5" id="archivo5" type="file" accept=".pdf"/></td>
                <td id="presen5" align="left"><?= $dirPresentacion5 ?><!--<img id="imgPresentacion5" src="<?php //if($dirPresentacion5 != ""){ echo $urlDashboard . $dirPresentacion5; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion5 != ""){ ?>
                    <td><a id="elim5" href="#" onclick="eliminar('<?= $dirPresentacion5 ?>', 'presen5', 'elim5')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 6:</th>
                <td width="300" align="left"><input name="archivo6" id="archivo6" type="file" accept=".pdf"/></td>
                <td id="presen6" align="left"><?= $dirPresentacion6 ?><!--<img id="imgPresentacion6" src="<?php //if($dirPresentacion6 != ""){ echo $urlDashboard . $dirPresentacion6; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion6 != ""){ ?>
                    <td><a id="elim6" href="#" onclick="eliminar('<?= $dirPresentacion6 ?>', 'presen6', 'elim6')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 7:</th>
                <td width="300" align="left"><input name="archivo7" id="archivo7" type="file" accept=".pdf"/></td>
                <td id="presen7" align="left"><?= $dirPresentacion7 ?><!--<img id="imgPresentacion7" src="<?php //if($dirPresentacion7 != ""){ echo $urlDashboard . $dirPresentacion7; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion7 != ""){ ?>
                    <td><a id="elim7" href="#" onclick="eliminar('<?= $dirPresentacion7 ?>', 'presen7', 'elim7')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>
            
            <tr>
                <th width="120" align="left" valign="top">Archivo 8:</th>
                <td width="300" align="left"><input name="archivo8" id="archivo8" type="file" accept=".pdf"/></td>
                <td id="presen8" width="150" align="left"><?= $dirPresentacion8 ?><!--<img id="imgPresentacion8" src="<?php //if($dirPresentacion8 != ""){ echo $urlDashboard . $dirPresentacion8; } ?>"  style='width:30px; height:auto;'>-->
                </td>
                <?php if($dirPresentacion8 != ""){ ?>
                    <td><a id="elim8" href="#" onclick="eliminar('<?= $dirPresentacion8 ?>', 'presen8', 'elim8')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
                <?php } ?>
            </tr>

            <tr>
                <td colspan="4" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="enviar();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    var img = "";
    $(document).ready(function(){
        $("#archivo").change(function(e){
            img = "#imgDashboard";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo").val("");
                $("#archivo").replaceWith($("#archivo").clone(true));
                $("#imgDashboard").attr("src", "");
            }
        });
        
        $("#archivo1").change(function(e){
            img = "#imgPresentacion1";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo1").val("");
                $("#archivo1").replaceWith($("#archivo1").clone(true));
                //$("#imgPresentacion1").attr("src", "");
            }
        });
        
        $("#archivo2").change(function(e){
            img = "#imgPresentacion2";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo2").val("");
                $("#archivo2").replaceWith($("#archivo2").clone(true));
                //$("#imgPresentacion2").attr("src", "");
            }
        });
        
        $("#archivo3").change(function(e){
            img = "#imgPresentacion3";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo3").val("");
                $("#archivo3").replaceWith($("#archivo3").clone(true));
                //$("#imgPresentacion3").attr("src", "");
            }
        });
        
        $("#archivo4").change(function(e){
            img = "#imgPresentacion4";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo4").val("");
                $("#archivo4").replaceWith($("#archivo4").clone(true));
                //$("#imgPresentacion4").attr("src", "");
            }
        });
        
        $("#archivo5").change(function(e){
            img = "#imgPresentacion5";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo5").val("");
                $("#archivo5").replaceWith($("#archivo5").clone(true));
                //$("#imgPresentacion5").attr("src", "");
            }
        });
        
        $("#archivo6").change(function(e){
            img = "#imgPresentacion6";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo6").val("");
                $("#archivo6").replaceWith($("#archivo6").clone(true));
                //$("#imgPresentacion6").attr("src", "");
            }
        });
        
        $("#archivo7").change(function(e){
            img = "#imgPresentacion7";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo7").val("");
                $("#archivo7").replaceWith($("#archivo7").clone(true));
                //$("#imgPresentacion7").attr("src", "");
            }
        });
        
        $("#archivo8").change(function(e){
            img = "#imgPresentacion8";
            if(addArchivo(e) === true){
                //$("#imgDashboard").attr("src", $("#archivo").val());
            } else{
                $("#archivo8").val("");
                $("#archivo8").replaceWith($("#archivo8").clone(true));
                //$("#imgPresentacion8").attr("src", "");
            }
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        if (file === undefined){
            return false;
        }
        
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
        if (img === "#imgDashboard"){
            $(img).attr("src", result);
        }
    }
    
    function enviar(){
        $("#form1").submit();
    }
    
    function eliminarDashboard(archivo, img){
        $.alert.open('confirm', 'Desea anular el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#fondo").show();
                $.post("ajax/eliminarDashboard.php", { cliente : $("#id").val(), archivo : archivo, token : localStorage.licensingassuranceToken }, function(data){
                    $("#fondo").hide();
                    if(data[0].result === true){
                        $.alert.open('info', 'Info', "Archivo eliminado con éxito", {'Aceptar' : 'Aceptar'}, function(){
                            $("#imgDashboard").attr("src", "");
                            $("#" + img).hide();
                        });
                        
                    } else{
                        $.alert.open('warning', "Alerta", "No se eliminó el archivo", {'Aceptar' : 'Aceptar'});
                    }
                   
                }, "json")
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'});
                });
            }
        });
    }
    
    function eliminar(archivo, variable, img){
        $.alert.open('confirm', 'Desea anular el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#fondo").show();
                $.post("ajax/eliminarPresentacion.php", { cliente : $("#id").val(), archivo : archivo, token : localStorage.licensingassuranceToken }, function(data){
                    $("#fondo").hide();
                    if(data[0].result === true){
                        $.alert.open('info', "Info", "Archivo eliminado con éxito", {'Aceptar' : 'Aceptar'}, function(){
                            $("#" + variable).empty("");
                            $("#" + img).hide();
                        });
                    } else{
                        $.alert.open('warning', "Alerta", "No se eliminó el archivo", {'Aceptar' : 'Aceptar'});
                    }
                   
                }, "json")
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'});
                });
            }
        });
    }
</script>