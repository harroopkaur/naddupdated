<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/clases/middleware.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
$general = new General();

include_once($GLOBALS["app_root"] . "/adminweb/plantillas/head.php");
?>
<section class="contenedor">
    <div class="contenedorMenuAdmin">
        <?php include_once($GLOBALS["app_root"] . "/adminweb/plantillas/titulo.php"); ?>

        <br><br>

        <?php
        $opcionm1 = 1;
        require_once($GLOBALS["app_root"] . "/adminweb/plantillas/menu1.php");
        ?>
    </div>

    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno"></div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root"] . "/adminweb/plantillas/inicio.php"); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/pie.php");
?>