<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");

$equivalencia = new Equivalencias();
$general     = new General();

if(!isset($_SESSION['usuario_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['usuario_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    require_once dirname(__FILE__) . '/../../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"
    
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Empleados");


    for($i = 1; $i < 9; $i++){
        $listado = $equivalencia->listar_todo($i);
        
        if($i == 1){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Adobe');
            // Add some data
        } 
        
        if($i == 2){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'IBM');
            // Add some data
        } 
        
        if($i == 3){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Microsoft');
            // Add some data
        } 
        
        if($i == 4){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Oracle');
            // Add some data
        } 
        
        if($i == 5){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SAP');
            // Add some data
        } 
        
        if($i == 6){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'VMWare');
            // Add some data
        } 
        
        if($i == 7){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'UNIX-IBM');
            // Add some data
        } 
        
        if($i == 8){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'UNIX-Oracle');
            // Add some data
        } 
        
        /*if($i == 9){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Adobe');
            // Add some data
        } */
        
        /*if($i == 10){
            // Add some data
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SPLA');
            // Add some data
        }*/ 
        
        $myWorkSheet->setCellValue('A1', 'Producto')
                    ->setCellValue('B1', 'Edición')
                    ->setCellValue('C1', 'Versión')
                    ->setCellValue('D1', 'SKU')
                    ->setCellValue('E1', 'Tipo')
                    ->setCellValue('F1', 'Cantidades')
                    ->setCellValue('G1', 'Precio Unitario')
                    ->setCellValue('H1', 'Asignación');
        
        $j = 2;
        foreach($listado as $row){
            $myWorkSheet->setCellValue('A' . $j, $row["familia"])
                        ->setCellValue('B' . $j, $row["edicion"])
                        ->setCellValueExplicit('C' . $j, $row["version"], PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValue('D' . $j, '')
                        ->setCellValue('E' . $j, '')
                        ->setCellValue('F' . $j, '')
                        ->setCellValue('G' . $j, '')
                        ->setCellValue('H' . $j, '');
            $j++;
        }
        
        $objPHPExcel->addSheet($myWorkSheet, $i - 1);
        $objPHPExcel->setActiveSheetIndex($i - 1);
        
        $objConditional2 = new PHPExcel_Style_Conditional();
        $objConditional2->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS)
                        ->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_LESSTHAN)
                        ->addCondition('0');
        $objConditional2->getStyle()->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
        $conditionalStyles = $objPHPExcel->getActiveSheet()->getStyle('B2')->getConditionalStyles();
        array_push($conditionalStyles, $objConditional2);

        $j = 2;
        foreach($listado as $row){
            if($i == 3){
                $objValidation = $objPHPExcel->getActiveSheet()->getCell('E' . $j)->getDataValidation();
                $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
                $objValidation->setAllowBlank(false);
                $objValidation->setShowInputMessage(true);
                $objValidation->setShowErrorMessage(true);
                $objValidation->setShowDropDown(true);
                $objValidation->setErrorTitle('Input error');
                $objValidation->setError('El valor no esta en la lista.');
                $objValidation->setFormula1('"Perpetuo,Subscripción,Software Assurance,Otro"');            
            } else{
                $objValidation = $objPHPExcel->getActiveSheet()->getCell('E' . $j)->getDataValidation();
                $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
                $objValidation->setAllowBlank(false);
                $objValidation->setShowInputMessage(true);
                $objValidation->setShowErrorMessage(true);
                $objValidation->setShowDropDown(true);
                $objValidation->setErrorTitle('Input error');
                $objValidation->setError('El valor no esta en la lista.');
                $objValidation->setFormula1('"Perpetuo,Subscripción,Otro"');       
            }
            $j++;
        }
    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(2);
    // Save Excel 2007 file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save(dirname(__FILE__) . '/../../formulario_compras_clientes.xlsx');
    echo 'Archivo Creado con éxito';
    exit;
}
else{
    $general->eliminarSesion();
}