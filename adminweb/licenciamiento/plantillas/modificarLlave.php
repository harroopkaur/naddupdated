<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/llaves.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar ningún acceso', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Accesos agregados parcialmente', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/';
            }
        });
    </script>
    <?php
}
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de la Llave del Producto</span></legend>

            <input type="hidden" name="insertar" id="insertar" value="1" />
            <input type="hidden" name="id" id="id" value="<?= $id_user ?>" />
            <input type="hidden" name="idLlave" id="idLlave" value="<?= $idLlave ?>" />
            <input type="hidden" name="modif" id="modif" value="<?= $autorizado ?>">
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $llaves->error;
            } ?></font>
            </div>
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
                <tr>
                    <th width="150" align="left" valign="top">Inicio Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaIni" id="fechaIni" type="text" value="<?= $general->reordenarFecha($row["fechaIni"], "-", "/") ?>" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaIni") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Fin Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="<?= $general->reordenarFecha($row["fechaFin"], "-", "/") ?>" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Serial:</th>
                    <td width="100" align="left"><input name="serial" id="serial" type="text" value="<?= $row["serial"] ?>" size="30" maxlength="20"  readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_serial") ?></font></div></td>
                    <td align="left"></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Cantidad Usuarios:</th>
                    <td colspan="2" align="left"><input name="cantidad" id="cantidad" type="text" size="30" maxlength="5"  value="<?= $row["cantidad"] ?>"/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_cantidad") ?></font></div></td>
                </tr>
                
                <tr>
                    <th width="80" align="left" valign="top">Autorizado:</th>
                    <td align="left">
                        <input type="checkbox" id="autorizado" name="autorizado" <?php if($row["status"] == 1 || $row["status"] == 2){ echo 'checked="checked" disabled="disabled"'; } ?>>
                    </td>
                </tr>
            </table>
    </fieldset>

    <div style="width:98%; display:table; margin:0 auto; overflow:hidden;">
        <fieldset class="fieldset float-lt" style="width:28%; margin-left:5px; height:182px;"> 
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Windows</span></legend>    

            <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2">
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="5" <?php if($adobe == 1){ echo "checked='checked'"; } ?>>Adobe</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="6" <?php if($ibm == 1){ echo "checked='checked'"; } ?>>IBM</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="7" <?php if($microsoft == 1){ echo "checked='checked'"; } ?>>Microsoft</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="8" <?php if($oracle == 1){ echo "checked='checked'"; } ?>>Oracle</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="14" <?php if($spla == 1){ echo "checked='checked'"; } ?>>SPLA</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="15" <?php if($usabilidad == 1){ echo "checked='checked'"; } ?>>Usabilidad</td>
                </tr>
            </table>
        </fieldset>

        <div class="float-lt" style="width:32%; margin-left:15px;">
            <fieldset class="fieldset" style="height:70px;"> 
                <legend class="text-left" style="margin-left:15px;"><span class="bold">UNIX/Linux</span></legend>    

                <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" style="margin-top:-15px;">
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="31" <?php if($nmap == 1){ echo "checked='checked'"; } ?>>NMAP</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="11" <?php if($unixIbm == 1){ echo "checked='checked'"; } ?>>IBM</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="12" <?php if($unixOracle == 1){ echo "checked='checked'"; } ?>>Oracle</td>
                    </tr>
                </table>
            </fieldset>
            
            <fieldset class="fieldset" style="height:70px;"> 
                <legend class="text-left" style="margin-left:15px;"><span class="bold">Otros</span></legend>    

                <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="9" <?php if($sap == 1){ echo "checked='checked'"; } ?>>SAP</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="10" <?php if($VMWare == 1){ echo "checked='checked'"; } ?>>VMWare</td>
                    </tr>
                </table>
            </fieldset>
        </div>

        <fieldset class="fieldset float-lt" style="width:28%; margin-left:15px; height: 182px;"> 
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Administrativo</span></legend>    

            <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2">
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="16" <?php if($gantt == 1){ echo "checked='checked'"; } ?>>GANTT</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="1" <?php if($sam == 1){ echo "checked='checked'"; } ?>>SAM</td>
                </tr>
            </table>
        </fieldset
    
        <br style="clear:both;">
        <div style="width:77px; margin:0 auto;">
            <input style="margin:0 auto; margin-top:10px;" name="insertar" type="button" id="insertar" value="MODIFICAR" onclick="validate();" class="boton" />
        </div>
    </div>
</form>  
    
<script>
    $(document).ready(function(){
        $("#cantidad").numeric(false);
        $("#fechaIni").datepicker();
        $("#fechaFin").datepicker();
        
        $("#cantidad").click(function(){
           $("#cantidad").val(""); 
        });
        
        $("#autorizado").click(function(){
            if($("#autorizado").prop("disabled")){
                return false;
            }
            
            if($("#autorizado").prop("checked")){
                $("#modif").val(1);
            } else{
                $("#modif").val(0);
            }
        });
    });
</script>