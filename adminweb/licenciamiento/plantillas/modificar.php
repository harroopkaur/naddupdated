<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo modificar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/';
            }
        });
    </script>
    <?php
}
?>                      

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Modificar Datos de la Empresa</span></legend>
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id_user ?>">
        <input type="hidden" name="modif" id="modif" value="<?= $autorizado ?>">
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
             echo $empresa->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="150" align="left" valign="top">Empresa:</th>
                <td align="left"><input name="empresa" id="empresa" type="text" size="30" maxlength="70" value="<?= $row["nombreEmpresa"] ?>" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="150" align="left" valign="top">Usuario:</th>
                <td align="left"><input name="usuario" id="usuario" type="text" size="30" maxlength="50" value="<?= $row["usuario"] ?>" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_usuario") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="150" align="left" valign="top">E-mail:</th>
                <td align="left"><input name="email" id="email" type="text" size="30" maxlength="50" value="<?= $row["email"] ?>" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="150" align="left" valign="top">Cargo:</th>
                <td align="left"><input name="cargo" id="cargo" type="text" size="30" maxlength="50"  value="<?= $row["cargo"] ?>"/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_cargo") ?></font></div></td>
            </tr>
                    
            <tr>
                <th width="150" align="left" valign="top">Pais:</th>
                <td align="left">
                    <select name="pais" id="pais">
                        <option value="" selected>--Seleccione--</option>
                        <?php
                        foreach($lista_p as $reg_p){
                        ?>
                            <option value="<?= $reg_p["id"] ?>" <?php if($reg_p["id"] == $row["pais"]){ echo "selected='selected';"; } ?>><?= $reg_p["nombre"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pais") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">Autorizado:</th>
                <td align="left">
                    <input type="checkbox" id="autorizado" name="autorizado" <?php if($row["status"] == 1){ echo 'checked="checked" disabled="disabled"'; } ?>>
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
       $("#empresa").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/ajax/verificarEmpresa.php", { id : <?= $id_user ?>, empresa : $("#empresa").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe la empresa', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#empresa").val("");
                            $("#empresa").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });

        $("#email").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/ajax/verificarCorreo.php", { id : <?= $id_user ?>, email : $("#email").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe el correo', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#autorizado").click(function(){
            if($("#autorizado").prop("disabled")){
                return false;
            }
            
            if($("#autorizado").prop("checked")){
                $("#modif").val(1);
            } else{
                $("#modif").val(0);
            }
        });
    });
</script>    