<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/llaves.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar ningún acceso', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Accesos agregados parcialmente', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/licenciamiento/';
            }
        });
    </script>
    <?php
}
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de la Llave del Producto</span></legend>

            <input type="hidden" name="insertar" id="insertar" value="1" />
            <input type="hidden" name="id" id="id" value="<?= $id_user ?>" />
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $llaves->error;
            } ?></font>
            </div>
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
                <tr>
                    <th width="150" align="left" valign="top">Inicio Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaIni" id="fechaIni" type="text" value="" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaIni") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Fin Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Serial:</th>
                    <td width="100" align="left"><input name="serial" id="serial" type="text" value="" size="30" maxlength="20"  readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_serial") ?></font></div></td>
                    <td align="left"><input name="generar" type="button" id="generar" value="Generar" class="boton" /></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Cantidad Usuarios:</th>
                    <td colspan="2" align="left"><input name="cantidad" id="cantidad" type="text" size="30" maxlength="5"  value="0"/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_cantidad") ?></font></div></td>
                </tr>
            </table>
    </fieldset>

    <div style="width:98%; display:table; margin:0 auto; overflow:hidden;">
        <fieldset class="fieldset float-lt" style="width:28%; margin-left:5px; height:182px;"> 
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Windows</span></legend>    

            <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2">
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="5">Adobe</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="6">IBM</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="7">Microsoft</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="8">Oracle</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="14">SPLA</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="15">Usabilidad</td>
                </tr>
            </table>
        </fieldset>

        <div class="float-lt" style="width:32%; margin-left:15px;">
            <fieldset class="fieldset" style="height:70px;"> 
                <legend class="text-left" style="margin-left:15px;"><span class="bold">UNIX/Linux</span></legend>    

                <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="11">IBM</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="12">Oracle</td>
                    </tr>
                </table>
            </fieldset>
            
            <fieldset class="fieldset" style="height:70px;"> 
                <legend class="text-left" style="margin-left:15px;"><span class="bold">Otros</span></legend>    

                <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="9">SAP</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="10">VMWare</td>
                    </tr>
                </table>
            </fieldset>
        </div>

        <fieldset class="fieldset float-lt" style="width:28%; margin-left:15px; height: 182px;"> 
            <legend class="text-left" style="margin-left:15px;"><span class="bold">Administrativo</span></legend>    

            <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2">
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="16">GANTT</td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="acceso<?= $z; ?>" name="acceso[]" value="1">SAM</td>
                </tr>
            </table>
        </fieldset
    
        <br style="clear:both;">
        <div style="width:77px; margin:0 auto;">
            <input style="margin:0 auto; margin-top:10px;" name="insertar" type="button" id="insertar" value="CREAR" onclick="validate();" class="boton" />
        </div>
    </div>
</form>  
    
<script>
    $(document).ready(function(){
        $("#cantidad").numeric(false);
        $("#fechaIni").datepicker();
        $("#fechaFin").datepicker();
        
        $("#cantidad").click(function(){
           $("#cantidad").val(""); 
        });
        
        $("#generar").click(function(){
           $.post("ajax/generarSerial.php", { id : $("#id").val(), token : localStorage.licensingassuranceToken }, function(data){
                if(data[0].resultado === true){
                    $("#serial").val(data[0].llaveGenerada);
                }
                else{
                    $.alert.open('alert', "No se pudo generar el serial por favor intente de nuevo", {'Aceptar' : 'Aceptar'});
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            }); 
        });
    });
</script>