<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general_licenciamiento.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empresa_licenc.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $empresas = new clase_empresa_licenciamiento();

            $emp = "";
            if(isset($_POST["empresa"])){
                $emp = $general->get_escape($_POST["empresa"]);
            }

            $email = "";
            if(isset($_POST["email"])){
                $email = $general->get_escape($_POST["email"]);
            }

            $fecha = "";
            $fec = "";
            if(isset($_POST["fecha"]) && $_POST["fecha"] != ""){
                $fecha = $general->get_escape($_POST["fecha"]);
                $fec = $general->reordenarFecha($fecha, "/", "-");
            }

            $estado = "-1";
            if(isset($_POST["estado"])){
                $estado = $general->get_escape($_POST["estado"]);
            }

            $start_record = 0;
            if(isset($_POST["pagina"]) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false){
                $start_record = $_POST["pagina"];
            }

            $listado = $empresas->listar_todo_paginado($emp, $email, $fec, $estado, $start_record);
            $count   = $empresas->total($emp, $email, $fec, $estado);
            $condiciones  =  '&emp=' . $emp . '&ema=' . $email . '&fec=' . $fecha . '&est=' . $estado;

            $pag = new paginator($count, $general->limit_paginacion, 'index.php?', $condiciones);
            $i = $pag->get_total_pages();

            $tabla = "";
            foreach ($listado as $registro) {
                $tabla .= '<tr onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                    <td align="left">' . $registro["nombreEmpresa"] . '</td>
                    <td align="left">' . $registro["email"] . '</td>
                    <td align="center">' . $general->muestrafecha($registro['fechaCreacion']) . '</td>
                    <td align="center">';
                        if($registro['status'] == 1){ $tabla .= 'Autorizado'; }else{ $tabla .= 'No autorizado'; } 
                    $tabla .= '</td>
                    <td  align="center">
                        <a href="modificar.php?id=' . $registro["id"] . '"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                    </td>
                    <td  align="center">
                        <a href="#" onclick="eliminar(' . $registro["id"] . ')">
                        <img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                    </td>
                </tr>';
            }

            $paginador = $pag->print_paginatorAjax("");
            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$paginador, 'resultado'=>true));
        }  
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);