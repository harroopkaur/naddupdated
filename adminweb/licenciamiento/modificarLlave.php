<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/adminweb/licenciamiento/procesos/modificarLlave.php");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/head.php");
?>

<section class="contenedor">
    <div class="contenedorMenuAdmin">
        <?php include_once($GLOBALS["app_root"] . "/adminweb/plantillas/titulo.php"); ?>

        <br><br>

        <?php
        $opcionm1 = 10;
        include_once($GLOBALS["app_root"] . "/adminweb/plantillas/menu1.php");
        ?>
    </div>

    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno">
                <?php 
                $menuLicenciamiento = 5;
                include_once($GLOBALS["app_root"] . "/adminweb/licenciamiento/plantillas/menu.php");
                ?>
            </div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root"] . "/adminweb/licenciamiento/plantillas/modificarLlave.php"); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once("../plantillas/pie.php");
?>