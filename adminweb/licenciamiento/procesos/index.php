<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general_licenciamiento.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empresa_licenc.php");

// Objetos
$empresas = new clase_empresa_licenciamiento();
$general  = new General();

$emp = "";
if(isset($_GET["emp"])){
    $emp = $general->get_escape($_GET["emp"]);
}

$email = "";
if(isset($_GET["ema"])){
    $email = $general->get_escape($_GET["ema"]);
}

$fecha = "";
$fec = "";
if(isset($_GET["fec"])){
    $fecha = $general->get_escape($_GET["fec"]);
    $fec = $general->reordenarFecha($fecha, "/", "-");
}

$estado = "-1";
if(isset($_GET["est"])){
    $estado = $general->get_escape($_GET["est"]);
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '&emp=' . $emp . '&ema=' . $email . '&fec=' . $fecha . '&est=' . $estado; //filtro adicional si se le coloca input para filtrar por columnas
} else {
    $start_record = 0;
    $parametros   = '';
}

$listado = $empresas->listar_todo_paginado($emp, $email, $fec, $estado, $start_record);
$count   = $empresas->total($emp, $email, $fec, $estado);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i   = $pag->get_total_pages();