<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general_licenciamiento.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empresa_llaves.php");
require_once($GLOBALS["app_root"] . "/clases/clase_llave_accesos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$llaves = new clase_empresa_llaves();
$llaveAccesos = new clase_llave_accesos();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$id_user = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id_user = $_GET["id"];
}

if (isset($_POST['insertar']) && isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false && 
filter_var($_POST['cantidad'], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        if ($llaves->insertar($_POST["id"], $general->reordenarFecha($_POST['fechaIni'], "/", "-"), $general->reordenarFecha($_POST['fechaFin'], "/", "-"), 
        $general->get_escape($_POST['serial']), $_POST['cantidad'])) {
            $id_serial = $llaves->ultimo_id();
            
            $acceso = array();
            if (isset($_POST['acceso'])){
                $acceso = $_POST["acceso"];
            }
            
            $j = 0;
            for ($index = 0; $index < count($acceso); $index++){
                $idAcceso = $acceso[$index];
                
                if ($llaveAccesos->insertar($id_serial, $idAcceso)){
                    $j++;
                }
            }
            
            if ($j == $index){
                $exito = 1;
            } else if ($j == 0){
                $exito = 0;
            } else{
                $exito = 2;
            }
            
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_fechaIni", "fechaIni", " Obligatorio", 0);
$validator->create_message("msj_fechaFin", "fechaFin", " Obligatorio", 0);
$validator->create_message("msj_serial", "serial", " Obligatorio", 0);
$validator->create_message("msj_cantidad", "cantidad", " Obligatorio", 0);