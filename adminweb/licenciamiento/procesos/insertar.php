<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general_licenciamiento.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empresa_licenc.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pais_licenciamiento.php");

// Objetos
$empresa = new clase_empresa_licenciamiento();
$validator = new validator("form1");
$paises = new pais_licenc();
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$lista_p = $paises->listar_todo();

if (isset($_POST['insertar']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false && filter_var($_POST['pais'], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        if ($empresa->insertar($general->get_escape($_POST['empresa']), $general->get_escape($_POST['usuario']), $general->get_escape($_POST['email']), 
        $general->get_escape($_POST['cargo']), $_POST['pais'])) {
            $id_user = $empresa->ultimo_id();
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_usuario", "usuario", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);
$validator->create_message("msj_cargo", "cargo", " Obligatorio", 0);
$validator->create_message("msj_pais", "pais", " Obligatorio", 0);