<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general_licenciamiento.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empresa_llaves.php");
require_once($GLOBALS["app_root"] . "/clases/clase_llave_accesos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$llaves = new clase_empresa_llaves();
$llaveAccesos = new clase_llave_accesos();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$id_user = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id_user = $_GET["id"];
}

$idLlave = 0;
if (isset($_GET["idLlave"]) && is_numeric($_GET["idLlave"])){
    $idLlave = $_GET["idLlave"];
}

if (isset($_POST['insertar']) && isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false && 
filter_var($_POST['cantidad'], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        $autorizado = 0;
        if(isset($_POST["modif"]) && $_POST["modif"] == 1){
            $autorizado = 1;
        }
        
        if ($llaves->actualizar($_POST["idLlave"], $general->reordenarFecha($_POST['fechaIni'], "/", "-"), $general->reordenarFecha($_POST['fechaFin'], "/", "-"), 
        $_POST['cantidad'], $autorizado)) {
            $idLlave = $_POST["idLlave"];
            
            $acceso = array();
            if (isset($_POST['acceso'])){
                $acceso = $_POST["acceso"];
            }
            
            $llaveAccesos->eliminarTodo($_POST["idLlave"]);
            
            $j = 0;
            for ($index = 0; $index < count($acceso); $index++){
                $idAcceso = $acceso[$index];
                
                if ($llaveAccesos->serial_existe($idLlave, $idAcceso) == false){
                    if ($llaveAccesos->insertar($idLlave, $idAcceso)){
                        $j++;
                    }
                } else{
                    if ($llaveAccesos->actualizar($idLlave, $idAcceso)){
                        $j++;
                    }
                }
            }
            
            if ($j == $index){
                $exito = 1;
            } else if ($j == 0){
                $exito = 0;
            } else{
                $exito = 2;
            }
            
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_fechaIni", "fechaIni", " Obligatorio", 0);
$validator->create_message("msj_fechaFin", "fechaFin", " Obligatorio", 0);
$validator->create_message("msj_serial", "serial", " Obligatorio", 0);
$validator->create_message("msj_cantidad", "cantidad", " Obligatorio", 0);

$row = $llaves->datos($idLlave);
$autorizado = $row["status"];
$listado = $llaveAccesos->listado($idLlave);

$adobe = 0;
$ibm = 0;
$microsoft = 0;
$spla = 0;
$oracle = 0;
$usabilidad = 0;

$unixIbm = 0;
$unixOracle = 0;

$sap = 0;
$VMWare = 0;

$gantt = 0;
$sam = 0;


foreach($listado as $row1){
    if($row1["accesos"] == 5){
        $adobe = 1;
    }
    
    if($row1["accesos"] == 6){
        $ibm = 1;
    }
    
    if($row1["accesos"] == 7){
        $microsoft = 1;
    }
    
    if($row1["accesos"] == 14){
        $spla = 1;
    }
    
    if($row1["accesos"] == 8){
        $oracle = 1;
    }
    
    if($row1["accesos"] == 15){
        $usabilidad = 1;
    }
    
    if($row1["accesos"] == 11){
        $unixIbm = 1;
    }
    
    if($row1["accesos"] == 12){
        $unixOracle = 1;
    }
    
    if($row1["accesos"] == 9){
        $sap = 1;
    }
    
    if($row1["accesos"] == 10){
        $VMWare = 1;
    }
    
    if($row1["accesos"] == 16){
        $gantt = 1;
    }
    
    if($row1["accesos"] == 1){
        $sam = 1;
    }
}