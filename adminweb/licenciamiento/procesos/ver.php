<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general_licenciamiento.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empresa_licenc.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pais_licenciamiento.php");
require_once($GLOBALS["app_root"] . "/clases/clase_nivel_servicio.php");

// Objetos
$empresa = new clase_empresa_licenciamiento();
$general = new General();
$validator = new validator("form1");
$pais = new pais_licenc();
$lista_p = $pais->listar_todo();

//procesos
$error = 0;
$agregar = 0;
$exito = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$row = $empresa->datos($id_user);
$autorizado = $row["status"];

if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false
&& filter_var($_POST['pais'], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    if ($error == 0) {
        $autorizado = 0;
        if(isset($_POST["modif"]) && $_POST["modif"] == 1){
            $autorizado = 1;
        }
        
        if ($empresa->actualizar($_POST['id'], $general->get_escape($_POST['empresa']), $general->get_escape($_POST['usuario']), $general->get_escape($_POST['email']), 
        $general->get_escape($_POST['cargo']), $_POST['pais'], $autorizado)) {
            $exito = 1;
        } else {
            $error = 3;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_usuario", "usuario", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);
$validator->create_message("msj_cargo", "cargo", " Obligatorio", 0);
$validator->create_message("msj_pais", "pais", " Obligatorio", 0);