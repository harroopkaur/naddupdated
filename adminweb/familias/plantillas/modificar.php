 <?php
if ($exito == true) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/familias/';
            }
        });
    </script>
<?php
}
?>
    
<fieldset style=" border: 1px solid #00B0F0;">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Familia</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="modificar" id="modificar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id ?>">
        <?php $validator->print_script(); ?>
        
        <div class="error_prog">
            <font color="#FF0000">
                <?php
                if ($error == 5) {
                    echo "Ocurrió un error al modificar la familia";
                } else if ($error == 1) {
                ?>
                    <script type="text/javascript">
                        $.alert.open('alert', 'Ya existe esa familia', {'Aceptar': 'Aceptar'}, function(button) {
                            if (button === 'Aceptar'){
                            }
                        });
                    </script>
                <?php
                }
                ?>
            </font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Familia:</th>
                <td align="left">
                    <input type="text" id="familia" name="familia" value="<?= $familia["nombre"] ?>" maxlength="150">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_familia") ?></font></div>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>