<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$configuraciones = new configuraciones();
$validator = new validator("form1");
$general = new General();

$id = 0;
if (isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false) {
    $id = $_GET['id'];
}
$familia = $configuraciones->familiaEspecifico($id);
$error = 0;
$exito = 0;

if (isset($_POST['modificar']) && $_POST["modificar"] == 1) {
    if (isset($_POST["id"]) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
        if ($error == 0) {
            if ($configuraciones->existeFamilia($_POST['familia']) > 0) {
                $error = 1;
            } else {
                if ($configuraciones->modificarFamilia($_POST['id'], $general->get_escape($_POST['familia']))) {
                    $exito = 1;
                } else {
                    $error = 3;
                }
            }
        }
    }
}

$validator->create_message("msj_familia", "familia", " Obligatorio", 0);