<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware
//
// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");

// Objetos
$configuracion = new configuraciones();
$general = new General();

$nombre = '';
if (isset($_GET['nombre'])) {
    $nombre = $_GET['nombre'];
}

if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros = '&pg=' . $_GET['pg'];
    $condiciones = '&nombre=' . $nombre;
} else {
    $start_record = 0;
    $parametros = '';
    $condiciones = '';
}

$listado = $configuracion->listar_familia($nombre, $start_record);

$count = $configuracion->totalFamilia($nombre);
$pag = new paginator($count, $general->limit_paginacion, 'familia.php?', $condiciones);
$i = $pag->get_total_pages();