<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario.php");
require_once($GLOBALS["app_root"] . "/clases/clase_email.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pais.php");

$usuario = new Usuario();
$general = new General();
$validator = new validator("form1");
$paises = new Pais();
$email = new email();


$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    if(isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false){
        if (!$usuario->email_existe($_POST['email'], 0)) {
            $error = 2;
        }  // Email duplicado

        if ($error == 0) {
            if ($usuario->recuperar_contrasena($_POST['email'])) {

                $email->recuperar_clave2($usuario->nombre, $usuario->apellido, $usuario->email, $usuario->clave);
                $exito = 1;
            } else {
                $error = 5;
            }
        }
    }
}


$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);
?>
<!DOCTYPE HTML>
<html>
    <head>
        <?php
        require_once($GLOBALS["app_root"] . "/adminweb/plantillas/head1.php");
        ?>
    </head>
    <body>
        <?php if ($exito == true) { ?>
            <script type="text/javascript">
                $.alert.open('alert', 'Recuperación de contraseña con éxito', {'Aceptar': 'Aceptar'}, function(button) {
                    if (button === 'Aceptar'){
                        location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb';
                    }
                });
            </script>
            <?php
        }
        ?>
        <!--sign up form start here-->
        <div class="app2">
            <div class="logo2" style="margin:20px;">
                <a href="index.php"><img src="<?= $GLOBALS['domain_root'] ?>/imagenes/inicio/logo.png" width="250" alt="licensing Assurance"/></a>
            </div>
            <div class="titulo2">
                <div class="cabeceratil">Recuperar Contrase&ntilde;a
                    <div style="float: right;font-size: 16px;
                         font-weight: normal;">
                    </div>
                </div>
            </div>

            <br>
            <div style="width:85%; margin:0 auto; overflow:hidden;">
                <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
                    <input type="hidden" name="insertar" id="insertar" value="1" />
                    <?php $validator->print_script(); ?>
                    <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                        echo $usuario->error;
                    } ?></font></div>

                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <th width="107" align="left" valign="top">E-mail:</th>
                            <td align="left"><input name="email" id="email" type="text" value="" size="30" maxlength="70"  />
                                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?><?php if ($error == 2) {
                                echo "E-mail Incorrecto";
                            } ?></font></div></td>
                        </tr>

                        <tr>
                            <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="Recuperar" onclick="validate();" class="boton" /></td>
                        </tr>
                    </table>
                </form>
            </div> 
        </div>
    <?php
    require_once("plantillas/pie.php");
    ?>
    <!--sign up form end here-->
    </body>
</html>