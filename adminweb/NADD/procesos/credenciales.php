<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");

// Objetos
$clientes = new Clientes();
$empleados = new empleados();
$general = new General();
$validator = new validator("form1");

//procesos
$error = 0;
$exito = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$empleados->datos($id_user);
$empresas = $clientes->listar_todo();

$autorizado = $empleados->estado; 
if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT)  !== false && isset($_POST["login"]) && isset($_POST["clave"])) {   
    if ($error == 0) {        
       
        $fechaInicio = "";
        if(isset($_POST["fechaInicio"]) && $_POST["fechaInicio"] != ""){
            $fechaInicio = $general->reordenarFecha($_POST["fechaInicio"], "/", "-", "dd/mm/YYYY");
        }
        
        $fechaFin = "";
        if(isset($_POST["fechaFin"]) && $_POST["fechaFin"] != ""){
            $fechaFin = $general->reordenarFecha($_POST["fechaFin"], "/", "-", "dd/mm/YYYY");
        }
        
        if ($empleados->verificarPassNADD($_POST['id'], $general->get_escape($_POST["clave"])) == 0){
            if ($empleados->actualizarNADD($_POST['id'], $general->get_escape($_POST['login']), $general->get_escape($_POST["clave"]),
            $fechaInicio, $fechaFin)) {
                $exito = 1;
            } else {
                $error = 3;
            }
        } else{
            if ($empleados->actualizarNADD1($_POST['id'], $general->get_escape($_POST['login']), $fechaInicio, $fechaFin)) {
                $exito = 1;
            } else {
                $error = 3;
            }
        }
    }
}

$validator->create_message("msj_login", "login", " Obligatorio", 0);
$validator->create_message("msj_clave", "clave", " Obligatorio", 0);
$validator->create_message("msj_fechaInicio", "fechaInicio", " Obligatorio", 0);
$validator->create_message("msj_fechaFin", "fechaFin", " Obligatorio", 0);