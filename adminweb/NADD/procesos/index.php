<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");

// Objetos
$empleados = new empleados();
$general   = new General();

$nombre = "";
if(isset($_GET["nom"])){
    $nombre = $general->get_escape($_GET["nom"]);
}

$empresa = "";
if(isset($_GET["emp"])){
    $empresa = $general->get_escape($_GET["emp"]);
}

$email = "";
if(isset($_GET["ema"])){
    $email = $general->get_escape($_GET["ema"]);
}

$fecha = "";
if(isset($_GET["fec"])){
    $fecha = $general->get_escape($_GET["fec"]);
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '&nom=' . $nom . '&emp=' . $empresa . '&ema=' . $email . '&fec=' . $fecha; //filtro adicional si se le coloca input para filtrar por columnas
} else {
    $start_record = 0;
    $parametros   = '';
}

$listado = $empleados->listar_todo_paginado($nombre, $empresa, $email, $fecha, 1, $start_record);
$count = $empleados->total($nombre, $empresa, $email, $fecha, 1);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i = $pag->get_total_pages();