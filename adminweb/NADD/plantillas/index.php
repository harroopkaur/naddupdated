<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>

<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroNombre" name="filtroNombre" style="width:120px;" value="<?= $nombre ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroEmail" name="filtroEmail" style="width:120px;" value="<?= $email ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroEmpresa" name="filtroEmpresa" style="width:120px;" value="<?= $empresa ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroFecha" name="filtroFecha" style="width:120px;" value="<?= $fecha ?>" readonly></th>
            <th  align="right" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer text-center">Buscar</div></th>
        </tr>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="midle" ><strong class="til">Nombre</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Email Empleado</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Empresa</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Fecha Registro</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Credenciales</strong></th>
        </tr> 
    </thead>
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left"><a href="ver.php?id=<?= $registro['idEmpleado'] ?>">
                     <?= $registro['nombre'] . ' ' . $registro['apellido'] ?>
                    </a></td>
                <td  align="left"><?= $registro['correoEmpleado'] ?></td>
                <td  align="left"><?= $registro['empresa'] ?></td>
                <td  align="center"><?= $general->muestrafecha($registro['fecha_registro']) ?></td>
                <td  align="center">
                    <a href="credenciales.php?id=<?= $registro['idEmpleado'] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                </td>
            </tr>
        <?php } ?>

    </tbody>
</table>
<div id="paginador" style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>
<?php 
    if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay Empleados</div>
<?php } ?>

<script>
    $(document).ready(function(){
        $("#filtroFecha").datepicker();
        
        $("#filtroNombre").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroEmpresa").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroEmail").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroFecha").click(function(){
            $("#filtroFecha").val("");
        });
        
        $("#buscar").click(function(){
            buscarData();
        }); 
    });
    
    function buscarData(){
        $.post("ajax/empleados.php", { nombre : $("#filtroNombre").val(), empresa : $("#filtroEmpresa").val(), email : $("#filtroEmail").val(), 
        fecha : $("#filtroFecha").val(), estado : $("#filtroEstado").val(), pagina : 0, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb";
                return false;
            }
            $("#bodyTable").empty();
            $("#paginador").empty();
            $("#bodyTable").append(data[0].tabla);
            $("#paginador").append(data[0].paginador);
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
</script>