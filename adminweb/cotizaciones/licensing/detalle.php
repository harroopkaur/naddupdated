<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_licensing.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general     = new General();   
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>'false', 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = 'true';
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $cotizacion = new cotizacionLicensing();
            
            $id = 0;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["id"];
            }
            
            $row = $cotizacion->cotizacionEspecifica($id);
            $div = "";
            if($row["id"] != ""){
                $div .= "<div style='width:450px; margin:0 auto;'>"
                    . "<div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Cliente:</div>"
                        . "<div class='divCotizLicensing'>" . $row["nombreSolicitante"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Email:</div>"
                        . "<div class='divCotizLicensing'>" . $row["email"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Teléfono:</div>"
                        . "<div class='divCotizLicensing'>" . $row["telefono"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Hoario:</div>"
                        . "<div class='divCotizLicensing'>" . $row["horario"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Nivel de Servicio:</div>"
                        . "<div class='divCotizLicensing'>" . $row["descripcion"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Fabricante(s):</div>"
                        . "<div class='divCotizLicensing'>" . $row["fabricantes"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>PCs:</div>"
                        . "<div class='divCotizLicensing'>" . $row["pcs"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Servidores:</div>"
                        . "<div class='divCotizLicensing'>" . $row["servidores"] . "</div>"   
                    . "</div>

                    <div class='contenedorFlex'>"
                        . "<div class='divCotizLicensing bold'>Otros:</div>"
                        . "<div class='divCotizLicensing'>" . $row["otro"] . "</div>"   
                    . "</div>"
                . "</div>";
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
?>