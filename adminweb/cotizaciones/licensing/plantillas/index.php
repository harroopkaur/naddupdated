<div class="fondo" id="fondo" style="display:none;">
    <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" alt="Cargando" style="width:50px; height:50px; top:50%; margin-top:25px; left:50%; margin-left:25px; position:absolute;">
</div>

<table style="width:calc(100% - 20px); margin: 0;" class="tablap" id="tablaDetalle">
    <thead>
        <tr style="background:#333; color:#fff;">
            <th align="center" valign="middle"><input type="text" id="nroSolicitud" name="nroSolicitud" style="width:auto;"></th>
            <th align="center" valign="middle"><input type="text" id="cliente" name="cliente" style="width:auto;"></th>
            <th align="center" valign="middle"><input type="text" id="fecha" name="fecha" style="width:auto"></th>
            <th align="center" valign="middle"><input type="text" id="fabricante" name="fabricante" style="width:auto"></th>
            <th align="center" valign="middle"><input type="text" id="estado" name="estado" style="width:auto"></th>
            <th align="center" valign="middle" style="width:60px;">&nbsp;</th>
            <th align="center" valign="middle" style="width:60px;">&nbsp;</th>
        </tr>
        
        <tr style="background:#333; color:#fff;">
            <th align="center" valign="middle"><span>Nro. Solicitud</span></th>
            <th align="center" valign="middle"><span>Cliente</span></th>
            <th align="center" valign="middle"><span>Fecha</span></th>
            <th align="center" valign="middle"><span>Fabricante</span></th>
            <th align="center" valign="middle"><span>Estado</span></th>
            <th align="center" valign="middle" style="width:60px;"><span>Detalle</span></th>
            <th align="center" valign="middle" style="width:60px;"><span></span></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($tabla as $row){
        ?>
            <tr>
                <td align="center" valign="middle"><?= $row["id"] ?></td>
                <td align="center" valign="middle"><?= $row["nombreSolicitante"] ?></td>
                <td align="center" valign="middle"><?= $row["fecha"] ?></td>
                <td align="center" valign="middle"><?= $row["fabricantes"] ?></td>
                <td align="center" valign="middle"><?php if($row["estado"] == 1){ echo "Por Responder"; }else if($row["estado"] == 2){ echo "Respondido"; } ?></td>
                <td align="center" valign="middle"><a href="#" onclick="verDetalle('<?= $row['id'] ?>')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_027_search.png" border="0" alt="Ver" title="Ver"/></a></td>
                <td align="center" valign="middle"><a href="#" onclick="confirmar(<?= $row["id"] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_206_ok_2.png" border="0" alt="Confirmar" title="Confirmar" style="<?php if($row['estado'] == 2){ echo 'display:none;'; }?>"/></a></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<div class="modal-personal modal-personal-md" id="modal-cotizacion">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Detalle <p style="color:#06B6FF; display:inline">Cotización N° <span id="cotiz"></span></p></h1>
    </div>
    <div class="modal-personal-body" id="cuerpo">

    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirCotizacion">Salir</div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#salirCotizacion").click(function(){
           $("#fondo1").hide();
           $("#modal-cotizacion").hide();
        });
    });

    function verDetalle(id){
        $("#fondo").show();
        $.post("<?= $GLOBALS['domain_root']; ?>/adminweb/cotizaciones/licensing/detalle.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb/";
                return false;
            }

            if(data[0].sesion === "false"){
                $.alert.open('alert', data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb/";
                });
                return false;
            }
            $("#cotiz").empty();
            $("#cuerpo").empty();
            $("#fondo").hide();
            $("#cotiz").append(id);
            $("#cuerpo").append(data[0].div);
            $("#fondo1").show();
            $("#modal-cotizacion").show();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        }); 
    }

    function confirmar(id){
        $("#fondo").show();
        $.post("<?= $GLOBALS['domain_root']; ?>/adminweb/cotizaciones/licensing/confirmar.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb/";
                return false;
            }
            if(data[0].sesion === "false"){
                $.alert.open('error', "Error: " + data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb/";
                });
                return false;
            }
            $("#fondo").hide();
            if(data[0].resultado === 0){
                $.alert.open('alert', "No se pudo confirmar la cotización", {'Aceptar' : 'Aceptar'}, function() {
                }); 
            }
            else if(data[0].resultado === 1){
                $.alert.open('info', "Cotización confirmada con éxito", {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/licensing/";
                }); 
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        }); 
    }

    function descargar(archivo){
        window.open("<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/archivos/" + archivo);
    }
</script>