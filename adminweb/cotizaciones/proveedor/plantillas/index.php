<div class="fondo" id="fondo" style="display:none;">
    <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" alt="Cargando" style="width:50px; height:50px; top:50%; margin-top:25px; left:50%; margin-left:25px; position:absolute;">
</div>
                                
<table style="width:calc(100% - 20px); margin: 0;" class="tablap" id="tablaDetalle">
    <thead>
        <tr style="background:#333; color:#fff;">
            <th align="center" valign="middle"><input type="text" id="nroSolicitud" name="nroSolicitud" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="cliente" name="cliente" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="empresa" name="empresa" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="fechaCotizacion" name="fechaCotizacion" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="fabricante" name="fabricante" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="nroCotizacion" name="nroCotizacion" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="proveedor" name="proveedor" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="status" name="status" style="width:100px"></th>
            <th align="center" valign="middle">&nbsp;</th>
            <th align="center" valign="middle" style="width:60px;">&nbsp;</th>
        </tr>
        
        <tr style="background:#333; color:#fff;">
            <th align="center" valign="middle"><span>Nro. Solicitud</span></th>
            <th align="center" valign="middle"><span>Cliente</span></th>
            <th align="center" valign="middle"><span>Empresa</span></th>
            <th align="center" valign="middle"><span>Fecha Solicitud</span></th>
            <th align="center" valign="middle"><span>Fabricante</span></th>
            <th align="center" valign="middle"><span>Nro. Cotización</span></th>
            <th align="center" valign="middle"><span>Proveedor</span></th>
            <th align="center" valign="middle"><span>Estatus</span></th>
            <th align="center" valign="middle" style="width:60px;"><span>Ofertas</span></th>
            <th align="center" valign="middle" style="width:60px;"><span></span></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($tabla as $row){
        ?>
            <tr>
                <td align="center" valign="middle"><a href="#" class="link1" onclick="verCotizacion(<?= $row['id'] ?>)"><?= $row["id"] ?></a></td>
                <td align="center" valign="middle"><?= $row["cliente"] ?></td>
                <td align="center" valign="middle"><?= $row["empresa"] ?></td>
                <td align="center" valign="middle"><?= $row["fechaCot"] ?></td>
                <td align="center" valign="middle"><?= $row["fabricante"] ?></td>
                <td align="center" valign="middle"><?= $row["nroCotizacion"] ?></td>
                <td align="center" valign="middle"><?= $row["proveedor"] ?></td>
                <td align="center" valign="middle"><?= $row["estatus"] ?></td>
                <td align="center" valign="middle"><a href="#" onclick="descargar('<?= $row['archivo'] ?>')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_181_download_alt.png" border="0" alt="Descargar" title="Descargar" style="<?php if($row['archivo'] == ''){ echo 'display:none;'; }?>"/></a></td>
                <td align="center" valign="middle"><a href="#" onclick="eliminar(<?= $row["id"] ?>, '<?= $row["idCotProv"] ?>')"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<div class="modal-personalizado" id="modal-cotizacion">
    <div class="modal-personalizado-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Solicitud de Cotización <p id="cot" style="color:#06B6FF; display:inline"></p></h1>
    </div>
    <div class="modal-personalizado-body">
        <table style="width: 100%;" class="tablap" id="tabla">

        </table>
    </div>
    <div class="modal-personalizado-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salir">Salir</div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#salir").click(function(){
            $("#fondo1").hide();
            $("#modal-cotizacion").hide();
        });
    });
    
    function verCotizacion(iden){
        $("#fondo1").show();
        $.post("ajax/detCotizacion.php", { cotizacion : iden, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>";
                return false;
            }
            /*if(data[0].sesion === "false"){
                $.alert.open('error', "Error: " + data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?= $GLOBALS['domain_root'] ?>";
                });
                return false;
            }*/
            $("#cot").empty();
            $("#cot").append("N° " + iden);
            $("#tabla").empty();
            $("#tabla").append(data[0].tabla);
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo1").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        }); 
        
        $("#modal-cotizacion").show();
    }
    
    function eliminar(id, idCotProv){
        $.alert.open('confirm', "Desea eliminar la cotización", {'Aceptar' : 'Aceptar', 'Cancelar' : 'Cancelar'}, function(button) {
            if(button === 'Aceptar'){
                $.post("<?= $GLOBALS['domain_root']; ?>/adminweb/cotizaciones/proveedor/ajax/eliminarCotProv.php", { id : id, idCotProv : idCotProv, token : localStorage.licensingassuranceToken }, function(data){
                    if(data[0].resultado === false){
                        location.href = "<?= $GLOBALS['domain_root'] ?>";
                        return false;
                    }
                    /*if(data[0].sesion === "false"){
                        $.alert.open('error', "Error: " + data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
                            location.href = "<?= $GLOBALS['domain_root'] ?>";
                        });
                        return false;
                    }*/

                    if(data[0].result === false){
                        $.alert.open('alert', "No se pudo eliminar la cotización", {'Aceptar' : 'Aceptar'}, function() {
                        });
                    } else{
                        $.alert.open('info', "Cotización eliminada con éxito", {'Aceptar' : 'Aceptar'}, function() {
                            location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/proveedor/";
                        });
                    }
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                }); 
            }
        });
    }

    function descargar(archivo){
        window.open("<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/archivos/" + archivo);
    }
</script>