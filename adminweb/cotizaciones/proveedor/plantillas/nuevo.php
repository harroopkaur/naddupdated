<div class="fondo" id="fondo" style="display:none;">
    <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" alt="Cargando" style="width:50px; height:50px; top:50%; margin-top:25px; left:50%; margin-left:25px; position:absolute;">
</div>

<div class="fondo1" id="fondo1"></div>

<table style="width:calc(100% - 20px); margin: 0;" class="tablap" id="tablaDetalle">
    <thead>
        <tr style="background:#333; color:#fff;">
            <th align="center" valign="middle"><input type="text" id="nroSolicitud" name="nroSolicitud" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="cliente" name="cliente" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="empresa" name="empresa" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="fechaCotizacion" name="fechaCotizacion" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="fabricante" name="fabricante" style="width:100px"></th>
            <th align="center" valign="middle"><input type="text" id="status" name="status" style="width:100px"></th>
            <th align="center" valign="middle">&nbsp;</th>
            <th align="center" valign="middle" style="width:60px;">&nbsp;</th>
        </tr>
        
        <tr style="background:#333; color:#fff;">
            <th align="center" valign="middle"><span>Nro. Solicitud</span></th>
            <th align="center" valign="middle"><span>Cliente</span></th>
            <th align="center" valign="middle"><span>Empresa</span></th>
            <th align="center" valign="middle"><span>Fecha Solicitud</span></th>
            <th align="center" valign="middle"><span>Fabricante</span></th>
            <th align="center" valign="middle"><span>Estatus</span></th>
            <th align="center" valign="middle" style="width:60px;"><span>Cotización</span></th>
            <th align="center" valign="middle" style="width:60px;"><span></span></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($tabla as $row){
        ?>
            <tr>
                <td align="center" valign="middle"><a href="#" class="link1" onclick="verCotizacion(<?= $row['id'] ?>)"><?= $row["id"] ?></a></td>
                <td align="center" valign="middle"><?= $row["cliente"] ?></td>
                <td align="center" valign="middle"><?= $row["empresa"] ?></td>
                <td align="center" valign="middle"><?= $row["fechaCot"] ?></td>
                <td align="center" valign="middle"><?= $row["fabricante"] ?></td>
                <td align="center" valign="middle"><?= $row["estatus"] ?></td>
                <td align="center" valign="middle"><a href="#" onclick="subir(<?= $row["id"] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_201_upload.png" border="0" alt="Subir" title="Subir" /></a></td>
                <td align="center" valign="middle"><a href="#" onclick="eliminar(<?= $row["id"] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<form id="form1" name="form1" method="post">
    <div class="modal-personalizado modal-personalizado-md" id="modal-cotizacion">
        <div class="modal-personalizado-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Guardar <p style="color:#06B6FF; display:inline">Cotización Proveedor</p></h1>
        </div>
        <div class="modal-personalizado-body">
            <input type="hidden" id="token" name="token"> 
            <input type="hidden" id="id" name="id">

            <div class="contenedorCot">
                <div class="titulo"><p>Nro Cotización:</p></div>
                <div class="input"><input type="text" id="nroCot" name="nroCot" maxlength="20"></div>

                <div class="titulo"><p>Proveedor:</p></div>
                <div class="input"><input type="text" id="proveedor" name="proveedor" maxlength="70"></div>

                <div class="titulo" style="margin-top:-15px;"><p>Archivo:</p></div>
                <!--<div class="input">-->
                    <div class="input">
                        <input type="text" class="url_file boton4" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo" accept=".pdf">
                        <input type="button" id="buscar" class="botones_m5 boton1" value="Buscar">
                    </div>
                <!--</div>-->
            </div>
        </div>
        <div class="modal-personalizado-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirListado">Salir</div>
            <div class="boton1 botones_m2" id="guardarCotizacion" style="float:right;">Guardar</div>
        </div>
    </div>
</form>

<div class="modal-personalizado" id="modal-ver-cotizacion">
    <div class="modal-personalizado-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Solicitud de Cotización <p id="cot" style="color:#06B6FF; display:inline"></p></h1>
    </div>
    <div class="modal-personalizado-body">
        <table style="width: 100%;" class="tablap" id="tabla">

        </table>
    </div>
    <div class="modal-personalizado-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salir">Salir</div>
    </div>
</div>

<script type="text/javascript">    
    $(document).ready(function(){
        $("#salirListado").click(function(){
            $("#fondo1").hide();
            $("#modal-cotizacion").hide();
            $("#nroCot").val("");
            $("#proveedor").val("");
            $("#archivo").val("");
            $("#archivo").replaceWith($("#archivo").clone(true));
            $("#url_file").val("Nombre del Archivo...");
        });

        $("#buscar").click(function(){
            $("#archivo").click();
        });

        $("#archivo").change(function(e){
            if(addArchivo(e) === false){
                $("#archivo").val("");
                $("#archivo").replaceWith($("#archivo").clone(true));
            }
        });

        $("#guardarCotizacion").click(function(){
            $("#fondo1").hide();
            $("#modal-cotizacion").hide();

            if($("#nroCot").val() === ""){
                $.alert.open("alert", "Debe llenar el número de la cotización", {"Aceptar" : "Aceptar"}, function() {
                    $("#fondo1").show();
                    $("#modal-cotizacion").show();
                    $("#nroCot").focus();
                });
                return false;
            } 

            if($("#proveedor").val() === ""){
                $.alert.open("alert", "Debe llenar el proveedor", {"Aceptar" : "Aceptar"}, function() {
                    $("#fondo1").show();
                    $("#modal-cotizacion").show();
                    $("#proveedor").focus();
                });
                return false;
            }

            if($("#archivo").val() === ""){
                $.alert.open("alert", "Debe buscar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    $("#fondo1").show();
                    $("#modal-cotizacion").show();
                    $("#url_file").focus();
                });
                return false;
            }

            $("#fondo").show();
            $("#token").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form1")[0]);	
            $.ajax({
                type: "POST",
                url: "guardar.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    $("#fondo").hide();
                    if(data[0].resultado === false){
                        location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb/";
                        return false;
                    }
                    /*if(data[0].sesion === "false"){
                        $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                            location.href = "<?//= $GLOBALS['domain_root'] ?>/adminweb/";
                            return false;
                        });
                    }*/
                    result = data[0].result;
                    if(result === false){
                        $.alert.open("alert", "No se pudo guardar la cotización", {"Aceptar" : "Aceptar"}, function() {
                        });
                    }
                    else if(result === true){
                        $.alert.open("info", "Cotización guardada con éxito", {"Aceptar" : "Aceptar"}, function() {
                        });
                    }
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });

        });
        
        $("#salir").click(function(){
            $("#fondo1").hide();
            $("#modal-ver-cotizacion").hide();
        });
    });

    function addArchivo(e){
        file = e.target.files[0]; 
        if (!file.type.match("application/pdf")){
            alert("El archivo a cargar debe ser .pdf");
            return false;
        }
        else if(parseInt(file.size) / 1024 > 5120 ){
            alert("El tamaño permitido es de 5 MB");
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $("#url_file").val("");
            $("#url_file").val(file.name);
            return true;
        }
    }

    function fileOnload(e) {
        result=e.target.result;
    }

    function subir(id){
        $("#fondo1").show();
        $("#modal-cotizacion").show();
        $("#id").val(id);
        $("#nroCot").focus();
    }

    function eliminar(id){
        $.post("<?= $GLOBALS['domain_root']; ?>/adminweb/cotizaciones/proveedor/eliminar.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb/";
                return false;
            }
            /*if(data[0].sesion === "false"){
                $.alert.open('error', "Error: " + data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?//= $GLOBALS['domain_root'] ?>";
                });
                return false;
            }*/

            $("#fondo").hide();
            if(data[0].result === false){
                $.alert.open('alert', "No se pudo eliminar la cotización", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else{
                $.alert.open('info', "Cotización eliminada con éxito", {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/proveedor/nuevo.php";
                });
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function verCotizacion(iden){
        $("#fondo1").show();
        $.post("ajax/detCotizacion.php", { cotizacion : iden, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>";
                return false;
            }
            /*if(data[0].sesion === "false"){
                $.alert.open('error', "Error: " + data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
                    location.href = "<?= $GLOBALS['domain_root'] ?>";
                });
                return false;
            }*/
            $("#cot").empty();
            $("#cot").append("N° " + iden);
            $("#tabla").empty();
            $("#tabla").append(data[0].tabla);
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo1").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        }); 
        
        $("#modal-ver-cotizacion").show();
    }
</script>