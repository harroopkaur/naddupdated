<?php
require_once("../../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_proveedor.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general     = new General();
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $cotizacion = new cotizacionProveedor($conn);
            
            $id = 0;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT)){
                $id = $_POST["id"];
            }
            
            $idCotProv = 0;
            if(isset($_POST["idCotProv"]) && filter_var($_POST["idCotProv"], FILTER_VALIDATE_INT)){
                $idCotProv = $_POST["idCotProv"];
            }
            
            if($idCotProv > 0){
                $archivos = $cotizacion->CotizacionesEspecificaId($id, $idCotProv);
                foreach($archivos as $row){
                    if($row["archivo"] != "" && file_exists ( $GLOBALS['app_root'] . "/adminweb/cotizaciones/archivos/" . $row["archivo"])){
                        unlink($GLOBALS['app_root'] . "/adminweb/cotizaciones/archivos/" . $row["archivo"]);
                    }
                }

                $result = $cotizacion->eliminar($id, $idCotProv);
            } else{
                $result = $cotizacion->eliminar($id, "");
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);