<?php
require_once("../../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_cotizacion_proveedor.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalle_cotizacion_prov.php");
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");

$general = new General();
$cotizacion = new cotizacionProveedor();
$detalleCot = new detalleCotizacionProv();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $id = 0;
            if(isset($_POST["cotizacion"]) && filter_var($_POST["cotizacion"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["cotizacion"];
            }
           
            $listado = $cotizacion->listadoCotizacionesId($id);
            
            $tabla = "";
            foreach($listado as $row){
                $tabla .= '<thead>
                    <tr>
                        <th class="text-center">Cliente</th>
                        <th class="text-center">Empleado</th>
                        <th class="text-center">Fabricante</th>
                    </tr>
                    <tr>
                        <td>' . $row["empresa"] . '</td>
                        <td>' . $row["nombre"] . '</td>
                        <td>' . $row["fabricante"] . '</td>
                    </tr>
                    <tr>
                        <th colspan="2" class="text-center">Producto</th>
                        <th class="text-center">Cantidad</th>
                    </tr>
                </thead>
                <tbody>'; 
            }
            
            $listaDetalle = $detalleCot->detalleCotizacion($id);
            
            foreach($listaDetalle as $row1){
                $tabla .= '<tr>
                        <td colspan="2">' . $row1["descripcion"] . '</td>
                        <td class="text-center">' . $row1["cantidad"] . '</td>
                    </tr>
                </thead>'; 
            }
            
            $tabla .= "</tbody>";
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
?>