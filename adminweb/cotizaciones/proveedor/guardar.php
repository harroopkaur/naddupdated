<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resp_cotizacion_prov.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general     = new General();
    /*if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>'false', 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = 'true';
        $_SESSION['client_tiempo'] = $verifSesion[1];*/
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $cotizacion = new respCotizacionProv($conn);
            
            $id = 0;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT)){
                $id = $_POST["id"];
            }
            
            $nroCotizacion = "";
            if(isset($_POST["nroCot"])){
                $nroCotizacion = $general->get_escape($_POST["nroCot"]);
            }
            
            $proveedor = "";
            if(isset($_POST["proveedor"])){
                $proveedor = $general->get_escape($_POST["proveedor"]);
            }
            
            $nombreArchivo = "cotizacion" . " " . $proveedor . date("dmYHis") . ".pdf";
            if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){
                $archivo = $_FILES['archivo']['name'];
                
                $aux = explode(".", $archivo);
                $ext = count($aux) - 1;
                
                if($aux[$ext] == "pdf" || $aux[$ext] == "PDF"){
                     move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] ."/adminweb/cotizaciones/archivos/" . $nombreArchivo); 
                }
            }
            
            $result = $cotizacion->insertar($id, $nroCotizacion, $proveedor, $nombreArchivo);
            
            //$array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
            $array = array(0=>array('result'=>$result, 'resultado'=>true));
        }
    /*}
    else{
        $general->eliminarSesion();
    }*/
}
echo json_encode($array);
?>