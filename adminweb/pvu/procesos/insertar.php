<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$pvu = new pvu();
$validator = new validator("form1");
$general = new General();

$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    $idProcesador = 0;
    if(isset($_POST["idProces"]) && filter_var($_POST["idProces"], FILTER_VALIDATE_INT) !== false){
        $idProcesador = $_POST["idProces"];
    }
    
    $idModelo = 0;
    if(isset($_POST["idModel"]) && filter_var($_POST["idModel"], FILTER_VALIDATE_INT) !== false){
        $idModelo = $_POST["idModel"];
    }
    
    $cantPVU = 0;
    if(isset($_POST["cantPVU"]) && filter_var($_POST["cantPVU"], FILTER_VALIDATE_INT) !== false){
        $cantPVU = $_POST["cantPVU"];
    }
    
    $existe = $pvu->existe_pvu($idProcesador, $idModelo, 0);
    if($existe > 0){
        $error = 1;
    }
    else{
        if ($pvu->insertar($idProcesador, $idModelo, $cantPVU)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_procesador", "procesador", " Obligatorio", 0);
$validator->create_message("msj_modelo", "modelo", " Obligatorio", 0);
$validator->create_message("msj_pvu", "cantPVU", " Obligatorio", 0);