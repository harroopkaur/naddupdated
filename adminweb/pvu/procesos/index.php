<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu.php");

// Objetos
$pvu = new pvu();
$general = new General();

$procesador = '';
if (isset($_GET['procesador'])) {
    $procesador = $general->get_escape($_GET['procesador']);
}

$modelo = '';
if (isset($_GET['modelo'])) {
    $modelo = $general->get_escape($_GET['modelo']);
}

$cantPVU = '';
if (isset($_GET['cantPVU']) && filter_var($_GET["cantPVU"], FILTER_VALIDATE_INT) !== false) {
    $cantPVU = $_GET['cantPVU'];
}

if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros = '&procesador=' . $procesador . '&modelo=' . $modelo . '&cantPVU=' . $cantPVU;
} else {
    $start_record = 0;
    $parametros = '';
}

// Listar Usuarios
$listaPVU = $pvu->listar_todo_paginado($procesador, $modelo, $cantPVU, $start_record);
$count = $pvu->total($procesador, $modelo, $cantPVU);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?', $parametros);
$i = $pag->get_total_pages();