<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>
<table style="width:90%; margin:0 auto; margin-top:45px; margin-bottom:45px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroProcesador" name="filtroProcesador" style="width:90%;" value="<?= $procesador ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroModelo" name="filtroModelo" style="width:90%;" value="<?= $modelo ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroPVU" name="filtroPVU" style="width:90%;" value="<?= $cantPVU ?>"></th>
            <th  align="center" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer">Buscar</div></th>
            <th  align="center" valign="middle" class="til" style="width:70px;">&nbsp;</th>
        </tr>
        
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til" id="nombre" name="nombre">Procesadores</strong></th>
            <th  align="center" valign="middle" ><strong class="til" id="nombre" name="nombre">Modelo</strong></th>
            <th  align="center" valign="middle" ><strong class="til" id="nombre" name="nombre">PVU</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Modificar</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    <tbody id="bodyTable">
        <?php foreach ($listaPVU as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                    <?= $registro["procesador"] ?>
                </td>
                <td align="left">
                    <?= $registro["modelo"] ?>
                </td>
                <td align="left">
                    <?= $registro["pvu"] ?>
                </td>
                <td  align="center">
                    <a href="modificar.php?id=<?= $registro["id"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>

                </td>
            </tr>
        <?php } ?>

    </tbody>
</table>
<div style="text-align:center; width:90%; margin:0 auto" id="paginador"><?= $pag->print_paginator("") ?>
</div>

<?php
if($count == 0) {
?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay productos</div>
<?php } ?>

<script>
    $(document).ready(function () {
        $("#filtroProcesador").keyup(function (e) {
            if(e.keyCode === 13){
                $.post("ajax/tablaPVU.php", {procesador: $("#filtroProcesador").val(), modelo: $("#filtroModelo").val(), 
                cantPVU: $("#filtroPVU").val(), pagina: 1, token : localStorage.licensingassuranceToken }, function (data) {
                    if(data[0].resultado === true){
                        $("#bodyTable").empty();
                        $("#paginador").empty();
                        $("#bodyTable").append(data[0].tabla);
                        $("#paginador").append(data[0].paginador);
                    }
                    else{
                        location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                    }
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
        
        $("#filtroModelo").keyup(function (e) {
            if(e.keyCode == 13){
                $.post("ajax/tablaPVU.php", { procesador: $("#filtroProcesador").val(), modelo: $("#filtroModelo").val(), 
                cantPVU: $("#filtroPVU").val(), pagina: 1, token : localStorage.licensingassuranceToken }, function (data) {
                    if(data[0].resultado === true){
                        $("#bodyTable").empty();
                        $("#paginador").empty();
                        $("#bodyTable").append(data[0].tabla);
                        $("#paginador").append(data[0].paginador);
                    }
                    else{
                        location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                    }
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
        
        $("#filtroPVU").keyup(function (e) {
            if(e.keyCode == 13){
                $.post("ajax/tablaPVU.php", {procesador: $("#filtroProcesador").val(), modelo: $("#filtroModelo").val(), 
                cantPVU: $("#filtroPVU").val(), pagina: 1, token : localStorage.licensingassuranceToken }, function (data) {
                    if(data[0].resultado === true){
                        $("#bodyTable").empty();
                        $("#paginador").empty();
                        $("#bodyTable").append(data[0].tabla);
                        $("#paginador").append(data[0].paginador);
                    }
                    else{
                        location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                    }
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
        
        $("#buscar").click(function(){
            $.post("ajax/tablaPVU.php", {procesador: $("#filtroProcesador").val(), modelo: $("#filtroModelo").val(), 
                cantPVU: $("#filtroPVU").val(), pagina: 1, token : localStorage.licensingassuranceToken }, function (data) {
                if(data[0].resultado === true){
                    $("#bodyTable").empty();
                    $("#paginador").empty();
                    $("#bodyTable").append(data[0].tabla);
                    $("#paginador").append(data[0].paginador);
                }
                else{
                    location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
    });
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>