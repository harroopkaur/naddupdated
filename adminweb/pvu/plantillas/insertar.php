<?php
if($exito==true){      
?>
<script type="text/javascript">
    $.alert.open('alert', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
        if (button === 'Aceptar'){
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/pvu/';
        }
    });
</script>
<?php 
}
?>
                                
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Procesador</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000">
            <?php if($error == 5) { 
                echo "Ocurrió un error al insertar el PVU"; 
            ?>
                <script type="text/javascript">
                    $.alert.open('alert', <?= $pvu->error ?>, {'Aceptar': 'Aceptar'}, function() {
                    });
                </script>
            <?php
            } 
            else if($error == 1){
            ?>
                <script type="text/javascript">
                    $.alert.open('alert', 'Ya existe ese PVU', {'Aceptar': 'Aceptar'}, function() {
                    });
                </script>
            <?php
            }
            ?></font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th style="width:90px; vertical-align: middle;" align="left" valign="top">Procesador:</th>
                <td style="width:50px; vertical-align: middle;">
                    <input type="hidden" id="idProces" name="idProces">
                    <input type="text" id="procesador" name="procesador" maxlength="150" readonly>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_procesador") ?></font></div>
                </td>
                <td style="vertical-align: middle">
                    <a href="#" id="buscarProcesador"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_027_search.png" style="height:20px;" alt="Buscar" title="Buscar" /></a>
                </td> 
            </tr>
            
            <tr>
                <th style="width:90px; vertical-align: middle;" align="left" valign="top">Modelo:</th>
                <td style="width:50px; vertical-align: middle;">
                    <input type="hidden" id="idModel" name="idModel">
                    <input type="text" id="modelo" name="modelo" maxlength="150" readonly>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_modelo") ?></font></div>
                </td>
                <td style="vertical-align: middle">
                    <a href="#" id="buscarModelo"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_027_search.png" style="height:20px;" alt="Buscar" title="Buscar" /></a>
                </td> 
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">PVU:</th>
                <td align="left">
                    <input type="text" id="cantPVU" name="cantPVU" maxlength="4">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pvu") ?></font></div>
                </td>
                <td></td>
            </tr>

            <tr>
                <td colspan="3" align="center"><input name="insertar" type="button" id="insertar" value="INSERTAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>

<div class="modal-personal" id="modal-Procesadores">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Procesadores</p></h1>
    </div>
    <div class="modal-personal-body">
        <table style="width:50%; margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th style="width:40px;">&nbsp;</th>
                    <th  align="center" valign="middle" ><input type="text" id="filtroProcesador" name="filtroProcesador" style="width:100%" value=""></th>
                    <th  align="center" valign="middle" class="til"><div id="buscarFiltroProcesador" class="pointer" style="background-color:#000000; color:#FFF; border-radius: 15px;padding:3px;width:100px;">Buscar</div></th>
                </tr>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th colspan="3" align="center" valign="middle" ><strong class="til">Procesador</strong></th>
                </tr> 
            </thead>
            <tbody id="bodyTableProcesador">
            </tbody>
        </table>

        <div class="text-center" id="paginacionProcesador"></div>                    
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirProcesador">Salir</div>
        <div class="boton1 botones_m2" id="cargarProcesador" style="float:right;">Cargar</div>
    </div>
</div>

<div class="modal-personal" id="modal-Modelo">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Modelos</p></h1>
    </div>
    <div class="modal-personal-body">
        <table style="width:50%; margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th style="width:40px;">&nbsp;</th>
                    <th  align="center" valign="middle" ><input type="text" id="filtroModelo" name="filtroModelo" style="width:100%" value=""></th>
                    <th  align="center" valign="middle" class="til"><div id="buscarFiltroModelo" class="pointer" style="background-color:#000000; color:#FFF; border-radius: 15px;padding:3px;width:100px;">Buscar</div></th>
                </tr>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th colspan="3" align="center" valign="middle" ><strong class="til">Modelo</strong></th>
                </tr> 
            </thead>
            <tbody id="bodyTableModelo">
            </tbody>
        </table>

        <div class="text-center" id="paginacionModelo"></div>                    
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirModelo">Salir</div>
        <div class="boton1 botones_m2" id="cargarModelo" style="float:right;">Cargar</div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#cantPVU").numeric(false);
        
        //inicio modal procesadores y su busqueda
        $("#buscarProcesador").click(function(){
            buscarProcesador("", 1);
        });
        
        $("#filtroProcesador").keyup(function(e){
            if(e.keyCode === 13){
                buscarProcesador($("#filtroProcesador").val(), 1);
            }
        });
        
        $("#buscarFiltroProcesador").click(function(){
            buscarProcesador($("#filtroProcesador").val(), 1);
        });
        
        $("#salirProcesador").click(function(){
            $("#filtroProcesador").val("");
            $("#fondo1").hide();
            $("#modal-Procesadores").hide();
        });
        
        $("#cargarProcesador").click(function(){
            for(i = 0; i < $("#bodyTableProcesador tr").length; i++){
                if($("#idProcesador" + i).prop("checked")){
                    array = $("#idProcesador" + i).val().split("*");
                    $("#idProces").val(array[0]);
                    $("#procesador").val(array[1]);
                    $("#salirProcesador").click();
                    break;
                }
            }
        });
        //fin modal procesadores y su busqueda
        
        //inicio modal modelos y su busqueda
        $("#buscarModelo").click(function(){
            buscarModelo($("#filtroModelo").val(), 1);
        });
        
        $("#filtroModelo").keyup(function(e){
            if(e.keyCode === 13){
                buscarModelo($("#filtroModelo").val(), 1);
            }
        });
        
        $("#buscarFiltroModelo").click(function(){
            buscarModelo($("#filtroModelo").val(), 1);
        });
        
        $("#salirModelo").click(function(){
            $("#filtroModelo").val("");
            $("#fondo1").hide();
            $("#modal-Modelo").hide();
        });
        
        $("#cargarModelo").click(function(){
            for(i = 0; i < $("#bodyTableModelo tr").length; i++){
                if($("#idModelo" + i).prop("checked")){
                    array = $("#idModelo" + i).val().split("*");
                    $("#idModel").val(array[0]);
                    $("#modelo").val(array[1]);
                    $("#salirModelo").click();
                    break;
                }
            }
        });
        //fin modal modelos y su busqueda
    }); 
    
    function buscarProcesador(nombre, pagina){
        $.post("ajax/tablaProcesadores.php", {nombre: nombre, pagina: pagina, token : localStorage.licensingassuranceToken }, function (data) {
            if(data[0].resultado === true){
                $("#bodyTableProcesador").empty();
                $("#paginacionProcesador").empty();
                $("#bodyTableProcesador").append(data[0].tabla);
                $("#paginacionProcesador").append(data[0].paginador);
                $("#fondo1").show();
                $("#modal-Procesadores").show();
            }
            else{
                location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
            }
        }, "json")
        .fail(function(jqXHR){
            $("#fondo1").hide();
            $("#modal-Procesadores").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function buscarModelo(nombre, pagina){
        $.post("ajax/tablaModeloProcesadores.php", {nombre: nombre, pagina: pagina, token : localStorage.licensingassuranceToken }, function (data) {
            if(data[0].resultado === true){
                $("#bodyTableModelo").empty();
                $("#paginacionModelo").empty();
                $("#bodyTableModelo").append(data[0].tabla);
                $("#paginacionModelo").append(data[0].paginador);
                $("#fondo1").show();
                $("#modal-Modelo").show();
            }
            else{
                location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
            }
        }, "json")
        .fail(function(jqXHR){
            $("#fondo1").hide();
            $("#modal-Modelo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function paginaProcesador(pagina){
        buscarProcesador($("#filtroProcesador").val(), pagina);
    }
    
    function paginaModelo(pagina){
        buscarModelo($("#filtroModelo").val(), pagina);
    }
    
    function selecProcsador(indice){
        $("#idProcesador" + indice).prop("checked", "checked");
    }
    
    function selecModelo(indice){
        $("#idModelo" + indice).prop("checked", "checked");
    }
</script>