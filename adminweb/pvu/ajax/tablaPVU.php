<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            // Objetos
            $pvu = new pvu();
            $general = new General();

            $procesador = '';
            if (isset($_POST['procesador'])) {
                $procesador = $general->get_escape($_POST['procesador']);
            }

            $modelo = '';
            if (isset($_POST['modelo'])) {
                $modelo = $general->get_escape($_POST['modelo']);
            }

            $cantPVU = '';
            if (isset($_POST['cantPVU']) && filter_var($_POST["cantPVU"], FILTER_VALIDATE_INT) !== false) {
                $cantPVU = $_POST['cantPVU'];
            }

            if (isset($_POST['pagina']) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false) {
                $start_record = ($_POST['pagina'] * $general->limit_paginacion) - $general->limit_paginacion;
                $parametros = '&procesador=' . $procesador . '&modelo=' . $modelo . '&cantPVU=' . $cantPVU;
            } else {
                $start_record = 0;
                $parametros = '';
            }

            // Listar Usuarios
            $listaPVU = $pvu->listar_todo_paginado($procesador, $modelo, $cantPVU, $start_record);
            
            $tabla = "";
            foreach ($listaPVU as $registro) {
                $tabla .= '<tr onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                    <td align="left">' . $registro["procesador"] . '</td>
                    <td align="left">' . $registro["modelo"] . '</td>
                    <td align="left">' . $registro["pvu"] . '</td>
                    <td  align="center">
                        <a href="modificar.php?id=' . $registro["id"] . '"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                    </td>
                    <td  align="center">
                        <a href="#" onclick="eliminar(' . $registro['id'] . ')"><img src="'. $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                    </td>
                </tr>';
            }
            
            $count = $pvu->total($procesador, $modelo, $cantPVU);

            $pag = new paginator($count, $general->limit_paginacion, 'index.php?', $parametros);
            $i = $pag->get_total_pages();

            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$pag->print_paginatorAjax(""), 'resultado'=>true));
        }
        
    }
    else{
        $general->eliminarSesion();
    }   
}
echo json_encode($array);
?>