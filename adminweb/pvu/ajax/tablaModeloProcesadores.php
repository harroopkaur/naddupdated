<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . '/clases/paginacion.php');

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $maestra = new tablaMaestra();
            $general = new General();

            $nombre   = "";
            if(isset($_POST["nombre"])){
                $nombre = $general->get_escape($_POST["nombre"]);
            }

            $pagina = 1;
            if(isset($_POST["pagina"]) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false){
                $pagina = $_POST["pagina"];
            }    
            $start_record = ($pagina * $general->limit_paginacion)  - $general->limit_paginacion;

            $tabla = "";
            $listadoProcesadores = $maestra->listar_todo_paginado($nombre, 9, $start_record);
            $i = 0;
            foreach ($listadoProcesadores as $registro) {
                $tabla .= '<tr class="pointer" onclick="selecModelo(' . $i . ');" onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                    <td style="width:40px"><input type="radio" id="idModelo' . $i . '" name="idModelo" value="' . $registro["idDetalle"] . '*' . $registro["descripcion"] . '"></td>
                    <td colspan="2" align="left">' . $registro["descripcion"] . '</td>
                </tr>';
                $i++;
            }
            
            $count = $maestra->total($nombre, 9);
            
            $condiciones  =  '&nombre=' . $nombre;

            $pag = new Paginacion($count, $pagina, "paginaModelo");

            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$pag->get_pag(), 'resultado'=>true));
        }
        
    }
    else{
        $general->eliminarSesion();
    }   
}
echo json_encode($array);
?>