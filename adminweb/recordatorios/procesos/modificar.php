<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_recordatorios.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$clientes = new Clientes();
$general = new General();
$validator = new validator("form1");
$recordatorios = new clase_recordatorios();

//procesos
$error = 0;
$exito = 0;
$actualizar = 0;
$modulo = 1;

$id = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id = $_GET['id']; 
}

$listadoClientes = $clientes->listar_todo();

if (isset($_POST['actualizar']) && isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && isset($_POST["email"])
&& filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
    $actualizar = 1;
            
    if ($recordatorios->actualizar($_POST['id'], $general->get_escape($_POST['email']))) {
        $exito = 1;
    } else {
        $error = 3;
    }
}

$rowRecordatorio = $recordatorios->datos($id);
$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);