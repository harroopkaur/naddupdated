<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_recordatorios.php");

// Objetos
$recordatorios = new clase_recordatorios();
$general  = new General();

$empresa = "";
if(isset($_GET["emp"])){
    $empresa = $general->get_escape($_GET["emp"]);
}

$email = "";
if(isset($_GET["ema"])){
    $email = $general->get_escape($_GET["ema"]);
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '&emp=' . $empresa . '&ema=' . $email;
} else {
    $start_record = 0;
    $parametros   = '';
}

$listado = $recordatorios->listar_todo_paginado($empresa, $email, $start_record);
$count   = $recordatorios->total($empresa, $email);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i   = $pag->get_total_pages();