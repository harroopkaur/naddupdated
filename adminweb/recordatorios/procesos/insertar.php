<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_recordatorios.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$clientes = new Clientes();
$recordatorios = new clase_recordatorios();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;
$modulo = 1;

$listadoClientes = $clientes->listar_todo();

if (isset($_POST['insertar']) && isset($_POST["empresa"]) && filter_var($_POST['empresa'], FILTER_VALIDATE_INT) !== false && 
isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
    $agregar = 1;

    // Validaciones
    
    if ($error == 0) {
        if ($recordatorios->insertar($modulo, $_POST["empresa"], $general->get_escape($_POST['email']))) {
            //$id_user = $recordatorios->ultimo_id();
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);