<?php
if ($actualizar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro actualizado con éxito', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/recordatorios/';
        });
    </script>
    <?php
} else if ($actualizar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'No se pudo actualizado el registro', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/recordatorios/';
        });
    </script>
    <?php
}
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Modificar Datos del Recordatorio</span></legend>
    
    <form id="form1" name="form1" method="post" enctype="multipart/form-data">
        <input type="hidden" name="actualizar" id="actualizar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id ?>">
        <?php $validator->print_script(); ?>
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
            echo $recordatorios->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Empresa:</th>
                <td align="left">
                    <select id="empresa" name="empresa" disabled>
                        <option value="">--Seleccione--</option>
                        <?php foreach($listadoClientes as $row){ ?>
                            <option value="<?= $row["id"] ?>" <?php if($row["id"] == $rowRecordatorio["cliente"]){ echo "selected='selected'"; } ?>><?= $row["empresa"] ?></option>
                        <?php } ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">E-mail:</th>
                <td align="left"><input name="email" id="email" type="text" value="<?= $rowRecordatorio["correo"] ?>" size="30" maxlength="70"/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div></td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
        $("#email").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/recordatorios/ajax/verificarCorreo.php", { empresa : $("#empresa").val(), 
            email : $("#email").val(), id : $("#id").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe el correo', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
    });
</script>