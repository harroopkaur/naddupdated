<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_recordatorios.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $recordatorios = new clase_recordatorios();
            
            $modulo = 1; 
            
            $empresa = 0;
            if(isset($_POST["empresa"]) && filter_var($_POST['empresa'], FILTER_VALIDATE_INT) !== false){
                $empresa = $_POST["empresa"];
            }
            
            $email = "";
            if(isset($_POST["email"]) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false){
                $email = $general->get_escape($_POST["email"]);
            }
                        
            $id = 0;
            if(isset($_POST["id"]) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["id"];
            }
            
            $result = $recordatorios->email_existe($modulo, $empresa, $email, $id);
        
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
?>