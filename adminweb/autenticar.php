<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario.php");
require_once($GLOBALS["app_root"] . "/clases/middleware.php");

// Objetos
$usuario = new Usuario();

// Inicializar error
$error = 0;

if (isset($_POST['entrar']) && $_POST['entrar'] == 1) {
    if ($usuario->autenticar($_POST['login'], $_POST['contrasena'])) {
        $nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
        ?>
        <script>
            localStorage.licensingassuranceToken =  '<?= $nuevo_middleware->obtener_token() ?>';
            location.href = "inicio.php";
        </script>
        <?php
    } else {
        header('location: index.php?error=1');
    }
}
?>
