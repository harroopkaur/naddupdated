<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pagos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pagos_fabricantes_detalle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");


// Objetos
$general = new General();
$validator = new validator("form1");
$pagoFabricante = new pagos_fabricantes();
$pagoFabDetalle = new pagos_fabricantes_detalle();

$exito = 1;
$agregar = 0;
$id = 0;

if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $id = $_GET["id"];
}

if (isset($_POST['insertar']) && filter_var($_POST['insertar'], FILTER_VALIDATE_INT) !== false && $_POST['insertar'] == 1) {
    $agregar = 1;
    
    $id = 0;
    if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
        $id = $_POST["id"];
    }
    
    $descrip = "";
    if(isset($_POST["descrip"])){
        $descrip = $general->get_escape($_POST["descrip"]);
    }
        
    $aniversario = $_POST["aniversario"];
    $renovacion = $_POST["renovacion"];
    $monto = $_POST["pago"];
    $idDetalle = $_POST["idDetalle"];
    
    if($pagoFabricante->actualizar($id, $descrip)){
        $j = 0;
        for($i = 0; $i < count($aniversario); $i++){
            $aniv = null;
            if($aniversario[$i] != ""){
                $aniv = $general->reordenarFecha($aniversario[$i], "/");
            }
            
            $reno = null;
            if($renovacion[$i] != ""){
                $reno = $general->reordenarFecha($renovacion[$i], "/");
            }

            $pago = 0;
            if(filter_var($monto[$i], FILTER_VALIDATE_FLOAT) !== false){
                $pago = $monto[$i];
            }

            $idDet = 0;
            if(filter_var($idDetalle[$i], FILTER_VALIDATE_INT) !== false){
                $idDet = $idDetalle[$i];
            }

            if($pagoFabDetalle->actualizar($idDet, $aniv, $reno, $pago)){
                $j++;
            }
        }

        if($j != $i){
            $exito = 2;
        }
    } else{
        $exito = 0;        
    }

    
}  

$datosPago = $pagoFabricante->datosPago($id);
$listaFabActivos = $pagoFabDetalle->pagoDetalle($id);

$id_user = $datosPago["cliente"];

$validator->create_message("msj_descrip", "descrip", " Obligatorio", 0);