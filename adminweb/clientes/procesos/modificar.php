<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pais.php");
require_once($GLOBALS["app_root"] . "/clases/clase_nivel_servicio.php");

// Objetos
$clientes = new Clientes();
$clientes2 = new Clientes();
$general = new General();
$validator = new validator("form1");
$pais = new Pais();
$nivelServicio = new nivelServicio();
$tablaNivelServicio = $nivelServicio->listar(); 
$lista_p = $pais->listar_todo();

//procesos
$error = 0;
$exito = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$clientes->datos($id_user);

if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && filter_var($_POST['pais'], FILTER_VALIDATE_INT) !== false
   && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false && filter_var($_POST['nivelServicio'], FILTER_VALIDATE_INT) !== false) {
    
    if ($error == 0) {
        if ($clientes2->actualizar($_POST['id'], $general->get_escape($_POST['email']), $general->get_escape($_POST['telefono']), $general->get_escape($_POST['empresa']), 
        $_POST['pais'], $_POST["nivelServicio"])) {
            $exito = 1;
        } else {
            $error = 3;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);
$validator->create_message("msj_telefono", "telefono", " Obligatorio", 0);
$validator->create_message("msj_pais", "pais", " Obligatorio", 0);
$validator->create_message("msj_nivelServicio", "nivelServicio", " Obligatorio", 0);