<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_certificaciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$certificaciones =  new certificaciones();
$general = new General();
$validator = new validator("form1");

//procesos
$exito = 1;
$id_user = 0;
$agregar = 0;

if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

if(isset($_GET["pg"]) && filter_var($_GET["pg"], FILTER_VALIDATE_INT) !== false){
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = ''; //filtro adicional si se le coloca input para filtrar por columnas
}
else{
    $start_record = 0;
    $parametros   = '';
}

$listado = $certificaciones->certificadosCliente($id_user, $start_record);
$count   = $certificaciones->total($id_user);

$pag = new paginator($count, $general->limit_paginacion, 'certificado.php?' . $parametros);

if (isset($_POST['insertar']) && filter_var($_POST['insertar'], FILTER_VALIDATE_INT) !== false && $_POST['insertar'] == 1) {
    $agregar = 1;
    
    // Validaciones
    if(isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
        $id_user = $_POST['id']; 
    }
    
    $descripcion = "";
    if(isset($descripcion)){
        $descripcion = $general->get_escape($_POST["descrip"]);
    }
   
    if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        if(!file_exists($GLOBALS['app_root'] . "/adminweb/clientes/certificados/" . $id_user)){
            mkdir($GLOBALS['app_root'] . "/adminweb/clientes/certificados/" . $id_user, 0777, true);
        }
        
        $fecha = date("dmYHis");
        $archivo = "certificado" . $fecha . ".pdf";
        move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] ."/adminweb/clientes/certificados/" . $id_user . "/" . $archivo); 
    }
    else{
        $exito = 0;
    }
    
    if ($exito == 1) {
        if(!$certificaciones->insertar($id_user, $descripcion, $archivo)){
            $exito = 2;
        }
    }
}

$validator->create_message("msj_descripcion", "descrip", " Obligatorio", 0);
$validator->create_message("msj_archivo", "archivo", " Obligatorio", 0);