<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_fabricantes_activos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pagos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pagos_fabricantes_detalle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");


// Objetos
$general = new General();
$validator = new validator("form1");
$fabricantes = new configuraciones();
$fabricantesActivos = new fabricantesActivos();
$pagoFabricante = new pagos_fabricantes();
$pagoFabDetalle = new pagos_fabricantes_detalle();

$exito = 1;
$agregar = 0;
$id_user = 0;

if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET["id"];
}

if(isset($_GET["pg"]) && filter_var($_GET["pg"], FILTER_VALIDATE_INT) !== false){
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = ''; //filtro adicional si se le coloca input para filtrar por columnas
}
else{
    $start_record = 0;
    $parametros   = '';
}

if (isset($_POST['insertar']) && filter_var($_POST['insertar'], FILTER_VALIDATE_INT) !== false && $_POST['insertar'] == 1) {
    $agregar = 1;
    
    if(isset($_POST["id_user"]) && filter_var($_POST["id_user"], FILTER_VALIDATE_INT) != false){
        $id_user = $_POST["id_user"];
    }
    
    $descrip = "";
    if(isset($_POST["descrip"])){
        $descrip = $general->get_escape($_POST["descrip"]);
    }
    
    if($pagoFabricante->insertar($id_user, $descrip)){
        $idPago = $pagoFabricante->ult_id();
        
        $aniversario = $_POST["aniversario"];
        $renovacion = $_POST["renovacion"];
        $monto = $_POST["pago"];
        $fabricante = $_POST["fab"];
        
        $j = 0;
        for($i = 0; $i < count($aniversario); $i++){
            $aniv = null;
            if($aniversario[$i] != ""){
                $aniv = $general->reordenarFecha($aniversario[$i], "/");
            }
            
            $reno = null;
            if($renovacion[$i] != ""){
                $reno = $general->reordenarFecha($renovacion[$i], "/");
            }
            
            $pago = 0;
            if(filter_var($monto[$i], FILTER_VALIDATE_FLOAT) !== false){
                $pago = $monto[$i];
            }
            
            $fab = 0;
            if(filter_var($fabricante[$i], FILTER_VALIDATE_INT) !== false){
                $fab = $fabricante[$i];
            }
            
            if($pagoFabDetalle->insertar($idPago, $fab, $aniv, $reno, $pago)){
                $j++;
            }
        }
        
        if($j != $i){
            $exito = 2;
        }
    } else{
        $exito = 0;
    }
}  

$listadoFabricantes = $fabricantes->listar_fabricantes("");
$listaFabActivos = $fabricantesActivos->listar_fabricantes($id_user); 
$listadoPagos = $pagoFabricante->listado($id_user, $start_record);
$count = $pagoFabricante->total($id_user);

$pag = new paginator($count, $general->limit_paginacion, 'fabricantesActivos.php?' . $parametros);

$validator->create_message("msj_descrip", "descrip", " Obligatorio", 0);