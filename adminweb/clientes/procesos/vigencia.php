<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$clientes = new Clientes();
$clientes2 = new Clientes();
$general = new General();
$validator = new validator("form1");

//procesos
$error = 0;
$exito = 0;

$id_user = 0;

if(filter_var(isset($_GET['id']) && $_GET['id'], FILTER_VALIDATE_INT) !== false){
   $id_user = $_GET['id'];
}

$clientes2->datos($id_user); 

if (isset($_POST['insertar']) && filter_var($_POST['estado'], FILTER_VALIDATE_INT) !== false) {
    // Validaciones

    $fecha1 = $general->guardafecha($_POST['fecha']);
    $fecha2 = $general->guardafecha($_POST['fecha2']);

    if ($error == 0) {
        if ($clientes->actualizar2($id_user, $_POST['estado'], $fecha1, $fecha2)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_fecha", "fecha", " Obligatorio", 0);
$validator->create_message("msj_fecha2", "fecha2", " Obligatorio", 0);