<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

//clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pagos_fabricantes.php");

// Objetos
$pago = new pagos_fabricantes();
$general = new General();

//procesos
$exito = false;

$id_user = 0;
if(isset($_POST["cliente"]) && filter_var($_POST["cliente"], FILTER_VALIDATE_INT) !== false){
    $id_user = $_POST["cliente"];
}

if (isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    $exito = $pago->eliminar($_POST['id']);
}