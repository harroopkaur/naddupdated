<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pais.php");
require_once($GLOBALS["app_root"] . "/clases/clase_nivel_servicio.php");

// Objetos
$clientes = new Clientes();
$clientes2 = new Clientes();
$general = new General();
$validator = new validator("form1");
$validator2 = new validator("form2");
$validator3 = new validator("form3");
$pais = new Pais();
$nivelServicio = new nivelServicio();
$tablaNivelServicio = $nivelServicio->listar(); 
$lista_p = $pais->listar_todo();

//procesos
$error = 0;
$exito = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

if (isset($_POST['insertar']) && isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
        $id_user = $_POST['id']; 
        $dataimgh = '';
        $dataimgf = '';
        $max      = 5*1024*1024; // se coloca el maximo permitido  //  size del archivo
	    $uploaddir = $GLOBALS["app_root1"]."/img/clientes/email/";

        if(isset($_FILES['imgheader']['name'])){
            $filename       = $_FILES['imgheader']['name'];
            $tipo_archivo   = $_FILES['imgheader']['type'];
            $tamano_archivo = $_FILES['imgheader']['size'];

            $imgHeaderFooter = $clientes2->seleccionarHeaderFooterNADD($id_user);
            
            if($filename != '') {
                if($tamano_archivo < $max){
                    if($tamano_archivo > 0){
                        if((preg_match("[.png]", substr($filename,-4))) || (preg_match("[.jpg]", substr($filename,-4))) || (preg_match("[.gif]", substr($filename,-4))) || (preg_match("[.JPG]", substr($filename,-4)))|| (preg_match("[.GIF]", substr($filename,-4)))){
                            $uploadfile = $uploaddir.$id_user.$filename;
                            if (move_uploaded_file($_FILES['imgheader']['tmp_name'], $uploadfile)) {
                                if ($imgHeaderFooter["img_header"] != ""){
                                    unlink($uploaddir.$id_user.$imgHeaderFooter["img_header"]);
                                }
                            }else{ print("Error de conexi&oacute;n con el servidor."); }
                        }else{ print("Solo se permiten imagenes en formato jpg. y gif., no se ha podido adjuntar."); }
                    }else{ print("Campo vacio, no ha seleccionado ninguna imagen"); }
                }else{ print("La imagen que ha intentado adjuntar es mayor de 1.5 Mb, si desea cambie el tamano del archivo y vuelva a intentarlo."); }

                $dataimgh = $filename;
            }
        }

        if(isset($_FILES['imgfooter']['name'])){
            $filename       = $_FILES['imgfooter']['name'];
            $tipo_archivo   = $_FILES['imgfooter']['type'];
            $tamano_archivo = $_FILES['imgfooter']['size'];

            if($filename != '') {
                if($tamano_archivo < $max){
                    if($tamano_archivo > 0){
                        if((preg_match("[.png]", substr($filename,-4))) || (preg_match("[.jpg]", substr($filename,-4))) || (preg_match("[.gif]", substr($filename,-4))) || (preg_match("[.JPG]", substr($filename,-4)))|| (preg_match("[.GIF]", substr($filename,-4)))){
                            $uploadfile = $uploaddir.$id_user.$filename;
                            if (move_uploaded_file($_FILES['imgfooter']['tmp_name'], $uploadfile)) {
                                if ($imgHeaderFooter["img_footer"] != ""){
                                    unlink($uploaddir.$id_user.$imgHeaderFooter["img_footer"]);
                                }
                            }else{ print("Error de conexi&oacute;n con el servidor."); }
                        }else{ print("Solo se permiten imagenes en formato png, jpg, gif "); }
                    }else{ print("Campo vacio, no ha seleccionado ninguna imagen"); }
                }else{ print("La imagen que ha intentado adjuntar es mayor de 1.5 Mb, si desea cambie el tamano del archivo y vuelva a intentarlo."); }

                $dataimgf = $filename;
            }
        }

        if(isset($_POST['htmlheader'])){
            $datahh = $_POST['htmlheader'];
        }

        if(isset($_POST['htmlfooter'])){
            $datahf = $_POST['htmlfooter'];
        }

        if ($clientes2->actualizarNadd($_POST['id'], $dataimgh, $dataimgf, $datahh, $datahf)) {
            $exito = 1;
        } else {
            $error = 3;
        }
}

$clientes->datos($id_user);