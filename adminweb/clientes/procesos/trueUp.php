<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_trueUp.php");
require_once($GLOBALS["app_root"] . "/clases/clase_trueUp_detalle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");


// Objetos
$general = new General();
$validator = new validator("form1");
$trueUp = new trueUp();
$trueUpDetalle = new trueUpDetalle();

$exito = 1;
$agregar = 0;
$id_user = 0;

if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET["id"];
}

if(isset($_GET["pg"]) && filter_var($_GET["pg"], FILTER_VALIDATE_INT) !== false){
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = ''; //filtro adicional si se le coloca input para filtrar por columnas
}
else{
    $start_record = 0;
    $parametros   = '';
}

if (isset($_POST['insertar']) && filter_var($_POST['insertar'], FILTER_VALIDATE_INT) !== false && $_POST['insertar'] == 1) {
    $agregar = 1;
    
    // Validaciones
    if(isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
        $id_user = $_POST['id']; 
    }
    
    $descripcion = "";
    if(isset($descripcion)){
        $descripcion = $general->get_escape($_POST["descrip"]);
    }
   
    if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        $nombre_archivo = $_FILES['archivo']['name'];

        // Validaciones
        if($nombre_archivo != ""){
            $extension = explode(".",$nombre_archivo);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "csv") && ($extension[$long] != "CSV")) { $exito = 3; }  
            // Permitir subir solo imagenes JPG,
        }else{
            $exito = 4;	
        }
        
        if($exito == 1){
            if(!file_exists($GLOBALS['app_root'] . "/adminweb/clientes/trueUp/" . $id_user)){
                mkdir($GLOBALS['app_root'] . "/adminweb/clientes/trueUp/" . $id_user, 0777, true);
            }
        
            $fecha = date("dmYHis");
            $archivo = "trueUp" . $fecha . ".csv";
            move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] ."/adminweb/clientes/trueUp/" . $id_user . "/" . $archivo); 
            
            $urlTrueUp = "trueUp/" . $id_user . "/" . $archivo;
            if($general->obtenerSeparador($urlTrueUp) === true){
                if (($fichero = fopen($urlTrueUp, "r")) !== false) {
                    $i=1;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i == 1 && ($datos[0] != "Producto" || utf8_encode($datos[1]) != "Edición" || utf8_encode($datos[2]) != "Versión" 
                        || $datos[3] != "Precio Estimado" || $datos[4] != "Actual" || $datos[6] != "LA Proposed" || $datos[8] != "Comentarios")){
                            $exito = 5;
                            break;
                        } else if($i == 2 && ($datos[4] != "Cantidad" || $datos[5] !=  "Total" || $datos[6] != "Cantidad" || $datos[7] != "Total")){
                            $exito = 5;
                            break;
                        } else if($i > 2){
                            break;
                        }
                        $i++;
                    }
                    fclose($fichero);
                }
            } else{
                $exito = 6;
            }
        }
    }
    else{
        $exito = 0;
    }
    
    if ($exito == 1) {
        if(!$trueUp->insertar($id_user, $descripcion, $archivo)){
            $exito = 2;
        } else{
            $newId = $trueUp->obtenerUltId();
            
            if($general->obtenerSeparador($urlTrueUp) === true){
                if (($fichero = fopen($urlTrueUp, "r")) !== false) {
                    $i=1;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i > 2){
                            $trueUpDetalle->insertar($newId, $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], 
                            $datos[6], $datos[7], $datos[8]);
                        }
                        $i++;
                    }
                    fclose($fichero);
                }
            }
        }
    }
}  

$listado = $trueUp->listadoPaginado($id_user, $start_record);
$count   = $trueUp->total($id_user);

$pag = new paginator($count, $general->limit_paginacion, 'trueUp.php?' . $parametros);

$validator->create_message("msj_descripcion", "descrip", " Obligatorio", 0);