<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_gantt.php");
require_once($GLOBALS["app_root"] . "/clases/clase_ganttDetalle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");


// Objetos
$general = new General();
$validator = new validator("form1");
$gantt = new gantt();
$ganttDetalle = new ganttDetalle();

$exito = 1;
$agregar = 0;
$id_user = 0;

if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET["id"];
}

if(isset($_GET["pg"]) && filter_var($_GET["pg"], FILTER_VALIDATE_INT) !== false){
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = ''; //filtro adicional si se le coloca input para filtrar por columnas
}
else{
    $start_record = 0;
    $parametros   = '';
}

if (isset($_POST['insertar']) && filter_var($_POST['insertar'], FILTER_VALIDATE_INT) !== false && $_POST['insertar'] == 1) {
    $agregar = 1;
    
    // Validaciones
    if(isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
        $id_user = $_POST['id']; 
    }
    
    $descripcion = "";
    if(isset($descripcion)){
        $descripcion = $general->get_escape($_POST["descrip"]);
    }
   
    if(!$gantt->insertar($id_user, $descripcion)){
        $exito = 0;
    } else{
        $newId = $gantt->obtenerUltId();
        
        $fechaL = array(null, null, null, null);
        $fechaTS = array(null, null, null, null);
        $fechaRS = array(null, null, null, null);
        $fechaRP = array(null, null, null, null);
        $fechaOpt = array(null, null, null, null);
        $fechaMit = array(null, null, null, null);
        $fechaReno = array(null, null, null, null);
        $fechaTU = array(null, null, null, null);
        $fechaRev = array(null, null, null, null);
        $fechaP = array(null, null, null, null);
        
        for($i = 1; $i < 5; $i++) {
            if(isset($_POST["fechaL" . $i]) && $general->validateDate($_POST["fechaL" . $i], 'd/m/Y')){
                $fechaL[($i - 1)] = $general->reordenarFecha($_POST["fechaL" . $i], "/");
            }
            
            if(isset($_POST["fechaTS" . $i]) && $general->validateDate($_POST["fechaTS" . $i], 'd/m/Y')){
                $fechaTS[($i - 1)] = $general->reordenarFecha($_POST["fechaTS" . $i], "/");
            }
            
            if(isset($_POST["fechaRS" . $i]) && $general->validateDate($_POST["fechaRS" . $i], 'd/m/Y')){
                $fechaRS[($i - 1)] = $general->reordenarFecha($_POST["fechaRS" . $i], "/");
            }
            
            if(isset($_POST["fechaRP" . $i]) && $general->validateDate($_POST["fechaRP" . $i], 'd/m/Y')){
                $fechaRP[($i - 1)] = $general->reordenarFecha($_POST["fechaRP" . $i], '/');
            }
            
            if(isset($_POST["fechaOpt" . $i]) && $general->validateDate($_POST["fechaOpt" . $i], 'd/m/Y')){
                $fechaOpt[($i - 1)] = $general->reordenarFecha($_POST["fechaOpt" . $i], '/');
            }
            
            if(isset($_POST["fechaMit" . $i]) && $general->validateDate($_POST["fechaMit" . $i], 'd/m/Y')){
                $fechaMit[($i - 1)] = $general->reordenarFecha($_POST["fechaMit" . $i], '/');
            }
            
            if(isset($_POST["fechaReno" . $i]) && $general->validateDate($_POST["fechaReno" . $i], 'd/m/Y')){
                $fechaReno[($i - 1)] = $general->reordenarFecha($_POST["fechaReno" . $i], '/');
            }
            
            if(isset($_POST["fechaTU" . $i]) && $general->validateDate($_POST["fechaTU" . $i], 'd/m/Y')){
                $fechaTU[($i - 1)] = $general->reordenarFecha($_POST["fechaTU" . $i], '/');
            }
            
            if(isset($_POST["fechaRev" . $i]) && $general->validateDate($_POST["fechaRev" . $i], 'd/m/Y')){
                $fechaRev[($i - 1)] = $general->reordenarFecha($_POST["fechaRev" . $i], '/');
            }
            
            if(isset($_POST["fechaP" . $i]) && $general->validateDate($_POST["fechaP" . $i], 'd/m/Y')){
                $fechaP[($i - 1)] = $general->reordenarFecha($_POST["fechaP" . $i], '/');
            }
        }
        
        if(!$ganttDetalle->insertar($newId, $fechaL[0], $fechaL[1], $fechaL[2], $fechaL[3], $fechaTS[0], 
        $fechaTS[1], $fechaTS[2], $fechaTS[3], $fechaRS[0], $fechaRS[1], $fechaRS[2], $fechaRS[3], 
        $fechaRP[0], $fechaRP[1], $fechaRP[2], $fechaRP[3], $fechaOpt[0], $fechaOpt[1], $fechaOpt[2], 
        $fechaOpt[3], $fechaMit[0], $fechaMit[1], $fechaMit[2], $fechaMit[3], $fechaReno[0], $fechaReno[1], 
        $fechaReno[2], $fechaReno[3], $fechaTU[0], $fechaTU[1], $fechaTU[2], $fechaTU[3], $fechaRev[0], 
        $fechaRev[1] ,$fechaRev[2], $fechaRev[3], $fechaP[0], $fechaP[1], $fechaP[2], $fechaP[3])){
            $exito = 2;
        }
    }
}  

$listado = $gantt->listadoPaginado($id_user, $start_record);
$count   = $gantt->total($id_user);

$pag = new paginator($count, $general->limit_paginacion, 'gantt.php?' . $parametros);

$validator->create_message("msj_descripcion", "descrip", " Obligatorio", 0);