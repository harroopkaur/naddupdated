<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $clientes = new clientes();
                     
            $clienteSuscripcion = 0;
            if(isset($_POST["cliente"]) && filter_var($_POST["cliente"]) !== false){
                $clienteSuscripcion = $_POST["cliente"];
            }
            
            $info = $clientes->suscripcionNADD($clienteSuscripcion);
            
            $cantCorreos = 0;
            if ($info["cantidadCorreosDominio"] > 0){
                $cantCorreos = $info["cantidadCorreosDominio"];
            }
            
            $cantDespliegue = 0;
            if ($info["cantidadDespliegueEquipos"] > 0){
                $cantDespliegue = $info["cantidadDespliegueEquipos"];
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'cantCorreos'=>$cantCorreos, 'cantDespliegue'=>$cantDespliegue, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);