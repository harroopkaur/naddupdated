<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_gantt.php");
require_once($GLOBALS["app_root"] . "/clases/clase_ganttDetalle.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $gantt = new gantt();
            $ganttDetalle = new ganttDetalle();
            
            $id = 2;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["id"];
            }  

            $rowGantt = $gantt->datosGantt($id);
            $row = $ganttDetalle->datosGanttDetalle($id);
            
            $listado = "";
            $i = 0;
            $totalPropuesto = 0;
            
            $listado .= '<tr>
                    <th class="text-center">Levantamiento</th>
                    <th class="text-center">L</th>
                    <td class="text-center">';
                        if($row["L1"] != ""){
                            $listado .= $general->muestrafecha($row["L1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["L2"] != ""){
                            $listado .= $general->muestrafecha($row["L2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["L3"] != ""){
                            $listado .= $general->muestrafecha($row["L3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["L4"] != ""){
                            $listado .= $general->muestrafecha($row["L4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Troubleshooting</th>
                    <th class="text-center">TS</th>
                    <td class="text-center">';
                        if($row["TS1"] != ""){
                            $listado .= $general->muestrafecha($row["TS1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["TS2"] != ""){
                            $listado .= $general->muestrafecha($row["TS2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["TS3"] != ""){
                            $listado .= $general->muestrafecha($row["TS3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["TS4"] != ""){
                            $listado .= $general->muestrafecha($row["TS4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Reporte Servidores</th>
                    <th class="text-center">RS</th>
                    <td class="text-center">';
                        if($row["RS1"] != ""){
                            $listado .= $general->muestrafecha($row["RS1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["RS2"] != ""){
                            $listado .= $general->muestrafecha($row["RS2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["RS3"] != ""){
                            $listado .= $general->muestrafecha($row["RS3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["RS4"] != ""){
                            $listado .= $general->muestrafecha($row["RS4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Reporte PCs</th>
                    <th class="text-center">RP</th>
                    <td class="text-center">';
                        if($row["RP1"] != ""){
                            $listado .= $general->muestrafecha($row["RP1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["RP2"] != ""){
                            $listado .= $general->muestrafecha($row["RP2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["RP3"] != ""){
                            $listado .= $general->muestrafecha($row["RP3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["RP4"] != ""){
                            $listado .= $general->muestrafecha($row["RP4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Otimización</th>
                    <th class="text-center">Opt</th>
                    <td class="text-center">';
                        if($row["Opt1"] != ""){
                            $listado .= $general->muestrafecha($row["Opt1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Opt2"] != ""){
                            $listado .= $general->muestrafecha($row["Opt2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Opt3"] != ""){
                            $listado .= $general->muestrafecha($row["Opt3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Opt4"] != ""){
                            $listado .= $general->muestrafecha($row["Opt4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Mitigación</th>
                    <th class="text-center">Mit</th>
                    <td class="text-center">';
                        if($row["Mit1"] != ""){
                            $listado .= $general->muestrafecha($row["Mit1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Mit2"] != ""){
                            $listado .= $general->muestrafecha($row["Mit2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Mit3"] != ""){
                            $listado .= $general->muestrafecha($row["Mit3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Mit4"] != ""){
                            $listado .= $general->muestrafecha($row["Mit4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Renovación</th>
                    <th class="text-center">Reno</th>
                    <td class="text-center">';
                        if($row["Reno1"] != ""){
                            $listado .= $general->muestrafecha($row["Reno1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Reno2"] != ""){
                            $listado .= $general->muestrafecha($row["Reno2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Reno3"] != ""){
                            $listado .= $general->muestrafecha($row["Reno3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Reno4"] != ""){
                            $listado .= $general->muestrafecha($row["Reno4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">True UP</th>
                    <th class="text-center">TU</th>
                    <td class="text-center">';
                        if($row["TU1"] != ""){
                            $listado .= $general->muestrafecha($row["TU1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["TU2"] != ""){
                            $listado .= $general->muestrafecha($row["TU2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["TU3"] != ""){
                            $listado .= $general->muestrafecha($row["TU3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["TU4"] != ""){
                            $listado .= $general->muestrafecha($row["TU4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Revisión</th>
                    <th class="text-center">Rev</th>
                    <td class="text-center">';
                        if($row["Rev1"] != ""){
                            $listado .= $general->muestrafecha($row["Rev1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Rev2"] != ""){
                            $listado .= $general->muestrafecha($row["Rev2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Rev3"] != ""){
                            $listado .= $general->muestrafecha($row["Rev3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["Rev4"] != ""){
                            $listado .= $general->muestrafecha($row["Rev4"]);
                        }
                    $listado .= '</td>
                </tr>

                <tr>
                    <th class="text-center">Presupuesto</th>
                    <th class="text-center">P</th>
                    <td class="text-center">';
                        if($row["P1"] != ""){
                            $listado .= $general->muestrafecha($row["P1"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["P2"] != ""){
                            $listado .= $general->muestrafecha($row["P2"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["P3"] != ""){
                            $listado .= $general->muestrafecha($row["P3"]);
                        }
                    $listado .= '</td>
                    <td class="text-center">';
                        if($row["P4"] != ""){
                            $listado .= $general->muestrafecha($row["P4"]);
                        }
                    $listado .= '</td>
                </tr>';
                
            $listado .= '<script> '
                . '$(document).ready(function(){
                    for(i = 1; i < 5; i++){
                        $("#fechaLEdit" + i).datepicker({changeMonth: true});
                        $("#fechaTSEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRSEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRPEdit" + i).datepicker({changeMonth: true});
                        $("#fechaOptEdit" + i).datepicker({changeMonth: true});
                        $("#fechaMitEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRenoEdit" + i).datepicker({changeMonth: true});
                        $("#fechaTUEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRevEdit" + i).datepicker({changeMonth: true});
                        $("#fechaPEdit" + i).datepicker({changeMonth: true});
                    } '
                . '});'
            . '</script>';
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$listado, 'cliente'=>$rowGantt["empresa"], 
            'fecha'=>$general->muestraFechaHora($rowGantt["fecha"]), 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);