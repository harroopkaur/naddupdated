<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_asignacion.php");
require_once($GLOBALS["app_root"] . "/clases/paginacion.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $asignacion = new asignacion();
                     
            $pagina = 1;
            if(isset($_POST["pagina"]) && filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false){
                $pagina = $_POST["pagina"];
            }
            
            $nombre = "";
            if(isset($_POST["nombre"])){
                $nombre = $general->get_escape($_POST["nombre"]);
            }
            
            $inicio = ($pagina * $general->limit_paginacion) - $general->limit_paginacion;
            
            $tabla = $asignacion->listar_asignacion($nombre, $inicio);
            $totalAsignacion =  $asignacion->totalAsignacion($nombre);
            
            $listado = "";
            $i = 0;
            foreach($tabla as $row){
                $listado .= '<tr class="pointer" onclick="seleccionarAsignacion(' . $i . ')">'
                    . '<td style="width:50px;"><input type="checkbox" id="asig'.$i.'" name="asig[]" value="' . $row["id"] . '*' . $row["nombre"] . '" onclick="seleccionarAsignacion(' . $i . ')"></td>'
                    . '<td colspan="2">' . $row["nombre"] . '</td>'
                . '</tr>';
                $i++;
            }
            
            $paginacion = new Paginacion($totalAsignacion, $pagina, "nuevaPagina");
                   
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'listado'=>$listado, 'paginacion'=>$paginacion->get_pag(),
            'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);