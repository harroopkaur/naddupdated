<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_gantt.php");
require_once($GLOBALS["app_root"] . "/clases/clase_ganttDetalle.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $gantt = new gantt();
            $ganttDetalle = new ganttDetalle();
            
            $id = 2;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["id"];
            }  

            $rowGantt = $gantt->datosGantt($id);
            $row = $ganttDetalle->datosGanttDetalle($id);
            
            $listado = "";
            $i = 0;
            $totalPropuesto = 0;
            
            $listado .= '<tr>
                    <th class="text-center">Levantamiento</th>
                    <th class="text-center">L</th>
                    <td class="text-center"><input type="text" id="fechaLEdit1" name="fechaLEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaLEdit1\', 1);" value="';
                        if($row["L1"] != ""){
                            $listado .= $general->muestrafecha($row["L1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaLEdit2" name="fechaLEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaLEdit2\', 2);" value="';
                        if($row["L2"] != ""){
                            $listado .= $general->muestrafecha($row["L2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaLEdit3" name="fechaLEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaLEdit3\', 3);" value="';
                        if($row["L3"] != ""){
                            $listado .= $general->muestrafecha($row["L3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaLEdit4" name="fechaLEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaLEdit4\', 4);" value="';
                        if($row["L4"] != ""){
                            $listado .= $general->muestrafecha($row["L4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Troubleshooting</th>
                    <th class="text-center">TS</th>
                    <td class="text-center"><input type="text" id="fechaTSEdit1" name="fechaTSEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTSEdit1\', 1);" value="';
                        if($row["TS1"] != ""){
                            $listado .= $general->muestrafecha($row["TS1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTSEdit2" name="fechaTSEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTSEdit2\', 2);" value="';
                        if($row["TS2"] != ""){
                            $listado .= $general->muestrafecha($row["TS2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTSEdit3" name="fechaTSEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTSEdit3\', 3);" value="';
                        if($row["TS3"] != ""){
                            $listado .= $general->muestrafecha($row["TS3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTSEdit4" name="fechaTSEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTSEdit4\', 4);" value="';
                        if($row["TS4"] != ""){
                            $listado .= $general->muestrafecha($row["TS4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Reporte Servidores</th>
                    <th class="text-center">RS</th>
                    <td class="text-center"><input type="text" id="fechaRSEdit1" name="fechaRSEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRSEdit1\', 1);" value="';
                        if($row["RS1"] != ""){
                            $listado .= $general->muestrafecha($row["RS1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRSEdit2" name="fechaRSEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRSEdit2\', 2);" value="';
                        if($row["RS2"] != ""){
                            $listado .= $general->muestrafecha($row["RS2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRSEdit3" name="fechaRSEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRSEdit3\', 3);" value="';
                        if($row["RS3"] != ""){
                            $listado .= $general->muestrafecha($row["RS3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRSEdit4" name="fechaRSEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRSEdit4\', 4);" value="';
                        if($row["RS4"] != ""){
                            $listado .= $general->muestrafecha($row["RS4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Reporte PCs</th>
                    <th class="text-center">RP</th>
                    <td class="text-center"><input type="text" id="fechaRPEdit1" name="fechaRPEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRPEdit1\', 1);" value="';
                        if($row["RP1"] != ""){
                            $listado .= $general->muestrafecha($row["RP1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRPEdit2" name="fechaRPEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRPEdit2\', 2);" value="';
                        if($row["RP2"] != ""){
                            $listado .= $general->muestrafecha($row["RP2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRPEdit3" name="fechaRPEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRPEdit3\', 3);" value="';
                        if($row["RP3"] != ""){
                            $listado .= $general->muestrafecha($row["RP3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRPEdit4" name="fechaRPEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRPEdit4\', 4);" value="';
                        if($row["RP4"] != ""){
                            $listado .= $general->muestrafecha($row["RP4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Otimización</th>
                    <th class="text-center">Opt</th>
                    <td class="text-center"><input type="text" id="fechaOptEdit1" name="fechaOptEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaOptEdit1\', 1);" value="';
                        if($row["Opt1"] != ""){
                            $listado .= $general->muestrafecha($row["Opt1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaOptEdit2" name="fechaOptEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaOptEdit2\', 2);" value="';
                        if($row["Opt2"] != ""){
                            $listado .= $general->muestrafecha($row["Opt2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaOptEdit3" name="fechaOptEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaOptEdit3\', 3);" value="';
                        if($row["Opt3"] != ""){
                            $listado .= $general->muestrafecha($row["Opt3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaOptEdit4" name="fechaOptEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaOptEdit4\', 4);" value="';
                        if($row["Opt4"] != ""){
                            $listado .= $general->muestrafecha($row["Opt4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Mitigación</th>
                    <th class="text-center">Mit</th>
                    <td class="text-center"><input type="text" id="fechaMitEdit1" name="fechaMitEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaMitEdit1\', 1);" value="';
                        if($row["Mit1"] != ""){
                            $listado .= $general->muestrafecha($row["Mit1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaMitEdit2" name="fechaMitEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaMitEdit2\', 2);" value="';
                        if($row["Mit2"] != ""){
                            $listado .= $general->muestrafecha($row["Mit2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaMitEdit3" name="fechaMitEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaMitEdit3\', 3);" value="';
                        if($row["Mit3"] != ""){
                            $listado .= $general->muestrafecha($row["Mit3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaMitEdit4" name="fechaMitEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaMitEdit4\', 4);" value="';
                        if($row["Mit4"] != ""){
                            $listado .= $general->muestrafecha($row["Mit4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Renovación</th>
                    <th class="text-center">Reno</th>
                    <td class="text-center"><input type="text" id="fechaRenoEdit1" name="fechaRenoEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRenoEdit1\', 1);" value="';
                        if($row["Reno1"] != ""){
                            $listado .= $general->muestrafecha($row["Reno1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRenoEdit2" name="fechaRenoEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRenoEdit2\', 2);" value="';
                        if($row["Reno2"] != ""){
                            $listado .= $general->muestrafecha($row["Reno2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRenoEdit3" name="fechaRenoEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRenoEdit3\', 3);" value="';
                        if($row["Reno3"] != ""){
                            $listado .= $general->muestrafecha($row["Reno3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRenoEdit4" name="fechaRenoEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRenoEdit4\', 4);" value="';
                        if($row["Reno4"] != ""){
                            $listado .= $general->muestrafecha($row["Reno4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">True UP</th>
                    <th class="text-center">TU</th>
                    <td class="text-center"><input type="text" id="fechaTUEdit1" name="fechaTUEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTUEdit1\', 1);" value="';
                        if($row["TU1"] != ""){
                            $listado .= $general->muestrafecha($row["TU1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTUEdit2" name="fechaTUEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTUEdit2\', 2);" value="';
                        if($row["TU2"] != ""){
                            $listado .= $general->muestrafecha($row["TU2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTUEdit3" name="fechaTUEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTUEdit3\', 3);" value="';
                        if($row["TU3"] != ""){
                            $listado .= $general->muestrafecha($row["TU3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTUEdit4" name="fechaTUEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaTUEdit4\', 4);" value="';
                        if($row["TU4"] != ""){
                            $listado .= $general->muestrafecha($row["TU4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Revisión</th>
                    <th class="text-center">Rev</th>
                    <td class="text-center"><input type="text" id="fechaRevEdit1" name="fechaRevEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRevEdit1\', 1);" value="';
                        if($row["Rev1"] != ""){
                            $listado .= $general->muestrafecha($row["Rev1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRevEdit2" name="fechaRevEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRevEdit2\', 2);" value="';
                        if($row["Rev2"] != ""){
                            $listado .= $general->muestrafecha($row["Rev2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRevEdit3" name="fechaRevEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRevEdit3\', 3);" value="';
                        if($row["Rev3"] != ""){
                            $listado .= $general->muestrafecha($row["Rev3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRevEdit4" name="fechaRevEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaRevEdit4\', 4);" value="';
                        if($row["Rev4"] != ""){
                            $listado .= $general->muestrafecha($row["Rev4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>

                <tr>
                    <th class="text-center">Presupuesto</th>
                    <th class="text-center">P</th>
                    <td class="text-center"><input type="text" id="fechaPEdit1" name="fechaPEdit1" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaPEdit1\', 1);" value="';
                        if($row["P1"] != ""){
                            $listado .= $general->muestrafecha($row["P1"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaPEdit2" name="fechaPEdit2" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaPEdit2\', 2);" value="';
                        if($row["P2"] != ""){
                            $listado .= $general->muestrafecha($row["P2"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaPEdit3" name="fechaPEdit3" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaPEdit3\', 3);" value="';
                        if($row["P3"] != ""){
                            $listado .= $general->muestrafecha($row["P3"]);
                        }
                    $listado .= '" readonly></td>
                    <td class="text-center"><input type="text" id="fechaPEdit4" name="fechaPEdit4" onclick="limpiarInput(this);" onchange="validarFechaModal(\'fechaPEdit4\', 4);" value="';
                        if($row["P4"] != ""){
                            $listado .= $general->muestrafecha($row["P4"]);
                        }
                    $listado .= '" readonly></td>
                </tr>';
                
            
            
            $listado .= '<script> '
                . '$(document).ready(function(){
                    for(i = 1; i < 5; i++){
                        $("#fechaLEdit" + i).datepicker({changeMonth: true});
                        $("#fechaTSEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRSEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRPEdit" + i).datepicker({changeMonth: true});
                        $("#fechaOptEdit" + i).datepicker({changeMonth: true});
                        $("#fechaMitEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRenoEdit" + i).datepicker({changeMonth: true});
                        $("#fechaTUEdit" + i).datepicker({changeMonth: true});
                        $("#fechaRevEdit" + i).datepicker({changeMonth: true});
                        $("#fechaPEdit" + i).datepicker({changeMonth: true});
                    } '
                . '});'
            . '</script>';
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$listado, 'cliente'=>$rowGantt["empresa"], 
            'fecha'=>$general->muestraFechaHora($rowGantt["fecha"]), 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);