<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes_dominios.php");
//require_once($GLOBALS["app_root"] . "/clases/paginacion.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $dominios = new clientes_dominios();
                     
            $pagina = 1;
            if(isset($_POST["pagina"]) && filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false){
                $pagina = $_POST["pagina"];
            }
            
            $cliente = 0;
            if(isset($_POST["cliente"]) && filter_var($_POST["cliente"]) !== false){
                $cliente = $_POST["cliente"];
            }
            
            $nombre = "";
            if(isset($_POST["nombre"])){
                $nombre = $general->get_escape($_POST["nombre"]);
            }
            
            $inicio = ($pagina * $general->limit_paginacion) - $general->limit_paginacion;
            
            $tabla = $dominios->listar_dominios($cliente, $nombre, $inicio);
            
            $listado = "";
            $i = 0;
            foreach($tabla as $row){
                $listado .= "<tr id=fila" . $i . ">
                        <td><input type='hidden' id='idDominio" . $i . "' name='idDominio[]' value='" . $row["id"] . "'>" . $row["dominio"] . "</td>
                        <td><a href='#' onclick='eliminar(" . $row["id"] . ")'><img src='" . $GLOBALS["domain_root"] . "/imagenes/png/glyphicons_016_bin.png' width='20' height='28' border='0' alt='Eliminar' title='Eliminar' /></a></td>
                    </tr>";
                $i++;
            }
    
            //$paginacion = new Paginacion($totalDominios, $pagina, "nuevaPagina");
                   
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'listado'=>$listado, 'resultado'=>true));
            //$array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'listado'=>$listado, 'paginacion'=>$paginacion->get_pag(),
            //'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);