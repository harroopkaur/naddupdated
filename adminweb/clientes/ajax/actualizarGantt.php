<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_ganttDetalle.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $ganttDetalle = new ganttDetalle();
            $general = new General();
            $id = 0;
            if(isset($_POST["idActualizar"]) && filter_var($_POST["idActualizar"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["idActualizar"];
            }
            
            $fechaL = array(null, null, null, null);
            $fechaTS = array(null, null, null, null);
            $fechaRS = array(null, null, null, null);
            $fechaRP = array(null, null, null, null);
            $fechaOpt = array(null, null, null, null);
            $fechaMit = array(null, null, null, null);
            $fechaReno = array(null, null, null, null);
            $fechaTU = array(null, null, null, null);
            $fechaRev = array(null, null, null, null);
            $fechaP = array(null, null, null, null);

            for($i = 1; $i < 5; $i++) {
                if(isset($_POST["fechaLEdit" . $i]) && $general->validateDate($_POST["fechaLEdit" . $i], 'd/m/Y')){
                    $fechaL[($i - 1)] = $general->reordenarFecha($_POST["fechaLEdit" . $i], "/");
                }

                if(isset($_POST["fechaTSEdit" . $i]) && $general->validateDate($_POST["fechaTSEdit" . $i], 'd/m/Y')){
                    $fechaTS[($i - 1)] = $general->reordenarFecha($_POST["fechaTSEdit" . $i], "/");
                }

                if(isset($_POST["fechaRSEdit" . $i]) && $general->validateDate($_POST["fechaRSEdit" . $i], 'd/m/Y')){
                    $fechaRS[($i - 1)] = $general->reordenarFecha($_POST["fechaRSEdit" . $i], "/");
                }

                if(isset($_POST["fechaRPEdit" . $i]) && $general->validateDate($_POST["fechaRPEdit" . $i], 'd/m/Y')){
                    $fechaRP[($i - 1)] = $general->reordenarFecha($_POST["fechaRPEdit" . $i], '/');
                }

                if(isset($_POST["fechaOptEdit" . $i]) && $general->validateDate($_POST["fechaOptEdit" . $i], 'd/m/Y')){
                    $fechaOpt[($i - 1)] = $general->reordenarFecha($_POST["fechaOptEdit" . $i], '/');
                }

                if(isset($_POST["fechaMitEdit" . $i]) && $general->validateDate($_POST["fechaMitEdit" . $i], 'd/m/Y')){
                    $fechaMit[($i - 1)] = $general->reordenarFecha($_POST["fechaMitEdit" . $i], '/');
                }

                if(isset($_POST["fechaRenoEdit" . $i]) && $general->validateDate($_POST["fechaRenoEdit" . $i], 'd/m/Y')){
                    $fechaReno[($i - 1)] = $general->reordenarFecha($_POST["fechaRenoEdit" . $i], '/');
                }

                if(isset($_POST["fechaTUEdit" . $i]) && $general->validateDate($_POST["fechaTUEdit" . $i], 'd/m/Y')){
                    $fechaTU[($i - 1)] = $general->reordenarFecha($_POST["fechaTUEdit" . $i], '/');
                }

                if(isset($_POST["fechaRevEdit" . $i]) && $general->validateDate($_POST["fechaRevEdit" . $i], 'd/m/Y')){
                    $fechaRev[($i - 1)] = $general->reordenarFecha($_POST["fechaRevEdit" . $i], '/');
                }

                if(isset($_POST["fechaPEdit" . $i]) && $general->validateDate($_POST["fechaPEdit" . $i], 'd/m/Y')){
                    $fechaP[($i - 1)] = $general->reordenarFecha($_POST["fechaPEdit" . $i], '/');
                }
            }

            $result = 0;
            if($ganttDetalle->actualizar($id, $fechaL[0], $fechaL[1], $fechaL[2], $fechaL[3], $fechaTS[0], 
            $fechaTS[1], $fechaTS[2], $fechaTS[3], $fechaRS[0], $fechaRS[1], $fechaRS[2], $fechaRS[3], 
            $fechaRP[0], $fechaRP[1], $fechaRP[2], $fechaRP[3], $fechaOpt[0], $fechaOpt[1], $fechaOpt[2], 
            $fechaOpt[3], $fechaMit[0], $fechaMit[1], $fechaMit[2], $fechaMit[3], $fechaReno[0], $fechaReno[1], 
            $fechaReno[2], $fechaReno[3], $fechaTU[0], $fechaTU[1], $fechaTU[2], $fechaTU[3], $fechaRev[0], 
            $fechaRev[1] ,$fechaRev[2], $fechaRev[3], $fechaP[0], $fechaP[1], $fechaP[2], $fechaP[3])){
                $result = 1;
            }
                   
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);