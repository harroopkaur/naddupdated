<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_fabricantes_activos.php");
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            if($nuevo_middleware->compararAjax($_POST["token"])){
                $fabricantesActivos = new fabricantesActivos();

                if(isset($_POST['cliente']) && filter_var($_POST['cliente'], FILTER_VALIDATE_INT) !== false && isset($_POST['marca']) 
                && filter_var($_POST['marca'], FILTER_VALIDATE_INT) !== false && isset($_POST['fabricante']) 
                && filter_var($_POST['fabricante'], FILTER_VALIDATE_INT) !== false && $_POST['cliente'] > 0 && ($_POST['marca'] == 0 || $_POST['marca'] == 1) 
                && $_POST['fabricante'] > 0){

                    $aprobado = true;
                    $id_user = $_POST['cliente'];
                    $marca   = $_POST['marca'];
                    $fabricante = $_POST['fabricante'];

                    ///***verificar los modulos insertado o elminados****////
                    if($marca==0){
                        $fabricantesActivos->eliminarFabricanteCliente($id_user, $fabricante);
                    } 
                    else {
                        if($fabricantesActivos->existeFabricanteCliente($id_user, $fabricante) > 0){
                            $fabricantesActivos->activarFabricanteCliente($id_user, $fabricante);
                        } else{
                            $fabricantesActivos->agregarFabricanteCliente($id_user, $fabricante);
                        }
                    }
                }
                
                $listaFabActivos = $fabricantesActivos->listar_fabricantes($id_user); 
                $i = 0;
                $tabla = "";
                foreach($listaFabActivos as $row){
                    $tabla .= '<tr>
                        <th>' . $row["nombre"] . '<input type="hidden" id="fab' . $i . '" name="fab[]" value="' . $row["idFabricante"] . '"></th>
                        <td class="text-center"><input type="text" id="aniversario' . $i . '" name="aniversario[]" readonly></td>
                        <td class="text-center"><input type="text" id="renovacion' . $i . '" name="renovacion[]" readonly></td>
                        <td class="text-center"><input type="text" id="pago' . $i . '" name="pago[]"></td>
                    </tr>';
                    
                    $i++;
                }
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);