<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $clientes = new Clientes();
            
            $clienteDominio = 0;
            if(isset($_POST["cliente"]) && filter_var($_POST["cliente"]) !== false){
                $clienteDominio = $_POST["cliente"];
            }
            
            $cantidadCorreo = 0;
            if(isset($_POST["cantCorreo"]) && filter_var($_POST["cantCorreo"], FILTER_VALIDATE_INT) !== false){
                $cantidadCorreo = $_POST["cantCorreo"];
            }
            
            $cantidadDespliegue = 0;
            if(isset($_POST["cantidadDespliegue"]) && filter_var($_POST["cantidadDespliegue"], FILTER_VALIDATE_INT) !== false){
                $cantidadDespliegue = $_POST["cantidadDespliegue"];
            }
            
            $result = $clientes->actualizarCantCorreoCantDespliegue($clienteDominio, $cantidadCorreo, $cantidadDespliegue);
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);