<?php
if ($exito == true) { ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/vigencia.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php
}        
?>


<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Modificar Datos del Cliente</span></legend>
    
    <form id="form1" name="form1" method="post" enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id_user ?>">
        <?php $validator->print_script(); ?>
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
            echo $clientes2->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr valign="middle">
                <td width="10%" align="left" valign="top">Empresa:</td>
                <td width="40%"  align="left"><input name="empresa" id="empresa" type="text" value="<?= $clientes->empresa ?>" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div>
                </td>

            </tr>
            
            <tr>
                <td width="" align="left" valign="top">E-mail:</td>
                <td align="left"><input name="email" id="email" type="text" value="<?= $clientes->correo ?>" size="30" maxlength="70" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div></td>


            </tr>

            <tr>
                <td width="" align="left" valign="top">Telefono:</td>
                <td align="left"><input name="telefono" id="telefono" type="text" value="<?= $clientes->telefono ?>" size="30" maxlength="20"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_telefono") ?></font></div></td>

            </tr>
            
            <tr>
                <td width="" align="left" valign="top">Pais:</td>
                <td align="left"><select name="pais" id="pais">
                        <option value="" >--Seleccione--</option>
                        <?php
                        foreach ($lista_p as $reg_p) {
                        ?>
                            <option value="<?= $reg_p["id"] ?>"  <?php if($reg_p["id"]==$clientes->pais){ echo 'selected';  }  ?>><?= $reg_p["nombre"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pais") ?></font></div></td>

            </tr>

            <tr>
                <td width="" align="left" valign="top">Nivel de Servicio:</td>
                <td align="left"><select name="nivelServicio" id="nivelServicio">
                        <option value="" >--Seleccione--</option>
                        <?php
                        foreach ($tablaNivelServicio as $reg_p) {
                        ?>
                            <option value="<?= $reg_p["id"] ?>"  <?php if($reg_p["id"]==$clientes->nivelServicio){ echo 'selected';  }  ?>><?= $reg_p["descripcion"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nivelServicio") ?></font></div></td>

            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>

            </tr>
        </table>
    </form>
</fieldset>


<script>
    $(document).ready(function(){
        $("#empresa").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/ajax/verificarEmpresa.php", { empresa : $("#empresa").val(), id : $("#id").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe la empresa', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#empresa").val("");
                            $("#empresa").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });

        $("#email").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/ajax/verificarCorreo.php", { email : $("#email").val(), id : $("#id").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe el correo', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
    });
</script>