<?php 
if($exito == true){  ?>
    <script type="text/javascript">
        $.alert.open('Registro anulado con éxito', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('Registro no ha sido anulado', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php
}
?>