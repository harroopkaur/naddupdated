<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/certificado.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo cargar el archivo', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/certificado.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
}
else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo insertar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/certificado.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
}
?>          
    
<form id="form1" name="form1" method="post" enctype="multipart/form-data"> 
    <input type="hidden" id="insertar" name="insertar" value="1">
    <input type="hidden" id="id" name="id" value="<?= $id_user ?>">
    <input type="hidden" id="token" name="token">
    
    <?php $validator->print_script(); ?>
    
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Agregar Certificado Actual</span></legend>

        <div class="contenedorCot">
            <div class="titulo"><p>Descripción:</p></div>
            <div class="input"><input type="text" id="descrip" name="descrip" style="width:100%" maxlength="70"></div>
            
            <div class="titulo">&nbsp;</div>
            <div class="input">
                <div class="error_prog">
                    <font color="#FF0000"><?= $validator->show("msj_descripcion") ?></font>
                </div>
            </div>

            <div class="titulo" style="margin-top:-15px;"><p>Certificado:</p></div>
            <div class="input">
                <input type="text" class="url_file boton4" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                <input type="file" class="archivo" name="archivo" id="archivo" accept=".pdf">
                <input type="button" id="buscar" class="boton" value="Buscar">
            </div>
            
            <div class="titulo">&nbsp;</div>
            <div class="input">
                <div class="error_prog">
                    <font color="#FF0000"><?= $validator->show("msj_archivo") ?></font>
                </div>
            </div>
        </div>

        <div style="width:250px; margin:0 auto;">
            <input name="cancelar" type="button" id="cancelar" value="CANCELAR" class="boton" style="display:inline-block"/>
            <input name="guardar" type="button" id="guardar" value="GUARDAR" class="boton" onclick="validate();" style="display:inline-block"/>
        </div>
    </fieldset>
</form>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Historial de Certificados</span></legend>

    <table style="width:100%;" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
        <thead>
            <tr>
                <th class="text-center" style="width:60%">Descripción</th> 
                <th class="text-center" style="width:20%">Fecha</th> 
                <th class="text-center" style="width:50px;">Archivo</th> 
                <th class="text-center" style="width:50px;">Eliminar</th> 
            </tr>
        </thead>
        <tbody id="contTabla">
            <?php 
            $i = 0;
            foreach($listado as $row){
            ?>
                <tr>
                    <td><?= $row["descripcion"] ?></td>
                    <td><?= $general->muestraFechaHora($row["fecha"]) ?></td>
                    <td class="text-center"><a href='#' onclick='descargar("<?= $row['ruta'] ?>")'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_181_download_alt.png' border='0' alt='Descargar' title='Descargar' /></a></td>
                    <td class="text-center"><a href='#' onclick='eliminar(<?= $row['id'] ?>)'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png' border='0' alt='Eliminar' title='Eliminar' /></a></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
    
    <div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>
</fieldset>

<script>
    $(document).ready(function(){
        $("#buscar").click(function(){
            $("#archivo").click();
        });
        
        $("#archivo").change(function(e){
            if(addArchivo(e) === false){
                $("#archivo").val("");
                $("#archivo").replaceWith($("#archivo").clone(true));
            }
        });
        
        $("#cancelar").click(function(){
            $("#archivo").val("");
            $("#archivo").replaceWith($("#archivo").clone(true));
            $("#descrip").val("");
            $("#url_file").val("Nombre del Archivo...");
        });
    }); 
    
    function addArchivo(e){
        file = e.target.files[0]; 
        if (!file.type.match("application/pdf")){
            $.alert.open('alert', 'El archivo a cargar debe ser .pdf', {'Aceptar': 'Aceptar'}, function() {
            });
            return false;
        }
        else if(parseInt(file.size) / 1024 > 5120 ){
            $.alert.open('alert', 'El tamaño permitido es de 5 MB', {'Aceptar': 'Aceptar'}, function() {
            });
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $("#url_file").val("");
            $("#url_file").val(file.name);
            return true;
        }
    }

    function fileOnload(e) {
        result=e.target.result;
    }
    
    function descargar(archivo){
        window.open("<?= $GLOBALS["domain_root"] ?>/adminweb/clientes/certificados/" + $("#id").val() + "/" + archivo);
    }
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el certificado?', {'Aceptar': 'Aceptar', 'Cancelar': 'Cancelar'}, function(button) {
            if(button === "Aceptar"){
                $.post("ajax/eliminarCertificado.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
                    <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                    if(data[0].result === true){
                        $.alert.open('info', 'Registro Eliminado con éxito', {'Aceptar': 'Aceptar'}, function() {
                            location.href = "certificado.php?id=" + $("#id").val();
                        });
                    }
                    else{
                        $.alert.open('alert', 'No se pudo eliminar el registro', {'Aceptar': 'Aceptar'}, function() {
                            location.href = "certificado.php?id=" + $("#id").val();
                        });
                    }
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
    }
</script>