<?php
if ($exito == 1) { ?>
    <script type="text/javascript">
        $.alert.open('info', 'Info', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'});
    </script>
<?php
}        
?>
<style>
    .rich-text-editor-view code {font-family:'Courier New',Courier,'Nimbus Mono L',Monospace}
</style>
<link  href="<?= $GLOBALS["domain_root"] ?>/adminweb/css/rich-text-editor.min.css" rel="stylesheet">
<link  href="<?= $GLOBALS["domain_root"] ?>/adminweb/css/estilos.css" rel="stylesheet">

<script src="<?= $GLOBALS["domain_root"] ?>/adminweb/js/rich-text-editor.min.js"></script>

<div class="agrupar">
    <ul class="pestania">
        <li><a href="#tab1"><span>Personalización de Correos NADD</span></a></li>
        <li><a href="#tab2"><span>Dominios Permitidos</span></a></li>
        <li><a href="#tab3"><span>Plan Adquirido</span></a></li>
    </ul>
</div>

<div class="secciones">
    <fieldset class="fieldset" id="tab1">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Personalización de Correos NADD</span></legend>

        <form id="form1" name="form1" method="post" enctype="multipart/form-data" action="nadd.php">
            <input type="hidden" name="insertar" id="insertar" value="1" />
            <input type="hidden" name="id" id="id" value="<?= $id_user ?>">

            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
                <tr>
                    <td width="25%" align="left">  Img. Header  </td>
                    <td width="75%">    <input type="file"
                                               id="imgheader" name="imgheader"
                                               accept="image/png, image/jpeg, image/jpg, image/bmp" />  </td>
                </tr>
                <tr>
                    <td width="25%" align="left">  Img. Footer  </td>
                    <td width="75%">    <input type="file"
                                               id="imgfooter" name="imgfooter"
                                               accept="image/png, image/jpeg, image/jpg, image/bmp" />  </td>
                </tr>
                <tr>
                    <td width="25%" align="left">  Html Header </td>
                    <td width="75%">  <textarea name="htmlheader" id="htmlheader"><?= $clientes->html_header ?></textarea>  </td>
                </tr>
                <tr>
                    <td width="25%" align="left">  Html Footer  </td>
                    <td width="75%">  <textarea name="htmlfooter" id="htmlfooter"><?= $clientes->html_footer ?></textarea>  </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input name="modificar" type="button" id="modificar" value="MODIFICAR"  class="boton" />
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
    
    <fieldset class="fieldset" id="tab2">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Dominios Permitidos</span></legend>
            <table style="width:400px;" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
                <thead>
                    <tr>
                        <th>Dominios</th> 
                        <th style="width:50px;">&nbsp;</th> 
                    </tr>
                </thead>
                <tbody id="contTablaDominios">
                    
                </tbody>
            </table>
            
            <br>
            
            <table class="text-center" style="margin: 0 auto;">
                <tr>
                    <td colspan='2' align='center'>
                        <input name='agregar' type='button' id='agregar' value='AGREGAR'  class='boton' />
                    </td>
                </tr>
            </table>
    </fieldset>
    
    <fieldset class="fieldset" id="tab3">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Plan Adquirido</span></legend>

        <form id="form3" name="form3" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" id="idUser" value="<?= $id_user ?>">

            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
                <tr>
                    <td width="25%" align="left">  Correos por Dominio  </td>
                    <td width="75%">    
                        <select id="correosDominio" name="correosDominio" style="width: 70px;">
                            <option>0</option>
                            <option>200</option>
                            <option>400</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="25%" align="left">  Despliegue por Dominio  </td>
                    <td width="75%"><select id="despliegueDominio" name="despliegueDominio" style="width: 70px;">
                            <option>0</option>
                            <option>400</option>
                            <option>1000</option>
                        </select>
                    </td>
                </tr>
            
                <tr>
                    <td colspan="2" align="center">
                        <input name="modificarDespliegue" type="button" id="modificarDespliegue" value="MODIFICAR"  class="boton" />
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
</div>

<form id="form2" name="form2" method="post" enctype="multipart/form-data">
    <input type="hidden" name="agr" id="agr" value="1" />
    <input type="hidden" name="id" id="id_usuario" value="<?= $id_user ?>">

    <div class="modal-personal" id="modal-dominios">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Crear <span style="color:#06B6FF; display:inline">Dominios</span></h1>
        </div>
        <div class="modal-personal-body">
            <table style="width:400px; margin:0 auto; margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap2">
                    <tr>
                        <th class="text-center">Dominio</th> 
                        <td class="text-center"><input type="text" id="nombre" name="nombre" maxlength="80" style="width:100%;"></td> 
                    </tr>
                </tbody>
            </table>               
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salir">Salir</div>
            <div class="boton1 botones_m2" id="guardarDominio" style="float:right;">Guardar</div>
        </div>
    </div> 
</form>

<?php require($GLOBALS["app_root"] . "/adminweb/clientes/plantillas/mainNADD.php"); ?>

<script>

    var editorh = new RTE(document.getElementById('htmlheader'));
    var editorf = new RTE(document.getElementById('htmlfooter'));


    font_sizes = [
        false, // inherit
        125,
        150,
        175,
        200,
        250,
        275,
        300,
        25,
        50,
        75
    ],
        font_sizes_count = font_sizes.length,
        font_sizes_i = 0,

        font_faces = [
            false, // inherit
            'arial,sans-serif',
            'times new roman,serif',
            'georgia,serif',
            'verdana,sans-serif',
            'tahoma,sans-serif',
            'trebuchet ms,sans-serif',
            'courier new,courier,monospace'
        ],
        font_faces_count = font_faces.length,
        font_faces_i = 0;

    editorh.t('color', ['Text Color', 'svg:M6.406 8h3.188l-1.594-4.219zM7.344 2h1.313l3.656 9.344h-1.5l-0.719-2h-4.188l-0.75 2h-1.5zM0 13.344h16v2.656h-16v-2.656z'], function() {
        var previous_color = editorh.e('span'),
            color = previous_color || editorh.w('span', 1); // force wrap
        if (editorh.is.dialog) {
            editorh.d.x(true); // cancel
            color = editorh.e('span');
            if (color && !color.getAttribute('style')) {
                editorh.w('span', 0); // force unwrap
            }
        } else {
            editorh.d('#FFFFFF', color && color.style.color || "", function(e, $, input) {
                color.style.color = input.value;
                if (!input.value && !color.getAttribute('style')) {
                    editorh.w('span', 0); // force unwrap
                }
            });
        }
    }, -1);

    editorh.t('background', ['Text Background Color', 'svg:M0 13.344h16v2.656h-16v-2.656zM12.656 7.656c0 0 1.344 1.469 1.344 2.344 0 0.719-0.625 1.344-1.344 1.344s-1.313-0.625-1.313-1.344c0-0.875 1.313-2.344 1.313-2.344zM3.469 6.656h6.406l-3.219-3.188zM11.031 5.969c0.406 0.406 0.406 1.031 0 1.406l-3.656 3.656c-0.188 0.188-0.469 0.313-0.719 0.313s-0.5-0.125-0.688-0.313l-3.688-3.656c-0.406-0.375-0.406-1 0-1.406l3.438-3.438-1.594-1.594 0.969-0.938z'], function() {
        var previous_color = editorh.e('span'),
            color = previous_color || editorh.w('span', 1); // force wrap
        if (editorh.is.dialog) {
            editorh.d.x(true); // cancel
            color = editorh.e('span');
            if (color && !color.getAttribute('style')) {
                editorh.w('span', 0); // force unwrap
            }
        } else {
            editorh.d('#000000', color && color.style.backgroundColor || "", function(e, $, input) {
                color.style.backgroundColor = input.value;
                if (!input.value && !color.getAttribute('style')) {
                    editorh.w('span', 0); // force unwrap
                }
            });
        }
    }, -1);

    editorh.t('font', ['Font Size', 'svg:M2 8v-2h6v2h-2v4.656h-2v-4.656h-2zM6 2.656h8.656v2h-3.313v8h-2v-8h-3.344v-2z'], function() {
        var font = editorh.e('span') || editorh.w('span', 1); // force wrap
        ++font_sizes_i;
        if (font_sizes_i > font_sizes_count - 1) {
            font_sizes_i = 0;
        }
        if (font_sizes_i !== 0) {
            font.style.fontSize = font_sizes[font_sizes_i] + '%';
        } else {
            font.style.fontSize = "";
            if (!font.getAttribute('style')) {
                editorh.w('span', 0); // force unwrap
            }
        }
    }, -1);

    // font family
    editorh.t('font', ['Font Family', 'svg:M10.625 12.344h1.406l-3.406-8.688h-1.25l-3.406 8.688h1.406l0.75-2h3.75zM13.344 1.344c0.719 0 1.313 0.594 1.313 1.313v10.688c0 0.719-0.594 1.313-1.313 1.313h-10.688c-0.719 0-1.313-0.594-1.313-1.313v-10.688c0-0.719 0.594-1.313 1.313-1.313h10.688zM6.625 9l1.375-3.688 1.375 3.688h-2.75z'], function() {
        var font = editorh.e('span') || editorh.w('span', 1); // force wrap
        ++font_faces_i;
        if (font_faces_i > font_faces_count - 1) {
            font_faces_i = 0;
        }
        if (font_faces_i !== 0) {
            font.style.fontFamily = font_faces[font_faces_i];
        } else {
            font.style.fontFamily = "";
            if (!font.getAttribute('style')) {
                editorh.w('span', 0); // force unwrap
            }
        }
    }, -1);
    editorf.t('color', ['Text Color', 'svg:M6.406 8h3.188l-1.594-4.219zM7.344 2h1.313l3.656 9.344h-1.5l-0.719-2h-4.188l-0.75 2h-1.5zM0 13.344h16v2.656h-16v-2.656z'], function() {
            var previous_color = editorf.e('span'),
                color = previous_color || editorf.w('span', 1); // force wrap
            if (editorf.is.dialog) {
                editorf.d.x(true); // cancel
                color = editorf.e('span');
                if (color && !color.getAttribute('style')) {
                    editorf.w('span', 0); // force unwrap
                }
            } else {
                editorf.d('#FFFFFF', color && color.style.color || "", function(e, $, input) {
                    color.style.color = input.value;
                    if (!input.value && !color.getAttribute('style')) {
                        editorf.w('span', 0); // force unwrap
                    }
                });
            }
        }, -1);

    editorf.t('background', ['Text Background Color', 'svg:M0 13.344h16v2.656h-16v-2.656zM12.656 7.656c0 0 1.344 1.469 1.344 2.344 0 0.719-0.625 1.344-1.344 1.344s-1.313-0.625-1.313-1.344c0-0.875 1.313-2.344 1.313-2.344zM3.469 6.656h6.406l-3.219-3.188zM11.031 5.969c0.406 0.406 0.406 1.031 0 1.406l-3.656 3.656c-0.188 0.188-0.469 0.313-0.719 0.313s-0.5-0.125-0.688-0.313l-3.688-3.656c-0.406-0.375-0.406-1 0-1.406l3.438-3.438-1.594-1.594 0.969-0.938z'], function() {
        var previous_color = editorf.e('span'),
            color = previous_color || editorf.w('span', 1); // force wrap
        if (editorf.is.dialog) {
            editorf.d.x(true); // cancel
            color = editorf.e('span');
            if (color && !color.getAttribute('style')) {
                editorf.w('span', 0); // force unwrap
            }
        } else {
            editorf.d('#000000', color && color.style.backgroundColor || "", function(e, $, input) {
                color.style.backgroundColor = input.value;
                if (!input.value && !color.getAttribute('style')) {
                    editorf.w('span', 0); // force unwrap
                }
            });
        }
    }, -1);

    editorf.t('font', ['Font Size', 'svg:M2 8v-2h6v2h-2v4.656h-2v-4.656h-2zM6 2.656h8.656v2h-3.313v8h-2v-8h-3.344v-2z'], function() {
        var font = editorf.e('span') || editorf.w('span', 1); // force wrap
        ++font_sizes_i;
        if (font_sizes_i > font_sizes_count - 1) {
            font_sizes_i = 0;
        }
        if (font_sizes_i !== 0) {
            font.style.fontSize = font_sizes[font_sizes_i] + '%';
        } else {
            font.style.fontSize = "";
            if (!font.getAttribute('style')) {
                editorf.w('span', 0); // force unwrap
            }
        }
    }, -1);

    // font family
    editorf.t('font', ['Font Family', 'svg:M10.625 12.344h1.406l-3.406-8.688h-1.25l-3.406 8.688h1.406l0.75-2h3.75zM13.344 1.344c0.719 0 1.313 0.594 1.313 1.313v10.688c0 0.719-0.594 1.313-1.313 1.313h-10.688c-0.719 0-1.313-0.594-1.313-1.313v-10.688c0-0.719 0.594-1.313 1.313-1.313h10.688zM6.625 9l1.375-3.688 1.375 3.688h-2.75z'], function() {
        var font = editorf.e('span') || editorf.w('span', 1); // force wrap
        ++font_faces_i;
        if (font_faces_i > font_faces_count - 1) {
            font_faces_i = 0;
        }
        if (font_faces_i !== 0) {
            font.style.fontFamily = font_faces[font_faces_i];
        } else {
            font.style.fontFamily = "";
            if (!font.getAttribute('style')) {
                editorf.w('span', 0); // force unwrap
            }
        }
    }, -1);
</script>
