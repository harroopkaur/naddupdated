<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/gantt.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo insertar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/gantt.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
}
else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo insertar el detalle del registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/gantt.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php
}
?>          
    
<form id="form1" name="form1" method="post" enctype="multipart/form-data"> 
    <input type="hidden" id="insertar" name="insertar" value="1">
    <input type="hidden" id="id" name="id" value="<?= $id_user ?>">
    <input type="hidden" id="token" name="token">
    
    <?php $validator->print_script(); ?>
    
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Alimentación del Gantt del Cliente</span></legend>
        
        <table class="tablap">
            <thead>
                <tr>
                    <th colspan="10" class="text-center">Leyenda</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center">L</th>
                    <td class="text-center">Levantamiento</td>
                
                    <th class="text-center">TS</th>
                    <td class="text-center">Troubleshooting</td>
                
                    <th class="text-center">RS</th>
                    <td class="text-center">Reporte Servidores</td>
                
                    <th class="text-center">RP</th>
                    <td class="text-center">Reporte PCs</td>
                
                    <th class="text-center">Opt</th>
                    <td class="text-center">Optimización</td>
                </tr>
                
                <tr>
                    <th class="text-center">Mit</th>
                    <td class="text-center">Mitigación</td>
                
                    <th class="text-center">Reno</th>
                    <td class="text-center">Renovación</td>
                
                    <th class="text-center">TU</th>
                    <td class="text-center">True UP</td>
                
                    <th class="text-center">Rev</th>
                    <td class="text-center">Revisión</td>
                
                    <th class="text-center">P</th>
                    <td class="text-center">Presupuesto</td>                    
                </tr>
            </tbody>
        </table>
        
        <br>
        
        <div class="contenedorCot">
            <div class="titulo"><p>Descripción:</p></div>
            <div class="input"><input type="text" id="descrip" name="descrip" style="width:100%" maxlength="70"></div>

            <div class="titulo">&nbsp;</div>
            <div class="input">
                <div class="error_prog">
                    <font color="#FF0000"><?= $validator->show("msj_descripcion") ?></font>
                </div>
            </div>
        </div>
        
        <table class="tablap">
            <thead>
                <tr>
                    <th class="text-center">Fase</th>
                    <th class="text-center">Siglas</th>
                    <th class="text-center">Fecha T1</th>
                    <th class="text-center">Fecha T2</th>
                    <th class="text-center">Fecha T3</th>
                    <th class="text-center">Fecha T4</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center">Levantamiento</th>
                    <th class="text-center">L</th>
                    <td class="text-center"><input type="text" id="fechaL1" name="fechaL1" onclick="limpiarInput(this);" onchange="validarFecha('fechaL1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaL2" name="fechaL2" onclick="limpiarInput(this);" onchange="validarFecha('fechaL2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaL3" name="fechaL3" onclick="limpiarInput(this);" onchange="validarFecha('fechaL3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaL4" name="fechaL4" onclick="limpiarInput(this);" onchange="validarFecha('fechaL4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Troubleshooting</th>
                    <th class="text-center">TS</th>
                    <td class="text-center"><input type="text" id="fechaTS1" name="fechaTS1" onclick="limpiarInput(this);" onchange="validarFecha('fechaTS1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTS2" name="fechaTS2" onclick="limpiarInput(this);" onchange="validarFecha('fechaTS2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTS3" name="fechaTS3" onclick="limpiarInput(this);" onchange="validarFecha('fechaTS3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTS4" name="fechaTS4" onclick="limpiarInput(this);" onchange="validarFecha('fechaTS4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Reporte Servidores</th>
                    <th class="text-center">RS</th>
                    <td class="text-center"><input type="text" id="fechaRS1" name="fechaRS1" onclick="limpiarInput(this);" onchange="validarFecha('fechaRS1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRS2" name="fechaRS2" onclick="limpiarInput(this);" onchange="validarFecha('fechaRS2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRS3" name="fechaRS3" onclick="limpiarInput(this);" onchange="validarFecha('fechaRS3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRS4" name="fechaRS4" onclick="limpiarInput(this);" onchange="validarFecha('fechaRS4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Reporte PCs</th>
                    <th class="text-center">RP</th>
                    <td class="text-center"><input type="text" id="fechaRP1" name="fechaRP1" onclick="limpiarInput(this);" onchange="validarFecha('fechaRP1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRP2" name="fechaRP2" onclick="limpiarInput(this);" onchange="validarFecha('fechaRP2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRP3" name="fechaRP3" onclick="limpiarInput(this);" onchange="validarFecha('fechaRP3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRP4" name="fechaRP4" onclick="limpiarInput(this);" onchange="validarFecha('fechaRP4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Otimización</th>
                    <th class="text-center">Opt</th>
                    <td class="text-center"><input type="text" id="fechaOpt1" name="fechaOpt1" onclick="limpiarInput(this);" onchange="validarFecha('fechaOpt1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaOpt2" name="fechaOpt2" onclick="limpiarInput(this);" onchange="validarFecha('fechaOpt2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaOpt3" name="fechaOpt3" onclick="limpiarInput(this);" onchange="validarFecha('fechaOpt3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaOpt4" name="fechaOpt4" onclick="limpiarInput(this);" onchange="validarFecha('fechaOpt4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Mitigación</th>
                    <th class="text-center">Mit</th>
                    <td class="text-center"><input type="text" id="fechaMit1" name="fechaMit1" onclick="limpiarInput(this);" onchange="validarFecha('fechaMit1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaMit2" name="fechaMit2" onclick="limpiarInput(this);" onchange="validarFecha('fechaMit2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaMit3" name="fechaMit3" onclick="limpiarInput(this);" onchange="validarFecha('fechaMit3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaMit4" name="fechaMit4" onclick="limpiarInput(this);" onchange="validarFecha('fechaMit4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Renovación</th>
                    <th class="text-center">Reno</th>
                    <td class="text-center"><input type="text" id="fechaReno1" name="fechaReno1" onclick="limpiarInput(this);" onchange="validarFecha('fechaReno1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaReno2" name="fechaReno2" onclick="limpiarInput(this);" onchange="validarFecha('fechaReno2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaReno3" name="fechaReno3" onclick="limpiarInput(this);" onchange="validarFecha('fechaReno3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaReno4" name="fechaReno4" onclick="limpiarInput(this);" onchange="validarFecha('fechaReno4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">True UP</th>
                    <th class="text-center">TU</th>
                    <td class="text-center"><input type="text" id="fechaTU1" name="fechaTU1" onclick="limpiarInput(this);" onchange="validarFecha('fechaTU1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTU2" name="fechaTU2" onclick="limpiarInput(this);" onchange="validarFecha('fechaTU2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTU3" name="fechaTU3" onclick="limpiarInput(this);" onchange="validarFecha('fechaTU3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaTU4" name="fechaTU4" onclick="limpiarInput(this);" onchange="validarFecha('fechaTU4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Revisión</th>
                    <th class="text-center">Rev</th>
                    <td class="text-center"><input type="text" id="fechaRev1" name="fechaRev1" onclick="limpiarInput(this);" onchange="validarFecha('fechaRev1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRev2" name="fechaRev2" onclick="limpiarInput(this);" onchange="validarFecha('fechaRev2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRev3" name="fechaRev3" onclick="limpiarInput(this);" onchange="validarFecha('fechaRev3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaRev4" name="fechaRev4" onclick="limpiarInput(this);" onchange="validarFecha('fechaRev4', 4);" readonly></td>
                </tr>
                
                <tr>
                    <th class="text-center">Presupuesto</th>
                    <th class="text-center">P</th>
                    <td class="text-center"><input type="text" id="fechaP1" name="fechaP1" onclick="limpiarInput(this);" onchange="validarFecha('fechaP1', 1);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaP2" name="fechaP2" onclick="limpiarInput(this);" onchange="validarFecha('fechaP2', 2);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaP3" name="fechaP3" onclick="limpiarInput(this);" onchange="validarFecha('fechaP3', 3);" readonly></td>
                    <td class="text-center"><input type="text" id="fechaP4" name="fechaP4" onclick="limpiarInput(this);" onchange="validarFecha('fechaP4', 4);" readonly></td>
                </tr>
            </tbody>
        </table>
        
        <br class="clear">

        <div style="width:250px; margin:0 auto;">
            <input name="cancelar" type="button" id="cancelar" value="CANCELAR" class="boton" style="display:inline-block"/>
            <input name="guardar" type="button" id="guardar" value="GUARDAR" class="boton" style="display:inline-block"/>
        </div>
    </fieldset>
</form>
    
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Historial del Gantt</span></legend>

    <table style="width:100%;" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
        <thead>
            <tr>
                <th class="text-center" style="width:60%">Descripción</th> 
                <th class="text-center" style="width:20%">Fecha</th> 
                <th class="text-center" style="width:50px;">Detalle</th> 
                <th class="text-center" style="width:50px;">Editar</th> 
                <th class="text-center" style="width:50px;">Eliminar</th> 
            </tr>
        </thead>
        <tbody id="contTabla">
            <?php 
            $i = 0;
            foreach($listado as $row){
            ?>
                <tr>
                    <td><?= $row["descripcion"] ?></td>
                    <td><?= $general->muestraFechaHora($row["fecha"]) ?></td>
                    <td class="text-center"><a href='#' onclick='ver("<?= $row['id'] ?>")'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_027_search.png' border='0' alt='Ver' title='Ver' /></a></td>
                    <td class="text-center"><a href='#' onclick='actualizar("<?= $row['id'] ?>")'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png' border='0' alt='Editar' title='Editar' /></a></td>
                    <td class="text-center"><a href='#' onclick='eliminar(<?= $row['id'] ?>)'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png' border='0' alt='Eliminar' title='Eliminar' /></a></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>

    <div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>
</fieldset>

<form id="formEditar" name="formEditar">
    <input type="hidden" id="tokenEditar" name="token">
    <input type="hidden" id="idActualizar" name="idActualizar">
    <div class="modal-personal modal-personal-lg" id="modal-gantt">
        <div class="modal-personal-head">
            <h1 class="bold" style="margin:20px; font-size:20px; text-align:center;">Detalle <p style="color:#06B6FF; display:inline">Gantt</p></h1>
        </div>
        <div class="modal-personal-body">
            <table style="margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th>Cliente:</th>
                    <td id="cliente"></td>
                </tr>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th>Fecha:</th>
                    <td id="fecha"></td>
                </tr>
            </table>

            <br>

            <table class="tablap">
                <thead>
                    <tr>
                        <th class="rowMiddle text-center">Fase</th>
                        <th class="rowMiddle text-center">Sigla</th>
                        <th class="rowMiddle text-center">Fecha T1</th>
                        <th class="rowMiddle text-center">Fecha T2</th>
                        <th class="text-center">Fecha T3</th>
                        <th class="text-center">Fecha T4</th>
                    </tr>
                </thead>
                <tbody id="bodyTable">
                </tbody>
            </table>            
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirListado">Salir</div>
            <div class="boton1 botones_m2" style="float:right; display:none;" id="actual">Guardar</div>
        </div>
    </div>
</form>
    
<script>
    $(document).ready(function(){
        for(i = 1; i < 5; i++){
            $("#fechaL" + i).datepicker({changeMonth: true});
            $("#fechaTS" + i).datepicker({changeMonth: true});
            $("#fechaRS" + i).datepicker({changeMonth: true});
            $("#fechaRP" + i).datepicker({changeMonth: true});
            $("#fechaOpt" + i).datepicker({changeMonth: true});
            $("#fechaMit" + i).datepicker({changeMonth: true});
            $("#fechaReno" + i).datepicker({changeMonth: true});
            $("#fechaTU" + i).datepicker({changeMonth: true});
            $("#fechaRev" + i).datepicker({changeMonth: true});
            $("#fechaP" + i).datepicker({changeMonth: true});
        }
        
        $("#cancelar").click(function(){
            $("#descrip").val("");
            for(i = 1; i < 5; i++){
                $("#fechaL" + i).val("");
                $("#fechaTS" + i).val("");
                $("#fechaRS" + i).val("");
                $("#fechaRP" + i).val("");
                $("#fechaOpt" + i).val("");
                $("#fechaMit" + i).val("");
                $("#fechaReno" + i).val("");
                $("#fechaTU" + i).val("");
                $("#fechaRev" + i).val("");
                $("#fechaP" + i).val("");
            }
            $("#descrip").focus();
        });
        
        $("#guardar").click(function(){
            fechaL = 0;
            fechaTS = 0;
            fechaRS = 0;
            fechaRP = 0;
            fechaOpt = 0;
            fechaMit = 0;
            fechaReno = 0;
            fechaTU = 0;
            fechaRev = 0;
            fechaP = 0;
            for(i = 1; i < 5; i++){
                if($("#fechaL" + i).val() === ""){
                    fechaL++;
                } 

                if($("#fechaTS" + i).val() === ""){
                    fechaTS++;
                }

                if($("#fechaRS" + i).val() === ""){
                    fechaRS++;
                }

                if($("#fechaRP" + i).val() === ""){
                    fechaRP++;
                }

                if($("#fechaOpt" + i).val() === ""){
                    fechaOpt++;
                }

                if($("#fechaMit" + i).val() === ""){
                    fechaMit++;
                }

                if($("#fechaReno" + i).val() === ""){
                    fechaReno++;
                }

                if($("#fechaTU" + i).val() === ""){
                    fechaTU++;
                }

                if($("#fechaRev" + i).val() === ""){
                    fechaRev++;
                }

                if($("#fechaP" + i).val() === ""){
                    fechaP++;
                }
            }

            if(fechaL === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Levantmiento', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaTS === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Troubleshooting', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaRS === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Reporte Servidores', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaRP === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Reporte PCs', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaOpt === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Optimización', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaMit === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Mitigación', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaReno === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Rennovación', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaTU === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de True UP', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaRev === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Revisión', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } else if(fechaP === 4){
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Presupuesto', {'Aceptar': 'Aceptar'}, function(button) {
                });
                return false;
            } 
            validate();
        });
        
        $("#salirListado").click(function(){
            $("#modal-gantt").hide();
            $("#fondo1").hide();
            $("#cliente").empty();
            $("#fecha").empty();
            $("#bodyTable").empty();
            $("#actual").hide();
        });
        
        $("#actual").click(function(){
            fechaLEdit = 0;
            fechaTSEdit = 0;
            fechaRSEdit = 0;
            fechaRPEdit = 0;
            fechaOptEdit = 0;
            fechaMitEdit = 0;
            fechaRenoEdit = 0;
            fechaTUEdit = 0;
            fechaRevEdit = 0;
            fechaPEdit = 0;
            for(i = 1; i < 5; i++){
                if($("#fechaLEdit" + i).val() === ""){
                    fechaLEdit++;
                } 

                if($("#fechaTSEdit" + i).val() === ""){
                    fechaTSEdit++;
                }

                if($("#fechaRSEdit" + i).val() === ""){
                    fechaRSEdit++;
                }

                if($("#fechaRPEdit" + i).val() === ""){
                    fechaRPEdit++;
                }

                if($("#fechaOptEdit" + i).val() === ""){
                    fechaOptEdit++;
                }

                if($("#fechaMitEdit" + i).val() === ""){
                    fechaMitEdit++;
                }

                if($("#fechaRenoEdit" + i).val() === ""){
                    fechaRenoEdit++;
                }

                if($("#fechaTUEdit" + i).val() === ""){
                    fechaTUEdit++;
                }

                if($("#fechaRevEdit" + i).val() === ""){
                    fechaRevEdit++;
                }

                if($("#fechaPEdit" + i).val() === ""){
                    fechaPEdit++;
                }
            }

            if(fechaLEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Levantmiento', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaTSEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Troubleshooting', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaRSEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Reporte Servidores', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaRPEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Reporte PCs', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaOptEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Optimización', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaMitEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Mitigación', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaRenoEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Rennovación', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaTUEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de True UP', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaRevEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Revisión', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } else if(fechaPEdit === 4){
                mostrarOcultarModal(false);
                $.alert.open('alert', 'Debe seleccionar al menos una fecha de Presupuesto', {'Aceptar': 'Aceptar'}, function(button) {
                    mostrarOcultarModal(true);
                });
                return false;
            } 
            
            mostrarOcultarModal(false);
            $("#fondo").show();
            $("#tokenEditar").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formEditar")[0]);	
            $.ajax({
                type: "POST",
                url: "ajax/actualizarGantt.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                            
                    result = data[0].result;
                    if(result === 0){
                        $.alert.open("alert", "No se pudo actualizar el Gantt", {"Aceptar" : "Aceptar"}, function() {
                            $("#salirListado").click();
                        });
                    }
                    else if(result === 1){
                        $.alert.open("info", "Gantt actualizado con éxito", {"Aceptar" : "Aceptar"}, function() {
                            $("#salirListado").click();
                        });
                    }
                    $("#fondo").hide();
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    $("#salirListado").click();
                });
            });
        });
    });
    
    function limpiarInput(input){
        input.value = "";
    }
    
    function validarFecha(input, trimestre){
        arrayFecha = $("#" + input).val().split("/");
        if(trimestre === 1 && parseInt(arrayFecha[1]) > 3){
            $.alert.open('alert', 'Esta fecha debe estar en el rango del primer trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        } else if(trimestre === 2 && (parseInt(arrayFecha[1]) < 4  || parseInt(arrayFecha[1]) > 6)){
            $.alert.open('alert', 'Esta fecha debe estar en el rango del segundo trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        } else if(trimestre === 3 && (parseInt(arrayFecha[1]) < 7  || parseInt(arrayFecha[1]) > 9)){
            $.alert.open('alert', 'Esta fecha debe estar en el rango del tercer trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        } else if(trimestre === 4 && parseInt(arrayFecha[1]) < 10){
            $.alert.open('alert', 'Esta fecha debe estar en el rango del cuarto trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        }
    }
    
    function validarFechaModal(input, trimestre){
        arrayFecha = $("#" + input).val().split("/");
        if(trimestre === 1 && parseInt(arrayFecha[1]) > 3){
            mostrarOcultarModal(false);
            $.alert.open('alert', 'Esta fecha debe estar en el rango del primer trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    mostrarOcultarModal(true);
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        } else if(trimestre === 2 && (parseInt(arrayFecha[1]) < 4  || parseInt(arrayFecha[1]) > 6)){
            mostrarOcultarModal(false);
            $.alert.open('alert', 'Esta fecha debe estar en el rango del segundo trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    mostrarOcultarModal(true);
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        } else if(trimestre === 3 && (parseInt(arrayFecha[1]) < 7  || parseInt(arrayFecha[1]) > 9)){
            mostrarOcultarModal(false);
            $.alert.open('alert', 'Esta fecha debe estar en el rango del tercer trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    mostrarOcultarModal(true);
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        } else if(trimestre === 4 && parseInt(arrayFecha[1]) < 10){
            mostrarOcultarModal(false);
            $.alert.open('alert', 'Esta fecha debe estar en el rango del cuarto trimestre', {'Aceptar': 'Aceptar'}, function(button) {
                if (button === 'Aceptar'){
                    mostrarOcultarModal(true);
                    $("#" + input).val("");
                    $("#" + input).focus();
                }
            });
        }
    }
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el Gantt?', {'Aceptar': 'Aceptar', 'Cancelar': 'Cancelar'}, function(button) {
            if(button === "Aceptar"){
                $.post("ajax/eliminarGantt.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
                    <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                    if(data[0].result === true){
                        $.alert.open('info', 'Registro Eliminado con éxito', {'Aceptar': 'Aceptar'}, function() {
                            location.href = "gantt.php?id=" + $("#id").val();
                        });
                    }
                    else{
                        $.alert.open('alert', 'No se pudo eliminar el registro', {'Aceptar': 'Aceptar'}, function() {
                            location.href = "gantt.php?id=" + $("#id").val();
                        });
                    }
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
    }
    
    function ver(id){
        $.post("ajax/verGantt.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
            
            $("#cliente").append(data[0].cliente);
            $("#fecha").append(data[0].fecha);
            $("#bodyTable").append(data[0].tabla);
            
            $("#fondo1").show();
            $("#modal-gantt").show();
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function actualizar(id){
        $.post("ajax/detalleGantt.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
            
            $("#cliente").append(data[0].cliente);
            $("#fecha").append(data[0].fecha);
            $("#bodyTable").append(data[0].tabla);
            
            $("#fondo1").show();
            $("#actual").show();
            $("#modal-gantt").show();
            $("#idActualizar").val(id);
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
            $("#idActualizar").val(0);
        });
    }
    
    function mostrarOcultarModal(opc){
        if(opc === true){
            $("#fondo1").show();
            $("#modal-gantt").show();
        } else{
            $("#fondo1").hide();
            $("#modal-gantt").hide();
        }    
    }
</script>