<script>
    $(document).ready(function(){
        $("ul.pestania li a:first").addClass("active3");
        $(".secciones fieldset").hide();
        $(".secciones fieldset:first").show();

        $("ul.pestania li a").click(function(){
            $("ul.pestania li a").removeClass("active3");
            $(this).addClass("active3");
            $(".secciones fieldset").hide();
            var pestaniaActual = $(this).attr("href");
            $(pestaniaActual).show();
            
            if (pestaniaActual === "#tab2"){
                $.post("ajax/listarDominios.php", { cliente : $("#id_usuario").val(), nombre : "", pagina : 1, token : localStorage.licensingassuranceToken }, function(data){
                    <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>   

                    $("#contTablaDominios").empty();
                    $("#contTablaDominios").append(data[0].listado);
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            } else if (pestaniaActual === "#tab3"){
                $.post("ajax/dataNADDSuscripcion.php", { cliente : $("#idUser").val(), token : localStorage.licensingassuranceToken }, function(data){
                    <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>   

                    $("#correosDominio").val(data[0].cantCorreos);
                    $("#despliegueDominio").val(data[0].cantDespliegue);
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
        
        $("#modificar").click(function(){
            $("#form1").submit();
        });

        $("#agregar").click(function(){
            $("#nombre").val("");
            $("#fondo1").show();
            $("#modal-dominios").show();
        });

        $("#salir").click(function(){
            $("#nombre").val("");
            $("#fondo1").hide();
            $("#modal-dominios").hide();
        });
        
        $("#guardarDominio").click(function(){
            $("#fondo1").hide();
            $("#modal-dominios").hide();
            $("#fondo").show();
                
            $.post("ajax/validarDominio.php", { cliente : $("#id_usuario").val(), nombre : $("#nombre").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                                        
                if(data[0].result === 1){
                    $.alert.open('info', "Dominio guardado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                        $("#contTablaDominios").empty();
                        $("#contTablaDominios").append(data[0].listado);
                    });
                    
                    $("#fondo").hide();
                } else if(data[0].result === 2){
                    $.alert.open('warning', 'Alerta', "No se pudo guardar el dominio", {'Aceptar' : 'Aceptar'}, function() {
                        $("#fondo1").show();
                        $("#modal-dominios").show();
                    });
                    
                    $("#fondo").hide();
                } else{
                    $.post("ajax/guardarDominio.php", { cliente : $("#id_usuario").val(), nombre : $("#nombre").val(), token : localStorage.licensingassuranceToken }, function(data){
                        <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                        if(data[0].result === false){
                            $.alert.open('warning', 'Alerta', "No se pudo guardar el dominio", {'Aceptar' : 'Aceptar'}, function() {
                                $("#fondo1").show();
                                $("#modal-dominios").show();
                            });
                        } else{
                            $.alert.open('info', "Dominio guardado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                                $("#contTablaDominios").empty();
                                $("#contTablaDominios").append(data[0].listado);
                            });
                        }
                        
                        $("#fondo").hide();
                    }, "json")
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                        });
                    });    
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#modificarDespliegue").click(function(){
            $("#fondo").show();
            $.post("ajax/guardarDespliegueDominio.php", { cliente : $("#idUser").val(), cantCorreo : $("#correosDominio").val(), cantidadDespliegue : $("#despliegueDominio").val(), 
            token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === false){
                    $.alert.open('warning', 'Alerta', "No se pudo guardar la data", {'Aceptar' : 'Aceptar'});
                } else{
                    $.alert.open('info', "data guardada con éxito");
                }

                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });    
        });
    });
    
    function eliminar(indice){
        $.alert.open('confirm', "Desea eliminar el Dominio?", {'Si' : 'Si', 'No' : 'No'}, function(resp) {
            if(resp === 'Si'){
                $("#fondo").show();
                $.post("ajax/eliminarDominio.php", { cliente : $("#id_usuario").val(), id : indice, token : localStorage.licensingassuranceToken }, function(data){
                    <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                    if(data[0].result === false){
                        $.alert.open('warning', 'Alerta', "No se pudo eliminar el dominio", {'Aceptar' : 'Aceptar'}, function() {
                            $("#contTablaDominios").empty();
                            $("#contTablaDominios").append(data[0].listado);
                        });
                    } else{
                        $.alert.open('info', "Dominio eliminado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                            $("#contTablaDominios").empty();
                            $("#contTablaDominios").append(data[0].listado);
                        });
                    }

                    $("#fondo").hide();
                }, "json")
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });    
            }
        });
    }
</script>

