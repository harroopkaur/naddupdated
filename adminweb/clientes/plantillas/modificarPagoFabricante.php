<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro actualizado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todos los registros del detalle', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
}
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Modificar Pago</span></legend>
    
    <form id="form1" name="form1" method="post" enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id ?>">
        
        <?php $validator->print_script(); ?>
        
        <div class="text-center">
            <span class="bold">Descripción:</span><input type="text" id="descrip" name="descrip" style="margin-left:15px;" value="<?= $datosPago["descripcion"] ?>">
            <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_descrip") ?></font></div>
        </div>
        <br>
        <br>
        <table class="tablap">
            <thead>
                <tr>
                    <th class="text-center">Fabricante</th>
                    <th class="text-center">Aniversario</th>
                    <th class="text-center">Renovación</th>
                    <th class="text-center">Monto del Pago</th>
                    <th class="text-center">Eliminar</th>
                </tr>
            </thead>
            <tbody id="tablaFabricantesActivos">
                <?php 
                $i = 0;
                foreach($listaFabActivos as $row){
                    $aniv = null;
                    if($row["aniversario"] != ""){
                        $aniv = $general->reordenarFecha($row["aniversario"], '-', '/');
                    }

                    $reno = null;
                    if($row["renovacion"] != ""){
                        $reno = $general->reordenarFecha($row["renovacion"], '-', '/');
                    }
                ?>
                    <tr id="fila<?= $row['id'] ?>">
                        <th><?= $row["nombre"] ?><input type="hidden" id="idDetalle<?= $i ?>" name="idDetalle[]" value="<?= $row["id"] ?>"></th>
                        <td class="text-center"><input type="text" id="aniversario<?= $i ?>" name="aniversario[]" value="<?= $aniv ?>" readonly></td>
                        <td class="text-center"><input type="text" id="renovacion<?= $i ?>" name="renovacion[]" value="<?= $reno ?>" readonly></td>
                        <td class="text-center"><input type="text" id="pago<?= $i ?>" name="pago[]" value="<?= $row["monto"] ?>"></td>
                        <td class="text-center">
                            <a href="#" onclick="eliminar(<?= $row['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" />
                        </td>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
        
        <br>

        <div style="width:250px; margin:0 auto;">
            <input name="volver" type="button" id="volver" value="VOLVER" class="boton" style="display:inline-block"/>
            <input name="guardar" type="button" id="guardar" value="GUARDAR" class="boton" style="display:inline-block"/>
        </div>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
        for(i = 0; i < $("#tablaFabricantesActivos tr").length; i++){
            $("#aniversario" + i).datepicker();
            $("#renovacion" + i).datepicker();
            $("#pago" + i).numeric();
        }
        
        $("#guardar").click(function(){
            for(i = 0; i < $("#tablaFabricantesActivos tr").length; i++){
                if($("#aniversario" + i).val() === "" && $("#renovacion" + i).val() === ""){
                    $.alert.open('alert', 'Debes agregar una fecha de aniversario o renovación', {Aceptar: 'Aceptar'}, function() {
                        $("#aniversario" + i).focus();
                    });
                    return false;
                }
                
                if($("#pago" + i).val() === ""){
                    $.alert.open('alert', 'Debes agregar un monto de pago', {Aceptar: 'Aceptar'}, function() {
                        $("#pago" + i).focus();
                    });
                    return false;
                }
            }
            validate();
        });
        
        $("#volver").click(function(){
            location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
        });
    });
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea anular el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $.post("ajax/eliminarDetallePago.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
                    <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                    if(data[0].result === true){
                        $.alert.open('info', 'Registro Eliminado con éxito', {'Aceptar': 'Aceptar'}, function() {
                            $("#fila"+id).remove();
                        });
                    }
                    else{
                        $.alert.open('alert', 'No se pudo eliminar el registro', {'Aceptar': 'Aceptar'}, function() {
                        });
                    }
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
    }
</script>