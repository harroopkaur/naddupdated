<?php
if ($agregar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar las asignaciones', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/asignacion.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 1) {
    ?>
    <script type="text/javascript">
        $.alert.open('info', 'Asignaciones actualiadas con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/asignacion.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php 
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se actualizaron todas las asignaciones', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/asignacion.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php    
}
?>
    
<form id="form1" name="form1" method="post"  enctype="multipart/form-data" action="asignacion.php">
    <input type="hidden" name="insertar" id="insertar" value="1" />
    <input type="hidden" name="id" id="id" value="<?= $id_user ?>">
    
    <h1><strong style="color:#000; font-weight:bold;"></strong></h1>
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Asignación</span></legend>
        
        <table style="width:400px;" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
            <thead>
                <tr>
                    <th>Asignación</th> 
                    <th style="width:50px;">&nbsp;</th> 
                </tr>
            </thead>
            <tbody id="contTabla">
                <?php 
                $i = 0;
                foreach($listaAsig as $row){
                ?>
                    <tr id='fila<?= $i ?>'>
                        <td><input type='hidden' id='idAsignacion<?= $i ?>' name='idAsignacion[]' value='<?= $row["idAsignacion"] ?>'><?= $row["nombre"] ?></td>
                        <td><a href='#' onclick='eliminar(<?= $i ?>)'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png' width='20' height='28' border='0' alt='Eliminar' title='Eliminar' /></a></td>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </fieldset>
   
    <br>
    
    <div style="width:250px; margin:0 auto;">
        <input name="agregar" type="button" id="agregar" value="AGREGAR" class="boton" style="display:inline-block"/>
        <input name="insertarAsig" type="button" id="insertarAsig" value="GUARDAR" class="boton" style="display:inline-block"/>
    </div>
</form>

<div class="modal-personal" id="modal-asignacion">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Crear <p style="color:#06B6FF; display:inline">Asignaciones</p></h1>
    </div>
    <div class="modal-personal-body">
        <table style="width:400px; margin:0 auto; margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap2">
                <tr>
                    <th class="text-center">Asignación</th> 
                    <td class="text-center"><input type="text" id="nombre" name="nombre" maxlength="80" style="width:100%;"></td> 
                </tr>
            </tbody>
        </table>               
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salir">Salir</div>
        <div class="boton1 botones_m2" id="guardarAsignacion" style="float:right;">Guardar</div>
    </div>
</div>    
    
<div class="modal-personal" id="modal-listadoAsignacion">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Asignaciones</p></h1>
    </div>
    <div class="modal-personal-body">
        <table style="width:400px; margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th colspan="2" class="text-center"><input type="text" id="nombAsignacion" name="nombAsignacion" maxlength="80" style="width:100%;"></th> 
                    <th class="text-center" style="width:100px;"><div id="buscar" class="botonBuscar pointer">Buscar</div></th> 
                </tr>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th colspan="3" class="text-center">Asignación</th> 
                </tr> 
            </thead>
            <tbody id="bodyTable">
            </tbody>
        </table>
        
        <br>
        <div class="text-center" id="paginacion"></div>                 
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirListado">Salir</div>
        <div class="boton1 botones_m2" id="agregarAsignacion" style="float:right;">Agregar</div>
        <div class="boton1 botones_m2" id="seleccionarAsignacion" style="float:right;">Seleccionar</div>
    </div>
</div>
<script>
    var fila = <?= $i ?>;
    $(document).ready(function () {
        $("#agregar").click(function(){
            $("#fondo1").show();
            $("#nombAsignacion").val("");
            buscarListado("", 1);            
        });
        
        $("#salirListado").click(function(){
            $("#nombAsignacion").val("");
            $("#fondo1").hide();
            $("#modal-listadoAsignacion").hide();
        });
        
        $("#seleccionarAsignacion").click(function(){
            id = new Array();
            nombre = new Array();
            j = 0;
            for(i = 0; i < $("#bodyTable tr").length; i++){
                if($("#asig" + i).prop("checked")){
                    registro = $("#asig" + i).val();
                    array = registro.split("*");
                    id[j] = array[0];
                    nombre[j] = array[1];
                    j++;
                }
            }
            
            for (k = 0; k < j; k++){
                existe = 0;
                for(i = 0; i < fila; i++){
                    if($("#idAsignacion" + i).val() === id[k]){
                        existe = 1;
                    }
                } 
                
                if (existe === 0){
                    agregarFila(id[k], nombre[k]);
                }
            }
            
            $("#fondo1").hide();
            $("#modal-listadoAsignacion").hide();
        });
        
        $("#buscar").click(function(){
            buscarListado($("#nombAsignacion").val(), 1);
        });
        
        /*inicio crear una asignacion*/
        $("#agregarAsignacion").click(function(){
            $("#modal-listadoAsignacion").hide();
            $("#nombre").val("");
            $("#modal-asignacion").show();
        });
        
        $("#guardarAsignacion").click(function(){
            $.post("ajax/validarAsignacion.php", { nombre : $("#nombre").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                        
                if(parseInt(data[0].result) > 0){
                    $("#fondo1").hide();
                    $("#modal-asignacion").hide();
                    $.alert.open('alert', "Ya existe esta asignación", {'Aceptar' : 'Aceptar'}, function() {
                        $("#fondo1").show();
                        $("#modal-asignacion").show();
                    });
                } else{
                    $.post("ajax/guardarAsignacion.php", { nombre : $("#nombre").val(), token : localStorage.licensingassuranceToken }, function(data){
                        <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                        $("#fondo1").hide();
                        $("#modal-asignacion").hide();

                        if(data[0].result === false){
                            $.alert.open('alert', "No se pudo guardar la asignación", {'Aceptar' : 'Aceptar'}, function() {
                                $("#fondo1").show();
                                $("#modal-listadoAsignacion").show();
                            });
                        } else{
                            $.alert.open('info', "Asignación guardada con éxito", {'Aceptar' : 'Aceptar'}, function() {
                                $("#agregar").click();
                            });
                        }
                    }, "json")
                    .fail(function( jqXHR ){
                        $("#fondo1").hide();
                        $("#modal-asignacion").hide();
                        $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                        });
                    });    
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo1").hide();
                $("#modal-asignacion").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#salir").click(function(){
            $("#modal-asignacion").hide();
            $("#modal-listadoAsignacion").show();
        });
        /*fin crear una asignacion*/
        
        $("#insertarAsig").click(function(){
            if($("#contTabla tr").length === 0){
                $.alert.open('alert', "Debe agregar al menos una asignación", {'Aceptar' : 'Aceptar'}, function() {
                });
                return false;
            }
            $("#form1").submit();
        });
    });
    
    function buscarListado(nombre, pagina){
        $.post("ajax/listarAsignacion.php", { nombre : nombre, pagina : pagina, token : localStorage.licensingassuranceToken }, function(data){
            <?php require($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>   
                    
            $("#bodyTable").empty();
            $("#paginacion").empty();
            $("#bodyTable").append(data[0].listado);
            $("#paginacion").append(data[0].paginacion);
            $("#modal-listadoAsignacion").show();
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function seleccionarAsignacion(indice){
        if ($("#asig" + indice).prop("checked")){
            $("#asig" + indice).prop("checked", "");
        } else{
            $("#asig" + indice).prop("checked", "checked");
        }
    }
    
    function eliminar(indice){
        $("#fila" + indice).remove();
        if($("#contTabla tr").length === 0){
            fila = 0;
        }
    }
    
    function nuevaPagina(indice){
        buscarListado($("#nombAsignacion").val(), indice);
    }
    
    function agregarFila(id, nombre){
        row = "<tr id='fila" + fila + "'>";
        row += "<td>";
        row += "<input type='hidden' id='idAsignacion" + fila + "' name='idAsignacion[]' value='" + id + "'>";
        row += nombre;
        row += "</td>";
        row += "<td><a href='#' onclick='eliminar(" + fila + ")'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png' width='20' height='28' border='0' alt='Eliminar' title='Eliminar' /></a></td>";
        row += "</tr>";
        fila++;

        $("#contTabla").append(row);
    }
</script>