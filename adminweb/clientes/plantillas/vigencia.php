<?php
if ($exito == true) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = 'asignacion.php';
            }
        });
    </script>
    <?php
}
?>
    
<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="insertar" id="insertar" value="1" />
    <input type="hidden" name="id" id="id" value="<?= $id_user ?>" />
    
    <h1><strong style="color:#000; font-weight:bold;"></strong></h1>
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Aprobación/Rechazo en Webtool</span></legend>
        
        <?php $validator->print_script(); ?>
        <div class="error_prog">
            <font color="#FF0000">
                <?php if ($error == 5) {
                    echo $clientes->error;
                 } ?>
            </font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <td align="left" width="20"><input type="radio" name="estado" value="1" <?php if($clientes2->estado==1){ echo 'checked';}  ?> ></td> 
                <th  align="left" valign="top">Activar cliente</th><td>&nbsp;</td>
                <td align="left" width="20"><input type="radio" name="estado" value="0" <?php if($clientes2->estado==0){ echo 'checked';}  ?> ></td>
                <th align="left" valign="top">Bloquear Cliente</th>
            </tr>
        </table>
    </fieldset>
    
    <br><br>

    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Vigencia del Cliente en WebTool</span></legend>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th  align="left" valign="top" width="90">Fecha Desde:</th>
               
                <td align="left">
                    <input type="text" name="fecha" id="fecha" readonly size="12" value="<?php if($general->muestrafecha($clientes->fecha1) != "00/00/0000"){ echo $general->muestrafecha($clientes2->fecha1); } else{ echo ""; } ?>"/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fecha") ?></font></div>
                </td>
                
                <td>&nbsp;</td>
                
                <th align="left" valign="top" width="90">Fecha Hasta:</th>
                
                <td align="left">
                    <input type="text" name="fecha2" id="fecha2" readonly size="12" value="<?php if($general->muestrafecha($clientes2->fecha2) != "00/00/0000"){ echo $general->muestrafecha($clientes2->fecha2); } else{ echo ""; } ?>"/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fecha2") ?></font></div>
                </td>
            </tr>
        </table>
    </fieldset>

    <br>
    
    <div style="width:100px; margin:0 auto;">
        <input name="insertar" type="button" id="insertar" value="GUARDAR" onclick="validate();" class="boton" />
    </div>
</form>

<script>
    $(document).ready(function () {
        $("#datepicker").datepicker();
        $("#fecha").datepicker();
        $("#fecha2").datepicker();
    });
</script>