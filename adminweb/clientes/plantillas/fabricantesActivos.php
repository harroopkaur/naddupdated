<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo insertar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo insertar todos los registros del detalle', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/fabricantesActivos.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
}
?>   

<script language="javascript" type="text/javascript">
    //validar que los modulos esten marcados
    function marcarFabricante(fabricante, indice) {
        var marca = 0;
        if ($('#fabricante' + indice).prop('checked')) {
            marca = 1;
        }
        $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/clientes/ajax/fabricantesActivos.php", { cliente : <?= $id_user ?>, marca : marca,
        fabricante : fabricante, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
                
            $("#tablaFabricantesActivos").empty();
            $("#tablaFabricantesActivos").append(data[0].tabla);
            
            for(i = 0; i < $("#tablaFabricantesActivos tr").length; i++){
                $("#aniversario" + i).datepicker();
                $("#renovacion" + i).datepicker();
                $("#pago" + i).numeric();
            }
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
</script>

<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminarPagoFabricante.php">
    <input type="hidden" id="cliente" name="cliente" value="<?= $id_user ?>">
    <input type="hidden" id="id" name="id">
</form>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Fabricantes Activos</span></legend>

    <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th colspan="3" style="text-align:center">Fabricantes</th>
        </tr>
        <?php
        $i = 0;
        $j = 0;
        $z = 0;
        foreach($listadoFabricantes as $row){
            if($i == $j){
                $j = 3;
            ?>
                <tr>
            <?php 
            }
            ?>        
                <td>
                    <input type="checkbox" id="fabricante<?= $z; ?>" name="fabricante" <?php if ($fabricantesActivos->existeFabricante($id_user, $row["idFabricante"])) { echo "checked='checked'"; } ?> value="<?= $row["idFabricante"] ?>"
                    onclick = "marcarFabricante(<?= $row["idFabricante"] ?>, <?= $z ?>)">
                    <?php
                    echo "&nbsp;&nbsp;" . $row["nombre"];
                    ?>
                </td>
            <?php 
            $i++;
            if($i == $j){
                $i = 0;
                $j = 0;
            ?>
                </tr>
            <?php   
            }
            $z++;
        }
        ?>
    </table>
</fieldset>

<form id="form1" name="form1" method="post" enctype="multipart/form-data"> 
    <input type="hidden" id="insertar" name="insertar" value="1">
    <input type="hidden" id="id_user" name="id_user" value="<?= $id_user ?>">
    <input type="hidden" id="token" name="token">
    
    <?php $validator->print_script(); ?>
    
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Información de Pagos</span></legend>
        
        <div class="text-center">
            <span class="bold">Descripción:</span><input type="text" id="descrip" name="descrip" style="margin-left:15px;">
            <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_descrip") ?></font></div>
        </div>
        <br>
        <br>
        <table class="tablap">
            <thead>
                <tr>
                    <th class="text-center">Fabricante</th>
                    <th class="text-center">Aniversario</th>
                    <th class="text-center">Renovación</th>
                    <th class="text-center">Monto del Pago</th>
                </tr>
            </thead>
            <tbody id="tablaFabricantesActivos">
                <?php 
                $i = 0;
                foreach($listaFabActivos as $row){
                ?>
                    <tr>
                        <th><?= $row["nombre"] ?><input type="hidden" id="fab<?= $i ?>" name="fab[]" value="<?= $row["idFabricante"] ?>"></th>
                        <td class="text-center"><input type="text" id="aniversario<?= $i ?>" name="aniversario[]" readonly></td>
                        <td class="text-center"><input type="text" id="renovacion<?= $i ?>" name="renovacion[]" readonly></td>
                        <td class="text-center"><input type="text" id="pago<?= $i ?>" name="pago[]"></td>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
        
        <br>

        <div style="width:250px; margin:0 auto;">
            <input name="cancelar" type="button" id="cancelar" value="CANCELAR" class="boton" style="display:inline-block"/>
            <input name="guardar" type="button" id="guardar" value="GUARDAR" class="boton" style="display:inline-block"/>
        </div>
    </fieldset>
</form>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Historial de Pagos</span></legend>

    <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th style="text-align:center">Descripción</th>
            <th style="text-align:center; width:100px;">Editar</th>
            <th style="text-align:center; width:100px;">Eliminar</th>
        </tr>
        <?php
        $i = 0;
        foreach($listadoPagos as $row){
        ?>
            <tr>   
                <td><?= $row["descripcion"] ?></td>
                <td class="text-center">
                    <a href="modificarPagoFabricante.php?id=<?= $row['id'] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                </td>
                <td class="text-center">
                    <a href="#" onclick="eliminar(<?= $row['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" />
                </td>
            </tr>
        <?php   
            $i++;
        }
        ?>
    </table>
    
    <div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>

    <?php 
    if($count == 0) { ?>
        <div style="text-align:center; width:90%; margin:0 auto">No hay Pagos</div>
    <?php 
    }
    ?>
</fieldset>

<script>
    $(document).ready(function(){
        for(i = 0; i < $("#tablaFabricantesActivos tr").length; i++){
            $("#aniversario" + i).datepicker({ changeMonth: true, changeYear: true });
            $("#renovacion" + i).datepicker({ changeMonth: true, changeYear: true });
            $("#pago" + i).numeric();
        }
        
        $("#cancelar").click(function(){
            $("#descrip").val("");
            
            for(i = 0; i < $("#tablaFabricantesActivos tr").length; i++){
                $("#aniversario" + i).val("");
                $("#renovacion" + i).val("");
                $("#pago" + i).val("");
            }
        });
        
        $("#guardar").click(function(){
            for(i = 0; i < $("#tablaFabricantesActivos tr").length; i++){
                if($("#aniversario" + i).val() === "" && $("#renovacion" + i).val() === ""){
                    $.alert.open('alert', 'Debes agregar una fecha de aniversario o renovación', {Aceptar: 'Aceptar'}, function() {
                        $("#aniversario" + i).focus();
                    });
                    return false;
                }
                
                if($("#pago" + i).val() === ""){
                    $.alert.open('alert', 'Debes agregar un monto de pago', {Aceptar: 'Aceptar'}, function() {
                        $("#pago" + i).focus();
                    });
                    return false;
                }
            }
            validate();
        });
    });
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea anular el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>