<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos del Cliente</span></legend>
    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <th width="150" align="left" valign="middle">Empresa:</th>
            <td align="left" valign="middle"><?= $clientes->empresa ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">E-mail:</th>
            <td align="left" valign="middle"><?= $clientes->correo ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">telefono:</th>
            <td align="left" valign="middle"><?= $clientes->telefono ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Pais:</th>
            <td align="left" valign="middle"><?= $pais->nombre ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Nivel de Servicio:</th>
            <td align="left" valign="middle"><?= $nivel ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">estado:</th>
            <td align="left" valign="middle"><?php if($clientes->estado=='1'){ echo'Autorizado';  } else{ echo'No Autorizado';  }  ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Fecha desde:</th>
            <td align="left" valign="middle"><?= $general->muestrafecha($clientes->fecha1) ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Fecha hasta:</th>
            <td align="left" valign="middle"><?= $general->muestrafecha($clientes->fecha2) ?></td>
        </tr>
       
        <tr>
            <th width="150" align="left" valign="middle">Fecha registro:</th>
            <td align="left" valign="middle"><?= $general->muestrafecha($clientes->fecha_registro) ?></td>
        </tr>
    </table>
</fieldset>