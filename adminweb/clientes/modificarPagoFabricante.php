<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/adminweb/clientes/procesos/modificarPagoFabricante.php");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/head.php");
?>

<section class="contenedor">
    <div class="contenedorMenuAdmin">
        <?php include_once($GLOBALS["app_root"] . "/adminweb/plantillas/titulo.php"); ?>

        <br><br>

        <?php
        $opcionm1 = 3;
        include_once($GLOBALS["app_root"] . "/adminweb/plantillas/menu1.php");
        ?>
    </div>

    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno">
                <?php
                    $menuClientes = 9;
                    include_once($GLOBALS["app_root"] . "/adminweb/clientes/plantillas/menu.php");
                ?>
            </div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root"] . "/adminweb/clientes/plantillas/modificarPagoFabricante.php") ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/pie.php");
?>