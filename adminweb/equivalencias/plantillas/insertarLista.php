<?php
if($exito == 1 && $agregar == 1){      
?>
<script type="text/javascript">
    $.alert.open('info', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
        if (button === 'Aceptar'){
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/equivalencias/';
        }
    });
</script>
<?php
} else if($exito == 2 && $agregar == 1){
?>
<script type="text/javascript">
    $.alert.open('warning', 'Alerta', 'No se insertaron todos los registros', {'Aceptar': 'Aceptar'}, function(button) {
        if (button === 'Aceptar'){
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/equivalencias/';
        }
    });
</script>
<?php
}else if($exito == 0 && $agregar == 1){
?>
<script type="text/javascript">
    $.alert.open('warniing', 'Alerta', 'No se insertó ningún registro', {'Aceptar': 'Aceptar'}, function(button) {
        if (button === 'Aceptar'){
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/equivalencias/';
        }
    });
</script>
<?php
}
?>
        
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Nuevo Producto</span></legend>

    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <th width="90" align="left" valign="top">Fabricante:</th>
            <td align="left">
                <select id="fabricante" name="fabricante">
                    <option value="">--Seleccione--</option>
                    <?php 
                    foreach($fabricante as $row){
                    ?>
                        <option value="<?= $row["idFabricante"] ?>"><?= $row["nombre"] ?></option>
                    <?php
                    }
                    ?>
                </select>
                <div class="error_prog" id="msj_fabricante"><font color="#FF0000"></font></div>
            </td>
        </tr>

        <tr>
            <th width="90" align="left" valign="top">Familia:</th>
            <td align="left">
                <select id="familia" name="familia">
                    <option value="">--Seleccione--</option>
                    <?php 
                    foreach($familia as $row){
                    ?>
                    <option value="<?= $row["idProducto"] ?>"><?= $row["nombre"] ?></option>
                    <?php
                    }
                    ?>
                </select>
                <!--<input name="familia" id="familia" type="text" value="<?//= $_POST['familia'] ?>" size="30" maxlength="250"  />-->
                <div class="error_prog" id="msj_familia"><font color="#FF0000"></font></div>
            </td>
        </tr>

        <tr>
            <th width="90" align="left" valign="top">Edicion:</th>
            <td align="left">
                <select id="edicion" name="edicion">
                    <option value="">--Seleccione--</option>
                    <?php 
                    foreach($edicion as $row){
                    ?>
                    <option value="<?= $row["idEdicion"] ?>"><?= $row["nombre"] ?></option>
                    <?php
                    }
                    ?>
                </select>
                <!--<input name="edicion" id="edicion" type="text" value="<?//= $_POST['edicion'] ?>" size="30" maxlength="250"  />-->
                <div class="error_prog" id="msj_edicion"><font color="#FF0000"></font></div>
            </td>
        </tr>

        <tr>
            <th width="90" align="left" valign="top">Version:</th>
            <td align="left">
                <select id="version" name="version">
                    <option value="">--Seleccione--</option>
                    <?php 
                    foreach($version as $row){
                    ?>
                    <option value="<?= $row["id"] ?>"><?= $row["nombre"] ?></option>
                    <?php
                    }
                    ?>
                </select>
                <!--<input name="version" id="version" type="text" value="<?//= $_POST['version'] ?>" size="30" maxlength="250"  />-->
                <div class="error_prog" id="msj_version"><font color="#FF0000"></font></div>
            </td>
        </tr>

        <tr>
            <th width="90" align="left" valign="top">Precio:</th>
            <?php 
            if(isset($_POST['precio'])){
                $precio = $_POST['precio'];
            }
            else{
                $precio = "";
            }
            ?>
            <td align="left"><input name="precio" id="precio" type="text" value="<?= $precio ?>" size="14" maxlength="20"  />
                <div class="error_prog" id="msj_precio"><font color="#FF0000"></font></div></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><input name="agregar" type="button" id="agregar" value="AGREGAR" onclick="verifProd();" class="boton" /></td>
        </tr>
    </table>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" id="agregar" name="agregar" value="1">
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
            <thead>
                <tr>
                    <th width="90" align="center" valign="top">Fabricante</th>
                    <th width="90" align="center" valign="top">Familia</th>
                    <th width="90" align="center" valign="top">Edicion</th>
                    <th width="90" align="center" valign="top">Version</th>
                    <th width="90" align="center" valign="top">Precio</th>
                    <th width="90" align="center" valign="top" >Eliminar</th>
                </tr>
            </thead>
            <tbody id="tabla"></tbody>
        </table>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <td align="center"><input name="insertar" type="button" id="insertar" value="GUARDAR" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>

<script>
    var i = 0;
    
    $(document).ready(function(){
        $("#precio").numeric();
        
        $("#insertar").click(function(){
            if($("#tabla tr").length === 0){
                $.alert.open('warning', 'Alerta', "Debe agregar al menos un registro", {'Aceptar' : 'Aceptar'});
                return false;
            }
            
            $("#form1").submit();
        });
    });
   
    function verifProd(){
        $("#msj_fabricante").empty();
        $("#msj_familia").empty();
        $("#msj_edicion").empty();
        $("#msj_version").empty();
        $("#msj_precio").empty();
        
        if ($("#fabricante").val() === '') { 
            $("#msj_fabricante").append(' Obligatorio');
            $("#fabricante").focus();
            return false;
        } 
        
        if ($("#familia").val() === '') { 
            $("#msj_familia").append(' Obligatorio');
            $("#familia").focus();
            return false;
        } 
        
        if ($("#edicion").val() === '') { 
            $("#msj_edicion").append(' Obligatorio');
            $("#edicion").focus();
            return false;
        } 
        
        if ($("#version").val() === '') { 
            $("#msj_version").append(' Obligatorio');
            $("#version").focus();
            return false;
        } 
        
        if ($("#precio").val() === '') { 
            $("#msj_precio").append(' Obligatorio');
            $("#precio").focus();
            return false;
        }       
        
        if ($("#precio").val() <= 0){
            $("#msj_precio").append(' Precio superior a 0');
            $("#precio").focus();
            return false;
        }
        
        $.post("ajax/verifProducto.php", { fabricante : $("#fabricante").val(), familia : $("#familia").val(), 
        edicion : $("#edicion").val(), version : $("#version").val(), token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].existe > 0){
                $.alert.open('warning', "Alerta", "Ya existe este producto", {'Aceptar' : 'Aceptar'}, function(){
                    limpiarCombos();
                });
                return false;
            }

            row = "<tr id='fila" + i + "'>";
            row += "<td>";
            row += "<input type='hidden' id='iden" + i + "' name='iden[]' value='" + $("#fabricante").val() + "*" + $("#familia").val() + "*" + $("#edicion").val() + "*" + $("#version").val() + "*" + $("#precio").val() + "'>";
            row += $("#fabricante option:selected").text();
            row += "</td>";
            row += "<td>";
            row += $("#familia option:selected").text();
            row += "</td>";
            row += "<td>";
            row += $("#edicion option:selected").text();
            row += "</td>";
            row += "<td>";
            row += $("#version option:selected").text();
            row += "</td>";
            row += "<td class='text-right'>";
            row += $("#precio").val();
            row += "</td>";
            row += "<td class='text-center'>";
            row += '<a href="#" onclick="eliminar(i)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>';
            row += "</td>";
            row += "</tr>";
            $("#tabla").append(row);
            
            limpiarCombos();
            
            i++;
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'});
        });
    }
    
    function eliminar(indice){
        $("#fila" + indice).remove();
        if($("#tabla tr").length === 0){
            i = 0;
        }
    }
    
    function limpiarCombos(){
        $("#fabricante").val("");
        $("#familia").val("");
        $("#edicion").val("");
        $("#version").val("");
        $("#precio").val("");
        $("#fabricante").focus();
    }
</script>