<?php
if ($exito == true) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/equivalencias/';
            }
        });
    </script>
<?php
}
?>
        
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Lista de Productos</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="modificar" id="modificar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id_equivalencia ?>">
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000">
            <?php
            if ($error == 5) {
                echo $office->error;
            } else if ($error == 3) {
                echo "Ya existe ese producto";
            }
            ?></font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Fabricante:</th>
                <td align="left">
                    <select id="fabricante" name="fabricante">
                        <option value="">Seleccione</option>
                        <?php
                        foreach ($fabricante as $row) {
                            ?>
                            <option value="<?= $row["idFabricante"] ?>" <?php if ($office2->idFabricante == $row["idFabricante"]) {
                                echo "selected='selected'";
                            } ?>><?= $row["nombre"] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fabricante") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Familia:</th>
                <td align="left">
                    <select id="familia" name="familia">
                        <option value="">Seleccione</option>
                        <?php
                        foreach ($familia as $row) {
                            ?>
                            <option value="<?= $row["idProducto"] ?>" <?php if ($office2->idFamilia == $row["idProducto"]) {
                            echo "selected='selected'";
                        } ?>><?= $row["nombre"] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <!--<input name="familia" id="familia" type="text" value="<?//= $_POST['familia'] ?>" size="30" maxlength="250"  />-->
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_familia") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Edicion:</th>
                <td align="left">
                    <select id="edicion" name="edicion">
                        <option value="">Seleccione</option>
                        <?php
                        foreach ($edicion as $row) {
                            ?>
                            <option value="<?= $row["idEdicion"] ?>" <?php if ($office2->idEdicion == $row["idEdicion"]) {
                            echo "selected='selected'";
                        } ?>><?= $row["nombre"] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <!--<input name="edicion" id="edicion" type="text" value="<?//= $_POST['edicion'] ?>" size="30" maxlength="250"  />-->
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_edicion") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Version:</th>
                <td align="left">
                    <select id="version" name="version">
                        <option value="">Seleccione</option>
                        <?php
                        foreach ($version as $row) {
                            ?>
                            <option value="<?= $row["id"] ?>" <?php if ($office2->idVersion == $row["id"]) {
                            echo "selected='selected'";
                        } ?>><?= $row["nombre"] ?></option>
                        <?php
                    }
                    ?>
                    </select>
                    <!--<input name="version" id="version" type="text" value="<?//= $_POST['version'] ?>" size="30" maxlength="250"  />-->
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_version") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Precio:</th>
                <td align="left"><input name="precio" id="precio" type="text" value="<?= $office2->precio ?>" size="14" maxlength="20"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_precio") ?></font></div></td>
            </tr>
         
            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
        $("#orden").numeric(false);
    });
</script>