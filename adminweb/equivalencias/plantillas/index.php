<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>
<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroFabricante" name="filtroFabricante" style="width:120px;" value="<?= $fab ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroFamilia" name="filtroFamilia" style="width:120px;" value="<?= $fam ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroEdicion" name="filtroEdicion" style="width:120px;" value="<?= $edi ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroVersion" name="filtroVersion" style="width:120px;" value="<?= $ver ?>"></th>
            <th  align="center" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer">Buscar</div></th>
            <th  align="center" valign="middle" class="til" ><div class="botonBuscar pointer" onclick="location.href='reportes/productos.php';">Exportar</div></th>
            <th  align="center" valign="middle" class="til" >&nbsp;</th>
        </tr>
        
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til" id="fabricante" name="fabricante">Fabricante</strong></th>
            <th  align="center" valign="middle" ><strong class="til" id="familia" name="familia">Familia</strong></th>
            <th  align="center" valign="middle" ><strong class="til" id="edicion" name="edicion">Edici&oacute;n</strong></th>
            <th  align="center" valign="middle" ><strong class="til" id="version" name="version">Versi&oacute;n</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Precio</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Modificar</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                <?= $registro["fabricante"] ?>
                </td>
                <td align="left">
                <?= $registro["familia"] ?>
                </td>
                <td align="left">
                <?= $registro["edicion"] ?>
                </td>
                <td align="left">
                <?= $registro["version"] ?>
                </td>
                <td  align="center"><?= $registro["precio"] ?></td>
                <td  align="center">
                    <a href="modificar.php?id=<?= $registro["id"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>

                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div style="text-align:center; width:90%; margin:0 auto" id="paginador"><?= $pag->print_paginator("") ?></div>

<?php if($count == 0){ ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay productos</div>
<?php } ?>

<script>
    $(document).ready(function(){
        $("#filtroFabricante").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroFamilia").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroEdicion").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroVersion").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#buscar").click(function(){
            buscarData();
        });
    });
    
    function buscarData(){
        $.post("ajax/tablaEquivalencia.php", { fabricante : $("#filtroFabricante").val(), familia : $("#filtroFamilia").val(), 
        edicion : $("#filtroEdicion").val(), version : $("#filtroVersion").val(), pagina : 0, token : localStorage.licensingassuranceToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb";
                return false;
            }
            $("#bodyTable").empty();
            $("#paginador").empty();
            $("#bodyTable").append(data[0].tabla);
            $("#paginador").append(data[0].paginador);
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>