<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware
//
// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$office    = new Equivalencias();
$office2   = new Equivalencias();
$validator = new validator("form1");
$general = new General();

$id_equivalencia = 0;
if(isset($_REQUEST['id']) && filter_var($_REQUEST['id'], FILTER_VALIDATE_INT) !== false){
    $id_equivalencia = $_REQUEST['id'];
}

$office2->datos3($id_equivalencia);
$error = 0;
$exito = 0;

$fabricante = $office->listar_fabricantes();
$familia    = $office->listar_familias();
$edicion    = $office->listar_ediciones();
$version    = $office->listar_versiones();

if (isset($_POST['modificar'])) {

    if ($error == 0) {
        if(isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && isset($_POST["fabricante"]) && 
        filter_var($_POST['fabricante'], FILTER_VALIDATE_INT) !== false && isset($_POST["edicion"]) && 
        filter_var($_POST['edicion'], FILTER_VALIDATE_INT) !== false && isset($_POST["version"]) && 
        filter_var($_POST['version'], FILTER_VALIDATE_INT) !== false&& isset($_POST["precio"]) && 
        filter_var($_POST['precio'], FILTER_VALIDATE_FLOAT) !== false){
           
            if ($office->existeEquivalencia($_POST['fabricante'], $_POST['familia'], $_POST['edicion'], 
            $_POST['version']) == 0 || ($office2->idFabricante == $_POST['fabricante'] && 
            $office2->idFamilia == $_POST['familia'] && $office2->idEdicion && $office2->idVersion == $_POST['version'])) {
                if ($office->actualizar($_POST['id'], $_POST['fabricante'], $_POST['familia'], $_POST['edicion'], $_POST['version'], $_POST['precio'])) {
                    $exito = 1;
                } else {
                    $error = 5;
                }
            } else {
                $error = 3;
            }
        }
    }
}

$validator->create_message("msj_fabricante", "fabricante", " Obligatorio", 0);
$validator->create_message("msj_familia", "familia", " Obligatorio", 0);
$validator->create_message("msj_edicion", "edicion", " Obligatorio", 0);
$validator->create_message("msj_version", "version", " Obligatorio", 0);
$validator->create_message("msj_precio", "precio", " Obligatorio", 0);