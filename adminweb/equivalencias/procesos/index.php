<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");

// Objetos
$office = new Equivalencias();
$general = new General();

$fab = '';
if(isset($_GET['fab'])){
    $fab = $_GET['fab'];
}

$fam = '';
if(isset($_GET['fam'])){
    $fam = $_GET['fam'];
}

$edi = '';
if(isset($_GET['edi'])){
    $edi = $_GET['edi'];
}

$ver = '';
if(isset($_GET['ver'])){
    $ver = $_GET['ver'];
}

if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   =  '&fab=' . $fab . '&fam=' . $fam . '&edi=' . $edi . '&ver=' . $ver;  
} else {
    $start_record = 0;
    $parametros   = '';
}

$listado = $office->listar_todo_paginado($fab, $fam, $edi, $ver, $start_record);
$count = $office->total($fab, $fam, $edi, $ver);                                

$pag = new paginator($count, $general->limit_paginacion, 'index.php?', $parametros);
$i = $pag->get_total_pages();