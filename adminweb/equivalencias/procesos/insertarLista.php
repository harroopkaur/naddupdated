<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");

// Objetos
$office = new Equivalencias();
$general = new General();

$fabricante = $office->listar_fabricantes();
$familia    = $office->listar_familias();
$edicion    = $office->listar_ediciones();
$version    = $office->listar_versiones();

$agregar = 0;
$exito = 0;

if (isset($_POST['agregar']) && filter_var($_POST["agregar"], FILTER_VALIDATE_INT) !== false && $_POST["agregar"] == 1){
    // Validaciones
    $tabIngresar = $_POST["iden"];
    $agregar = 1;
    
    $j = 0;
    for ($index = 0; $index < count($_POST["iden"]); $index++){
        $array = explode("*", $tabIngresar[$index]);
       
        $fabricante = 0;
        if(is_numeric($array[0])){
            $fabricante = $array[0];
        }
        
        $familia = 0;
        if(is_numeric($array[1])){
            $familia = $array[1];
        }
        
        $edicion = 0;
        if(is_numeric($array[2])){
            $edicion = $array[2];
        }
        
        $version = 0;
        if(is_numeric($array[3])){
            $version = $array[3];
        }
        
        $precio = 0;
        if(is_numeric($array[4])){
            $precio = $array[4];
        }
        
        if ($fabricante > 0 && $familia > 0 && $edicion > 0 && $version > 0 && $precio > 0) {
            if ($office->insertar1($fabricante, $familia, $edicion, $version, $precio)) {
                $j++;
            }    
        } 
    }
    
    if($j == count($_POST["iden"])){
        $exito = 1;
    } else if($j < count($_POST["iden"]) && $j > 0){
        $exito = 2;
    }
}