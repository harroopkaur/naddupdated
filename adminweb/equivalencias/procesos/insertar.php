<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$office = new Equivalencias();
$validator = new validator("form1");

$fabricante = $office->listar_fabricantes();
$familia    = $office->listar_familias();
$edicion    = $office->listar_ediciones();
$version    = $office->listar_versiones();
$general = new General();

$error = 0;
$exito = 0;

if (isset($_POST['insertar'])) {
    // Validaciones
    if(isset($_POST['fabricante']) && filter_var($_POST['fabricante'], FILTER_VALIDATE_INT) !== false && 
    isset($_POST['familia']) && filter_var($_POST['familia'], FILTER_VALIDATE_INT) !== false && isset($_POST['edicion']) 
    && filter_var($_POST['edicion'], FILTER_VALIDATE_INT) !== false && isset($_POST['version']) && filter_var($_POST['version'], FILTER_VALIDATE_INT)){
        
        if ($error == 0) {
            if($office->existeEquivalencia($_POST['fabricante'], $_POST['familia'], $_POST['edicion'], $_POST['version']) == 0){
                if ($office->insertar1($_POST['fabricante'], $_POST['familia'], $_POST['edicion'], $_POST['version'], $_POST['precio'])) {
                    $exito = 1;
                } else {
                    $error = 5;
                }    
            }
            else{
                $error = 3;
            }
        }
    }
}

$validator->create_message("msj_fabricante", "fabricante", " Obligatorio", 0);
$validator->create_message("msj_familia", "familia", " Obligatorio", 0);
$validator->create_message("msj_edicion", "edicion", " Obligatorio", 0);
$validator->create_message("msj_version", "version", " Obligatorio", 0);
$validator->create_message("msj_precio", "precio", " Obligatorio", 0);