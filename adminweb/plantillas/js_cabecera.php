<style>
body {
    background: #00B0F0 url(<?=$GLOBALS['domain_root']?>/imagenes/inicio/bg_body.jpg) top left no-repeat !important;
    padding:0px !important;		
}

div.box {font-size:108.3%;margin:2px 0 15px;padding:20px 15px 20px 65px;-moz-border-radius:6px;-webkit-border-radius:6px;border-radius:6px;/*behavior:url(http://www.yourinspirationweb.com/tf/bolder/wp-content/themes/bolder/PIE.htc);*/}
div.success-box {background:url("<?=$GLOBALS['domain_root']?>/img/check.png") no-repeat 15px center #ebfab6;border:1px solid #bbcc5b;color:#599847;}

div.error-box {
    background:#00B0F0 url("<?=$GLOBALS['domain_root']?>/img/error.png") no-repeat 15px center #fdd2d1;
    border: 1px solid #f6988f;
    color: #883333;
}
</style>