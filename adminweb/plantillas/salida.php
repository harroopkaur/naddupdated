<?php
require_once("../../configuracion/inicio.php");
session_unset();                 // Vacia las variables de sesion
session_destroy();               // Destruye la sesion

header("location: " . $GLOBALS["domain_root"] . "/adminweb/");
?>
