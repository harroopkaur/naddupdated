<style>
    ul, ol {
            list-style:none;
    }

    .nav {
        width:140px; /*Le establecemos un ancho*/
        margin:0 auto; /*Centramos automaticamente*/
        margin:5px;
        margin-left:10px;
        float:left;	
        width:120px;
        padding:10px;
        border-radius:10px;
        text-align:center;
        cursor:pointer;
        background:#000;
        color:#fff;
    }
    
    .nav:hover {
        border-radius:10px 10px 0px 0px;
    }

    .nav li a {
        background-color:#000;
        color:#fff;
        text-decoration:none;
        display:block;
    }
    
    .nav li a:hover {
        background-color:#434343;
    }

    .nav li ul {
        margin-top: 0px;
        margin-left: -10px;
        display:none;
        position:absolute;
        min-width:160px;
    }

    .nav li:hover > ul {
        display:block;
    }
    
    .navSelected{
        background-color:#ffffff;
        color:#000000;
    }
    
    /*inicio submenu*/
    .nav li ul li{
        position: relative;
    }
    
    .nav li ul li ul{
        right: -140px;
        top: 0;
    }
    /*fin submenu*/
</style>


<div class="botones_m <?php if($opcionm1==1){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/inicio.php';" >
Inicio
</div>
<div class="botones_m <?php if($opcionm1==2){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/usuarios/';" >
Usuarios
</div>
<div class="botones_m <?php if($opcionm1==3){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/clientes/';">
Clientes
</div>
<div class="botones_m <?php if($opcionm1==9){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/empleados/';">
Empleados
</div>
<div class="botones_m <?php if($opcionm1==4){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/offices/';" >
Office
</div>
<div class="botones_m <?php if($opcionm1==5){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/equivalencias/';" >
Productos
</div>
<ul class="nav <?php if($opcionm1==6){ echo 'navSelected';   } ?>">
    <li><a href="" <?php if($opcionm1==6){ echo "style='background-color:#ffffff; color:#000000; font-weight:bold;'";   } ?>>Configuraciones</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/conversiones/">Conversiones</a></li>
            <li><a href="#">Despliegue</a>
                <ul>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=1">Adobe</a></li>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=3">Microsoft</a></li>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=5">SAP</a></li>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=7">UNIX-IBM</a></li>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=8">UNIX-Oracle</a></li>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=6">VMWare</a></li>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=2">Windows-IBM</a></li>
                    <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/despliegue/index.php?fab=4">Windows-Oracle</a></li>
                </ul>
            </li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/ediciones/">Edici&oacute;n</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/fabricantes/">Fabricante</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/familias/">Familia</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/formularioCompras/" target="_blank">Formulario Compras</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/modeloProcesadores/">Modelo Procesadores</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/LADOutput/">Password LAD_Output</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/permisos/">Permisos</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/procesadores/">Procesadores</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/pvu/">PVU</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/versiones/">Versi&oacute;n</a></li>
        </ul>
    </li>
</ul>
<div class="botones_m <?php if($opcionm1==7){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/poc/'" >
POC
</div>
<ul class="nav <?php if($opcionm1==8){ echo 'navSelected';   } ?>">
    <li><a href="" <?php if($opcionm1==8){ echo "style='background-color:#ffffff; color:#000000; font-weight:bold;'";   } ?>>Cotizaciones</a>
        <ul>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/licensing/">Licensing</a></li>
            <li><a href="<?= $GLOBALS["domain_root"] ?>/adminweb/cotizaciones/proveedor/">Proveedores</a></li>
        </ul>
    </li>
</ul>
<div class="botones_m <?php if($opcionm1==10){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/licenciamiento/';" >
Licenciamiento
</div>
<div class="botones_m <?php if($opcionm1==15){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/centralizador/';" >
Centralizador
</div>
<div class="botones_m <?php if($opcionm1==13){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/NADD/';" >
NADD
</div>
<div class="botones_m <?php if($opcionm1==11){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/visitas/';" >
Visitas
</div>
<div class="botones_m <?php if($opcionm1==14){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/dashboard/';" >
Dashboard
</div>
<div class="botones_m <?php if($opcionm1==16){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/adminweb/recordatorios/';" >
Recordatorios
</div>