<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_office.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$office = new Office();
$office2 = new Office();
$oficce = new Office();
$validator = new validator("form1");
$general = new General();

$id_office = 0;
if (isset($_GET["id"])) {
    $id_office = $_GET["id"];
}
$office2->datos($id_office);
$error = 0;
$exito = 0;

if (isset($_POST['modificar'])) {
    if(isset($_POST["id"]) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && isset($_POST["familia"])
    && isset($_POST["edicion"]) && isset($_POST["version"]) && isset($_POST["precio"]) && 
    filter_var($_POST['precio'], FILTER_VALIDATE_FLOAT) !== false){
        if ($error == 0) {
            if ($office->actualizar($_REQUEST['id'], $general->get_escape($_POST['familia']), $general->get_escape($_POST['edicion']), 
            $general->get_escape($_POST['version']), $_POST['precio'])) {
                $exito = 1;
            } else {
                $error = 3;
            }
        }
    }       
}

$validator->create_message("msj_familia", "familia", " Obligatorio", 0);
$validator->create_message("msj_edicion", "edicion", " Obligatorio", 0);
$validator->create_message("msj_version", "version", " Obligatorio", 0);
$validator->create_message("msj_precio", "precio", " Obligatorio", 0);

