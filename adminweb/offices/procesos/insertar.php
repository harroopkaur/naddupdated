<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_office.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");


// Objetos
$office = new Office();
$oficce = new Office();
$validator = new validator("form1");
$general = new General();

$error = 0;
$exito = 0;

if(isset($_POST['insertar'])) {
    // Validaciones
    if(isset($_POST["familia"]) && isset($_POST["edicion"]) && isset($_POST["version"]) && isset($_POST["precio"]) && filter_var($_POST['precio'], FILTER_VALIDATE_FLOAT) !== false){ 
        if ($error == 0) {
            if ($oficce->insertar($general->get_escape($_POST['familia']), $general->get_escape($_POST['edicion']), 
            $general->get_escape($_POST['version']), $_POST['precio'])) {
                $exito = 1;
            } else {
                $error = 5;
            }
        }
    }
}

$validator->create_message("msj_familia", "familia", " Obligatorio", 0);
$validator->create_message("msj_edicion", "edicion", " Obligatorio", 0);
$validator->create_message("msj_version", "version", " Obligatorio", 0);
$validator->create_message("msj_precio", "precio", " Obligatorio", 0);