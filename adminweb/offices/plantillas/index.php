<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>

<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til">Office</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>precio</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Modificar</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    
    <tbody>
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                    <?= $registro["familia"] . ' ' . $registro["edicion"] . ' ' . $registro["version"] ?>
                </td>
                
                <td  align="center"><?= $registro["precio"] ?></td>

                <td  align="center">
                    <a href="modificar.php?id=<?= $registro["id"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                </td>
                
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>

<?php if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay Office</div>
<?php } ?>

<script>
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>