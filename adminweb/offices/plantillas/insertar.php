<?php
if ($exito == true) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/offices/';
            }
        });
    </script>
<?php
}
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de Office</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                echo $oficce->error;
            } ?></font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Familia:</th>
                <td align="left"><input name="familia" id="familia" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_familia") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Edicion:</th>
                <td align="left"><input name="edicion" id="edicion" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_edicion") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Version:</th>
                <td align="left"><input name="version" id="version" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_version") ?></font></div></td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Precio:</th>
                <td align="left"><input name="precio" id="precio" type="text" value="" size="14" maxlength="20"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_precio") ?></font></div>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="INSERTAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>