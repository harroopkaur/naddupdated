<?php
if ($exito == true) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/offices/';
            }
        });
    </script>
<?php
}
?>
    
<h1><strong style="color:#000; font-weight:bold;"></strong></h1>
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de Office</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="modificar" id="modificar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id_office ?>">
        <?php $validator->print_script(); ?>
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                echo $oficce->error;
            } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">

            <tr>
                <th width="90" align="left" valign="top">Familia:</th>
                <td align="left"><input name="familia" id="familia" type="text" value="<?= $office2->familia ?>" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_familia") ?></font></div></td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Edicion:</th>
                <td align="left"><input name="edicion" id="edicion" type="text" value="<?= $office2->edicion ?>" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_edicion") ?></font></div></td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Version:</th>
                <td align="left"><input name="version" id="version" type="text" value="<?= $office2->version ?>" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_version") ?></font></div></td>
            </tr>
            <tr>
                <th width="90" align="left" valign="top">Precio:</th>
                <td align="left"><input name="precio" id="precio" type="text" value="<?= $office2->precio ?>" size="14" maxlength="20"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_precio") ?></font></div></td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>

</fieldset>