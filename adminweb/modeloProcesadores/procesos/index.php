<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");

// Objetos
$maestra = new tablaMaestra();
$general = new General();

$nombre = '';
if (isset($_GET['nombre'])) {
    $nombre = $_GET['nombre'];
}

if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros = '&nombre=' . $nombre;
} else {
    $start_record = 0;
    $parametros = '';
}

// Listar Usuarios
$listaModeloProcesadores = $maestra->listar_todo_paginado($nombre, 9, $start_record);
$count = $maestra->total($nombre, 9);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?', $parametros);
$i = $pag->get_total_pages();