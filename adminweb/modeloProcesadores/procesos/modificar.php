<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$maestra = new tablaMaestra();
$validator = new validator("form1");
$general = new General();

$nombre = "";
$status = 0;

$id = 0;
if(isset($_GET["id"]) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id = $_GET["id"];
}

$detalle = $maestra->registroEspecifico(9, $id);
$nombre = $detalle["descripcion"];

$error = 0;
$exito = 0;

if (isset($_POST['modificar']) && $_POST["modificar"] == 1) {
    if(isset($_POST["id"]) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
        if ($error == 0) {
            $nombre = "";
            if(isset($_POST['nombre'])){
                $nombre = $general->get_escape($_POST['nombre']);
            }
                    
            if ($maestra->existe_nombre($nombre, 9, $_POST['id']) == 0) {
                if ($maestra->modificarDetalle($nombre, 9, $_POST['id'])) {
                    $exito = 1;
                } else {
                    $error = 3;
                }
            }
            else{
                $error = 1;
            }
        }
    }
}

$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);