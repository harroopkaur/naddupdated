<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $maestra = new tablaMaestra();
            $general = new General();

            $nombre   = "";
            if(isset($_POST["nombre"])){
                $nombre = $general->get_escape($_POST["nombre"]);
            }

            $start_record = 0;
            if(isset($_POST["pagina"]) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false){
                $start_record = ($_POST["pagina"] * $general->limit_paginacion)  - $general->limit_paginacion;
            }           

            $tabla = "";
            $listadoProcesadores = $maestra->listar_todo_paginado($nombre, 9, $start_record);
            foreach ($listadoProcesadores as $registro) {
                $tabla .= '<tr onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                    <td align="left">' . $registro["descripcion"] . '</td>
                    <td  align="center">
                        <a href="modificar.php?id=' . $registro["idDetalle"] . '"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                    </td>
                    <td  align="center">
                        <a href="#" onclick="eliminar(' . $registro["idDetalle"] . ')">
                        <img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                    </td>
                </tr>';
            }
            
            $count = $maestra->total($nombre, 9);
            
            $condiciones  =  '&nombre=' . $nombre;

            $pag = new paginator($count, $general->limit_paginacion, 'index.php?', $condiciones);
            $i = $pag->get_total_pages();

            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$pag->print_paginatorAjax(""), 'resultado'=>true));
        }
        
    }
    else{
        $general->eliminarSesion();
    }   
}
echo json_encode($array);
?>