<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="eliminar.php">
    <input type="hidden" id="id" name="id">
</form>
<table style="width:600px; margin:0 auto; margin-top:45px; margin-bottom:45px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroNombre" name="filtroNombre" style="width:90%;" value="<?= $nombre ?>"></th>
            <th  align="center" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer">Buscar</div></th>
            <th  align="center" valign="middle" class="til" style="width:70px;">&nbsp;</th>
        </tr>
        
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><strong class="til" id="nombre" name="nombre">Nombre</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Modificar</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Eliminar</strong></th>
        </tr> 
    </thead>
    <tbody id="bodyTable">
        <?php foreach ($listaModeloProcesadores as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left">
                    <?= $registro["descripcion"] ?>
                </td>
                <td  align="center">
                    <a href="modificar.php?id=<?= $registro["idDetalle"] ?>"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                </td>
                <td  align="center">
                    <a href="#" onclick="eliminar(<?= $registro['idDetalle'] ?>)"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>

                </td>
            </tr>
        <?php } ?>

    </tbody>
</table>
<div style="text-align:center; width:90%; margin:0 auto" id="paginador"><?= $pag->print_paginator("") ?>
</div>

<?php
if($count == 0) {
?>
    <div style="text-align:center; width:90%; margin:0 auto">No hay productos</div>
<?php } ?>

<script>
    $(document).ready(function () {
        $("#filtroNombre").keyup(function (e) {
            if(e.keyCode === 13){
                $.post("ajax/tablaModeloProcesadores.php", {nombre: $("#filtroNombre").val(), pagina: 1, token : localStorage.licensingassuranceToken }, function (data) {
                    if(data[0].resultado === true){
                        $("#bodyTable").empty();
                        $("#paginador").empty();
                        $("#bodyTable").append(data[0].tabla);
                        $("#paginador").append(data[0].paginador);
                    }
                    else{
                        location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                    }
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
        
        $("#buscar").click(function(){
            $.post("ajax/tablaModeloProcesadores.php", {nombre: $("#filtroNombre").val(), pagina: 1, token : localStorage.licensingassuranceToken }, function (data) {
                if(data[0].resultado === true){
                    $("#bodyTable").empty();
                    $("#paginador").empty();
                    $("#bodyTable").append(data[0].tabla);
                    $("#paginador").append(data[0].paginador);
                }
                else{
                    location.href = "<?= $GLOBALS["domain_root"] ?>/adminweb";
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
    });
    
    function eliminar(id){
        $.alert.open('confirm', 'Desea eliminar el registro', {Si: 'Si', No: 'No'}, function(button) {
            if (button === 'Si'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>