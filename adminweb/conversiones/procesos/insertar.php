<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$configuraciones = new configuraciones();
$general = new General();
$validator = new validator("form1");

$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    if(isset($_POST["codigo"]) && isset($_POST["os"])){
        if ($error == 0) {
            if($configuraciones->existeConversion($_POST['codigo'], $_POST['os']) > 0){
                $error = 1;
            }
            else{
                if ($configuraciones->insertarConverion($_POST['codigo'], $_POST['os'])) {
                    $exito = 1;
                } else {
                    $error = 5;
                }
            } 
        }
    }
}

$validator->create_message("msj_codigo", "codigo", " Obligatorio", 0);
$validator->create_message("msj_os", "os", " Obligatorio", 0);