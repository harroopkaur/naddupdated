<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$configuraciones = new configuraciones();
$validator = new validator("form1");
$general = new General();

$id = 0;
if (isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false) {
    $id = $_GET['id'];
}
$conversion = $configuraciones->conversionEspecifico($id);
$error = 0;
$exito = 0;

if (isset($_POST['modificar']) && $_POST["modificar"] == 1) {
    if (isset($_POST["codigo"]) && isset($_POST["os"])) {
        if ($error == 0) {
            if ($configuraciones->existeConversion($_POST['codigo'], $_POST['os']) > 0) {
                $error = 1;
            } else {
                if ($configuraciones->modificarConversion($_POST['id'], $_POST['codigo'], $_POST['os'])) {
                    $exito = 1;
                } else {
                    $error = 3;
                }
            }
        }
    }
}

$validator->create_message("msj_codigo", "codigo", " Obligatorio", 0);
$validator->create_message("msj_os", "os", " Obligatorio", 0);