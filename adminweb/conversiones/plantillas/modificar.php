<?php
if ($exito == true) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/conversiones/';
            }
        });
    </script>
<?php
}
?>

<fieldset style=" border: 1px solid #00B0F0;">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Conversi&oacute;n</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="modificar" id="modificar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id ?>">
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000">
            <?php
            if ($error == 5) {
                echo "Ocurrió un error al modificar";
            } else if ($error == 1) {
            ?>
                <script>
                    $.alert.open('alert', 'Ya existe ese conversión', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                        }
                    });
                </script>
            <?php
            }
            ?></font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">C&oacute;digo:</th>
                <td align="left">
                    <input type="text" id="codigo" name="codigo" maxlength="70" value="<?= $conversion["codigo"] ?>">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_codigo") ?></font></div>
                </td>
            </tr>

            <tr>
                <th width="130" align="left" valign="top">Sistema Operativo:</th>
                <td align="left">
                    <input type="text" id="os" name="os" maxlength="70" value="<?= $conversion["os"] ?>">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_os") ?></font></div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>