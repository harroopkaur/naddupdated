<?php
if ($exito == 1) { ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/';
            }
        });
    </script>
<?php
}        
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Modificar Datos del Empleado</span></legend>
    
    <form id="form1" name="form1" method="post" enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id_user ?>">
        <input type="hidden" name="modif" id="modif" value="<?= $autorizado ?>">
        <?php $validator->print_script(); ?>
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
            echo $empleados2->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Nombre:</th>
                <td align="left"><input name="nombre" id="nombre" type="text" value="<?= $empleados->nombre ?>" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nombre") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Apellido:</th>
                <td align="left"><input name="apellido" id="apellido" type="text" value="<?= $empleados->apellido ?>" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_apellido") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">E-mail:</th>
                <td align="left"><input name="email" id="email" type="text" value="<?= $empleados->correoEmpleado ?>" size="30" maxlength="70" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div>
                </td>
            </tr>

            <tr>
                <th width="90" align="left" valign="top">Empresa:</th>
                <td align="left"><select name="empresa" id="empresa">
                        <option value="" >--Seleccione--</option>
                        <?php
                        foreach ($empresas as $reg_p) {
                        ?>
                            <option value="<?= $reg_p["id"] ?>"  <?php if($reg_p["id"] == $empleados->id){ echo 'selected';  }  ?>><?= $reg_p["empresa"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">Login:</th>
                <td align="left"><input name="login" id="login" type="text" value="<?= $empleados->login ?>" size="30" maxlength="70" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">Contraseña:</th>
                <td align="left"><input name="clave" id="clave" type="password" value="<?= $empleados->clave ?>" size="30" maxlength="70" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_clave") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">Autorizado:</th>
                <td align="left">
                    <input type="checkbox" id="autorizado" name="autorizado" <?php if($empleados->estado == 1){ echo 'checked="checked" disabled="disabled"'; } ?>>
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
<script>
    $(document).ready(function(){
        $("#email").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/ajax/verificarCorreo.php", { email : $("#email").val(), 
            id : $("#id").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe el correo', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#login").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/ajax/verificarLogin.php", { login : $("#login").val(), 
            id : $("#id").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe el login', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#login").val("");
                            $("#login").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#autorizado").click(function(){
            if($("#autorizado").prop("disabled")){
                return false;
            }
            
            if($("#autorizado").prop("checked")){
                $("#modif").val(1);
            } else{
                $("#modif").val(0);
            }
        });
    });
</script>