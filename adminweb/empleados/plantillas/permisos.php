<script language="javascript" type="text/javascript">
    //validar que los modulos esten marcados
    function marcarPermiso(permiso, indice) {
        $("#fondo").show();
        var marca = 0;
        if ($('#permiso' + indice).prop('checked')) {
            marca = 1;
        }
        $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/ajax/permisos.php", { empleado : <?= $id_user ?>, marca : marca,
        permiso : permiso, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
            $("#fondo").hide();
        });
    }
</script>

<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
    <tr>
        <td colspan="2" align="right"><input name="insertar" type="button" id="insertar" value="VOLVER"  onclick="document.location = '<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/accesos.php?id=<?= $id_user ?>';" class="boton" /></td>
    </tr>
</table>

<form>
    <input type="checkbox" id="selectTodo" name="selectTodo" value="">Seleccionar Todo
    <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th colspan="3" style="text-align:center">Permisos</th>
        </tr>
        <?php
        if($permisos->listado()){
            $i = 0;
            $j = 0;
            $z = 0;
            foreach($permisos->lista as $row){
                if($i == $j){
                    $j = 3;
                ?>
                    <tr>
                <?php 
                }
                ?>        
                    <td>
                        <input type="checkbox" id="permiso<?= $z; ?>" name="permiso" <?php if ($moduloSam->existe_permisoEmpleado($id_user, $row["idPermisos"])) { echo "checked='checked'"; } ?> value="<?= $row["idPermisos"] ?>"
                        onclick = "marcarPermiso(<?= $row["idPermisos"] ?>, <?= $z ?>)">
                        <?php
                        echo "&nbsp;&nbsp;" . $row["nombre"];
                        ?>
                    </td>
                <?php 
                $i++;
                if($i == $j){
                    $i = 0;
                    $j = 0;
                ?>
                    </tr>
                <?php   
                }
                $z++;
            }
        }
        ?>
    </table>
</form>

<script>
    var k = <?= $z ?>;
    $(document).ready(function(){
        $("#selectTodo").click(function(){
            if ($("#selectTodo").prop("checked")){
                for (i = 0; i < k; i++){
                    $("#permiso" + i).prop("checked", "checked");
                    marcarPermiso($("#permiso" + i).val(), i);
                }
            } else{
                for (i = 0; i < k; i++){
                    $("#permiso" + i).prop("checked", "");
                    marcarPermiso($("#permiso" + i).val(), i);
                }
            }
        });
    });
</script>