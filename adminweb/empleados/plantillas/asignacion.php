<?php
if ($agregar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar las asignaciones', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = 'asignacion.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 1) {
    ?>
    <script type="text/javascript">
        $.alert.open('info', 'Asignaciones actualiadas con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = 'asignacion.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php 
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se actualizaron todas las asignaciones', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = 'asignacion.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php    
}
?>
    
<form id="form1" name="form1" method="post" action="asignacion.php">
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <td colspan="2" align="right"><input name="insertarAsig" type="submit" id="insertarAsig" value="MODIFICAR"  onclick="" class="boton" /></td>
        </tr>
    </table>

    <input type="hidden" id="insertar" name="insertar" value="1">
    <input type="hidden" id="id" name="id" value="<?= $id_user ?>">
    
    <input type="checkbox" id="selectTodo" name="selectTodo" value="">Seleccionar Todo
    <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
        <thead>
            <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                <th colspan="4" style="text-align:center">Asignaciones</th>
            </tr>
        </thead>
        <tbody  id="tablaBody">
        <?php
            $i=0;
            $j=0;
            $k=0;
            foreach($listado as $row){
                if($i == $j){
                    $j = 4;
                ?>
                    <tr>
                <?php 
                }
                ?>        
                    <td>
                        <input type="checkbox" id="idAsignacion<?= $k ?>" name="idAsignacion[]" value="<?= $row["idAsignacion"] ?>" 
                        <?php 
                        foreach($listadoEmpleado as $row1){
                            if($row["idAsignacion"] == $row1["idAsignacion"]){
                                echo "checked='checked';";
                            }
                        }
                        ?>
                        ><?= $row["nombre"] ?>
                    </td>
                <?php 
                $i++;
                $k++;
                if($i == $j){
                    $i = 0;
                    $j = 0;
                ?>
                    </tr>
                <?php   
                }
            }
        ?>
        </tbody>
    </table>
</form>
    
<script>
    var k = <?= $k ?>;
    $(document).ready(function(){
        $("#selectTodo").click(function(){
            if ($("#selectTodo").prop("checked")){
                for (i = 0; i < k; i++){
                    $("#idAsignacion" + i).prop("checked", "checked");
                }
            } else{
                for (i = 0; i < k; i++){
                    $("#idAsignacion" + i).prop("checked", "");
                }
            }
        });
    });
</script>