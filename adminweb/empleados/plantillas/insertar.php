<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Registro insertado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/accesos.php?id=<?= $id_user ?>';
            }
        });
    </script>
<?php  
} else if ($agregar == 1 && $exito == 0){
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar el registro', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/';
            }
        });
    </script>
<?php  
}
?>
    
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos del Empleado</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <?php $validator->print_script(); ?>
        <div class="error_prog">
            <font color="#FF0000">
                <?php if ($error == 5) {
                    echo $clientes->error;
                } ?>
            </font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Nombre:</th>
                <td align="left">
                    <input name="nombre" id="nombre" type="text" value="" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nombre") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Apellido:</th>
                <td align="left">
                    <input name="apellido" id="apellido" type="text" value="" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_apellido") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">E-mail:</th>
                <td align="left">
                    <input name="email" id="email" type="text" value="" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Empresa:</th>
                <td align="left">
                    <select  id="empresa" name="empresa">
                        <option value="">--Seleccione--</option>
                        <?php
                        foreach($empresas as $row){
                        ?>
                            <option value="<?= $row["id"] ?>"><?= $row["empresa"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Login:</th>
                <td align="left">
                    <input name="login" id="login" type="text" value="" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?><?php if ($error == 2) {
                        echo "El login ya existe";
                    } ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Contrase&ntilde;a:</th>
                <td align="left"><input name="clave" id="clave" type="password" value="" size="14" maxlength="20"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_clave") ?><?php if ($error == 3) {
                        echo "Contrase�a m�nima de 6 caracteres";
                    } ?></font></div>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="CREAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
        $("#email").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/ajax/verificarCorreo.php", { email : $("#email").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe el correo', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#login").blur(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/adminweb/empleados/ajax/verificarLogin.php", { login : $("#login").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Ya existe el login', {'Aceptar': 'Aceptar'}, function(button) {
                        if (button === 'Aceptar'){
                            $("#login").val("");
                            $("#login").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
    });
</script>