<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos del Empleado</span></legend>
    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <th width="150" align="left" valign="middle">Nombre:</th>
            <td align="left" valign="middle"><?= $empleados->nombre ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Apellido:</th>
            <td align="left" valign="middle"><?= $empleados->apellido ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">E-mail:</th>
            <td align="left" valign="middle"><?= $empleados->correoEmpleado ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Empresa:</th>
            <td align="left" valign="middle"><?= $clientes->empresa ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">estado:</th>
            <td align="left" valign="middle"><?php if($empleados->estado=='1'){ echo'Autorizado';  } else{ echo'No Autorizado';  }  ?></td>
        </tr>
       
        <tr>
            <th width="150" align="left" valign="middle">Fecha registro:</th>
            <td align="left" valign="middle"><?= $general->muestrafecha($empleados->fecha_registro) ?></td>
        </tr>
    </table>
</fieldset>