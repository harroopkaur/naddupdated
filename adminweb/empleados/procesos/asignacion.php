<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_asignacion_cliente.php");
require_once($GLOBALS["app_root"] . "/clases/clase_asignacion_cliente_empleado.php");

// Objetos
$asignacion = new asignacionCliente();
$asignacionEmpleado  = new asignacionClienteEmpleado();
$general = new General();
//$paises = new Pais();

$exito   = 1;

$id_user = 0;
$cliente = 0;
$agregar = 0;

if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
    $cliente = $asignacionEmpleado->obtenerCliente($id_user);
}

$listado = $asignacion->listar_asignacion($cliente);
$listadoEmpleado = $asignacionEmpleado->listar_asignacion($id_user);

if (isset($_POST['insertar']) && filter_var($_POST['insertar'], FILTER_VALIDATE_INT) !== false && $_POST['insertar'] == 1) {
    $agregar = 1;
    // Validaciones
    if(isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false){
        $id_user = $_POST['id']; 
    }
   
    $asig = $_POST["idAsignacion"];
    $j = 0;
    for($i = 0; $i < count($asig); $i++){
        if(filter_var($asig[$i], FILTER_VALIDATE_INT) !== false){
            $j++;
        }
    }
    
    if($i != $j){
        $exito = 0;
    }
    
    if ($exito == 1) {
        $cliente = $asignacionEmpleado->obtenerCliente($id_user);
        $asignacionEmpleado->anularAsignaciones($id_user);
        
        $j = 0;
        for($i = 0; $i < count($asig); $i++){
            if ($asignacionEmpleado->existeAsignacion($asig[$i], $id_user)) {
                if($asignacionEmpleado->actualizar($asig[$i], $id_user, 1)){
                    $j++;
                }
            } else {
                if($asignacionEmpleado->insertar($asig[$i], $cliente, $id_user)){
                    $j++;
                }
            }
        }
        
        if($j != $i){
            $exito = 2;
        }
    }
}