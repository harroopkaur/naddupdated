<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");

// Objetos
$clientes = new Clientes();
$empleados = new empleados();
$general = new General();

//procesos
$id_user = 0;

if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id'];
    $empleados->datos($id_user);
    $clientes->datos($empleados->id);
}
else{
    $empleados->datos($id_user);
    $clientes->datos($id_user);
}