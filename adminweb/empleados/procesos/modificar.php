<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");

// Objetos
$clientes = new Clientes();
$empleados = new empleados();
$general = new General();
$validator = new validator("form1");

//procesos
$error = 0;
$exito = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$empleados->datos($id_user);
$empresas = $clientes->listar_todo();

$autorizado = $empleados->estado; 
if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && isset($_POST["email"]) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false
&& isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["login"]) && isset($_POST["clave"]) && isset($_POST["empresa"])
&& filter_var($_POST["empresa"], FILTER_VALIDATE_INT) !== false) {   
    if ($error == 0) {        
        $autorizado = 0;
        if(isset($_POST["modif"]) && $_POST["modif"] == 1){
            $autorizado = 1;
        }
                
        if ($empleados->actualizar($_POST['id'], $general->get_escape($_POST['nombre']), $general->get_escape($_POST['apellido']), 
        $general->get_escape($_POST['email']), $_POST["empresa"], $general->get_escape($_POST['login']), $general->get_escape($_POST["clave"]),
        $autorizado)) {
            $exito = 1;
        } else {
            $error = 3;
        }
    }
}

$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);
$validator->create_message("msj_apellido", "apellido", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);
$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_login", "login", " Obligatorio", 0);
$validator->create_message("msj_clave", "clave", " Obligatorio", 0);