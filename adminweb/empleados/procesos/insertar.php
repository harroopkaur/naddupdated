<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_empleados.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$empleados = new empleados();
$clientes = new Clientes();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

if (isset($_POST['insertar']) && isset($_POST['empresa']) && filter_var($_POST['empresa'], FILTER_VALIDATE_INT) !== false 
    && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
    // Validaciones
    $agregar = 1;
   
    if ($empleados->login_existe($_POST['login'], 0)) {
        $error = 2;
    }  // login duplicado
    if (strlen($_POST['clave']) < 6) {
        $error = 3;
    }  // Contrase�a m�nima de 6 caracteres

    if ($error == 0) {
        if ($empleados->insertar($general->get_escape($_POST['nombre']), $general->get_escape($_POST['apellido']), 
        $general->get_escape($_POST['email']), $_POST['empresa'], $general->get_escape($_POST['login']), 
        $general->get_escape($_POST['clave']))) {
            $id_user = $empleados->ultimo_id();
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$empresas = $clientes->listar_todo();

$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);
$validator->create_message("msj_apellido", "apellido", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail válido", 3);
$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_login", "login", " Obligatorio", 0);
$validator->create_message("msj_clave", "clave", " Obligatorio", 0);