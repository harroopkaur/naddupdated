<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");
require_once($GLOBALS["app_root"] . "/clases/clase_permisos.php");

// Objetos
$permisos  = new Permisos();
$validator = new validator("form1");
$general = new General();
//$paises = new Pais();

$error   = 0;
$exito   = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$moduloSam = new ModuloSam($conn);