<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            if($nuevo_middleware->compararAjax($_POST["token"])){
                $perfil_modulo= new ModuloSam($conn);

                if(isset($_POST['empleado']) && filter_var($_POST['empleado'], FILTER_VALIDATE_INT) !== false && isset($_POST['marca']) 
                && filter_var($_POST['marca'], FILTER_VALIDATE_INT) !== false && isset($_POST['permiso']) 
                && filter_var($_POST['permiso'], FILTER_VALIDATE_INT) !== false && $_POST['empleado'] != 0 && ($_POST['marca'] == 0 || $_POST['marca'] == 1) 
                && $_POST['permiso'] != 0){

                    $aprobado = true;
                    $id_user = $_POST['empleado'];
                    $marca   = $_POST['marca'];
                    $permiso = $_POST['permiso'];

                    ///***verificar los modulos insertado o elminados****////
                    if($marca==0){
                        $perfil_modulo->eliminarPermisoEmpleado($id_user, $permiso);
                    } 
                    else {
                        if($permiso == 22){
                            $perfil_modulo->insertarPermisoEmpleado($id_user, $permiso);
                            if(!$perfil_modulo->existe_permisoEmpleado($id_user, 1)){
                                $perfil_modulo->insertarPermisoEmpleado($id_user, 1);
                            }
                        }else{
                            $perfil_modulo->insertarPermisoEmpleado($id_user, $permiso);
                        }
                    }
                }
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);