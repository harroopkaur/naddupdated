<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$maestra = new tablaMaestra();
$validator = new validator("form1");
$general = new General();

$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    if(isset($_POST["nombre"])){
        $existe = $maestra->existe_nombre($_POST['nombre'], 8, 0);
        if($existe > 0){
            $error = 1;
        }
        else{
            $idDetalle = (int)$maestra->obtenerUltIdDetalle(8) + 1;
            if ($maestra->insertarDetalle($general->get_escape($_POST['nombre']), 8, $idDetalle)) {
                $exito = 1;
            } else {
                $error = 5;
            }
        }
    }
}

$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);