<?php require("../configuracion/inicio.php"); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>.:Licensing Assurance:.</title>
        <!-- Custom Theme files -->
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <meta name="keywords" content="trial, sofware" />
        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
        <script src="js/jquery.min.js"></script>
    </head>
    <body>
        <!--sign up form start here-->
        <div class="app">
            <div class="top-bar" style="overflow:hidden;">
                <h1 style="font-size:36px; font-weight:bold; color:#fff;">Administrador Webtool</h1>
                <div class="navg" style="overflow:hidden; padding:5px; width:100%;">
                    <img src="../imagenes/inicio/logo2.png" style=" width:70%; margin:0 auto; display:block;">

                </div>

            </div>
            <div style="width:100%; margin:0px; padding:0px; background-color:#D9D9D9; overflow:hidden;">
                <form id="form1" name="form1" method="post" action="autenticar.php">
                    <div style="width:95%; margin:0 auto; margin-top:10px; overflow:hidden; background:#7f7f7f;">
                        <h1 style="font-size:16px; font-weight:bold; color:#fff; margin-top:10px;">Inicio de Sesi&oacute;n</h1>
                        
                        <!--<input type="hidden" name="clave" id="clave" value="<?//= $_GET['rest'] . $_GET['rels'] ?>" />-->
                        <input type="hidden" name="entrar" id="entrar" value="1" />
                        <input type="text" name="login" id="login" autofocus value="" placeholder="Usuario" onfocus="this.value = '';" onblur="if (this.value == '') {
                                    this.value = '';
                                }"/>
                        <input type="password" name="contrasena" id="contrasena" value="" placeholder="*****" onfocus="this.value = '';" onblur="if (this.value == '') {
                                    this.value = '';}"/>
                    </div>
                    <input type="submit" value="Entrar" />
                </form>
                <?php if (isset($_GET['error']) == 1) { ?>
                    <div align="center" class="error_prog">Login o Contrase&ntilde;a inv&aacute;lidos</div>
                <?php } ?>
                <p><a href="olvido.php">&iquest; Olvid&oacute; Contrase&ntilde;a?</a></p>

            </div>

        </div>
        <div class="copyright" >
            <p>Todos Los Derechos Reservados 2016 - <a href="https://www.licensingassurance.com"> www.licensingassurance.com </a></p>
        </div>
        <!--sign up form end here-->
    </body>
</html>