<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_permisos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $permisos = new Permisos();
            $general = new General();

            $nombre   = "";
            if(isset($_POST["nombre"])){
                $nombre = $general->get_escape($_POST["nombre"]);
            }

            $start_record = 0;
            if(isset($_POST["pagina"]) && filter_var($_POST['pagina'], FILTER_VALIDATE_INT) !== false){
                $start_record = $_POST["pagina"];
            }

            $page_size    = 50;

            $count = 0;
            if($permisos->total($nombre)){
                $count = $permisos->result["cantidad"];
            }

            $condiciones  =  '&nombre=' . $nombre;

            $pag = new paginator($count, $general->limit_paginacion, 'index.php?', $condiciones);
            $i = $pag->get_total_pages();

            $tabla = "";
            if($permisos->listar_todo_paginado($nombre, $start_record)){
                foreach ($permisos->lista as $registro) {
                    $tabla .= '<tr onmouseover="this.style.backgroundColor = \'#DEDEDE\'" onmouseout="this.style.backgroundColor = \'#FFFFFF\'">
                        <td align="left">' . $registro["nombre"] . '</td>
                        <td align="left">' . $registro["status"] . '</td>
                        <td  align="center">
                            <a href="modificar.php?id=' . $registro["idPermisos"] . '"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Editar" title="Editar" /></a>
                        </td>
                        <td  align="center">
                            <a href="#" onclick="eliminar(' . $registro["idPermisos"] . ')">
                            <img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Eliminar" title="Eliminar" /></a>
                        </td>
                    </tr>';
                }
            }

            $paginador = $pag->print_paginatorAjax("");
            $array = array(0=>array('tabla'=>$tabla, 'paginador'=>$paginador, 'resultado'=>true));
        }
        
    }
    else{
        $general->eliminarSesion();
    }   
}
echo json_encode($array);
?>