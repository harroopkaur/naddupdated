<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario.php");
require_once($GLOBALS["app_root"] . "/clases/clase_permisos.php");

// Objetos
$usuario = new Usuario();
$permisos = new Permisos();
$general = new General();

$nombre = '';
if (isset($_GET['nombre'])) {
    $nombre = $_GET['nombre'];
}

if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros = '&fab=' . $fab . '&fam=' . $fam . '&edi=' . $edi . '&ver=' . $ver;
} else {
    $start_record = 0;
    $parametros = '';
}

// Listar Usuarios
$count = 0;
$permisos->listar_todo_paginado($nombre, $start_record);
if ($permisos->total($nombre)) {
    $count = $permisos->result["cantidad"];
}

$pag = new paginator($count, $general->limit_paginacion, 'index.php?', $parametros);
$i = $pag->get_total_pages();
?>