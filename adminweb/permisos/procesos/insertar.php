<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
require_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_permisos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$permisos = new Permisos();
$validator = new validator("form1");
$general = new General();

$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    if(isset($_POST["nombre"])){
        if($permisos->existe_permiso($_POST['nombre'])){
            if($permisos->result["cantidad"] > 0){
                $error = 1;
            }
            else{
                if ($error == 0) {
                    if ($permisos->insertar($general->get_escape($_POST['nombre']))) {
                        $exito = 1;
                    } else {
                        $error = 5;
                    }
                }
            }
        }
    }
}

$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);