<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminweb");
include_once($GLOBALS["app_root"] . "/adminweb/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_permisos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validator.php");

// Objetos
$permisos = new Permisos();
$validator = new validator("form1");
$general = new General();

$nombre = "";
$status = 0;

$id = 0;
if(isset($_GET["id"]) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id = $_GET["id"];
    if ($permisos->permisosEspecifico($_GET['id'])) {
        $nombre = $permisos->permisoEspecifico["nombre"];
        $status = $permisos->permisoEspecifico["status"];
    }
}

$error = 0;
$exito = 0;

if (isset($_POST['modificar']) && $_POST["modificar"] == 1) {
    if(isset($_POST["id"]) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
        if ($error == 0) {
            if ($permisos->existe_permiso($_POST['id'])) {
                if ($permisos->result["cantidad"] > 0) {
                    $error = 1;
                } else {
                    $status = 0;
                    if (isset($_POST['status'])) {
                        $status = 1;
                    }
                    
                    $nombre = "";
                    if(isset($_POST['nombre'])){
                        $nombre = $general->get_escape($_POST['nombre']);
                    }
                    
                    if ($permisos->modificar($_POST['id'], $nombre, $status)) {
                        $exito = 1;
                    } else {
                        $error = 3;
                    }
                }
            }
        }
    }
}

$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);