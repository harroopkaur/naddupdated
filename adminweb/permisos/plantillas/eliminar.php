<?php if($exito==true){  ?>
<script type="text/javascript">
    $.alert.open('alert', 'Registro eliminado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
        if (button === 'Aceptar'){
            location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/permisos/';
        }
    });
</script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro no ha sido eliminado', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/permisos/';
            }
        });
    </script>
<?php
}
?>