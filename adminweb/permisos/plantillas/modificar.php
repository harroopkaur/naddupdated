<?php
if ($exito == true) {
    ?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registro modificado con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminweb/permisos/';
            }
        });
    </script>
    <?php
}
?>
    
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Permiso</span></legend>
    
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="modificar" id="modificar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id ?>">
        
        <?php $validator->print_script(); ?>
        <div class="error_prog"><font color="#FF0000">
            <?php
            if ($error == 5) {
                echo "Ocurrió un error al modificar la edici&oacute;n";
            } else if ($error == 1) {
            ?>
                <script type="text/javascript">
                    $.alert.open('alert', 'Ya existe el permiso', {'Aceptar': 'Aceptar'}, function() {
                    });
                </script>
            <?php
            }
            ?></font>
        </div>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Nombre:</th>
                <td align="left">
                    <input type="text" id="nombre" name="nombre" value="<?= $nombre ?>" maxlength="150">
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nombre") ?></font></div>
                </td>
            </tr>

            <tr>
                <th width="90" align="left" valign="top">Activo:</th>
                <td align="left">
                    <input type="checkbox" id="status" name="status" <?php if ($status == 1) {
                echo "checked='checked'";
            } ?>>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="MODIFICAR" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>