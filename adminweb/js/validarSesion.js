if(data[0].resultado === false){
    $.alert.open('alert', "No coinciden los Tokens", {'Aceptar' : 'Aceptar'}, function() {
        location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb/";
        return false;
    });
}
if(data[0].sesion === "false"){
    $.alert.open('alert', data[0].mensaje, {'Aceptar' : 'Aceptar'}, function() {
        location.href = "<?= $GLOBALS['domain_root'] ?>/adminweb/";
        return false;
    });
}