<?php
class portalSPLA_en{
    public $su0001 = "en";
    
    public $su0002 = "Bienvenido al Portal de Soporte para Datacenters";
    public $su0003 = "En esta sección Ud. encontrará respuesta a las dudas acerca del Licenciamiento SPLA. <br><br>Nuestro personal especializado está atento para responder sus interrogantes. Solo debe preguntar, completar los datos y enviar. Posteriormente recibirá un correo electrónico con la respuesta del caso";
    public $su0004 = "Ingresa su pregunta aquí";
    public $su0005 = "/SPLAPortal";
    public $su0006 = "/SPLAPortal/ask.php";
    public $su0007 = "Tema";
    public $su0008 = "Descripción de la Pregunta";
    public $su0009 = "Email";
    public $su0010 = "Enviar";
    public $su0011 = "SPLA Portal";
    public $su0012 = "PORTAL DE SOPORTE SERVICES PROVIDER LICENSE AGREEMENT (SPLA)";
    public $su0013 = "Bienvenido";
    public $su0014 = "1. Ingrese - 2. Consulte - 3. Envíe";
    public $su0015 = "En este portal encontrará la ayuda que necesita sobre Licenciamiento SPLA";
    public $su0016 = "Ingresar";
    public $su0017 = "2018 Todos los derechos reservados";
    public $su0018 = "/SPLAPortal/welcome.php";
    public $su0019 = "/SPLAPortal/emailSend.php";
    public $su0020 = "No Mail sent, try again, thanks";
    public $su0021 = "Warning";
    public $su0022 = "Ok";
    public $su0023 = "Estimado Usuario<br><br>Su pregunta ha sido enviada exitosamente.<br><br>Nuestro equipo técnico le estará enviando la respuesta vía email a la brevedad.";
}