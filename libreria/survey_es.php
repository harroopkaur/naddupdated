<?php
class survey_es{
    public $su0001 = "es";
    
    public $su0002 = "Encuesta rápida de satisfacción";
    public $su0003 = "Su opinión importa";
    public $su0004 = "1.- ¿Cuan probable es que recomiende Licensing Assurance con un amigo o socio?";
    public $su0005 = array('0'=>'No es probable', '1'=>'2', '2'=>'3', '3'=>'4', '4'=>'Neutral', '5'=>'6', '6'=>'7', 
                     '7'=>'8', '8'=>'9', '9'=>'Extremadamente probable');
    public $su0006 = "2.- ¿Cuál es el motivo principal de su puntaje?";
    public $su0007 = "3.- ¿Podemos contactarlo para saber más?";
    public $su0008 = "Por favor, coloque su email o numero de teléfono";
    public $su0009 = "Enviar";
    public $su0010 = "Gracias por sus comentarios";
    public $su0011 = "Apreciamos que se haya tomado el tiempo de responder la encuesta.
                     Nos encontramos en constante trabajo para mejorar su experiencia y sus comentarios ayudan a que esto suceda";
    public $su0012 = "Encuesta";
    public $su0013 = "Licensing Assurance LLC. Todos los derechos reservados.";
    public $su0014 = "Bienvenido a nuestra encuesta rápida de satisfacción";
    public $su0015 = "Nos gustaría saber cómo lo estamos haciendo";
    public $su0016 = "Califíquenos";
    public $su0017 = "/es/encuesta/encuesta.php";
}