<?php
class idioma_web_es{
    public $lg0001 = "es";
    
    //inicio meta tags title
    public $ti0001 = "Software Asset Management, Auditoría de software, Gestión de activos de software, SAM como servicio - Licensing Assurance"; 
    public $ti0002 = "SAM as a service, Defensa de auditoría, SPLA, Microsoft, auditoría SAM, auditoría de software - Licensing Assurance";
    public $ti0003 = "SAM as a service, Defensa de auditoría, SAM as a service para datacenters, SPLA, Diagnostico de despliegue, Microsoft, auditoría de software - Licensing Assurance";
    public $ti0004 = "SAM para Microsoft, Defensa de auditoria, herramienta SAM, gestion de activos de software, Auditoria Microsoft, Auditoria Oracle - Licensing Assurance";
    public $ti0005 = "Defensa de auditoría, auditoría de Microsoft, auditoría SAM, auditoría de software - Licensing Assurance";
    public $ti0006 = "Auditoría SPLA, servicio SPLA, soluciones para datacenters - Licensing Assurance";
    public $ti0007 = "Diagnostico de despliegue, salud del software, diagnostico sam - Licensing Assurance";
    public $ti0008 = "Resumen SAM, SAM, Servicios licensing assurance, servicios de optimización de software - Licensing Assurance";
    public $ti0009 = "articulos de software, noticias de software, SAM como servicio, gestión de activos de software, nube, defensa de auditoría, datacenters, ciberseguridad - Licensing Assurance";
    public $ti0010 = "Licensing Assurance contact, Licensing assurance offices, licensing assurance phone- Licensing Assurance";
    //fin meta tags title
    
    //inicio meta tags description
    public $de0001 = "Licensing Assurance fue fundada en 2014 en la creencia de que el objetivo principal de SAM es ahorrar controlando el gasto de software. Nuestro propósito es permitir a nuestros clientes controlar el gasto de software con las soluciones de SAM.";
    public $de0002 = "Ayudar a las organizaciones a ahorrar en gasto de software mediante la implementación de metodologías y controles para lograr la mejor optimización posible del mismo";
    public $de0003 = "Nuestro servicio está orientado a ayudar en el ahorro de software mediante: control del sobregasto, maximización de la inversión y rectificación de licencias.";
    public $de0004 = "Uno de los mejores proveedores de gestión de activos de software. Solución multiplataforma para gestión de software, con el fin de optimizar y controlar el licenciamiento para proporcionar ahorro en software.";
    public $de0005 = "Nuestro servicio está orientado a ayudarlo a eliminar las auditorías del software de su empresa.";
    public $de0006 = "Para ayudar a sus organizaciones a obtener mayor rentabilidad, ofrecemos SAM as a Service para datacenters. La solución de administración de software para optimizar los informes mensuales de forma fácil y rápida.";
    public $de0007 = "Nuestro servicio de Diagnóstico de Implementación le permite detectar las aplicaciones de software no utilizadas y alinear las licencias con las necesidades reales de negocio.";
    public $de0008 = "vista general de servicios de optimización de software.";
    public $de0009 = "información de gestión de software importante y oportuna.";
    public $de0010 = "Call us -(305) 851-3545, mail info@licensingassurance.com.";
    //fin meta tags description
    
    //inicio meta tags keywords
    public $ke0001 = "ahorro de software, optimización de software, SaaS, licensing assurance, libre de conflictos, solución independiente de SAM, gestión de activos de software"; 
    public $ke0002 = "auditoría de microsoft, SAM para microsoft, auditoría de oracle, sam para oracle, auditoría de sap, ddespliegue, licenciamiento sap, licenciamiento Microsoft, spla, centro de datos, soluciones sam, licenciamiento";
    public $ke0003 = "microsoft audit, microsoft sam, oracle audit, oracle sam, deployment, software diagnostic, sap licensing, Microsoft licensing, spla, datacenters, sam solutions";
    public $ke0004 = "auditoria microsoft, auditoria oracle, gestion de software, SAM, optimización de software, ciberseguridad, maximización de licencias, inventario de licencias";
    public $ke0005 = "auditoría de microsoft, auditoría de oracle, auditoría sap, auditoría sam, soluciones sam, mitigación de auditorías, tacticas de defensa de auditorías";
    public $ke0006 = "auditoría spla, soluciones para datacenters, rentabilidad para datacenters, sql server, centro de datos";
    public $ke0007 = "despliegue de software, salud del software,diagnostico de licenciamiento";
    public $ke0008 = "servicios sam, servicio de optimización, servicios de licenciamiento, servicio de defensa de auditoría, software, servicio en la nube, migración en la nube";
    public $ke0009 = "noticias de licenciamiento, licenciamiento en la nube, SAM, software, noticias de microsoft, noticias de sap, noticias de oracle, ciberseguridad";
    public $ke0010 = "contact LA, contact SAM, contact software asset management";
    //fin meta tags keywords
    
    //inicio alt img
    public $al0001 = "Beneficios SAM";
    public $al0002 = "SAM as a Service";
    public $al0003 = "SAM for YOU";
    public $al0004 = "Servicio de Optimización";
    public $al0005 = "Flujo de SAM as a Service";
    public $al0006 = "Cargando";
    public $al0007 = "Bandera de EEUU";
    public $al0008 = "Bandera de España";
    public $al0009 = "Logo LinkedIn";
    public $al0010 = "Logo Twitter";
    public $al0011 = "Logo Facebook";
    public $al0012 = "Logo Youtube";
    public $al0013 = "mira de apuntar";
    public $al0014 = "ojo";
    public $al0015 = "valores";
    public $al0016 = "propósito";
    public $al0017 = "Mapa del Continente Americano";
    public $al0018 = "confidencial";
    public $al0019 = "independiente";
    public $al0020 = "sin conflictos de interés";
    public $al0021 = "satisfacción de clientes";
    public $al0022 = "engranaje";
    public $al0023 = "defensa de auditoría";
    public $al0024 = "SPLA";
    public $al0025 = "despliegue de diagnóstico";
    public $al0026 = "garantizado";
    public $al0027 = "logos de fabricantes";
    public $al0028 = "reportes SPLA";
    public $al0029 = "Solución para Datacenter";
    public $al0030 = "portal en línea";
    public $al0031 = "beneficios";
    public $al0032 = "signo azul de chequeado";
    public $al0033 = "portal de diagnóstico en línea";
    public $al0034 = "resumen";
    public $al0035 = "Alianza con Mazars";
    public $al0036 = "impulsamos tu emprendimiento";
    public $al0037 = "Bandera de EEUU";
    public $al0038 = "Bandera de Perú";
    public $al0039 = "Bandera de México";
    public $al0040 = "Bandera de Guatemala";
    public $al0041 = "Bandera de El Salvador";
    public $al0042 = "Bandera de Costa Rica";
    public $al0043 = "Bandera de Panamá";
    public $al0044 = "Bandera de Colombia";
    public $al0045 = "Bandera de Ecuador";
    public $al0046 = "Bandera de Brasil";
    public $al0047 = "Bandera de Bolivia";
    public $al0048 = "Bandera de Chile";
    public $al0049 = "ubicación";
    //fin alt img
    
    //inicio navegacion
    public $lg0002 = "Inicio";
    public $lg0003 = "Sobre Nosotros";
    public $lg0004 = "Servicios";
    public $lg0005 = "SAM como Servicio";
    public $lg0006 = "Defensa de Auditoría";
    public $lg0007 = "SAM como Servicio para SPLA";
    public $lg0008 = "Diagnóstico de Despliegue";
    public $lg0009 = "Resumen";
    public $lg0010 = "Artículos";
    public $lg0011 = "Eventos";
    public $lg0012 = "Colombia";
    public $lg0013 = "México";
    public $lg0014 = "Contáctenos";
    public $ln0001 = "Webinar: Conozca cómo administrar el software SPLA mitigando los riesgos de Auditorías";
    public $ln0002 = "Webinar Licenciamiento SAP";
    //fin navegacion
    
    //inicio index
    public $lg0015 = "Optimización de Software Fácil y Eficazmente";
    public $lg0016 = "INNOVADA Y PERSONALIZADA";
    public $lg0017 = "SOLUCIÓN DE CONTROL DE SOFTWARE";
    public $lg0018 = "circulo_3_es.png";
    public $lg0019 = "circulo_1_es.png";
    public $lg0020 = "circulo_2_es.png";
    public $lg0021 = "gartner_es.png";
    public $lg0022 = "SAM COMO SERVICIO";
    public $lg0023 = "La mejor solución para ahorrar eliminando";
    public $lg0024 = "el sobre gasto de software rápidamente y a bajo costo";
    public $lg0025 = "best_award_es.png";
    public $lg0026 = "SAM_Chantal_es.png";
    public $lg0027 = "Más";
    public $lg0028 = "https://www.youtube.com/embed/r6XCL0_6v84";
    public $lg0029 = "https://www.youtube.com/embed/qHg1Pd66FZI";
    public $lg0030 = "https://www.youtube.com/embed/5rSk2DVqaPw";
    public $lg0031 = "Resultados Garantizados";
    public $lg0032 = "Conozca SAM as a Service";
    public $lg0033 = "Testimonios de Clientes";
    //fin index
    
    //inicio aboutUs
    public $lg0034 = "100% INDEPENDENTE";
    public $lg0035 = "SOLUCIONES DE CONTROL DE SOFTWARE";
    public $lg0036 = "HISTORIA";
    public $lg0037 = "Licensing Assurance fue fundada en 2014 en la creencia de que el objetivo principal de SAM es controlar
                    el gasto de software. Nuestros fundadores argumentan que SAM está libre de conflictos de intereses e 
                    independiente de todos los fabricantes de software. LA, como pionero de SAM as a Service, ha diseñado sus 
                    herramientas, procesos y metodologías para permitirles a los clientes administrar y controlar sus costos de 
                    software de manera efectiva.";
    public $lg0038 = "MISIÓN";
    public $lg0039 = "Ayudar a las organizaciones a reducir el gasto de software implementando metodologías y controles para 
                    lograr la mejor optimización posible.";
    public $lg0040 = "VISIÓN";
    public $lg0041 = "Prevemos convertirnos en la empresa de administración de activos de software líder a nivel mundial al 
                    proporcionar soluciones SAM libres de conflictos e independientes.";
    public $lg0042 = "NUESTROS VALORES";
    public $lg0043 = "La mayoría de las organizaciones gastan en exceso en software";
    public $lg0044 = "El propósito de toda administración de software es el ahorro";
    public $lg0045 = "Los clientes deben administrar sus activos sin conflicto de interés";
    public $lg0046 = "NUESTRO PROPÓSITO";
    public $lg0047 = "Nuestro propósito es permitir a nuestros clientes controlar el gasto de  software con las soluciones 
                    de SAM.";
    public $lg0048 = "Customers throughout";
    public $lg0049 = "Americas have trusted us!!";
    public $lg0050 = "¿POR QUÉ NOSOTROS?";
    public $lg0051 = "Combinamos la metodología, el servicio, las herramientas y los KPI para optimizar y controlar el 
                    software";
    public $lg0052 = "No estamos vinculados a los fabricantes";
    public $lg0053 = "independiente_es.png";
    public $lg0054 = "No tenemos conflictos de intereses (ahorro vs venta)";
    public $lg0055 = "sin_conflicto_es.png";
    public $lg0056 = "100% de confidencialidad y privacidad en la información";
    public $lg0057 = "confidencial_es.png";
    public $lg0058 = "sello3_es.png";
    //fin aboutUs
    
    //inicio services
    public $lg0059 = "LOGRA EL AHORRO QUE DESEAS";
    public $lg0060 = "CON NUESTROS SERVICIOS";
    public $lg0061 = "pruebaImagen_es.jpg";
    public $lg0062 = "n1_es.png";
    public $lg0063 = "SAM COMO SERVICIO";
    public $lg0064 = "Solución completa de Software Asset Management que incluye metodología, controles y procesos para controlar 
                    el gasto de software utilizando indicadores clave de rendimiento (KPI)";
    public $lg0065 = "DEFENSA DE AUDITORÍA";
    public $lg0066 = "La experiencia de los más calificados en auditoria de software para ayudarlo a proteger los activos de su empresa 
                    durante una auditoría de software del fabricante";
    public $lg0067 = "SPLA";
    public $lg0068 = "Administración integral, incluida la facturación a los clientes, la implementación de software, la
                    optimización y los informes mensuales para reducir el gasto de software y maximizar la facturación a los clientes.";
    public $lg0069 = "DIAGNÓSTICO DE DESPLIEGUE";
    public $lg0070 = "Nuestro servicio de Diagnóstico de despliegue le permite detectar de manera efectiva las aplicaciones de 
                    software no utilizadas y alinear las licencias con las necesidades comerciales reales entre muchos beneficios 
                    de optimización.";
    //fin services
    
    //inicio SAM as a Service
    public $lg0071 = "SOLUCIÓN COMPLETA";
    public $lg0072 = "SAM COMO SERVICIO";
    public $lg0073 = "gartner_es.png";
    public $lg0074 = "Es una solución multiplataforma de herramientas, personal, procesos, controles, indicadores (KPI) y 
                    metodología, con el objetivo de optimizar y controlar el software.";
    public $lg0075 = "circulo_2_es.png";
    public $lg0076 = "CS_es.png";
    public $lg0077 = "Eliminamos el software en desuso al 0%";
    public $lg0078 = "TENEMOS NUESTRAS PROPIAS HERRAMIENTAS ENFOCADAS EN EL AHORRO";
    public $lg0079 = "HERRAMIENTA DE MEDICIÓN LA";
    public $lg0080 = "Es una herramienta que entrega los resultados. Usabilidad del software de un agente sin ser un agente.";
    public $lg0081 = "LA TOOL";
    public $lg0082 = "Herramienta propia para el descubrimiento en el entorno de Windows";
    public $lg0083 = "WEBLOGIC";
    public $lg0084 = "Es un script que permite identificar a los usuarios que se conectan a esta plataforma, así como también 
                    obtener la edición y la versión instalada.";
    public $lg0085 = "TODO EN UNO";
    public $lg0086 = "Es un script que se ejecuta en Unix e incluye un conjunto de herramientas que le permiten obtener 
                    información de varios productos de Oracle al mismo tiempo.";
    public $lg0087 = "LA VTOOL";
    public $lg0088 = "Permite obtener la infraestructura de entorno virtualizada bajo VMWare";
    public $lg0089 = "BENEFICIOS";
    public $lg0090 = "-Mayor control para mejor ahorro";
    public $lg0091 = "-El mejor TCO";
    public $lg0092 = "-Servicio multiplataforma";
    //fin SAM as a Service
    
    //inicio audit defense
    public $lg0093 = "ELIMINE LAS AUDITORÍAS DE SOFTWARE DE SU VIDA";
    public $lg0094 = "57% DE PROBABILIDADES DE SER AUDITADO";
    public $lg0095 = "Estamos preparados para protegerlo ante una posible auditoría";
    public $lg0096 = "Experiencia de expertos en auditoría de software para ayudarle y proteger los activos de la empresa durante 
                    una auditoría de software fabricante.";
    public $lg0097 = "Nuestro servicio está orientado a ayudar a eliminar las auditorías de software de su empresa, yá que al tener 
                    su sistema de licencias controlado y con 0% de software en desuso, el riesgo de ser llamado para la auditoría 
                    se reduce casi en su totalidad.";
    public $lg0098 = "sello3_es.png";
    public $lg0099 = "BENEFICIOS";
    public $lg0100 = "Mitigación de auditoría";
    public $lg0101 = "Evaluación de escenarios";
    public $lg0102 = "Tácticas de defensa de auditoría";
    public $lg0103 = "Negociación con los fabricantes para obtener un valor agregado adicional";
    //fin audit defense
    
    //inicio SPLA
    public $lg0104 = "OPTIMICE SUS INFORMES MENSUALES DE FORMA FÁCIL Y RÁPIDA";
    public $lg0105 = "SAM COMO SERVICIO SPLA";
    public $lg0106 = "Presentamos el mejor servicio para los informes de licencias de SPLA. Una solución multiplataforma que
                    incluye herramientas, personal, procesos, controles, KPI y metodología para optimizar y controlar el 
                    gasto de software.";
    public $lg0107 = "Sabemos:<br>
                    -Está expuesto a auditorías rutinarias de software<br>
                    -El uso de software es continuamente superior<br>
                    -Podemos aumentar su rentabilidad con controles de software más efectivos<br>
                    -Usted tiene derecho a gestionar su software sin conflicto de intereses";
    public $lg0108 = "SERVICIO SPLA";
    public $lg0109 = "spla_chantal_es.png";
    public $lg0110 = "CONTROLA, GENERA INGRESOS Y MITIGA LOS RIESGOS";
    //fin SPLA
    
    //inicio deployment diagnostic
    public $lg0111 = "CONOZCA SU SOBREGASTO EN SOFTWARE Y ELIMÍNELO";
    public $lg0112 = "DIAGNÓSTICO DE DESPLIEGUE";
    public $lg0113 = "Nuestro servicio de Diagnóstico de Despliegue le permite detectar de manera efectiva las aplicaciones 
                    de software no utilizadas y alinear las licencias con las necesidades comerciales reales entre muchos 
                    beneficios de optimización.<br>
                    Con base en los KPI de Control, Optimización, Maximización y Seguridad, llevamos a cabo el despliegue de 
                    nuestra herramienta para determinar las áreas de mejora y establecer un plan de rectificación para eliminar 
                    el exceso de software, entre otras cosas.";
    public $lg0114 = "DDWEB_es.png";
    public $lg0115 = "BENEFICIOS";
    public $lg0116 = "sello2_es.png";
    public $lg0117 = "Optimización de: duplicados, erróneos, inactivos, desuso, virtualización";
    public $lg0118 = "Mejor y mejor control de aplicaciones";
    public $lg0119 = "Resultados rápidos";
    public $lg0120 = "Fácil y eficaz";
    public $lg0121 = "Mejor control,... mayor rentabilidad";
    public $lg0122 = "https://www.youtube.com/embed/6gAjtmw66ak";
    //fin deployment diagnostic
    
    //inicio overview
    public $lg0123 = "oi2_es.png";
    public $lg0153 = '<map name="map">
        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
        <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
        <!-- #$-:Please do not edit lines starting with "#$" -->
        <!-- #$VERSION:2.3 -->
        <!-- #$AUTHOR:Dx -->
        <area shape="rect" coords="330,68,650,337"  href="contacto.php" target="_blank" alt="Diagnóstico adquirir aquí"/>
        <area shape="rect" coords="669,67,987,335"  href="contacto.php" target="_blank" alt="Nube adquirir aquí"/>
        <area shape="rect" coords="1001,67,1322,340" href="contacto.php" target="_blank" alt="SAM Diagnóstico adquirir aquí"/>
        <area shape="rect" coords="1328,13,1708,339" href="contacto.php" target="_blank" alt="SAM Administración adquirir aquí"/>
        </map>';
    //fin overview
    
    //inicio articles
    public $lg0124 = "INFORMACIÓN IMPORTANTE Y OPORTUNA";
    public $lg0125 = "ARTÍCULOS DE LICENSING ASSURANCE";
    public $lg0126 = "https://www.licensingassurance.com/ComunicadoAlianzaLA&Mazars.pdf"; //"https://www.gartner.com/newsroom/id/3382317";
    public $lg0127 = "Licensing Assurance y Mazars firman alianza para ofrecer servicio de administración de Licenciamiento y consultoría a empresas mexicanas"; /*"Gartner dice que las organizaciones pueden reducir los costos de software en un 30 por ciento usando 
                    tres mejores prácticas.";*/
    public $lg0128 = "Licensing Assurance, uno de los principales proveedores de servicios de administración de activos de software con basta experiencia en toda América 
                    y Mazars, importante empresa de consultoría, accouting & outsourcing, impuestos y asesoría financiera en Mexico, firmam importante acuerdo estratégico 
                    de alianza comercial para expandir las operaciones en Mexico."; /*"Muchas organizaciones pueden reducir el gasto en software hasta en un 30 por ciento implementando tres 
                    mejores prácticas de optimización de licencias de software, según una investigación de Gartner, Inc. 
                    Las claves para reducir el gasto de licencias de software son optimización de configuración de aplicaciones, 
                    reciclaje de licencias de software y uso de activos de software herramientas de gestión (SAM).";*/
    public $lg0129 = "http://factorypyme.thestandardit.com/2015/08/17/es-el-software-independiente-una-solucion-para-las-empresas/";
    public $lg0130 = "¿El software independiente es una solución para las empresas?";
    public $lg0131 = "Licensing Assurance es una empresa pionera en el mercado de administración de software de forma independiente, 
                    enfocada en la optimización y licencia de software. La compañía afirma que en América Latina, es importante 
                    contar con asesoramiento independiente para tomar decisiones al comprar software.";
    public $lg0132 = "http://samforyou-es.blogspot.com/";
    public $lg0133 = "5 errores a evitar durante la ejecución de los procesos de Gestión de activos de software.";
    public $lg0134 = "El SAM funciona según cuatro pasos o fases: primero se lleva a cabo un análisis de las instalaciones 
                    para realizar un proceso de diagnóstico de su estado. Luego se determina la ejecución de la licencia, 
                    identificando fallas y derechos. En el siguiente paso, se prepara un informe de todo lo investigado y 
                    finalmente procede a llevar a cabo la negociación, es decir, la compra de licencias. Para que pueda 
                    obtener más información sobre este tema, le mostraremos una breve selección de 5 errores comunes que 
                    deben evitarse al ejecutar procesos SAM, para que puedan ser más rentables.";
    public $lg0135 = "Tenemos muchas cosas más que decir, ... Visita nuestro Blog";
    public $lg0136 = "Hablemos";
    public $lg0155 = "Webinar: Guía Práctica de Supervivencia para Auditorías SPLA";
    public $lg0156 = "/WebinarSPLA.mp4";
    public $lg0157 = "Las auditorías de software SPLA se han convertido en un hecho recurrente en los últimos años. 
                     Teniendo en cuenta que su próxima auditoría puede estar más cerca de lo que piensa, es importante 
                     implementar las mejores prácticas para sobrevivir a una auditoría de software sin morir en el intento. 
                     La manera en la que su organización logre gestionar una Auditoría SPLA podría ser la diferencia entre 
                     resolver su caso de manera inteligente y eficaz o tener una pesadilla muy costosa.";
    //fin articles
    
    //inicio contact
    public $lg0137 = "CONTÁCTENOS";
    public $lg0138 = "Teléfono: (305) 851-3545<br>
                    Correo Electrónico: info@licensingassurance.com<br><br>
                    
                    Licensing Assurance LLC<br>
                    16192 Coastal Highway<br>
                    Lewes, DE 19958";
    public $lg0139 = "Nombre";
    public $lg0140 = "Correo Electrónico";
    public $lg0141 = "Teléfono";
    public $lg0142 = "País";
    public $lg0143 = "Compañía";
    public $lg0144 = "Asunto";
    public $lg0145 = "Mensaje";
    public $lg0146 = "Enviar";
    public $lg0147 = "Para todas sus preguntas, no dude en enviarnos un correo electrónico o llámenos en cualquier momento.";
    public $lg0148 = "Estados Unidos";
    public $lg0149 = "México";
    public $lg0150 = "Panamá";
    public $lg0151 = "Brasil";
    public $lg0152 = "<p>¡Gracias por ponerse en contacto con nosotros! Nos encontraremos de nuevo con usted muy pronto.</p>";
    public $lg0154 = "Perú";
    public $lg0158 = "Office Contáctanos";
    public $lg0159 = "SPLA LATAM";
    public $lg0160 = "Diagnóstico"; 
    public $lg0161 = "Departamento de Contabilidad";
    public $lg0162 = "Gerente de Ventas LATAM";
    public $lg0163 = "Director CS";
    public $lg0164 = "Administrador";
    //fin contact
    
    //inicio redireccionamiento navegacion
    public $nv0001 = "index.php";
    public $nv0002 = "sobreNosotros.php";
    public $nv0003 = "servicios.php";
    public $nv0004 = "SAMcomoServicio.php";
    public $nv0005 = "defensaAuditoria.php";
    public $nv0006 = "SPLA.php";
    public $nv0007 = "despliegue.php";
    public $nv0008 = "resumen.php";
    public $nv0009 = "articulos.php";
    public $nv0010 = "eventosColombia.php";
    public $nv0011 = "eventosMexico.php";
    public $nv0012 = "contacto.php";
    public $nv0013 = "webinar.php";
    public $nv0014 = "webinar_licenciamiento_SAP.php";
    //fin redireccionamiento navegacion
    
    //inicio foot
    public $ft0001 = "NUESTROS PRODUCTOS";
    public $ft0002 = "NUESTRO EQUIPO";
    public $ft0003 = "index.php?videos=true";
    public $ft0004 = "¡¡CONTÁCTENOS!!";
    public $ft0005 = "Teléfono: (305) 851-3545";
    public $ft0006 = "info@licensingassurance.com";
    public $ft0007 = "Licensing Assurance LLC";
    public $ft0008 = "16192 Coastal Highway";
    public $ft0009 = "Lewes, DE 19958";
    public $ft0010 = "Estamos disponibles";
    public $ft0011 = "Lunes - Viernes";
    public $ft0012 = "8:00 am - 5:00 pm";
    public $ft0013 = "Hora del Este";
    public $ft0014 = "Copyright Licensing Assurance LLC. Todos los Derechos Reservados";
    public $ft0015 = "Síguenos y comparte";
    //fin foot
}
