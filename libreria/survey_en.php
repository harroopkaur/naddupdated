<?php
class survey_en{
    public $su0001 = "en";
    
    public $su0002 = "Quick Satisfaction Survey";
    public $su0003 = "Your opinion matters";
    public $su0004 = "1.- How likely is it to recommend Licensing Assurance with a friend or partner?";
    public $su0005 = array('0'=>'Not at all likely', '1'=>'2', '2'=>'3', '3'=>'4', '4'=>'Neutral', '5'=>'6', '6'=>'7', 
                     '7'=>'8', '8'=>'9', '9'=>'Extremely probable');
    public $su0006 = "2.- What´s the main reason for your score?";
    public $su0007 = "3.- May we contact you to find out more?";
    public $su0008 = "Please, put your email or phone number";
    public $su0009 = "Send";
    public $su0010 = "Thank you for your feedback";
    public $su0011 = "We appreciate that you have taken the time to answer the survey.
                      We are constantly working to improve your experience and your comments help make this happen";
    public $su0012 = "Survey";
    public $su0013 = "Licensing Assurance LLC. All rights reserved";
    public $su0014 = "Welcome to our quick satisfaction survey";
    public $su0015 = "We would like to know how we are doing";
    public $su0016 = "Rate us";
    public $su0017 = "/survey/survey.php";
}