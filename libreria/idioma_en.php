<?php
class idioma_web_en{       
    public $lg0001 = "en";
    
    //inicio meta tags title
    public $ti0001 = "Software Asset Management, SAM, SAM as a Service - Licensing Assurance"; 
    public $ti0002 = "SAM as a service, Audit Defense, SPLA, Microsoft, SAM Audit, software audit - Licensing Assurance";
    public $ti0003 = "SAM as a service, Audit Defense, SAM as a service for datacenters, SPLA, Deployment Diagnostic, Microsoft, SAM Audit, software audit - Licensing Assurance";
    public $ti0004 = "SAM as a service, licensing optimization, license maximization, SAM, Software asset management - Licensing Assurance";
    public $ti0005 = "Audit Defense, Microsoft Audit, SAM Audit, software audit - Licensing Assurance";
    public $ti0006 = "SPLA Audit, SPLA Service, datacenters solutions - Licensing Assurance";
    public $ti0007 = "Deploment Diagnostic, software health, SAM diagnostic - Licensing Assurance";
    public $ti0008 = "SAM Overview, SAM, Services licensing assurance, software optimización services - Licensing Assurance";
    public $ti0009 = "software articles, news software, SAM as a service, software asset management, cloud, audit defense, datacenters, cibersecurity - Licensing Assurance";
    public $ti0010 = "Licensing Assurance contact, Licensing assurance offices, licensing assurance phone- Licensing Assurance";
    //fin meta tags title
    
    //inicio meta tags description
    public $de0001 = "Licensing Assurance was founded in 2014 in the belief that the main objective of SAM is to save by controlling software spending . Our purpose is to enable our customers to control their software spending with SAM solutions."; 
    public $de0002 = "Help organizations save on software spending by implementing methodologies and controls to achieve the best possible optimization of it.";
    public $de0003 = "Our service is oriented to help in the saving of software through: control of the excess, maximization of the investment and rectification of licenses.";
    public $de0004 = "One of the best providers of software asset management. Multi-platform solution for software management, in order to optimize and control licensing to provide software savings.";
    public $de0005 = "Our service is oriented to help you eliminate the audits of your company's software.";
    public $de0006 = "To help your organizations obtain greater profitability, we offer SAM as a Service for datacenters. The software management solution to optimize monthly reports easily and quickly. Call us -(305) 851-3545.";
    public $de0007 = "Our Implementation Diagnostic service allows you to detect unused software applications and align licenses with real business needs.";
    public $de0008 = "overview of software optimization services.";
    public $de0009 = "important and timely software management information.";
    public $de0010 = "Call us -(305) 851-3545, mail info@licensingassurance.com.";
    //fin meta tags description
    
    //inicio meta tags keywords
    public $ke0001 = "Software savings, software optimization , SaaS, licensing assurance, conflict free, independent SAM solution, cloud, cloud migration, software asset management"; 
    public $ke0002 = "Microsoft audit, microsoft sam, oracle audit, oracle sam, sap audit, deployment, sap licensing, Microsoft licensing, spla, datacenters, sam solutions, licensing";
    public $ke0003 = "microsoft audit, microsoft sam, oracle audit, oracle sam, deployment, software diagnostic, sap licensing, Microsoft licensing, cloud, cloud migration, spla, datacenters, sam solutions";
    public $ke0004 = "microsoft audit, microsoft sam, cloud migration, oracle audit, oracle sam, deployment, software diagnostic, sap licensing, Microsoft licensing, sam solutions";
    public $ke0005 = "microsoft audit, oracle audit, sap audit, sam audit, solutions sam, mitigate audits, audit Defense Tactics";
    public $ke0006 = "spla audit, datacenters solutions,  profitability for datacenters, sql server, data center";
    public $ke0007 = "software deploymentsoftware healt,  cloud migration, licensing diagnostic";
    public $ke0008 = "sam services, optimization services, licensing services, audit defense services, cloud services, cloud migration";
    public $ke0009 = "licensing news, cloud licensing,  SAM, software, microsoft news, sap news, oracle news, cibersecurity";
    public $ke0010 = "contact LA, contact SAM, contact software asset management";
    //fin meta tags keywords
    
    //inicio alt img
    public $al0001 = "Benefits SAM";
    public $al0002 = "SAM as a Service";
    public $al0003 = "SAM for YOU";
    public $al0004 = "Optimization Service";
    public $al0005 = "SAM as a Service Flow";
    public $al0006 = "Loading";
    public $al0007 = "USA Flag";
    public $al0008 = "Spain Flag";
    public $al0009 = "LinkedIn Logo";
    public $al0010 = "Twitter Logo";
    public $al0011 = "Facebook Logo";
    public $al0012 = "Youtube Logo";
    public $al0013 = "look at point";
    public $al0014 = "eye";
    public $al0015 = "values";
    public $al0016 = "purpose";
    public $al0017 = "Map American Continent";
    public $al0018 = "confidential";
    public $al0019 = "independent";
    public $al0020 = "no conflicts of interest";
    public $al0021 = "customer satisfaction";
    public $al0022 = "gear";
    public $al0023 = "audit defense";
    public $al0024 = "SPLA";
    public $al0025 = "diagnostic deployment";
    public $al0026 = "guaranteed";
    public $al0027 = "manufacturers logo";
    public $al0028 = "SPLA reports";
    public $al0029 = "Datacenter Solution";
    public $al0030 = "online portal";
    public $al0031 = "benefits";
    public $al0032 = "blue check sign";
    public $al0033 = "diagnostic online portal";
    public $al0034 = "overview";
    public $al0035 = "Alliance with Mazars";
    public $al0036 = "Factory PYME";
    public $al0037 = "America Flag";
    public $al0038 = "Peru Flag";
    public $al0039 = "Mexico Flag";
    public $al0040 = "Guatemala Flag";
    public $al0041 = "El Salvador Flag";
    public $al0042 = "Costa Rica Flag";
    public $al0043 = "Panama Flag";
    public $al0044 = "Colombia Flag";
    public $al0045 = "Ecuador Flag";
    public $al0046 = "Brazil Flag";
    public $al0047 = "Bolivia Flag";
    public $al0048 = "Chile Flag";
    public $al0049 = "location";
    //fin alt img
    
    //inicio navegacion
    public $lg0002 = "Home";
    public $lg0003 = "About Us";
    public $lg0004 = "Services";
    public $lg0005 = "SAM as a Service";
    public $lg0006 = "Audit Defense";
    public $lg0007 = "SAM as a Service for SPLA";
    public $lg0008 = "Deployment Diagnostic";
    public $lg0009 = "Overview";
    public $lg0010 = "Articles";
    public $lg0011 = "Events";
    public $lg0012 = "Colombia";
    public $lg0013 = "Mexico";
    public $lg0014 = "Contact Us";
    public $ln0001 = "Webinar: Conozca cómo administrar el software SPLA mitigando los riesgos de Auditorías";
    public $ln0002 = "Webinar Licenciamiento SAP";
    //fin navegacion
    
    //inicio index
    public $lg0015 = "Software Optimization Made Easy";
    public $lg0016 = "EFFECTIVE AND PERSONALIZED";
    public $lg0017 = "SOFTWARE CONTROL SOLUTIONS";
    public $lg0018 = "circulo_3.png";
    public $lg0019 = "circulo_1.png";
    public $lg0020 = "circulo_2.png";
    public $lg0021 = "gartner.png";
    public $lg0022 = "SAM AS SERVICE";
    public $lg0023 = "The best solution to save by eliminating software";
    public $lg0024 = "overshoots quickly and at low cost";
    public $lg0025 = "best_award.png";
    public $lg0026 = "SAM_Chantal.png";
    public $lg0027 = "More";
    public $lg0028 = "https://www.youtube.com/embed/-FHh-LuZn2w";
    public $lg0029 = "https://www.youtube.com/embed/rj7D-wgX4NI";
    public $lg0030 = "https://www.youtube.com/embed/5rSk2DVqaPw";
    public $lg0031 = "DISRUPTING THE SAM INDUSTRY";
    public $lg0032 = "LICENSING ASSURANCE";
    public $lg0033 = "CUSTOMERS REVIEWS";
    //fin index
    
    //inicio aboutUs
    public $lg0034 = "100% INDEPENDENT";
    public $lg0035 = "SOFTWARE CONTROL SOLUTIONS";
    public $lg0036 = "HISTORY";
    public $lg0037 = "Licensing Assurance was founded in 2014 in the belief that the main goal of SAM is to 
                    control software spending. Our founders argue that SAM is free from conflicts of interest and independent of all 
                    software manufacturers. LA as a pioneer of SAM as a Service has designed its tools, processes and methodologies 
                    to enable customers to manage and control their software costs effectively.";
    public $lg0038 = "MISION";
    public $lg0039 = "Help organizations reduce software spending by 
                    implementing methodologies and controls to achieve the best possible optimization of the same";
    public $lg0040 = "VISION";
    public $lg0041 = "We envision becoming the world's leading software 
                    asset management company by providing conflict-free and independent SAM solutions.";
    public $lg0042 = "OUR VALUES";
    public $lg0043 = "Most organizations over spend on software";
    public $lg0044 = "The purpose of any software administration is saving";
    public $lg0045 = "Customers must manage their assets without conflict of interest";
    public $lg0046 = "OUR PURPOSE";
    public $lg0047 = "Our purpose is to enable our customers to control 
                    their software spending with SAM solutions.";
    public $lg0048 = "Customers throughout";
    public $lg0049 = "Americas have trusted us!!";
    public $lg0050 = "WHY US?";
    public $lg0051 = "We combine methodology, service, tools and KPI´s to 
                    optimize and control the software assets.";
    public $lg0052 = "We are not linked to manufacturers";
    public $lg0053 = "independiente.png";
    public $lg0054 = "We have no conflicts of interest (sell vs save)";
    public $lg0055 = "sin_conflicto.png";
    public $lg0056 = "100% confidentiality and privacy in information";
    public $lg0057 = "confidencial.png";
    public $lg0058 = "sello3.png";
    //fin aboutUs
    
    //inicio services
    public $lg0059 = "ACHIEVE THE SAVINGS YOU WANT";
    public $lg0060 = "WITH OUR SERVICES";
    public $lg0061 = "pruebaImagen.jpg";
    public $lg0062 = "n1.png";
    public $lg0063 = "SAM AS A SERVICE";
    public $lg0064 = "Complete Software Asset Management solution that includes the methodology controls and 
                    processes to control software spending using key performance indicators (KPI)";
    public $lg0065 = "AUDIT DEFENSE";
    public $lg0066 = "The expertise of software compliance audit 
                    experts to assist you protect your company’s assets during a manufacturer software audit";
    public $lg0067 = "SPLA";
    public $lg0068 = "The comprehensive administration including 
                    customer billing, software deployment, optimization, and monthly reporting to reduce software 
                    spending and maximize customer billing.";
    public $lg0069 = "DEPLOYMENT DIAGNOSTIC";
    public $lg0070 = "Our Deployment Diagnostic service allows 
                    you to effectively detect unused software applications and align licenses with real business 
                    needs among many Optimization benefits.";
    //fin services
    
    //inicio SAM as a Service
    public $lg0071 = "THE COMPLETE SOLUTION";
    public $lg0072 = "SAM AS A SERVICE";
    public $lg0073 = "gartner.png";
    public $lg0074 = "It is a Multi platform solution of tools, personnel, processes, controls, indicators (KPI) and methodology, 
                    with the purpose of optimizing and controlling the software";
    public $lg0075 = "circulo_2.png";
    public $lg0076 = "CS.png";
    public $lg0077 = "We eliminated the unused software to 0%";
    public $lg0078 = "WE HAVE OUR OWN TOOLS FOCUSED ON SAVINGS";
    public $lg0079 = "LA METERING TOOL";
    public $lg0080 = "A tool is developed to deliver software usability 
                    results like an agent, but not being an agent";
    public $lg0081 = "LA TOOL";
    public $lg0082 = "Proprietary tool for discovery under 
                    Windows environment";
    public $lg0083 = "WEBLOGIC";
    public $lg0084 = "It is a script that allows to identify the 
                    users that connect to this platform as well as to obtain the edition and installed version";
    public $lg0085 = "ALL IN ONE";
    public $lg0086 = "It is a script that runs under Unix that 
                    includes a set of tools that allow you to obtain information from various Oracle products at the 
                    same time";
    public $lg0087 = "LA VTOOL";
    public $lg0088 = "It allows to obtain the virtual environment 
                    infrastructure that is virtualized under VMWare";
    public $lg0089 = "BENEFITS";
    public $lg0090 = "-Greater control for better savings";
    public $lg0091 = "-The best TCO";
    public $lg0092 = "-Multiplatform Service";
    //fin SAM as a Service
    
    //inicio audit defense
    public $lg0093 = "WE ELIMINATE THE SOFTWARE AUDITS OF YOUR LIFE";
    public $lg0094 = "57% PROBABILITIES OF BEING AUDITED";
    public $lg0095 = "We are prepared to protect you in case of a possible audit";
    public $lg0096 = "The expertise of software compliance audit experts to assist you and 
                    protect your company’s assets during a manufacturer software audit";
    public $lg0097 = "Our service is oriented to help you eliminate the audits of your company's software, since having your licensing 
                    system controlled and with 0% unused software, your risk of being called to audit is reduced almost entirely";
    public $lg0098 = "sello3.png";
    public $lg0099 = "BENEFITS";
    public $lg0100 = "Audit mitigation";
    public $lg0101 = "Scenario evaluation";
    public $lg0102 = "Audit Defense Tactics";
    public $lg0103 = "Negotiating with manufacturers to obtain additional added value";
    //fin audit defense
    
    //inicio SPLA
    public $lg0104 = "OPTIMIZE YOUR MONTHLY REPORTS EASY AND FAST";
    public $lg0105 = "SAM AS A SERVICE SPLA";
    public $lg0106 = "We present the best service for SPLA Licensing Reporting. 
                    A multiplatform solution including tools, personnel, processes, controls, KPIs and methodology to 
                    optimize and control software spending.";
    public $lg0107 = "We know:<br>
                    -You are exposed to routine software audits<br>
                    -Your Software usage is continuously over reported<br>
                    -We can increase your profitability with more effective software Controls<br>
                    -You have the right to manage your software without conflict of interest";
    public $lg0108 = "SPLA SERVICE";
    public $lg0109 = "spla_chantal.png";
    public $lg0110 = "CONTROL, GENERATES REVENUE AND MITIGATES RISKS";
    //fin SPLA
    
    //inicio deployment diagnostic
    public $lg0111 = "KNOW YOUR OVERSPEND IN SOFTWARE AND ELIMINATE IT";
    public $lg0112 = "DEPLOYMENT DIAGNOSTIC";
    public $lg0113 = "Our Deployment Diagnostic service allows you to effectively 
                    detect unused software applications and align licenses with real business needs among many Optimization 
                    benefits.<br>
                    Based on the KPIs of Control, Optimization, Maximization and Security, we carried out the deployment 
                    of our tool to determine the areas of improvement and establish a rectification plan to eliminate the 
                    software excess, among other things.";
    public $lg0114 = "DDWEB.png";
    public $lg0115 = "BENEFITS";
    public $lg0116 = "sello2.png";
    public $lg0117 = "Optimization of: Duplicates, Erroneous, Inactive,Disuse, Virtualization";
    public $lg0118 = "Better and better application control";
    public $lg0119 = "Quick results";
    public $lg0120 = "Easy and effective";
    public $lg0121 = "Better control, ... higher profitability";
    public $lg0122 = "https://www.youtube.com/embed/6gAjtmw66ak";
    //fin deployment diagnostic
    
    //inicio overview
    public $lg0123 = "oi2.png";
    public $lg0153 = '<map name="map">
        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
        <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
        <!-- #$-:Please do not edit lines starting with "#$" -->
        <!-- #$VERSION:2.3 -->
        <!-- #$AUTHOR:Dx -->
        <area shape="rect" coords="237,48,465,239" href="contact.php" target="_blank" alt="Deployment Diagnostic purchase here"/>
        <area shape="rect" coords="477,48,708,241" href="contact.php" target="_blank" alt="Cloud Assestment purchase here"/>
        <area shape="rect" coords="716,48,944,240" href="contact.php" target="_blank" alt="SAM Diagnostic purchase here"/>
        <area shape="rect" coords="958,12,1207,241" href="contact.php" target="_blank" alt="SAM Administration purchase here"/>
        </map>';
    //fin overview
    
    //inicio articles
    public $lg0124 = "IMPORTANT AND TIMELY INFORMATION";
    public $lg0125 = "ARTICLES LICENSING ASSURANCE";
   public $lg0126 = "https://www.licensingassurance.com/ComunicadoAlianzaLA&Mazars.pdf"; //"https://www.gartner.com/newsroom/id/3382317";
    public $lg0127 = "Licensing Assurance y Mazars firman alianza para ofrecer servicio de administración de Licenciamiento y consultoría a empresas mexicanas"; /*"Gartner Says Organizations Can Cut Software Costs by 30 Percent Using Three Best Practices";*/
    public $lg0128 = "Licensing Assurance, uno de los principales proveedores de servicios de administración de activos de software con basta experiencia en toda América 
                    y Mazars, importante empresa de consultoría, accouting & outsourcing, impuestos y asesoría financiera en Mexico, firmam importante acuerdo estratégico 
                    de alianza comercial para expandir las operaciones en Mexico."; /*"Many organizations can cut spending on software by as much 
                    as 30 percent by implementing three software license optimization best practices, according 
                    to research from Gartner, Inc. The keys to reducing software license spending are application 
                    configuration optimization, recycling software licenses and by using software asset management 
                    (SAM) tools.";*/
    public $lg0129 = "http://factorypyme.thestandardit.com/2015/08/17/es-el-software-independiente-una-solucion-para-las-empresas/";
    public $lg0130 = "Is independent software a solution for companies?";
    public $lg0131 = "Licensing Assurance is a pioneer in the software management 
                    market independently, focused on software optimization and licensing. The company states 
                    that in Latin America, it is important to have independent advice for making decisions when 
                    purchasing software.";
    public $lg0132 = "http://samforyoula.blogspot.com/";
    public $lg0133 = "5 errors to avoid during the execution of Software Asset Management processes";
    public $lg0134 = "The SAM works according to four steps or phases: first 
                    an analysis of the facilities is carried out in order to make a diagnosis process of their status. 
                    Then the execution of the licensing is determined, identifying faults and rights. In the next step 
                    a report is prepared of everything investigated and finally proceeds to carry out the negotiation, 
                    that is, the purchase of licenses. For you to have more information on this topic, we will show you 
                    a brief selection of 5 common mistakes that should be avoided when running SAM processes, so that 
                    they can be more profitable.";
    public $lg0135 = "We have many more things to say, ... Visit our Blog";
    public $lg0136 = "Let's talk";
    public $lg0155 = "";
    public $lg0156 = "";
    public $lg0157 = "";
    //fin articles
    
    //inicio contact
    public $lg0137 = "CONTACT US";
    public $lg0138 = "Phone: (305) 851-3545<br>
                    E-mail: info@licensingassurance.com<br><br>
                    
                    Licensing Assurance LLC<br>
                    16192 Coastal Highway<br>
                    Lewes, DE 19958";
    public $lg0139 = "Name";
    public $lg0140 = "Email";
    public $lg0141 = "Phone";
    public $lg0142 = "Country";
    public $lg0143 = "Company";
    public $lg0144 = "Subject";
    public $lg0145 = "Message";
    public $lg0146 = "Send";
    public $lg0147 = "For all your questions do not hesitate to 
                    send us an email or call us at any time.";
    public $lg0148 = "United States";
    public $lg0149 = "Mexico";
    public $lg0150 = "Panama";
    public $lg0151 = "Brazil";
    public $lg0152 = "<p>Thanks for contacting us! We will get back to you very soon.</p>";
    public $lg0154 = "Peru";
    public $lg0158 = "Office Contact Us";
    public $lg0159 = "SPLA LATAM";
    public $lg0160 = "Diagnostic";
    public $lg0161 = "Accounting Department";
    public $lg0162 = "Sales Manager LATAM";
    public $lg0163 = "CS Director";
    public $lg0164 = "Administrator";
    //fin contact
    
    //inicio redireccionamiento navegacion
    public $nv0001 = "index.php";
    public $nv0002 = "aboutUs.php";
    public $nv0003 = "services.php";
    public $nv0004 = "SAMasaService.php";
    public $nv0005 = "auditDefense.php";
    public $nv0006 = "SPLA.php";
    public $nv0007 = "deployment.php";
    public $nv0008 = "overview.php";
    public $nv0009 = "articles.php";
    public $nv0010 = "eventsColombia.php";
    public $nv0011 = "eventsMexico.php";
    public $nv0012 = "contact.php";
    public $nv0013 = "webinar.php";
    public $nv0014 = "webinar_licenciamiento_SAP.php";
    //fin redireccionamiento navegacion
    
    //inicio foot
    public $ft0001 = "OUR PRODUCTS";
    public $ft0002 = "OUR TEAM";
    public $ft0003 = "index.php?videos=true";
    public $ft0004 = "CONTACT US!!";
    public $ft0005 = "Phone: (305) 851-3545";
    public $ft0006 = "info@licensingassurance.com";
    public $ft0007 = "Licensing Assurance LLC";
    public $ft0008 = "16192 Coastal Highway";
    public $ft0009 = "Lewes, DE 19958";
    public $ft0010 = "We are available";
    public $ft0011 = "Monday - Friday";
    public $ft0012 = "8:00 am - 5:00 pm";
    public $ft0013 = "East Time";
    public $ft0014 = "Copyright Licensing Assurance LLC. All Rights Reserved";
    public $ft0015 = "Follow us and Share";
    //fin foot
}
