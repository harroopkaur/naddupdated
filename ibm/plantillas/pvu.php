<?php
if ($modificar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'PVU actualizado con éxito', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/ibm/pvu.php';
        });
    </script>
<?php  
} else if ($modificar == 1 && $exito == 0){
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar el PVU', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/ibm/pvu.php';
        });
    </script>
<?php  
} else if ($modificar == 1 && $exito == 2){
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todos los registros del PVU', {'Aceptar': 'Aceptar'}, function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/ibm/pvu.php';
        });
    </script>
<?php
}
?>
    
<div style="padding:20px; overflow:hidden;">
    <form id="form1" name="form1" method="post" action="pvu.php">
        <input type="hidden" id="modificar" name="modificar" value="1">
        
        <div id="ttabla1"  style="width:99%; max-height:400px; overflow:auto;">
            <table style="width:1712px;" class="tablap" id="tablaPVU">
                <thead>
                <tr style="background:#333; color:#fff;">
                    <th rowspan="2" align="center" valign="middle"><span>Servidor</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Tipo de Procesador</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Producto</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Tiene</span></th>
                    <th colspan="2" align="center" valign="middle"><span>Lo que Usa</span></th>
                    <th rowspan="2" align="center" valign="middle"><span>PVU</span></th>
                    <th rowspan="2" align="center" valign="middle">Conteo<span></span></th>
                    <th rowspan="2" align="center" valign="middle"><span>Total PVU</span></th>
                </tr>
                <tr style="background:#333; color:#fff;">
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                    <th align="center" valign="middle"><span>Conteo Procesadores</span></th>
                    <th align="center" valign="middle"><span>Conteo Core</span></th>
                </tr>
                </thead>
                <tbody id="tabla">
                    <?php 
                    $i=0;
                    foreach($tablaPVU as $row){
                    ?>
                        <tr>
                            <td><input type="hidden" name="idPvuCliente[]" value="<?= $row["id"] ?>"><?= $row["servidor"] ?></td>
                            <td><?= $row["tipoProcesador"] ?></td>
                            <td><?= $row["producto"] ?></td>
                            <td class="text-center"><?= $row["conteoProcesadores"] ?></td>
                            <td class="text-center"><?= $row["conteoCore"] ?></td>
                            <td><input type="text" id="usaProc<?= $i ?>" name="usaProc[]" value="<?= $row["usaProcesadores"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaProc<?= $i ?>')" readonly></td>
                            <td><input type="text" id="usaCore<?= $i ?>" name="usaCore[]" value="<?= $row["usaCore"] ?>" maxlength="2" style="width:50px;" onclick="limpiar('usaCore<?= $i ?>')" readonly></td>
                            <td class="text-center"><?= $row["pvu"] ?></td>
                            <td class="text-center"><?= $row["conteo"] ?></td>
                            <td class="text-center"><?= $row["totalPvu"] ?></td>
                        </tr>
                    <?php 
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <br>
        <div style="float:left; margin-left:0;" class="botonesSAM boton5" id="editar">Editar</div>
        <div style="float:left;" class="botonesSAM boton5" id="actualizar">Actualizar</div>
    </form>
</div>

<script>
    $(document).ready(function(){
        //$("#tablaPVU").tableHeadFixer(); 
        $("#tablaPVU").tableHeadFixer({"left" : 3});
        
        $("#editar").click(function(){
            for(i = 0; i < $("#tabla tr").length; i++){
                $("#usaProc" + i).removeAttr("readonly");
                $("#usaCore" + i).removeAttr("readonly");
            }
            $("#ttabla1").scrollLeft(300);
        });
        
        $("#actualizar").click(function(){
            $("#form1").submit();
        });
    });
    
    function limpiar(variable){
        if(!$("#" + variable).prop("readonly")){
            $("#" + variable).val("");
        }
    }
</script>