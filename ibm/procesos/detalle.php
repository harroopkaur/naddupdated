<?php
require_once("../configuracion/inicio.php");
require_once("../plantillas/sesion.php");
require_once("../clases/clase_general.php");
require_once("../clases/clase_tabla_maestra.php");
//require_once("../clases/clase_balance_ibm.php");
require_once("../clases/clase_resumen_ibm.php");
require_once("../clases/clase_equivalenciasPDO.php");

//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once("../plantillas/middleware.php");
//fin middleware

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$tablaMaestra = new tablaMaestra();
//$balance      = new balanceIbm();
$resumen      = new resumenIbm();
$equivalencia = new EquivalenciasPDO();
$general      = new General();

$asig = "";
if(isset($_GET['asig'])){
    $asig = $general->get_escape($_GET["asig"]);
}

$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

$total_i     = 0;
$vert1 = "Todo";
if(isset($_GET['vert'])){
    $vert  = $general->get_escape($_GET['vert']);
    if(isset($_GET['vert1'])){
        $vert1 = $general->get_escape($_GET['vert1']); 
    }
}
else{
    $vert  = 'DS Storage';
    //$vert1 = 'Manager Host';
}

if($vert != "Otros"){
    $ediciones = $equivalencia->edicionesProductoxNombre(2, $vert);
    /*if(!isset($_GET['vert1'])){
        $vert1 = $ediciones[0]["nombre"];
    }*/
    
    $titulo = $vert . ' ' . $vert1;
}
else{
    $balance->productosNoIncluir($_SESSION["client_id"], $_SESSION["client_empleado"]); 
    $ediciones = $equivalencia->ProductoxNombre(2, $balance->listaNoIncluir);
    if(!isset($_GET['vert1'])){
        $vert1 = $ediciones[0]["nombre"];
    }
    
    $titulo = $vert1;
}

if($vert1 != "Todo"){
    $result = $resumen->listar_datos6AgrupadoAsignacion($_SESSION['client_id'], $vert, $vert1, $asig, $asignaciones);
} else if($vert1 == "Todo"){
    $result = $resumen->listar_datos6AgrupadoTodosAsignacion($_SESSION['client_id'], $vert, $asig, $asignaciones);
}

if(count($result) > 0){
    $total_i = 0;
    foreach($result as $reg_calculo){
        $total_i += $reg_calculo["cantidad"];
    }	
}

if($vert1  != "Todo"){
    $listado = $resumen->listar_datos6Asignacion($_SESSION['client_id'], $vert, $vert1, $asig, $asignaciones);
} else if($vert1 == "Todo"){
    $listado = $resumen->listar_datos6TodosAsignacion($_SESSION['client_id'], $vert, $asig, $asignaciones);
}