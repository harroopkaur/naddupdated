<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$compra_datos  = new comprasIbm();
$resumen       = new resumenIbm();
$balance_datos = new balanceIbm();
$equivalencia  = new Equivalencias();
$general       = new General();
$detalle       = new DetallesIbm();
$archivosDespliegue = new clase_archivos_fabricantes();
$nueva_funcion = new funcionesSam();
$log = new log();

$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 2);

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {

    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];

    // Validaciones
    $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
    $long = count($extension) - 1;
    if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
        $error = 1;
    }  // Permitir subir solo imagenes JPG,
    
    if($general->obtenerSeparadorUniversal($temporal_imagen, 1, 7) === true){
        if (($fichero = fopen($temporal_imagen, "r")) !== FALSE) {
            $i = 1;
            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                if($i == 1 && ($datos[0] != "Producto" || utf8_encode($datos[1]) != "Edición" || utf8_encode($datos[2]) != "Versión" || 
                $datos[3] != "SKU" || $datos[4] != "Tipo" || $datos[5] != "Cantidades" || $datos[6] != "Precio Unitario" ||
                utf8_encode($datos[7]) != "Asignación")){
                    $error = 3;
                    break;
                } else{
                    break;
                }
                $i++;
            }
        } else{
            $error =  4;
        }
    }//cago

    if ($error == 0) {

        $imagen = $_SESSION['client_id'] . $_SESSION["client_empleado"] . "_comprasIBM" . date("dmYHis") . ".csv";

        if (!$compra_datos->cargar_archivo($imagen, $temporal_imagen)) {
            $error = 5;
        } else {

            $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            $balance_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

            if($general->obtenerSeparadorUniversal("archivos_csvf4/" . $imagen, 1, 7) === true){
                if (($fichero = fopen("archivos_csvf4/" . $imagen, "r")) !== FALSE) {
                    $i = 1;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        $compraCantidad = 0;
                        if(filter_var($datos[5], FILTER_VALIDATE_INT) !== false){
                            $compraCantidad = $datos[5];
                        }

                        $precio = 0;
                        if(filter_var($datos[6], FILTER_VALIDATE_FLOAT) !== false){
                            $precio = $datos[6];
                        }

                        $asignacion = $general->get_escape(utf8_encode($datos[7]));
                        if($general->validarAsignacion($_SESSION["client_id"], $_SESSION["client_empleado"], $asignacion)){
                            if ($i > 1 && !$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[0], $datos[1], $datos[2], $compraCantidad, $precio, $asignacion)) {
                                echo $compra_datos->error;
                            } //exito
                        }
                        $i++;
                        $exito2 = 1;
                    }//WhILE

                    if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 2) === false){
                        $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 2);
                    }
                    $archivosDespliegue->actualizarCompras($_SESSION["client_id"], $_SESSION["client_empleado"], 2, $imagen);
                }//FICHERO
            }

        }//is error
    }


    if ($exito2 == 1){
        /*$listaEquivalencia = $equivalencia->listar_todo(2); //1 es el id fabricante IBM
        if ($listaEquivalencia) {
            foreach ($listaEquivalencia as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;

                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
               
                if($compra_datos->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    $resultCompra = $compra_datos->row;
                    $compra       = $resultCompra["compra"];
                    $precio       = $resultCompra["precio"];
                }
                
                if ($resumen->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumen->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen

                if($precio == 0 || $precio == ""){
                    $precio = $reg_o["precio"];
                }
                
                $balancec1 = $compra - $instalaciones;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                $tipo = 1;
             
                //$balance_datos->insertar($_SESSION['client_id'],$newFamilia,$newEdicion, $newVersion,$reg_o->precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
            }//office
        }//office*/

        $lista_compras = $balance_datos->listarBalanzaAgregar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        foreach($lista_compras as $reg_o){
            $instalaciones = $reg_o["instalacion"];
            $compra = $reg_o["compra"];
            $newFamilia = $reg_o["familia"];
            $newEdicion = $reg_o["edicion"];
            $newVersion = $reg_o["version"];
            $precio = $balance_datos->obtenerPrecio($_SESSION["client_id"], $_SESSION["client_empleado"], $newFamilia,
            $newEdicion, $newVersion);
            $balancec1=$compra-$instalaciones;
            $balancec2=$balancec1*$precio;
            $asig = $reg_o["asignacion"];

            if(strpos($newFamilia, "Windows Server") !== false){
                $tipo=2;	
            }
            else if(strpos($newFamilia, "Windows") !== false){
                $tipo=1;	
            }
            else if($balancec2<0){
                $tipo=2;	
            }else{
                $tipo=1;	
            }
            if(!$balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, 
            $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo, $asig)){
                echo $balance_datos->error . "<br>";
            }
        }
        
        $exito2 = 1;
        
        $log->insertar(10, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Compras Formulario");
    }
}