<?php
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
//require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

$tablaMaestra = new tablaMaestra();
$detalles     = new DetallesIbm();
//$balance      = new balanceIbm();
$resumen      = new resumenIbm();
$compras      = new comprasIbm();
$general = new General();

$tablaMaestra->productosSalida(2);
$total_1LAc       = 0;
$total_1InacLAc   = 0;
$total_2DVc       = 0;
$total_1LAs       = 0;
$total_1InacLAs   = 0;
$total_2DVs       = 0;
$reconciliacion1c = 0;
$reconciliacion1s = 0;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$total_1 = 0;
$total_2 = 0;
$total_3 = 0;
$total_4 = 0;
$total_5 = 0;
$total_6 = 0;
$tclient = 0;
$tserver = 0;
$activosc = 0;
$activoss = 0;

$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';

$vert = 0;
if (isset($_GET['vert']) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false) {
    $vert = $_GET['vert'];
}

$asig = "";
if (isset($_GET['asig'])) {
    $asig = $_GET['asig'];
}

$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

if ($vert == 0) {
    $lista = $detalles->listar_todo1Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    foreach ($lista as $reg_equipos) {

        if ($reg_equipos["tipo"] == 1) {//cliente
            $tclient = ($tclient + 1);
            if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                $total_1LAc = ($total_1LAc + 1);
            }

            if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_2DVc = ($total_2DVc + 1);
            }
            else{
                $total_1InacLAc++;
            }
        } else {//server
            if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                $total_1LAs = ($total_1LAs + 1);
            }

            $tserver = ($tserver + 1);

            if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_2DVs = ($total_2DVs + 1);
            }
            else{
                $total_1InacLAs++;
            }
        }
    }
    $reconciliacion1c = $total_2DVc - $total_1LAc;
    $reconciliacion1s = $total_2DVs - $total_1LAs;
}


if ($vert == 1 || $vert == 3) {
$lista = $detalles->listar_todo1Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    foreach ($lista as $reg_equipos) {

        if ($reg_equipos["tipo"] == 1) {
            $tclient = ($tclient + 1);

            if ($reg_equipos["rango"] == 1) {
                $total_1 = ($total_1 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activosc = ($activosc + 1);
                }
            } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_2 = ($total_2 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activosc = ($activosc + 1);
                }
            } else {
                $total_3 = ($total_3 + 1);
            }
        } else {//server
            $tserver = ($tserver + 1);

            if ($reg_equipos["rango"] == 1) {
                $total_4 = ($total_4 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activoss = ($activoss + 1);
                }
            } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_5 = ($total_5 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activoss = ($activoss + 1);
                }
            } else {
                $total_6 = ($total_6 + 1);
            }
        }
    }
}

if ($vert == 2) {
    $balanza = true;
    
    //$TotalDSStorageCompras = 0;
    $TotalDSStorageUsar    = 0;
    $TotalDSStorageSinUsar = 0;
    
    //$TotalInformixCompras = 0;
    $TotalInformixUsar    = 0;
    $TotalInformixSinUsar = 0;
    
    //$TotalSPSSCompras = 0;
    $TotalSPSSUsar    = 0;
    $TotalSPSSSinUsar = 0;
    
    //$TotalIntegrationCompras = 0;
    $TotalIntegrationUsar    = 0;
    $TotalIntegrationSinUsar = 0;
    
    //$TotalSterlingCompras = 0;
    $TotalSterlingUsar    = 0;
    $TotalSterlingSinUsar = 0;
    
    //$TotalTivoliStorageCompras = 0;
    $TotalTivoliStorageUsar    = 0;
    $TotalTivoliStorageSinUsar = 0;
    
    //$TotalWebserviceCompras = 0;
    $TotalWebserviceUsar    = 0;
    $TotalWebserviceSinUsar = 0;
    
    //$TotalWebsphereCompras = 0;
    $TotalWebsphereUsar    = 0;
    $TotalWebsphereSinUsar = 0;
 
    $clientTotalProductCompras = 0;
    $clientTotalProductInstal  = 0;
    $clientProductNeto         = 0;
    
    //DS Storage
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "DS Storage")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalDSStorageCompras += $reg_equipos["compra"];
            //$TotalDSStorageInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "DS Storage")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalDSStorageUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalDSStorageSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalDSStorageInstal = $TotalDSStorageUsar + $TotalDSStorageSinUsar;
    $DSStorageNeto = $TotalDSStorageCompras - $TotalDSStorageInstal;*/
    $listar_DSStorage = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "DS Storage", "Todo", $asig, $asignaciones); 
    foreach ($listar_DSStorage as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalDSStorageUsar += $reg_equipos["conteo"];
        } else{
            $TotalDSStorageSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalDSStorageCompras = $compras->totalCompras($_SESSION['client_id'], "DS Storage", "Todo", $asig, $asignaciones);
    $DSStorageNeto = $TotalDSStorageCompras - ($TotalDSStorageUsar + $TotalDSStorageSinUsar);
    
    //Informix
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "Informix")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalInformixCompras += $reg_equipos["compra"];
            //$TotalInformixInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "Informix")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalInformixUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalInformixSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalInformixInstal = $TotalInformixUsar + $TotalInformixSinUsar;
    $InformixNeto = $TotalInformixCompras - $TotalInformixInstal;*/
    $listar_Informix = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Informix", "Todo", $asig, $asignaciones); 
    foreach ($listar_Informix as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalInformixUsar += $reg_equipos["conteo"];
        } else{
            $TotalInformixSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalInformixCompras = $compras->totalCompras($_SESSION['client_id'], "Informix", "Todo", $asig, $asignaciones);
    $InformixNeto = $TotalInformixCompras - ($TotalInformixUsar + $TotalInformixSinUsar);
    
    //SPSS
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "SPSS")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalSPSSCompras += $reg_equipos["compra"];
            //$TotalSPSSInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "SPSS")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalSPSSUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalSPSSSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalSPSSInstal = $TotalSPSSUsar + $TotalSPSSSinUsar;
    $SPSSNeto = $TotalSPSSCompras - $TotalSPSSInstal;*/
    $listar_SPSS = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "SPSS", "Todo", $asig, $asignaciones); 
    foreach ($listar_SPSS as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalSPSSUsar += $reg_equipos["conteo"];
        } else{
            $TotalSPSSSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalSPSSCompras = $compras->totalCompras($_SESSION['client_id'], "SPSS", "Todo", $asig, $asignaciones);
    $SPSSNeto = $TotalSPSSCompras - ($TotalSPSSUsar + $TotalSPSSSinUsar);
    
    //Integration
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "Integration")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalIntegrationCompras += $reg_equipos["compra"];
            //$TotalIntegrationInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "Integration")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalIntegrationUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalIntegrationSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalIntegrationInstal = $TotalIntegrationUsar + $TotalIntegrationSinUsar;
    $IntegrationNeto = $TotalIntegrationCompras - $TotalIntegrationInstal;*/
    $listar_Integration = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Integration", "Todo", $asig, $asignaciones); 
    foreach ($listar_Integration as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalIntegrationUsar += $reg_equipos["conteo"];
        } else{
            $TotalIntegrationSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalIntegrationCompras = $compras->totalCompras($_SESSION['client_id'], "Integration", "Todo", $asig, $asignaciones);
    $IntegrationNeto = $TotalIntegrationCompras - ($TotalIntegrationUsar + $TotalIntegrationSinUsar);
    
    //Sterling
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "Sterling")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalSterlingCompras += $reg_equipos["compra"];
            //$TotalSterlingInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "Sterling")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalSterlingUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalSterlingSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalSterlingInstal = $TotalSterlingUsar + $TotalSterlingSinUsar;
    $SterlingNeto = $TotalSterlingCompras - $TotalSterlingInstal;*/
    $listar_Sterling = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Sterling", "Todo", $asig, $asignaciones); 
    foreach ($listar_Sterling as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalSterlingUsar += $reg_equipos["conteo"];
        } else{
            $TotalSterlingSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalSterlingCompras = $compras->totalCompras($_SESSION['client_id'], "Sterling", "Todo", $asig, $asignaciones);
    $SterlingNeto = $TotalSterlingCompras - ($TotalSterlingUsar + $TotalSterlingSinUsar);
    
    //Tivoli Storage
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "Tivoli Storage")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalTivoliStorageCompras += $reg_equipos["compra"];
            //$TotalTivoliStorageInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "Tivoli Storage")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalTivoliStorageUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalTivoliStorageSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalTivoliStorageInstal = $TotalTivoliStorageUsar + $TotalTivoliStorageSinUsar;
    $TivoliStorageNeto = $TotalTivoliStorageCompras - $TotalTivoliStorageInstal;*/
    $listar_TivoliStorage = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Tivoli Storage", "Todo", $asig, $asignaciones); 
    foreach ($listar_TivoliStorage as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalTivoliStorageUsar += $reg_equipos["conteo"];
        } else{
            $TotalTivoliStorageSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalTivoliStorageCompras = $compras->totalCompras($_SESSION['client_id'], "Tivoli Storage", "Todo", $asig, $asignaciones);
    $TivoliStorageNeto = $TotalTivoliStorageCompras - ($TotalTivoliStorageUsar + $TotalTivoliStorageSinUsar);
    
    //Webservice
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "Webservice")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalWebserviceCompras += $reg_equipos["compra"];
            //$TotalWebserviceInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "Webservice")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalWebserviceUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalWebserviceSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalWebserviceInstal = $TotalWebserviceUsar + $TotalWebserviceSinUsar;
    $WebserviceNeto = $TotalWebserviceCompras - $TotalWebserviceInstal;*/
    $listar_Webservice = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Webservice", "Todo", $asig, $asignaciones); 
    foreach ($listar_Webservice as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalWebserviceUsar += $reg_equipos["conteo"];
        } else{
            $TotalWebserviceSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalWebserviceCompras = $compras->totalCompras($_SESSION['client_id'], "Webservice", "Todo", $asig, $asignaciones);
    $WebserviceNeto = $TotalWebserviceCompras - ($TotalWebserviceUsar + $TotalWebserviceSinUsar);
    
    //Websphere
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION["client_empleado"], "Websphere")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalWebsphereCompras += $reg_equipos["compra"];
            //$TotalWebsphereInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION["client_empleado"], "Websphere")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalWebsphereUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalWebsphereSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalWebsphereInstal = $TotalWebsphereUsar + $TotalWebsphereSinUsar;
    $WebsphereNeto = $TotalWebsphereCompras - $TotalWebsphereInstal;*/
    $listar_Websphere = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Websphere", "Todo", $asig, $asignaciones); 
    foreach ($listar_Websphere as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalDSStorageUsar += $reg_equipos["conteo"];
        } else{
            $TotalDSStorageSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalWebsphereCompras = $compras->totalCompras($_SESSION['client_id'], "Websphere", "Todo", $asig, $asignaciones);
    $WebsphereNeto = $TotalWebsphereCompras - ($TotalWebsphereUsar + $TotalWebsphereSinUsar);
}