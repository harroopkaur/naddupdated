<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$compra_datos  = new comprasIbm();
$resumen       = new resumenIbm();
$balance_datos = new balanceIbm();
$equivalencia  = new Equivalencias();
$general       = new General();
$detalle       = new DetallesIbm();
$nueva_funcion = new funcionesSam();
$log = new log();

$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 2);

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if (isset($_POST['insertarRepoF']) && $_POST["insertarRepoF"] == 1) {
    if ($error == 0) {
        $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $balance_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

        $tablaCompraRepo = array();
        if (isset($_POST["check"])) {
            $tablaCompraRepo = $_POST["check"];

            for ($i = 0; $i < count($tablaCompraRepo); $i++) {
                $newArray = explode("*", $tablaCompraRepo[$i]);
                $familia  = $general->get_escape($newArray[0]);
                $edicion  = $general->get_escape($newArray[1]);
                $version  = $general->get_escape($newArray[2]);
                $cantidad = 0;
                if(filter_var($newArray[3], FILTER_VALIDATE_FLOAT) !== false){
                    $cantidad = $newArray[3];
                }
                $precio = 0;
                if(filter_var($newArray[4], FILTER_VALIDATE_FLOAT) !== false){
                    $precio = $general->get_escape($newArray[4]);
                }
                
                $asignacion = $general->get_escape($newArray[5]);
                if($general->validarAsignacion($_SESSION["client_id"], $_SESSION["client_empleado"], $asignacion)){
                    if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], trim($familia), trim($edicion), trim($version), $cantidad, $precio, $asignacion)) {
                        echo $compra_datos->error;
                    }//exito*/
                }
                $exito2 = 1;
            }//WhILE
        }
    }

    if ($exito2 == 1){
        /*$listaEquivalencia = $equivalencia->listar_todo(2); //1 es el id fabricante Adobe
        if ($listaEquivalencia) {
            foreach ($listaEquivalencia as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;
                
                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
                
                if($compra_datos->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    $resultCompra = $compra_datos->row;
                    $compra       = $resultCompra["compra"];
                    $precio       = $resultCompra["precio"];
                }
                
                //echo $newFamilia.",". $newEdicion.",". $newVersion;
                if ($resumen->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumen->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen

                if($compra == ""){
                    $compra = 0;
                }
                
                if($precio == ""){
                    $precio = 0;
                }
                
                $balancec1 = $compra - $instalaciones;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                $tipo = 1;
                //$balance_datos->insertar($_SESSION['client_id'],$newFamilia,$newEdicion, $newVersion,$reg_o->precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
            }//office
        }//office*/
        $lista_compras = $balance_datos->listarBalanzaAgregar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        foreach($lista_compras as $reg_o){
            $instalaciones = $reg_o["instalacion"];
            $compra = $reg_o["compra"];
            $newFamilia = $reg_o["familia"];
            $newEdicion = $reg_o["edicion"];
            $newVersion = $reg_o["version"];
            $precio = $balance_datos->obtenerPrecio($_SESSION["client_id"], $_SESSION["client_empleado"], $newFamilia,
            $newEdicion, $newVersion);
            $balancec1=$compra-$instalaciones;
            $balancec2=$balancec1*$precio;
            $asig = $reg_o["asignacion"];

            if(strpos($newFamilia, "Windows Server") !== false){
                $tipo=2;	
            }
            else if(strpos($newFamilia, "Windows") !== false){
                $tipo=1;	
            }
            else if($balancec2<0){
                $tipo=2;	
            }else{
                $tipo=1;	
            }
            if(!$balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, 
            $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo, $asig)){
                echo $balance_datos->error . "<br>";
            }
        }
        
        $exito2 = 1;
        
        $log->insertar(10, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Compras Repositorio");
    }
}