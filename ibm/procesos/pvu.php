<?php   
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente.php");

$pvu = new pvuCliente();
$general = new General();
$modificar = 0;
$tablaPVU = array();

if(isset($_POST["modificar"]) && $_POST["modificar"] == 1){
    $modificar = 1;
    $idPvuCliente = array();
    $usaProc = array();
    $usaCore = array();
    
    if(isset($_POST["idPvuCliente"])){
        $idPvuCliente = $_POST["idPvuCliente"];
    }
    
    if(isset($_POST["usaProc"])){
        $usaProc = $_POST["usaProc"];
    }
    
    if(isset($_POST["usaCore"])){
        $usaCore = $_POST["usaCore"];
    }
    
    $j = 0;
    for($i = 0; $i < count($idPvuCliente); $i++){
        $id = 0;
        if(filter_var($idPvuCliente[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idPvuCliente[$i];
        }
        
        $proc = 0;
        if(filter_var($usaProc[$i], FILTER_VALIDATE_INT) !== false){
            $proc = $usaProc[$i];
        }
        
        $core = 0;
        if(filter_var($usaCore[$i], FILTER_VALIDATE_INT) !== false){
            $core = $usaCore[$i];
        }
        
        if($pvu->actualizar($id, $proc, $core)){
           $j++; 
        }
    }
    
    if($j == 0){
        $exito = 0;
    }
    else if($j > 0 && $j == $i){
        $exito = 1;
    }
    else{
        $exito = 2;
    }
    
    $log->insertar(11, 2, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
} else{
    $tablaPVU = $pvu->listadoPVUCliente($_SESSION["client_id"], $_SESSION["client_empleado"]);
}