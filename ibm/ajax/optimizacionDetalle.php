<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $optimizacion = new DetallesIbm();
            $opcion = "";
            if(isset($_POST["opcion"])){
                $opcion = $general->get_escape($_POST['opcion']);
            }

            $os = "Windows";
            if($opcion == "servidor"){
                $os = "Windows Server"; 
            }
            
            $asig = "";
            if(isset($_POST["asig"])){
                $asig = $general->get_escape($_POST["asig"]); 
            }
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $tabla = '<div style="width:98%; height:400px; overflow-x:hidden; overflow-y:auto;">
                <table class="tablap" id="tablaOptimizacion" style="width:2500px; margin-top:-5px;">
                <thead>
                    <tr style="background:#333; color:#fff;">
                        <th align="center" valign="middle"><span>&nbsp;</span></th>
                        <th align="center" valign="middle"><span>Equipo</span></th>
                        <th align="center" valign="middle"><span>IBM DS Storage</span></th>
                        <th align="center" valign="middle"><span>IBM High</span></th>
                        <th align="center" valign="middle"><span>IBM Informix</span></th>
                        <th align="center" valign="middle"><span>IBM SPSS</span></th>
                        <th align="center" valign="middle"><span>IBM Integration</span></th>
                        <th align="center" valign="middle"><span>IBM Security AppScan</span></th>
                        <th align="center" valign="middle"><span>IBM Sterling Certificate</span></th>
                        <th align="center" valign="middle"><span>IBM Sterling Connect</span></th>
                        <th align="center" valign="middle"><span>IBM Sterling External</span></th>
                        <th align="center" valign="middle"><span>IBM Sterling Secure Proxy</span></th>
                        <th align="center" valign="middle"><span>IBM Tivoli Storage</span></th>
                        <th align="center" valign="middle"><span>IBM Web service</span></th>
                        <th align="center" valign="middle"><span>IBM WebSphere</span></th>
                        <th align="center" valign="middle"><span>Usabilidad</span></th>
                        <th align="center" valign="middle"><span>Observaci&oacute;n</span></th>
                    </tr>
                </thead>
                <tbody >';

            $i = 1;
            $lista = $optimizacion->listar_optimizacionAsignacion($_SESSION['client_id'], $os, $asig, $asignaciones);
            foreach ($lista as $reg_equipos) {
                $tabla .= '<tr>
                            <td>' . $i . '</td>
                            <td>' . $reg_equipos["equipo"] . '</td>
                            <td>' . $reg_equipos["IBMDSStorage"] . '</td>
                            <td>' . $reg_equipos["IBMHigh"] . '</td>
                            <td>' . $reg_equipos["IBMInformix"] . '</td>
                            <td>' . $reg_equipos["IBMSPSS"] . '</td>
                            <td>' . $reg_equipos["IBMIntegration"] . '</td>
                            <td>' . $reg_equipos["IBMSecurityAppScan"] . '</td>
                            <td>' . $reg_equipos["IBMSterlingCertificate"] . '</td>
                            <td>' . $reg_equipos["IBMSterlingConnect"] . '</td>
                            <td>' . $reg_equipos["IBMSterlingExternal"] . '</td>
                            <td>' . $reg_equipos["IBMSterlingSecureProxy"] . '</td>
                            <td>' . $reg_equipos["IBMTivoliStorage"] . '</td>
                            <td>' . $reg_equipos["IBMWebservice"] . '</td>
                            <td>' . $reg_equipos["IBMWebSphere"] . '</td>
                            <td>' . $reg_equipos["usabilidad"] . '</td>
                            <td>' . $reg_equipos["duplicado"] . '</td>
                        </tr>';
                $i++;
            }

            $tabla .= '</tbody>
                    </table>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#tablaOptimizacion").tablesorter();
                        $("#tablaOptimizacion").tableHeadFixer();
                    });
                </script>';
            $array = array(0 => array('sesion'=>$sesion, 'mensaje'=>'', 'tabla' => $tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);