<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_ibmAux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores_ibmAux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pvu_cliente.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_filepcs_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_ibm.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_ibm.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $resumen      = new resumenIbm();
            $consolidado  = new consolidadoIbm();
            $consolidadoAux  = new consolidadoIbmAux();
            $archivosDespliegue = new clase_archivos_fabricantes();
            $procesadores = new consolidadoProcesadoresIBM();
            $procesadoresAux = new consolidadoProcesadoresIBMAux();
            $pvu = new pvuCliente();
            $detalles     = new DetallesIbm();
            $filepcs      = new filepcsIbm();
            $escaneo      = new escaneoIbm();
            $compras  = new comprasIbm();
            $balance = new balanceIbm();
          
            $i = 12;
            $j = 0;
            if($consolidado->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($consolidadoAux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($resumen->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
           
            if($archivosDespliegue->eliminarArchivosFabricante($_SESSION["client_id"], $_SESSION["client_empleado"], 2)){
                $j++;
            }
            
            if($procesadores->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($procesadoresAux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($pvu->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($filepcs->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($detalles->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($escaneo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($compras->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($balance->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            $result = 0;
            if($i > $j){
                $result = 2;
            } else if($i == $j){
                $result = 1;
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);