<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_ibm.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $nombre        = "";
    $vert = "";
    if(isset($_POST["vert"])){
        $vert = $general->get_escape($_POST["vert"]);
    }
    
    $vert1 = "Todo";
    if(isset($_POST["vert1"])){
        $vert1 = $general->get_escape($_POST["vert1"]);
    }

    //inicio opción seleccionada para el gráfico
    if($vert != "Otros"){
       $nombre = str_replace(" ", "", $vert) . str_replace(" ", "", $vert1). ".xlsx"; 
    }
    else{
       $nombre = str_replace(" ", "", $vert1). ".xlsx";  
    }
    
    $asig = "";
    if(isset($_POST['asig'])){
        $asig = $general->get_escape($_POST["asig"]);
    }

    $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

    //fin opción seleccionada para el gráfico
    $resumen   = new resumenIbm();
    //fin consultas de los diferentes gráficos

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("");


    // Add some data

    //inicio cabecera de la tabla
    $i = 2;
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '')
                ->setCellValue('B1', 'Equipo')
                ->setCellValue('C1', 'Tipo')
                ->setCellValue('D1', 'Familia')
                ->setCellValue('E1', 'Edición')
                ->setCellValue('F1', 'Versión')
                ->setCellValue('G1', 'Fecha Instalación')
                ->setCellValue('H1', 'Usabilidad')
                ->setCellValue('I1', 'Observación');
    //fin cabecera de la tabla

    //inicio cuerpo de la tabla
    if($vert1 != "Todo"){
        $result = $resumen->listar_datos6Asignacion($_SESSION['client_id'], $vert, $vert1, $asig, $asignaciones);
    } else if($vert1 == "Todo"){
        $result = $resumen->listar_datos6TodosAsignacion($_SESSION['client_id'], $vert, $asig, $asignaciones);
    }
    
    if(count($result) > 0){    
        $j = 1;
        foreach($result as $reg_calculo){            
            $duplicado = "";
            if($resumen->duplicado($_SESSION['client_id'], $reg_calculo["equipo"], $reg_calculo["familia"], $reg_calculo["edicion"], $reg_calculo["version"]) > 1){
                $duplicado = "Duplicado";
            }
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_calculo["equipo"])
                        ->setCellValue('C'.$i, $reg_calculo["tipo"])
                        ->setCellValue('D'.$i, $reg_calculo["familia"])
                        ->setCellValue('E'.$i, utf8_encode($reg_calculo["edicion"]))
                        ->setCellValue('F'.$i, $reg_calculo["version"])
                        ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                        ->setCellValue('H'.$i, $reg_calculo["rango"])
                        ->setCellValue('I'.$i, $duplicado);
            $i++;
            $j++;
        }	
    }
    //fin cuerpo de la tabla                                    

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesionReporte($mensaje);
}