<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $optimizacion = new DetallesIbm($conn);

    $vert1 = 0;
    if(isset($_GET["vert1"]) && filter_var($_GET['vert1'], FILTER_VALIDATE_INT) !== false){
        $vert1 = $_GET["vert1"];
    }
    
    $fecha = $optimizacion->fechaSAMArchivo($vert1);

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Optimización");


    // Add some data
    //inicio clientes
    $os = "Windows";

    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Clientes');
    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Equipo')
                ->setCellValue('C1', 'IBM DS Storage')
                ->setCellValue('D1', 'IBM High')
                ->setCellValue('E1', 'IBM Informix')
                ->setCellValue('F1', 'IBM SPSS')
                ->setCellValue('G1', 'IBM Integration')
                ->setCellValue('H1', 'IBM Security AppScan')
                ->setCellValue('I1', 'IBM Sterling Certificate')
                ->setCellValue('J1', 'IBM Sterling Connect')
                ->setCellValue('K1', 'IBM Sterling External')
                ->setCellValue('L1', 'IBM Sterling Secure Proxy')
                ->setCellValue('M1', 'IBM Tivoli Storage')
                ->setCellValue('N1', 'IBM Webservice')
                ->setCellValue('O1', 'IBM WebSphere')
                ->setCellValue('P1', 'Usabilidad')
                ->setCellValue('Q1', 'Observación');

    $i = 2;
    $j = 1;
    if($optimizacion->listar_optimizacionSamArchivo($vert1, $os)){
        foreach ($optimizacion->listaCliente as $reg_equipos) {
            $myWorkSheet->setCellValue('A' . $i, $j)
                        ->setCellValue('B' . $i, $reg_equipos["equipo"])
                        ->setCellValue('C' . $i, $reg_equipos["IBMDSStorage"])
                        ->setCellValue('D' . $i, $reg_equipos["IBMHigh"])
                        ->setCellValue('E' . $i, $reg_equipos["IBMInformix"])
                        ->setCellValue('F' . $i, $reg_equipos["IBMSPSS"])
                        ->setCellValue('G' . $i, $reg_equipos["IBMIntegration"])
                        ->setCellValue('H' . $i, $reg_equipos["IBMSecurityAppScan"])
                        ->setCellValue('I' . $i, $reg_equipos["IBMSterlingCertificate"])
                        ->setCellValue('J' . $i, $reg_equipos["IBMSterlingConnect"])
                        ->setCellValue('K' . $i, $reg_equipos["IBMSterlingExternal"])
                        ->setCellValue('L' . $i, $reg_equipos["IBMSterlingSecureProxy"])
                        ->setCellValue('M' . $i, $reg_equipos["IBMTivoliStorage"])
                        ->setCellValue('N' . $i, $reg_equipos["IBMWebservice"])
                        ->setCellValue('O' . $i, $reg_equipos["IBMWebSphere"])
                        ->setCellValue('P' . $i, $reg_equipos["usabilidad"])
                        ->setCellValue('Q' . $i, $reg_equipos["duplicado"]);
            $i++;
            $j++;
        }    
    }
    $objPHPExcel->addSheet($myWorkSheet, 0);
    //fin clientes


    //inicio servidores
    $os = "Windows Server";
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Servidores');
    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Equipo')
                ->setCellValue('C1', 'IBM DS Storage')
                ->setCellValue('D1', 'IBM High')
                ->setCellValue('E1', 'IBM Informix')
                ->setCellValue('F1', 'IBM SPSS')
                ->setCellValue('G1', 'IBM Integration')
                ->setCellValue('H1', 'IBM Security AppScan')
                ->setCellValue('I1', 'IBM Sterling Certificate')
                ->setCellValue('J1', 'IBM Sterling Connect')
                ->setCellValue('K1', 'IBM Sterling External')
                ->setCellValue('L1', 'IBM Sterling Secure Proxy')
                ->setCellValue('M1', 'IBM Tivoli Storage')
                ->setCellValue('N1', 'IBM Webservice')
                ->setCellValue('O1', 'IBM WebSphere')
                ->setCellValue('P1', 'Usabilidad')
                ->setCellValue('Q1', 'Observación');

    $i = 2;
    $j = 1;
    if($optimizacion->listar_optimizacionSamArchivo($vert1, $os)){
        foreach ($optimizacion->listaServidor as $reg_equipos) {
            $myWorkSheet->setCellValue('A' . $i, $j)
                        ->setCellValue('B' . $i, $reg_equipos["equipo"])
                        ->setCellValue('C' . $i, $reg_equipos["IBMDSStorage"])
                        ->setCellValue('D' . $i, $reg_equipos["IBMHigh"])
                        ->setCellValue('E' . $i, $reg_equipos["IBMInformix"])
                        ->setCellValue('F' . $i, $reg_equipos["IBMSPSS"])
                        ->setCellValue('G' . $i, $reg_equipos["IBMIntegration"])
                        ->setCellValue('H' . $i, $reg_equipos["IBMSecurityAppScan"])
                        ->setCellValue('I' . $i, $reg_equipos["IBMSterlingCertificate"])
                        ->setCellValue('J' . $i, $reg_equipos["IBMSterlingConnect"])
                        ->setCellValue('K' . $i, $reg_equipos["IBMSterlingExternal"])
                        ->setCellValue('L' . $i, $reg_equipos["IBMSterlingSecureProxy"])
                        ->setCellValue('M' . $i, $reg_equipos["IBMTivoliStorage"])
                        ->setCellValue('N' . $i, $reg_equipos["IBMWebservice"])
                        ->setCellValue('O' . $i, $reg_equipos["IBMWebSphere"])
                        ->setCellValue('P' . $i, $reg_equipos["usabilidad"])
                        ->setCellValue('Q' . $i, $reg_equipos["duplicado"]);
            $i++;
            $j++;
        }    
    }
    $objPHPExcel->addSheet($myWorkSheet, 1);
    //fin servidores

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    //$objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="excelOptimizacionIBM' . $fecha . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
?>
