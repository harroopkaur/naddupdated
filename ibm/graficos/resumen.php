<script type="text/javascript">
    $(function () {        
        <?php
        if ($vert == 0) {
        ?>
            $('#container1').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    size: '75%'
                },
                title: {
                    text: 'Cliente'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1LAc + $reconciliacion1c ?>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Equipos',
                    colorByPoint: true,
                    data: [{
                        name: 'No Levantado',
                        y: <?= $reconciliacion1c ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Levantado',
                        y: <?= $total_1LAc ?>,
                        color: '<?= $color2 ?>'
                    }]
                }]
            });
        });


        $(function () {
            $('#container2').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    size: '75%'
                },
                title: {
                    text: 'Servidor'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1LAs + $reconciliacion1s ?>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Equipos',
                    colorByPoint: true,
                    data: [{
                        name: 'No levantado',
                        y: <?= $reconciliacion1s ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Levantado',
                        y: <?= $total_1LAs ?>,
                        color: '<?= $color2 ?>'
                    }]
                }]
            });
        <?php
        }//0
        if ($vert == 1 || $vert == 3) {
            if ($vert == 1) {
                $titulo = "Usabilidad";
            } else {
                $titulo = "Optimización";
            }
        ?>
            $('#container2').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Clientes'
                },
                subtitle: {
                    text: '<?php echo $titulo; ?>'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Nro. Equipos'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1 + $total_2 + $total_3 ?></b><br/>'
                },
                series: [{
                    name: 'Clientes',
                    colorByPoint: true,
                    data: [{
                        name: 'En Uso',
                        y: <?= round($total_1, 0) ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Probablemente en Uso',
                        y: <?= round($total_2, 0) ?>,
                        color: '<?= $color2 ?>'
                    },
                    {
                        name: 'Obsoleto',
                        y: <?= round($total_3, 0) ?>,
                        color: '<?= $color3 ?>'
                    }]
                }]
            });
        });

        $(function () {
            // Create the chart
            $('#container1').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Servidores'
                },
                subtitle: {
                    text: '<?php echo $titulo; ?>'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Nro. Equipos'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b><?= $total_4 + $total_5 + $total_6 ?></b><br/>'
                },
                series: [{
                    name: 'Servidores',
                    colorByPoint: true,
                    data: [{
                        name: 'En Uso',
                        y: <?= round($total_4, 0) ?>,
                        color: '<?= $color1 ?>'
                    },
                    {
                        name: 'Probablemente en Uso',
                        y: <?= round($total_5, 0) ?>,
                        color: '<?= $color2 ?>'
                    },
                    {
                        name: 'Obsoleto',
                        y: <?= round($total_6, 0) ?>,
                        color: '<?= $color3 ?>'
                    }]
                }]
            });
        <?php
        }//1
        if ($vert == 2) {
        ?>
            $('#container3').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'DS Storage'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalDSStorageSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $DSStorageNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalDSStorageUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalDSStorageCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container4').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Informix'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalInformixSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $InformixNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalInformixUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalInformixCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container5').highcharts({    
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'SPSS'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalSPSSSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $SPSSNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalSPSSUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalSPSSCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container6').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Integration'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalIntegrationSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $IntegrationNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalIntegrationUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalIntegrationCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container7').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Sterling'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalSterlingSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $SterlingNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalSterlingUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalSterlingCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container8').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Tivoli Storage'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalTivoliStorageSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $TivoliStorageNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalTivoliStorageUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalTivoliStorageCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container9').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Webservice'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalWebserviceSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $WebserviceNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalWebserviceUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalWebserviceCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container10').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Websphere'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Obsoleto',
                    data: [0, <?= $TotalWebsphereSinUsar ?>, 0],
                    color: '<?= $color4 ?>'
                }, {
                    name: 'Neto',
                    data: [0, 0, <?= $WebsphereNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalWebsphereUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalWebsphereCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });
        <?php
        }//2
        ?>
     });
</script>