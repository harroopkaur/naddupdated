<?php 
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
$general = new General();
$base = "webtool";
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>.:Licensing Assurance:.</title>
        <!-- Custom Theme files -->
        <link rel="shortcut icon" href="../img/Logo.ico">
        
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="keywords" content="trial, sofware" />
        <!--Google Fonts-->
        
        <link href="../css/example.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style3.css" rel="stylesheet" type="text/css"/>
        <link href="../plugin-alert/css/alert.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?=$GLOBALS['domain_root']?>/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="../webtool/css/exampleSam.css" type="text/css" />
        
        <style>
            @import 'https://code.highcharts.com/5/css/highcharts.css';
        </style>
        <style>
            body {
                background: #00B0F0 url(../imagenes/inicio/bg_body.jpg) top left no-repeat !important;
                padding:0px !important;	

            }

            .botonesPOC{
                width                 : 250px;
                height                : 45px;
                -moz-border-radius    : 10px 10px 10px 10px; 
                -webkit-border-radius : 10px 10px 10px 10px; 
                border-radius         : 10px 10px 10px 10px;
                text-align            : center;
                font-weight           : bold;
                font-size             : 30px;
                line-height           : 45px;
                float                 : right;
                margin-right          : 50px;
            }

            .botonesPOC1{
                width                 : 180px;
                height                : 45px;
                color                 : #FFFFFF;
                background-color      : #000000;
                -moz-border-radius    : 10px 10px 10px 10px; 
                -webkit-border-radius : 10px 10px 10px 10px; 
                border-radius         : 10px 10px 10px 10px;
                text-align            : center;
                font-weight           : bold;
                font-size             : 16px;
                line-height           : 45px;
                float                 : right;
                margin-right          : 50px;
            }
            
            .botonesPOC2{
                width                 : 100px;
                height                : 45px;
                -moz-border-radius    : 10px 10px 10px 10px; 
                -webkit-border-radius : 10px 10px 10px 10px; 
                border-radius         : 10px 10px 10px 10px;
                text-align            : center;
                font-weight           : bold;
                font-size             : 16px;
                line-height           : 45px;
                margin                : 0 auto;
            }

            .ovalo {
                width                 : 65px;
                height                : 45px;
                -moz-border-radius    : 50%;
                -webkit-border-radius : 50%;
                border-radius         : 50%;
                background-color      : #000000;
                color                 : #FFFFFF;
                border                : 3px solid #FFFFFF;
                float                 : left;
                margin-top            : -2px;
                margin-left           : 10px; 
                text-align            : center;
                line-height           : 45px;
                font-size             : 16px;
                font-weight           : bold;
            }
            
            .botones_m2{
                border-radius : 10px;
                font-size     : 15px;
                font-weight   : bold;
            }
            
            .fondo{
                position:fixed;
                top:0;
                right:0;
                bottom:0;
                left:0;
                z-index:1500;
                background-color:#000;
                filter:alpha(opacity=0.7);
                opacity:0.7;
                display:none;
            }
            
            .img{
                position:absolute;
                top:50%;
                left:50%;
                width:50px;
                height:50px;
                margin-top:-25px;
                margin-left:-25px;
            }
        </style>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="../plugin-alert/js/alert.min.js"></script>
        <script type="text/javascript" src="../js/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="../js/tableHeadFixer.js"></script>
    </head>
    <body>	
        <div class="fondo" id="fondo">
            <img src="loading.gif" alt="Loading" class="img">
            <p style="text-align: center;">Espere mientras se procesa la información</p>
        </div>
        <header id="cabecera" style="position:fixed; z-index:1200">
            <img src="../imagenes/inicio/logo3.png" alt="Logo LA" style=" width:300px;float:left; margin:10px;">
            <div style="float:left; margin:25px;">
                <div style="font-size:20px; color:#fff; font-weight:bold; margin-left:20px; margin-right:10px; float:left;">Efectivo</div>
                <div style="font-size:20px; color:#fff; font-weight:bold; margin-left:10px; margin-right:10px; float:left;">Bajo costo</div>
                <div style="font-size:20px; color:#fff; font-weight:bold; margin-left:10px; margin-right:10px; float:left;">Conveniente</div>
            </div>
        </header>

        <section id="contenedor">
            <div style="overflow:hidden; margin:0 auto; margin:20px; margin-right:10px; width:95%; margin-top:100px;">
                <div style="float:left; width:25%; overflow:hidden; position:relative;">
                    <div style="position:fixed">
                        <div class="botonesPOC boton1">POC</div>
                        
                        <div style="position:relative; clear:both; top:5px;">
                            <div class="ovalo" style="position:absolute;">1</div>
                            <div class="botonesPOC1">Despliegue</div>
                        </div>
                        
                        <div style="position:relative; clear:both; top:14px;">
                            <div class="ovalo" style="position:absolute;">2</div>
                            <div class="botonesPOC1">Licensing Webtool</div>
                        </div>
                        <!--<div style="position:relative; clear:both; top:24px;">
                            <iframe width="280" height="200" src="https://www.youtube.com/embed/bpWWx06Z0F0" frameborder="0" allowfullscreen></iframe>
                        </div>-->
                        <video width="280" height="200" controls style="position:relative; clear:both; top:24px;">
                            <source src="POCDEMO.mp4" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <div style="position:relative; top:24px;">
                            <iframe width="280" height="200" src="https://www.youtube.com/embed/bpWWx06Z0F0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div style="width:70%; margin:0; margin-left:23%; padding:0px; float:left;">
                    <div style="float:left; width:100%; overflow:hidden;  min-height:500px;">
                        <div style="width:100%;">
                            <div style="display:inline-block; width:33%; text-align: center">
                                <div class="botonesPOC2 boton2 pointer" id="btnDesuso">Desuso</div>
                                <br>
                                <div style="width:100px; height:50px; margin: 0 auto; overflow:hidden;">
                                    <img src="../imagenes/Microsoft_Logo1.png" style="width:70px; height:100%; visibility:hidden;">
                                </div>
                                
                                <p style="font-weight: bold; line-height: 30px;">Muestra de instalaciones en desuso</p>
                                <p style="font-weight: bold; line-height: 30px;">Tiempo: 15 minutos</p>
                            </div>
                            <div style="display:inline-block; width:33%; text-align: center;">
                                <div class="botonesPOC2 boton1 pointer" id="btnPOCSAP">POC</div>
                                <br>
                                <div style="width:100px; height:50px; margin: 0 auto; overflow:hidden;">
                                    <img src="../imagenes/SAP_Logo.png" style="width:auto; height:100%;">
                                </div>
                                <p style="font-weight: bold; line-height: 30px;">Demo de Webtool</p>
                                <p style="font-weight: bold; line-height: 30px;">Tiempo: 45 minutos</p>
                            </div>
                            <div style="display:inline-block; width:33%; text-align: center;">
                                <div class="botonesPOC2 boton1 pointer" id="btnPOC">POC</div>
                                <br>
                                <div style="width:100px; height:50px; margin:0 auto; overflow:hidden;">
                                    <img src="../imagenes/Microsoft_Logo1.png" style="width:auto; height:100%;">
                                </div>
                                <p style="font-weight: bold; line-height: 30px;">Demo de Webtool</p>
                                <p style="font-weight: bold; line-height: 30px;">Tiempo: 45 minutos</p>
                            </div>
                        </div>
                        <br>
                        
                        
                        <div id="contenedor_ver3" style=" display:block;width:100%; margin:0px; padding:0px;background:#D9D9D9; min-height:500px; overflow:hidden;">
                            <div id="contenedor_ver4" style=" margin:0 auto; margin-top:20px; margin-bottom:20px; border-radius:8px; background:#F2F2F2; overflow:hidden;min-height:500px; width:95%;">
                                <div style="width:calc(100%-50px); padding:10px 30px 10px 30px; overflow:hidden;" id="contenedorCentral">
                                    <?php
                                        require_once("plantillas/POC.php");
                                        require_once("plantillas/desuso.php");
                                        require_once("plantillas/POCSAP.php");
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        require_once("../plantillas/pie.php");
        ?>
        
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script>
            var ipV4 = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}$/;
            function fileOnload(e) {
                result=e.target.result;
            }
            
            function showhide(tabla){
                if($("#" + tabla).is(":visible")){
                    $("#" + tabla).hide();
                }
                else{
                    $("#" + tabla).show();
                }
            }
        </script>
    </body>
</html>