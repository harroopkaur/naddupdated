<div style="width:100%; height:auto; overflow:hidden; display:none;" id="contPOC">
    <form id="form1" name="form1" method="post" enctype="multipart/form-data">
        <div style="text-align:center; font-size:25px; font-weight:bold;">Licensing Webtool Prueba de Concepto</div>
        <br>
        <div id="despliegueSegmentado">
            <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="../webtool/<?= $general->ToolLocal ?>"><?= $general->ToolLocal ?></a></p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Configurar el rango de máximo 150 IPs para ejecutar la herramienta</p>
            <p style="float:left">Rango de Ips, Desde:&nbsp;</p><input type="text" id="desdeIp" name="desdeIp" style="float:left"><p style="float:left">&nbsp;- Hasta:&nbsp;</p><input type="text" id="hastaIp" name="hastaIp" style="float:left"><input name="actualizIp" type="button" id="actualizIp" value="Generar Archivo" class="botones_m5" style="float:left"><br><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>En el paso anterior se generar&aacute; un archivo llamado Equipos.zip, descomprimir el mismo y guardarlo en la misma carpeta en la que se encuentran los componentes de la herramienta reemplazando el archivo original Equipos.txt por el nuevo.</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.1 -&nbsp;</span>Confirmar que el archivo nuevo Equipos.txt fue reemplazado correspondiente abriendo el mismo y confirmar que se encuentra el rango de IP s citado y no se encuentra solamente la máquina "Localhost"</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Se iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>" haciendo click en el bot&oacute;n <strong style="font-weight:bold">Buscar</strong></p>
            <br>
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <input name="opcionDespliegue" id="opcionSegmentado" type="hidden" value="segmentado" >
            <div class="contenedor_archivo" style="width:800px;">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_fileLAD_Output" disabled id="url_fileLAD_Output" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="LAD_Output" id="LAD_Output" accept=".rar">
                    <input type="button" class="botones_m5 boton1" id="buscarLAD_Output" value="Buscar">
                </div>
            </div>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo</p>

            <div class="exito_archivo" id="exito_archivo" style="display:none;">&nbsp;&nbsp;Archivo cargado con &eacute;xito</div>
            <div class="error_archivo" id="error_archivo" style="display:none;">&nbsp;&nbsp;&nbsp;Debe ser un archivo con extension rar</div>
            <br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">10.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>" haciendo click en el bot&oacute;n <strong style="font-weight:bold">Buscar</strong></p><br />
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <input name="opcionDespliegue" id="opcionSegmentadoFile" type="hidden" value="segmentado" >
            <div class="contenedor_archivo" style="width:800px;">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_fileLAE_Output" disabled id="url_fileLAE_Output" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="LAE_Output" id="LAE_Output" accept=".csv">
                    <input type="button" class="botones_m5 boton1" value="Buscar">
                </div>
            </div>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo</p>
            <div id="cargando2" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">11.-&nbsp;</span>Una vez identificados ambos archivos con el bot&oacute;n buscar, llenar los datos a continuaci&oacute;n y dar click en el bot&oacute;n <strong style="font-weight:bold;">Enviar</strong>.</p><br />

            <p style="float:left;">Nombre:&nbsp;</p><input type="text" id="nombre" name="nombre" maxlength="70" style="float:left">
            <p style="float:left; margin-left:10px;">Email:&nbsp;</p><input type="email" id="email" name="email" maxlength="70" style="float:left">
            <div style="float:right;"><div class="boton1 botones_m2" id="boton1" onclick="enviarArchivos()">Enviar</div></div>
        </div>
    </form>
</div>
                             
<script>
    var validEmail = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    $(document).ready(function(){
        $("#btnPOC").click(function(){
            $("#btnPOC").removeClass("boton1").addClass("boton2");
            $("#contPOC").show();

            $("#btnDesuso").removeClass("boton2").addClass("boton1");
            $("#contDesuso").hide();
            
            $("#btnPOCSAP").removeClass("boton2").addClass("boton1");
            $("#contPOCSAP").hide();

            $("#LAD_Output").val("");
            $("#LAD_Output").replaceWith($("#LAD_Output").clone(true));
            $("#url_fileLAD_Output").val("Nombre del Archivo...");

            $("#LAE_Output").val("");
            $("#LAE_Output").replaceWith($("#LAE_Output").clone(true));
            $("#url_fileLAE_Output").val("Nombre del Archivo...");

            $("#container1").empty();
            $("#container2").empty();
            $("#ttabla1").empty();
            $("#grafica").hide();
        });

        $("#LAD_Output").change(function(e){
            if(addArchivo(e) === false){
                $("#LAD_Output").val("");
                $("#LAD_Output").replaceWith($("#LAD_Output").clone(true));
                $("#url_fileLAD_Output").val("Nombre del Archivo...");
            }
        }); 

        $("#LAE_Output").change(function(e){
            if(addArchivo1(e) === false){
                $("#LAE_Output").val("");
                $("#LAE_Output").replaceWith($("#LAE_Output").clone(true));
                $("#url_fileLAE_Output").val("Nombre del Archivo...");
            }
        }); 

        $("#actualizIp").click(function(){
            if(ipV4.test($("#desdeIp").val()) === false){
                $.alert.open('warning', "Alerta", "No es una dirección ipV4", {'Aceptar' : 'Aceptar'}, function(){
                    $("#desdeIp").val("");
                    $("#desdeIp").focus();
                });
                return false;
            }

            if(ipV4.test($("#hastaIp").val()) === false){
                $.alert.open('warning', "Alerta", "No es una dirección ipV4", {'Aceptar' : 'Aceptar'}, function(){
                    $("#hastaIp").val("");
                    $("#hastaIp").focus();
                });
                return false;
            }

            ip1 = $("#desdeIp").val();
            arrayIp1 = ip1.split(".");
            ip2 = $("#hastaIp").val();
            arrayIp2 = ip2.split(".");
            if((arrayIp1[0]+arrayIp1[1]+arrayIp1[2] !== arrayIp2[0]+arrayIp2[1]+arrayIp2[2]) 
            || ((arrayIp2[3] - arrayIp1[3]) > 150)){
                $.alert.open('warning', "Alerta", "Limite 150 IPs", {'Aceptar' : 'Aceptar'}, function(){
                    $("#hastaIp").focus();
                });
                return false;
            }

            $.post("generarArchivo.php", { desdeIp : $("#desdeIp").val(), hastaIp : $("#hastaIp").val() }, function(data){
                if(data[0].result === 0){
                    $.alert.open('warning', "Alerta", "El rango de ip no es compatibles", {'Aceptar' : 'Aceptar'});
                }
                else{
                    window.open(data[0].archivo);
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'});
            });
        });
    });

    function enviarArchivos(){        
        if($("#LAD_Output").val() === ""){
            $.alert.open('warning', "Alerta", "Debe seleccionar el archivo LAD_Output.rar", {'Aceptar' : 'Aceptar'});
            return false;
        }

        if($("#LAE_Output").val() === ""){
            $.alert.open('warning', "Alerta", "Debe seleccionar el archivo LAE_Output.csv", {'Aceptar' : 'Aceptar'});
            return false;
        }

        if($("#nombre").val() === ""){
            $.alert.open('warning', "Alerta", "Debe llenar el nombre", {'Aceptar' : 'Aceptar'}, function(){
                $("#nombre").focus();
            });
            return false;
        }
        if($("#email").val() === ""){
            $.alert.open('warning', "Alerta", "Debe llenar el email", {'Aceptar' : 'Aceptar'}, function(){
                $("#email").focus();
            });
            return false;
        }
        if(validEmail.test($("#email").val()) === false){
            $.alert.open('warning', 'Alert', "Debe colocar un email válido", {"Aceptar" : "Aceptar"}, function(){
                $("#email").val("");
                $("#email").focus();
            });
            return false;
        }
        $("#fondo").show();
        var formData = new FormData($("#form1")[0]);	
        $.ajax({
                type: "POST",
                url: "enviarArchivos.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    result = data[0].result;
                    if(result === 0){
                        $.alert.open('warning', "Alerta", "No se pudo enviar los archivos por favor inténtelo de nuevo", {'Aceptar' : 'Aceptar'});
                    }
                    else if(result === 1){
                        $.alert.open('info', "Muchas Gracias por enviar su información, un representante de Licensing Assurance le estará contactando para completar la prueba de concepto", {'Aceptar' : 'Aceptar'});
                    }
                    else if(result === 2){
                        $.alert.open('warning', "Alerta", "No se pudo cargar el archivo .rar por favor inténtelo de nuevo", {'Aceptar' : 'Aceptar'});
                    }
                    else if(result === 3){
                        $.alert.open('warning', "Alerta", "No se pudo cargar el archivo .csv por favor inténtelo de nuevo", {'Aceptar' : 'Aceptar'});
                    }
                    $("#fondo").hide();
                }
        })
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'});
        });
    }

    function addArchivo(e){
        file        = e.target.files[0]; 
        nombre      = file.name;
        arrayNombre = nombre.split("."); 
        if(arrayNombre.pop() !== "rar"){
            $.alert.open('warning', "Alerta", "El archivo a cargar debe ser .rar", {'Aceptar' : 'Aceptar'});
            return false;
        }
        else{
            if(parseInt(file.size) / 1024 > 5120 ){
                $.alert.open('warning', "Alerta", "El tamaño permitido es de 5 MB", {'Aceptar' : 'Aceptar'});
                return false;
            }
            else{
                reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
                $("#url_fileLAD_Output").val(file.name);
                return true;
            }
        }  
    }

    function addArchivo1(e){
        file        = e.target.files[0];
        nombre      = file.name;
        arrayNombre = nombre.split("."); 
        if(arrayNombre.pop() !== "csv"){
            $.alert.open('warning', "Alerta", "El archivo a cargar debe ser .csv", {'Aceptar' : 'Aceptar'});
            return false;
        }
        else{
            if(parseInt(file.size) / 1024 > 5120 ){
                $.alert.open('warning', "Alerta", "El tamaño permitido es de 5 MB", {'Aceptar' : 'Aceptar'});
                return false;
            }
            else{
                reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
                $("#url_fileLAE_Output").val(file.name);
                return true;
            }
        }  
    }
</script>