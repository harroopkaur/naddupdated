<div style="width:100%; height:auto; overflow:hidden;" id="contDesuso">
    <form id="formDesuso" name="formDesuso" method="post" enctype="multipart/form-data">
        <div style="text-align:center; font-size:25px; font-weight:bold;">Muestra de Instalaciones en desuso</div>
        <br>
        <div id="desuso">
            <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="../webtool/<?= $general->ToolLocal ?>"><?= $general->ToolLocal ?></a></p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Configurar el rango de máximo 40 IPs para ejecutar la herramienta</p>
            <p style="float:left">Rango de Ips, Desde:&nbsp;</p><input type="text" id="desdeDesusoIp" name="desdeIp" style="float:left"><p style="float:left">&nbsp;- Hasta:&nbsp;</p><input type="text" id="hastaDesusoIp" name="hastaIp" style="float:left"><input name="actualizIpDesuso" type="button" id="actualizIpDesuso" value="Generar Archivo" class="botones_m5" style="float:left"><br><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>En el paso anterior se generar&aacute; un archivo llamado Equipos.zip, descomprimir el mismo y guardarlo en la misma carpeta en la que se encuentran los componentes de la herramienta reemplazando el archivo original Equipos.txt por el nuevo.</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.1 -&nbsp;</span>Confirmar que el archivo nuevo Equipos.txt fue reemplazado correspondiente abriendo el mismo y confirmar que se encuentra el rango de IP s citado y no se encuentra solamente la máquina "Localhost"</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Se iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>" haciendo click en el bot&oacute;n <strong style="font-weight:bold">Buscar</strong></p>
            <br>

            <div class="contenedor_archivo" style="width:800px;">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_fileLAD_Output" disabled id="url_fileLAD_OutputDesuso" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="LAD_Output" id="LAD_OutputDesuso" accept=".rar">
                    <input type="button" class="botones_m5 boton1" id="buscarLAD_OutputDesuso" value="Buscar">
                </div>
            </div>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo</p>

            <div class="exito_archivo" id="exito_archivo" style="display:none;">&nbsp;&nbsp;Archivo cargado con &eacute;xito</div>
            <div class="error_archivo" id="error_archivo" style="display:none;">&nbsp;&nbsp;&nbsp;Debe ser un archivo con extension rar</div>
            <br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">10.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>" haciendo click en el bot&oacute;n <strong style="font-weight:bold">Buscar</strong></p><br />
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <input name="opcionDespliegue" id="opcionSegmentadoFile" type="hidden" value="segmentado" >
            <div class="contenedor_archivo" style="width:800px;">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_fileLAE_Output" disabled id="url_fileLAE_OutputDesuso" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="LAE_Output" id="LAE_OutputDesuso" accept=".csv">
                    <input type="button" class="botones_m5 boton1" id="buscarLAE_OutputDesuso" value="Buscar">
                </div>
            </div>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo</p>
            <div id="cargando2" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">11.-&nbsp;</span>Una vez identificados ambos archivos con el botón buscar, llenar los datos a continuación y dar click en el botón Ver Reporte. También puede omitir el paso 11 y solo dar click en el botón <strong style="font-weight:bold;">Ver Reporte</strong>.</p><br />

            <p style="float:left;">Nombre:&nbsp;</p><input type="text" id="nombreDesuso" name="nombre" maxlength="70" style="float:left">
            <p style="float:left; margin-left:10px;">Email:&nbsp;</p><input type="email" id="emailDesuso" name="email" maxlength="70" style="float:left">
            <div style="float:right;"><div class="boton1 botones_m2" onclick="enviarArchivosDesuso()">Ver Reporte</div></div>
        </div>
    </form>
</div>

<div style="width:100%; height:auto; overflow:hidden; display:none;" id="grafica">
    <div id="container1" style="height:400px; width:45%; max-width: 45%; margin:10px; float:left;"></div>
    <div id="container2" style="height:400px; width:45%; max-width: 45%; margin:10px; float:left;"></div>
    <div  style=" width:45%; max-width: 45%; margin:10px; float:left; text-align:center;"><a onclick="showhide('ttabla1')" style="cursor:pointer;" >Ver Detalles</a></div>
    <div  style=" width:45%; max-width: 45%; margin:10px; float:left; text-align:center;"><a onclick="showhide('ttabla1')" style="cursor:pointer;" >Ver Detalles</a></div>
    <div id="ttabla1"  style="display:none; width:95%; margin:10px; float:left;"></div>
</div>
                               
<script>
    $(document).ready(function(){
        $("#btnDesuso").click(function(){
            $("#btnPOC").removeClass("boton2").addClass("boton1");
            $("#contPOC").hide();

            $("#btnDesuso").removeClass("boton1").addClass("boton2");
            $("#contDesuso").show();
            
            $("#btnPOCSAP").removeClass("boton2").addClass("boton1");
            $("#contPOCSAP").hide();

            $("#LAD_OutputDesuso").val("");
            $("#LAD_OutputDesuso").replaceWith($("#LAD_OutputDesuso").clone(true));
            $("#url_fileLAD_OutputDesuso").val("Nombre del Archivo...");

            $("#LAE_OutputDesuso").val("");
            $("#LAE_OutputDesuso").replaceWith($("#LAE_OutputDesuso").clone(true));
            $("#url_fileLAE_OutputDesuso").val("Nombre del Archivo...");

            $("#container1").empty();
            $("#container2").empty();
            $("#ttabla1").empty();
            $("#grafica").hide();
        });

        $("#LAD_OutputDesuso").change(function(e){
            if(addArchivoDesuso(e) === false){
                $("#LAD_OutputDesuso").val("");
                $("#LAD_OutputDesuso").replaceWith($("#LAD_OutputDesuso").clone(true));
                $("#url_fileLAD_OutputDesuso").val("Nombre del Archivo...");
            }
        }); 

        $("#LAE_OutputDesuso").change(function(e){
            if(addArchivoDesuso1(e) === false){
                $("#LAE_OutputDesuso").val("");
                $("#LAE_OutputDesuso").replaceWith($("#LAE_OutputDesuso").clone(true));
                $("#url_fileLAE_OutputDesuso").val("Nombre del Archivo...");
            }
        }); 

        $("#actualizIpDesuso").click(function(){
            if(ipV4.test($("#desdeDesusoIp").val()) === false){
                alert("No es una dirección ipV4");
                $("#desdeDesusoIp").val("");
                $("#desdeDesusoIp").focus();
                return false;
            }

            if(ipV4.test($("#hastaDesusoIp").val()) === false){
                alert("No es una dirección ipV4");
                $("#hastaDesusoIp").val("");
                $("#hastaDesusoIp").focus();
                return false;
            }

            ip1 = $("#desdeDesusoIp").val();
            arrayIp1 = ip1.split(".");
            ip2 = $("#hastaDesusoIp").val();
            arrayIp2 = ip2.split(".");
            if((arrayIp1[0]+arrayIp1[1]+arrayIp1[2] !== arrayIp2[0]+arrayIp2[1]+arrayIp2[2]) 
            || ((arrayIp2[3] - arrayIp1[3]) > 150)){
                alert("Limite 150 IPs");
                $("#hastaDesusoIp").focus();
                return false;
            }

            $.post("generarArchivo.php", { desdeIp : $("#desdeDesusoIp").val(), hastaIp : $("#hastaDesusoIp").val() }, function(data){
                if(data[0].result === 0){
                    alert("el rango de ip no es compatibles");
                }
                else{
                    window.open(data[0].archivo);
                }
            }, "json")
            .fail(function(){
                alert("Ocurrió un error");
            });
        });
    });

    function enviarArchivosDesuso(){
        if($("#LAD_OutputDesuso").val() === ""){
            alert("Debe seleccionar el archivo LAD_Output.rar");
            return false;
        }

        if($("#LAE_OutputDesuso").val() === ""){
            alert("Debe seleccionar el archivo LAE_Output.csv");
            return false;
        }

        $("#fondo").show();
        var formData = new FormData($("#formDesuso")[0]);	
        $.ajax({
                type: "POST",
                url: "desuso/index.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    result = data[0].result;
                    if(result === 0){
                        alert("No se pudo procesar la información");
                    }
                    else if(result === 1){
                        $("#ttabla1").empty();
                        $("#contDesuso").hide();
                        $("#ttabla1").append(data[0].tabla);
                        $("#grafica").show();

                        $(function () {
                            // Create the chart
                            $('#container1').highcharts({
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: 'Clientes'
                                },
                                subtitle: {
                                    text: ''
                                },
                                credits: {
                                    enabled: false
                                },
                                xAxis: {
                                    type: 'category'
                                },
                                yAxis: {
                                    title: {
                                        text: 'Nro. Equipos'
                                    }

                                },
                                legend: {
                                    enabled: false
                                },
                                plotOptions: {
                                    series: {
                                        borderWidth: 0,
                                        dataLabels: {
                                            enabled: true,
                                            format: '{point.y:.0f}'
                                        }
                                    }
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                    pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b>' + (data[0].total_1 + data[0].total_2 + data[0].total_3) + '</b><br/>'
                                },
                                series: [{
                                    name: 'Clientes',
                                    colorByPoint: true,
                                    data: [
                                    {
                                        name: 'Probablemente en Uso',
                                        y: Math.round(data[0].total_2),
                                        color: '<?= $color2 ?>'
                                    },
                                    {
                                        name: 'Obsoleto',
                                        y: Math.round(data[0].total_3),
                                        color: '<?= $color3 ?>'
                                    }]
                                }]
                            });
                        });

                        $(function () {
                            // Create the chart
                            $('#container2').highcharts({
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: 'Servidores'
                                },
                                subtitle: {
                                    text: ''
                                },
                                credits: {
                                    enabled: false
                                },
                                xAxis: {
                                    type: 'category'
                                },
                                yAxis: {
                                    title: {
                                        text: 'Nro. Equipos'
                                    }

                                },
                                legend: {
                                    enabled: false
                                },
                                plotOptions: {
                                    series: {
                                        borderWidth: 0,
                                        dataLabels: {
                                            enabled: true,
                                            format: '{point.y:.0f}'
                                        }
                                    }
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                    pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b>' + (data[0].total_4 + data[0].total_5 + data[0].total_6) + '</b><br/>'
                                },
                                series: [{
                                    name: 'Servidores',
                                    colorByPoint: true,
                                    data: [
                                    {
                                        name: 'Probablemente en Uso',
                                        y: Math.round(data[0].total_5),
                                        color: '<?= $color2 ?>'
                                    },
                                    {
                                        name: 'Obsoleto',
                                        y: Math.round(data[0].total_6),
                                        color: '<?= $color3 ?>'
                                    }]
                                }],
                            });
                        });
                    }

                    $("#fondo").hide();
                }
        })
        .fail(function(){
            alert("Hubo un error");
            $("#fondo").hide();
        });
    }

    function addArchivoDesuso(e){
        file        = e.target.files[0]; 
        nombre      = file.name;
        arrayNombre = nombre.split("."); 
        if(arrayNombre.pop() !== "rar"){
            alert("El archivo a cargar debe ser .rar");
            return false;
        }
        else{
            if(parseInt(file.size) / 1024 > 5120 ){
                alert("El tamaño permitido es de 5 MB");
                return false;
            }
            else{
                reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
                $("#url_fileLAD_OutputDesuso").val(file.name);
                return true;
            }
        }  
    }

    function addArchivoDesuso1(e){
        file        = e.target.files[0];
        nombre      = file.name;
        arrayNombre = nombre.split("."); 
        if(arrayNombre.pop() !== "csv"){
            alert("El archivo a cargar debe ser .csv");
            return false;
        }
        else{
            if(parseInt(file.size) / 1024 > 5120 ){
                alert("El tamaño permitido es de 5 MB");
                return false;
            }
            else{
                reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
                $("#url_fileLAE_OutputDesuso").val(file.name);
                return true;
            }
        }  
    }
</script>