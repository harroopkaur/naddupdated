<input type="hidden" id="urlInput">
<div style="width:100%; height:auto; overflow:hidden; display:none;" id="contPOCSAP">
    <form id="formSAP" name="formSAP" method="post" enctype="multipart/form-data">
        <div style="text-align:center; font-size:25px; font-weight:bold;">Licensing Webtool Prueba de Concepto (SAP)</div>
        <br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Versiones</span></p>									
        <p>Release Producto</p>
        <p>4.6 SAP R/3</p>
        <p>4.7 SAP R/3</p>
        <p>5.0 ECC SAP ECC 5.0</p>
        <p>6.0 ECC SAP ECC 6.0</p>
        <br>

        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Requisitos</span></p>									
        <p>Acceso al servidor desarrollo del Sistema SAP ECC y un usuario con licencia para ejecutar la transacci&oacute;n ZLICSAP. El SAP GUI debe permitir descargar archivos a la PC del usuario. En los reportes ALV se debe permitir la integraci&oacute;n con MS Excel (2007 a 2013).</p><br />

        <fieldset class="fieldset">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Primera Parte	Descarga Herramienta</span></p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Ejecutar en la l&iacute;nea de comandos la transacci&oacute;n SE38 y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"> <span style="font-weight:bold; margin-left:25px;">"Favoritos"</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Ingresamos el nombre del programa ZSISTDE, Seleccionamos <span style="font-weight:bold;">"codigo fuente"</span> y pulsamos <span style="font-weight:bold;">"Crear"</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Descargar C&oacute;digo fuente.  <a class="link1" href="<?=$GLOBALS['domain_root']?>/ZSISTDET.txt" target="_blank">ZSISTDET.txt</a></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Reemplazamos todo por el codigo fuente</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>Guardar <img src="<?=$GLOBALS['domain_root']?>/imagenes/guardar.png" style="width:20px; margin-top:2px; position:absolute;"><span style="margin-left:25px; margin-right:5px;">y Generar </span><img src="<?=$GLOBALS['domain_root']?>/imagenes/generar1.png" style="width:22px; position:absolute;"></p><br>
        </fieldset>
        
        <br>
        
        <fieldset class="fieldset">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Segunda Parte	Componentes de Software Instalados</span></p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Componentes de Software Instalado"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Hacer click en el icono.  Elegir la opci&oacute;n <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversi&oacute;n"</span>. El sistema crear&aacute; el archivo con el nombre <span style="font-weight:bold;">"Output_1_texto Componentes Sistema.txt"</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>CARGAR EL RESULTADO</p><br>
        </fieldset>
            
        <br />

        <fieldset class="fieldset">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Tercera Parte	Usuarios por m&oacute;dulo</span></p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Usuarios por m&oacute;dulo"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Para generar el output del archivo damos click en el icono.  Elegir <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversion"</span>.</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Hacer click al icono crear y el fichero ser&aacute; guardado con el nombre <span style="font-weight:bold;">"Output_1_texto_Usuarios x Modulo.txt"</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>CARGAR EL RESULTADO</p><br>
        </fieldset>
        
        <br />

        <fieldset class="fieldset">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Cuarta Parte Usabilidad de Usuarios</span></p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1.-&nbsp;</span>Se digita en la línea de comandos la transacción ZLICSAP y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/enter.png" style="width:25px; margin-top:-2px; position:absolute;"><span style="font-weight:bold; margin-left:25px;">(Enter)</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2.-&nbsp;</span>Elegir la opcion: <span style="font-weight:bold;">"Aging Usuarios"</span> y luego pulsamos <img src="<?=$GLOBALS['domain_root']?>/imagenes/ejecutar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:28px;">(Ejecutar)</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3.-&nbsp;</span>Para generar el output del archivo damos click en el icono.  Elegir <img src="<?=$GLOBALS['domain_root']?>/imagenes/generar.png" style="width:25px; position:absolute;"><span style="font-weight:bold; margin-left:30px;">"S/Conversion"</span>.</p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4.-&nbsp;</span>Hacer click al icono crear y el fichero ser&aacute; guardado con el nombre <span style="font-weight:bold;">"Output_1_texto Aging Usuarios.txt"</span></p><br>
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 5.-&nbsp;</span>CARGAR EL RESULTADO</p><br>
        </fieldset>
        
        <br />
        
        <fieldset class="fieldset">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Quinta Parte Subir los resultados</span></p><br />
            
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 1. Subir el archivo </span><span class="bold">"Output_1_texto Componentes Sistema.txt"</span></p><br />
            <div class="contenedor_archivo">
                <div class="input_archivo" style="width:80%;">
                    <input type="text" class="url_file" name="url_fileComponentes" disabled id="url_fileComponentes" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivoComponentes" id="archivoComponentes" accept='.txt'>
                    <input type="button" id="buscarArchivoComponentes" class="botones_m5 boton1" value="Buscar">
                </div>
            </div>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo</p>
            
            <br>
            
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 2. Subir el archivo </span><span class="bold">"Output_1_texto_Usuarios x Modulo.txt"</span></p><br />
            <div class="contenedor_archivo">
                <div class="input_archivo" style="width:80%;">
                    <input type="text" class="url_file" name="url_fileUsuarios" disabled id="url_fileUsuarios" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivoUsuarios" id="archivoUsuarios" accept='.txt'>
                    <input type="button" id="buscarArchivoUsuarios" class="botones_m5 boton1" value="Buscar">
                </div>
            </div>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo</p>
            
            <br>
        
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 3. Subir el archivo </span><span class="bold">"Output_1_texto Aging Usuarios.txt"</span></p><br />
            <div class="contenedor_archivo">
                <div class="input_archivo" style="width:80%;">
                    <input type="text" class="url_file" name="url_fileAging" disabled id="url_fileAging" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivoAging" id="archivoAging" accept=".txt"> 
                    <input type="button" id="buscarArchivoAging" class="botones_m5 boton1" value="Buscar">
                </div>
            </div>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo</p>
            
            <br>
            
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Paso 4. </span>Una vez cargados los archivos, llenar los datos a continuación y dar click en el botón enviar..</p><br />

            <p style="float:left;">Nombre:&nbsp;</p><input type="text" id="nombreSAP" name="nombre" maxlength="70" style="float:left">
            <p style="float:left; margin-left:10px;">Email:&nbsp;</p><input type="email" id="emailSAP" name="email" maxlength="70" style="float:left">
            <div style="float:right;"><div class="boton1 botones_m2" onclick="enviarArchivosSAP()">Enviar</div></div>
        </fieldset>
    </form>
</div>

<script>
    $(document).ready(function(){
       $("#btnPOCSAP").click(function(){
           $("#btnPOC").removeClass("boton2").addClass("boton1");
            $("#contPOC").hide();

            $("#btnDesuso").removeClass("boton2").addClass("boton1");
            $("#contDesuso").hide();
            
            $("#btnPOCSAP").removeClass("boton1").addClass("boton2");
            $("#contPOCSAP").show();
            
            $("#archivoComponentes").val("");
            $("#archivoComponentes").replaceWith($("#archivoComponentes").clone(true));
            $("#url_fileComponentes").val("Nombre del Archivo...");

            $("#archivoUsuarios").val("");
            $("#archivoUsuarios").replaceWith($("#archivoUsuarios").clone(true));
            $("#url_fileUsuarios").val("Nombre del Archivo...");
            
            $("#archivoAging").val("");
            $("#archivoAging").replaceWith($("#archivoAging").clone(true));
            $("#url_fileAging").val("Nombre del Archivo...");

            $("#container1").empty();
            $("#container2").empty();
            $("#ttabla1").empty();
            $("#grafica").hide();
       });
       
       $("#archivoComponentes").change(function(e){
            $("#urlInput").val("url_fileComponentes");
            if(addArchivo2(e) === false){
                $("#archivoComponentes").val("");
                $("#archivoComponentes").replaceWith($("#archivoComponentes").clone(true));
                $("#url_fileComponentes").val("Nombre del Archivo...");
            }
        }); 

        $("#archivoUsuarios").change(function(e){
            $("#urlInput").val("url_fileUsuarios");
            if(addArchivo2(e) === false){
                $("#archivoUsuarios").val("");
                $("#archivoUsuarios").replaceWith($("#archivoUsuarios").clone(true));
                $("#url_fileUsuarios").val("Nombre del Archivo...");
            }
        }); 

        $("#archivoAging").change(function(e){
            $("#urlInput").val("url_fileAging");
            if(addArchivo2(e) === false){
                $("#archivoAging").val("");
                $("#archivoAging").replaceWith($("#archivoAging").clone(true));
                $("#url_fileAging").val("Nombre del Archivo...");
            }
        }); 
    });
    
    function enviarArchivosSAP(){        
        if($("#archivoComponentes").val() === ""){
            $.alert.open('warning', "Alerta", "Debe seleccionar el archivo Output_1_texto Componentes Sistema.txt", {'Aceptar' : 'Aceptar'});
            return false;
        }

        if($("#archivoUsuarios").val() === ""){
            $.alert.open('warning', "Alerta", "Debe seleccionar el archivo Output_1_texto_Usuarios x Modulo.txt", {'Aceptar' : 'Aceptar'});
            return false;
        }
        
        if($("#archivoAging").val() === ""){
            $.alert.open('warning', "Alerta", "Debe seleccionar el archivo Output_1_texto Aging Usuarios.txt", {'Aceptar' : 'Aceptar'});
            return false;
        }

        if($("#nombreSAP").val() === ""){
            $.alert.open('warning', "Alerta", "Debe llenar el nombre", {'Aceptar' : 'Aceptar'}, function(){
                $("#nombreSAP").focus();
            });
            return false;
        }
        if($("#emailSAP").val() === ""){
            $.alert.open('warning', "Alerta", "Debe llenar el email", {'Aceptar' : 'Aceptar'}, function(){
                $("#emailSAP").focus();
            });
            return false;
        }
        if(validEmail.test($("#emailSAP").val()) === false){
            $.alert.open('warning', 'Alert', "Debe llenar un email válido", {"Aceptar" : "Aceptar"}, function(){
                $("#emailSAP").val("");
                $("#emailSAP").focus();
            });
            return false;
        }
        $("#fondo").show();
        var formData = new FormData($("#formSAP")[0]);	
        $.ajax({
            type: "POST",
            url: "enviarArchivosSAP.php", 
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",  
            cache:false,
            success: function(data){
                result = data[0].result;
                if(result === 0){
                    $.alert.open('warning', "Alerta", "No se pudo enviar los archivos por favor inténtelo de nuevo", {'Aceptar' : 'Aceptar'});
                } else if(result === 1){
                    $.alert.open('info', "Muchas Gracias por enviar su información, un representante de Licensing Assurance le estará contactando para completar la prueba de concepto", {'Aceptar' : 'Aceptar'});
                }  else if(result === 2){
                    $.alert.open('warning', "Alerta", "No se pudo cargar el archivo Output_1_texto Componentes Sistema.txt por favor inténtelo de nuevo", {'Aceptar' : 'Aceptar'});
                } else if(result === 3){
                    $.alert.open('warning', "Alerta", "No se pudo cargar el archivo Output_1_texto_Usuarios x Modulo.txt por favor inténtelo de nuevo", {'Aceptar' : 'Aceptar'});
                } else if(result === 4){
                    $.alert.open('warning', "Alerta", "No se pudo cargar el archivo Output_1_texto Aging Usuarios.txt por favor inténtelo de nuevo", {'Aceptar' : 'Aceptar'});
                } 
                $("#fondo").hide();
            }
        })
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'});
        });
    }
    
    function addArchivo2(e){
        file        = e.target.files[0];
        nombre      = file.name;
        arrayNombre = nombre.split("."); 
        if(arrayNombre.pop() !== "txt"){
            $.alert.open('warning', "Alerta", "El archivo a cargar debe ser .txt", {'Aceptar' : 'Aceptar'});
            return false;
        }
        else{
            if(parseInt(file.size) / 1024 > 5120 ){
                $.alert.open('warning', "Alerta", "El tamaño permitido es de 5 MB", {'Aceptar' : 'Aceptar'});
                return false;
            }
            else{
                reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
                $("#" + $("#urlInput").val()).val(file.name);
                return true;
            }
        }  
    }
</script>