<?php 
require_once("../configuracion/conexionPDO1.php");
$array = "";
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    $result = 0;
    $nombre = $_POST["nombre"];
    $email  = $_POST["email"];
    $fecha  = date("d-m-Y H-i-s");
    
    if(!is_uploaded_file($_FILES["LAD_Output"]["tmp_name"]) ){	
        $result = 2;
    }
    else{
        $imagen = $_FILES["LAD_Output"]["name"]; 
        move_uploaded_file($_FILES['LAD_Output']['tmp_name'], "archivosPOC/" . $email . $fecha . "LAD_output.rar"); 
    }
    
    if(!is_uploaded_file($_FILES["LAE_Output"]["tmp_name"]) ){	
        $result = 3;
    }
    else{
        $imagen = $_FILES["LAE_Output"]["name"]; 
        move_uploaded_file($_FILES['LAE_Output']['tmp_name'], "archivosPOC/" . $email . $fecha . "LAE_output.csv"); 
    }
    /*if(!move_uploaded_file($_FILES["LAD_Output"]["tmp_name"], "")){
        
    }
    if(!move_uploaded_file($_FILES["LAE_Output"]["tmp_name"], "LAE_output.csv")){
        $result = 3;
    }*/
    
    
    if($result == 0){
        $cliente_email = "Dimitri@licensingassurance.com";
        $copia_email   = "<m_acero_n@hotmail.com>,<paola@licensingassurance.com>";
        $mensaje       = "Nombre: " . $nombre . "   Email: " . $email . " envió información por la WebTool POC";
        //$num = md5(time());

        //MAIL BODY
        /*$body = "
        <html>
        <head>
        <title>WebTool POC</title>
        </head>
        <body style='background:#EEE; padding:30px;'>
        <h2 style='color:#767676;'>WebTool POC</h2>";

        $body .= "
        <strong style='color:#0090C6;'>Mensaje: </strong>
        <span style='color:#767676;'>" . $message . "</span><br><br>";

        $body .= "</body></html>";

        $_name=$_FILES["LAD_Output"]["name"];
        $_type=$_FILES["LAD_Output"]["type"];
        $_size=$_FILES["LAD_Output"]["size"];
        $_temp=$_FILES["LAD_Output"]["tmp_name"];

        $fp   = fopen($_temp, "rb");
        $file = fread($fp, $_size);
        $file = chunk_split(base64_encode($file)); 
        $eol  = PHP_EOL;

        // MULTI-HEADERS Content-Type: multipart/mixed and Boundary is mandatory.
        $headers = "From:<" . $cliente_email . ">".$eol;
        $headers = "cc:<colososhark@gmail.com>".$eol;
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/mixed; "; 
        $headers .= "boundary=".$num.$eol;
        $mensaje = "--".$num.$eol; 

        // HTML HEADERS 
        $mensaje .= "Content-Type: text/html; charset=UTF-8";
        $mensaje .= "Content-Transfer-Encoding: 8bit".$eol;
        $mensaje .= "".$body.$eol;
        $mensaje .= "--".$num.$eol; 

        // FILES HEADERS 
        $mensaje .= "Content-Type:application/octet-stream "; 
        $mensaje .= "name=\"".$_name."\"".$eol;
        $mensaje .= "Content-Transfer-Encoding: base64".$eol;
        $mensaje .= "Content-Disposition: attachment; ";
        $mensaje .= "filename=\"".$_name."\"".$eol;
        $mensaje .= "".$file.$eol;
        $mensaje .= "--".$num."--";

        // SEND MAIL
        /*if(mail($cliente_email, "WebTool POC", $mensaje, $headers)){*/
            
            //MAIL BODY
            /*$body = "
            <html>
            <head>
            <title>WebTool POC LAE_output.csv</title>
            </head>
            <body style='background:#EEE; padding:30px;'>
            <h2 style='color:#767676;'>WebTool POC</h2>";

            $body .= "
            <strong style='color:#0090C6;'>Mensaje: </strong>
            <span style='color:#767676;'>" . $message . "</span><br><br>";

            $body .= "</body></html>";

            $_name=$_FILES["LAE_Output"]["name"];
            $_type=$_FILES["LAE_Output"]["type"];
            $_size=$_FILES["LAE_Output"]["size"];
            $_temp=$_FILES["LAE_Output"]["tmp_name"];

            $fp   = fopen($_temp, "rb");
            $file = fread($fp, $_size);
            $file = chunk_split(base64_encode($file)); 
            $eol  = PHP_EOL;

            // MULTI-HEADERS Content-Type: multipart/mixed and Boundary is mandatory.
            $headers = "From:<" . $cliente_email . ">".$eol;
            $headers = "cc:<colososhark@gmail.com>".$eol;
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: multipart/mixed; "; 
            $headers .= "boundary=".$num.$eol;
            $mensaje = "--".$num.$eol; 

            // HTML HEADERS 
            $mensaje .= "Content-Type: text/html; charset=UTF-8";
            $mensaje .= "Content-Transfer-Encoding: 8bit".$eol;
            $mensaje .= "".$body.$eol;
            $mensaje .= "--".$num.$eol; 

            // FILES HEADERS 
            $mensaje .= "Content-Type:application/octet-stream "; 
            $mensaje .= "name=\"".$_name."\"".$eol;
            $mensaje .= "Content-Transfer-Encoding: base64".$eol;
            $mensaje .= "Content-Disposition: attachment; ";
            $mensaje .= "filename=\"".$_name."\"".$eol;
            $mensaje .= "".$file.$eol;
            $mensaje .= "--".$num."--";
            
            // SEND MAIL*/
            /*if(mail($cliente_email, "WebTool POC", $mensaje, $headers)){
                $result = 1;
            }*/
        //}    
        
        $asunto = "WebTool POC"; 
        $eol  = PHP_EOL;
        $desde  = "From:<" . $cliente_email . ">".$eol;
        $desde .= "cc:" . $copia_email;
        if(mail($cliente_email,$asunto,$mensaje,$desde)){
            try{
                $sql = $conn->prepare('INSERT INTO poc (nombre, fecha) VALUES (:nombre, NOW())');
                $sql->execute(array('nombre'=>$email . $fecha . "LAD_output.rar"));
                
                $sql = $conn->prepare('INSERT INTO poc (nombre, fecha) VALUES (:nombre, NOW())');
                $sql->execute(array('nombre'=>$email . $fecha . "LAE_output.csv"));
            }catch(PDOException $e){
                return $e->getMessage();
            }
            $result = 1;
        } 
    }
    $array = array(0=>array('result'=>$result));
}
echo json_encode($array);