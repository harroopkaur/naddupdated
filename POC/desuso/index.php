<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/POC/clases/clase_detalles_equipo.php");
require_once($GLOBALS["app_root1"] . "/POC/clases/clase_filepcs.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_conversion.php");
require_once($GLOBALS["app_root1"] . "/POC/clases/clase_escaneo.php");
require_once($GLOBALS["app_root1"] . "/POC/clases/pruebaDesuso.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");

$array = array(0=>array('mensaje'=>'No tiene permiso para acceder'));
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    $detalles   = new detallesDesuso();
    $filepcs    = new filepcsDesuso();
    $conversion = new TablaC();
    $general    = new General();
    $escaneo    = new escaneoDesuso();
    $cabecera   = new pruebaDesuso();
    $config     = new configuraciones();

    $total_1 = 0;
    $total_2 = 0;
    $total_3 = 0;
    $total_4 = 0;
    $total_5 = 0;
    $total_6 = 0;
    $tclient = 0;
    $tserver = 0;
    $activosc = 0;
    $activoss = 0;
    
    $result = 0;

    $listaEdiciones = $config->listar_edicionTotal();
    $listaVersiones = $config->listar_versionTotal();
    $nombre = "";
    if(isset($_POST["nombre"])){
        $nombre = $general->get_escape($nombre);
    }

    $email = "";
    if(isset($_POST["email"])){
        $email = $general->get_escape($email);
    }

    $error = 0;
    if(!$cabecera->insertar($nombre, $email)){
        $error = -1;
    }
    else{
        $idPrueba = $cabecera->ultId();
    }

    if($error == 0){
        if(!file_exists("archivosDesuso/" . $idPrueba . "/")){
            mkdir("archivosDesuso/" . $idPrueba, 0777, true);
        }


        if(is_uploaded_file($_FILES["LAD_Output"]["tmp_name"])){	
            $nombreLAD = $_FILES['LAD_Output']['name'];
            if($nombreLAD!=""){
                $extension = explode(".",$nombreLAD);  // Obtener tipo de archivo
                $long = count($extension) - 1;
                if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
                // Permitir subir solo imagenes JPG,
            }else{
                $error=2;	
            }
        }
        else{
            $error = 3;
        }
    }

    if($error == 0){
        if(is_uploaded_file($_FILES["LAE_Output"]["tmp_name"])){	
            $nombreLAE = $_FILES['LAE_Output']['name'];

            if($nombreLAE!=""){
            $extension = explode(".",$nombreLAE);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "csv") && ($extension[$long] != "CSV")) { $error = 4; }  
                // Permitir subir solo imagenes JPG,
            }else{
                $error = 5;	
            }
        }
    }
    else{
        $error = 6;
    }

    if($error == 0){
        $rar_file = rar_open($_FILES['LAD_Output']['tmp_name'], 'InfoLA2015**') or die("Can't open Rar archive");

        $entries = rar_list($rar_file);

        foreach ($entries as $entry) {
            $entry->extract('archivosDesuso/' . $idPrueba . '/');
        }

        rar_close($rar_file);

        $files = glob("archivosDesuso/" . $idPrueba . "/*"); // obtiene todos los archivos        
        foreach($files as $file => $valor){
            if(is_file($valor) && $valor != "archivosDesuso/" . $idPrueba . "/Resultados_Escaneo.csv"){
                unlink($valor);
            }
        } 

        $nombre = "LAE_Output.csv";
        move_uploaded_file($_FILES["LAE_Output"]["tmp_name"], "archivosDesuso/" . $idPrueba . "/" . $nombre);

        if (($fichero = fopen("archivosDesuso/" . $idPrueba . "/" . $nombre, "r")) !== FALSE) {
            $i = 1;
            
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                if($i == 1 && ($datos[0] != "DN" || $datos[1] != "objectClass" || $datos[2] != "cn" || $datos[3] != "userAccountControl" 
                || $datos[4] != "lastLogon" || $datos[5] != "pwdLastSet" || $datos[6] != "operatingSystem" || $datos[7] != "operatingSystemVersion")){
                    $error = 7;
                    break;
                }
                if ($i > 1) {
                    $sistema = $datos[6];

                    if ($conversion->codigo_existe($datos[6], 0)) {
                        $conversion->datos2($datos[6]);
                        $sistema = $conversion->os;
                    }

                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:idPrueba" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                        . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                        . ":os" . $j . ", :lastlogontimes" . $j . ")";
                    } else {
                        $bloque .= ", (:idPrueba" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                        . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                        . ":os" . $j . ", :lastlogontimes" . $j . ")";
                    } 

                    $bloqueValores[":idPrueba" . $j] = $idPrueba;
                    $bloqueValores[":dn" . $j] = $general->truncarString($datos[0], 250);
                    $bloqueValores[":objectclass" . $j] = $general->truncarString($datos[1], 250);
                    $bloqueValores[":cn" . $j] = $general->truncarString($datos[2], 250);
                    $bloqueValores[":useracountcontrol" . $j] = $general->truncarString($datos[3], 250);
                    $bloqueValores[":lastlogon" . $j] = $general->truncarString($datos[4], 250);
                    $bloqueValores[":pwdlastset" . $j] = $general->truncarString($datos[5], 250);
                    $bloqueValores[":os" . $j] = $general->truncarString($sistema, 250);
                    $bloqueValores[":lastlogontimes" . $j] = $general->truncarString($datos[8], 250);
                    /*if (!$filepcs->insertar($idPrueba, $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], $sistema, $datos[7])) {
                        echo $filepcs->error;
                    }*/
                    
                    if($j == $general->registrosBloque){
                        if(!$filepcs->insertarEnBloque($bloque, $bloqueValores)){ 
                            //echo "LAE: " . $filec->error;
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }
                $i++;
                $exito = 1;
            }
            
            if($insertarBloque === true){
                if(!$filepcs->insertarEnBloque($bloque, $bloqueValores)){
                    //echo "LAE: " . $filec->error;
                }
            }
        }

        //inicio actualizar filepcs
        $lista_todos_files = $filepcs->listar_todo($idPrueba);

        if ($lista_todos_files) {
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
                
            foreach ($lista_todos_files as $reg_f) {

                $host = explode('.', $reg_f["cn"]);

                $sistema = $reg_f["os"];

                if ($reg_f["lastlogon"] != "") {

                    $value = round(($reg_f["lastlogon"] - 116444735995904000) / 10000000);

                    $fecha1 = date('d/m/Y', $value);

                    $dias1 = $general->daysDiff($value, strtotime('now'));
                } else {
                    $dias1 = NULL;
                }

                if ($reg_f["pwdlastset"] != "") {

                    $value2 = round(($reg_f["pwdlastset"] - 116444735995904000) / 10000000);

                    $fecha2 = date('d/m/Y', $value2);

                    $dias2 = $general->daysDiff($value2, strtotime('now'));
                } else {
                    $dias2 = NULL;
                }
                if ($reg_f["lastlogontimes"] != "") {

                    $value3 = round(($reg_f["lastlogontimes"] - 116444735995904000) / 10000000);

                    $fecha3 = date('d/m/Y', $value3);

                    $dias3 = $general->daysDiff($value3, strtotime('now'));
                } else {
                    $dias3 = NULL;
                }

                $minimos = $general->minimo($dias1, $dias2, $dias3);

                $minimor = round(abs($minimos), -1);

                if ($minimor <= 30) {
                    $minimo = 1;
                } else if ($minimor <= 60) {
                    $minimo = 2;
                } else if ($minimor <= 90) {
                    $minimo = 3;
                } else if ($minimor <= 365) {
                    $minimo = 4;
                } else {
                    $minimo = 5;
                }

                if ($minimo < 4) {
                    $activo = 1;
                } else {
                    $activo = 0;
                }

                $tipos = $general->search_server($reg_f["os"]);

                $tipo = $tipos[0];
                $familia = "";
                if(strpos($sistema, "Windows Server") !== false || (strpos($sistema, "Windows") !== false && strpos($sistema, "Server") !== false)){
                    $familia = "Windows Server";
                }
                else if(strpos($sistema, "Windows") !== false){
                    $familia = "Windows"; 
                }

                $edicion = "";
                foreach($listaEdiciones as $rowEdiciones){
                    if(trim($rowEdiciones["nombre"]) != "" && strpos($sistema, trim($rowEdiciones["nombre"])) !== false && strlen(trim($rowEdiciones["nombre"])) > strlen($edicion)){
                         $edicion = trim($rowEdiciones["nombre"]);    
                    }
                }
                if($edicion == "" || ($familia == "Windows Server" && $edicion == "Server") || $edicion == "Windows 2000 Server"){
                    $edicion = "Standard";
                }

                $version = "";
                foreach($listaVersiones as $rowVersiones){
                    if(trim($rowVersiones["nombre"]) != "" && strpos($sistema, trim($rowVersiones["nombre"])) !== false && strlen(trim($rowVersiones["nombre"])) > strlen($version)){
                         $version = trim($rowVersiones["nombre"]);    
                    }
                } 

                if($familia == ""){
                    $edicion = "";
                    $version = "";
                }

                /*if ($detalles->insertar($idPrueba, addslashes($host[0]), $sistema, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimor, $activo, $tipo, $minimo)) {
                    $exito = 1;
                } else {
                    echo $detalles->error;
                }*/
                
                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:idPrueba" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                    . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                    . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
                } else {
                    $bloque .= ", (:idPrueba" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                    . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                    . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
                } 

                $bloqueValores[":idPrueba" . $j] = $idPrueba;
                $bloqueValores[":equipo" . $j] = $host[0];
                $bloqueValores[":os" . $j] = $sistema;
                $bloqueValores[":familia" . $j] = $familia;
                $bloqueValores[":edicion" . $j] = $edicion;
                $bloqueValores[":version" . $j] = $version;
                $bloqueValores[":dias1" . $j] = $dias1;
                $bloqueValores[":dias2" . $j] = $dias2;
                $bloqueValores[":dias3" . $j] = $dias3;
                $bloqueValores[":minimo" . $j] = $minimor;
                $bloqueValores[":activo" . $j] = $activo;
                $bloqueValores[":tipo" . $j] = $tipo;
                $bloqueValores[":rango" . $j] = $minimo;

                if($j == $general->registrosBloque){
                    if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){ 
                        //echo "Detalles: " . $detalles->error;
                    }
                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
            }//foreach
        }//if
        
        if($insertarBloque === true){
            if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){   
                //echo "Detalles: " . $detalles->error;
            }
        }
        //fin actualizar filepcs
        //
        //inicio escaneo
        $imagen = $idPrueba . "/Resultados_Escaneo.csv";

        if (($fichero = fopen("archivosDesuso/" . $imagen, "r")) !== FALSE) {
            $i = 1;
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                if ($i != 1) {
                    $host = explode('.', $datos[0]);
                    
                    /*if (!$escaneo->insertar($idPrueba, addslashes($host[0]), $datos[1], $datos[2])) {

                    }*/
                    if($j == 0){
                    $insertarBloque = true;
                        $bloque .= "(:idPrueba" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                    } else {
                        $bloque .= ", (:idPrueba" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                    } 

                    $bloqueValores[":idPrueba" . $j] = $idPrueba;
                    $bloqueValores[":equipo" . $j] = $host[0];
                    $bloqueValores[":status" . $j] = $datos[1];
                    $bloqueValores[":errors" . $j] = $datos[2];

                    if($j == $general->registrosBloque){
                        if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){ 
                            //echo "Escaneo: " . $escaneo->error;
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }
                $i++;
                $exito1 = 1;
            }
            
            if($insertarBloque === true){
                if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){   
                    //echo "Escaneo: " . $escaneo->error;
                }
            }
        }

        //inicio actualizar escaneo
        $lista_equipos_scaneados = $escaneo->listar_todo2($idPrueba);
        if ($lista_equipos_scaneados) {
            foreach ($lista_equipos_scaneados as $reg_e) {
                $detalles->actualizar($idPrueba, addslashes($reg_e["equipo"]), $reg_e["errors"]);
            }
        }
        //fin actualizar escaneo
    }
    
    $listar_equipos = array();
    if($error == 0){
        $listar_equipos = $detalles->listar_todo($idPrueba);
        if ($listar_equipos) {
            foreach ($listar_equipos as $reg_equipos) {

                if ($reg_equipos["tipo"] == 1) {
                    $tclient = ($tclient + 1);

                    if ($reg_equipos["rango"] == 1) {
                        $total_1 = ($total_1 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activosc = ($activosc + 1);
                        }
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_2 = ($total_2 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activosc = ($activosc + 1);
                        }
                    } else {
                        $total_3 = ($total_3 + 1);
                    }
                } else {//server
                    $tserver = ($tserver + 1);

                    if ($reg_equipos["rango"] == 1) {
                        $total_4 = ($total_4 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activoss = ($activoss + 1);
                        }
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_5 = ($total_5 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activoss = ($activoss + 1);
                        }
                    } else {
                        $total_6 = ($total_6 + 1);
                    }
                }
            }  
        }
        
        $tabla = '<table id="tablaDesuso" style="width:95%;" class="tablap">
                    <tr style="background:#333; color:#fff;">
                        <th align="center" valign="middle">Usabilidad</th>
                        <th align="center"  valign="middle">Clientes</th>
                        <th align="center" valign="middle">%</th>
                        <th align="center" valign="middle">Servidores</th>
                        <th align="center" valign="middle">%</th>
                    </tr>
                    <tr>
                        <td align="center" valign="middle">Probablemente en Uso</td>
                        <td align="center" valign="middle">' . $total_2 . '</td>
                        <td align="center" valign="middle">';

                            $porct2 = 0;
                            if($tclient > 0){
                                $porct2 = ($total_2 / $tclient) * 100;
                            }
                            $tabla .= round($porct2) . '

                        </td>
                        <td align="center" valign="middle">' . $total_5 . '</td>
                        <td align="center" valign="middle">';

                            $porct22 = 0;
                            if($tserver > 0){
                                $porct22 = ($total_5 / $tserver) * 100;
                            }
                            $tabla .= round($porct22) . '

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle">Obsoleto</td>
                        <td align="center" valign="middle">' . $total_3 . '</td>
                        <td align="center" valign="middle">';

                            $porct3 = 0;
                            if($tclient > 0){
                                $porct3 = ($total_3 / $tclient) * 100;
                            }
                            $tabla .= round($porct3) . '

                        </td>
                        <td align="center" valign="middle">' . $total_6 . '</td>
                        <td align="center" valign="middle">';

                            $porct33 = 0;
                            if($tserver > 0){
                                $porct33 = ($total_6 / $tserver) * 100;
                            }
                            $tabla .= round($porct33) . '

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle"><strong style="font-weight:bold;">Gran total</strong></td>
                        <td align="center" valign="middle"><strong style="font-weight:bold;">' . $tclient . '</strong></td>
                        <td align="center" valign="middle"></td>
                        <td align="center" valign="middle"><strong style="font-weight:bold;">' . $tserver . '</strong></td>
                        <td align="center" valign="middle"></td>
                    </tr>
            </table>
            
            <div style="overflow-x:auto; overflow-y:hidden; height:400px;">
                <table class="tablap" id="tablaAlcance" style="width:95%; margin-top:-5px; overflow-y:hidden; overflow-x:auto;" >
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th  align="center" valign="middle">&nbsp;</th>
                            <th  align="center" valign="middle">Nombre Equipo</th>
                            <th  align="center" valign="middle">Tipo</th>
                            <th  align="center" valign="middle">Usabilidad</th>
                            <th  align="center" valign="middle">Escaneado</th>
                        </tr>
                        </thead>
                        <tbody>';

                        $i = 1;
                        foreach ($listar_equipos as $reg_equipos2) {
                            if($reg_equipos2["rango"] != 1){
                                $tabla .= '<tr>
                                    <td align="center">' . $i . '</td>
                                    <td >' . $reg_equipos2["equipo"] . '</td>
                                    <td align="center">';

                                        if ($reg_equipos2["tipo"] == 1) {
                                            $tabla .= 'Cliente';
                                        } else {
                                            $tabla .= 'Servidor';
                                        }

                                $tabla .= '</td>
                                    <td align="center">';
                                        if ($reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {                                                               
                                            $tabla .= 'Probablemente en uso';
                                        } else {
                                            $tabla .= 'Obsoleto';
                                        }

                                $tabla .= '</td>
                                    <td align="center">';

                                        if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3){
                                            $tabla .= 'Si';
                                        }
                                        else{
                                            $tabla .= 'No';
                                        }

                                $tabla .= '</td>
                                </tr>';

                                $i++;   
                            }            
                        }
                        $tabla .= '</tbody>
                    </table>
                </div>
                <script>
                    $("#tablaAlcance").tablesorter();
                    $("#tablaAlcance").tableHeadFixer();
                </script>';
        $result = 1;
    }
    $array = array(0=>array('error'=>$error, 'result'=> $result, 'tabla'=>$tabla, 'total_1'=>$total_1, 'total_2'=>$total_2, 'total_3'=>$total_3,
    'total_4'=>$total_4, 'total_5'=>$total_5, 'total_6'=>$total_6));
}
echo json_encode($array);