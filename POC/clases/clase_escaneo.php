<?php
class escaneoDesuso extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $equipo;
    var $status;
    var $errors;
    var $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($idPrueba, $equipo, $status, $errors) {
        $query = "INSERT INTO escaneo_equiposDesuso (idPrueba, equipo, status, errors) ";
        $query .= "VALUES (:idPrueba, :equipo, :status, :errors)";
        try {
            $this->conexion();   
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idPrueba'=>$idPrueba, ':equipo'=>$equipo, ':status'=>$status, ':errors'=>$errors));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO escaneo_equiposDesuso (idPrueba, equipo, status, errors) ";
        $query .= "VALUES " . $bloque;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_todo2($idPrueba) {
        $query = "SELECT * FROM escaneo_equiposDesuso WHERE idPrueba = :idPrueba GROUP BY equipo";
        try {
            $this->conexion();   
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idPrueba'=>$idPrueba));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
}