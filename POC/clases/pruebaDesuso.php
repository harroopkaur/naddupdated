<?php
class pruebaDesuso extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($nombre, $email) {
        $query = "INSERT INTO pruebaDesuso (nombre, email, fecha) ";
        $query .= "VALUES (:nombre, :email, NOW())";
        try {
            $this->conexion();   
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':nombre'=>$nombre, ':email'=>$email));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function ultId(){
        $query = "SELECT MAX(id) AS id FROM pruebaDesuso";
        try {
            $this->conexion();   
            $sql = $this->conn->prepare($query);        
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return 0;
        } 
    }
}