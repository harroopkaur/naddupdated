<?php
class detallesDesuso extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($idPrueba, $equipo, $os, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try{
            $this->conexion();   
            $sql = $this->conn->prepare("INSERT INTO detalles_equipoDesuso (idPrueba, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES (:idPrueba, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)");
            $sql->execute(array(':idPrueba'=>$idPrueba, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo, 
            ':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO detalles_equipoDesuso (idPrueba, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) ";
        $query .= "VALUES " . $bloque;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Actualizar
    function actualizar($idPrueba, $equipo, $errors) {
        try{
            $this->conexion();   
            $sql = $this->conn->prepare("UPDATE detalles_equipoDesuso SET errors = :errors "
            . "WHERE equipo = :equipo AND idPrueba = :idPrueba");
            $sql->execute(array(':equipo'=>$equipo, ':idPrueba'=>$idPrueba, ':errors'=>$errors));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_todo($idPrueba) {
        try{
            $this->conexion();   
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipoDesuso WHERE idPrueba = :idPrueba AND id != 0 AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':idPrueba'=>$idPrueba));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}