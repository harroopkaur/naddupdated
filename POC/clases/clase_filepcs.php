<?php
class filepcsDesuso extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($idPrueba, $dn, $objectclass, $cn, $useracountcontrol, $lastlogon, $pwdlastset, $os, $lastlogontimes) {
        $query = "INSERT INTO filepcsDesuso (idPrueba, dn, objectclass, cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) ";
        $query .= "VALUES (:idPrueba, :dn, :objectclass, :cn, :useracountcontrol, :lastlogon, :pwdlastset, :os, :lastlogontimes)";
        try {
            $this->conexion();   
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':idPrueba'=>$idPrueba, ':dn'=>$dn, ':objectclass'=>$objectclass, ':cn'=>$cn, ':useracountcontrol'=>$useracountcontrol, 
            ':lastlogon'=>$lastlogon, ':pwdlastset'=>$pwdlastset, ':os'=>$os,':lastlogontimes'=>$lastlogontimes));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO filepcsDesuso (idPrueba, dn, objectclass, cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) ";
        $query .= "VALUES " . $bloque;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
   
    // Obtener listado de todos los Usuarios
    function listar_todo($idPrueba) {
        $query = "SELECT * FROM filepcsDesuso WHERE idPrueba = :idPrueba ORDER BY id";
        try { 
            $this->conexion();   
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':idPrueba'=>$idPrueba));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }
}