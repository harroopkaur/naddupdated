<?php
require_once("../../webtool/configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/portal/clase/clase_portal.php");

$portal = new portal();

$tabla = "";
$array = array(0=>array('listado'=>$tabla));
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    $tipoUsuario = "";
    if(isset($_POST["tipoUsuario"])){
        $tipoUsuario = $_POST["tipoUsuario"];
    }
    
    $tipoDocumento = "";
    if(isset($_POST["tipoDocumento"])){
        $tipoDocumento = $_POST["tipoDocumento"];
    }
    
    $para = "";
    if(isset($_POST["para"])){
        $para = $_POST["para"];
    }
    
    $theme = "";
    if(isset($_POST["theme"])){
        $theme = $_POST["theme"];
    }
    
    $manufacturer = "";
    if(isset($_POST["manufacturer"])){
        $manufacturer = $_POST["manufacturer"];
    }
    
    $listado = $portal->listar($tipoUsuario, $tipoDocumento, $para, $theme, $manufacturer);
    foreach($listado as $row){
        $tabla .= '<tr>
                        <th>' . $row["tipoUsuario"] . '</th>
                        <th>' . $row["tipoDocumento"] . '</th>
                        <td>' . $row["para"] . '</td>
                        <td>' . $row["theme"] . '</td>
                        <td>' . $row["manufacturer"] . '</td>
                        <td>';
                            if($row["spanish"] == ""){
                                $tabla .= '<img src="img/SAMasaService.png" class="img img-responsive">';
                            } else{
                                $tabla .= '<a href="' . $row["spanish"] . '" target="_blank"><img src="img/' . $row["imgSpanish"] . '" class="img img-responsive center-block" style="width:auto; height:50px;"></a>';
                            }
                        $tabla .= '</td>
                        <td>';
                            if($row["english"] == ""){
                                $tabla .= '<img src="img/SAMasaService.png" class="img img-responsive">';
                            } else{
                                $tabla .= '<a href="' . $row["english"] . '" target="_blank"><img src="img/' . $row["imgEnglish"] . '" class="img img-responsive center-block" style="width:auto; height:50px;"></a>';
                            }
                        $tabla .= '</td>
                    </tr>';
    }
    
    $array = array(0=>array('listado'=>$tabla));
}  
echo json_encode($array);