<?php
class portal extends General{ 
    //private $llavePortal = "5/98==79*brjFRH5l/03=f09x0e911*42";
    private $llavePortal = "lksjd$%=isub3&*+*--iek27492jsdl*3";
    
    function listar($tipoUsuario, $tipoDocumento, $para, $theme, $manufacturer) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM portal
            WHERE tipoUsuario LIKE :tipoUsuario AND tipoDocumento LIKE :tipoDocumento AND para LIKE :para AND theme LIKE :theme
            AND manufacturer LIKE :manufacturer
            ORDER BY tipoUsuario, tipoDocumento, para, theme, manufacturer');
            $sql->execute(array(':tipoUsuario'=>'%' . $tipoUsuario . '%', ':tipoDocumento'=>'%' . $tipoDocumento . '%', 
            ':para'=>'%' . $para . '%', ':theme'=>'%' . $theme . '%', ':manufacturer'=>'%' . $manufacturer . '%'));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    public function obtener_tokenPortal() {
        $header = [
                'typ'=>'JWT', 
                'alg'=>'HS256'
            ];
        $header = json_encode($header);
        $header = base64_encode($header);   
        
        $playload = [
            'iss'      =>'https://www.licensingassurance.com/portal', 
            'fecha'    => date("d/m/Y H:i:s")
        ];
                
        $playload = json_encode($playload);
        $playload = base64_encode($playload);  
        
        $signature = hash_hmac('sha256', $header.".".$playload, $this->llavePortal, true);
        $signature = base64_encode($signature);

        return $header.".".$playload.".".$signature; //$this->llave;
    }
}