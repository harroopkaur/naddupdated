<div class="container">
    <h1 class="text-center tituloLogin">Welcome to the Licensing Assurance<br> Information Portal</h1>

    <div class="col-sm-2 col-sm-offset-2">
        <img src="<?= $GLOBALS["domain_root1"] ?>/img/circulo_2.png" alt="SAM as a Service" class="img img-responsive">
    </div>

    <div class="well col-sm-2 col-sm-offset-1 sombreado divLogin">
        <p class="tamText text-center"><strong>Code</strong></p>
        <div class="row">
            <div class="col-xs-12 bordeLicensing">
                <div class="row">
                    <input type="password" id="code" name="code" maxlength="20" style="width:100%;">
                    <script>$("#code").focus();</script>
                </div>
            </div>
        </div>

        <div class="row">&nbsp;</div>

        <div class="row">
            <div class="col-md-5 centradoPorcentualHorizontal">
                <div class="row">
                    <form id='form1' name='form1' method='post' action='list.php'>
                        <input type='hidden' id="token" name="token" value='<?= $token ?>'>
                        <div id="enviar" class="boton">Log In</div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-2 col-sm-offset-1">
        <img src="<?= $GLOBALS["domain_root1"] ?>/img/circulo_3.png" alt="SAM for YOU" class="img img-responsive">
    </div>

    <div class="col-sm-6 col-sm-offset-3">
        <img src="img/information.png" class="img img-responsive" style="margin: 0 auto;">
    </div>   
</div>
    
<script>
    $(document).ready(function(){
        $("#enviar").click(function(){
            $.post("ajax/verificar.php", { code: $("#code").val() }, function (data) {
                if(data[0].result === true){
                    $("#form1").submit();
                } else{
                    $.alert.open('warning', 'Warning', "Code does not match", {'Ok' : 'Ok'}, function(){
                        $("#code").val("");
                    });
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
            });
        });
    });
</script>