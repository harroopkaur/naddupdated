<!DOCTYPE html>
<html lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
        <meta name="description" content="">
        <meta name="author" content="">

        <title>.:Licensing Assurance:.</title>
        <link rel="shortcut icon" href="../img/Logo.ico">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link href="../webtool/plugin-alert/css/alert.min.css" rel="stylesheet" />
        <link href="../webtool/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        
        <style>
            body.fondo{
                background-image:url(img/fondoLicensing1.jpg);
                background-size: 100%;
                color: #000000;
            }
            
            body.fondo1{
                background-image:url(img/fondoLicensing_opt.jpg);
                background-size: 100%;
                color: #000000;
            }
            
            html{
                width:100%;
                height:auto;
            }
            
            .colorGris{
                background: #a6a6a6;
            }
            
            .colorAzul{
                background: #00b0f0;
            }
            
            .tituloLogin{
                margin-top:-80px;
            }
            
            .sombreado{
                -webkit-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
            }
            
            .tamText{
                font-size: 18px;
            }
            
            .divLogin{
                padding: 30px;
                overflow:hidden;
                height: 190px;
            }
            
            .bordeLicensing{
                border: 3px solid #0C3C60;
            }
            
            .boton{
                background-color: #00B0F0;
                font-size: 18px;
                font-weight: bold;
                width:100%;
                height:40px;
                line-height: 40px;
                text-align: center;
                border-radius: 10px 10px 10px 10px;
                -moz-border-radius: 10px 10px 10px 10px;
                -webkit-border-radius: 10px 10px 10px 10px;
                -webkit-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                cursor: pointer;
            }
            
            .centradoPorcentualHorizontal{
                position: absolute;
                left: 50%;
                transform: translate(-50%, 0%);
                -webkit-transform: translate(-50%, 0%);
            }
            
            .tablap {     
                font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
                /*font-size: 12px;*/ 
                margin: 0 auto;   
                width: 100%; 
                text-align: left;    
                border-collapse: collapse; 
                margin-top: 4vw;
            }

            .tablap th {       
                font-weight: normal;     
                padding: 1vw;     
                background: #0C3C60;
                /*border-top: 0.4vw solid #aabcfe;    
                border-bottom: 0.1vw solid #fff;*/ 
                color: #ffffff; 
                vertical-align: middle;
                font-size:1.32vw;
            }

            .tablap td {    
                padding: 1vw;     
                background: #e8edff;     
                border-bottom: 1px solid #fff;
                color: #669;    
                border-top: 1px solid transparent; 
                vertical-align: middle;
                font-size:1.32vw;
            }

            .tablap tr:hover td { 
                background: #d0dafd; 
                color: #339; 
            }

            .tablap tbody td.nuevoProducto{
                background-color:#CC0000;
                color: #ffffff;
            }

            .tablap tbody td.productoNoExiste{
                background-color:#000000;
                color: #ffffff;
            }
            
            .nav-pills > li > a{
                color: #ffffff;
                background-color: #a6a6a6;
                border-radius: 10px 10px 10px 10px;
                -moz-border-radius: 10px 10px 10px 10px;
                -webkit-border-radius: 10px 10px 10px 10px;
                -webkit-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
            }
            
            .nav-pills > li > a:hover,
            .nav-pills > li.active > a,
            .nav-pills > li.active > a:hover{
                color: #ffffff;
                background-color: #00b0f0;
            }
            
            .foot{
                position: absolute;
                bottom: 0;
            }
            
            .btn{
                color: #ffffff;
                background-color: #a6a6a6;
                border-radius: 10px 10px 10px 10px;
                -moz-border-radius: 10px 10px 10px 10px;
                -webkit-border-radius: 10px 10px 10px 10px;
                -webkit-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
                box-shadow: 5px 5px 25px 0px rgba(0,0,0,0.75);
            }
            
            .btn:hover,
            .btn.active{
                color: #ffffff;
                background-color: #00b0f0;
            }
            
            .imgExit{
                margin-top:-11.5vw;
                z-index: 1032;
            }
            
            .imgExit > img{
                margin: 0 auto; 
                width:3vw; 
                height:auto; 
                cursor:pointer;
                position:fixed;
            }
            
            .menu-flotante{
                position: fixed;
                z-index: 1030;
                margin: 0;
                width: 94.5%;
                overflow:hidden;
            }
            
            .container{
                width:90%;
                margin-top:21vw;
            }  
            
            .contPrincipal{
                margin-top:-2.5vw;
            }
            
            .botonesMenu{
                display: inline-block;
                width: 12vw;
                height: 2.5vw;
                padding:0;
                margin-left: 0.8vw;
                line-height: 2.5vw;
                font-size: 1vw;
            }
            
            .botonesMenu1{
                display: inline-block;
                width: 12vw;
                height: 2.5vw;
                padding:0;
                margin-left: -1.1vw;
                line-height: 2.5vw;
                font-size: 1vw;
            }
        </style>
        
        <!-- include font awesome -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        
        
        
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script src="../webtool/plugin-alert/js/alert.js"></script>
    </head>
   
    <body class="<?php if($pag == "index"){ echo "fondo"; } else{ echo "fondo1"; }?>">
        <div class="container-fluid">
            <img src="<?php if($pag == "index"){ echo 'img/imgCabLicensing1.png';} else{ echo 'img/imgCabLicensing.png'; } ?>" class="img" style="width:100%; height:auto; top:0; z-index:1030; position: fixed;">    