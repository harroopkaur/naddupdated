</div>

<div class="container">
    <div class="col-sm-1 pull-right imgExit" id="salir"><img src="img/salir.png"></div>
    
    <div class="row contPrincipal"> 
        <div class="col-sm-12 menu-flotante" id="menu">
            <div class="col-sm-12 btn botonesMenu1" id="endUsers">End Users</div>
            <div class="col-sm-12 btn botonesMenu" id="SPLA">SPLA</div>
            <div class="col-sm-12 btn botonesMenu" id="diagnostics">Diagnostics</div>
            <div class="col-sm-12 btn botonesMenu" id="mediaArticles">Media & Articles</div>
            <div class="col-sm-12 btn botonesMenu" id="presentation">Presentations</div>
            <div class="col-sm-12 btn botonesMenu" id="documents">Documents</div>
            <div class="col-sm-12 btn botonesMenu" id="clear">Clear</div>
        </div>

        <div class="col-sm-12">
            <div class="row">
                <table class="tablap table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <select id="tipoUsuario" name="tipoUsuario">
                                    <option value="">Type</option>
                                    <option value="End User">End User</option>
                                    <option value="SPLA">SPLA</option>
                                </select>
                            </th>
                            <th>
                                <select id="tipoDocumento" name="tipoDocumento">
                                    <option value="">Type</option>
                                    <option value="Business Case">Business Case</option>
                                    <option value="Business Proposals">Business Proposals</option>
                                    <option value="Commercial Proposals">Commercial Proposals</option>
                                    <option value="Diagnostics">Diagnostics</option>
                                    <option value="Documents">Documents</option>
                                    <option value="Executive Summary">Executive Summary</option>
                                    <option value="Final Summary">Final Summary</option>
                                    <option value="Formulary">Formulary</option>
                                    <option value="GANTT">GANTT</option>
                                    <option value="Media & Articles">Media & Articles</option>
                                    <option value="Minuta">Minuta</option>
                                    <option value="Presentation">Presentation</option>
                                    <option value="Purchases">Purchases</option>
                                    <option value="Reports">Reports</option>
                                    <option value="Technical Information">Technical Information</option>
                                    <option value="Technical Proposals">Technical Proposals</option>
                                </select>
                            </th>
                            <td>
                                <select id="para" name="para">
                                    <option value="">For</option>
                                    <option value="Customers">Customers</option>
                                </select>
                            </td>
                            <td>
                                <select id="theme" name="theme">
                                    <option value="">Theme</option>
                                    <option value="Acuerdo de Confidencialidad">Acuerdo de Confidencialidad</option>
                                    <option value="Apps Executive Summary">Apps Executive Summary</option>
                                    <option value="Apps Reports">Apps Reports</option>
                                    <option value="Audit Defense">Audit Defense</option>
                                    <option value="Bill">Bill</option>
                                    <option value="Confidentiality agreement">Confidentiality agreement</option>
                                    <option value="Contrato de Trabajo">Contrato de Trabajo</option>
                                    <option value="Declaración de Trabajo">Declaración de Trabajo</option>
                                    <option value="Diagnostic">Diagnostic</option>
                                    <option value="Diagnostic IBM">Diagnostic IBM</option>
                                    <option value="Diagnostic MS">Diagnostic MS</option>
                                    <option value="Diagnostic Oracle">Diagnostic Oracle</option>
                                    <option value="Diagnostic SAP">Diagnostic SAP</option>
                                    <option value="Diagnostic SPLA">Diagnostic SPLA</option>
                                    <option value="End User Sales PPT - What is LA and Service">End User Sales PPT - What is LA and Service</option>
                                    <option value="Executive Summary">Executive Summary</option>
                                    <option value="Final summary">Final summary</option>
                                    <option value="GANTT">GANTT</option>
                                    <option value="Internal Form Customer Records">Internal Form Customer Records</option>
                                    <option value="KPI´s">KPI´s</option>
                                    <option value="LA Services">LA Services</option>
                                    <option value="Licensing Report">Licensing Report</option>
                                    <option value="List of brands and tools">List of brands and tools</option>
                                    <option value="Microsoft Deliverables">Microsoft Deliverables</option>
                                    <option value="Other brands deliverables">Other brands deliverables</option>
                                    <option value="PM Master">PM Master</option>
                                    <option value="Purchases">Purchases</option>
                                    <option value="Rectified Executive Summary">Rectified Executive Summary</option>
                                    <option value="SAMasaS">SAMasaS</option>
                                    <option value="SAM Report">SAM Report</option>
                                    <option value="SAM SPLA">SAM SPLA</option>
                                    <option value="Service deliverables">Service deliverables</option>
                                    <option value="Service Start Form - Hand Off">Service Start Form - Hand Off</option>
                                    <option value="Services Brochure">Services Brochure</option>
                                    <option value="SPLA Sales PPT - What is LA and Service">SPLA Sales PPT - What is LA and Service</option>
                                    <option value="Start of Service">Start of Service</option>
                                    <option value="Start of Service Diagnostic">Start of Service Diagnostic</option>
                                    <option value="Statement of Work">Statement of Work</option>
                                    <option value="Technical Diagrams">Technical Diagrams</option>
                                    <option value="True Up Report">True Up Report</option>
                                    <option value="Usability Report">Usability Report</option>
                                    <option value="Windows Server Executive Summary">Windows Server Executive Summary</option>
                                    <option value="Windows Server Reports">Windows Server Reports</option>
                                    <option value="Work contract">Work contract</option>
                                </select>
                            </td>
                            <td>
                                <select id="manufacturer" name="manufacturer">
                                    <option value="">Manufacturer</option>
                                    <option value="All">All</option>
                                    <option value="Apps">Apps</option>
                                    <option value="IBM">IBM</option>
                                    <option value="Microsoft">Microsoft</option>
                                    <option value="Oracle">Oracle</option>
                                    <option value="SAP">SAP</option>
                                    <option value="Sybase">Sybase</option>
                                    <option value="VMWare">VMWare</option>


                                    <option value="WinSvr">WinSvr</option>
                                </select>
                            </td>
                            <td class="text-center"><strong>Spanish</strong></td>
                            <td class="text-center"><strong>English</strong></td>
                        </tr>
                    </thead>
                    <tbody id="tablaListado">
                        <?php 
                        foreach($listado as $row){
                        ?>
                            <tr>
                                <th><?= $row["tipoUsuario"] ?></th>
                                <th><?= $row["tipoDocumento"] ?></th>
                                <td><?= $row["para"] ?></td>
                                <td><?= $row["theme"] ?></td>
                                <td><?= $row["manufacturer"] ?></td>
                                <td><?php
                                    if($row["spanish"] == ""){
                                    ?>
                                        <img src="img/SAMasaService.png" class="img img-responsive">
                                    <?php 
                                    } else{
                                    ?>
                                        <a href="<?= $row["spanish"] ?>" target="_blank"><img src="img/<?= $row["imgSpanish"] ?>" class="img img-responsive center-block" style="width:auto; height:50px;"></a>
                                    <?php
                                    }?>
                                </td>
                                <td><?php
                                    if($row["english"] == ""){
                                    ?>
                                        <img src="img/SAMasaService.png" class="img img-responsive">
                                    <?php 
                                    } else{
                                    ?>
                                        <a href="<?= $row["english"] ?>" target="_blank"><img src="img/<?= $row["imgEnglish"] ?>" class="img img-responsive center-block" style="width:auto; height:50px;"></a>
                                    <?php
                                    }?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <script>
        $(document).ready(function(){
            $("#tipoUsuario").change(function(){
                filtrar();
            });
            
            $("#tipoDocumento").change(function(){
                filtrar();
            });
            
            $("#para").change(function(){
                filtrar();
            });
            
            $("#theme").change(function(){
                filtrar();
            });
            
            $("#manufacturer").change(function(){
                filtrar();
            });
            
            $("#salir").click(function(){
                location.href = "https://www.licensingassurance.com/portal";
            });
            
            $("#endUsers").click(function(){
                limpiarFiltros();
                limpiarClase();
                $("#endUsers").addClass("active");
                $.post("ajax/filtrar.php", { tipoUsuario : "End User", tipoDocumento : "",
                para : "", theme : "", manufacturer : "" }, function (data) {
                    $("#tablaListado").empty();
                    $("#tablaListado").append(data[0].listado);
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                });
            });
            
            $("#SPLA").click(function(){
                limpiarFiltros();
                limpiarClase();
                $("#SPLA").addClass("active");
                $.post("ajax/filtrar.php", { tipoUsuario : "SPLA", tipoDocumento : "",
                para : "", theme : "", manufacturer : "" }, function (data) {
                    $("#tablaListado").empty();
                    $("#tablaListado").append(data[0].listado);
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                });
            });
            
            $("#diagnostics").click(function(){
                limpiarFiltros();
                limpiarClase();
                $("#diagnostics").addClass("active");
                $.post("ajax/filtrar.php", { tipoUsuario : "", tipoDocumento : "Diagnostics",
                para : "", theme : "", manufacturer : "" }, function (data) {
                    $("#tablaListado").empty();
                    $("#tablaListado").append(data[0].listado);
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                });
            });
            
            $("#mediaArticles").click(function(){
                limpiarFiltros();
                limpiarClase();
                $("#mediaArticles").addClass("active");
                $.post("ajax/filtrar.php", { tipoUsuario : "", tipoDocumento : "Media & Articles",
                para : "", theme : "", manufacturer : "" }, function (data) {
                    $("#tablaListado").empty();
                    $("#tablaListado").append(data[0].listado);
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                });
            });
            
            $("#presentation").click(function(){
                limpiarFiltros();
                limpiarClase();
                $("#presentation").addClass("active");
                $.post("ajax/filtrar.php", { tipoUsuario : "", tipoDocumento : "Presentation",
                para : "", theme : "", manufacturer : "" }, function (data) {
                    $("#tablaListado").empty();
                    $("#tablaListado").append(data[0].listado);
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                });
            });
            
            $("#documents").click(function(){
                limpiarFiltros();
                limpiarClase();
                $("#documents").addClass("active");
                $.post("ajax/filtrar.php", { tipoUsuario : "", tipoDocumento : "Documents",
                para : "", theme : "", manufacturer : "" }, function (data) {
                    $("#tablaListado").empty();
                    $("#tablaListado").append(data[0].listado);
                }, "json")
                .fail(function(jqXHR){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                });
            });
            
            $("#clear").click(function(){
                limpiarFiltros();
                filtrar();
            });
        });
        
        function filtrar(){
            limpiarClase();
            $.post("ajax/filtrar.php", { tipoUsuario : $("#tipoUsuario").val(), tipoDocumento : $("#tipoDocumento").val(),
            para : $("#para").val(), theme : $("#theme").val(), manufacturer : $("#manufacturer").val() }, function (data) {
                $("#tablaListado").empty();
                $("#tablaListado").append(data[0].listado);
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
            });
        }
        
        function limpiarFiltros(){
            $("#tipoUsuario").val("");
            $("#tipoDocumento").val("");
            $("#para").val("");
            $("#theme").val("");
            $("#manufacturer").val("");
        }
        
        function limpiarClase(){
            $("#endUsers").removeClass("active");
            $("#SPLA").removeClass("active");
            $("#diagnostics").removeClass("active");
            $("#mediaArticles").removeClass("active");
            $("#presentation").removeClass("active");
            $("#documents").removeClass("active");
        }
    </script>
 