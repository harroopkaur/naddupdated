<?php
require_once("../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $result = 0;
    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $fabricante = 0;
            if(isset($_POST["fab"]) &&  filter_var($_POST["fab"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fab"];
            }

            $nombre = "";
            if(isset($_POST["equipElim"])){
                $nombre = $general->get_escape($_POST["equipElim"]);
            }

            $result = 0;
            if($fabricante == 1){
                require_once($GLOBALS["app_root"] . "/clases/clase_elimEquiposLAEAdobe.php");
                $equipos = new elimEquiposLAEAdobe();
                $ruta = $GLOBALS["app_root"] . "/adobe/archivos_csvf3/" . $nombre;
            } else if($fabricante == 2){
                require_once($GLOBALS["app_root"] . "/clases/clase_elimEquiposLAEIbm.php");
                $equipos = new elimEquiposLAEIbm();
                $ruta = $GLOBALS["app_root"] . "/ibm/archivos_csvf3/" . $nombre;
            } else if($fabricante == 3){
                require_once($GLOBALS["app_root"] . "/clases/clase_elimEquiposLAEMicrosoft.php");
                $equipos = new elimEquiposLAEMicrosoft();
                $ruta = $GLOBALS["app_root"] . "/microsoft/archivos_csvf3/" . $nombre;
            } else if($fabricante == 4){

            }

            if(file_exists($ruta)){
                if(unlink($ruta)){
                    if(!$equipos->eliminarArchivo($_SESSION["client_id"], $_SESSION["client_empleado"], $nombre)){
                        
                    } else{
                        $result = 1;
                    }
                }
            } else{
                if(!$equipos->eliminarArchivo($_SESSION["client_id"], $_SESSION["client_empleado"], $nombre)){

                } else {
                    $result = 1;
                }
            }
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);