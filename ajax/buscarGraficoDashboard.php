<?php
require_once("../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $result = 0;
    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            //$general     = new General();
            $fabricante = $general->get_escape($_POST["fabricante"]);
            $asignacion = $general->get_escape($_POST["asignacion"]);
            
            $containerC = "";
            $containerS = "";
            $tclient = 0;
            $tserver = 0;
            $total_1LAc = 0;
            $total_2DVc = 0;
            $total_ActC = 0;
            $total_DVc = 0;
            $total_1InacC = 0;
            $total_ActS = 0;
            $total_DVs = 0;
            $total_1InacS = 0;
            $reconciliacionC = 0;
            $reconciliacionS = 0;    
            $totalGeneralActivo = 0;
            $totalGeneralDV = 0;
            $porc = 0;
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            if($fabricante == "Adobe"){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
                $detallesAdobe = new DetallesAdobe();
                $detallesAdobe->listar_todo1Asignacion($_SESSION['client_id'], $asignacion, $asignaciones);
                $containerC = "containerAdobe";
                $containerS = "containerAdobe2";
                
                foreach ($detallesAdobe->lista as $reg_equipos) {
                    if ($reg_equipos["tipo"] == 1) {//cliente
                        $tclient++;
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActC++;
                        }

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVc ++;
                        }
                        else{
                            $total_1InacC++;
                        }
                    } else {//server
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActS++;
                        }

                        $tserver++;

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVs++;
                        }
                        else{
                            $total_1InacS++;
                        }
                    }
                }
                $reconciliacionC = $total_DVc - $total_ActC;
                $reconciliacionS = $total_DVs - $total_ActS;    

                $totalGeneralActivo = $total_ActC + $total_ActS;
                $totalGeneralDV = $total_DVc + $total_DVs;

                if(($total_DVc + $total_ActC) == 0){
                    $porc = 0;
                } else{
                    $porc = round($totalGeneralActivo * 100 / ($totalGeneralDV + $totalGeneralActivo), 0);
                }
            } else if($fabricante == "Windows-IBM"){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
                $detallesIBM = new DetallesIbm();
                $lista = $detallesIBM->listar_todo1Asignacion($_SESSION['client_id'], $asignacion, $asignaciones);
                $containerC = "containerIBM";
                $containerS = "containerIBM2";

                foreach ($lista as $reg_equipos) {
                    if ($reg_equipos["tipo"] == 1) {//cliente
                        $tclient++;
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActC++;
                        }

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVc ++;
                        }
                        else{
                            $total_1InacC++;
                        }
                    } else {//server
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActS++;
                        }

                        $tserver++;

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVs++;
                        }
                        else{
                            $total_1InacS++;
                        }
                    }
                }
                $reconciliacionC = $total_DVc - $total_ActC;
                $reconciliacionS = $total_DVs - $total_ActS;

                $totalGeneralActivo = $total_ActC + $total_ActS;
                $totalGeneralDV = $total_DVc + $total_DVs;


                if(($total_DVc + $total_ActC) == 0){
                    $porc = 0;
                } else{
                    $porc = round($totalGeneralActivo * 100 / ($totalGeneralDV + $totalGeneralActivo), 0);
                }        
            } else if($fabricante == "Microsoft"){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
                $detallesMicrosoft = new DetallesE_f();
                $lista = $detallesMicrosoft->listar_todo1Asignacion($_SESSION['client_id'], $asignacion, $asignaciones);
                $containerC = "containerMicrosoft";
                $containerS = "containerMicrosoft2";

                foreach ($lista as $reg_equipos) {
                    if ($reg_equipos["tipo"] == 1) {//cliente
                        $tclient++;
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActC++;
                        }

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVc ++;
                        }
                        else{
                            $total_1InacC++;
                        }
                    } else {//server
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActS++;
                        }

                        $tserver++;

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVs++;
                        }
                        else{
                            $total_1InacS++;
                        }
                    }
                }
                $reconciliacionC = $total_DVc - $total_ActC;
                $reconciliacionS = $total_DVs - $total_ActS;

                $totalGeneralActivo = $total_ActC + $total_ActS;
                $totalGeneralDV = $total_DVc + $total_DVs;


                if(($total_DVc + $total_ActC) == 0){
                    $porc = 0;
                } else{
                    $porc = round($totalGeneralActivo * 100 / ($totalGeneralDV + $totalGeneralActivo), 0);
                }
            } else if($fabricante == "Windows-Oracle"){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");
                $detallesOracle = new DetallesOracle();
                $lista = $detallesOracle->listar_todo1Asignacion($_SESSION['client_id'], $asignacion, $asignaciones);
                $containerC = "containerOracle";
                $containerS = "containerOracle2";

                foreach ($lista as $reg_equipos) {
                    if ($reg_equipos["tipo"] == 1) {//cliente
                        $tclient++;
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActC++;
                        }

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVc ++;
                        }
                        else{
                            $total_1InacC++;
                        }
                    } else {//server
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActS++;
                        }

                        $tserver++;

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVs++;
                        }
                        else{
                            $total_1InacS++;
                        }
                    }
                }
                $reconciliacionC = $total_DVc - $total_ActC;
                $reconciliacionS = $total_DVs - $total_ActS;

                $totalGeneralActivo = $total_ActC + $total_ActS;
                $totalGeneralDV = $total_DVc + $total_DVs;


                if(($total_DVc + $total_ActC) == 0){
                    $porc = 0;
                } else{
                    $porc = round($totalGeneralActivo * 100 / ($totalGeneralDV + $totalGeneralActivo), 0);
                }
            } else if($fabricante == "SPLA"){
                require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_SPLA.php");
                $detallesSPLA = new Detalles_SPLA();
                $lista = $detallesSPLA->listar_todo1Asignacion($_SESSION['client_id'], $asignacion, $asignaciones);
                $containerC = "containerSPLA";
                $containerS = "containerSPLA2";

                foreach ($lista as $reg_equipos) {
                    if ($reg_equipos["tipo"] == 1) {//cliente
                        $tclient++;
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActC++;
                        }

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVc ++;
                        }
                        else{
                            $total_1InacC++;
                        }
                    } else {//server
                        if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                            $total_ActS++;
                        }

                        $tserverOracle++;

                        if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                            $total_DVs++;
                        }
                        else{
                            $total_1InacS++;
                        }
                    }
                }
                $reconciliacionC = $total_DVc - $total_ActC;
                $reconciliacionS = $total_DVs - $total_ActS;

                $totalGeneralActivo = $total_ActC + $total_ActS;
                $totalGeneralDV = $total_DVc + $total_DVs;


                if(($total_DVc + $total_ActC) == 0){
                    $porc = 0;
                } else{
                    $porc = round($totalGeneralActivo * 100 / ($totalGeneralDV + $totalGeneralActivo), 0);
                }
            }            
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'containerC'=>$containerC, 'reconciliacionC'=>$reconciliacionC,
            'total_ActC'=>$total_ActC, 'containerS'=>$containerS, 'reconciliacionS'=>$reconciliacionS, 'total_ActS'=>$total_ActS, 'porc'=>$porc, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);