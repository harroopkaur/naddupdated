<?php
require_once("../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $result = 0;
    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $tabla = "";
            if(isset($_FILES["equipElim"]["tmp_name"]) && is_uploaded_file($_FILES["equipElim"]["tmp_name"])){
                
                $fabricante = "";
                if(isset($_POST["fab"])){
                    $fabricante = $general->get_escape($_POST["fab"]);
                }
                
                $nombre = "elimEquipo" . date("dmYHis") . ".csv";
                if($fabricante == 1){
                    require_once($GLOBALS["app_root"] . "/clases/clase_elimEquiposLAEAdobe.php");
                    $equipos = new elimEquiposLAEAdobe();
                    $ruta = $GLOBALS["app_root"] . "/adobe/archivos_csvf3/" . $nombre;
                } else if($fabricante == 2){
                    require_once($GLOBALS["app_root"] . "/clases/clase_elimEquiposLAEIbm.php");
                    $equipos = new elimEquiposLAEIbm();
                    $ruta = $GLOBALS["app_root"] . "/ibm/archivos_csvf3/" . $nombre;
                } else if($fabricante == 3){
                    require_once($GLOBALS["app_root"] . "/clases/clase_elimEquiposLAEMicrosoft.php");
                    $equipos = new elimEquiposLAEMicrosoft();
                    $ruta = $GLOBALS["app_root"] . "/microsoft/archivos_csvf3/" . $nombre;
                } else if($fabricante == 4){
                    
                }
                if(move_uploaded_file($_FILES["equipElim"]["tmp_name"], $ruta)){
                    if(!$equipos->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $nombre)){
                        
                    }
                    
                    if (($fichero = fopen($ruta, "r")) !== FALSE) {
                        while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                            $tabla .= '<tr> '
                                    . '<td>' . $datos[0] . '</td> '
                                . '</tr>';
                        }
                        $result = 1;
                        fclose($fichero);
                    }
                }
            } 
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'nombre'=>$nombre, 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);