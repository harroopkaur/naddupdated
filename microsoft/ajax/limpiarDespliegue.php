<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_officesAux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_increm_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_tipo_equipo.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_vs.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario_equipo_microsoft.php");
require_once($GLOBALS["app_root"] . "/clases/clase_filepcs2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usabilidad_Microsoft.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_windows.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_sql.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_msdn.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $consolidadoOf = new Consolidado_Of();
            $consolidadoAux = new ConsolidadoAux();
            $resumenOf     = new Resumen_Of();
            $archivosDespliegue = new clase_archivos_fabricantes();
            $archivosIncremDespliegue = new clase_archivos_increm_fabricantes();
            $escaneo      = new Scaneo_f();
            $detalles     = new DetallesE_f();
            $procesadores = new ConsolidadoProcesadores();
            $tipoEquipo   = new ConsolidadoTipoEquipo();
            $desarrolloPrueba = new desarrolloPruebaVS();
            $usuarioEquipo = new usuarioEquipoMicrosoft();
            $filec        = new Filepc_f();
            $servidores   = new moduloServidores();
            $compras   = new Compras_f();
            $balance  = new Balance_f();
            $usabilidad = new UsabilidadMicrosoft();  
            $desarrolloPruebaWindows = new desarrolloPruebaWindows();
            $desarrolloPruebaSQL = new desarrolloPruebaSQL();
            $desarrolloPruebaMSDN = new desarrolloPruebaMSDN();
            $log = new log();
          
            $i = 22;
            $j = 0;
            if($consolidadoOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($consolidadoAux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
           
            if($archivosDespliegue->eliminarArchivosFabricante($_SESSION["client_id"], $_SESSION["client_empleado"], 3)){
                $j++;
            }
            
            if($archivosIncremDespliegue->eliminarIncremFab($_SESSION["client_id"], $_SESSION["client_empleado"], 3)){
                $j++;
            }
            
            if($resumenOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($usuarioEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($desarrolloPrueba->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($desarrolloPruebaWindows->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($desarrolloPruebaSQL->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($desarrolloPruebaMSDN->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($filec->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($detalles->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($escaneo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($procesadores->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($tipoEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarAlineaionWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarAlineaionSqlServer($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarWindowServer1($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($servidores->eliminarSqlServer1($_SESSION["client_id"], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($compras->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($balance->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            if($usabilidad->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"])){
                $j++;
            }
            
            $result = 0;
            if($i > $j){
                $result = 2;
            } else if($i == $j){
                $result = 1;
            }
            
            $log->insertar(1, 3, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Limpiar Despliegue");
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);