<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $moduloServidor = new moduloServidores();
            $log = new log();
            
            $carpetaBase = $GLOBALS["app_root"] . "/microsoft/archivos_csvf6/";
            $fileVirtual = $_SESSION['client_id'] . $_SESSION['client_empleado'] . "_servidorVirtual" . date("dmYHis") . ".csv";
        
            $vert = 0;
            if(isset($_POST["vert"]) && filter_var($_POST["vert"], FILTER_VALIDATE_INT) !== false){
                $vert = $_POST["vert"];
            }
            
            $error = 0;
            if(isset($_FILES["fileVirtual"]["tmp_name"]) && is_uploaded_file($_FILES["fileVirtual"]["tmp_name"])){
                $nombre_imagen = $_FILES['fileVirtual']['name'];
                $tipo_imagen = $_FILES['fileVirtual']['type'];
                $tamano_imagen = $_FILES['fileVirtual']['size'];
                $temporal_imagen = $_FILES['fileVirtual']['tmp_name'];

                // Validaciones
                if ($nombre_imagen != "") {
                    $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
                    $long = count($extension) - 1;
                    if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
                        $error = 1;
                    }  // Permitir subir solo imagenes JPG,
                } else {
                    $error = 2;
                }
            }else{
                $error = 3;
            }    
            
            if($general->obtenerSeparadorUniversal($temporal_imagen, 1, 12) === true){
                if (($fichero = fopen($temporal_imagen, "r")) !== FALSE) {
                    $i = 0;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i == 0){
                            if($datos[0] != "Cluster" || $datos[1] != "Host" || $datos[2] != "Nombre" || $datos[3] != "Familia" || utf8_encode($datos[4]) != "Edición" || utf8_encode($datos[5]) != "Versión" ||
                            $datos[6] != "MSDN" || $datos[7] != "Tipo" || $datos[8] != "Centro Costos" || $datos[9] != "Procesadores" || 
                            $datos[10] != "Cores" || $datos[11] != "Lic Srv/CAL" || $datos[12] != "Lic Proc" || $datos[13] != "Lic Core"){
                                $error = 4;
                            }
                        } else{
                            break;
                        }
                        $i++;
                    }
                    
                    fclose($fichero);
                }
            }

            $i = 0;
            $conteo = 1;
            if($vert == 0){
                $moduloServidor->eliminarWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");

                if($error == 0){
                    move_uploaded_file($temporal_imagen, $carpetaBase . $fileVirtual);
                    
                    if($general->obtenerSeparadorUniversal($carpetaBase . $fileVirtual, 1, 14) === true){
                        if (($fichero = fopen($carpetaBase . $fileVirtual, "r")) !== FALSE) {
                            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                                if($i > 0){
                                    $procesadoresVirtual = 0;
                                    $coresVirtual = 0;
                                    $licSrvVirtual = 0;
                                    $licProcVirtual = 0;
                                    $licCoreVirtual = 0;

                                    if(filter_var($datos[9], FILTER_VALIDATE_INT) !== false){
                                        $procesadoresVirtual = $datos[9];
                                    }

                                    if(filter_var($datos[10], FILTER_VALIDATE_INT) !== false){
                                        $coresVirtual = $datos[10];
                                    }

                                    if(filter_var($datos[11], FILTER_VALIDATE_INT) !== false){
                                        $licSrvVirtual = $datos[11];
                                    }

                                    if(filter_var($datos[12], FILTER_VALIDATE_INT) !== false){
                                       $licProcVirtual = $datos[12]; 
                                    }

                                    if(filter_var($datos[13], FILTER_VALIDATE_INT) !== false){
                                        $licCoreVirtual = $datos[13];
                                    }

                                    $edi = str_replace("_", " ", $general->get_escape($datos[4]));
                                    $edi = str_replace(".", "-", $edi);
                                    
                                    if($moduloServidor->agregarWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"], $general->get_escape($datos[0]), 
                                    $general->get_escape($datos[1]), $general->get_escape($datos[2]), $general->get_escape($datos[3]), 
                                    $edi, $general->get_escape($datos[5]), $general->get_escape($datos[6]), 
                                    "Virtual", $general->get_escape($datos[8]), $procesadoresVirtual, $coresVirtual, $licSrvVirtual, $licProcVirtual, $licCoreVirtual)){
                                        $conteo++;
                                    }
                                }
                                $i++;
                            }
                            
                            $log->insertar(3, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Windows Server Virtual");
                        
                            fclose($fichero);
                        }
                    }
                }  
            }
            else{
                $moduloServidor->eliminarSqlServer($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");

                if($error == 0){
                    move_uploaded_file($temporal_imagen, $carpetaBase . $fileVirtual);
                    
                    if($general->obtenerSeparadorUniversal($carpetaBase . $fileVirtual, 1, 12) === true){
                        if (($fichero = fopen($carpetaBase . $fileVirtual, "r")) !== FALSE) {
                            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                                if($i > 0){
                                    $procesadoresVirtual = 0;
                                    $coresVirtual = 0;
                                    $licSrvVirtual = 0;
                                    $licProcVirtual = 0;
                                    $licCoreVirtual = 0;

                                    if(filter_var($datos[9], FILTER_VALIDATE_INT) !== false){
                                        $procesadoresVirtual = $datos[9];
                                    }

                                    if(filter_var($datos[10], FILTER_VALIDATE_INT) !== false){
                                        $coresVirtual = $datos[10];
                                    }

                                    if(filter_var($datos[11], FILTER_VALIDATE_INT) !== false){
                                        $licSrvVirtual = $datos[11];
                                    }

                                    if(filter_var($datos[12], FILTER_VALIDATE_INT) !== false){
                                       $licProcVirtual = $datos[12]; 
                                    }

                                    if(filter_var($datos[13], FILTER_VALIDATE_INT) === false){
                                        $licCoreVirtual = $datos[13];
                                    }

                                    $edi = str_replace("_", " ", $general->get_escape($datos[4]));
                                    $edi = str_replace(".", "-", $edi);
                                    
                                    if($moduloServidor->agregarSQLServer($_SESSION["client_id"], $_SESSION["client_empleado"], $general->get_escape($datos[0]), 
                                    $general->get_escape($datos[1]), $general->get_escape($datos[2]), $general->get_escape($datos[3]), 
                                    $edi, $general->get_escape($datos[5]), $general->get_escape($datos[6]), 
                                    "Virtual", $general->get_escape($datos[8]), $procesadoresVirtual, $coresVirtual, $licSrvVirtual, $licProcVirtual, $licCoreVirtual)){
                                        $conteo++;
                                    }
                                }
                                $i++;
                            }
                            
                            $log->insertar(3, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "SQL Server Virtual");
                        
                            fclose($fichero);
                        }
                    }
                }      
            }

            $result = 0;

            if($conteo == $i){
                $result = 1;
            }
            else if($conteo > 1){
                $result = 2;
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'error'=>$error, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);