<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $moduloServidor = new moduloServidores($conn);
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST['fabricante'], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }
            
            $producto   = $general->get_escape($_POST["producto"]);

            $ediciones = $moduloServidor->obtenerEdicProductoNombre($fabricante, $producto);
            $option = "<option value=''></option>";
            foreach($ediciones as $row){
                $option .= "<option value='" . $row["nombre"] . "'>" . $row["nombre"] . "</option>";
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'option'=>$option, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
?>