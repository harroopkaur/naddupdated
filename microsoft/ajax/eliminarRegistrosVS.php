<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_vs.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_msdn.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $desarrolloPrueba = new desarrolloPruebaVS();
            $desarrolloPruebaMSDN = new desarrolloPruebaMSDN();
               
            if(isset($_POST["checkVS"])){
                $idVS = $_POST["checkVS"];
                $i = 0;
                foreach($idVS as $row){
                    $id = 0;
                    if(filter_var($row, FILTER_VALIDATE_INT) !== false){
                        $id = $row;
                    }
                    
                    $desarrolloPrueba->eliminarId($id);
                    $i++;
                }
            }
            
            $listado = $desarrolloPruebaMSDN->listadoDesarrolloPruebaMSDN($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $tabla = "";
            $i = 0;
            $j = 1;
            foreach($listado as $row){
                $tabla .= '<tr>
                    <td>' . $j . '</td>
                    <td class="text-center">
                        <input type="checkbox" id="checkMSDN' . $i . '" name="checkMSDN[]" value="' . $row["id"] .'">
                        <input type="hidden" id="idMSDN' . $i . '" name="idMSDN[]" value="' . $row["id"] . '">
                    </td>
                    <td>' . $row["equipo"] . '</td>
                    <td>' . $row["tipo"] . '</td>
                    <td>' . $row["familia"] . '</td>
                    <td>' . $row["edicion"] . '</td>
                    <td>' . $row["version"] . '</td>
                    <td>' . $general->reordenarFecha($row["fechaInstalacion"], "-", "/") . '</td>
                    <th class="text-center"><input type="text" style="width:100%" id="usuarioMSDN' . $i . '" name="usuarioMSDN[]" value="' . $row["usuario"] . '" readonly></th>
                    <th class="text-center"><input type="text" style="width:100%" id="equipoMSDN' . $i . '" name="equipoMSDN[]" value="' . $row["equipoUsuario"] . '" readonly></th>
                    <th class="text-center">
                        <select id="msdn' . $i . '" name="msdn' . $i . '">
                            <option value="0"';
                                if($row["msdn"] == "" || $row["msdn"] == 0){ $tabla .= "selected='selected'"; }
                            $tabla .= '>0</option>
                            <option value="1"';
                                if($row["msdn"] == 1){ $tabla .= "selected='selected'"; }
                            $tabla .= '>1</option>
                        </select>
                    </th>
                </tr>';
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'cant'=>count($tabla), 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);