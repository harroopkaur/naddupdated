<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
} 

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }
    
    if($verifSesion[0]){
        $_SESSION['client_tiempo'] = $verifSesion[1];
        
        $tipoConsolidado = "";
        if(isset($_POST["tipoArchivo"])){
            $tipoConsolidado = $_POST["tipoArchivo"];
        } 

        $archivoCargado = "";
        $carpetaCargar = $GLOBALS['app_root'] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/despliegueOtros";
        if($tipoConsolidado == "consolidado"){
            $archivoCargado = "archivoConsolidado";
            $nombreArchivo = "Consolidado Addremove".date("dmYHis") . ".csv";
        } else if($tipoConsolidado == "procesadores"){
            $archivoCargado = "archivoProcesadores";
            $nombreArchivo = "Consolidado Procesadores".date("dmYHis") . ".csv";
        } else if($tipoConsolidado == "tipoEquipo"){
            $archivoCargado = "archivoTipoEquipo";
            $nombreArchivo = "Consolidado Tipo de Equipo".date("dmYHis") . ".csv";
        } else if($tipoConsolidado == "usuarioEquipo"){
            $archivoCargado = "archivoUsuarioEquipo";
            $nombreArchivo = "Consolidado Usuario-Equipo".date("dmYHis") . ".csv";
        } else if($tipoConsolidado == "equipo"){
            $archivoCargado = "archivoEquipo";
            $nombreArchivo = $_SESSION["client_id"] . $_SESSION["client_empleado"] . "_LAE_Output".date("dmYHis") . ".csv";
            $carpetaCargar = $GLOBALS['app_root'] . "/microsoft/archivos_csvf2/despliegueOtros";
        } else if($tipoConsolidado == "escaneo"){
            $archivoCargado = "archivoEscaneo";
            $nombreArchivo = "Resultados_Escaneo".date("dmYHis") . ".csv";
        }
            
        if(isset($_FILES[$archivoCargado]) && is_uploaded_file($_FILES[$archivoCargado]["tmp_name"])){	
            $archivoTemp = $_FILES[$archivoCargado]["tmp_name"];
            $tipoArchivo = $_FILES[$archivoCargado]["type"];
            $tamano = $_FILES[$archivoCargado]["size"];
            $mensajeError = $_FILES[$archivoCargado]["error"];

            if(!file_exists($carpetaCargar)){
                mkdir($carpetaCargar, 0777, true);
            }

            if(move_uploaded_file($archivoTemp, $carpetaCargar . "/" . $nombreArchivo)){
                $archivo = $carpetaCargar . "/" . $nombreArchivo;

                if($general->obtenerSeparador($archivo) === true){
                    if (($fichero = fopen($archivo, "r")) !== false) {
                        $i = 0;

                        $option = "<option value=''>--Seleccione--</option>";
                        if(($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                            for($i = 0; $i < count($datos); $i++){
                                $option .= "<option value='" . utf8_encode($datos[$i]) . "'>" . utf8_encode($datos[$i]) . "</option>";
                            }
                        }
                        fclose($fichero);
                    }
                } else{
                    echo "5*";
                } 
                    echo "1*" . $option . "*" . $nombreArchivo; 
            } else{
                echo "3*" . $tipoConsolidado; 
            }
        } else {
            echo "4*";
        } 
    }
    else{
        $general->eliminarSesion();
        echo "0*"; 
    }
} else{
    echo "2*";
}