<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_trueUp.php");
require_once($GLOBALS["app_root"] . "/clases/clase_trueUp_detalle.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $trueUp = new trueUp();
            $trueUpDetalle = new trueUpDetalle();
            
            $id = 0;
            if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
                $id = $_POST["id"];
            }  

            $rowTrueUp = $trueUp->datosTrueUp($id);
            $tabla = $trueUpDetalle->detalleTrueUp($id);
            
            $listado = "";
            $i = 0;
            $totalPropuesto = 0;
            foreach($tabla as $row){
                $listado .= '<tr>
                    <td class="rowMiddle">' . $row["producto"] . '</td>
                    <td class="rowMiddle">' . $row["edicion"] . '</td>
                    <td class="rowMiddle">' . $row["version"] . '</td>
                    <td class="rowMiddle">' . $row["precioEstimado"] . '</td>
                    <td class="rowMiddle">' . $row["cantidad"] . '</td>
                    <td class="rowMiddle text-right">$ ' . $row["total"] . '</td>
                    <td class="rowMiddle">' . $row["cantidadPropuesta"] . '</td>
                    <td class="rowMiddle text-right">$ ' . $row["totalPropuesto"] . '</td>
                    <td class="rowMiddle" style="width:100px;">' . $row["comentarios"] . '</td>
                </tr>';
                
                $totalPropuesto += $row["totalPropuesto"];
                $i++;
            }
            $listado .= '<tr>
                    <td colspan="7" class="rowMiddle text-right">Costo Estimado True Up</td>
                    <td class="rowMiddle text-right">$ ' . $totalPropuesto . '</td>
                    <td>&nbsp;</td>   
                </tr>';
                   
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$listado, 'cliente'=>$rowTrueUp["empresa"], 
            'fecha'=>$general->muestraFechaHora($rowTrueUp["fecha"]), 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);