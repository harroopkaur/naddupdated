<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $optimizacion = new DetallesE_f();
            
            $ordenar = "";
            if(isset($_POST['ordenar'])){
                $ordenar = $general->get_escape($_POST['ordenar']);
            }
            
            /*$direccion = "";
            if(isset($_POST['direccion'])){
                $direccion = $general->get_escape($_POST['direccion']);
            }*/
            
            $opcion = "";
            if(isset($_POST['opcion'])){
                $opcion = $general->get_escape($_POST['opcion']);
            }
            
            $asignacion = "";
            if(isset($_POST['asignacion'])){
                $asignacion = $general->get_escape($_POST['asignacion']);
            }
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            if($opcion == "servidor"){
                $listarOptimizacion = $optimizacion->listar_optimizacionServidorAsignacion($_SESSION['client_id'], $asignacion, $asignaciones);
                $tabla = '<div style="width:98%;height:400px; overflow-x:hidden; overflow-y:auto;">
                    <table width="95%" class="tablap" id="tablaOptimizacion" style="margin-top:-5px;">
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th align="center" valign="middle"><span>&nbsp;</span></th>
                            <th align="center" valign="middle"><span>Equipo</span></th>
                            <th align="center" valign="middle"><span>Windows Server</span></th>
                            <th align="center" valign="middle"><span>SQL Server</span></th>
                            <th align="center" valign="middle"><span>Usabilidad</span></th>
                            <th align="center" valign="middle"><span>Observaci&oacute;n</span></th>
                        </tr>
                    </thead>
                    <tbody >';
            }
            else if($opcion == "cliente"){
                $listarOptimizacion = $optimizacion->listar_optimizacionAsignacion($_SESSION['client_id'], $asignacion, $asignaciones);
                $tabla = '<div style="width:98%;height:400px; overflow-x:hidden; overflow-y:auto;">
                    <table width="95%" class="tablap" id="tablaOptimizacion" style="margin-top:-5px;">
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th align="center" valign="middle"><span>&nbsp;</span></th>
                            <th align="center" valign="middle"><span>Equipo</span></th>
                            <th align="center" valign="middle"><span>Office</span></th>
                            <th align="center" valign="middle"><span>Visio</span></th>
                            <th align="center" valign="middle"><span>Project</span></th>
                            <th align="center" valign="middle"><span>Visual Studio</span></th>
                            <th align="center" valign="middle"><span>Usabilidad</span></th>
                            <th align="center" valign="middle"><span>Observaci&oacute;n</span></th>
                        </tr>
                    </thead>
                    <tbody >';
            }

            $i = 1;
            if($opcion == "servidor"){
                foreach ($listarOptimizacion as $reg_equipos) {
                    $tabla .= '<tr>
                                <td>' . $i . '</td>
                                <td>' . $reg_equipos["equipo"] . '</td>
                                <td>' . $reg_equipos["os"] . '</td>
                                <td>' . $reg_equipos["SQLServer"] . '</td>
                                <td>' . $reg_equipos["usabilidad"] . '</td>
                                <td>' . $reg_equipos["duplicado"] . '</td>
                            </tr>';
                    $i++;
                }
            }
            else if($opcion == "cliente"){
                foreach ($listarOptimizacion as $reg_equipos) {
                    $tabla .= '<tr>
                                <td>' . $i . '</td>
                                <td>' . $reg_equipos["equipo"] . '</td>
                                <td>' . $reg_equipos["office"] . '</td>
                                <td>' . $reg_equipos["visio"] . '</td>
                                <td>' . $reg_equipos["project"] . '</td>
                                <td>' . $reg_equipos["visual"] . '</td>
                                <td>' . $reg_equipos["usabilidad"] . '</td>
                                <td>' . $reg_equipos["duplicado"] . '</td>
                            </tr>';
                    $i++;
                }
            }

            $tabla .= '</tbody>
                    </table>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#tablaOptimizacion").tablesorter();
                        $("#tablaOptimizacion").tableHeadFixer();
                    });
                </script>';
            $array = array(0 => array('sesion'=>$sesion, 'mensaje'=>'', 'tabla' => $tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);