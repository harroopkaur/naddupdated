<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen2.php");

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

$array = array(0=>array('resultado'=>false));
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $vert = 5;
            if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
                $vert = $_POST['vert']; 
            }
            
            $opcion = "0";
            if(isset($_POST['opcion']) AND filter_var($_POST["opcion"], FILTER_VALIDATE_INT) !== false){
                $opcion = $_POST['opcion'];
            }
            
            $asig = "caracas";
            if(isset($_POST['asig'])){
                $asig = $general->get_escape($_POST['asig']);
            }
            
            $dup = "Si";
            if(isset($_POST['dup']) && $_POST["dup"] == "No"){
                $dup = "No";
            }

            $detalles1 = new DetallesE_f();
            $detalles2 = new DetallesE_f();
            $resumen   = new Resumen_f();
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $tabla = '';
            if($vert == 0 || $vert == 1 || $vert == 2 || $vert == 3 || $vert == 4 || $vert == 8 || $vert == 81 || $vert == 82 || $vert == 83 || $vert == 84){
                $tabla .= '<table width="95%;" style="margin-top:-5px;" class="tablap" id="tablaMicrosoftDetalle">
                            <thead>
                                <tr style="background:#333; color:#fff;">
                                        <th valign="middle"><span>&nbsp;</span></th>
                                        <th valign="middle"><span>Nombre Equipo</span></th>
                                        <th valign="middle"><span>Tipo</span></th>
                                        <th valign="middle"><span>Sistema Operativo</span></th>
                                        <th valign="middle"><span>Activo AD</span></th>
                                        <th valign="middle"><span>LA Tool</span></th>
                                        <th valign="middle"><span>Usabilidad</span></th>
                                        <th valign="middle"><span>Asignación</span></th>
                                </tr>
                            </thead>
                            <tbody>';

                if($vert == 0 || $vert == 8){
                    $listar_equipos0 = $detalles2->listar_todog0Asignacion($_SESSION['client_id'], $asig, $asignaciones);
                    $i = 1;
                    foreach($listar_equipos0 as $reg_equipos0){
                        if($vert == 0){
                            $tipo = 1;
                            $tipoMaquina = 'Cliente';
                        }
                        else{
                            $tipo = 2;
                            $tipoMaquina = 'Servidor';
                        }
                        if($opcion == 1){
                            if(($reg_equipos0["rango"] == 1 || $reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3) && $reg_equipos0["tipo"] == $tipo){    
                                $tabla .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $reg_equipos0["equipo"] . '</td>
                                    <td>' . $tipoMaquina . '</td>
                                    <td>' . $reg_equipos0["os"] . '</td>
                                    <td>' . $reg_equipos0["ActivoAD"] . '</td>
                                    <td>' . $reg_equipos0["LaTool"] . '</td>
                                    <td>' . $reg_equipos0["usabilidad"] . '</td>
                                    <td>' . $reg_equipos0["asignacion"] . '</td>
                                    </tr>';
                                 $i++;
                            }
                        }
                        else{
                            if($reg_equipos0["rango"] != 1 && $reg_equipos0["rango"] != 2 && $reg_equipos0["rango"] != 3 && $reg_equipos0["tipo"] == $tipo){
                                $tabla .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $reg_equipos0["equipo"] . '</td>
                                    <td>' . $tipoMaquina . '</td>
                                    <td>' . $reg_equipos0["os"] . '</td>
                                    <td>' . $reg_equipos0["ActivoAD"] . '</td>
                                    <td>' . $reg_equipos0["LaTool"] . '</td>
                                    <td>' . $reg_equipos0["usabilidad"] . '</td>
                                    <td>' . $reg_equipos0["asignacion"] . '</td>
                                </tr>';
                                 $i++;
                            }
                        }   

                    }
                }
                else{
                    if($vert == 1){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
                        'Standard', $asig, $asignaciones);
                        $tipo = 1;
                        $tipoMaquina = 'Cliente';
                    }
                    if($vert == 2){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
                        'Enterprise', $asig, $asignaciones);
                        $tipo = 1;
                        $tipoMaquina = 'Cliente';
                    }
                    if($vert == 3){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
                        'Professional', $asig, $asignaciones);
                        $tipo = 1;
                        $tipoMaquina = 'Cliente';
                    }
                    if($vert == 4){
                        $listar_equipos0=$detalles1->listar_todog4Asignacion($_SESSION['client_id'],
                        'Standard', 'Enterprise','Professional', $asig, $asignaciones);
                        $tipo = 1;
                        $tipoMaquina = 'Cliente';
                    }
                    if($vert == 81){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'],
                        'Standard', $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Servidor';
                    }
                    if($vert == 82){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'],
                        'Datacenter', $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Servidor';
                    }
                    if($vert == 83){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'],
                        'Enterprise', $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Servidor';
                    }
                    if($vert == 84){
                        $listar_equipos0=$detalles1->listar_todog3Asignacion($_SESSION['client_id'], $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Servidor';
                    }
                    $i = 1;
                    foreach($listar_equipos0 as $reg_equipos0){
                        if($reg_equipos0["tipo"] == $tipo){
                            $tabla .= '<tr>
                                <td>' . $i . '</td>
                                <td>' . $reg_equipos0["equipo"] . '</td>
                                <td>' . $tipoMaquina . '</td>
                                <td>' . $reg_equipos0["os"] . '</td>
                                <td>' . $reg_equipos0["ActivoAD"] . '</td>
                                <td>' . $reg_equipos0["errors"] . '</td>
                                <td>' . $reg_equipos0["usabilidad"] . '</td>
                                <td>' . $reg_equipos0["asignacion"] . '</td>
                            </tr>';
                            $i++;
                        }
                    }
                }
            }
            else{
                $tabla .= '<table width="95%; style="margin-top:-5px;" class="tablap" id="tablaMicrosoftDetalle">
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th valign="middle"><span>&nbsp;</span></th>
                            <th valign="middle"><span>Equipo</span></th>
                            <th valign="middle"><span>Tipo</span></th>
                            <th valign="middle"><span>Familia</span></th>
                            <th valign="middle"><span>Edici&oacute;n</span></th>
                            <th valign="middle"><span>Versi&oacute;n</span></th>
                            <th valign="middle"><span>Instalaci&oacute;n</span></th>
                            <th valign="middle"><span>Usabilidad</span></th>
                            <th valign="middle"><span>Observaci&oacute;n</span></th>
                            <th valign="middle"><span>Asignación</span></th>
                        </tr>
                    </thead>
                    <tbody>';
                if($vert == 5){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Office','', $asig, $asignaciones, $dup);
                }
                if($vert == 51){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Office','Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 52){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'],
                    'Office','Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 53){
                    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 
                    'Office', 'Standard', 'Professional', $asig, $asignaciones, $dup);
                } 
                if($vert == 6){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Project','', $asig, $asignaciones, $dup);
                }
                if($vert == 61){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Project','Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 62){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Project','Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 63){
                    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 
                    'Project','Professional', 'Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 7){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'visio','', $asig, $asignaciones, $dup);
                }
                if($vert == 71){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'visio','Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 72){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'visio','Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 73){
                    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 
                    'visio','Standard','Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 9){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','', $asig, $asignaciones, $dup);
                }
                if($vert == 91){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 92){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','Datacenter', $asig, $asignaciones, $dup);
                }
                if($vert == 93){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','Enterprise', $asig, $asignaciones, $dup);
                }
                if($vert == 94){
                    $lista_calculo = $resumen->listar_datos8Asignacion($_SESSION['client_id'], 
                    'SQL Server','Standard','Datacenter','Enterprise', $asig, $asignaciones, $dup);
                }
                if($vert == 10){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Visual Studio','', $asig, $asignaciones, $dup);
                }
                if($vert == 11){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Exchange Server','', $asig, $asignaciones, $dup);
                }
                if($vert == 12){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Sharepoint Server','', $asig, $asignaciones, $dup);
                }
                if($vert == 13){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Skype for Business','', $asig, $asignaciones, $dup);
                }
                if($vert == 14){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'System Center','', $asig, $asignaciones, $dup);
                }

                $i = 1;
                foreach($lista_calculo as $reg_calculo){
                    $tabla .= '<tr>
                        <td>' . $i . '</td>
                        <td>' . $reg_calculo["equipo"] . '</td>
                        <td>' . $reg_calculo["tipo"] . '</td>
                        <td>' . $reg_calculo["familia"] . '</td>
                        <td>' . $reg_calculo["edicion"] . '</td>
                        <td>' . $reg_calculo["version"] . '</td>
                        <td>' . $reg_calculo["fecha_instalacion"] . '</td>
                        <td>' . $reg_calculo["rango"] . '</td>
                        <td>';
                        if($resumen->duplicado($_SESSION['client_id'], $_SESSION['client_empleado'], $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
                            $tabla .= " Duplicado";
                        }
                        $tabla .= '</td>
                        <td>' . $reg_calculo["asignacion"] . '</td>
                    </tr>';
                    $i++;
                }
            }
            $tabla .= '</tbody>
                </table>
            <script> 
                $(document).ready(function(){
                    $("#tablaMicrosoftDetalle").tablesorter();
                    $("#tablaMicrosoftDetalle").tableHeadFixer();
                });
            </script>';
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);