<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras2.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            if($_SESSION["idioma"] == 1){
                $otrosIdioma = "Otros";
            }else if($_SESSION["idioma"] == 2){
                $otrosIdioma = "Others";
            }
            
            $balance2= new Balance_f();
            $resumen = new Resumen_Of();
            $detalles1 = new DetallesE_f();
            $compras = new Compras_f();
            
            $familia = "";
            if(isset($_POST["familia"])){
                $familia = $general->get_escape($_POST["familia"]);
            }

            $edicion = "";
            if(isset($_POST["edicion"])){
                $edicion = $general->get_escape($_POST["edicion"]);
            }
            
            $asig = "";
            if(isset($_POST["asignacion"])){
                $asig = $general->get_escape($_POST["asignacion"]);
            }
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            //$clientTotalOfficeCompras  = 0;
            $clientTotalOfficeInstal   = 0;
            $clientTotalOfficeInac     = 0;	
            //$clientOfficeNeto          = 0;
            $total_1Inac               = 0;

            $titulo = "";

            if($familia != "Otros"){
                $titulo = $familia;
            }
            else{
                 $titulo = $edicion;
            }

            if($familia == "Windows" || $familia == "Windows Server"){
                if($edicion != "Otros"){
                    //$listar_Of = $balance2->listar_todo_familias6($_SESSION['client_id'], $_SESSION["client_empleado"], $familia, $edicion);
                    $listar = $detalles1->totalWindowsAsignacion($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones); 
                    $clientTotalOfficeCompras = $compras->totalCompras($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones);
                    $listar_Of = $balance2->balanzaAsignacion($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones);
                }
                else{
                    //$listar_Of = $balance2->listar_todo_familias10($_SESSION['client_id'], $_SESSION["client_empleado"], $familia);
                    $listar = $detalles1->totalWindowsAsignacionOtros($_SESSION['client_id'], $familia, $asig, $asignaciones); 
                    $clientTotalOfficeCompras = $compras->totalComprasOtros($_SESSION['client_id'], $familia, $asig, $asignaciones);
                    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION['client_id'], $familia, $asig, $asignaciones);
                }
            }
            else if($familia == "Others"){
                //$listar_Of = $balance2->listar_todo_familias7($_SESSION['client_id'], $_SESSION["client_empleado"], $edicion);
                $familiaAux = $familia;
                if($edicion == ""){
                    $familiaAux = $familia; 
                } else{
                    $familiaAux = $edicion;
                }
                $listar = $resumen->instalResumenAsignacion($_SESSION['client_id'], $familiaAux, "", $asig, $asignaciones); 
                $clientTotalOfficeCompras = $compras->totalCompras($_SESSION['client_id'], $familiaAux, "", $asig, $asignaciones);
                $listar_Of = $balance2->balanzaAsignacion($_SESSION['client_id'], $familiaAux, "", $asig, $asignaciones);
            }
            else{
                if($edicion != "Otros"){
                    //$listar_Of = $balance2->listar_todo_familias1($_SESSION['client_id'], $_SESSION["client_empleado"], $familia, $edicion);
                    $listar = $resumen->instalResumenAsignacion($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones); 
                    $clientTotalOfficeCompras = $compras->totalCompras($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones);
                    $listar_Of = $balance2->balanzaAsignacion($_SESSION['client_id'], $familia, $edicion, $asig, $asignaciones);
                }
                else{
                    //$listar_Of = $balance2->listar_todo_familias9($_SESSION['client_id'], $_SESSION["client_empleado"], $familia);
                    $listar = $resumen->instalResumenAsignacionOtros($_SESSION['client_id'], $familia, $asig, $asignaciones); 
                    $clientTotalOfficeCompras = $compras->totalComprasOtros($_SESSION['client_id'], $familia, $asig, $asignaciones);
                    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION['client_id'], $familia, $asig, $asignaciones);
                }
            }

            foreach($listar as $reg_equipos){
                //$clientTotalOfficeCompras += $reg_equipos["compra"];
                if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                    $clientTotalOfficeInstal  += $reg_equipos["instalaciones"];
                } else{
                    if($familia != "Windows" && $familia != "Windows Server"){
                        $clientTotalOfficeInac  += $reg_equipos["instalaciones"];
                    }
                }
                //$clientOfficeNeto          = $clientTotalOfficeCompras - $clientTotalOfficeInstal;
            }
            
            $clientOfficeNeto = $clientTotalOfficeCompras - ($clientTotalOfficeInstal + $clientTotalOfficeInac);
            

            /*if($familia == "Windows" || $familia == "Windows Server"){
                if($edicion == ""){
                    $listar_equipos = $detalles1->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                }
                else{
                    $listar_equipos = $detalles1->listar_todo_office($_SESSION['client_id'], $_SESSION["client_empleado"], $edicion);
                }

                if ($listar_equipos) {
                    $totalInstalC = 0;
                    $totalInstalS = 0;
                    $totalInactC  = 0;
                    $totalInactS  = 0; 
                    foreach ($listar_equipos as $reg_equipos) {

                        if ($reg_equipos["tipo"] == 1) {//cliente
                            if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                                $totalInstalC++;
                            }
                            else{
                                $totalInactC++;
                            }
                        } else {//server
                            if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                                $totalInstalS++;
                            }
                            else{
                                $totalInactS++;
                            }
                        }
                    }
                    if($familia == "Windows"){
                        $clientTotalOfficeInstal = $totalInstalC;
                        $total_1Inac             = $totalInactC;
                    }
                    else{
                        $clientTotalOfficeInstal = $totalInstalS;
                        $total_1Inac             = $totalInactS;
                    }
                    $clientOfficeNeto = $clientTotalOfficeCompras - $clientTotalOfficeInstal;
                }
            }*/

            $tabla = "";
            $script = "";
            $z = 0;
            $familiaAux = "";
            $edicionAux = "";
            foreach($listar_Of as $reg_equipos){
                if($familiaAux != $reg_equipos["familia"]){
                    $familiaAux = $reg_equipos["familia"];
                    $edicionAux = $reg_equipos["office"];
                    $habilitar = $balance2->verifSoftwareAssurance($_SESSION["client_id"], $_SESSION["client_empleado"], 
                    $reg_equipos["familia"], $reg_equipos["office"]);
                } else{
                    if($edicionAux != $reg_equipos["office"]){
                        $familiaAux = $reg_equipos["familia"];
                        $edicionAux = $reg_equipos["office"];
                        $habilitar = $balance2->verifSoftwareAssurance($_SESSION["client_id"], $_SESSION["client_empleado"], 
                        $reg_equipos["familia"], $reg_equipos["office"]);
                    }
                }
               
                $tabla .= '<tr>
                        <td><input type="hidden" id="productoGAP' . $z . '" name="productoGAP[]" value="' . $reg_equipos["id"] . '">' . $reg_equipos["familia"] . '</td>
                        <td>' . $reg_equipos["office"] . '</td>
                        <td>' . $reg_equipos["version"] . '</td>
                        <td>' . $reg_equipos["asignacion"] . '</td>
                        <td align="center">' . $reg_equipos["instalaciones"] . '</td>
                        <td align="center">' . round($reg_equipos["compra"], 0) . '</td>
                        <td align="center">' . round($reg_equipos["balance"], 0) . '</td>
                        <td align="center"'; 
                if($reg_equipos["balancec"] < 0){
                    $tabla .= ' style="color:red;"';
                }
                $tabla .= '>' . round($reg_equipos["balancec"], 0) . '</td>
                    <td><input type="hidden" id="habilitar' . $z . '" value="' . $habilitar . '">
                    <input type="text" id="cantidadGAP' . $z . '" name="cantidadGAP[]" value="';
                    if($reg_equipos["cantidadGAP"] == ""){
                        $tabla .= 0;
                    } else{
                        $tabla .= $reg_equipos["cantidadGAP"];
                    }
                    $tabla .=  '" maxlength="4" style="width:50px; text-align:right; background-color:#C1C1C1;" readonly onclick="limpiarInput(this);" '
                    . 'onblur="actualizarInput(this, ' . round($reg_equipos["compra"], 0) . ', ' . $reg_equipos["instalaciones"] . ', \'totalGAP' . $z . '\')" '
                    . 'onkeyup="verifEnter(this, ' . round($reg_equipos["compra"], 0) . ', ' . $reg_equipos["instalaciones"] . ', \'totalGAP' . $z . '\')"></td>
                    <td><input type="text" style="width:80px; text-align:right; background-color:#C1C1C1;" id="totalGAP' . $z . '" name="totalGAP[]" value="';
                    if($reg_equipos["totalGAP"] == ""){
                        $tabla .= round($reg_equipos["balance"]);
                    } else{
                        $tabla .= $reg_equipos["totalGAP"];
                    }
                    
                    $tabla .= '" maxlength="11" readonly';
                    $tabla .= '></td>
                </tr>';
                $script .= '$("#cantidadGAP' . $z . '").numeric(false);';
                $script .= '$("#totalGAP' . $z . '").numeric(false);';
                $z++;
            }
            
            $tabla .= '<script>
                ' . $script . '        
                function limpiarInput(input){
                    if(input.readOnly === true){
                        return false;
                    }
                    input.value = "";
                }
                
                function actualizarInput(input, compras, instalaciones, total){
                    if(input.readOnly === true){
                        return false;
                    }
                    
                    if(input.value === ""){
                        input.value = 0;
                        $("#" + total).val(compras - instalaciones);
                    } else{
                        $("#" + total).val(parseInt(input.value) + compras - instalaciones); 
                    }
                }
                
                function verifEnter(input, compras, instalaciones, total){
                    if(event.which == 13){
                        actualizarInput(input, compras, instalaciones, total);
                    }
                }
            </script>';

            $ediciones = '<option value="" ';
            if($edicion == ""){ 
                $ediciones .= "selected='selected'";
            }
            $ediciones .= '>Seleccione..</option>';
            if($familia == "Windows"){
                $ediciones .= '<option value="Standard" ';
                if($edicion == "Standard"){ 
                    $ediciones .= "selected='selected'";
                }
                $ediciones .= '>Standard</option>
                <option value="Enterprise" ';
                if($edicion == "Enterprise"){ 
                    $ediciones .= "selected='selected'";
                }
                $ediciones .= '>Enterprise</option>
                <option value="Professional" ';
                if($edicion == "Professional"){ 
                    $ediciones .= "selected='selected'";
                }
                $ediciones .= '>Professional</option>
                <option value="Otros" ';
                if($edicion == "Otros"){ 
                    $ediciones .= "selected='selected'";
                }
                $ediciones .= '>' . $otrosIdioma . '</option>';
            }
            else if($familia == "Office" || $familia == "Project" || $familia == "Visio"){
                    $ediciones .= '<option value="Standard" ';
                    if($edicion == "Standard"){ 
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Standard</option>
                    <option value="Professional" ';
                    if($edicion == "Professional"){ 
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Professional</option>
                    <option value="Otros" ';
                    if($edicion == "Otros"){ 
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Otros</option>';
            }
            else if($familia == "Windows Server" || $familia == "SQL Server"){
                    $ediciones .= '<option value="Standard" ';
                    if($edicion == "Standard"){ 
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Standard</option>';
                    
                    if ($familia == "Windows Server"){
                        $ediciones .= '<option value="Datacenter" ';
                        if($edicion == "Datacenter"){
                            $ediciones .= "selected='selected'";
                        }
                        $ediciones .= '>Datacenter</option>';
                    }
                    
                    $ediciones .= '<option value="Enterprise" ';
                    if($edicion == "Enterprise"){
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Enterprise</option>
                    <option value="Otros" ';
                    if($edicion == "Otros"){
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>' . $otrosIdioma . '</option>';
            }
            else{
                    $ediciones .= '<option value="Visual Studio" ';
                    if($edicion == "Visual Studio"){
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Visual Studio</option>
                    <option value="Exchange Server" ';
                    if($edicion == "Exchange Server"){
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Exchange Server</option>
                    <option value="Sharepoint Server" ';
                    if($edicion == "Sharepoint Server"){
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Sharepoint Server</option>
                    <option value="Skype for Business" ';
                    if($edicion == "Skype for Business"){
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>Skype for Business</option>
                    <option value="System Center" ';
                    if($edicion == "System Center"){
                        $ediciones .= "selected='selected'";
                    }
                    $ediciones .= '>System Center</option>';
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'titulo'=>$titulo, 'compra'=>$clientTotalOfficeCompras, 
            'instalacion'=>$clientTotalOfficeInstal, 'tabla'=>$tabla, 'edicion'=>$ediciones, 'neto'=>$clientOfficeNeto, 
            'obsoleto'=>$clientTotalOfficeInac, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);