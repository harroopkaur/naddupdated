<?php
//inicio agregado VS
if (($agregarVS == 1 && $exitoAgrVS == 1) || ($agregarWindows == 1 && $exitoAgrWindows == 1) || ($agregarSQL == 1 && $exitoAgrSQL == 1)) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registros insertados con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if (($agregarVS == 1 && $exitoAgrVS == 0) || ($agregarWindows == 1 && $exitoAgrWindows == 0) || ($agregarSQL == 1 && $exitoAgrSQL == 0)) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if (($agregarVS == 1 && $exitoAgrVS == 2) || ($agregarSQL == 1 && $exitoAgrSQL == 2) || ($agregarSQL == 1 && $exitoAgrSQL == 2)) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar todos los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
}
//fin agregado VS

//inicio actualizar VS
if (($actualVS == 1 && $exitoActualVS == 1) || ($actualWindows == 1 && $exitoActualWindows == 1) || ($actualSQL == 1 && $exitoActualSQL == 1)) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registros actualizados con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if (($actualVS == 1 && $exitoActualVS == 0) || ($actualWindows == 1 && $exitoActualWindows == 0) || ($actualSQL == 1 && $exitoActualSQL == 0)) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if (($actualVS == 1 && $exitoActualVS == 2) || ($actualWindows == 1 && $exitoActualWindows == 2) || ($actualSQL == 1 && $exitoActualSQL == 2)) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todos los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
}
//fin actualizar VS

//inicio agregado MSDN
if ($agregarMSDN == 1 && $exitoAgrMSDN == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registros insertados con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if ($agregarMSDN == 1 && $exitoAgrMSDN == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if ($agregarMSDN == 1 && $exitoAgrMSDN == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo agregar todos los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
}
//fin agregado MSDN

//inicio actualizar MSDN
if ($actualMSDN == 1 && $exitoActualMSDN == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Registros actualizados con éxito', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if ($actualMSDN == 1 && $exitoActualMSDN == 0) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
} else if ($actualMSDN == 1 && $exitoActualMSDN == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todos los registros', {'Aceptar': 'Aceptar'}, function(button) {
            if (button === 'Aceptar'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/microsoft/desarrolloPrueba.php';
            }
        });
    </script>
<?php
}
//fin actualizar MSDN
?>          
    
<div style="overflow:hidden; padding:20px;">    
    <div style="width:20%; margin:0px; margin-top:55px; padding:0px; overflow:hidden; float:left;">
        <p style="font-weight:bold; font-size:20px; color:blue; margin-top:12px;">Visual Studio</p>
        
        <br>
        <div style="overflow:hidden; background-color:#C1C1C1; font-size:12px;">
            <p style="font-weight:bold">Siga los siguientes pasos, puede asignar Usuarios y Equipos a Licencias de Visual Studio</p>
            <br>
            <p style="font-weight:bold; float:left">Paso 1:</p><p style="float:left">&nbsp;Click botón "Asignar"</p>
            <p style="font-weight:bold; clear:both; float:left">Paso 2:</p><p style="float:left">&nbsp;Escribe el nombre del Usuario y Equipo</p>
            <p style="font-weight:bold; clear:both; float:left">Paso 3:</p><p style="float:left">&nbsp;Click bot&oacute;n "Actualizar"</p>
        </div>
    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; height:auto; overflow:hidden;">
        <div style="overflow:hidden; padding-bottom:20px;">
            <div class="botonesSAM boton5" id="borrarVS" style="float:left;">Borrar</div>
            <div class="botonesSAM boton5" id="asignarVS" style="float:right;">Asignar</div>
            <div class="botonesSAM boton5" id="editarVS" style="float:right;">Editar</div>
            <div class="botonesSAM boton5" id="agregarVS" style="float:right;">Agregar</div>
        </div>
        
        <form id="formVS" name="formVS" method="post" action="desarrolloPrueba.php">
            <input type="hidden" id="tokenVS" name="token">
            <input type="hidden" id="actualVS" name="actualVS" value="1">
            
            <div id="contenedorVS" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                <table id="tablaVS" style="width:1250px; margin-top:-5px;" class="tablap">
                    <thead>
                        <tr>
                            <th class="text-center"><input type="checkbox" id="checkAllVS"></th>
                            <th class="text-center">&nbsp;</th>
                            <th class="text-center">Equipo</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Familia</th>
                            <th class="text-center">Edici&oacute;n</th>
                            <th class="text-center">Versi&oacute;n</th>
                            <th class="text-center">Instalación</th>
                            <th class="text-center">Usuario</th>
                            <th class="text-center">Equipo</th>
                        </tr>
                    </thead>
                    <tbody id="bodyVS">
                        <?php 
                        $j = 1;
                        $i = 0;
                        foreach($listadoVS as $row){
                        ?>
                            <tr id="rowVS<?= $i ?>">
                                <td class="text-center">
                                    <input type="checkbox" id="checkVS<?= $i ?>" name="checkVS[]" value="<?= $row["id"] ?>">
                                    <input type="hidden" id="idVS<?= $i ?>" name="idVS[]" value="<?= $row["id"] ?>">
                                </td>
                                <td id="numVS<?= $i ?>"><?= $j ?></td>
                                <td><?= $row["equipo"] ?></td>
                                <td><?= $row["tipo"] ?></td>
                                <td><?= $row["familia"] ?></td>
                                <td><?= $row["edicion"] ?></td>
                                <td><?= $row["version"] ?></td>
                                <td><?= $general->reordenarFecha($row["fechaInstalacion"], "-", "/") ?></td>
                                <th class="text-center"><input type="text" style="width:100%" id="usuarioVS<?= $i; ?>" name="usuarioVS[]" value="<?= $row["usuario"] ?>" readonly></th>
                                <th class="text-center"><input type="text" style="width:100%" id="equipoVS<?= $i; ?>" name="equipoVS[]" value="<?= $row["equipoUsuario"] ?>" readonly></th>
                            </tr>
                        <?php 
                            $i++;
                            $j++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </form>
        
        <form id="formAgrVS" name="formAgrVS" method="post" action="desarrolloPrueba.php">
            <br>
            <div id="agrVS" style="display:none;">
                <input type="hidden" id="tokenAgrVS" name="token">
                <input type="hidden" id="agrVS" name="agrVS" value="1">
                
                <div id="contenedorAgrVS" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                    <table id="tablaAgrVS" style="width:1250px;" class="tablap">
                        <thead>
                            <tr>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">Equipo</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Familia</th>
                                <th class="text-center">Edici&oacute;n</th>
                                <th class="text-center">Versi&oacute;n</th>
                                <th class="text-center">Instalación</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Equipo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyAgrVS">
                        </tbody>
                    </table>
                </div>
            
                <br>
            </div>
        </form>
        
        <div style="float:left;" class="botonesSAM boton5" id="actualizarVS">Actualizar</div>
    </div>
</div>
       
<br>

<?php 
if(count($listadoMSDN) > 0){
?>
    <div id="divMSDN" style="overflow:hidden; margin-top:-20px; padding:20px;">
        <div style="width:20%; margin:0px; margin-top:55px; padding:0px; overflow:hidden; float:left;">
            <p style="font-weight:bold; font-size:20px; color:blue; margin-top:12px;">Visual MSDN</p>

            <br>
   
            <div style="overflow:hidden; background-color:#C1C1C1; font-size:12px;">
                <p style="font-weight:bold">Siga los siguientes pasos, puede asignar las Licencias MSDN</p>
                <br>
                <p style="font-weight:bold; float:left">Paso 1:</p><p style="float:left">&nbsp;Click botón "Asignar"</p>
                <p style="font-weight:bold; clear:both; float:left">Paso 2:</p><p style="float:left">&nbsp;Asignar los MSDN</p>
                <p style="font-weight:bold; clear:both; float:left">Paso 3:</p><p style="float:left">&nbsp;Click bot&oacute;n "Actualizar"</p>
            </div>
        </div>

        <div style="width:80%; float:left; margin:0px; padding:0px; height:auto; overflow:hidden;">
            <div style="overflow:hidden; margin:0px; padding-bottom:20px;">
                <div class="botonesSAM boton5" id="borrarMSDN" style="float:left;">Borrar</div>
                <div class="botonesSAM boton5" id="asignarMSDN" style="float:right;">Asignar</div>
                <div class="botonesSAM boton5" id="editarMSDN" style="float:right;">Editar</div>
                <div class="botonesSAM boton5" id="agregarMSDN" style="float:right;">Agregar</div>
            </div>
           
            <form id="formMSDN" name="formMSDN" method="post">
                <input type="hidden" id="tokenMSDN" name="token">
                <input type="hidden" id="actualMSDN" name="actualMSDN" value="1">
                
                <div id="contenedorMSDN" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                    <table id="tablaMSDN" style="width:1250px;" class="tablap">
                        <thead>
                            <tr>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center"><input type="checkbox" id="checkAllMSDN"></th>
                                <th class="text-center">Equipo</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Familia</th>
                                <th class="text-center">Edici&oacute;n</th>
                                <th class="text-center">Versi&oacute;n</th>
                                <th class="text-center">Instalación</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Equipo</th>
                                <th class="text-center">MSDN</th>
                            </tr>
                        </thead>
                        <tbody id="bodyMSDN">
                            <?php 
                            $i = 0;
                            $j = 1;
                            foreach($listadoMSDN as $row){
                            ?>
                                <tr id="rowMSDN<?= $i ?>">
                                    <td id="numMSDN<?= $i ?>"><?= $j ?></td>
                                    <td class="text-center">
                                        <input type="checkbox" id="checkMSDN<?= $i ?>" name="checkMSDN[]" value="<?= $row["id"] ?>">
                                        <input type="hidden" id="idMSDN<?= $i ?>" name="idMSDN[]" value="<?= $row["id"] ?>">
                                    </td>
                                    <td><?= $row["equipo"] ?></td>
                                    <td><?= $row["tipo"] ?></td>
                                    <td><?= $row["familia"] ?></td>
                                    <td><?= $row["edicion"] ?></td>
                                    <td><?= $row["version"] ?></td>
                                    <td><?= $general->reordenarFecha($row["fechaInstalacion"], "-", "/") ?></td>
                                    <th class="text-center"><input type="text" style="width:100%" id="usuarioMSDN<?= $i; ?>" name="usuarioMSDN[]" value="<?= $row["usuario"] ?>" readonly></th>
                                    <th class="text-center"><input type="text" style="width:100%" id="equipoMSDN<?= $i; ?>" name="equipoMSDN[]" value="<?= $row["equipoUsuario"] ?>" readonly></th>
                                    <th class="text-center">
                                        <select id="msdn<?= $i ?>" name="msdn[]" disabled="disabled">
                                            <option value="0" <?php if($row["msdn"] == "" || $row["msdn"] == 0){ echo "selected='selected'"; } ?>>0</option>
                                            <option value="1" <?php if($row["msdn"] == 1){ echo "selected='selected'"; } ?>>1</option>
                                        </select>
                                    </th>
                                </tr>
                            <?php 
                                $i++;
                                $j++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </form>
            
            <form id="formAgrMSDN" name="formAgrMSDN" method="post" action="desarrolloPrueba.php">
                <br>
                <div id="agrMSDN" style="display:none;">
                    <input type="hidden" id="tokenAgrMSDN" name="token">
                    <input type="hidden" id="agrMSDN" name="agrMSDN" value="1">

                    <div id="contenedorAgrMSDN" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                        <table id="tablaAgrMSDN" style="width:1250px;" class="tablap">
                            <thead>
                                <tr>
                                    <th class="text-center">&nbsp;</th>
                                    <th class="text-center">&nbsp;</th>
                                    <th class="text-center">Equipo</th>
                                    <th class="text-center">Tipo</th>
                                    <th class="text-center">Familia</th>
                                    <th class="text-center">Edici&oacute;n</th>
                                    <th class="text-center">Versi&oacute;n</th>
                                    <th class="text-center">Instalación</th>
                                    <th class="text-center">Usuario</th>
                                    <th class="text-center">Equipo</th>
                                </tr>
                            </thead>
                            <tbody id="bodyAgrMSDN">
                            </tbody>
                        </table>
                    </div>

                    <br>
                </div>
            </form>
            
            <div style="float:left;" class="botonesSAM boton5" id="actualizarMSDN">Actualizar</div>
        </div>
    </div>

    <br>

<?php 
}
?>

<div style="overflow:hidden; padding:20px;">    
    <div style="width:20%; margin:0px; margin-top:55px; padding:0px; overflow:hidden; float:left;">
        <p style="font-weight:bold; font-size:20px; color:blue; margin-top:12px;">Windows Server MSDN</p>
        
        <br>
        <div style="overflow:hidden; background-color:#C1C1C1; font-size:12px;">
            <p style="font-weight:bold">Siga los siguientes pasos, puede asignar Usuarios y Equipos a Licencias de Windows Server</p>
            <br>
            <p style="font-weight:bold; float:left">Paso 1:</p><p style="float:left">&nbsp;Click botón "Asignar"</p>
            <p style="font-weight:bold; clear:both; float:left">Paso 2:</p><p style="float:left">&nbsp;Escribe el nombre del Usuario y Equipo</p>
            <p style="font-weight:bold; clear:both; float:left">Paso 3:</p><p style="float:left">&nbsp;Click bot&oacute;n "Actualizar"</p>
        </div>
    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; height:auto; overflow:hidden;">
        <div style="overflow:hidden; padding-bottom:20px;">
            <div class="botonesSAM boton5" id="borrarWindows" style="float:left;">Borrar</div>
            <div class="botonesSAM boton5" id="asignarWindows" style="float:right;">Asignar</div>
            <div class="botonesSAM boton5" id="editarWindows" style="float:right;">Editar</div>
            <div class="botonesSAM boton5" id="agregarWindows" style="float:right;">Agregar</div>
        </div>
        
        <form id="formWindows" name="formWindows" method="post" action="desarrolloPrueba.php">
            <input type="hidden" id="tokenWindows" name="token">
            <input type="hidden" id="actualWindows" name="actualWindows" value="1">
            
            <div id="contenedorWindows" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                <table id="tablaWindows" style="width:1250px; margin-top:-5px;" class="tablap">
                    <thead>
                        <tr>
                            <th class="text-center"><input type="checkbox" id="checkAllWindows"></th>
                            <th class="text-center">&nbsp;</th>
                            <th class="text-center">Equipo</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Familia</th>
                            <th class="text-center">Edici&oacute;n</th>
                            <th class="text-center">Versi&oacute;n</th>
                            <th class="text-center">Instalación</th>
                            <th class="text-center">Usuario</th>
                            <th class="text-center">Equipo</th>
                        </tr>
                    </thead>
                    <tbody id="bodyWindows">
                        <?php 
                        $j = 1;
                        $i = 0;
                        foreach($listadoWindows as $row){
                        ?>
                            <tr id="rowWindows<?= $i ?>">
                                <td class="text-center">
                                    <input type="checkbox" id="checkWindows<?= $i ?>" name="checkWindows[]" value="<?= $row["id"] ?>">
                                    <input type="hidden" id="idWindows<?= $i ?>" name="idWindows[]" value="<?= $row["id"] ?>">
                                </td>
                                <td id="numWindows<?= $i ?>"><?= $j ?></td>
                                <td><?= $row["equipo"] ?></td>
                                <td><?= $row["tipo"] ?></td>
                                <td><?= $row["familia"] ?></td>
                                <td><?= $row["edicion"] ?></td>
                                <td><?= $row["version"] ?></td>
                                <td><?= $general->reordenarFecha($row["fechaInstalacion"], "-", "/") ?></td>
                                <th class="text-center"><input type="text" style="width:100%" id="usuarioWindows<?= $i; ?>" name="usuarioWindows[]" value="<?= $row["usuario"] ?>" readonly></th>
                                <th class="text-center"><input type="text" style="width:100%" id="equipoWindows<?= $i; ?>" name="equipoWindows[]" value="<?= $row["equipoUsuario"] ?>" readonly></th>
                            </tr>
                        <?php 
                            $i++;
                            $j++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </form>
        
        <form id="formAgrWindows" name="formAgrWindows" method="post" action="desarrolloPrueba.php">
            <br>
            <div id="agrWindows" style="display:none;">
                <input type="hidden" id="tokenAgrWindows" name="token">
                <input type="hidden" id="agrWindows" name="agrWindows" value="1">
                
                <div id="contenedorAgrWindows" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                    <table id="tablaAgrWindows" style="width:1250px;" class="tablap">
                        <thead>
                            <tr>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">Equipo</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Familia</th>
                                <th class="text-center">Edici&oacute;n</th>
                                <th class="text-center">Versi&oacute;n</th>
                                <th class="text-center">Instalación</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Equipo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyAgrWindows">
                        </tbody>
                    </table>
                </div>
            
                <br>
            </div>
        </form>
        
        <div style="float:left;" class="botonesSAM boton5" id="actualizarWindows">Actualizar</div>
    </div>
</div>
    
<br>

<div style="overflow:hidden; padding:20px;">    
    <div style="width:20%; margin:0px; margin-top:55px; padding:0px; overflow:hidden; float:left;">
        <p style="font-weight:bold; font-size:20px; color:blue; margin-top:12px;">SQL Server MSDN</p>
        
        <br>
        <div style="overflow:hidden; background-color:#C1C1C1; font-size:12px;">
            <p style="font-weight:bold">Siga los siguientes pasos, puede asignar Usuarios y Equipos a Licencias de SQL Server</p>
            <br>
            <p style="font-weight:bold; float:left">Paso 1:</p><p style="float:left">&nbsp;Click botón "Asignar"</p>
            <p style="font-weight:bold; clear:both; float:left">Paso 2:</p><p style="float:left">&nbsp;Escribe el nombre del Usuario y Equipo</p>
            <p style="font-weight:bold; clear:both; float:left">Paso 3:</p><p style="float:left">&nbsp;Click bot&oacute;n "Actualizar"</p>
        </div>
    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; height:auto; overflow:hidden;">
        <div style="overflow:hidden; padding-bottom:20px;">
            <div class="botonesSAM boton5" id="borrarSQL" style="float:left;">Borrar</div>
            <div class="botonesSAM boton5" id="asignarSQL" style="float:right;">Asignar</div>
            <div class="botonesSAM boton5" id="editarSQL" style="float:right;">Editar</div>
            <div class="botonesSAM boton5" id="agregarSQL" style="float:right;">Agregar</div>
        </div>
        
        <form id="formSQL" name="formSQL" method="post" action="desarrolloPrueba.php">
            <input type="hidden" id="tokenSQL" name="token">
            <input type="hidden" id="actualSQL" name="actualSQL" value="1">
            
            <div id="contenedorSQL" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                <table id="tablaSQL" style="width:1250px; margin-top:-5px;" class="tablap">
                    <thead>
                        <tr>
                            <th class="text-center"><input type="checkbox" id="checkAllSQL"></th>
                            <th class="text-center">&nbsp;</th>
                            <th class="text-center">Equipo</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Familia</th>
                            <th class="text-center">Edici&oacute;n</th>
                            <th class="text-center">Versi&oacute;n</th>
                            <th class="text-center">Instalación</th>
                            <th class="text-center">Usuario</th>
                            <th class="text-center">Equipo</th>
                        </tr>
                    </thead>
                    <tbody id="bodySQL">
                        <?php 
                        $j = 1;
                        $i = 0;
                        foreach($listadoSQL as $row){
                        ?>
                            <tr id="rowSQL<?= $i ?>">
                                <td class="text-center">
                                    <input type="checkbox" id="checkSQL<?= $i ?>" name="checkSQL[]" value="<?= $row["id"] ?>">
                                    <input type="hidden" id="idSQL<?= $i ?>" name="idSQL[]" value="<?= $row["id"] ?>">
                                </td>
                                <td id="numSQL<?= $i ?>"><?= $j ?></td>
                                <td><?= $row["equipo"] ?></td>
                                <td><?= $row["tipo"] ?></td>
                                <td><?= $row["familia"] ?></td>
                                <td><?= $row["edicion"] ?></td>
                                <td><?= $row["version"] ?></td>
                                <td><?= $general->reordenarFecha($row["fechaInstalacion"], "-", "/") ?></td>
                                <th class="text-center"><input type="text" style="width:100%" id="usuarioSQL<?= $i; ?>" name="usuarioSQL[]" value="<?= $row["usuario"] ?>" readonly></th>
                                <th class="text-center"><input type="text" style="width:100%" id="equipoSQL<?= $i; ?>" name="equipoSQL[]" value="<?= $row["equipoUsuario"] ?>" readonly></th>
                            </tr>
                        <?php 
                            $i++;
                            $j++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </form>
        
        <form id="formAgrSQL" name="formAgrSQL" method="post" action="desarrolloPrueba.php">
            <br>
            <div id="agrSQL" style="display:none;">
                <input type="hidden" id="tokenAgrSQL" name="token">
                <input type="hidden" id="agrSQL" name="agrSQL" value="1">
                
                <div id="contenedorAgrSQL" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                    <table id="tablaAgrQL" style="width:1250px;" class="tablap">
                        <thead>
                            <tr>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">&nbsp;</th>
                                <th class="text-center">Equipo</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Familia</th>
                                <th class="text-center">Edici&oacute;n</th>
                                <th class="text-center">Versi&oacute;n</th>
                                <th class="text-center">Instalación</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Equipo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyAgrSQL">
                        </tbody>
                    </table>
                </div>
            
                <br>
            </div>
        </form>
        
        <div style="float:left;" class="botonesSAM boton5" id="actualizarSQL">Actualizar</div>
    </div>
</div>

<script>
    var rowVS = <?= count($listadoVS) ?>;
    var rowAgrVS = 0;
    var rowMSDN = <?= count($listadoMSDN) ?>;
    var rowAgrMSDN = 0;
    var rowWindows = <?= count($listadoWindows) ?>;
    var rowAgrWindows = 0;
    var rowSQL = <?= count($listadoSQL) ?>;
    var rowAgrSQL = 0;
    
    $(document).ready(function(){
        $("#tablaVS").tableHeadFixer();
        $("#tablaMSDN").tableHeadFixer();
        $("#tablaWindows").tableHeadFixer();
        $("#tablaSQL").tableHeadFixer();
        
        //inicio procesos VS
        $("#checkAllVS").click(function(){
            if($("#checkAllVS").prop("checked")){
                for(i = 0; i < rowVS; i++){
                $("#checkVS" + i).prop("checked", "checked");
                }

                for(i = 0; i < rowAgrVS; i++){
                    $("#checkAgrVS" + i).prop("checked", "checked");
                }
            } else{
                for(i = 0; i < rowVS; i++){
                $("#checkVS" + i).prop("checked", "");
                }

                for(i = 0; i < rowAgrVS; i++){
                    $("#checkAgrVS" + i).prop("checked", "");
                }
            }
        });
        
        $("#borrarVS").click(function(){
            $("#tokenVS").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formVS")[0]);
            $.ajax({
                type: "POST",
                url: "ajax/eliminarRegistrosVS.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    $("#bodyMSDN").empty();
                    $("#bodyMSDN").append(data[0].tabla);
                    rowMSDN = data[0].cant;
                    
                    if(rowMSDN === 0){
                        $("#divMSDN").hide();
                    }
                }
            })
            .fail(function( jqXHR ){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            j = 1;
            for(i = 0; i < rowVS; i++){
                if($("#checkVS" + i).prop("checked")){
                    $("#rowVS" + i).remove();
                } else if ($("#checkVS" + i).length > 0){
                    $("#numVS" + i).empty();
                    $("#numVS" + i).append(j);
                    j++;
                }
            }
            
            j = 1;
            for(i = 0; i < rowAgrVS; i++){
                if($("#checkAgrVS" + i).prop("checked")){
                    $("#rowAgrVS" + i).remove();
                } else if ($("#checkAgrVS" + i).length > 0){
                    $("#numAgrVS" + i).empty();
                    $("#numAgrVS" + i).append(j);
                    j++;
                }
            }
            
            $("#checkAllVS").prop("checked", "");
            
            if($("#bodyAgrVS tr").length === 0){
                rowAgrVS = 0;
                $("#agrVS").hide();
            }
        });
        
        $("#agregarVS").click(function(){            
            if($("#bodyAgrVS tr").length === 0){
                $("#agrVS").show();
            }
            filaVS = "<tr id='rowAgrVS" + rowAgrVS + "'>";
            filaVS += "<td class='text-center'><input type='checkbox' id='checkAgrVS" + rowAgrVS + "'></td>";
            filaVS += "<td id='numAgrVS" + rowAgrVS + "'>" + (rowAgrVS + 1) + "</td>";
            filaVS += "<td><input type='text' id='servidorVS" + rowAgrVS + "' name='servidorVS[]' maxlength='70' style='width:100px;'></td>";
            filaVS += "<td>\n\
                    <select id='tipoVS" + rowAgrVS + "' name='tipoVS[]' style='width:100px;'>\n\
                        <option value='Cliente'>Cliente</option>\n\
                        <option value='Servidor'>Servidor</option>\n\
                    </select>\n\
                </td>";
            filaVS += "<td>Visual Studio</td>";
            filaVS += '<td><select id="edicionVS' + rowAgrVS + '" name="edicionVS[]" onchange = "selectEdicion(this.value, \'VS\', ' + rowAgrVS + ')"></select></td>';
            filaVS += '<td><select id="versionVS' + rowAgrVS + '" name="versionVS[]"></select></td>';
            filaVS += "<td><input type='text' id='fechaVS" + rowAgrVS + "' name='fechaVS[]' maxlength='15' style='width:100px;' readonly></td>";
            filaVS += "<th class='text-center'><input type='text' style='width:100%' id='usuarioAgrVS" + rowAgrVS + "' name='usuarioVS[]' value='' maxlength='70' readonly></th>";
            filaVS += "<th class='text-center'><input type='text' style='width:100%' id='equipoAgrVS" + rowAgrVS + "' name='equipoVS[]' value='' maxlength='70' readonly></th>";
            filaVS += "</tr>";
            
            $("#bodyAgrVS").append(filaVS);
            
            $.post("ajax/obtenerEdicion.php", { fabricante : 3, producto : 'Visual Studio', token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#edicionVS" + (rowAgrVS - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : "Visual Studio", edicion : "", token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#versionVS" + (rowAgrVS - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $("#servidorVS" + rowAgrVS).focus();
 
            rowAgrVS++;
            $("#fechaVS" + (rowAgrVS - 1)).datepicker();
        });
        
        $("#editarVS").click(function(){
            if(verifAgr("VS")){
                return false;
            }
            
            j = 0;
            for(i = 0; i < rowVS; i++){
                if($("#usuarioVS" + i).val() !== "" && $("#usuarioVS" + i).val() !== undefined && $("#equipoVS" + i).val() !== undefined 
                && $("#equipoVS" + i).val() !== ""){
                    $("#usuarioVS" + i).removeAttr("readonly");
                    $("#equipoVS" + i).removeAttr("readonly");
                    j++;
                }
            }
            
            if(j === 0){
                $.alert.open('alert', "Debe asignar primero los usuarios y equipos", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else{
                $("#contenedorVS").scrollLeft($("#contenedorVS").width());
            }
        });
        
        $("#asignarVS").click(function(){
            if(verifAgr("VS")){
                return false;
            }
            
            $("#contenedorVS").scrollLeft($("#contenedorVS").width());
            for(i = 0; i < rowVS; i++){
                $("#usuarioVS" + i).removeAttr("readonly");
                $("#equipoVS" + i).removeAttr("readonly");
            }
            
            $("#usuarioVS0").focus();
        });
        
        $("#actualizarVS").click(function(){
            if($("#bodyAgrVS tr").length > 0){
                for(i = 0; i < rowAgrVS; i++){
                    if($("#servidorVS" + i).val() === ""){
                        $.alert.open('alert', "Debe llenar el equipo", {'Aceptar' : 'Aceptar'}, function() {
                            $("#servidorVS" + i).focus();
                        });
                        return false;
                    } else if($("#edicionVS" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la edición", {'Aceptar' : 'Aceptar'}, function() {
                            $("#edicionVS" + i).focus();
                        });
                        return false;
                    } else if($("#versionVS" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la versión", {'Aceptar' : 'Aceptar'}, function() {
                            $("#versionVS" + i).focus();
                        });
                        return false;
                    } else if($("#fechaVS" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la fecha", {'Aceptar' : 'Aceptar'}, function() {
                            $("#fechaVS" + i).focus();
                        });
                        return false;
                    }
                }
                $("#formAgrVS").submit();
            }
            else{
                if($("#bodyVS tr").length === 0){
                    location.href = "desarrolloPrueba.php";
                } else {
                    for(i = 0; i < rowVS; i++){
                        if(($("#equipoVS" + i).length > 0 && $("#equipoVS" + i).val() !== "" && $("#usuarioVS" + i).length > 0 && $("#usuarioVS" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el usuario", {'Aceptar' : 'Aceptar'}, function() {
                                $("#usuarioVS" + i).focus();
                            });
                            return false;
                        } else if(($("#usuarioVS" + i).length > 0 && $("#usuarioVS" + i).val() !== "" && $("#equipoVS" + i).length > 0 && $("#equipoVS" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el equipo", {'Aceptar' : 'Aceptar'}, function() {
                                $("#equipoVS" + i).focus();
                            });
                            return false;
                        }
                    }

                    $("#formVS").submit();
                }
            }
        });
        //fin procesos VS
        
        
        //inicio procesos Windows
        $("#checkAllWindows").click(function(){
            if($("#checkAllWindows").prop("checked")){
                for(i = 0; i < rowWindows; i++){
                $("#checkWindows" + i).prop("checked", "checked");
                }

                for(i = 0; i < rowAgrWindows; i++){
                    $("#checkAgrWindows" + i).prop("checked", "checked");
                }
            } else{
                for(i = 0; i < rowWindows; i++){
                $("#checkWindows" + i).prop("checked", "");
                }

                for(i = 0; i < rowAgrWindows; i++){
                    $("#checkAgrWindows" + i).prop("checked", "");
                }
            }
        });
        
        $("#borrarWindows").click(function(){
            $("#tokenWindows").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formWindows")[0]);
            $.ajax({
                type: "POST",
                url: "ajax/eliminarRegistrosWindows.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    $("#bodyWindowsMSDN").empty();
                    $("#bodyWindowsMSDN").append(data[0].tabla);
                    rowWindowsMSDN = data[0].cant;
                    
                    if(rowWindowsMSDN === 0){
                        $("#divWindowsMSDN").hide();
                    }
                }
            })
            .fail(function( jqXHR ){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            j = 1;
            for(i = 0; i < rowWindows; i++){
                if($("#checkWindows" + i).prop("checked")){
                    $("#rowWindows" + i).remove();
                } else if ($("#checkWindows" + i).length > 0){
                    $("#numWindows" + i).empty();
                    $("#numWindows" + i).append(j);
                    j++;
                }
            }
            
            j = 1;
            for(i = 0; i < rowAgrWindows; i++){
                if($("#checkAgrWindows" + i).prop("checked")){
                    $("#rowAgrWindows" + i).remove();
                } else if ($("#checkAgrWindows" + i).length > 0){
                    $("#numAgrWindows" + i).empty();
                    $("#numAgrWindows" + i).append(j);
                    j++;
                }
            }
            
            $("#checkAllWindows").prop("checked", "");
            
            if($("#bodyAgrWindows tr").length === 0){
                rowAgrWindows = 0;
                $("#agrWindows").hide();
            }
        });
        
        $("#agregarWindows").click(function(){            
            if($("#bodyAgrWindows tr").length === 0){
                $("#agrWindows").show();
            }
            filaWindows = "<tr id='rowAgrWindows" + rowAgrWindows + "'>";
            filaWindows += "<td class='text-center'><input type='checkbox' id='checkAgrWindows" + rowAgrWindows + "'></td>";
            filaWindows += "<td id='numAgrWindows" + rowAgrWindows + "'>" + (rowAgrWindows + 1) + "</td>";
            filaWindows += "<td><input type='text' id='servidorWindows" + rowAgrWindows + "' name='servidorWindows[]' maxlength='70' style='width:100px;'></td>";
            filaWindows += "<td>\n\
                    <select id='tipoWindows" + rowAgrWindows + "' name='tipoWindows[]' style='width:100px;'>\n\
                        <option value='Servidor'>Servidor</option>\n\
                    </select>\n\
                </td>";
            filaWindows += "<td>Windows Server</td>";
            filaWindows += '<td><select id="edicionWindows' + rowAgrWindows + '" name="edicionWindows[]" onchange = "selectEdicion(this.value, \'Windows\', ' + rowAgrWindows + ')"></select></td>';
            filaWindows += '<td><select id="versionWindows' + rowAgrWindows + '" name="versionWindows[]"></select></td>';
            filaWindows += "<td><input type='text' id='fechaWindows" + rowAgrWindows + "' name='fechaWindows[]' maxlength='15' style='width:100px;' readonly></td>";
            filaWindows += "<th class='text-center'><input type='text' style='width:100%' id='usuarioAgrWindows" + rowAgrWindows + "' name='usuarioWindows[]' value='' maxlength='70' readonly></th>";
            filaWindows += "<th class='text-center'><input type='text' style='width:100%' id='equipoAgrWindows" + rowAgrWindows + "' name='equipoWindows[]' value='' maxlength='70' readonly></th>";
            filaWindows += "</tr>";
            
            $("#bodyAgrWindows").append(filaWindows);
            
            $.post("ajax/obtenerEdicion.php", { fabricante : 3, producto : 'Windows Server', token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#edicionWindows" + (rowAgrWindows - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : "Windows Server", edicion : "", token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#versionWindows" + (rowAgrWindows - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $("#servidorWindows" + rowAgrWindows).focus();
 
            rowAgrWindows++;
            $("#fechaWindows" + (rowAgrWindows - 1)).datepicker();
        });
        
        $("#editarWindows").click(function(){
            if(verifAgr("Windows")){
                return false;
            }
            
            j = 0;
            for(i = 0; i < rowWindows; i++){
                if($("#usuarioWindows" + i).val() !== "" && $("#usuarioWindows" + i).val() !== undefined && $("#equipoWindows" + i).val() !== undefined 
                && $("#equipoWindows" + i).val() !== ""){
                    $("#usuarioWindows" + i).removeAttr("readonly");
                    $("#equipoWindows" + i).removeAttr("readonly");
                    j++;
                }
            }
            
            if(j === 0){
                $.alert.open('alert', "Debe asignar primero los usuarios y equipos", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else{
                $("#contenedorWindows").scrollLeft($("#contenedorWindows").width());
            }
        });
        
        $("#asignarWindows").click(function(){
            if(verifAgr("Windows")){
                return false;
            }
            
            $("#contenedorWindows").scrollLeft($("#contenedorWindows").width());
            for(i = 0; i < rowWindows; i++){
                $("#usuarioWindows" + i).removeAttr("readonly");
                $("#equipoWindows" + i).removeAttr("readonly");
            }
            
            $("#usuarioWindows0").focus();
        });
        
        $("#actualizarWindows").click(function(){
            if($("#bodyAgrWindows tr").length > 0){
                for(i = 0; i < rowAgrWindows; i++){
                    if($("#servidorWindows" + i).val() === ""){
                        $.alert.open('alert', "Debe llenar el equipo", {'Aceptar' : 'Aceptar'}, function() {
                            $("#servidorWindows" + i).focus();
                        });
                        return false;
                    } else if($("#edicionWindows" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la edición", {'Aceptar' : 'Aceptar'}, function() {
                            $("#edicionWindows" + i).focus();
                        });
                        return false;
                    } else if($("#versionWindows" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la versión", {'Aceptar' : 'Aceptar'}, function() {
                            $("#versionWindows" + i).focus();
                        });
                        return false;
                    } else if($("#fechaWindows" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la fecha", {'Aceptar' : 'Aceptar'}, function() {
                            $("#fechaWindows" + i).focus();
                        });
                        return false;
                    }
                }
                $("#formAgrWindows").submit();
            }
            else{
                if($("#bodyWindows tr").length === 0){
                    location.href = "desarrolloPrueba.php";
                } else {
                    for(i = 0; i < rowWindows; i++){
                        if(($("#equipoWindows" + i).length > 0 && $("#equipoWindows" + i).val() !== "" && $("#usuarioWindows" + i).length > 0 && $("#usuarioWindows" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el usuario", {'Aceptar' : 'Aceptar'}, function() {
                                $("#usuarioWindows" + i).focus();
                            });
                            return false;
                        } else if(($("#usuarioWindows" + i).length > 0 && $("#usuarioWindows" + i).val() !== "" && $("#equipoWindows" + i).length > 0 && $("#equipoWindows" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el equipo", {'Aceptar' : 'Aceptar'}, function() {
                                $("#equipoWindows" + i).focus();
                            });
                            return false;
                        }
                    }

                    $("#formWindows").submit();
                }
            }
        });
        //fin procesos Windows
        
        //inicio procesos SQL
        $("#checkAllSQL").click(function(){
            if($("#checkAllSQL").prop("checked")){
                for(i = 0; i < rowSQL; i++){
                $("#checkSQL" + i).prop("checked", "checked");
                }

                for(i = 0; i < rowAgrSQL; i++){
                    $("#checkAgrSQL" + i).prop("checked", "checked");
                }
            } else{
                for(i = 0; i < rowSQL; i++){
                $("#checkSQL" + i).prop("checked", "");
                }

                for(i = 0; i < rowAgrSQL; i++){
                    $("#checkAgrSQL" + i).prop("checked", "");
                }
            }
        });
        
        $("#borrarSQL").click(function(){
            $("#tokenSQL").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formSQL")[0]);
            $.ajax({
                type: "POST",
                url: "ajax/eliminarRegistrosSQL.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                }
            })
            .fail(function( jqXHR ){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            j = 1;
            for(i = 0; i < rowSQL; i++){
                if($("#checkSQL" + i).prop("checked")){
                    $("#rowSQL" + i).remove();
                } else if ($("#checkSQL" + i).length > 0){
                    $("#numSQL" + i).empty();
                    $("#numSQL" + i).append(j);
                    j++;
                }
            }
            
            j = 1;
            for(i = 0; i < rowAgrSQL; i++){
                if($("#checkAgrSQL" + i).prop("checked")){
                    $("#rowAgrSQL" + i).remove();
                } else if ($("#checkAgrSQL" + i).length > 0){
                    $("#numAgrSQL" + i).empty();
                    $("#numAgrSQL" + i).append(j);
                    j++;
                }
            }
            
            $("#checkAllSQL").prop("checked", "");
            
            if($("#bodyAgrSQL tr").length === 0){
                rowAgrSQL = 0;
                $("#agrSQL").hide();
            }
        });
        
        $("#agregarSQL").click(function(){            
            if($("#bodyAgrSQL tr").length === 0){
                $("#agrSQL").show();
            }
            filaSQL = "<tr id='rowAgrSQL" + rowAgrSQL + "'>";
            filaSQL += "<td class='text-center'><input type='checkbox' id='checkAgrSQL" + rowAgrSQL + "'></td>";
            filaSQL += "<td id='numAgrSQL" + rowAgrSQL + "'>" + (rowAgrSQL + 1) + "</td>";
            filaSQL += "<td><input type='text' id='servidorSQL" + rowAgrSQL + "' name='servidorSQL[]' maxlength='70' style='width:100px;'></td>";
            filaSQL += "<td>\n\
                    <select id='tipoSQL" + rowAgrSQL + "' name='tipoSQL[]' style='width:100px;'>\n\
                        <option value='Servidor'>Servidor</option>\n\
                    </select>\n\
                </td>";
            filaSQL += "<td>SQL Server</td>";
            filaSQL += '<td><select id="edicionSQL' + rowAgrSQL + '" name="edicionSQL[]" onchange = "selectEdicion(this.value, \'SQL\', ' + rowAgrSQL + ')"></select></td>';
            filaSQL += '<td><select id="versionSQL' + rowAgrSQL + '" name="versionSQL[]"></select></td>';
            filaSQL += "<td><input type='text' id='fechaSQL" + rowAgrSQL + "' name='fechaSQL[]' maxlength='15' style='width:100px;' readonly></td>";
            filaSQL += "<th class='text-center'><input type='text' style='width:100%' id='usuarioAgrSQL" + rowAgrSQL + "' name='usuarioSQL[]' value='' maxlength='70' readonly></th>";
            filaSQL += "<th class='text-center'><input type='text' style='width:100%' id='equipoAgrSQL" + rowAgrSQL + "' name='equipoSQL[]' value='' maxlength='70' readonly></th>";
            filaSQL += "</tr>";
            
            $("#bodyAgrSQL").append(filaSQL);
            
            $.post("ajax/obtenerEdicion.php", { fabricante : 3, producto : 'SQL Server', token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#edicionSQL" + (rowAgrSQL - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : "SQL Server", edicion : "", token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#versionSQL" + (rowAgrSQL - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $("#servidorSQL" + rowAgrSQL).focus();
 
            rowAgrSQL++;
            $("#fechaSQL" + (rowAgrSQL - 1)).datepicker();
        });
        
        $("#editarSQL").click(function(){
            if(verifAgr("SQL")){
                return false;
            }
            
            j = 0;
            for(i = 0; i < rowSQL; i++){
                if($("#usuarioSQL" + i).val() !== "" && $("#usuarioSQL" + i).val() !== undefined && $("#equipoSQL" + i).val() !== undefined 
                && $("#equipoSQL" + i).val() !== ""){
                    $("#usuarioSQL" + i).removeAttr("readonly");
                    $("#equipoSQL" + i).removeAttr("readonly");
                    j++;
                }
            }
            
            if(j === 0){
                $.alert.open('alert', "Debe asignar primero los usuarios y equipos", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else{
                $("#contenedorSQL").scrollLeft($("#contenedorSQL").width());
            }
        });
        
        $("#asignarSQL").click(function(){
            if(verifAgr("SQL")){
                return false;
            }
            
            $("#contenedorSQL").scrollLeft($("#contenedorSQL").width());
            for(i = 0; i < rowSQL; i++){
                $("#usuarioSQL" + i).removeAttr("readonly");
                $("#equipoSQL" + i).removeAttr("readonly");
            }
            
            $("#usuarioSQL0").focus();
        });
        
        $("#actualizarSQL").click(function(){
            if($("#bodyAgrSQL tr").length > 0){
                for(i = 0; i < rowAgrSQL; i++){
                    if($("#servidorSQL" + i).val() === ""){
                        $.alert.open('alert', "Debe llenar el equipo", {'Aceptar' : 'Aceptar'}, function() {
                            $("#servidorSQL" + i).focus();
                        });
                        return false;
                    } else if($("#edicionSQL" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la edición", {'Aceptar' : 'Aceptar'}, function() {
                            $("#edicionSQL" + i).focus();
                        });
                        return false;
                    } else if($("#versionSQL" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la versión", {'Aceptar' : 'Aceptar'}, function() {
                            $("#versionSQL" + i).focus();
                        });
                        return false;
                    } else if($("#fechaSQL" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la fecha", {'Aceptar' : 'Aceptar'}, function() {
                            $("#fechaSQL" + i).focus();
                        });
                        return false;
                    }
                }
                $("#formAgrSQL").submit();
            }
            else{
                if($("#bodySQL tr").length === 0){
                    location.href = "desarrolloPrueba.php";
                } else {
                    for(i = 0; i < rowSQL; i++){
                        if(($("#equipoSQL" + i).length > 0 && $("#equipoSQL" + i).val() !== "" && $("#usuarioSQL" + i).length > 0 && $("#usuarioSQL" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el usuario", {'Aceptar' : 'Aceptar'}, function() {
                                $("#usuarioSQL" + i).focus();
                            });
                            return false;
                        } else if(($("#usuarioSQL" + i).length > 0 && $("#usuarioSQL" + i).val() !== "" && $("#equipoSQL" + i).length > 0 && $("#equipoSQL" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el equipo", {'Aceptar' : 'Aceptar'}, function() {
                                $("#equipoSQL" + i).focus();
                            });
                            return false;
                        }
                    }

                    $("#formSQL").submit();
                }
            }
        });
        //fin procesos SQL
        
        //inicio procesos MSDN
        $("#checkAllMSDN").click(function(){
            if($("#checkAllMSDN").prop("checked")){
                for(i = 0; i < rowMSDN; i++){
                $("#checkMSDN" + i).prop("checked", "checked");
                }

                for(i = 0; i < rowAgrMSDN; i++){
                    $("#checkAgrMSDN" + i).prop("checked", "checked");
                }
            } else{
                for(i = 0; i < rowMSDN; i++){
                $("#checkMSDN" + i).prop("checked", "");
                }

                for(i = 0; i < rowAgrMSDN; i++){
                    $("#checkAgrMSDN" + i).prop("checked", "");
                }
            }
        });
        
        $("#borrarMSDN").click(function(){
            $("#tokenMSDN").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formMSDN")[0]);
            $.ajax({
                type: "POST",
                url: "ajax/eliminarRegistrosMSDN.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                }
            })
            .fail(function( jqXHR ){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
            
            j = 1;
            for(i = 0; i < rowMSDN; i++){
                if($("#checkMSDN" + i).prop("checked")){
                    $("#rowMSDN" + i).remove();
                } else if ($("#checkMSDN" + i).length > 0){
                    $("#numMSDN" + i).empty();
                    $("#numMSDN" + i).append(j);
                    j++;
                }
            }
            
            j = 1;
            for(i = 0; i < rowAgrMSDN; i++){
                if($("#checkAgrMSDN" + i).prop("checked")){
                    $("#rowAgrMSDN" + i).remove();
                } else if ($("#checkAgrMSDN" + i).length > 0){
                    $("#numAgrMSDN" + i).empty();
                    $("#numAgrMSDN" + i).append(j);
                    j++;
                }
            }
            
            $("#checkAllMSDN").prop("checked", "");
            
            if($("#bodyAgrMSDN tr").length === 0){
                rowAgrMSDN = 0;
                $("#agrMSDN").hide();
            }
        });
        
        $("#agregarMSDN").click(function(){            
            if($("#bodyAgrMSDN tr").length === 0){
                $("#agrMSDN").show();
            }
            filaMSDN = "<tr id='rowAgrMSDN" + rowAgrMSDN + "'>";
            filaMSDN += "<td class='text-center'><input type='checkbox' id='checkAgrMSDN" + rowAgrMSDN + "'></td>";
            filaMSDN += "<td id='numAgrMSDN" + rowAgrMSDN + "'>" + (rowAgrMSDN + 1) + "</td>";
            filaMSDN += "<td><input type='text' id='servidorMSDN" + rowAgrMSDN + "' name='servidorMSDN[]' maxlength='70' style='width:100px;'></td>";
            filaMSDN += "<td>\n\
                    <select id='tipoMSDN" + rowAgrMSDN + "' name='tipoMSDN[]' style='width:100px;'>\n\
                        <option value='Cliente'>Cliente</option>\n\
                        <option value='Servidor'>Servidor</option>\n\
                    </select>\n\
                </td>";
            filaMSDN += "<td>Visual Studio</td>";
            filaMSDN += '<td><select id="edicionMSDN' + rowAgrMSDN + '" name="edicionMSDN[]" onchange = "selectEdicion(this.value, \'MSDN\', ' + rowAgrMSDN + ')"></select></td>';
            filaMSDN += '<td><select id="versionMSDN' + rowAgrMSDN + '" name="versionMSDN[]"></select></td>';
            filaMSDN += "<td><input type='text' id='fechaMSDN" + rowAgrMSDN + "' name='fechaMSDN[]' maxlength='15' style='width:100px;' readonly></td>";
            filaMSDN += "<th class='text-center'><input type='text' style='width:100%' id='usuarioAgrMSDN" + rowAgrMSDN + "' name='usuarioMSDN[]' value='' maxlength='70' readonly></th>";
            filaMSDN += "<th class='text-center'><input type='text' style='width:100%' id='equipoAgrMSDN" + rowAgrMSDN + "' name='equipoMSDN[]' value='' maxlength='70' readonly></th>";
            filaMSDN += "</tr>";
            
            $("#bodyAgrMSDN").append(filaMSDN);
            
            $.post("ajax/obtenerEdicion.php", { fabricante : 3, producto : 'Visual Studio', token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#edicionMSDN" + (rowAgrMSDN - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : "Visual Studio", edicion : "", token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#versionMSDN" + (rowAgrMSDN - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $("#servidorMSDN" + rowAgrMSDN).focus();
 
            rowAgrMSDN++;
            $("#fechaMSDN" + (rowAgrMSDN - 1)).datepicker();
        });
        
        $("#editarMSDN").click(function(){
            if(verifAgr("MSDN")){
                return false;
            }
            
            j = 0;
            for(i = 0; i < rowMSDN; i++){
                if($("#usuarioMSDN" + i).val() !== "" && $("#usuarioMSDN" + i).val() !== undefined && $("#equipoMSDN" + i).val() !== undefined 
                && $("#equipoMSDN" + i).val() !== ""){
                    //$("#usuarioMSDN" + i).removeAttr("readonly");
                    //$("#equipoMSDN" + i).removeAttr("readonly");
                    $("#msdn" + i).removeAttr("disabled");
                    j++;
                }
            }
            
            if(j === 0){
                $.alert.open('alert', "Debe asignar primero los usuarios y equipos", {'Aceptar' : 'Aceptar'}, function() {
                });
            } else{
                $("#contenedorMSDN").scrollLeft($("#contenedorMSDN").width());
            }
        });
        
        $("#asignarMSDN").click(function(){
            if(verifAgr("MSDN")){
                return false;
            }
            
            $("#contenedorMSDN").scrollLeft($("#contenedorMSDN").width());
            $("#msdn0").focus(); 
            for(i = 0; i < rowMSDN; i++){
                $("#msdn" + i).removeAttr("disabled");
                
                if($("#equipoMSDN" + i).val() === ""){
                    $("#equipoMSDN" + i).removeAttr("readonly");
                    $("#equipoMSDN" + i).focus();
                }
                
                if($("#usuarioMSDN" + i).val() === ""){
                    $("#usuarioMSDN" + i).removeAttr("readonly");
                    $("#usuarioMSDN" + i).focus();
                }
            }
        });
        
        $("#actualizarMSDN").click(function(){
            if($("#bodyAgrMSDN tr").length > 0){
                for(i = 0; i < rowAgrMSDN; i++){
                    if($("#servidorMSDN" + i).val() === ""){
                        $.alert.open('alert', "Debe llenar el equipo", {'Aceptar' : 'Aceptar'}, function() {
                            $("#servidorMSDN" + i).focus();
                        });
                        return false;
                    } else if($("#edicionMSDN" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la edición", {'Aceptar' : 'Aceptar'}, function() {
                            $("#edicionMSDN" + i).focus();
                        });
                        return false;
                    } else if($("#versionMSDN" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la versión", {'Aceptar' : 'Aceptar'}, function() {
                            $("#versionMSDN" + i).focus();
                        });
                        return false;
                    } else if($("#fechaMSDN" + i).val() === ""){
                        $.alert.open('alert', "Debe seleccionar la fecha", {'Aceptar' : 'Aceptar'}, function() {
                            $("#fechaMSDN" + i).focus();
                        });
                        return false;
                    }
                }
                $("#formAgrMSDN").submit();
            }
            else{
                if($("#bodyMSDN tr").length === 0){
                    location.href = "desarrolloPrueba.php";
                } else {
                    for(i = 0; i < rowMSDN; i++){
                        if(($("#equipoMSDN" + i).length > 0 && $("#equipoMSDN" + i).val() !== "" && $("#usuarioMSDN" + i).length > 0 && $("#usuarioMSDN" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el usuario", {'Aceptar' : 'Aceptar'}, function() {
                                $("#usuarioMSDN" + i).focus();
                            });
                            return false;
                        } else if(($("#usuarioMSDN" + i).length > 0 && $("#usuarioMSDN" + i).val() !== "" && $("#equipoMSDN" + i).length > 0 && $("#equipoMSDN" + i).val() === "")){
                            $.alert.open('alert', "No ha asignado el equipo", {'Aceptar' : 'Aceptar'}, function() {
                                $("#equipoMSDN" + i).focus();
                            });
                            return false;
                        }
                    }

                    $("#formMSDN").submit();
                }
            }
        });
        //fin procesos MSDN
    });
    
    function selectEdicion(valor, tipo, indice){
        tipoAux = "";
        if(tipo === "VS" || tipo === "MSDN"){
            tipoAux = "Visual Studio";
        } else if(tipo === "Windows"){
            tipoAux = "Windows Server";
        } else if(tipo === "SQL"){
            tipoAux = "SQL Server";
        }
        
        $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : tipoAux, edicion : valor, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#version"+tipo+indice).empty();
            $("#version"+tipo+indice).append(data[0].option);
        }, 'json')
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function verifAgr(tipo){
        result = false;
        if(tipo === "VS" && $("#bodyAgrVS tr").length > 0){
            $.alert.open('alert', "Debe actualizar los registros agregados en Visual Studio", {'Aceptar' : 'Aceptar'}, function() {
            });
            result = true;
        } else if(tipo === "Windows" && $("#bodyArgWindows").length > 0){
            $.alert.open('alert', "Debe actualizar los registros agregados en Windows Server", {'Aceptar' : 'Aceptar'}, function() {
            });
            result = true;
        } else if(tipo === "MSDN" && $("#bodyArgMSDN").length > 0){
            $.alert.open('alert', "Debe actualizar los registros agregados en MSDN", {'Aceptar' : 'Aceptar'}, function() {
            });
            result = true;
        }
        return result;
    }
</script>