<div style="overflow:hidden; padding:20px;">
    <div style="width:20%; margin:0px; height:100%; padding:0px; overflow:hidden; float:left;">
        <span style="color:#9F9F9F;">Seleccione el Producto</span><br><br> 
        <div>Familia: <strong style="color:#000; font-weight:bold;">
                <?php
                switch ($vert) {
                    case 0: echo 'MS Windows Server';
                        break;
                    case 1: echo 'MS SQL';
                        break;
                }
                ?>
            </strong>
            <br> 

            <select onchange="MM_jumpMenu('self', this, 0)">
                <option value="servidores.php" selected>Seleccione..</option>
                <option value="servidores.php?vert=0">MS Windows Server</option>
                <option value="servidores.php?vert=1">MS SQL</option>
            </select>

        </div><br><br>

        <?php 
        if($existeAlineacionFisico){
        ?>
            <form id="formExportarFisico" name="formExportarFisico" method="post" action="reportes/excelMicrosoftServidor.php">
                <input type="hidden" id="vertExportarFisico" name="vert" value="<?php echo $vert; ?>">
                <input type="hidden" id="tipoTablaFisico" name="tipoTabla" value="Fisico">
                <input type="submit" id="exportarFisico" name="exportar" style="display:none">
                <div class="botones_m2 boton1" id="exportFisico">Exportar Excel</div>
            </form>
        <?php 
        }
        ?>

        <br style="clear:both;"><br>
        <div style="overflow:hidden; background-color:#C1C1C1; font-size:12px;">
            <p style="font-weight:bold">Asignación de Licencias</p>
            <p style="font-weight:bold; float:left">Paso 1:</p><p style="float:left">&nbsp;Click bot&oacute;n "Asignar"</p><br>
            <p style="font-weight:bold; clear:both; float:left">Paso 2:</p><p style="float:left">&nbsp;Escribir el n&uacute;mero de licencias</p>
            <p style="font-weight:bold; clear:both; float:left">Paso 3:</p><p style="float:left">&nbsp;Click bot&oacute;n "Actualizar"</p>
            <br style="clear:both; float:left;"><br>
            <p style="font-weight:bold">Editar / Borrar / Agregar</p>
            <p style="font-weight:bold; float:left">Editar:</p><p style="float:left">&nbsp;Habilita la opci&oacute;n de Edici&oacute;n</p>
            <p style="font-weight:bold; clear:both; float:left">Borrar:<Paso/p><p style="float:left">&nbsp;Habilita la opci&oacute;n de Borrar una l&iacute;nea espec&iacute;fica</p>
            <p style="font-weight:bold; clear:both; float:left">Agregar:</p><p style="float:left">&nbsp;Habilita la opci&oacute;n de Agregar una l&iacute;nea espec&iacute;fica</p>
        </div>
    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; height:auto; overflow:hidden;">

        <br style="clear:both;"><br><br>
         <p style="font-weight:bold;">
            <?php 
            if($vert == 0){
                echo "Windows Server F&iacute;sico";
            }
            else{
                echo "SQL Server F&iacute;sico";
            }
            ?>
        </p>
        <br>

        <div style="float:left;" class="botonesSAM boton5" id="borrarFisico">Borrar</div>
        <div style="float:right;" class="botonesSAM boton5" id="asignarFisico">Asignar</div>
        <div style="float:right;" class="botonesSAM boton5" id="editarFisico">Editar</div>
        <div style="float:right;" class="botonesSAM boton5" id="agregarFisico">Agregar</div>

        <br style="clear:both;"><br>

        <form id="formFisico" name="formFisico" method="post">
            <input type="hidden" id="vertFisico" name="vert" value="<?= $vert; ?>">
            <input type="hidden" id="tokenFisico" name="token">
            <div id="contenedorFisico" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                <table id="tablaFisico" style="width:1752px;">
                    <thead>
                        <tr>
                            <th style="background-color:#C1C1C1; text-align:center; border-left: 1px solid #000000; border-top: 1px solid #000000;" colspan="2">&nbsp;</th>
                            <th style="background-color:#C1C1C1; text-align:center; border-top: 1px solid #000000;" colspan="7">&nbsp;</th>
                            <th style="background-color:#C1C1C1; text-align:center; border: 1px solid #000000;" colspan="4"><p style="font-weight:bold;">Asignaci&oacute;n de Licencias</p></th>
                        </tr>
                        <tr style="background-color:#C1C1C1; border: 1px solid #000000;">
                            <th style="text-align:center; border-left: 1px solid #000000; border-right: 1px solid #000000;"><input type="checkbox" id="checkAllFisico" name="checkAllFisico"></th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Nombre</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Familia</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Edici&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Versi&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">MSDN</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Tipo</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Centro de Costos</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Procesadores</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Cores</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Srv/CAL</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Proc</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Core</th>
                        </tr>
                    </thead>
                    <tbody id="bodyFisico" style="border-left: 1px solid #000000">
                        <?php 
                        $i = 0;
                        foreach($tablaFisico as $row){
                            $versiones = $moduloServidores->obtenerVersionEquivalencia(3, $row["familia"], $row["edicion"]);
                        ?>
                            <input type="hidden" id="edicionFisicoSelected<?= $i; ?>" name="edicionFisicoSelected[]" value="<?= $row["edicion"]; ?>">
                            <input type="hidden" id="versionFisicoSelected<?= $i; ?>" name="versionFisicoSelected[]" value="<?= $row["version"]; ?>">
                            <input type="hidden" id="MSDNFisicoSelected<?= $i; ?>" name="MSDNFisicoSelected[]" value="<?= $row["MSDN"]; ?>">
                            <tr style="border-bottom: 1px solid #ddd" id="rowFisico<?= $i; ?>">
                                <td><input type="checkbox" id="checkFisico<?= $i; ?>" name="checkFisico[]" value="<?= $i ?>"></td>
                                <td><input type="text" id="nombreFisico<?= $i; ?>" name="nombreFisico[]" value="<?= $row["equipo"] ?>" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="familiaFisico<?= $i; ?>" name="familiaFisico[]" value="<?= $row["familia"] ?>" style="background-color:#C1C1C1;" readonly></td>
                                <td>
                                    <select id="edicionFisico<?= $i; ?>" name="edicionFisico[]" onchange="selectEdicion(this.value, 'Fisico', <?= $i; ?>)" disabled>
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        foreach($ediciones as $row1){
                                        ?>
                                        <option value="<?= $row1["nombre"]; ?>" <?php if($row["edicion"] == $row1["nombre"]){ echo "selected='selected'"; } ?>><?= $row1["nombre"]; ?></option>
                                        <?php    
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select id="versionFisico<?= $i; ?>" name="versionFisico[]" onchange="selectVersion(this.value, 'Fisico', <?= $i; ?>)" disabled>
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        foreach($versiones as $row1){
                                        ?>
                                        <option value="<?= $row1["nombre"]; ?>" <?php if($row["version"] == $row1["nombre"]){ echo "selected='selected'"; } ?>><?= $row1["nombre"]; ?></option>
                                        <?php    
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select id="MSDNFisico<?= $i; ?>" name="MSDNFisico[]" onchange="selectMSDN(this.value, 'Fisico', <?= $i; ?>)" disabled>
                                        <option value="No" <?php if($row["MSDN"] == "No"){ echo "selected='selected'"; } ?>>No</option>
                                        <option value="Si" <?php if($row["MSDN"] == "Si"){ echo "selected='selected'"; } ?>>Si</option>
                                    </select>
                                </td>
                                <td><input type="text" id="tipoFisico<?= $i; ?>" name="tipoFisico[]" value="<?= $row["tipo"] ?>" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="ccFisico<?= $i; ?>" name="ccFisico[]" value="<?= $row["centroCosto"] ?>" readonly></td>
                                <td><input type="text" style="text-align:center" id="procesadoresFisico<?= $i; ?>" name="procesadoresFisico[]" value="<?= $row["cpu"]  ?>" onclick="selProcCores('procesadoresFisico', <?= $i ?>)" readonly></td>
                                <td><input type="text" style="text-align:center" id="coresFisico<?= $i; ?>" name="coresFisico[]" value="<?= $row["cores"] ?>" onclick="selProcCores('coresFisico', <?= $i ?>)" readonly></td>
                                <td><input type="text" style="text-align:center" id="licSrvFisico<?= $i; ?>" name="licSrvFisico[]" value="<?= $row["licSrv"] ?>" onclick="seleccionarFisico('licSrvFisico', <?= $i ?>)" readonly></td>
                                <td><input type="text" style="text-align:center" id="licProcFisico<?= $i; ?>" name="licProcFisico[]" value="<?= $row["licProc"] ?>" onclick="seleccionarFisico('licProcFisico', <?= $i ?>)" readonly></td>
                                <td><input type="text" style="text-align:center" id="licCoreFisico<?= $i; ?>" name="licCoreFisico[]" value="<?= $row["licCore"] ?>" onclick="seleccionarFisico('licCoreFisico', <?= $i ?>)" readonly></td>
                            </tr>
                        <?php 
                            echo '<script>
                                $("#procesadoresFisico' . $i . '").numeric(false);
                                $("#coresFisico' . $i . '").numeric(false);
                                $("#licSrvFisico' . $i . '").numeric(false);
                                $("#licProcFisico' . $i . '").numeric(false);
                                $("#licCoreFisico' . $i . '").numeric(false);
                            </script>';
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </form>

        <div style="float:left;" class="botonesSAM boton5" id="actualizarFisico">Actualizar</div>
        <div style="float:left;" class="botonesSAM boton5" id="descargar">Descargar</div>
        
        <form id="formCargarFisico" name="formCargarFisico" method="post">
            <input type="hidden" id="vertCargarFisico" name="vert" value="<?= $vert; ?>">
            <input type="hidden" id="tokenCargarFisico" name="token">
            <input type="file" id="fileFisico" name="fileFisico" class="hide" accept=".csv">
        </form>
        <div style="float:left;" class="botonesSAM boton5" id="cargarFisico">Cargar</div>&nbsp;&nbsp;<span class="bold">Total Servidores Físicos: </span>&nbsp;<span><?= count($tablaFisico) ?></span>
    </div>
</div>

<input type="hidden" id="alinear" name="alinear" value="false">
<div style="overflow:hidden; padding:20px;">
    <div style="width:20%; margin:0px; padding:0px; height:100%; overflow:hidden; float:left;">
        <br><br>

        <?php 
        if($existeAlineacionVirtual){
        ?>
            <form id="formExportarVirtual" name="formExportarVirtual" method="post" action="reportes/excelMicrosoftServidor.php">
                <input type="hidden" id="vertExportarVirtual" name="vert" value="<?php echo $vert; ?>">
                <input type="hidden" id="tipoTablaVirtual" name="tipoTabla" value="Virtual">
                <input type="submit" id="exportarVirtual" name="exportar" style="display:none">
                <div class="botones_m2 boton1" id="exportVirtual">Exportar Excel</div>
            </form>
        <?php 
        }
        ?>

        <br style="clear:both;"><br>
        <div style="overflow:hidden; background-color:#C1C1C1; font-size:12px;">
            <p style="font-weight:bold">Alinear Host y Cluster</p>
            <p style="float:left; font-weight:bold">Paso 1:</p><p style="float:left;">&nbsp;Ingresar</p><p style="float:left; font-weight: bold;">&nbsp;Cluster y Host</p><p style="float:left;">&nbsp;para cada servidor virtual</p>
            <p style="float:left; clear:both; font-weight:bold">Paso 2:</p><p style="float:left;">&nbsp;Click bot&oacute;n "Alinear"</p>
            <br style="clear:both;"><br>
            <p style="font-weight:bold">Asignaci&oacute;n de Licencias</p>
            <p style="float:left; font-weight:bold">Paso 1:</p><p style="float:left;">&nbsp;Click bot&oacute;n "Asignar"</p>
            <p style="float:left; clear:both; font-weight:bold">Paso 2:</p><p style="float:left;">&nbsp;Escribir el n&uacute;mero de Edici&oacute;n, Versi&oacute;n, Procesadores y Cores</p>
            <p style="float:left; clear:both; font-weight:bold">Paso 3:</p><p style="float:left;">&nbsp;Escribir el n&uacute;mero de licencias</p>
            <p style="float:left; clear:both; font-weight:bold">Paso 4:</p><p style="float:left;">&nbsp;Click bot&oacute;n "Actualizar"</p>
            <br style="clear:both; float:left;"><br>
            <p style="font-weight:bold">Editar / Borrar / Agregar</p>
            <p style="font-weight:bold; float:left">Editar:</p><p style="float:left">&nbsp;Habilita la opci&oacute;n de Edici&oacute;n</p>
            <p style="font-weight:bold; clear:both; float:left">Borrar:</p><p style="float:left">&nbsp;Habilita la opci&oacute;n de Borrar una l&iacute;nea espec&iacute;fica</p>
            <p style="font-weight:bold; clear:both; float:left">Agregar:</p><p style="float:left">&nbsp;Habilita la opci&oacute;n de Agregar una l&iacute;nea espec&iacute;fica</p>
        </div>
    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; height:auto; overflow:hidden;">

        <br style="clear:both;"><br><br>
         <p style="font-weight:bold;">
            <?php 
            if($vert == 0){
                echo "Windows Server Virtual";
            }
            else{
                echo "SQL Server Virtual";
            }
            ?>
        </p>
        <br>

        <div style="float:left;" class="botonesSAM boton5" id="alinearVirtual">Alinear</div>
        <div style="float:left;" class="botonesSAM boton5" id="borrarVirtual">Borrar</div>
        <div style="float:right;" class="botonesSAM boton5" id="asignarVirtual">Asignar</div>
        <div style="float:right;" class="botonesSAM boton5" id="editarVirtual">Editar</div>
        <div style="float:right;" class="botonesSAM boton5" id="agregarVirtual">Agregar</div>

        <br style="clear:both;"><br>

        <form id="formVirtual" name="formVirtual" method="post">
            <input type="hidden" id="vertVirtual" name="vert" value="<?= $vert; ?>">
            <input type="hidden" id="tokenVirtual" name="token">
            <div id="contenedorVirtual" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">
                <table id="tablaVirtual" style="width:2000px;">
                    <thead>
                        <tr>
                            <th style="background-color:#C1C1C1; text-align:center; border: 1px solid #000000;" colspan="3"><p style="font-weight:bold;">Alineaci&oacute;n</p></th>
                            <th style="background-color:#C1C1C1; text-align:center; border: 1px solid #000000;" 
                            <?php if($existeAlineacionVirtual == 0){ echo 'colspan="9"'; } else{ echo 'colspan="8"'; } ?>>&nbsp;</th>
                            <th style="background-color:#C1C1C1; text-align:center; border: 1px solid #000000;" colspan="3"><p style="font-weight:bold;">Asignaci&oacute;n de Licencias</p></th>
                        </tr>
                        <tr style="background-color:#C1C1C1; border: 1px solid #000000;">
                            <th class="headcol" style="text-align:center; border-left: 1px solid #000000; border-right: 1px solid #000000;"><input type="checkbox" id="checkAllVirtual" name="checkAllVirtual"></th>
                            <th class="headcol" style="text-align:center; border-left: 1px solid #000000; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #000000;">Cluster</th>
                            <th class="headcol" style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #000000; border-bottom: 1px solid #000000;">Host</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Nombre</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Familia</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Edici&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Versi&oacute;n</th>
                            <?php if($existeAlineacionVirtual == 0){ ?>
                                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">MSDN</th>
                            <?php } ?>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Tipo</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Centro de Costos</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Procesadores</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Cores</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Srv/CAL</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Proc</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Core</th>
                        </tr>
                    </thead>
                    <tbody id="bodyVirtual" style="border-left: 1px solid #000000">
                        <?php 
                        $j = 0;

                        $clusterAux = "";

                        foreach($tablaVirtual as $row){
                            if($clusterAux != $row["cluster"] && $existeAlineacionVirtual > 0){
                            ?>
                            <input type="hidden" id="edicionVirtualSelected<?= $j; ?>" name="edicionVirtualSelected[]" value="<?= $row["edicion"]; ?>">
                            <input type="hidden" id="versionVirtualSelected<?= $j; ?>" name="versionVirtualSelected[]" value="<?= $row["version"]; ?>">
                            <input type="hidden" id="MSDNVirtualSelected<?= $j; ?>" name="MSDNVirtualSelected[]" value="<?= $row["MSDN"]; ?>">
                            <tr style="border-bottom: 1px solid #ddd" id="rowVirtual<?= $j ?>">
                                <td><input type="checkbox" id="checkVirtual<?= $j; ?>" name="checkVirtual[]" value="<?= $j ?>"></td>
                                <td><input type="text" id="clusterVirtual<?= $j; ?>" name="clusterVirtual[]" value="<?= $row["cluster"] ?>" <?php if($existeAlineacionVirtual > 0){ echo "style='background-color:#C1C1C1;' readonly"; } ?>></td>
                                <td><input type="text" id="hostVirtual<?= $j; ?>" name="hostVirtual[]" value="" <?php if($existeAlineacionVirtual > 0){ echo "style='background-color:#C1C1C1;' readonly"; } ?>></td>
                                <td><input type="text" id="nombreVirtual<?= $j; ?>" name="nombreVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="familiaVirtual<?= $j; ?>" name="familiaVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="edicionVirtual<?= $j; ?>" name="edicionVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="versionVirtual<?= $j; ?>" name="versionVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="tipoVirtual<?= $j; ?>" name="tipoVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="ccVirtual<?= $j; ?>" name="ccVirtual[]" value="" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="procesadoresVirtual<?= $j; ?>" name="procesadoresVirtual[]" value="" onclick="selProcCores('procesadoresVirtual', <?= $j ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="coresVirtual<?= $j; ?>" name="coresVirtual[]" value="" onclick="selProcCores('coresVirtual', <?= $j ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licSrvVirtual<?= $j; ?>" name="licSrvVirtual[]" value="" onclick="seleccionar('licSrvVirtual', <?= $j; ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licProcVirtual<?= $j; ?>" name="licProcVirtual[]" value="" onclick="seleccionar('licProcVirtual', <?= $j; ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; background-color:#C1C1C1;" id="licCoreVirtual<?= $j; ?>" name="licCoreVirtual[]" value="" onclick="seleccionar('licCoreVirtual', <?= $j; ?>)" readonly></td>
                            </tr>
                        <?php 
                            echo '<script>
                                $("#procesadoresVirtual' . $j . '").numeric(false);
                                $("#coresVirtual' . $j . '").numeric(false);
                                $("#licSrvVirtual' . $j . '").numeric(false);
                                $("#licProcVirtual' . $j . '").numeric(false);
                                $("#licCoreVirtual' . $j . '").numeric(false);
                            </script>';
                            }
                            else{
                        ?>
                            <input type="hidden" id="edicionVirtualSelected<?= $j; ?>" name="edicionVirtualSelected[]" value="<?= $row["edicion"]; ?>">
                            <input type="hidden" id="versionVirtualSelected<?= $j; ?>" name="versionVirtualSelected[]" value="<?= $row["version"]; ?>">
                            <input type="hidden" id="MSDNVirtualSelected<?= $j; ?>" name="MSDNVirtualSelected[]" value="<?= $row["MSDN"]; ?>">
                            <tr style="border-bottom: 1px solid #ddd" id="rowVirtual<?= $j ?>">
                                <td><input type="checkbox" id="checkVirtual<?= $j; ?>" name="checkVirtual[]" value="<?= $j ?>"></td>
                                <td><input type="text" id="clusterVirtual<?= $j; ?>" name="clusterVirtual[]" value="<?= $row["cluster"] ?>" <?php if($existeAlineacionVirtual > 0){ echo "style='background-color:#C1C1C1;' readonly"; } ?>></td>
                                <td><input type="text" id="hostVirtual<?= $j; ?>" name="hostVirtual[]" value="<?= $row["host"] ?>" <?php if($existeAlineacionVirtual > 0){ echo "style='background-color:#C1C1C1;' readonly"; } ?>></td>
                                <td><input type="text" id="nombreVirtual<?= $j; ?>" name="nombreVirtual[]" value="<?= $row["equipo"] ?>" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="familiaVirtual<?= $j; ?>" name="familiaVirtual[]" value="<?= $row["familia"] ?>" style="background-color:#C1C1C1;" readonly></td>
                                <td>
                                    <select id="edicionVirtual<?= $j; ?>" name="edicionVirtual[]" onchange="selectEdicion(this.value, 'Virtual', <?=  $j; ?>)" disabled>
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        foreach($ediciones as $row1){
                                        ?>
                                        <option value="<?= $row1["nombre"]; ?>" <?php if($row["edicion"] == $row1["nombre"]){ echo "selected='selected'"; } ?>><?= $row1["nombre"]; ?></option>
                                        <?php    
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select id="versionVirtual<?= $j; ?>" name="versionVirtual[]" onchange="selectVersion(this.value, 'Virtual', <?= $j; ?>)" disabled>
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        $versiones = $moduloServidores->obtenerVersionEquivalencia(3, $row["familia"], $row["edicion"]);
                                        foreach($versiones as $row1){
                                        ?>
                                        <option value="<?= $row1["nombre"]; ?>" <?php if($row["version"] == $row1["nombre"]){ echo "selected='selected'"; } ?>><?= $row1["nombre"]; ?></option>
                                        <?php    
                                        }
                                        ?>
                                    </select>
                                </td>
                                <?php if($existeAlineacionVirtual == 0){ ?>
                                    <td>
                                        <select id="MSDNVirtual<?= $j; ?>" name="MSDNVirtual[]" onchange="selectMSDN(this.value, 'Virtual', <?= $j; ?>)">
                                            <option value="No" <?php if($row["MSDN"] == "No"){ echo "selected='selected'"; } ?>>No</option>
                                            <option value="Si" <?php if($row["MSDN"] == "Si"){ echo "selected='selected'"; } ?>>Si</option>
                                        </select>
                                    </td>
                                <?php } ?>
                                <td><input type="text" id="tipoVirtual<?= $j; ?>" name="tipoVirtual[]" value="<?= $row["tipo"] ?>" style="background-color:#C1C1C1;" readonly></td>
                                <td><input type="text" id="ccVirtual<?= $j; ?>" name="ccVirtual[]" value="<?= $row["centroCosto"] ?>" style="<?php if($row["equipo"] != ""){ echo ' background-color:#C1C1C1;'; }?>" readonly></td>
                                <td><input type="text" style="text-align:center; <?php if($row["equipo"] != ""){ echo ' background-color:#C1C1C1;'; }?>" id="procesadoresVirtual<?= $j; ?>" name="procesadoresVirtual[]" value="<?= $row["cpu"]  ?>" onclick="selProcCores('procesadoresVirtual', <?= $j ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; <?php if($row["equipo"] != ""){ echo ' background-color:#C1C1C1;'; }?>" id="coresVirtual<?= $j; ?>" name="coresVirtual[]" value="<?= $row["cores"] ?>" onclick="selProcCores('coresVirtual', <?= $j ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; <?php if($row["equipo"] != ""){ echo ' background-color:#C1C1C1;'; }?>" id="licSrvVirtual<?= $j; ?>" name="licSrvVirtual[]" value="<?= $row["licSrv"] ?>" onclick="seleccionar('licSrvVirtual', <?= $j; ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; <?php if($row["equipo"] != ""){ echo ' background-color:#C1C1C1;'; }?>" id="licProcVirtual<?= $j; ?>" name="licProcVirtual[]" value="<?= $row["licProc"] ?>" onclick="seleccionar('licProcVirtual', <?= $j; ?>)" readonly></td>
                                <td><input type="text" style="text-align:center; <?php if($row["equipo"] != ""){ echo ' background-color:#C1C1C1;'; }?>" id="licCoreVirtual<?= $j; ?>" name="licCoreVirtual[]" value="<?= $row["licCore"] ?>" onclick="seleccionar('licCoreVirtual', <?= $j; ?>)" readonly></td>
                            </tr>
                            <input type="hidden" id="block<?php echo $j;?>" value="<?php if($row["equipo"] != ""){ echo 'true'; }else{ echo 'false'; }?>">
                        <?php 
                            echo '<script>
                                $("#procesadoresVirtual' . $j . '").numeric(false);
                                $("#coresVirtual' . $j . '").numeric(false);
                                $("#licSrvVirtual' . $j . '").numeric(false);
                                $("#licProcVirtual' . $j . '").numeric(false);
                                $("#licCoreVirtual' . $j . '").numeric(false);
                            </script>';
                            }
                            $j++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </form>

        <form id="formSinAlinear" name="formSinAlinear" method="post">
            <input type="hidden" id="vertVirtualSinAlinear" name="vert" value="<?= $vert; ?>">
            <input type="hidden" id="tokenVirtualSinAlinear" name="token">
            <div id="contenedorVirtualSinAlinear" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto; display:none;">
               <br>
                <table id="tablaVirtualSinAlinear" style="width:2000px;">
                    <thead>
                        <tr>
                            <th style="background-color:#C1C1C1; text-align:center; border: 1px solid #000000;" colspan="3"><p style="font-weight:bold;">Alineaci&oacute;n</p></th>
                            <th style="background-color:#C1C1C1; text-align:center; border: 1px solid #000000;" colspan="9">&nbsp;</th>
                            <th style="background-color:#C1C1C1; text-align:center; border: 1px solid #000000;" colspan="3"><p style="font-weight:bold;">Asignaci&oacute;n de Licencias</p></th>
                        </tr>
                        <tr style="background-color:#C1C1C1; border: 1px solid #000000;">
                            <th class="headcol" style="text-align:center; border-left: 1px solid #000000; border-right: 1px solid #000000;"><input type="checkbox" id="checkAllVirtualSinAlinear" name="checkAllVirtualSinAlinear"></th>
                            <th class="headcol" style="text-align:center; border-left: 1px solid #000000; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #000000;">Cluster</th>
                            <th class="headcol" style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #000000; border-bottom: 1px solid #000000;">Host</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Nombre</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Familia</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Edici&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Versi&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">MSDN</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Tipo</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Centro de Costos</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Procesadores</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Cores</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Srv/CAL</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Proc</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Lic Core</th>
                        </tr>
                    </thead>
                    <tbody id="bodyVirtualSinAlinear" style="border-left: 1px solid #000000">
                    </tbody>
                </table>
            </div>
        </form>

        <div style="float:left;" class="botonesSAM boton5" id="actualizarVirtual">Actualizar</div>
        <div style="float:left;" class="botonesSAM boton5" id="descargarVirtual">Descargar</div>
        
        <form id="formCargarVirtual" name="formCargarVirtual" method="post">
            <input type="hidden" id="vertCargarVirtual" name="vert" value="<?= $vert; ?>">
            <input type="hidden" id="tokenCargarVirtual" name="token">
            <input type="file" id="fileVirtual" name="fileVirtual" class="hide" accept=".csv">
        </form>
        <div style="float:left;" class="botonesSAM boton5" id="cargarVirtual">Cargar</div>&nbsp;&nbsp;<span class="bold">Total Servidores Virtuales: </span>&nbsp;<span><?= count($tablaVirtual) ?></span>
    </div>
</div>

<?php 
if($mostrarBalanza){
?>
    <div style="overflow:hidden;">
        <div style="width:20%; margin:0px; padding:0px; height:100%; overflow:hidden; float:left;">
            <span style="color:#9F9F9F; visibility:hidden;">Seleccione el Producto</span><br><br> 
            <div style="visibility:hidden;">Familia: <strong style="color:#000; font-weight:bold;">
                    <?php
                    switch ($vert) {
                        case 0: echo 'MS Windows Server';
                            break;
                        case 1: echo 'MS SQL';
                            break;
                    }
                    ?>
                </strong>
                <br> 

                <select onchange="MM_jumpMenu('self', this, 0)">
                    <option value="<?= $GLOBALS['domain_root'] ?>/servidores.php" selected>Seleccione..</option>
                    <option value="<?= $GLOBALS['domain_root'] ?>/servidores.php?vert=0">MS Windows Server</option>
                    <option value="<?= $GLOBALS['domain_root'] ?>/servidores.php?vert=1">MS SQL</option>
                </select>
            </div>

            <form id="formExportarBalanza" name="formExportarBalanza" method="post" action="reportes/excelMicrosoftServidor.php">
                <input type="hidden" id="vertExportarBalanza" name="vert" value="<?php echo $vert; ?>">
                <input type="hidden" id="tipoTablaBalanza" name="tipoTabla" value="Balanza">
                <input type="submit" id="exportarBalanza" name="exportar" style="display:none">
                <div class="botones_m2 boton1" id="exportBalanza">Exportar Excel</div>
            </form>
        </div>

        <div style="width:80%; float:left; margin:0px; padding:0px; height:auto; overflow:hidden;">

            <br style="clear:both;"><br><br>
             <p style="font-weight:bold;">
                <?php 
                if($vert == 0){
                    echo "Windows Server Balanza";
                }
                else{
                    echo "SQL Server Balanza";
                }
                ?>
            </p>
            <br>

            <div id="contenedorBalanza" style="width:100%; max-height:400px; overflow-x:scroll; overflow-y:auto;">

                <table id="tablaBalanza" style="width:100%;">
                    <thead>
                        <tr style="background-color:#C1C1C1; border: 1px solid #000000;">
                            <th style="text-align:center; border-left: 1px solid #000000; border-right: 1px solid #FFFFFF;">Producto</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #000000; cursor:pointer;" onclick="ordenar(\'nombre\', $(\'#direccion\').val())">Edici&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #000000; cursor:pointer;" onclick="ordenar(\'numero\', $(\'#direccion\').val())">Versi&oacute;n</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #000000; cursor:pointer;" onclick="ordenar(\'producto\', $(\'#direccion\').val())">Licencias</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;" onclick="ordenar(\'edicion\', $(\'#direccion\').val())">F&iacute;sico</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;" onclick="ordenar(\'edicion\', $(\'#direccion\').val())">Virtual</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #000000; cursor:pointer;" onclick="ordenar(\'version\', $(\'#direccion\').val())">Total</th>
                            <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;" onclick="ordenar(\'cantidad\', $(\'#direccion\').val())">Neto</th>
                        </tr>
                    </thead>
                    <tbody id="bodyBalanza" style="border-left: 1px solid #000000">
                        <?php 
                        foreach($tablaBalanza as $row){
                        ?>
                        <tr style="border-bottom: 1px solid #ddd">
                            <td><?= $row["familia"]; ?></th>
                            <td><?= $row["edicion"];?></th>
                            <td><?= $row["version"]; ?></th>
                            <td align="center"><?= $row["instalaciones"]; ?></th>
                            <td align="center"><?= $row["Fisico"]; ?></th>
                            <td align="center"><?= $row["Virtual"]; ?></th>
                            <td align="center"><?= $row["Fisico"] + $row["Virtual"]; ?></th>
                            <td align="center"><?= $row["instalaciones"] - ($row["Fisico"] + $row["Virtual"]);?></th>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php 
}
?>

<script>
    var vert                 = <?= $vert; ?>;
    var rowFisico            = <?= $i; ?>;
    var rowVirtual           = <?= $j; ?>;
    var producto             = <?= $producto; ?>;
    var rowVirtualSinAlinear = 0; 
    var existeAlineacion     = <?= $existeAlineacionVirtual; ?>;
    var asignarFisico        = false;
    var asignarVirtual       = false;
    
    $(document).ready(function(){ 
        $("#tablaFisico").tableHeadFixer({"left" : 2}); 
        $("#tablaVirtual").tableHeadFixer({"left" : 3}); 
        $("#tablaBalanza").tableHeadFixer(); 
        
        $("#checkAllFisico").click(function(){
            for(i=0; i < rowFisico;i++){
                if($("#checkAllFisico").prop("checked")){
                    for(i=0; i < rowFisico; i++){
                        if($("#checkFisico"+i).length > 0){
                            $("#checkFisico"+i).attr("checked", true);
                        }
                    }
                }
                else{
                    for(i=0; i < rowFisico; i++){
                        if($("#checkFisico"+i).length > 0){
                            $("#checkFisico"+i).attr("checked", false);
                        }
                    }
                }
            }
        });
        
        $("#checkAllVirtual").click(function(){
            for(i=0; i < rowVirtual;i++){
                if($("#checkAllVirtual").prop("checked")){
                    for(i=0; i < rowVirtual; i++){
                        if($("#checkVirtual"+i).length > 0){
                            $("#checkVirtual"+i).attr("checked", true);
                        }
                    }
                }
                else{
                    for(i=0; i < rowVirtual; i++){
                        if($("#checkVirtual"+i).length > 0){
                            $("#checkVirtual"+i).attr("checked", false);
                        }
                    }
                }
            }
        });
        
        $("#checkAllVirtualSinAlinear").click(function(){
            for(i=0; i < rowVirtualSinAlinear;i++){
                if($("#checkAllVirtualSinAlinear").prop("checked")){
                    for(i=0; i < rowVirtual; i++){
                        if($("#checkVirtualSinAlinear"+i).length > 0){
                            $("#checkVirtualSinAlinear"+i).attr("checked", true);
                        }
                    }
                }
                else{
                    for(i=0; i < rowVirtualSinAlinear; i++){
                        if($("#checkVirtualSinAlinear"+i).length > 0){
                            $("#checkVirtualSinAlinear"+i).attr("checked", false);
                        }
                    }
                }
            }
        });
        
        //inicio borrar registros de las tablas
        $("#borrarFisico").click(function(){
            for(i=0; i < rowFisico;i++){
                if($("#rowFisico"+i).length > 0 && $("#checkFisico"+i).prop("checked")){
                    $("#rowFisico"+i).remove();
                }
            }
            $("#checkAllFisico").attr("checked", false);
            if($("#bodyFisico tr").length === 0){
                rowFisico = 0;
            }
        });
        
        $("#borrarVirtual").click(function(){
            for(i=0; i < rowVirtual;i++){
                if($("#rowVirtual"+i).length > 0 && $("#checkVirtual"+i).prop("checked")){
                    $("#rowVirtual"+i).remove();
                }
            }
            $("#checkAllVirtual").attr("checked", false);
            if($("#bodyVirtual tr").length === 0){
                rowVirtual = 0;
            }
            
            if($("#contenedorVirtualSinAlinear").is(":visible")){
                for(i=0; i < rowVirtualSinAlinear;i++){
                    if($("#rowVirtualSinAlinear"+i).length > 0 && $("#checkVirtualSinAlinear"+i).prop("checked")){
                        $("#rowVirtualSinAlinear"+i).remove();
                    }
                }
                
                if($("#bodyVirtualSinAlinear tr").length === 0){
                    rowVirtualSinAlinear = 0;
                    $("#contenedorVirtualSinAlinear").hide();
                    $("#checkAllVirtualSinAlinear").attr("checked", false);
                }
            }
        });
        //fin borrar registros de las tablas
        
        //inicio agregar registros en la tabla
        $("#agregarFisico").click(function(){
            if($("#bodyFisico tr").length > 100){
                $.alert.open('alert', "No se pueden agregar más de 100 Registros", {'Aceptar' : 'Aceptar'}, function() {
                    return false;
                });
            }
            
            if(vert === 0){
                nombre = "Windows Server";
            }
            else{
                nombre = "SQL Server";
            }
            
            row  = '<tr style="border-bottom: 1px solid #ddd" id="rowFisico' + rowFisico + '">';
            row += '<td><input type="checkbox" id="checkFisico' + rowFisico + '" name="checkFisico[]" value="' + rowFisico + '"></td>';
            row += '<td><input type="text" id="nombreFisico' + rowFisico + '" name="nombreFisico[]" value=""></td>';
            row += '<td><input type="text" id="familiaFisico' + rowFisico + '" name="familiaFisico[]" value="' + nombre + '" readonly></td>';
            row += '<td><select id="edicionFisico' + rowFisico + '" name="edicionFisico[]" onchange = "selectEdicion(this.value, \'Fisico\', ' + (rowFisico) + ')"></select></td>';
            row += '<td><select id="versionFisico' + rowFisico + '" name="versionFisico[]" onchange = "selectVersion(this.value, \'Fisico\', ' + (rowFisico) + ')"></select></td>';
            row += '<td><select id="MSDNFisico' + rowFisico + '" name="MSDNFisico[]" onchange = "selectMSDN(this.value, \'Fisico\', ' + (rowFisico) + ')">';
            row += '<option value="No">No</option>';
            row += '<option value="Si">Si</option>';
            row += '</select></td>';
            row += '<td><input type="text" id="tipoFisico' + rowFisico + '" name="tipoFisico[]" value="Fisico" readonly></td>';
            row += '<td><input type="text" id="ccFisico' + rowFisico + '" name="ccFisico[]" value="" readonly></td>';
            row += '<td><input type="text" style="text-align:center" id="procesadoresFisico' + rowFisico + '" name="procesadoresFisico[]" value="0" onclick="selProcCores(\'procesadoresFisico\', ' + rowFisico + ')"></td>';
            row += '<td><input type="text" style="text-align:center" id="coresFisico' + rowFisico + '" name="coresFisico[]" value="0" onclick="selProcCores(\'coresFisico\', ' + rowFisico + ')"></td>';
            row += '<td><input type="text" style="text-align:center" id="licSrvFisico' + rowFisico + '" name="licSrvFisico[]" value="0" onclick="seleccionarFisico(\'licSrvFisico\', ' + rowFisico + ')" readonly></td>';
            row += '<td><input type="text" style="text-align:center" id="licProcFisico' + rowFisico + '" name="licProcFisico[]" value="0" onclick="seleccionarFisico(\'licProcFisico\', ' + rowFisico + ')" readonly></td>';
            row += '<td><input type="text" style="text-align:center" id="licCoreFisico' + rowFisico + '" name="licCoreFisico[]" value="0" onclick="seleccionarFisico(\'licCoreFisico\', ' + rowFisico + ')" readonly></td>';
            row += '</tr>';
            row += '<input type="hidden" id="edicionFisicoSelected' + rowFisico + '" name="edicionFisicoSelected[]" value="">';
            row += '<input type="hidden" id="versionFisicoSelected' + rowFisico + '" name="versionFisicoSelected[]" value="">';
            row += '<input type="hidden" id="MSDNFisicoSelected' + rowFisico + '" name="MSDNFisicoSelected[]" value="">';
            $("#bodyFisico").append(row);
           
            $("#procesadoresFisico" + rowFisico).numeric(false);
            $("#coresFisico" + rowFisico).numeric(false);
            $("#licSrvFisico" + rowFisico).numeric(false);
            $("#licProcFisico" + rowFisico).numeric(false);
            $("#licCoreFisico" + rowFisico).numeric(false);
            
            rowFisico++;
            
            $.post("ajax/obtenerEdicion.php", { fabricante : 3, producto : nombre, token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#edicionFisico" + (rowFisico - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : nombre, edicion : "", token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#versionFisico" + (rowFisico - 1)).append(data[0].option);
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
            
            $("#tablaFisico").tableHeadFixer({"left" : 2}); 
            $("#contenedorFisico").scrollTop($("#tablaFisico").height());
        });
        
        $("#agregarVirtual").click(function(){
            var otroRow; 
            if($("#alinear").val() == "false" && existeAlineacion == 0){
                bodyTable = "#bodyVirtual";
                
                if($("#bodyVirtual tr").length > 100){
                    alert("No se pueden agregar más de 100 Registros");
                    return false;
                }
                otroRow = rowVirtual;
            }
            else{
                bodyTable = "#bodyVirtualSinAlinear";
                $("#contenedorVirtualSinAlinear").show();
                if($("#bodyVirtualSinAlinear tr").length > 100){
                    alert("No se pueden agregar más de 100 Registros");
                    return false;
                }
                otroRow = rowVirtualSinAlinear;
            }
            
            
            
            if(vert == 0){
                nombre = "Windows Server";
            }
            else{
                nombre = "SQL Server";
            }
            
            if($("#alinear").val() == "false" && existeAlineacion == 0){
                row  = '<tr style="border-bottom: 1px solid #ddd" id="rowVirtual' + otroRow  + '">';
                row += '<td class="headcol"><input type="checkbox" id="checkVirtual' + otroRow  + '" name="checkVirtual[]" value="' + otroRow  + '"></td>';
                row += '<td class="headcol"><input type="text" id="clusterVirtual' + otroRow  + '" name="clusterVirtual[]" value=""></td>';
                row += '<td class="headcol"><input type="text" id="hostVirtual' + otroRow  + '" name="hostVirtual[]" value=""></td>';
                row += '<td class="long"><input type="text" id="nombreVirtual' + otroRow  + '" name="nombreVirtual[]" value=""></td>';
                row += '<td class="long"><input type="text" id="familiaVirtual' + otroRow  + '" name="familiaVirtual[]" value="' + nombre + '" readonly></td>';
                row += '<td><select id="edicionVirtual' + otroRow + '" name="edicionVirtual[]" onchange="selectEdicion(this.value, \'Virtual\', ' + otroRow + ')"></select></td>';
                row += '<td><select id="versionVirtual' + otroRow + '" name="versionVirtual[]" onchange="selectVersion(this.value, \'Virtual\', ' + otroRow + ')"></select></td>';
                row += '<td><select id="MSDNVirtual' + otroRow + '" name="MSDNVirtual[]" onchange="selectMSDN(this.value, \'Virtual\', ' + otroRow + ')">';
                row += '<option value="No">No</option>';
                row += '<option value="Si">Si</option>';
                row += '</select></td>';
                row += '<td class="long"><input type="text" id="tipoVirtual' + otroRow  + '" name="tipoVirtual[]" value="Virtual" readonly></td>';
                row += '<td class="long"><input type="text" id="ccVirtual' + otroRow  + '" name="ccVirtual[]" value="" readonly></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="procesadoresVirtual' + otroRow  + '" name="procesadoresVirtual[]" value="0" onclick="selProcCores(\'procesadoresVirtual\', ' + otroRow + ')"></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="coresVirtual' + otroRow  + '" name="coresVirtual[]" value="0" onclick="selProcCores(\'coresVirtual\', ' + otroRow + ')"></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="licSrvVirtual' + otroRow  + '" name="licSrvVirtual[]" value="0" onclick="seleccionar(\'licSrvVirtual\', ' + otroRow + ')" readonly></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="licProcVirtual' + otroRow  + '" name="licProcVirtual[]" value="0" onclick="seleccionar(\'licProcVirtual\', ' + otroRow + ')" readonly></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="licCoreVirtual' + otroRow  + '" name="licCoreVirtual[]" value="0" onclick="seleccionar(\'licCoreVirtual\', ' + otroRow + ')" readonly></td>';
                row += '</tr>';
                row += '<input type="hidden" id="edicionVirtualSelected' + otroRow + '" name="edicionVirtualSelected[]" value="">';
                row += '<input type="hidden" id="versionVirtualSelected' + otroRow + '" name="versionVirtualSelected[]" value="">';
                row += '<input type="hidden" id="MSDNVirtualSelected' + otroRow + '" name="MSDNVirtualSelected[]" value="">';
            }
            else{
                row  = '<tr style="border-bottom: 1px solid #ddd" id="rowVirtualSinAlinear' + otroRow  + '">';
                row += '<td class="headcol"><input type="checkbox" id="checkVirtualSinAlinear' + otroRow  + '" name="checkVirtualSinAlinear[]" value="' + otroRow  + '"></td>';
                row += '<td class="headcol"><input type="text" id="clusterVirtualSinAlinear' + otroRow  + '" name="clusterVirtualSinAlinear[]" value=""></td>';
                row += '<td class="headcol"><input type="text" id="hostVirtualSinAlinear' + otroRow  + '" name="hostVirtualSinAlinear[]" value=""></td>';
                row += '<td class="long"><input type="text" id="nombreVirtualSinAlinear' + otroRow  + '" name="nombreVirtualSinAlinear[]" value=""></td>';
                row += '<td class="long"><input type="text" id="familiaVirtualSinAlinear' + otroRow  + '" name="familiaVirtualSinAlinear[]" value="' + nombre + '" readonly></td>';
                row += '<td><select id="edicionVirtualSinAlinear' + otroRow + '" name="edicionVirtualSinAlinear[]" onchange="selectEdicionSinAlinerar(this.value, \'Virtual\', ' + otroRow + ')"></select></td>';
                row += '<td><select id="versionVirtualSinAlinear' + otroRow + '" name="versionVirtualSinAlinear[]" onchange="selectVersionSinAlinerar(this.value, \'Virtual\', ' + otroRow + ')"></select></td>';
                row += '<td><select id="MSDNVirtualSinAlinear' + otroRow + '" name="MSDNVirtualSinAlinear[]" onchange="selectMSDN(this.value, \'Virtual\', ' + otroRow + ')">';
                row += '<option value="No">No</option>';
                row += '<option value="Si">Si</option>';
                row += '</select></td>';
                row += '<td class="long"><input type="text" id="tipoVirtualSinAlinear' + otroRow  + '" name="tipoVirtualSinAlinear[]" value="Virtual" readonly></td>';
                row += '<td class="long"><input type="text" id="ccVirtualSinAlinear' + otroRow  + '" name="ccVirtualSinAlinear[]" value="" readonly></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="procesadoresVirtualSinAlinear' + otroRow  + '" name="procesadoresVirtualSinAlinear[]" value="0" onclick="selProcCores(\'procesadoresVirtualSinAlinear\', ' + otroRow + ')"></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="coresVirtualSinAlinear' + otroRow  + '" name="coresVirtualSinAlinear[]" value="0" onclick="selProcCores(\'coresVirtualSinAlinear\', ' + otroRow + ')"></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="licSrvVirtualSinAlinear' + otroRow  + '" name="licSrvVirtualSinAlinear[]" value="0" onclick="seleccionar(\'licSrvVirtual\', ' + otroRow + ')" readonly></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="licProcVirtualSinAlinear' + otroRow  + '" name="licProcVirtualSinAlinear[]" value="0" onclick="seleccionar(\'licProcVirtual\', ' + otroRow + ')" readonly></td>';
                row += '<td class="long"><input type="text" style="text-align:center" id="licCoreVirtualSinAlinear' + otroRow  + '" name="licCoreVirtualSinAlinear[]" value="0" onclick="seleccionar(\'licCoreVirtual\', ' + otroRow + ')" readonly></td>';
                row += '</tr>';
                row += '<input type="hidden" id="edicionVirtualSelectedSinAlinear' + otroRow + '" name="edicionVirtualSelectedSinAlinear[]" value="">';
                row += '<input type="hidden" id="versionVirtualSelectedSinAlinear' + otroRow + '" name="versionVirtualSelectedSinAlinear[]" value="">';
                row += '<input type="hidden" id="MSDNVirtualSelectedSinAlinear' + otroRow + '" name="MSDNVirtualSelectedSinAlinear[]" value="">';
            }
            
            $(bodyTable).append(row);
            
            $("#procesadoresVirtual" + rowVirtual).numeric(false);
            $("#coresVirtual" + rowVirtual).numeric(false);
            $("#licSrvVirtual" + rowVirtual).numeric(false);
            $("#licProcVirtual" + rowVirtual).numeric(false);
            $("#licCoreVirtual" + rowVirtual).numeric(false);
            
            otroRow++;
            
            if($("#alinear").val() == "false" && existeAlineacion == 0){
                $("#tablaVirtual").tableHeadFixer({"left" : 3}); 
                               
                $.post("ajax/obtenerEdicion.php", { fabricante : 3, producto : nombre, token : localStorage.licensingassuranceToken }, function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    $("#edicionVirtual" + (otroRow - 1)).append(data[0].option);
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });

                $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : nombre, edicion :  "", token : localStorage.licensingassuranceToken }, function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    $("#versionVirtual" + (otroRow - 1)).append(data[0].option);
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
            else{
                $("#tablaVirtualSinAlinear").tableHeadFixer({"left" : 3}); 
                $.post("ajax/obtenerEdicion.php", { fabricante : 3, producto : nombre, token : localStorage.licensingassuranceToken }, function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    $("#edicionVirtualSinAlinear" + (otroRow - 1)).append(data[0].option);
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });

                $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : nombre, edicion :  "", token : localStorage.licensingassuranceToken }, function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    $("#versionVirtualSinAlinear" + (otroRow - 1)).append(data[0].option);
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
            
            if($("#alinear").val() == "false" && existeAlineacion == 0){
                rowVirtual = otroRow;
            }
            else{
                bodyTable = "#bodyVirtualSinAlinear";
                $("#contenedorVirtualSinAlinear").show();
                if($("#bodyVirtualSinAlinear tr").length > 100){
                    alert("No se pueden agregar más de 100 Registros");
                    return false;
                }
                rowVirtualSinAlinear = otroRow;
            }
        });
        //fin agregar registros en la tabla
        
        //inicio asignar
        $("#asignarFisico").click(function(){
            for(i=0; i < rowFisico;i++){
                if($("#licSrvFisico"+i).length > 0){
                   $("#procesadoresFisico"+i).removeAttr("readonly");
                   $("#coresFisico"+i).removeAttr("readonly");
                   $("#MSDNFisico"+i).removeAttr("disabled");
                   $("#ccFisico"+i).removeAttr("readonly");
                   /*$("#licSrvFisico"+i).removeAttr("readonly");
                   $("#licProcFisico"+i).removeAttr("readonly");
                   $("#licCoreFisico"+i).removeAttr("readonly");*/
                }
            }
            
            $("#contenedorFisico").scrollLeft($("#tablaFisico").width());
            asignarFisico = true;
        });
        
        $("#asignarVirtual").click(function(){
            for(i=0; i < rowVirtual;i++){
                if($("#block"+i).val() == "false"){
                    $("#procesadoresVirtual"+i).removeAttr("readonly");
                    $("#coresVirtual"+i).removeAttr("readonly");
                    $("#MSDNVirtual"+i).removeAttr("disabled");
                    $("#ccVirtual"+i).removeAttr("readonly");
                }
            }
            
            if($("#alinear").val() == "false" || $("#alinear").val() == "true" && !$('#contenedorVirtualSinAlinear').is(":visible")){
                $("#contenedorVirtual").scrollLeft($("#tablaVirtual").width());
            }
            else{
                $("#contenedorVirtualSinAlinear").scrollLeft($("#tablaVirtualSinAlinear").width());
            }
            asignarVirtual = true;
        });
        //fin asignar
        
        //inicio editar
        $("#editarFisico").click(function(){
            for(i=0; i < rowFisico;i++){                                           
                if($("#nombreFisico"+i).length > 0){
                   $("#edicionFisico"+i).removeAttr("disabled");
                   $("#versionFisico"+i).removeAttr("disabled");
                   $("#MSDNFisico"+i).removeAttr("disabled");
                   $("#procesadoresFisico"+i).removeAttr("readonly");
                   $("#coresFisico"+i).removeAttr("readonly");
                   $("#ccFisico"+i).removeAttr("readonly");
                }
            }
        });
        
        $("#editarVirtual").click(function(){
            if($("#alinear").val() == "true" && $('#contenedorVirtualSinAlinear').is(":visible")){
                $.alert.open('alert', "Debe actualizar la alineación antes de editar", {'Aceptar' : 'Aceptar'}, function() {
                    return false;
                });
            }
            
            for(i=0; i < rowVirtual;i++){                                           
                if($("#clusterVirtual"+i).length > 0){
                    if($("#block"+i).val() == "false"){
                        $("#edicionVirtual"+i).removeAttr("disabled");
                        $("#versionVirtual"+i).removeAttr("disabled");
                        $("#MSDNVirtual"+i).removeAttr("disabled");
                        $("#procesadoresVirtual"+i).removeAttr("readonly");
                        $("#coresVirtual"+i).removeAttr("readonly");
                        $("#ccVirtual"+i).removeAttr("readonly");
                    }
                }
            }
        });
        //inicio editar
        
        //inicio alinear servidores
        $("#alinearVirtual").click(function(){
            if($("#alinear").val() == "false"){
                for(i=0; i < rowVirtual;i++){                                           
                    if($("#clusterVirtual"+i).length > 0 && $("#clusterVirtual"+i).val() == ""){
                        $.alert.open('alert', "Existen cluster vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                            return false;
                        });
                    }
                    
                    if($("#hostVirtual"+i).length > 0 && $("#hostVirtual"+i).val() == ""){
                        $.alert.open('alert', "Existen host vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                            return false;
                        });
                    }
                    
                    if($("#nombreVirtual"+i).length > 0 && $("#nombreVirtual"+i).val() == ""){
                        $.alert.open('alert', "Existen nombres vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                            return false;
                        });
                    }
                }
                
                $("#tokenVirtual").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#formVirtual")[0]);
                $.ajax({
                    type: "POST",
                    url: "ajax/guardarVirtualServidor.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                
                        result = data[0].result;
                        if(result == 0){
                            alert("No se guardó la información para alinear");
                        }
                        else if(result == 1 || result == 2){
                            $.post("ajax/alinearServidores.php", { vert : vert, token : localStorage.licensingassuranceToken }, function(data){
                                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                        
                                $("#bodyVirtual").empty();
                                $("#bodyVirtual").append(data[0].tabla);
                                rowVirtual = data[0].newRow;
                            }, "json")
                            .fail(function( jqXHR ){
                                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                                });
                            });
                        }
                    }
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
            else{
                if($("#checkAllVirtualSinAlinear").is(":visible")){
                    for(i=0; i < rowVirtualSinAlinear;i++){ 
                        if($("#clusterVirtualSinAlinear"+i).length > 0 && $("#clusterVirtualSinAlinear"+i).val() == ""){
                            $.alert.open('alert', "Existen cluster vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                                return false;
                            });
                        }
                        if($("#hostVirtualSinAlinear"+i).length > 0 && $("#hostVirtualSinAlinear"+i).val() == ""){
                            $.alert.open('alert', "Existen host vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                                return false;
                            });
                        }
                        if($("#nombreVirtualSinAlinear"+i).length > 0 && $("#nombreVirtualSinAlinear"+i).val() == ""){
                            $.alert.open('alert', "Existen nombres vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                                return false;
                            });
                        }
                        $("#edicionVirtualSelectedSinAlinear"+i).val($("#edicionVirtualSinAlinear"+i).val());
                        $("#versionVirtualSelectedSinAlinear"+i).val($("#versionVirtualSinAlinear"+i).val());
                        $("#MSDNVirtualSelectedSinAlinear"+i).val($("#MSDNVirtualSinAlinear"+i).val());
                    }
                    
                    $("tokenVirtualSinAlinear").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#formSinAlinear")[0]);
                    $.ajax({
                        type: "POST",
                        url: "ajax/guardarVirtualSinAlinear.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                    
                            result = data[0].result;
                            if(result == 0){
                                $.alert.open('alert', "No se guardó la información", {'Aceptar' : 'Aceptar'}, function() {
                                });
                            }
                            else if(result == 1){
                                $.alert.open('alert', "Se guardó la información con éxito", {'Aceptar' : 'Aceptar'}, function() {
                                });
                            }
                            else if(result == 2){
                                $.alert.open('alert', "No se guardó toda la información", {'Aceptar' : 'Aceptar'}, function() {
                                });
                            }

                            $.post("ajax/alinearServidores.php", { vert : vert, token : localStorage.licensingassuranceToken }, function(data){
                                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                        
                                $("#bodyVirtual").empty();
                                $("#bodyVirtual").append(data[0].tabla);
                                rowVirtual = data[0].newRow;
                                $("#bodyVirtualSinAlinear").empty();
                                $("#contenedorVirtualSinAlinear").hide();
                                rowVirtualSinAlinear = 0;
                            }, "json")
                            .fail(function( jqXHR ){
                                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                                });
                            });
                        }
                    })
                    .fail(function( jqXHR ){
                        $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                        });
                    });
                }
            }
          
            $("#alinear").val("true");
        });
        //fin alinear servidores
        
        //inicio actualizar
        $("#actualizarFisico").click(function(){
            if($("#bodyFisico tr").length == 0){
                $.alert.open('alert', "Debe existir al menos un registro para actualizar", {'Aceptar' : 'Aceptar'}, function() {
                });
                return false;
            }
            
            for(i=0; i < rowFisico;i++){                                           
                if($("#nombreFisico"+i).length > 0 && $("#nombreFisico"+i).val() == ""){
                    $.alert.open('alert', "Existen nombres vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                    });
                    return false;
                }
                $("#edicionFisicoSelected"+i).val($("#edicionFisico"+i).val());
                $("#versionFisicoSelected"+i).val($("#versionFisico"+i).val());
            }
            
            $("#tokenFisico").val(localStorage.licensingassuranceToken);
            $("#fondo").show();
            var formData = new FormData($("#formFisico")[0]);	
            $.ajax({
                type: "POST",
                url: "ajax/guardarFisicoServidor.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    $("#fondo").hide();
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    result = data[0].result;
                    if(result === 0){
                        $.alert.open('alert', "No se guardó la información", {'Aceptar' : 'Aceptar'}, function() {
                            location.href = "servidores.php?vert=" + vert;
                        });
                    }
                    else if(result === 1){
                        $.alert.open('alert', "Se guardó la información con éxito", {'Aceptar' : 'Aceptar'}, function() {
                            location.href = "servidores.php?vert=" + vert;
                        });
                    }
                    else if(result === 2){
                        $.alert.open('alert', "No se guardó toda la información", {'Aceptar' : 'Aceptar'}, function() {
                            location.href = "servidores.php?vert=" + vert;
                        });
                    }
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#actualizarVirtual").click(function(){            
            if($("#alinear").val() == "false" && existeAlineacion == 0){   
                for(i=0; i < rowVirtual;i++){                                           
                    if($("#nombreVirtual"+i).length > 0 && $("#nombreVirtual"+i).val() == ""){
                        $.alert.open('alert', "Existen nombres vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                            return false;
                        }); 
                    }
                    $("#edicionVirtualSelected"+i).val($("#edicionVirtual"+i).val());
                    $("#versionVirtualSelected"+i).val($("#versionVirtual"+i).val());
                    $("#MSDNVirtualSelected"+i).val($("#MSDNVirtual"+i).val());
                }
                
                $("#tokenVirtual").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#formVirtual")[0]);	
                $.ajax({
                    type: "POST",
                    url: "ajax/guardarVirtualServidor.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                
                        result = data[0].result;
                        if(result == 0){
                            $.alert.open('alert', "No se guardó la información", {'Aceptar' : 'Aceptar'}, function() {
                                location.href = "servidores.php?vert=" + vert;
                            }); 
                        }
                        else if(result == 1){
                            $.alert.open('alert', "Se guardó la información con éxito", {'Aceptar' : 'Aceptar'}, function() {
                                location.href = "servidores.php?vert=" + vert;
                            });
                        }
                        else if(result == 2){
                            $.alert.open('alert', "No se guardó toda la información", {'Aceptar' : 'Aceptar'}, function() {
                                location.href = "servidores.php?vert=" + vert;
                            });
                        }
                    }
                })
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
            else if(($("#alinear").val() == "true" || existeAlineacion > 0) && $("#contenedorVirtualSinAlinear").is(":visible")){
                for(i=0; i < rowVirtualSinAlinear;i++){ 
                    if($("#clusterVirtualSinAlinear"+i).length > 0 && $("#clusterVirtualSinAlinear"+i).val() == ""){
                        $.alert.open('alert', "Existen cluster vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                            return false;
                        });                        
                    }
                    if($("#hostVirtualSinAlinear"+i).length > 0 && $("#hostVirtualSinAlinear"+i).val() == ""){
                        $.alert.open('alert', "Existen host vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                            return false;
                        }); 
                    }
                    if($("#nombreVirtualSinAlinear"+i).length > 0 && $("#nombreVirtualSinAlinear"+i).val() == ""){
                        $.alert.open('alert', "Existen nombres vacíos por favor llénelos", {'Aceptar' : 'Aceptar'}, function() {
                            return false;
                        });
                    }
                    $("#edicionVirtualSelectedSinAlinear"+i).val($("#edicionVirtualSinAlinear"+i).val());
                    $("#versionVirtualSelectedSinAlinear"+i).val($("#versionVirtualSinAlinear"+i).val());
                    $("#MSDNVirtualSelectedSinAlinear"+i).val($("#MSDNVirtualSinAlinear"+i).val());
                }
                
                $("#tokenVirtualSinAlinear").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#formSinAlinear")[0]);
                $.ajax({
                    type: "POST",
                    url: "ajax/guardarVirtualSinAlinear.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                
                        result = data[0].result;
                        if(result == 0){
                            $.alert.open('alert', "No se guardó la información", {'Aceptar' : 'Aceptar'}, function() {
                            });
                        }
                        else if(result == 1){
                            $.alert.open('alert', "Se guardó la información con éxito", {'Aceptar' : 'Aceptar'}, function() {
                            });
                        }
                        else if(result == 2){
                            $.alert.open('alert', "No se guardó toda la información", {'Aceptar' : 'Aceptar'}, function() {
                            });
                        }

                        $.post("ajax/alinearServidores.php", { vert : vert, token : localStorage.licensingassuranceToken }, function(data){
                            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                    
                            $("#bodyVirtual").empty();
                            $("#bodyVirtual").append(data[0].tabla);
                            rowVirtual = data[0].newRow;
                            $("#bodyVirtualSinAlinear").empty();
                            $("#contenedorVirtualSinAlinear").hide();
                            rowVirtualSinAlinear = 0;
                        }, "json")
                        .fail(function( jqXHR ){
                            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                            });
                        });
                    }
                })
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
            else{
                for(i=0; i < rowVirtual;i++){                                           
                    $("#edicionVirtualSelected"+i).val($("#edicionVirtual"+i).val());
                    $("#versionVirtualSelected"+i).val($("#versionVirtual"+i).val());
                    $("#MSDNVirtualSelected"+i).val($("#MSDNVirtual"+i).val());
                }
                
                $("#tokenVirtual").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#formVirtual")[0]);	
                $.ajax({
                    type: "POST",
                    url: "ajax/guardarAlineacionServidor.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                
                        result = data[0].result;
                        if(result == 0){
                            $.alert.open('alert', "No se guardó la información", {'Aceptar' : 'Aceptar'}, function() {
                                location.href = "servidores.php?vert=" + vert;
                            });
                        }
                        else if(result == 1){
                            $.alert.open('alert', "Se guardó la información con éxito", {'Aceptar' : 'Aceptar'}, function() {
                                location.href = "servidores.php?vert=" + vert;
                            });
                        }
                        else if(result == 2){
                            $.alert.open('alert', "No se guardó toda la información", {'Aceptar' : 'Aceptar'}, function() {
                                location.href = "servidores.php?vert=" + vert;
                            });
                        }
                    }
                })
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
        //fin actualizar
        
        //inicio exportar tablas
        $("#exportFisico").click(function(){
           $("#exportarFisico").click(); 
        });
        
        $("#exportVirtual").click(function(){
           $("#exportarVirtual").click(); 
        });
        
        $("#exportBalanza").click(function(){
           $("#exportarBalanza").click(); 
        });
        //fin exportar tablas
        
        $("#descargar").click(function(){
            window.open("<?= $GLOBALS["domain_root"] ?>/reportes/formServidorFisico.php?vert=<?= $vert ?>");
        });
        
        $("#cargarFisico").click(function(){
            $("#fileFisico").click();
        });
        
        $("#fileFisico").change(function(e){
            if(addArchivo(e)){
                $("#fondo").show();
                $("#tokenCargarFisico").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#formCargarFisico")[0]);
                $.ajax({
                    type: "POST",
                    url: "ajax/cargarFisicoServidor.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        $("#fondo").hide();
                        <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>

                        result = data[0].result;
                        if(data[0].error == 0){
                            if(result == 0){
                                $.alert.open('warning', 'Alerta', "No se guardó la información", {'Aceptar' : 'Aceptar'});
                            }
                            else if(result == 1){
                                $.alert.open('info', "Se guardó la información con éxito", {'Aceptar' : 'Aceptar'}, function(){
                                    location.reload();
                                });
                            }
                            else if(result == 2){
                                $.alert.open('warning', "No se guardó toda la información", {'Aceptar' : 'Aceptar'}, function(){
                                    location.reload();
                                });
                            }
                        } else{
                            if(data[0].error == 1){
                                $.alert.open('warning', "La extesnión del archivo no es .csv", {'Aceptar' : 'Aceptar'});
                            } else if(data[0].error == 2 || data[0].error == 3){
                                $.alert.open('warning', "No se cargó ningún archivo", {'Aceptar' : 'Aceptar'});
                            } else if(data[0].error == 4){
                                $.alert.open('warning', "La cabecera del archivo no es compatible", {'Aceptar' : 'Aceptar'});
                            }
                        }
                    }
                    
                })
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            } 
        });
        
        $("#descargarVirtual").click(function(){
            window.open("<?= $GLOBALS["domain_root"] ?>/reportes/formServidorVirtual.php?vert=<?= $vert ?>");
        });
        
        $("#cargarVirtual").click(function(){
            $("#fileVirtual").click();
        });
        
        $("#fileVirtual").change(function(e){
            if(addArchivo(e)){
                $("#fondo").show();
                $("#tokenCargarVirtual").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#formCargarVirtual")[0]);
                $.ajax({
                    type: "POST",
                    url: "ajax/cargarVirtualServidor.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        $("#fondo").hide();
                        <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>

                        result = data[0].result;
                        if(data[0].error == 0){
                            if(result == 0){
                                $.alert.open('warning', 'Alerta', "No se guardó la información", {'Aceptar' : 'Aceptar'});
                            }
                            else if(result == 1){
                                $.alert.open('info', "Se guardó la información con éxito", {'Aceptar' : 'Aceptar'}, function(){
                                    location.reload();
                                });
                            }
                            else if(result == 2){
                                $.alert.open('warning', "No se guardó toda la información", {'Aceptar' : 'Aceptar'}, function(){
                                    location.reload();
                                });
                            }
                        } else{
                            if(data[0].error == 1){
                                $.alert.open('warning', "La extesnión del archivo no es .csv", {'Aceptar' : 'Aceptar'});
                            } else if(data[0].error == 2 || data[0].error == 3){
                                $.alert.open('warning', "No se cargó ningún archivo", {'Aceptar' : 'Aceptar'});
                            } else if(data[0].error == 4){
                                $.alert.open('warning', "La cabecera del archivo no es compatible", {'Aceptar' : 'Aceptar'});
                            }
                        }
                    }
                    
                })
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            } 
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
    
    function selectEdicion(valor, tipo, indice){
        $("#edicion"+tipo+"Selected"+indice).val(valor);
        $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : $("#familia"+tipo+indice).val(), edicion : valor, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#version"+tipo+indice).empty();
            $("#version"+tipo+indice).append(data[0].option);
        }, 'json')
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function selectVersion(valor, tipo, indice){
        $("#version"+tipo+"Selected"+indice).val(valor);
    }  
    
    function selectMSDN(valor, tipo, indice){
        $("#MSDN"+tipo+"Selected"+indice).val(valor);
    }
    
    function selectEdicionSinAlinerar(valor, tipo, indice){
        $("#edicion"+tipo+"SelectedSinAlinear"+indice).val(valor);
        $.post("ajax/obtenerVersion.php", { fabricante : 3, producto : $("#familia"+tipo+"SinAlinear"+indice).val(), edicion : valor, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#version"+tipo+"SinAlinear"+indice).empty();
            $("#version"+tipo+"SinAlinear"+indice).append(data[0].option);
        }, 'json')
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function selectVersionSinAlinerar(valor, tipo, indice){
        $("#version"+tipo+"SelectedSinAlinerar"+indice).val(valor);
    }  
    
    function selProcCores(campo, indice){
        if(!$("#"+campo+indice).prop("readonly")){
            $("#"+campo+indice).val("");
        }
    }
    
    function seleccionarFisico(campo, indice){
        if(asignarFisico == true){
            habilitarCampoSeleccion(campo, indice);
        }
    }
    
    function seleccionar(campo, indice){
        if(asignarVirtual == true){
            habilitarCampoSeleccion(campo, indice);
        }     
    }
    
    function habilitarCampoSeleccion(campo, indice){
        
            if(campo == "licSrvFisico" && $("#licProcFisico"+indice).val() == 0 && $("#licCoreFisico"+indice).val() == 0){
                $("#"+campo+indice).val("");
                $("#licCoreFisico"+indice).attr("readonly", true);
                $("#licProcFisico"+indice).attr("readonly", true);
                $("#"+campo+indice).removeAttr("readonly");
            }
            else if(campo == "licProcFisico" && $("#licSrvFisico"+indice).val() == 0 && $("#licCoreFisico"+indice).val() == 0){
                $("#"+campo+indice).val("");
                $("#licCoreFisico"+indice).attr("readonly", true);
                $("#licSrvFisico"+indice).attr("readonly", true);
                $("#"+campo+indice).removeAttr("readonly");
            }
            else if(campo == "licCoreFisico" && $("#licProcFisico"+indice).val() == 0 && $("#licSrvFisico"+indice).val() == 0){
                $("#"+campo+indice).val("");
                $("#licSrvFisico"+indice).attr("readonly", true);
                $("#licProcFisico"+indice).attr("readonly", true);
                $("#"+campo+indice).removeAttr("readonly");
            }
        if($("#block"+indice).val() == "false"){   
            if(campo == "licSrvVirtual" && $("#licProcVirtual"+indice).val() == 0 && $("#licCoreVirtual"+indice).val() == 0){
                $("#"+campo+indice).val("");
                $("#licProcVirtual"+indice).attr("readonly", true);
                $("#licCoreVirtual"+indice).attr("readonly", true);
                $("#"+campo+indice).removeAttr("readonly");
            }
            else if(campo == "licProcVirtual" && $("#licSrvVirtual"+indice).val() == 0 && $("#licCoreVirtual"+indice).val() == 0){
                $("#"+campo+indice).val("");
                $("#licSrvVirtual"+indice).attr("readonly", true);
                $("#licCoreVirtual"+indice).attr("readonly", true);
                $("#"+campo+indice).removeAttr("readonly");
            }
            else if(campo == "licCoreVirtual" && $("#licSrvVirtual"+indice).val() == 0 && $("#licProcVirtual"+indice).val() == 0){
                $("#"+campo+indice).val("");
                $("#licSrvVirtual"+indice).attr("readonly", true);
                $("#licProcVirtual"+indice).attr("readonly", true);
                $("#"+campo+indice).removeAttr("readonly");
            }
        }
    }    
</script>