<table class="tablap">
    <tr style="background:#333; color:#fff;">
        <th align="center" valign="middle">Type</th>
        <th align="center"  valign="middle">Inactive</th>
        <th align="center"  valign="middle">Active</th>
        <th align="center" valign="middle">Not Scanned</th>
        <th align="center" valign="middle">Scanned</th>
        <th align="center" valign="middle">Coverage %</th>
    </tr>
    <tr>
        <th align="left" valign="middle">Clients</th>
        <td align="center" valign="middle"><?= $total_1InacLAc ?></td>
        <td align="center" valign="middle"><?= $total_2DVc ?></td>
        <td align="center" valign="middle">
            <?php
            $t1 = $total_2DVc - $total_1LAc;
            echo $t1;
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_1LAc ?></td>
        <td align="center" valign="middle">
            <?php
            $pot11 = 0;
            if($total_2DVc != 0){
                $pot11 = ($total_1LAc / $total_2DVc) * 100;
            }
            echo round($pot11) . " %";
            ?>
        </td>
    </tr>
    <tr>
        <th align="left" valign="middle">Servers</th>
        <td align="center" valign="middle"><?= $total_1InacLAs ?></td>
        <td align="center" valign="middle"><?= $total_2DVs ?></td>
        <td align="center" valign="middle">
            <?php
            $t2 = $total_2DVs - $total_1LAs;
            echo $t2;
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_1LAs ?></td>
        <td align="center" valign="middle">
            <?php
            $pot12 = 0;
            if($total_2DVs != 0){
                $pot12 = ($total_1LAs / $total_2DVs) * 100;
            }
            echo round($pot12) . " %";
            ?>
        </td>
    </tr>



    <tr>
        <th align="left" valign="middle"><strong style="font-weight:bold;">Total</strong></th>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $total_1InacLAc + $total_1InacLAs ?></strong></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $total_2DVc + $total_2DVs ?></strong></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $reconciliacion1c + $reconciliacion1s ?></strong></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $total_1LAc + $total_1LAs ?></strong></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;">

            <?php
            $to1 = $total_1LAc + $total_1LAs;
            $to2 = $total_2DVc + $total_2DVs;

            $pot13 = 0;
            if($to2 != 0){
                $pot13 = ($to1 / $to2) * 100;
            }
            echo round($pot13) . " %";
            ?>
        </strong></td>

    </tr>
</table>

<br>

<div style="width:99%; height:400px;">
    <table class="tablap" id="tablaAlcance" style="margin-top:-5px;">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>Device Name</span></th>
            <th  align="center" valign="middle"><span>Type</span></th>
            <th  align="center" valign="middle"><span>Active AD</span></th>
            <th  align="center" valign="middle"><span>LA Tool</span></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        foreach ($listar_equipos as $reg_equipos2) {
        ?>
            <tr>
                <td align="center"><?= $i ?></td>
                <td ><?= $reg_equipos2["equipo"] ?></td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["tipo"] == 1) {
                        echo 'Client';
                    } else {
                        echo 'Server';
                    }
                    ?>

                </td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                        echo 'Yes';
                    } else {
                        echo 'No';
                    }
                    ?>


                </td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["errors"] == 'Ninguno') {
                        echo 'Yes';
                    } else {
                        echo 'No';
                    }
                    ?>

                </td>
            </tr>
        <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
</div>