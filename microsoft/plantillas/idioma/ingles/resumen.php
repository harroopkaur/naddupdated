<script type="text/javascript">
    $(document).ready(function ()
    {
        $(".filtra").click(function () {
            var valor = parseInt($("input[name='filt']:checked").val());

            if (valor === 1) {
                window.location.href = "resumen.php";
            } else if (valor === 2) {
                window.location.href = "resumen.php?vert=1";
            } else if (valor === 3) {
                window.location.href = "resumen.php?vert=2";
            } else if (valor === 4) {
                window.location.href = "resumen.php?vert=3";
            } else if (valor === 5) {
                window.location.href = "resumen.php?vert=4";
            } else if (valor === 6) {
                window.location.href = "resumen.php?vert=5";
            } else if (valor === 7) {
                window.location.href = "resumen.php?vert=6";
            } else if (valor === 8) {
                window.location.href = "resumen.php?vert=7";
            } else if (valor === 9) {
                window.location.href = "resumen.php?vert=8";
            } else if (valor === 10) {
                window.location.href = "resumen.php?vert=9";
            }
            
        });

    });
    
    function MM_jumpMenu(targ, selObj, restore) { //v3.0
        eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
        if (restore)
            selObj.selectedIndex = 0;
    }
</script>
                                    
<div style="width:99%; margin:0 auto; padding:0px; min-height:300px; overflow:hidden;">
    <?php
    if ($vert == 0  || $vert == 4 || $vert == 7 || $vert == 8 || $vert == 9) {
    ?>
        <div id="container1" style="height:400px; width:46%; margin:10px; float:left;"></div>
        <div id="container2" style="height:400px; width:46%; margin:10px; float:left;"></div>
        <div  style="width: 46%; margin:10px; float:left; text-align:center;"><a onClick="showhide('ttabla1');" style="cursor:pointer;" >See Details</a></div>
        <div  style="width: 46%; margin:10px; float:left; text-align:center;"><a onClick="showhide('ttabla1');" style="cursor:pointer;" >See Details</a></div>
    <?php
    }

    if ($vert == 1 || $vert == 3 || $vert == 6) {
    ?>
        <div id="container1" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div id="container2" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div id="container3" class="hide" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div id="container4" class="hide" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div id="container5" class="hide" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div id="container6" class="hide" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div  style="width: 46%; margin:10px; float:left; text-align:center;"><a <?php if ($vert == 1) { echo 'onclick="showhide(\'ttabla1\')";'; } 
        else if ($vert == 3){ echo 'id="verOptimizacionCliente"'; } else{ echo 'id="verDetalleCliente"'; }?> style="cursor:pointer;" >See Details</a></div>
        <div  style="width: 46%; margin:10px; float:left; text-align:center;"><a <?php if ($vert == 1) { echo 'onclick="showhide(\'ttabla1\')";'; } 
        else if ($vert == 3){ echo 'id="verOptimizacionServidor"'; } else{ echo 'id="verDetalleServidor"'; }?> style="cursor:pointer;" >See Details</a></div>
    <?php
    }
    if($vert == 5){
    ?>
        <div id="container1" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div id="container2" style="height:400px; width: 46%; margin:10px; float:left;"></div>
        <div  style="width: 46%; margin:10px; float:left; text-align:center;"><a onclick="mostrarListado('cliente');" class="pointer">See Details</a></div>
        <div  style="width: 46%; margin:10px; float:left; text-align:center;"><a onclick="mostrarListado('servidor');" class="pointer" >See Details</a></div>
    <?php
    }

    if ($vert == 2) {
    ?>
        <div style="width:99%; overflow-x:auto; overflow-y:hidden;">
            <div id="btnServidores" class="pointer" style="width:45%; text-align:center; padding:10px; float:left">
                <p id="server" style="width:100%; height:50px; font-size:20px; font-weight:bold; line-height:50px;  background-color:#06B6FF; color:#FFFFFF">Servers</p>
            </div>
            <div id="btnClientes" class="pointer" style="width:45%; text-align:center; padding: 10px 0px 10px 10px; float:right">
                <p id="client" style="width:100%; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF; color:#06B6FF">Clients</p>
            </div>
            <div id="contenedorGraficos" style="width:100%; overflow-x: auto; overflow-y:hidden;">
                <div id="graficosServidores" style="width:650px; float:left; text-align:center;">

                    <div id="container3" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container4" style="height:250px; width:300px; margin:10px; float:right;"></div>

                    <br style="clear:both">
                    <div style="width:auto; text-align:center; padding:10px; float:left">
                        <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">SQL</p>
                    </div>
                    <div style="width:auto; text-align:center; padding:10px; float:right">
                        <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">OS</p>
                    </div>
                </div>

                <div id="graficosClientes" style="width:auto; float:left; text-align:center; display:none;">

                    <div id="container5" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container6" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container7" style="height:250px; width:300px; margin:10px; float:left;"></div>

                    <br style="clear:both">
                    <div style="width:auto; text-align:center; padding:10px; float:left">
                        <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">OS</p>
                    </div>
                    <div style="width:auto; text-align:center; padding:10px; float:left">
                        <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">Office</p>
                    </div>
                    <div style="width:auto; text-align:center; padding:10px; float:left">
                        <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">Products</p>
                    </div>
                </div>
            </div>
        </div>	
    <?php
    }
    ?>
        
    <br>
    
    <div style="width:99%; margin:0 auto; padding:0px; padding-bottom: 20px; overflow:hidden; clear:both; margin-top:15px;">        
        <span style="color:#9F9F9F;">Select the Report</span><br> 
        <div style="width:100px; height:<?php if($asig == ''){ echo '44px'; } else{ echo '70px'; } ?>; float:left;">Allocation:<strong style="color:#000; font-weight:bold;">
            <?= $asig ?>
                
            <br>
            <select onchange="MM_jumpMenu('self',this,0)">
                <option value="resumen.php" selected>Select..</option>
                <option value="resumen.php?vert=<?= $vert ?>">All</option>
                <?php 
                foreach($asignaciones as $row){
                ?>
                    <option value="resumen.php?vert=<?= $vert ?>&asig=<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        
        <div style="float:left; width:calc(100% - 120px); height:44px;">
            <?php 
            if ($vert == 6){
            ?>
                <div style="width:650px; height: <?php if($asig == ''){ echo '44px'; } else{ echo '70px'; } ?>; margin:0 auto;">
                    <fieldset class="fieldsetNoDescubiertoEquipo">
                        <input type="radio" id="equipoBoton" name="noDescubierto" checked="checked"><span class="bold pointer" style="margin-right:150px; line-height:44px;" id="EquipoNoDesc">Computer</span>
                    </fieldset>
                    
                    <fieldset class="fieldsetNoDescubiertoReto"> 
                        <input type="radio" id="retoBoton" name="noDescubierto"><span class="bold pointer" style="line-height:44px;" id="RetoNoDesc">Retos</span>
                        <div style="float:right; overflow:hidden; margin-top:2px; margin-right:5px;">
                            <div id="btnTotal" name="btnTotal" class="botonesSAM boton1 hide">Total</div>
                            <div id="btnActivos" name="btnActivos" class="botonesSAM boton5 hide">Activos</div>
                        </div>
                    </fieldset>
                </div>
            <?php
            }
            ?>
        </div>
       
        <div class="clear">
            <div style="margin:10px; float:left;"><input type="radio" name="filt" id="filt1" value="1" class="filtra"   <?php if ($vert == 0) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Compleatness</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt" id="filt2" value="2" class="filtra"   <?php if ($vert == 1) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Usability</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt3" value="3" class="filtra"  <?php if ($vert == 2) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Balance</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt4" value="4" class="filtra"  <?php if ($vert == 3) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Optimization</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt5" value="5" class="filtra"  <?php if ($vert == 4) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Unused Software</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt5" value="6" class="filtra"  <?php if ($vert == 5) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Detail por Device</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt5" value="7" class="filtra"  <?php if ($vert == 6) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Devices not Discovered</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt5" value="8" class="filtra"  <?php if ($vert == 7) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Duplicate Software</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt5" value="9" class="filtra"  <?php if ($vert == 8) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Erroneous Installations</strong></div>
            <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt5" value="10" class="filtra"  <?php if ($vert == 9) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Unsupported Software</strong></div>
        </div>
        
        <div class="clear" style="margin:10px; float:right;">
            <div style="float:right;">                
                <form id="formExportar" name="formExportar" method="post" action="<?= $dirExport ?>">
                    <input type="hidden" id="familiaExcel" name="familiaExcel">
                    <input type="hidden" id="edicionExcel" name="edicionExcel">
                    <input type="hidden" id="vertExportar" name="vert" value = "<?= $vert ?>">
                    <input type="hidden" id="asig" name="asig" value = "<?= $asig ?>">
                    <input type="hidden" id="opcion" name="opcion"><!-- Usado para saber que exportar en optimizacion o en Equipos No Descubiertos-->
                    <input type="submit" id="exportar" name="exportar" style="display:none">
                    <div class="botones_m2 boton1" id="export" style="display:none">Export Excel</div>
                </form>
            </div>
            
            <div style="float:right;">
                <div class="botones_m2 boton1" id="exportarTodo" style="display: none;">Export All Products</div>
            </div>
            
            <div style="float:right; <?php if (!$moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 1)) { echo 'display:none;'; } ?>">
                <div class="botones_m2 boton1" id="actualizarRepo">Update Repository</div>
            </div>   
        </div>
        <br><br>

        <div style="<?php if (!$balanza){ echo 'display:none;'; } ?>">
            <br>
            <div style="width:25%; margin:0px; padding:0px; overflow:hidden; float:left;">
                <span style="color:#9F9F9F;">Select the Product</span><br><br> 
                <div>Family: 
                    <br> 

                    <select id="familia" name="familia">
                        <option value="">Select..</option>
                        <option value="Windows">Windows OS</option>
                        <option value="Office">MS Office</option>
                        <option value="Project">MS Project</option>
                        <option value="Visio">MS Visio</option>
                        <option value="Windows Server">MS Windows Server</option>
                        <option value="SQL Server">MS SQL</option>
                        <option value="Others">Others</option>
                    </select>

                </div><br>
                <div>Edici&oacute;n:
                    <br> 
                    <select id="edicion" name="edicion">
                        <option value="">Select..</option>
                    </select>				
                </div>
            </div>

            <div style="width:73%; margin:0px; padding:0px; overflow:hidden; float:left;">
                <div id="containerDetalle">

                </div>
            </div>

            <br style="clear:both;">
            <br>
            
            <form id="formGAP" name="formGAP" method="post">
                <input type="hidden" id="tokenGAP" name="token">
                <input type="hidden" id="bandEditar" value="false">
                <div id="divTabla" style="display:none; width:100%; max-height:400px; overflow:auto;">
                    <table class="tablap" id="tablaBalanza" style="margin-top:-5px;">
                        <thead>
                            <tr style="background:#333; color:#fff;">
                                <th class="text-center">Product</th>
                                <th class="text-center">Edition</th>
                                <th class="text-center">Version</th>
                                <th class="text-center">Allocation</th>
                                <th class="text-center">Installations</th>
                                <th class="text-center">Purchases</th>
                                <th class="text-center">Disponible</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Quantity GAP</th>
                                <th class="text-center">Disponible GAP</th>
                            </tr>
                        </thead>
                        <tbody id="tablaDetalle">

                        </tbody>
                    </table>
                </div>
                
                <br>
                <div class="botones_m2 boton1" style="display:none; float:right;" id="guardarGAP">Update GAP</div>
                <div class="botones_m2 boton1" style="display:none; float:right;" id="editarGAP" onclick="editarGAP()">Edit GAP</div>
            </form>
        </div>

    </div>
    <?php
    if ($vert == 0) {
    ?>
        <div id="ttabla1"  style="display:none; width:99%; margin:10px; float:left;">
            <?php 
            if($_SESSION["idioma"] == 1){
                include_once("alcance.php"); 
            }else if($_SESSION["idioma"] == 2){
                include_once("plantillas/idioma/ingles/alcance.php"); 
            }
            ?>
        </div>
    <?php
    }//0

    if ($vert == 1) {
    ?>
        <div id="ttabla1"  style=" display:none; width:99%; margin:10px; float:left;">
            <?php 
            if($_SESSION["idioma"] == 1){
                include_once("usabilidad.php"); 
            }else if($_SESSION["idioma"] == 2){
                include_once("plantillas/idioma/ingles/usabilidad.php"); 
            }
            ?>
        </div>
    <?php
    }
    else if ($vert == 3) {
    ?>
        <div id="ttabla1"  style=" display:none; width:99%; margin:10px; float:left;">
           
        </div>
    <?php
    } else if($vert == 5){
        if($_SESSION["idioma"] == 1){
            include_once("detalleEquipos.php");
        }else if($_SESSION["idioma"] == 2){
            include_once("plantillas/idioma/ingles/detalleEquipos.php");
        }
    } else if ($vert == 4) {
        if($_SESSION["idioma"] == 1){
            include_once("softwareDesuso.php");
        }else if($_SESSION["idioma"] == 2){
            include_once("plantillas/idioma/ingles/softwareDesuso.php");
        }
    } else if ($vert == 6){
        if($_SESSION["idioma"] == 1){
            include_once("equiposNoDescubiertos.php");
        }else if($_SESSION["idioma"] == 2){
            include_once("plantillas/idioma/ingles/equiposNoDescubiertos.php");
        }
    } else if ($vert == 7){
        if($_SESSION["idioma"] == 1){
            include_once("equiposDuplicados.php");
        }else if($_SESSION["idioma"] == 2){
            include_once("plantillas/idioma/ingles/equiposDuplicados.php");
        }
    } else if ($vert == 8){
        if($_SESSION["idioma"] == 1){
            include_once("instalErroneas.php");
        }else if($_SESSION["idioma"] == 2){
            include_once("plantillas/idioma/ingles/instalErroneas.php");
        }
    } else if ($vert == 9){
        if($_SESSION["idioma"] == 1){
            include_once("softSinSoporte.php");
        }else if($_SESSION["idioma"] == 2){
            include_once("plantillas/idioma/ingles/softSinSoporte.php");
        }
    }
    ?>									
</div>

<script>
    var vertAux = "";
    
    $(document).ready(function () {
        $("#tablaAlcance").tablesorter();
        $("#tablaAlcance").tableHeadFixer();
        $("#tablaDesusoCliente").tablesorter();
        $("#tablaDesusoServidor").tablesorter();
        $("#tablaBalanza").tableHeadFixer();
        
        $("#btnServidores").click(function () {
            $("#server").css("color", "#FFFFFF");
            $("#server").css("background-color", "#06B6FF");
            $("#client").css("color", "#06B6FF");
            $("#client").css("background-color", "#FFFFFF");
            $("#contenedorGraficos").css("width", "100%");
            $("#graficosClientes").hide();
            $("#graficosServidores").show();
        });

        $("#btnClientes").click(function () {
            $("#client").css("color", "#FFFFFF");
            $("#client").css("background-color", "#06B6FF");
            $("#server").css("color", "#06B6FF");
            $("#server").css("background-color", "#FFFFFF");
            $("#contenedorGraficos").css("width", "1000px");
            $("#graficosClientes").show();
            $("#graficosServidores").hide();
        });

        $("#export").click(function () {
            $("#exportar").click();
        });
        
        $("#verOptimizacionServidor").click(function(){
            mostrarTabla(3, "", "servidor");
        });
        
        $("#verOptimizacionCliente").click(function(){
             mostrarTabla(3, "", "cliente");
        });
        
        $("#verDetalleCliente").click(function(){
            <?php if($vert != 6 ){ ?>
                mostrarTabla(5, "", "cliente");
            <?php 
            } else{
            ?>
                if($("#container1").is(":visible") || $("#container5").is(":visible")){
                    if($('#ttabla1').is(":visible")){
                        $('#ttabla1').hide();
                        $('#ttabla2').hide();
                        $("#export").hide();
                        $("#opcion").val("Total");
                    } else{
                        $('#ttabla1').show();
                        $('#ttabla2').hide();
                        $("#export").show();
                        $("#opcion").val("Total");
                    }
                } else if($("#container3").is(":visible")){
                    if($('#ttabla2').is(":visible")){
                        $('#ttabla1').hide();
                        $('#ttabla2').hide();
                        $("#export").hide();
                        $("#opcion").val("Activo");
                    } else{
                        $('#ttabla1').hide();
                        $('#ttabla2').show();
                        $("#export").show();
                        $("#opcion").val("Activo");
                    }
                }
            <?php    
            }
            ?>
        });
        
        $("#verDetalleServidor").click(function(){
            <?php if($vert != 6 ){ ?>
                mostrarTabla(5, "", "cliente");
            <?php 
            } else{
            ?>
               $("#verDetalleCliente").click();
            <?php    
            }
            ?>
        });

        $("#familia").change(function () {
            $("#edicion").val("");
            realizarBusqueda();
        });
        
        $("#edicion").change(function () {
            realizarBusqueda();
        });
        
        $("#actualizarRepo").click(function(){
            $("#fondo").show();
            $.post("<?=$GLOBALS['domain_root']; ?>/sam/actRepoDespliegue.php", { tabla : 3, token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                if(data[0].result === 0){
                    $.alert.open('alert', "No se pudo actualizar el repositorio de despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
                else if(data[0].result === 2){
                    $.alert.open('alert', "No se actualizó por completo el repositorio de despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
                else if(data[0].result === 1){
                    $.alert.open('alert', "Repositorio de despliegue actualizado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#guardarGAP").click(function(){
            if($("#bandEditar").val() === "false"){
                $.alert.open("warning", "Alert", "Debe dar click en Editar GAP, realizar las modificaciones y después actualizar", {"Aceptar" : "Aceptar"});
                return false;
            }
            
            $("#fondo").show();
            $("#tokenGAP").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formGAP")[0]);	
            $.ajax({
                type: "POST",
                url: "ajax/actualizarGAP.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    <?php //require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>   
                    
                    $("#fondo").hide();
                    if(data[0].result === 0){
                        $.alert.open("warning", "Alert", "No se pudo actualizar el GAP", {"Aceptar" : "Aceptar"});
                    } else if(data[0].result === 1){
                        $.alert.open("info", "GAP actualizado con éxito", {"Aceptar" : "Aceptar"});
                    }else {
                        $.alert.open("warning", "Alert", "No se pudo actualizar todos los registros del GAP", {"Aceptar" : "Aceptar"});
                    }                    
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
            
            for(i=0; i < $("#tablaDetalle tr").length; i++){
                if(parseInt($("#habilitar" + i).val()) > 0){
                    $("#cantidadGAP" + i).attr("readonly", true);
                    $("#cantidadGAP" + i).css("background-color", "#C1C1C1");
                }
            }
            $("#bandEditar").val("false");
        });
        
        $("#exportarTodo").click(function(){
            window.open("<?= $exportBalanzaTodo ?>");
        });
        
        $("#editarGAP").click(function(){
            j=0;
            for(i=0; i < $("#tablaDetalle tr").length; i++){
                if(parseInt($("#habilitar" + i).val()) > 0){
                    $("#cantidadGAP" + i).attr("readonly", false);
                    $("#cantidadGAP" + i).css("background-color", "");
                    j++;
                }
            }
            
            if(j === 0){
                $.alert.open("warning", "Alert", "No existen productos con Software Assurance", {"Aceptar" : "Aceptar"});
            } else{
                $("#bandEditar").val("true");
            }
        });
        
        $("#EquipoNoDesc, #equipoBoton").click(function(){
            $("#equipoBoton").prop("checked", true);
            $("#container1").show();
            $("#container2").show();
            $("#container3").hide();
            $("#container4").hide();
            $("#container5").hide();
            $("#container6").hide();
            $("#btnTotal").hide();
            $("#btnActivos").hide();
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#export").hide();
        });
        
        $("#RetoNoDesc, #retoBoton").click(function(){
            $("#retoBoton").prop("checked", true);
            $("#container1").hide();
            $("#container2").hide();
            $("#container3").show();
            $("#container4").show();
            $("#btnTotal").show();
            $("#btnActivos").show();
            $("#btnActivos").click();
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#export").hide();
        });
        
        $("#btnActivos").click(function(){
            $("#container3").show();
            $("#container4").show();
            $("#container5").hide();
            $("#container6").hide();
            $("#btnActivos").removeClass("boton1").addClass("boton5");
            $("#btnTotal").removeClass("boton5").addClass("boton1");
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#export").hide();
            //$("#btnActivos").click();
        });
        
        $("#btnTotal").click(function(){
            $("#container3").hide();
            $("#container4").hide();
            $("#container5").show();
            $("#container6").show();
            $("#btnActivos").removeClass("boton5").addClass("boton1");
            $("#btnTotal").removeClass("boton1").addClass("boton5");
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#export").hide();
        });
    });
    
    function realizarBusqueda(){
        if ($("#familia").val() === "") {
            $.alert.open('alert', "Debe seleccionar una familia", {'Aceptar' : 'Aceptar'}, function() {
            });
            return false;
        }
        
        $("#fondo").show();
        $("#export").show();
        $("#exportarTodo").show();

        $.post("ajax/balanzaDetalle.php", {familia: $("#familia").val(), edicion: $("#edicion").val(), asignacion : '<?= $asig ?>', token : localStorage.licensingassuranceToken }, function (data) {
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
            $("#containerDetalle").empty();
            $("#tablaDetalle").empty();
            $("#edicion").empty();
            $("#edicion").append(data[0].edicion);
            $("#familiaExcel").val($("#familia").val());
            $("#edicionExcel").val($("#edicion").val());

            familia = $("#familia").val();
            
            if($("#edicion").val() === "Otros"){
                edicion = "<?= $otrosIdioma ?>";
            }else{
                edicion = $("#edicion").val();
            }
            
            if(familia === "Others" && edicion === ""){
                familia = "<?= $otrosIdioma ?>"; 
                edicion = "";
            }
            else if(familia === "Others" && edicion !== ""){
                if($("#edicion").val() === "Visual"){
                    familia = "Visual Studio";
                }
                else if($("#edicion").val() === "Exchange"){
                    familia = "Exchange Server";
                }
                else if($("#edicion").val() === "Sharepoint"){
                    familia = "Sharepoint Server";
                }
                else if($("#edicion").val() === "Skype"){
                    familia = "Skype for Business";
                }
                else{
                    familia = $("#edicion").val();
                }
                edicion = "";
            }

            serie = [{
                name: 'Obsolete',
                data: [0, data[0].obsoleto, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Neto',
                data: [0, 0, data[0].neto],
                color: '<?= $color3 ?>'
            }, {
                name: 'Installations',
                data: [0, data[0].instalacion, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Purchases',
                data: [parseInt(data[0].compra), 0, 0],
                color: '<?= $color1 ?>'
            }];

            $(function () {
                $('#containerDetalle').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                         text: familia + ' ' + edicion
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: ['', '', ''],
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                         stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    legend: {
                       reversed: true
                    },
                    plotOptions: {
                        dataLabels: {
                            enabled: true
                        },
                        series: {
                           stacking: 'normal'
                        }
                    },
                    tooltip: {
                        headerFormat: ''
                    },
                    series: serie
                });
            });

            $("#tablaDetalle").append(data[0].tabla);
            $("#divTabla").show();
            $("#guardarGAP").show();
            $("#editarGAP").show();
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    
    function mostrarTabla(vert, ordenar, opcion){
        if(vertAux === vert && $("#opcion").val() === opcion){
            if($('#ttabla1').is(':visible')){
                $('#ttabla1').hide();
                $("#export").hide();
            }
            else{
                $('#ttabla1').show();
                $("#export").show();
            }
        }
        else{
            $("#fondo").show();
            vertAux = vert;
            $("#opcion").val(opcion);
            
            if(vert === 3){
                url = "ajax/optimizacionDetalle.php";
            }
            else if(vert === 5){
                url = "ajax/detallesEquipoResumen.php";
            }    
            
            $.post(url, { ordenar : ordenar, direccion : $("#direccion").val(), opcion : opcion, asignacion : '<?= $asig ?>', token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>   
                
                $('#ttabla1').empty();
                $('#ttabla1').append(data[0].tabla);
                $('#ttabla1').show();
                $("#export").show();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        }
    }
</script>