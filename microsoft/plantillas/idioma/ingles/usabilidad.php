<table class="tablap">
    <tr style="background:#333; color:#fff;">
        <th align="center" valign="middle">Usability</th>
        <th align="center"  valign="middle">Clients</th>
        <th align="center" valign="middle">%</th>
        <th align="center" valign="middle">Servers</th>
        <th align="center" valign="middle">%</th>
    </tr>
    <tr>
        <td align="center" valign="middle">In Use</td>
        <td align="center" valign="middle"><?= $total_1 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct1 = 0;
            if($tclient > 0){
                $porct1 = ($total_1 / $tclient) * 100;
            }
            echo round($porct1);
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_4 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct11 = 0;
            if($tserver > 0){
                $porct11 = ($total_4 / $tserver) * 100;
            }
            echo round($porct11);
            ?>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle">Probably in Use</td>
        <td align="center" valign="middle"><?= $total_2 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct2 = 0;
            if($tclient > 0){
                $porct2 = ($total_2 / $tclient) * 100;
            }
            echo round($porct2);
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_5 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct22 = 0;
            if($tserver > 0){
                $porct22 = ($total_5 / $tserver) * 100;
            }
            echo round($porct22);
            ?>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle">Obsolete</td>
        <td align="center" valign="middle"><?= $total_3 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct3 = 0;
            if($tclient > 0){
                $porct3 = ($total_3 / $tclient) * 100;
            }
            echo round($porct3);
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_6 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct33 = 0;
            if($tserver > 0){
                $porct33 = ($total_6 / $tserver) * 100;
            }
            echo round($porct33);
            ?>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle"><strong style="font-weight:bold;">Grand total</strong></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $tclient ?></strong></td>
        <td align="center" valign="middle"></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $tserver ?></strong></td>
        <td align="center" valign="middle"></td>
    </tr>
</table>

<br>

<div style="width:99%; height:400px;">
    <table class="tablap" id="tablaAlcance" style="margin-top:-5px;">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>Device Name</span></th>
            <th  align="center" valign="middle"><span>Type</span></th>
            <th  align="center" valign="middle"><span>Usability</span></th>
            <th  align="center" valign="middle"><span>Scanning</span></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        foreach ($listar_equipos as $reg_equipos2) {
        ?>
            <tr>
                <td align="center"><?= $i ?></td>
                <td ><?= $reg_equipos2["equipo"] ?></td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["tipo"] == 1) {
                        echo 'Client';
                    } else {
                        echo 'Server';
                    }
                    ?>

                </td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["rango"] == 1){
                        echo 'In Use';
                    }
                    else if ($reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {                                                               
                        echo 'Probably in Use';
                    } else {
                        echo 'Obsolete';
                    }
                    ?>
                </td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3){
                        echo 'Yes';
                    }
                    else{
                        echo 'No';
                    }
                    ?>
                </td>
            </tr>
        <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
</div>