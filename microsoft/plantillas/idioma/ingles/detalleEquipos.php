<div style="width:99%; height:400px; display:none;" id="detalleEquipoCliente">
    <table class="tablap" id="tablaDetallesEquipoCliente" style="margin-top:-5px;">
        <thead>
            <tr style="background:#333; color:#fff;">
                <th align="center" valign="middle"><span>&nbsp;</span></th>
                <th align="center" valign="middle"><span>Device</span></th>
                <th align="center" valign="middle"><span>Office</span></th>
                <th align="center" valign="middle"><span>Visio</span></th>
                <th align="center" valign="middle"><span>Project</span></th>
                <th align="center" valign="middle"><span>Visual Studio</span></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i=1;
            foreach ($detalleEquiposCliente as $reg_equipos) {
            ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $reg_equipos["equipo"] ?></td>
                    <td><?= $reg_equipos["office"] ?></td>
                    <td><?= $reg_equipos["visio"] ?></td>
                    <td><?= $reg_equipos["project"] ?></td>
                    <td><?= $reg_equipos["visual"] ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>

<div style="width:99%; height:400px; display:none;" id="detalleEquipoServidor">
    <table class="tablap" id="tablaDetallesEquipoServidor" style="margin-top:-5px;">
        <thead>
            <tr style="background:#333; color:#fff;">
                <th align="center" valign="middle"><span>&nbsp;</span></th>
                <th align="center" valign="middle"><span>Device</span></th>
                <th align="center" valign="middle"><span>Windows Server</span></th>
                <th align="center" valign="middle"><span>SQL Server</span></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i=1;
            foreach ($detalleEquiposServidor as $reg_equipos) {
            ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $reg_equipos["equipo"] ?></td>
                    <td><?= $reg_equipos["os"] ?></td>
                    <td><?= $reg_equipos["SQLServer"] ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function(){
        $("#tablaDetallesEquipoCliente").tablesorter();
        $("#tablaDetallesEquipoCliente").tableHeadFixer(); 
        $("#tablaDetallesEquipoServidor").tablesorter();
        $("#tablaDetallesEquipoServidor").tableHeadFixer(); 
    });
    
    function mostrarListado(opc){
        if(opc === "cliente" && $('#detalleEquipoCliente').is(':visible')){
            $("#detalleEquipoCliente").hide();
            $("#export").hide();
        }else if(opc === "cliente" && !$('#detalleEquipoCliente').is(':visible')){
            $("#opcion").val("cliente");
            $("#detalleEquipoServidor").hide();
            $("#detalleEquipoCliente").show();
            $("#export").show();
        }else if(opc === "servidor" && $('#detalleEquipoServidor').is(':visible')){
            $("#detalleEquipoServidor").hide();
            $("#export").hide();
        }
        else if(opc === "servidor" && !$('#detalleEquipoServidor').is(':visible')){
            $("#opcion").val("servidor");
            $("#detalleEquipoCliente").hide();
            $("#detalleEquipoServidor").show();
            $("#export").show();
        }
    }
</script>