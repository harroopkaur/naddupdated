<!--inicio Procesadores-->
<br />

<!--<p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/LA_Tool_ADv6.5.rar">LA_Tool_ADv6.5.rar</a></p><br />
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
<br>-->
<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">Procesadores</legend>

    <form enctype="multipart/form-data" name="form_procesadores" id="form_procesadores" method="post">
        <input type="hidden" id="tokenCargarArchivoProcesadores" name="token">
        <input type="hidden" id="tipoArchivoProcesadores" name="tipoArchivo" value="procesadores">

        <div class="contenedor_archivo">
            <div class="input_archivo">
                <input type="text" class="url_file" name="url_fileProcesadores" disabled id="url_fileProcesadores" value="Nombre del Archivo...">
                <input type="file" class="archivo" name="archivoProcesadores" id="archivoProcesadores" accept=".csv">
                <input type="button" id="botonProcesadores" class="botones_m5 boton1" value="Buscar">
            </div>
            <div class="contenedor_boton_archivo">
                <input name="subirProcesadores" type="button" id="subirProcesadores" value="Cargar Archivo" class="botones_m5" />
            </div>
        </div>

        <div style="margin:0 auto; width:250px; overflow:hidden; background:#7F7F7F;">
            <progress id="barraProgresoProcesadores" style="clear:both;top:30px; width:100%; height:25px;" value="0" max="100"></progress>
        </div>
    </form>         

    <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
    <div id="cargandoProcesadores" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>

    <?php if($exitoProcesadores == 1){ ?>
        <div class="exito_archivo">Archivo procesado con éxito</div>
        <script>
            $.alert.open('info', "Archivo procesado con éxito", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php } else if($exitoProcesadores == 2){
    ?>
        <div class="error_archivo">No se pudo insertar todos los registros</div>
        <script>
            $.alert.open('alert', "No se pudo insertar todos los registros", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    }
    ?>
</fieldset>

<!--inicio modal Procesadores-->
<form id="formProcesarProcesadores" name="formProcesarProcesadores" method="post" action="despliegueOtros.php">
    <input name="insertarProcesadores" id="insertarProcesadores" type="hidden" value="1" >
    <input type="hidden" id="nombreArchivoProcesadores" name="nombreArchivoProcesadores">
    <input name="token" id="tokenProcesarProcesadores" type="hidden">

    <div class="modal-personal modal-personal-md" id="modal-procesadores">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Campos <p style="color:#06B6FF; display:inline">Procesadores</p></h1>
        </div>

        <style>
            tr{
                line-height:30px;
            }
        </style>
        <div class="modal-personal-body">

            <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Dato de Control: </span></th>
                    <td><select id="datoControlProcesadores" name="datoControlProcesadores"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>HostName: </span></th>
                    <td><select id="hostNameProcesadores" name="hostNameProcesadores"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Tipo de CPU: </span></th>
                    <td><select id="tipoCPU" name="tipoCPU"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>CPUs: </span></th>
                    <td><select id="CPUs" name="CPUs"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Cores: </span></th>
                    <td><select id="cores" name="cores"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Procesadores Lógicos: </span></th>
                    <td><select id="procesadoresLogicos" name="procesadoresLogicos"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Tipo de Escaneo: </span></th>
                    <td><select id="tipoEscaneo" name="tipoEscaneo"></select></td>
                </tr>
            </table>         
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirProcesadores">Salir</div>
            <div class="boton1 botones_m2" id="procesarProcesadores" style="float:right;">Procesar</div>
        </div>
    </div>
</form>
<!--fin modal Procesadores-->
<!--fin Procesadores-->

<script>
    $(document).ready(function(){
        /*inicio procesadores*/
        $("#botonProcesadores").click(function(){
            $("#archivoProcesadores").click();
        });
        
        $("#archivoProcesadores").change(function(e){
            $("#urlNombre").val("#url_fileProcesadores");
            if(addArchivoConsolidado(e) === false){
                $("#archivoProcesadores").val("");
                $("#archivoProcesadores").replaceWith($("#archivoProcesadores").clone(true));
            }
        });
        
        $("#subirProcesadores").click(function(){
            $("#tokenCargarArchivoProcesadores").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form_procesadores")[0]);	
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandlerProcesadores, true);
            ajax.addEventListener("load", completeHandlerProcesadores, false);
            ajax.addEventListener("error", errorHandlerProcesadores, false);
            ajax.addEventListener("abort", abortHandlerProcesadores, false);
            ajax.open("POST", "<?= $GLOBALS["domain_root"] ?>/microsoft/ajax/subirArchivoConsolidado.php", true);
            ajax.send(formData);
        });
        
        $("#salirProcesadores").click(function(){
            $("#nombreArchivoProcesadores").val("");
            $("#datoControlProcesadores").empty();
            $("#hostNameProcesadores").empty();
            $("#tipoCPU").empty();
            $("#CPUs").empty();
            $("#cores").empty();
            $("#procesadoresLogicos").empty();
            $("#tipoEscaneo").empty();
            $("#fondo1").hide();
            $("#modal-procesadores").hide();
            $("#archivoProcesadores").val("");
            $("#archivoProcesadores").replaceWith($("#archivoProcesadores").clone(true));
            $("#url_fileProcesadores").val("Nombre del Archivo...");
            $("#barraProgresoProcesadores").val(0);
        });
        
        $("#procesarProcesadores").click(function(){
            $("#fondo1").hide();
            $("#modal-procesadores").hide();
            if($("#datoControlProcesadores").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Dato de Control", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalProcesadores();
                    $("#datoControlProcesadores").focus();
                });
                return false;
            }
            
            if($("#hostNameProcesadores").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del HostName", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalProcesadores();
                    $("#hostNameProcesadores").focus();
                });
                return false;
            }

            if($("#tipoCPU").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de Tipo de CPU", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalProcesadores();
                    $("#tipoCPU").focus();
                });
                return false;
            }
            
            if($("#CPUs").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de CPUs", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalProcesadores();
                    $("#CPUs").focus();
                });
                return false;
            }
            
            if($("#cores").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de Cores", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalProcesadores();
                    $("#cores").focus();
                });
                return false;
            }
            
            if($("#procesadoresLogicos").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de Procesadores Lógicos", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalProcesadores();
                    $("#procesadoresLogicos").focus();
                });
                return false;
            }
            
            if($("#tipoEscaneo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de Tipo de Escaneo", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalProcesadores();
                    $("#tipoEscaneo").focus();
                });
                return false;
            }
            
            $("#formProcesarProcesadores").submit();
        });  
        /*fin procesadores*/
    });
    
    /*inicio procesadores*/
    function progressHandlerProcesadores(e){
        var porcentaje = (e.loaded / e.total) * 100;
        $("#barraProgresoProcesadores").val(Math.round(porcentaje)); 
    }

    function completeHandlerProcesadores(e){
        var data = [{resultado:true, sesion:true}];
        $("#barraProgresoProcesadores").val(100);
        res = e.target.responseText.split("*");
        
        if(parseInt(res[0]) === 0){
            data[0].sesion = false;
        } else if(parseInt(e.target.responseText) === 2){
            data[0].resultado = false;
        }
        
        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
        if(parseInt(res[0]) === 1){
            $("#nombreArchivoProcesadores").val(res[2]);
            $("#datoControlProcesadores").append(res[1]);
            $("#hostNameProcesadores").append(res[1]);
            $("#tipoCPU").append(res[1]);
            $("#CPUs").append(res[1]);
            $("#cores").append(res[1]);
            $("#procesadoresLogicos").append(res[1]);
            $("#tipoEscaneo").append(res[1]);
            $("#fondo1").show();
            $("#modal-procesadores").show();
            
        } else if(parseInt(res[0]) === 3){
            $("#barraProgresoProcesadores").val(0);
            $.alert.open('alert', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 4){
            $("#barraProgresoProcesadores").val(0);
            $.alert.open('alert', "Debe seleccionar el archivo antes de subirlo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 5){
            $("#barraProgresoProcesadores").val(0);
            $.alert.open('alert', "Los separadores válidos para archivo CSV son \",\" o \";\"", {'Aceptar' : 'Aceptar'}, function() {
            });
        }    
    }

    function errorHandlerProcesadores(){
        $.alert.open('error', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function abortHandlerProcesadores(){
        $.alert.open('alert', "Carga del archivo abortada", {'Aceptar' : 'Aceptar'}, function() {
        });
    }
    
    function mostrarModalProcesadores(){
        $("#fondo1").show();
        $("#modal-procesadores").show();
    }
    /*fin procesadores*/
</script>