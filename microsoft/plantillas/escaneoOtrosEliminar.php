<!--inicio escaneo-->
<br />

<!--<p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/LA_Tool_ADv6.5.rar">LA_Tool_ADv6.5.rar</a></p><br />
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
<br>-->
<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">Escaneo Equipos</legend>

    <form enctype="multipart/form-data" name="form_Escaneo" id="form_Escaneo" method="post">
        <input type="hidden" id="tokenCargarArchivoEscaneo" name="token">
        <input type="hidden" id="tipoArchivoEscaneo" name="tipoArchivo" value="escaneo">

        <div class="contenedor_archivo">
            <div class="input_archivo">
                <input type="text" class="url_file" name="url_fileEscaneo" disabled id="url_fileEscaneo" value="Nombre del Archivo...">
                <input type="file" class="archivo" name="archivoEscaneo" id="archivoEscaneo" accept=".csv">
                <input type="button" id="botonEscaneo" class="botones_m5 boton1" value="Buscar">
            </div>
            <div class="contenedor_boton_archivo">
                <input name="subirEscaneo" type="button" id="subirEscaneo" value="Cargar Archivo" class="botones_m5" />
            </div>
        </div>

        <div style="margin:0 auto; width:250px; overflow:hidden; background:#7F7F7F;">
            <progress id="barraProgresoEscaneo" style="clear:both;top:30px; width:100%; height:25px;" value="0" max="100"></progress>
        </div>
    </form>         

    <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
    <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>

    <?php if($exitoEscaneo == 1){ ?>
        <div class="exito_archivo">Archivo procesado con éxito</div>
        <script>
            $.alert.open('info', "Archivo procesado con éxito", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php } else if($exitoEscaneo == 2){
    ?>
        <div class="error_archivo">No se pudo insertar todos los registros</div>
        <script>
            $.alert.open('alert', "No se pudo insertar todos los registros", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    } else if($exitoEscaneo == 3){
    ?>
        <div class="error_archivo">No se pudo actualizar todos los registros del escaneo</div>
        <script>
            $.alert.open('alert', "No se pudo actualizar todos los registros del escaneo", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    }
    ?>
</fieldset>

<!--inicio modal Escaneo-->
<form id="formProcesarEscaneo" name="formProcesarEscaneo" method="post" action="despliegueOtros.php">
    <input name="insertarEscaneo" id="insertarEscaneo" type="hidden" value="1" >
    <input type="hidden" id="nombreArchivoEscaneo" name="nombreArchivoEscaneo">
    <input name="token" id="tokenProcesarEscaneo" type="hidden">

    <div class="modal-personal modal-personal-md" id="modal-Escaneo">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Campos <p style="color:#06B6FF; display:inline">Escaneo Equipos</p></h1>
        </div>

        <style>
            tr{
                line-height:30px;
            }
        </style>
        <div class="modal-personal-body">

            <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>HostName: </span></th>
                    <td><select id="hostNameEscaneo" name="hostNameEscaneo"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Status: </span></th>
                    <td><select id="status" name="status"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Error: </span></th>
                    <td><select id="error" name="error"></select></td>
                </tr>
            </table>         
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirEscaneo">Salir</div>
            <div class="boton1 botones_m2" id="procesarEscaneo" style="float:right;">Procesar</div>
        </div>
    </div>
</form>
<!--fin modal Usuario Equipo-->
<!--fin Usuario Equipo-->
        
<script>
    $(document).ready(function(){
        /*inicio UsuarioEquipo*/
        $("#botonEscaneo").click(function(){
            $("#archivoEscaneo").click();
        });
        
        $("#archivoEscaneo").change(function(e){
            $("#urlNombre").val("#url_fileEscaneo");
            if(addArchivoConsolidado(e) === false){
                $("#archivoEscaneo").val("");
                $("#archivoEscaneo").replaceWith($("#archivoEscaneo").clone(true));
            }
        });
        
        $("#subirEscaneo").click(function(){
            $("#tokenCargarArchivoEscaneo").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form_Escaneo")[0]);	
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandlerEscaneo, true);
            ajax.addEventListener("load", completeHandlerEscaneo, false);
            ajax.addEventListener("error", errorHandlerEscaneo, false);
            ajax.addEventListener("abort", abortHandlerEscaneo, false);
            ajax.open("POST", "<?= $GLOBALS["domain_root"] ?>/microsoft/ajax/subirArchivoConsolidado.php", true);
            ajax.send(formData);
        });
        
        $("#salirEscaneo").click(function(){
            $("#nombreArchivoEscaneo").val("");
            $("#hostNameEscaneo").empty();
            $("#status").empty();
            $("#error").empty();
            $("#archivoEscaneo").val("");
            $("#archivoEscaneo").replaceWith($("#archivoEscaneo").clone(true));
            $("#url_fileEscaneo").val("Nombre del Archivo...");
            $("#barraProgresoEscaneo").val(0);
        });
        
        $("#procesarEscaneo").click(function(){
            $("#fondo1").hide();
            $("#modal-Escaneo").hide();
          
            if($("#hostNameEscaneo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del HostName", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEscaneo();
                    $("#hostNameEscaneo").focus();
                });
                return false;
            }

            if($("#status").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Status", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEscaneo();
                    $("#status").focus();
                });
                return false;
            }
            
            if($("#error").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Error", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEscaneo();
                    $("#error").focus();
                });
                return false;
            }
                       
            $("#formProcesarEscaneo").submit();
        });  
        /*fin UsuarioEquipo*/
    });
    
    /*inicio UsuarioEquipo*/
    function progressHandlerEscaneo(e){
        var porcentaje = (e.loaded / e.total) * 100;
        $("#barraProgresoEscaneo").val(Math.round(porcentaje)); 
    }

    function completeHandlerEscaneo(e){
        var data = [{resultado:true, sesion:true}];
        $("#barraProgresoEscaneo").val(100);
        res = e.target.responseText.split("*");
        
        if(parseInt(res[0]) === 0){
            data[0].sesion = false;
        } else if(parseInt(e.target.responseText) === 2){
            data[0].resultado = false;
        }
        
        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
        if(parseInt(res[0]) === 1){
            $("#nombreArchivoEscaneo").val(res[2]);
            $("#hostNameEscaneo").append(res[1]);
            $("#status").append(res[1]);
            $("#error").append(res[1]);
            $("#fondo1").show();
            $("#modal-Escaneo").show();
            
        } else if(parseInt(res[0]) === 3){
            $("#barraProgresoEscaneo").val(0);
            $.alert.open('alert', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 4){
            $("#barraProgresoEscaneo").val(0);
            $.alert.open('alert', "Debe seleccionar el archivo antes de subirlo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 5){
            $("#barraProgresoEscaneo").val(0);
            $.alert.open('alert', "Los separadores válidos para archivo CSV son \",\" o \";\"", {'Aceptar' : 'Aceptar'}, function() {
            });
        }    
    }

    function errorHandlerEscaneo(){
        $.alert.open('error', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function abortHandlerEscaneo(){
        $.alert.open('alert', "Carga del archivo abortada", {'Aceptar' : 'Aceptar'}, function() {
        });
    }
    
    function mostrarModalEscaneo(){
        $("#fondo1").show();
        $("#modal-Escaneo").show();
    }
    /*fin UsuarioEquipo*/
</script>