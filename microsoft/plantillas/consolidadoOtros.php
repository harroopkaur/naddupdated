<!--inicio consolidado-->
<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">1.- Importar</legend>

    <form enctype="multipart/form-data" name="form_consolidado" id="form_consolidado" method="post">
        <input type="hidden" id="tokenCargarArchivoConsolidado" name="token">
        <input type="hidden" id="tipoArchivoConsolidado" name="tipoArchivo" value="consolidado">

        <div class="contenedor_archivo">
            <div class="input_archivo">
                <input type="text" class="url_file" name="url_fileConsolidado" disabled id="url_fileConsolidado" value="Nombre del Archivo...">
                <input type="file" class="archivo" name="archivoConsolidado" id="archivoConsolidado" accept=".csv">
                <input type="button" id="botonConsolidado" class="botones_m5 boton1" value="Buscar">
            </div>
            <div class="contenedor_boton_archivo">
                <input name="subirConsolidado" type="button" id="subirConsolidado" value="Cargar Archivo" class="botones_m5" />
            </div>
        </div>

        <div style="margin:0 auto; width:250px; overflow:hidden; background:#7F7F7F;">
            <progress id="barraProgresoConsolidado" style="clear:both;top:30px; width:100%; height:25px;" value="0" max="100"></progress>
        </div>
    </form>         

    <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
    <div id="cargandoConsolidado" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>

    <?php if($exitoConsolidado == 1){ ?>
        <div class="exito_archivo">Archivo procesado con éxito</div>
        <script>
            $.alert.open('info', "Archivo procesado con éxito", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php } else if($exitoConsolidado == 2){
    ?>
        <div class="error_archivo">No se pudo insertar todos los registros</div>
        <script>
            $.alert.open('alert', "No se pudo insertar todos los registros", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    } else if($exitoConsolidado == 3){
    ?>
        <div class="error_archivo">No se pudo actualizar todos los registros del resumen</div>
        <script>
            $.alert.open('alert', "No se pudo actualizar todos los registros del resumen", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    }
    ?>
        
    <form id="formProcesarConsolidado" name="formProcesarConsolidado" method="post" action="despliegueOtros.php">
        <input name="insertarConsolidado" id="insertarConsolidado" type="hidden" value="1" >
        <input type="hidden" id="nombreArchivoConsolidado" name="nombreArchivoConsolidado">
        <input name="token" id="tokenProcesarConsolidado" type="hidden">

        <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
            <tr>
                <th class="text-right bold" style="width:16.5%; padding-right:10px;"><span>Nombre Equipo: </span></th>
                <td style="width:16.5%;"><select id="nombreEquipo" name="nombreEquipo"></select></td>
                <td style="width:16.5%;"></td>
                <td style="width:16.5%;"></td>
                <td style="width:16.5%;"></td>
                <td style="width:16.5%;"></td>
            </tr>
            <tr>
                <th class="text-right bold" style="padding-right:10px;"><span>Software: </span></th>
                <td><select id="software" name="software"></select></td>
                <th class="text-right bold" style="padding-right:10px;"><span>edición: </span></th>
                <td><select id="edicion" name="edicion"></select></td>
                <th class="text-right bold" style="padding-right:10px;"><span>Versión: </span></th>
                <td><select id="version" name="version"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="padding-right:10px;"><span>Fecha de Instalación: </span></th>
                <td><select id="fechaInstal" name="fechaInstal"></select></td>
                <th class="text-right bold" style="padding-right:10px;"><span>Fecha de Último Uso: </span></th>
                <td><select id="fechaUltUso" name="fechaUltUso"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="padding-right:10px;"><span>Usuario del Equipo: </span></th>
                <td><select id="usuarioEquipo" name="usuarioEquipo"></select></td>
                <th class="text-right bold" style="padding-right:10px;"><span>Procesadores: </span></th>
                <td><select id="procesadores" name="procesadores"></select></td>
            </tr>
        </table>         
    </form>
</fieldset>

<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">2.- Vista Previa</legend>

    <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
        <tr>
            <th class="text-right bold" style="width:16.5%; padding-right:10px;"><span>Nombre Equipo: </span></th>
            <td style="width:16.5%;"><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgNombreEquipo"></td>
            <td style="width:16.5%;"></td>
            <td style="width:16.5%;"></td>
            <td style="width:16.5%;"></td>
            <td style="width:16.5%;"></td>
        </tr>
        <tr>
            <th class="text-right bold" style="padding-right:10px;"><span>Software: </span></th>
            <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgSoftware"></td>
            <th class="text-right bold" style="padding-right:10px;"><span>edición: </span></th>
            <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgEdicion"></td>
            <th class="text-right bold" style="padding-right:10px;"><span>Versión: </span></th>
            <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgVersion"></td>
        </tr>
        <tr>
            <th class="text-right bold" style="padding-right:10px;"><span>Fecha de Instalación: </span></th>
            <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgFechaInstal"></td>
            <th class="text-right bold" style="padding-right:10px;"><span>Fecha de Último Uso: </span></th>
            <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgFechaUltUso"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th class="text-right bold" style="padding-right:10px;"><span>Usuario del Equipo: </span></th>
            <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgUsuarioEquipo"></td>
            <th class="text-right bold" style="padding-right:10px;"><span>Procesadores: </span></th>
            <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png" id="imgProcesadores"></td>
            <td></td>
            <td></td>
        </tr>
    </table>        
</fieldset>

<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">3.- Cargar Detalle</legend>

    <div class="botones_m2 boton1" style="float:none; margin:0 auto; width:100px;" id="procesarConsolidado">Cargar Detalle</div>
</fieldset>

<!--inicio modal consolidado-->
<!--<form id="formProcesarConsolidado" name="formProcesarConsolidado" method="post" action="despliegueOtros.php">
    <input name="insertarConsolidado" id="insertarConsolidado" type="hidden" value="1" >
    <input type="hidden" id="nombreArchivoConsolidado" name="nombreArchivoConsolidado">
    <input name="token" id="tokenProcesarConsolidado" type="hidden">

    <div class="modal-personal modal-personal-md" id="modal-consolidado">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Campos <p style="color:#06B6FF; display:inline">Consolidado</p></h1>
        </div>

        <style>
            tr{
                line-height:30px;
            }
        </style>
        <div class="modal-personal-body">

            <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Dato de Control: </span></th>
                    <td><select id="datoControl" name="datoControl"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>HostName: </span></th>
                    <td><select id="hostName" name="hostName"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Registro: </span></th>
                    <td><select id="registro" name="registro"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Editor: </span></th>
                    <td><select id="editor" name="editor"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Versión: </span></th>
                    <td><select id="version" name="version"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Día de Instalación: </span></th>
                    <td><select id="diaInstalacion" name="diaInstalacion"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Software: </span></th>
                    <td><select id="software" name="software"></select></td>
                </tr>
            </table>         
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirConsolidado">Salir</div>
            <div class="boton1 botones_m2" id="procesarConsolidado" style="float:right;">Procesar</div>
        </div>
    </div>
</form>
<!--fin modal consolidado-->
<!--inicio Consolidado-->
  
<script>
    $(document).ready(function(){
        /*inicio consolidado*/
        $("#botonConsolidado").click(function(){
            $("#archivoConsolidado").click();
        });
        
        $("#archivoConsolidado").change(function(e){
            $("#urlNombre").val("#url_fileConsolidado");
            if(addArchivoConsolidado(e) === false){
                $("#archivoConsolidado").val("");
                $("#archivoConsolidado").replaceWith($("#archivoConsolidado").clone(true));
            }
        });
        
        $("#subirConsolidado").click(function(){
            $("#tokenCargarArchivoConsolidado").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form_consolidado")[0]);	
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandlerConsolidado, true);
            ajax.addEventListener("load", completeHandlerConsolidado, false);
            ajax.addEventListener("error", errorHandlerConsolidado, false);
            ajax.addEventListener("abort", abortHandlerConsolidado, false);
            ajax.open("POST", "<?= $GLOBALS["domain_root"] ?>/microsoft/ajax/subirArchivoConsolidado.php", true);
            ajax.send(formData);
        });
        
        /*$("#salirConsolidado").click(function(){
            $("#nombreArchivoConsolidado").val("");
            $("#datoControl").empty();
            $("#hostName").empty();
            $("#registro").empty();
            $("#editor").empty();
            $("#version").empty();
            $("#diaInstalacion").empty();
            $("#software").empty();
            $("#fondo1").hide();
            $("#modal-consolidado").hide();
            $("#archivoConsolidado").val("");
            $("#archivoConsolidado").replaceWith($("#archivoConsolidado").clone(true));
            $("#url_fileConsolidado").val("Nombre del Archivo...");
            $("#barraProgresoConsolidado").val(0);
        });*/
        
        $("#procesarConsolidado").click(function(){
            /*$("#fondo1").hide();
            $("#modal-consolidado").hide();*/
            if($("#nombreEquipo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Nombre de Equipo", {'Aceptar' : 'Aceptar'}, function() {
                    $("#nombreEquipo").focus();
                });
                return false;
            }
            
            if($("#software").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Software", {'Aceptar' : 'Aceptar'}, function() {
                    $("#software").focus();
                });
                return false;
            }

            if($("#edicion").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de la Edición", {'Aceptar' : 'Aceptar'}, function() {
                    $("#edicion").focus();
                });
                return false;
            }
            
            if($("#version").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de la Versión", {'Aceptar' : 'Aceptar'}, function() {
                    $("#version").focus();
                });
                return false;
            }
                        
            if($("#fechaInstal").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de la Fecha de Instalación", {'Aceptar' : 'Aceptar'}, function() {
                    $("#fechaInstal").focus();
                });
                return false;
            }
            
            if($("#fechaUltUso").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de la fecha de Último Uso", {'Aceptar' : 'Aceptar'}, function() {
                    $("#fechaUltUso").focus();
                });
                return false;
            }
            
            if($("#usuarioEquipo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Usuario del Equipo", {'Aceptar' : 'Aceptar'}, function() {
                    $("#usuarioEquipo").focus();
                });
                return false;
            }
            
            if($("#procesadores").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de Procesadores", {'Aceptar' : 'Aceptar'}, function() {
                    $("#procesadores").focus();
                });
                return false;
            }
            
            $("#formProcesarConsolidado").submit();
        });  
        
        $("#nombreEquipo").change(function(){
            if($("#nombreEquipo").val() === ""){
                $("#imgNombreEquipo").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgNombreEquipo").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });

        $("#software").change(function(){
            if($("#software").val() === ""){
                $("#imgSoftware").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgSoftware").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });
        
        $("#edicion").change(function(){
            if($("#edicion").val() === ""){
                $("#imgEdicion").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgEdicion").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });
 
        $("#version").change(function(){
            if($("#version").val() === ""){
                $("#imgVersion").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgVersion").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });

        $("#fechaInstal").change(function(){
            if($("#fechaInstal").val() === ""){
                $("#imgFechaInstal").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgFechaInstal").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });

        $("#fechaUltUso").change(function(){
            if($("#fechaUltUso").val() === ""){
                $("#imgFechaUltUso").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgFechaUltUso").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });
        
        $("#usuarioEquipo").change(function(){
            if($("#usuarioEquipo").val() === ""){
                $("#imgUsuarioEquipo").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgUsuarioEquipo").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });

        $("#procesadores").change(function(){
            if($("#procesadores").val() === ""){
                $("#imgProcesadores").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png");
            }else {
                $("#imgProcesadores").attr("src","<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png");
            }
        });
    });
    
    /*inicio consolidado*/
    function progressHandlerConsolidado(e){
        var porcentaje = (e.loaded / e.total) * 100;
        $("#barraProgresoConsolidado").val(Math.round(porcentaje)); 
    }

    function completeHandlerConsolidado(e){
        var data = [{resultado:true, sesion:true}];
        $("#barraProgresoConsolidado").val(100);
        res = e.target.responseText.split("*");
        
        if(parseInt(res[0]) === 0){
            data[0].sesion = false;
        } else if(parseInt(e.target.responseText) === 2){
            data[0].resultado = false;
        }
        
        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
        if(parseInt(res[0]) === 1){
            $("#nombreArchivoConsolidado").val(res[2]);
            $("#nombreEquipo").append(res[1]);
            $("#software").append(res[1]);
            $("#edicion").append(res[1]);
            $("#version").append(res[1]);
            $("#fechaInstal").append(res[1]);
            $("#fechaUltUso").append(res[1]);
            $("#usuarioEquipo").append(res[1]);
            $("#procesadores").append(res[1]);
            /*$("#fondo1").show();
            $("#modal-consolidado").show();*/
            
        } else if(parseInt(res[0]) === 3){
            $("#barraProgresoConsolidado").val(0);
            $.alert.open('alert', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 4){
            $("#barraProgresoConsolidado").val(0);
            $.alert.open('alert', "Debe seleccionar el archivo antes de subirlo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 5){
            $("#barraProgresoConsolidado").val(0);
            $.alert.open('alert', "Los separadores válidos para archivo CSV son \",\" o \";\"", {'Aceptar' : 'Aceptar'}, function() {
            });
        }    
    }

    function errorHandlerConsolidado(){
        $.alert.open('error', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function abortHandlerConsolidado(){
        $.alert.open('alert', "Carga del archivo abortada", {'Aceptar' : 'Aceptar'}, function() {
        });
    }
    
    /*function mostrarModalConsolidado(){
        $("#fondo1").show();
        $("#modal-consolidado").show();
    }*/
    /*fin consolidado*/
</script>