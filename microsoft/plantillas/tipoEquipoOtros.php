<!--inicio Procesadores-->
<br />

<!--<p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/LA_Tool_ADv6.5.rar">LA_Tool_ADv6.5.rar</a></p><br />
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
<br>-->
<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">Tipo de Equipo</legend>

    <form enctype="multipart/form-data" name="form_TipoEquipo" id="form_TipoEquipo" method="post">
        <input type="hidden" id="tokenCargarArchivoTipoEquipo" name="token">
        <input type="hidden" id="tipoArchivoTipoEquipo" name="tipoArchivo" value="tipoEquipo">
        
        <div class="contenedor_archivo">
            <div class="input_archivo">
                <input type="text" class="url_file" name="url_fileTipoEquipo" disabled id="url_fileTipoEquipo" value="Nombre del Archivo...">
                <input type="file" class="archivo" name="archivoTipoEquipo" id="archivoTipoEquipo" accept=".csv">
                <input type="button" id="botonTipoEquipo" class="botones_m5 boton1" value="Buscar">
            </div>
            <div class="contenedor_boton_archivo">
                <input name="subirTipoEquipo" type="button" id="subirTipoEquipo" value="Cargar Archivo" class="botones_m5" />
            </div>
        </div>

        <div style="margin:0 auto; width:250px; overflow:hidden; background:#7F7F7F;">
            <progress id="barraProgresoTipoEquipo" style="clear:both;top:30px; width:100%; height:25px;" value="0" max="100"></progress>
        </div>
    </form>         

    <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
    <div id="cargandoTipoEquipo" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>

    <?php if($exitoTipoEquipo == 1){ ?>
        <div class="exito_archivo">Archivo procesado con éxito</div>
        <script>
            $.alert.open('info', "Archivo procesado con éxito", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php } else if($exitoTipoEquipo == 2){
    ?>
        <div class="error_archivo">No se pudo insertar todos los registros</div>
        <script>
            $.alert.open('alert', "No se pudo insertar todos los registros", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    }
    ?>
</fieldset>

<!--inicio modal tipoEquipo-->
<form id="formProcesarTipoEquipo" name="formProcesarTipoEquipo" method="post" action="despliegueOtros.php">
    <input name="insertarTipoEquipo" id="insertarTipoEquipo" type="hidden" value="1" >
    <input type="hidden" id="nombreArchivoTipoEquipo" name="nombreArchivoTipoEquipo">
    <input name="token" id="tokenProcesarTipoEquipo" type="hidden">

    <div class="modal-personal modal-personal-md" id="modal-TipoEquipo">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Campos <p style="color:#06B6FF; display:inline">Tipo de Equipo</p></h1>
        </div>

        <style>
            tr{
                line-height:30px;
            }
        </style>
        <div class="modal-personal-body">

            <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Dato de Control: </span></th>
                    <td><select id="datoControlTipoEquipo" name="datoControlTipoEquipo"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>HostName: </span></th>
                    <td><select id="hostNameTipoEquipo" name="hostNameTipoEquipo"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Fabricante: </span></th>
                    <td><select id="fabricante" name="fabricante"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Modelo: </span></th>
                    <td><select id="modelo" name="modelo"></select></td>
                </tr>
            </table>         
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirTipoEquipo">Salir</div>
            <div class="boton1 botones_m2" id="procesarTipoEquipo" style="float:right;">Procesar</div>
        </div>
    </div>
</form>
<!--fin modal Procesadores-->
<!--fin Procesadores-->

<script>
    $(document).ready(function(){
        /*inicio procesadores*/
        $("#botonTipoEquipo").click(function(){
            $("#archivoTipoEquipo").click();
        });
        
        $("#archivoTipoEquipo").change(function(e){
            $("#urlNombre").val("#url_fileTipoEquipo");
            if(addArchivoConsolidado(e) === false){
                $("#archivoTipoEquipo").val("");
                $("#archivoTipoEquipo").replaceWith($("#archivoTipoEquipo").clone(true));
            }
        });
        
        $("#subirTipoEquipo").click(function(){
            $("#tokenCargarArchivoTipoEquipo").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form_TipoEquipo")[0]);	
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandlerTipoEquipo, true);
            ajax.addEventListener("load", completeHandlerTipoEquipo, false);
            ajax.addEventListener("error", errorHandlerTipoEquipo, false);
            ajax.addEventListener("abort", abortHandlerTipoEquipo, false);
            ajax.open("POST", "<?= $GLOBALS["domain_root"] ?>/microsoft/ajax/subirArchivoConsolidado.php", true);
            ajax.send(formData);
        });
        
        $("#salirTipoEquipo").click(function(){
            $("#nombreArchivoTipoEquipo").val("");
            $("#datoControlTipoEquipo").empty();
            $("#hostNameTipoEquipo").empty();
            $("#fabricante").empty();
            $("#modelo").empty();
            $("#fondo1").hide();
            $("#modal-TipoEquipo").hide();
            $("#archivoTipoEquipo").val("");
            $("#archivoTipoEquipo").replaceWith($("#archivoTipoEquipo").clone(true));
            $("#url_fileTipoEquipo").val("Nombre del Archivo...");
            $("#barraProgresoTipoEquipo").val(0);
        });
        
        $("#procesarTipoEquipo").click(function(){
            $("#fondo1").hide();
            $("#modal-TipoEquipo").hide();
            if($("#datoControlTipoEquipo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Dato de Control", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalTipoEquipo();
                    $("#datoControlTipoEquipo").focus();
                });
                return false;
            }
            
            if($("#hostNameTipoEquipo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del HostName", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalTipoEquipo();
                    $("#hostNameTipoEquuipo").focus();
                });
                return false;
            }

            if($("#fabricante").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Fabricante", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalTipoEquipo();
                    $("#fabricante").focus();
                });
                return false;
            }
            
            if($("#modelo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Modelo", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalTipoEquipo();
                    $("#modelo").focus();
                });
                return false;
            }
            
            $("#formProcesarTipoEquipo").submit();
        });  
        /*fin procesadores*/
    });
    
    /*inicio procesadores*/
    function progressHandlerTipoEquipo(e){
        var porcentaje = (e.loaded / e.total) * 100;
        $("#barraProgresoTipoEquipo").val(Math.round(porcentaje)); 
    }

    function completeHandlerTipoEquipo(e){
        var data = [{resultado:true, sesion:true}];
        $("#barraProgresoTipoEquipo").val(100);
        res = e.target.responseText.split("*");
        
        if(parseInt(res[0]) === 0){
            data[0].sesion = false;
        } else if(parseInt(e.target.responseText) === 2){
            data[0].resultado = false;
        }
        
        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
        if(parseInt(res[0]) === 1){
            $("#nombreArchivoTipoEquipo").val(res[2]);
            $("#datoControlTipoEquipo").append(res[1]);
            $("#hostNameTipoEquipo").append(res[1]);
            $("#fabricante").append(res[1]);
            $("#modelo").append(res[1]);
            $("#fondo1").show();
            $("#modal-TipoEquipo").show();
            
        } else if(parseInt(res[0]) === 3){
            $("#barraProgresoTipoEquipo").val(0);
            $.alert.open('alert', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 4){
            $("#barraProgresoTipoEquipo").val(0);
            $.alert.open('alert', "Debe seleccionar el archivo antes de subirlo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 5){
            $("#barraProgresoTipoEquipo").val(0);
            $.alert.open('alert', "Los separadores válidos para archivo CSV son \",\" o \";\"", {'Aceptar' : 'Aceptar'}, function() {
            });
        }    
    }

    function errorHandlerTipoEquipo(){
        $.alert.open('error', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function abortHandlerTipoEquipo(){
        $.alert.open('alert', "Carga del archivo abortada", {'Aceptar' : 'Aceptar'}, function() {
        });
    }
    
    function mostrarModalTipoEquipo(){
        $("#fondo1").show();
        $("#modal-procesadores").show();
    }
    /*fin procesadores*/
</script>