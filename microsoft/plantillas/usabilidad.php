<table class="tablap">
    <tr style="background:#333; color:#fff;">
        <th align="center" valign="middle">Usabilidad</th>
        <th align="center"  valign="middle">Clientes</th>
        <th align="center" valign="middle">%</th>
        <th align="center" valign="middle">Servidores</th>
        <th align="center" valign="middle">%</th>
    </tr>
    <tr>
        <td align="center" valign="middle">En Uso</td>
        <td align="center" valign="middle"><?= $total_1 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct1 = 0;
            if($tclient > 0){
                $porct1 = ($total_1 / $tclient) * 100;
            }
            echo round($porct1);
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_4 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct11 = 0;
            if($tserver > 0){
                $porct11 = ($total_4 / $tserver) * 100;
            }
            echo round($porct11);
            ?>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle">Probablemente en Uso</td>
        <td align="center" valign="middle"><?= $total_2 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct2 = 0;
            if($tclient > 0){
                $porct2 = ($total_2 / $tclient) * 100;
            }
            echo round($porct2);
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_5 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct22 = 0;
            if($tserver > 0){
                $porct22 = ($total_5 / $tserver) * 100;
            }
            echo round($porct22);
            ?>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle">Obsoleto</td>
        <td align="center" valign="middle"><?= $total_3 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct3 = 0;
            if($tclient > 0){
                $porct3 = ($total_3 / $tclient) * 100;
            }
            echo round($porct3);
            ?>
        </td>
        <td align="center" valign="middle"><?= $total_6 ?></td>
        <td align="center" valign="middle">
            <?php
            $porct33 = 0;
            if($tserver > 0){
                $porct33 = ($total_6 / $tserver) * 100;
            }
            echo round($porct33);
            ?>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle"><strong style="font-weight:bold;">Gran total</strong></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $tclient ?></strong></td>
        <td align="center" valign="middle"></td>
        <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $tserver ?></strong></td>
        <td align="center" valign="middle"></td>
    </tr>
</table>

<br>

<div style="width:99%; height:400px;">
    <table class="tablap" id="tablaAlcance" style="margin-top:-5px;">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>Nombre Equipo</span></th>
            <th  align="center" valign="middle"><span>Tipo</span></th>
            <th  align="center" valign="middle"><span>Usabilidad</span></th>
            <th  align="center" valign="middle"><span>Escaneado</span></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        foreach ($listar_equipos as $reg_equipos2) {
        ?>
            <tr>
                <td align="center"><?= $i ?></td>
                <td ><?= $reg_equipos2["equipo"] ?></td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["tipo"] == 1) {
                        echo 'Cliente';
                    } else {
                        echo 'Servidor';
                    }
                    ?>

                </td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["rango"] == 1){
                        echo 'En Uso';
                    }
                    else if ($reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {                                                               
                        echo 'Probablemente en uso';
                    } else {
                        echo 'Obsoleto';
                    }
                    ?>
                </td>
                <td align="center">
                    <?php
                    if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3){
                        echo 'Si';
                    }
                    else{
                        echo 'No';
                    }
                    ?>
                </td>
            </tr>
        <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
</div>