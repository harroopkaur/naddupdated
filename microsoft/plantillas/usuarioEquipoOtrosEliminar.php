<!--inicio Usuarios Equipo-->
<br />

<!--<p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/LA_Tool_ADv6.5.rar">LA_Tool_ADv6.5.rar</a></p><br />
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
<br>-->
<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">Usuario-Equipo</legend>

    <form enctype="multipart/form-data" name="form_UsuarioEquipo" id="form_UsuarioEquipo" method="post">
        <input type="hidden" id="tokenCargarArchivoUsuarioEquipo" name="token">
        <input type="hidden" id="tipoArchivoUsuarioEquipo" name="tipoArchivo" value="usuarioEquipo">

        <div class="contenedor_archivo">
            <div class="input_archivo">
                <input type="text" class="url_file" name="url_fileUsuarioEquipo" disabled id="url_fileUsuarioEquipo" value="Nombre del Archivo...">
                <input type="file" class="archivo" name="archivoUsuarioEquipo" id="archivoUsuarioEquipo" accept=".csv">
                <input type="button" id="botonUsuarioEquipo" class="botones_m5 boton1" value="Buscar">
            </div>
            <div class="contenedor_boton_archivo">
                <input name="subirUsuarioEquipo" type="button" id="subirUsuarioEquipo" value="Cargar Archivo" class="botones_m5" />
            </div>
        </div>

        <div style="margin:0 auto; width:250px; overflow:hidden; background:#7F7F7F;">
            <progress id="barraProgresoUsuarioEquipo" style="clear:both;top:30px; width:100%; height:25px;" value="0" max="100"></progress>
        </div>
    </form>         

    <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
    <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>

    <?php if($exitoUsuarioEquipo == 1){ ?>
        <div class="exito_archivo">Archivo procesado con éxito</div>
        <script>
            $.alert.open('info', "Archivo procesado con éxito", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php } else if($exitoUsuarioEquipo == 2){
    ?>
        <div class="error_archivo">No se pudo insertar todos los registros</div>
        <script>
            $.alert.open('alert', "No se pudo insertar todos los registros", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    }
    ?>
</fieldset>

<!--inicio modal Usuario Equipo-->
<form id="formProcesarUsuarioEquipo" name="formProcesarUsuarioEquipo" method="post" action="despliegueOtros.php">
    <input name="insertarUsuarioEquipo" id="insertarUsuarioEquipo" type="hidden" value="1" >
    <input type="hidden" id="nombreArchivoUsuarioEquipo" name="nombreArchivoUsuarioEquipo">
    <input name="token" id="tokenProcesarUsuarioEquipo" type="hidden">

    <div class="modal-personal modal-personal-md" id="modal-UsuarioEquipo">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Campos <p style="color:#06B6FF; display:inline">Usuario-Equipo</p></h1>
        </div>

        <style>
            tr{
                line-height:30px;
            }
        </style>
        <div class="modal-personal-body">

            <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Dato de Control: </span></th>
                    <td><select id="datoControlUsuarioEquipo" name="datoControlUsuarioEquipo"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>HostName: </span></th>
                    <td><select id="hostNameUsuarioEquipo" name="hostNameUsuarioEquipo"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Dominio: </span></th>
                    <td><select id="dominio" name="dominio"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Usuario: </span></th>
                    <td><select id="usuario" name="usuario"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>IP: </span></th>
                    <td><select id="ip" name="ip"></select></td>
                </tr>
            </table>         
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirUsuarioEquipo">Salir</div>
            <div class="boton1 botones_m2" id="procesarUsuarioEquipo" style="float:right;">Procesar</div>
        </div>
    </div>
</form>
<!--fin modal Usuario Equipo-->
<!--fin Usuario Equipo-->
        
<script>
    $(document).ready(function(){
        /*inicio UsuarioEquipo*/
        $("#botonUsuarioEquipo").click(function(){
            $("#archivoUsuarioEquipo").click();
        });
        
        $("#archivoUsuarioEquipo").change(function(e){
            $("#urlNombre").val("#url_fileUsuarioEquipo");
            if(addArchivoConsolidado(e) === false){
                $("#archivoUsuarioEquipo").val("");
                $("#archivoUsuarioEquipo").replaceWith($("#archivoUsuarioEquipo").clone(true));
            }
        });
        
        $("#subirUsuarioEquipo").click(function(){
            $("#tokenCargarArchivoUsuarioEquipo").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form_UsuarioEquipo")[0]);	
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandlerUsuarioEquipo, true);
            ajax.addEventListener("load", completeHandlerUsuarioEquipo, false);
            ajax.addEventListener("error", errorHandlerUsuarioEquipo, false);
            ajax.addEventListener("abort", abortHandlerUsuarioEquipo, false);
            ajax.open("POST", "<?= $GLOBALS["domain_root"] ?>/microsoft/ajax/subirArchivoConsolidado.php", true);
            ajax.send(formData);
        });
        
        $("#salirUsuarioEquipo").click(function(){
            $("#nombreArchivoUsuarioEquipo").val("");
            $("#datoControlUsuarioEquipo").empty();
            $("#hostNameUsuarioEquipo").empty();
            $("#dominio").empty();
            $("#usuario").empty();
            $("#ip").empty();
            $("#fondo1").hide();
            $("#modal-UsuarioEquipo").hide();
            $("#archivoUsuarioEquipo").val("");
            $("#archivoUsuarioEquipo").replaceWith($("#archivoUsuarioEquipo").clone(true));
            $("#url_fileUsuarioEquipo").val("Nombre del Archivo...");
            $("#barraProgresoUsuarioEquipo").val(0);
        });
        
        $("#procesarUsuarioEquipo").click(function(){
            $("#fondo1").hide();
            $("#modal-UsuarioEquipo").hide();
            if($("#datoControlUsuarioEquipo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Dato de Control", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalUsuarioEquipo();
                    $("#datoControlUsuarioEquipo").focus();
                });
                return false;
            }
            
            if($("#hostNameUsuarioEquipo").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del HostName", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalUsuarioEquipo();
                    $("#hostNameUsuarioEquipo").focus();
                });
                return false;
            }

            if($("#dominio").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Dominio", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalUsuarioEquipo();
                    $("#dominio").focus();
                });
                return false;
            }
            
            if($("#usuario").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo del Usuario", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalUsuarioEquipo();
                    $("#usuario").focus();
                });
                return false;
            }
            
            if($("#ip").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de la IP", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalUsuarioEquipo();
                    $("#ip").focus();
                });
                return false;
            }
            
            $("#formProcesarUsuarioEquipo").submit();
        });  
        /*fin UsuarioEquipo*/
    });
    
    /*inicio UsuarioEquipo*/
    function progressHandlerUsuarioEquipo(e){
        var porcentaje = (e.loaded / e.total) * 100;
        $("#barraProgresoUsuarioEquipo").val(Math.round(porcentaje)); 
    }

    function completeHandlerUsuarioEquipo(e){
        var data = [{resultado:true, sesion:true}];
        $("#barraProgresoUsuarioEquipo").val(100);
        res = e.target.responseText.split("*");
        
        if(parseInt(res[0]) === 0){
            data[0].sesion = false;
        } else if(parseInt(e.target.responseText) === 2){
            data[0].resultado = false;
        }
        
        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
        if(parseInt(res[0]) === 1){
            $("#nombreArchivoUsuarioEquipo").val(res[2]);
            $("#datoControlUsuarioEquipo").append(res[1]);
            $("#hostNameUsuarioEquipo").append(res[1]);
            $("#dominio").append(res[1]);
            $("#usuario").append(res[1]);
            $("#ip").append(res[1]);
            $("#fondo1").show();
            $("#modal-UsuarioEquipo").show();
            
        } else if(parseInt(res[0]) === 3){
            $("#barraProgresoUsuarioEquipo").val(0);
            $.alert.open('alert', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 4){
            $("#barraProgresoUsuarioEquipo").val(0);
            $.alert.open('alert', "Debe seleccionar el archivo antes de subirlo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 5){
            $("#barraProgresoUsuarioEquipo").val(0);
            $.alert.open('alert', "Los separadores válidos para archivo CSV son \",\" o \";\"", {'Aceptar' : 'Aceptar'}, function() {
            });
        }    
    }

    function errorHandlerUsuarioEquipo(){
        $.alert.open('error', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function abortHandlerUsuarioEquipo(){
        $.alert.open('alert', "Carga del archivo abortada", {'Aceptar' : 'Aceptar'}, function() {
        });
    }
    
    function mostrarModalUsuarioEquipo(){
        $("#fondo1").show();
        $("#modal-UsuarioEquipo").show();
    }
    /*fin UsuarioEquipo*/
</script>