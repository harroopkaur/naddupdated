<div id="ttabla1"  style="display:none; width:99%; margin:10px; float:left;">
    <table class="tablap">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>OS</span></th>
            <th  align="center" valign="middle"><span>Office</span></th>
            <th  align="center" valign="middle"><span>Visio</span></th>
            <th  align="center" valign="middle"><span>Project</span></th>
            <th  align="center" valign="middle"><span>Visual Studio</span></th>
            <th  align="center" valign="middle"><span>Otros</span></th>
            <th  align="center" valign="middle"><span>%</span></th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th align="center"><span>Clientes</span></th>
                <td class="text-center"><?= $cantDesusoOS ?></td>
                <td class="text-center"><?= $cantDesusoOffice ?></td>
                <td class="text-center"><?= $cantDesusoVisio ?></td>
                <td class="text-center"><?= $cantDesusoProject ?></td>
                <td class="text-center"><?= $cantDesusoVisualStudio ?></td>
                <td class="text-center"><?= $cantDesusoOtros ?></td>
                <td class="text-center"><?= $porcentajeDesusoCliente  ?></td>
            </tr>
        </tbody>
    </table>
    
    <br>
    
    <table class="tablap">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>Windows Server</span></th>
            <th  align="center" valign="middle"><span>SQL Server</span></th>
            <th  align="center" valign="middle"><span>%</span></th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th align="center"><span>Servidores</span></th>
                <td class="text-center"><?= $cantDesusoWindowsServer ?></td>
                <td class="text-center"><?= $cantDesusoSQLServer ?></td>
                <td class="text-center"><?= $porcentajeDesusoCliente  ?></td>
            </tr>
        </tbody>
    </table>

    <br>
    
    <table class="tablap" id="tablaDesusoCliente">
        <thead>
        <tr>
            <th align="center" valign="middle" colspan="5"><span>Clientes</span></th>
        </tr>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>Familia</span></th>
            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
            <th  align="center" valign="middle"><span>Cantidad</span></th>
            <th  align="center" valign="middle"><span>%</span></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            foreach($productosDesusoCliente as $row){
            ?>
                <tr>
                    <td class="text-left"><?= $row["familia"] ?></td>
                    <td class="text-left"><?= $row["edicion"] ?></td>
                    <td class="text-left"><?= $row["version"] ?></td>
                    <td class="text-center"><?= $row["cantidad"] ?></td>
                    <td class="text-center"><?= round($row["cantidad"] * 100 / $cantDesusoOS, 0) ?></td>
                </tr>
            <?php 
            }
            ?>
        </tbody>
    </table>

    <br>
    
    <table class="tablap" id="tablaDesusoServidor">
        <thead>
        <tr>
            <th align="center" valign="middle" colspan="5"><span>Servidores</span></th>
        </tr>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>Familia</span></th>
            <th  align="center" valign="middle"><span>Edici&oacute;n</span></th>
            <th  align="center" valign="middle"><span>Versi&oacute;n</span></th>
            <th  align="center" valign="middle"><span>Cantidad</span></th>
            <th  align="center" valign="middle"><span>%</span></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            foreach($productosDesusoServidor as $row){
            ?>
                <tr>
                    <td class="text-left"><?= $row["familia"] ?></td>
                    <td class="text-left"><?= $row["edicion"] ?></td>
                    <td class="text-left"><?= $row["version"] ?></td>
                    <td class="text-center"><?= $row["cantidad"] ?></td>
                    <td class="text-center"><?= round($row["cantidad"] * 100 / $cantDesusoWindowsServer, 0) ?></td>
                </tr>
            <?php 
            }
            ?>
        </tbody>
    </table>
</div>