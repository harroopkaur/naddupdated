<div style="padding:20px; overflow:hidden;">
    <table style="width:100%;" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap">
        <thead>
            <tr>
                <th class="text-center" style="width:60%">Descripción</th> 
                <th class="text-center" style="width:20%">Fecha</th> 
                <th class="text-center" style="width:50px;">Detalle</th> 
                <th class="text-center" style="width:50px;">Descargar</th>
            </tr>
        </thead>
        <tbody id="contTabla">
            <?php 
            $i = 0;
            foreach($listado as $row){
            ?>
                <tr>
                    <td><?= $row["descripcion"] ?></td>
                    <td><?= $general->muestraFechaHora($row["fecha"]) ?></td>
                    <td class="text-center"><a href='#' onclick='ver("<?= $row['id'] ?>")'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_027_search.png' border='0' alt='Ver' title='Ver' /></a></td>
                    <td class="text-center"><a href='#' onclick='descargar("<?= $row['ruta'] ?>")'><img src='<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_181_download_alt.png' border='0' alt='Descargar' title='Descargar' /></a></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
    
    <div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>
    
    <div class="modal-personal modal-personal-lg" id="modal-trueUp">
        <div class="modal-personal-head">
            <h1 class="bold" style="margin:20px; font-size:20px; text-align:center;">Resumen Ejecutivo: <p style="color:#06B6FF; display:inline">Propuesta True Up para Presupuesto</p></h1>
        </div>
        <div class="modal-personal-body" style="width:96%; height:400px; overflow-x:auto;">
            <table style="width:1250px; margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th>Cliente:</th>
                    <td id="cliente"></td>
                </tr>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th>Fecha:</th>
                    <td id="fecha"></td>
                </tr>
            </table>

            <br>

            <table class="tablap" style="width:1250px">
                <thead>
                    <tr>
                        <th rowspan="2" class="rowMiddle text-center">Prodcuto</th>
                        <th rowspan="2" class="rowMiddle text-center">Edición</th>
                        <th rowspan="2" class="rowMiddle text-center">Versión</th>
                        <th rowspan="2" class="rowMiddle text-center">Precio Estimado</th>
                        <th colspan="2" class="text-center">Actual</th>
                        <th colspan="2" class="text-center">LA Proposed</th>
                        <th rowspan="2" class="rowMiddle text-center">Comentarios</th>
                    </tr>
                    <tr>
                        <th class="text-center">Cantidad</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">Cantidad</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody id="bodyTable">
                </tbody>
            </table>            
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirListado">Salir</div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#salirListado").click(function(){
            $("#fondo1").hide();
            $("#modal-trueUp").hide();
        });
    }); 
    
    function descargar(archivo){
        window.open("<?= $GLOBALS["domain_root"] ?>/adminweb/clientes/trueUp/<?= $_SESSION["client_id"] ?>/" + archivo);
    }
    
    function ver(id){
        $.post("ajax/detalleTrueUp.php", { id : id, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/adminweb/js/validarSesion.js"); ?>
            
            $("#cliente").empty();
            $("#cliente").append(data[0].cliente);
            $("#fecha").empty();
            $("#fecha").append(data[0].fecha);
            $("#bodyTable").empty();
            $("#bodyTable").append(data[0].tabla);
            
            $("#fondo1").show();
            $("#modal-trueUp").show();
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
</script>