<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:20px; margin-bottom:20px;">
    <p class="text-center" style="font-size:30px; line-height:35px; font-weight:bold;">Troubleshooting</p>
    
    <br>
    <div id="paso1" style="<?php if($buscarResultado == 1){ echo 'display:none;'; } ?>">
        <p>Estimado cliente, le pedimos descargue la Guia Paso a Paso: <a href="<?= $GLOBALS["domain_root"] . "/microsoft/Guia de Uso Troubleshooting.pdf" ?>" class="link1" target="_blank">GUIA PASO A PASO</a> e ir verificando cada 
        uno de los puntos a traves del siguiente Checklist, se sugiere hacer la verificacion en una muestra al 
        azar no mayor a 10 equipos de la siguiente lista (<a href="#" class="link1" onclick="mostrarListado();">Lista de Equipos con Error</a>):</p>

        <br>
        <table class="tablap" style="width:80%">
            <thead>
                <tr>
                    <th>Nro</th>
                    <th>Qué Verificar</th>
                    <th>Cómo Verificar</th>
                    <th>Chequeo</th>
                </tr>
            </thead>
            <tbody id="tablaVerif">
                <tr>
                    <td class="text-center rowMiddle">1</td>
                    <td>Verificar que exista conectividad de Red al equipo que se desea escanear</td>
                    <td>Realizar ping al equipo</td>
                    <td class="text-center rowMiddle"><input type="checkbox" id="check0" name="check[]"></td>
                </tr>
                <tr>
                    <td class="text-center rowMiddle">2</td>
                    <td>Verificar que el equipo se conecte al Control del Instrumental de administración de 
                        Windows (<span class="bold">WMI</span>)</td>
                    <td>Ejecutar WBEMTEST</td>
                    <td class="text-center rowMiddle"><input type="checkbox" id="check1" name="check[]"></td>
                </tr>
                <tr>
                    <td class="text-center rowMiddle">3</td>
                    <td>Verificar disponibilidad del puerto 135</td>
                    <td>Realizar un telnet al puerto 135</td>
                    <td class="text-center rowMiddle"><input type="checkbox" id="check2" name="check[]"></td>
                </tr>
                <tr>
                    <td class="text-center rowMiddle">4</td>
                    <td>Verificar que el firewall de Windows no esté bloqueando la comunicación con el Control 
                        del Instrumental de administración de Windows (<span class="bold">WMI</span>)</td>
                    <td>Deshabilitar el firewall de Windows en el equipo a escanear</td>
                    <td class="text-center rowMiddle"><input type="checkbox" id="check3" name="check[]"></td>
                </tr>
                <tr>
                    <td class="text-center rowMiddle">5</td>
                    <td>Aplicar políticas de grupo a nivel de Active Directory para permitir el acceso a Control
                        del Instrumental de administración de Windows (<span class="bold">WMI</span>)</td>
                    <td>Establecer una política de grupo a través del dominio</td>
                    <td class="text-center rowMiddle"><input type="checkbox" id="check4" name="check[]"></td>
                </tr>
                <tr>
                    <td class="text-center rowMiddle">6</td>
                    <td>Verificar si el script de LA Tool puede escanear los equipos una vez aplicadas las 
                        soluciones anteriores</td>
                    <td>Verificar mediante el script LA Tool el resultado de escaneo de 10 equipos</td>
                    <td class="text-center rowMiddle"><input type="checkbox" id="check5" name="check[]"></td>
                </tr>
            </tbody>
        </table>
        
        <br>
        <div style="float:right;"><div class="botones_m2 boton1 pointer" id="verificacion">Pasar a Verificación</div></div>
    </div>
    
    <div id="paso2" style="<?php if(!isset($_POST["buscarResultado"]) || $_POST["buscarResultado"] != 1){ echo 'display:none'; }?>">
        <p class="text-center bold" style="font-size:20px;">Verificar Resolución de Errores</p><br>
        
        <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos 
        a continuaci&oacute;n:</p><br>
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo 
        <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/LA_Tool_Localv6.4.rar">LA_Tool_Localv6.4.rar</a></p><br />
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos 
        contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo 
        click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Ejecutar la herramienta 
        renombrando en Equipos.txt la lista de 10 equipos con error en el punto anterior separadas por comas una debajo de la otra</p><br>
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Click derecho 
        sobre el archivo "<span class="bold">LA_Tool.vbe</span>". Seleccionar la opción "<span class="bold">
        Open with Command Prompt</span>" o "<span class="bold">Ejecutar con Símbolo de Sistema</span>"</p><br>
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se iniciará una ventana de Command Prompt
        que muestra el progreso de la ejecución del escaneo de equipos.  Esta ventana se cerrará automáticamente
        una vez finalice el escaneo.  Esto puede demorar unos minutos en finalizar, dependiendo de las máquinas conectadas
        a la red y la velocidad de conexión</p><br>
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Se generará un (1) archivo llamado
        LAE_Output.csv y una (1) carpeta llamada Resultados, dentro de la misma se encontrará un archivo llamado
        "<span class="bold">LAD_Output[fechaejecucion].rar</span>" con la fecha de ejecución de la herramienta.</p><br>
        
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Subir el archivo 
        "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> 
        El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje 
        "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
        
        <br>
        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">LAD_Output</legend>
            
            <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="troubleshooting.php" >
                <input type="hidden" id="buscarResultado" name="buscarResultado" value="1">
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo" id="archivo" accept=".rar">
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
            <?php if($exito == 1){ ?>
                <div class="exito_archivo">Archivo cargado con éxito</div>
                <script>
                    $.alert.open('info', "Archivo cargado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                    });
                </script>
            <?php }
            if($error == 1){ ?>
                <div class="error_archivo">Debe ser un archivo con extensión rar</div>
                <script>
                    $.alert.open('alert', "Debe ser un archivo con extensión rar", {'Aceptar' : 'Aceptar'}, function() {
                    });
                </script>
            <?php }
            else if($error == 2){ ?>
                <div class="error_archivo">Debe seleccionar un archivo</div>
                <script>
                  $.alert.open('alert', "Debe seleccionar un archivo", {'Aceptar' : 'Aceptar'}, function() {
                      });
                </script>
            <?php }
            else if($error == 3){
            ?>
                <div class="error_archivo">No existe el archivo Resultados_Escaneo.csv en el archivo LAD_Output.rar</div>
                <script>
                    $.alert.open('alert', "No existe el archivo Resultados_Escaneo.csv en el archivo .rar", {'Aceptar' : 'Aceptar'}, function() {
                    });
                </script>
            <?php
            }
            else if($error == 4){
            ?>
                <div class="error_archivo">Los campos del archivo Resultados_Escaneo.csv no son compatibles</div>
                <script>
                    $.alert.open('alert', "Los campos del archivo Resultados_Escaneo.csv no son compatibles", {'Aceptar' : 'Aceptar'}, function() {
                    });
                </script>
            <?php
            }
            else if($error == 5){
            ?>
                <div class="error_archivo">El separador del archivo Resultados_Escaneo.csv no es válido</div>
                <script>
                    $.alert.open('alert', "El separador del archivo Resultados_Escaneo.csv no es válido", {'Aceptar' : 'Aceptar'}, function() {
                    });
                </script>
            <?php
            }
            ?>
        </fieldset>
        
        <br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Resultados de Troubleshooting:</p>
        
        <br>
        <div id="resultados" style="<?php if($buscarResultado == 0){ echo 'display:none;'; } ?>">
            <table class="tablap" style="width:60%">
                <thead>
                    <tr>
                        <th>Nombre de Equipo</th>
                        <th>Resultado Escaneo</th>
                    </tr>
                </thead>
                <tbody id="contenidoTroubleshooting">
                    <?php 
                    foreach($tablaEscaneoTrouble as $row){
                    ?>
                        <tr>
                            <td><?= $row["equipo"] ?></td>
                            <td><?= $row["errors"] ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        
        <br>
        <div style="float:right;"><div class="botones_m2 boton1 pointer" id="volver">Volver</div></div>
    </div>
</div>

<div class="modal-personal" id="modal-equipos">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Equipos con Error</p></h1>
    </div>
    <div class="modal-personal-body">
        <table style="margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
            <thead>
                <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                    <th  align="center" valign="middle" ><strong class="til" id="fabricante" name="fabricante">Equipo</strong></th>
                    <th  align="center" valign="middle" ><strong class="til" id="familia" name="familia">Error</strong></th>
                </tr> 
            </thead>
            <tbody id="bodyTable">
                <?php 
                foreach($tablaEscaneo as $row){
                ?>
                    <tr>
                        <td><?= $row["equipo"] ?></td>
                        <td><?= $row["errors"] ?></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>                    
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirListado">Salir</div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#verificacion").click(function(){
            j = 0;
            for(i = 0; i < ($("#tablaVerif tr").length - 1); i++){
                if(!$("#check" + i).prop("checked")){
                    j++;
                }
            }
            
            if(j === ($("#tablaVerif tr").length - 1)){
                $.alert.open('alert', 'Debe seleccionar mínimo un chequeo incluyendo además el chequeo Nro 6', {'Aceptar': 'Aceptar'}, function() {
                });
                return false;
            }
            
            if(!$("#check5").prop("checked")){
                $.alert.open('alert', 'Debe seleccionar el chequeo Nro 6', {'Aceptar': 'Aceptar'}, function() {
                });
                return false;
            }
            
            $("#paso1").hide();
            $("#paso2").show();
        });
        
        $("#archivo").change(function(e){
            if(!addArchivo(e)){
                $("#archivo").val("");
                $("#archivo").replaceWith($("#archivo").clone(true));
            }
        });
        
        $("#volver").click(function(){
            $("#paso2").hide();
            $("#url_file").val("Nombre del Archivo...");
            
            
            for(i = 0; i < $("#tablaVerif tr").length; i++){
                $("#check" + i).prop("checked", "");
            }
            
            $("#contenidoTroubleshooting").empty();
            $("#resultados").hide();
            $(".exito_archivo").hide();
            $(".error_archivo").hide();
            
            $("#paso1").show();
        });
        
        $("#salirListado").click(function(){
            $("#fondo1").hide();
            $("#modal-equipos").hide();
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        if(!file.name.match(/\.(rar)$/) ){
            $.alert.open('alert', 'Debe seleccionar un archivo .rar', {'Aceptar': 'Aceptar'}, function() {
            });
            return false;
        }
        
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $("#url_file").val("");
        $("#url_file").val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
    
    function mostrarListado(){
        $("#fondo1").show();
        $("#modal-equipos").show();
    }
</script>