<div id="ttabla1"  style="display:none; width:95%; margin:10px; float:left; max-height:400px;">
    <table width="95%" class="tablap" id="tablaEquipoErroneus" style="margin-top: -5px;">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>Equipo</span></th>
            <th  align="center" valign="middle"><span>Tipo</span></th>
            <th  align="center" valign="middle"><span>Familia</span></th>
            <th  align="center" valign="middle"><span>Edición</span></th>
            <th  align="center" valign="middle"><span>Versión</span></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $i = 1;
            foreach($softErroneus as $row){
            ?>
                <tr>
                    <td class="text-left"><?= $i ?></td>
                    <td class="text-left"><?= $row["equipo"] ?></td>
                    <td class="text-left"><?= $row["tipo"] ?></td>
                    <td class="text-left"><?= $row["familia"] ?></td>
                    <td class="text-left"><?= $row["edicion"] ?></td>
                    <td class="text-left"><?= $row["version"] ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function(){
        $("#tablaEquipoErroneus").tableHeadFixer();
        $("#tablaEquipoErroneus").tablesorter();
    });
</script>