<input type="hidden" id="urlNombre">

<div style="width:98%; padding:20px; overflow:hidden;">                                
    <div style="margin-right:20px;">
        <?php 
        include_once($GLOBALS["app_root"] . "/microsoft/plantillas/consolidadoOtros.php"); 
        ?>        
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='compras.php';">Compras</div></div>
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='despliegue1.php';">Atras</div></div>
    </div>
</div>

<script>
    function addArchivoConsolidado(e){
        file = e.target.files[0]; 
        if (!file.type.match("application/vnd.ms-excel")){
            $.alert.open('alert', "El archivo a cargar debe ser .csv", {'Aceptar' : 'Aceptar'}, function() {
            });
            return false;
        }
        else if(parseInt(file.size) / 1024 > 15360 ){
            $.alert.open('alert', "El tamaño permitido es de 15 MB", {'Aceptar' : 'Aceptar'}, function() {
            });
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $($("#urlNombre").val()).val(file.name);
            return true;
        }
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
</script>