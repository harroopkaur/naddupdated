<!--inicio Usuarios Equipo-->
<br />

<!--<p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/LA_Tool_ADv6.5.rar">LA_Tool_ADv6.5.rar</a></p><br />
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
<br>-->
<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">LAE_Output</legend>

    <form enctype="multipart/form-data" name="form_Equipo" id="form_Equipo" method="post">
        <input type="hidden" id="tokenCargarArchivoEquipo" name="token">
        <input type="hidden" id="tipoArchivoEquipo" name="tipoArchivo" value="equipo">

        <div class="contenedor_archivo">
            <div class="input_archivo">
                <input type="text" class="url_file" name="url_fileEquipo" disabled id="url_fileEquipo" value="Nombre del Archivo...">
                <input type="file" class="archivo" name="archivoEquipo" id="archivoEquipo" accept=".csv">
                <input type="button" id="botonEquipo" class="botones_m5 boton1" value="Buscar">
            </div>
            <div class="contenedor_boton_archivo">
                <input name="subirEquipo" type="button" id="subirEquipo" value="Cargar Archivo" class="botones_m5" />
            </div>
        </div>

        <div style="margin:0 auto; width:250px; overflow:hidden; background:#7F7F7F;">
            <progress id="barraProgresoEquipo" style="clear:both;top:30px; width:100%; height:25px;" value="0" max="100"></progress>
        </div>
    </form>         

    <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
    <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>

    <?php if($exitoEquipo == 1){ ?>
        <div class="exito_archivo">Archivo procesado con éxito</div>
        <script>
            $.alert.open('info', "Archivo procesado con éxito", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php } else if($exitoEquipo == 2){
    ?>
        <div class="error_archivo">No se pudo insertar todos los registros</div>
        <script>
            $.alert.open('alert', "No se pudo insertar todos los registros", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    } else if($exitoEquipo == 3){
    ?>
        <div class="error_archivo">No se pudo actualizar todos los registros del Detalle</div>
        <script>
            $.alert.open('alert', "No se pudo actualizar todos los registros del Detalle", {'Aceptar' : 'Aceptar'}, function() {
            });
        </script>
    <?php    
    }
    ?>
</fieldset>

<!--inicio modal Usuario Equipo-->
<form id="formProcesarEquipo" name="formProcesarEquipo" method="post" action="despliegueOtros.php">
    <input name="insertarEquipo" id="insertarEquipo" type="hidden" value="1" >
    <input type="hidden" id="nombreArchivoEquipo" name="nombreArchivoEquipo">
    <input name="token" id="tokenProcesarEquipo" type="hidden">

    <div class="modal-personal modal-personal-md" id="modal-Equipo">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Campos <p style="color:#06B6FF; display:inline">LAE_Output</p></h1>
        </div>

        <style>
            tr{
                line-height:30px;
            }
        </style>
        <div class="modal-personal-body">

            <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>DN: </span></th>
                    <td><select id="DN" name="DN"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>objectClass: </span></th>
                    <td><select id="objectClass" name="objectClass"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>cn: </span></th>
                    <td><select id="cn" name="cn"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>userAccountControl: </span></th>
                    <td><select id="userAccountControl" name="userAccountControl"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>lastLogon: </span></th>
                    <td><select id="lastLogon" name="lastLogon"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>pwdLastSet: </span></th>
                    <td><select id="pwdLastSet" name="pwdLastSet"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>operatingSystem: </span></th>
                    <td><select id="operatingSystem" name="operatingSystem"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>operatingSystemVersion: </span></th>
                    <td><select id="operatingSystemVersion" name="operatingSystemVersion"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>lastLogonTimestamp: </span></th>
                    <td><select id="lastLogonTimestamp" name="lastLogonTimestamp"></select></td>
                </tr>
                <tr>
                    <th class="text-right bold" style="width:50%;padding-right:10px;"><span>description: </span></th>
                    <td><select id="description" name="description"></select></td>
                </tr>
            </table>         
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirEquipo">Salir</div>
            <div class="boton1 botones_m2" id="procesarEquipo" style="float:right;">Procesar</div>
        </div>
    </div>
</form>
<!--fin modal Usuario Equipo-->
<!--fin Usuario Equipo-->
        
<script>
    $(document).ready(function(){
        /*inicio UsuarioEquipo*/
        $("#botonEquipo").click(function(){
            $("#archivoEquipo").click();
        });
        
        $("#archivoEquipo").change(function(e){
            $("#urlNombre").val("#url_fileEquipo");
            if(addArchivoConsolidado(e) === false){
                $("#archivoEquipo").val("");
                $("#archivoEquipo").replaceWith($("#archivoEquipo").clone(true));
            }
        });
        
        $("#subirEquipo").click(function(){
            $("#tokenCargarArchivoEquipo").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#form_Equipo")[0]);	
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandlerEquipo, true);
            ajax.addEventListener("load", completeHandlerEquipo, false);
            ajax.addEventListener("error", errorHandlerEquipo, false);
            ajax.addEventListener("abort", abortHandlerEquipo, false);
            ajax.open("POST", "<?= $GLOBALS["domain_root"] ?>/microsoft/ajax/subirArchivoConsolidado.php", true);
            ajax.send(formData);
        });
        
        $("#salirEquipo").click(function(){
            $("#nombreArchivoEquipo").val("");
            $("#DN").empty();
            $("#objectClass").empty();
            $("#cn").empty();
            $("#userAccountControl").empty();
            $("#lastLogon").empty();
            $("#pwdLastSet").empty();
            $("#operatingSystem").empty();
            $("#operatingSystemVersion").empty();
            $("#lastLogonTimestamp").empty();
            $("#description").empty();
            $("#fondo1").hide();
            $("#modal-Equipo").hide();
            $("#archivoEquipo").val("");
            $("#archivoEquipo").replaceWith($("#archivoEquipo").clone(true));
            $("#url_fileEquipo").val("Nombre del Archivo...");
            $("#barraProgresoEquipo").val(0);
        });
        
        $("#procesarEquipo").click(function(){
            $("#fondo1").hide();
            $("#modal-Equipo").hide();
            if($("#DN").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de DN", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#DN").focus();
                });
                return false;
            }
            
            if($("#objectClass").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de objectClass", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#objectClass").focus();
                });
                return false;
            }

            if($("#cn").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de cn", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#cn").focus();
                });
                return false;
            }
            
            if($("#userAccountControl").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de userAccountControl", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#userAccountControl").focus();
                });
                return false;
            }
            
            if($("#lastLogon").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de lastLogon", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#lastLogon").focus();
                });
                return false;
            }
            
            if($("#pwdLastSet").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de pwdLastSet", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#pwdLastSet").focus();
                });
                return false;
            }
            
            if($("#operatingSystem").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de operatingSystem", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#operatingSystem").focus();
                });
                return false;
            }

            if($("#operatingSystemVersion").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de operatingSystemVersion", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#operatingSystemVersion").focus();
                });
                return false;
            }
            
            if($("#lastLogonTimestamp").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de lastLogonTimestamp", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#lastLogonTimestamp").focus();
                });
                return false;
            }
            
            if($("#description").val() === ""){
                $.alert.open('alert', "Debe seleccionar el campo de description", {'Aceptar' : 'Aceptar'}, function() {
                    mostrarModalEquipo();
                    $("#description").focus();
                });
                return false;
            }
            
            $("#formProcesarEquipo").submit();
        });  
        /*fin UsuarioEquipo*/
    });
    
    /*inicio UsuarioEquipo*/
    function progressHandlerEquipo(e){
        var porcentaje = (e.loaded / e.total) * 100;
        $("#barraProgresoEquipo").val(Math.round(porcentaje)); 
    }

    function completeHandlerEquipo(e){
        var data = [{resultado:true, sesion:true}];
        $("#barraProgresoEquipo").val(100);
        res = e.target.responseText.split("*");
        
        if(parseInt(res[0]) === 0){
            data[0].sesion = false;
        } else if(parseInt(e.target.responseText) === 2){
            data[0].resultado = false;
        }
        
        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
        if(parseInt(res[0]) === 1){
            $("#nombreArchivoEquipo").val(res[2]);
            $("#DN").append(res[1]);
            $("#objectClass").append(res[1]);
            $("#cn").append(res[1]);
            $("#userAccountControl").append(res[1]);
            $("#lastLogon").append(res[1]);
            $("#pwdLastSet").append(res[1]);
            $("#operatingSystem").append(res[1]);
            $("#operatingSystemVersion").append(res[1]);
            $("#lastLogonTimestamp").append(res[1]);
            $("#description").append(res[1]);
            $("#fondo1").show();
            $("#modal-Equipo").show();
            
        } else if(parseInt(res[0]) === 3){
            $("#barraProgresoEquipo").val(0);
            $.alert.open('alert', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 4){
            $("#barraProgresoEquipo").val(0);
            $.alert.open('alert', "Debe seleccionar el archivo antes de subirlo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 5){
            $("#barraProgresoEquipo").val(0);
            $.alert.open('alert', "Los separadores válidos para archivo CSV son \",\" o \";\"", {'Aceptar' : 'Aceptar'}, function() {
            });
        }    
    }

    function errorHandlerEquipo(){
        $.alert.open('error', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function abortHandlerEquipo(){
        $.alert.open('alert', "Carga del archivo abortada", {'Aceptar' : 'Aceptar'}, function() {
        });
    }
    
    function mostrarModalEquipo(){
        $("#fondo1").show();
        $("#modal-Equipo").show();
    }
    /*fin UsuarioEquipo*/
</script>