<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    $moduloServidores = new moduloServidores();
    
    $vert = 0;
    if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
        $vert = $_POST["vert"];
    }
    
    $tipoTabla = "";
    if(isset($_POST["tipoTabla"])){
        $tipoTabla = $general->get_escape($_POST["tipoTabla"]);
    }

    if($vert == 0 && $tipoTabla == "Fisico"){
       $tabla  = $moduloServidores->windowServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Fisico");
       $nombre = "WindowsServerFisico.xlsx";
    }
    else if($vert == 0 && $tipoTabla == "Virtual"){
       $tabla = $moduloServidores->windowServerAlineacion($_SESSION["client_id"], $_SESSION["client_empleado"]);
       $nombre = "WindowsServerVirtual.xlsx";
    }
    else if($vert == 0 && $tipoTabla == "Balanza"){
       $tabla = $moduloServidores->balanzaServer($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows Server");
       $nombre = "WindowsServerBalanza.xlsx";
    }
    else if($vert == 1 && $tipoTabla == "Fisico"){
       $tabla = $moduloServidores->sqlServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Fisico");
       $nombre = "SQLServerFisico.xlsx";
    }
    else if($vert == 1 && $tipoTabla == "Virtual"){
       $tabla = $moduloServidores->sqlServerAlineacion($_SESSION["client_id"], $_SESSION["client_empleado"]);
       $nombre = "SQLServerVirtual.xlsx";
    }
    else if($vert == 1 && $tipoTabla == "Balanza"){
       $tabla = $moduloServidores->balanzaServer($_SESSION["client_id"], $_SESSION["client_empleado"], "SQL Server");
       $nombre = "SQLServerBalanza.xlsx";
    }

    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"


    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("");


    // Add some data

    //inicio cabecera de la tabla
    $i = 2;
    if($tipoTabla == "Fisico"){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Nombre')
                    ->setCellValue('B1', 'Familia')
                    ->setCellValue('C1', 'Edición')
                    ->setCellValue('D1', 'Versión')
                    ->setCellValue('E1', 'Tipo')
                    ->setCellValue('F1', 'Procesadores')
                    ->setCellValue('G1', 'Cores')
                    ->setCellValue('H1', 'Lic Srv')
                    ->setCellValue('I1', 'Lic Proc')
                    ->setCellValue('J1', 'Lic Core');
    }
    else if($tipoTabla == "Virtual"){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Cluster')
                    ->setCellValue('B1', 'Host')
                    ->setCellValue('C1', 'Nombre')
                    ->setCellValue('D1', 'Familia')
                    ->setCellValue('E1', 'Edición')
                    ->setCellValue('F1', 'Versión')
                    ->setCellValue('G1', 'Tipo')
                    ->setCellValue('H1', 'Procesadores')
                    ->setCellValue('I1', 'Cores')
                    ->setCellValue('J1', 'Lic Srv')
                    ->setCellValue('K1', 'Lic Proc')
                    ->setCellValue('L1', 'Lic Core');
    }
    else{
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Familia')
                    ->setCellValue('B1', 'Edición')
                    ->setCellValue('C1', 'Versión')
                    ->setCellValue('D1', 'Instalaciones')
                    ->setCellValue('E1', 'Físico')
                    ->setCellValue('F1', 'Virtual')
                    ->setCellValue('G1', 'Total')
                    ->setCellValue('H1', 'Neto');
    }
    //fin cabecera de la tabla

    //inicio cuerpo de la tabla
    if($tipoTabla == "Fisico"){
        foreach($tabla as $row){
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row["equipo"])
                        ->setCellValue('B'.$i, $row["familia"])
                        ->setCellValue('C'.$i, $row["edicion"])
                        ->setCellValue('D'.$i, $row["version"])
                        ->setCellValue('E'.$i, $row["tipo"])
                        ->setCellValue('F'.$i, $row["cpu"])
                        ->setCellValue('G'.$i, $row["cores"])
                        ->setCellValue('H'.$i, $row["licSrv"])
                        ->setCellValue('I'.$i, $row["licProc"])
                        ->setCellValue('J'.$i, $row["licCore"]);
            $i++;
        } 
    }
    else if($tipoTabla == "Virtual"){
        foreach($tabla as $row){
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row["cluster"])
                        ->setCellValue('B'.$i, $row["host"])
                        ->setCellValue('C'.$i, $row["equipo"])
                        ->setCellValue('D'.$i, $row["familia"])
                        ->setCellValue('E'.$i, $row["edicion"])
                        ->setCellValue('F'.$i, $row["version"])
                        ->setCellValue('G'.$i, $row["tipo"])
                        ->setCellValue('H'.$i, $row["cpu"])
                        ->setCellValue('I'.$i, $row["cores"])
                        ->setCellValue('J'.$i, $row["licSrv"])
                        ->setCellValue('K'.$i, $row["licProc"])
                        ->setCellValue('L'.$i, $row["licCore"]);
            $i++;
        } 
    }
    else{
        foreach($tabla as $row){
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row["familia"])
                        ->setCellValue('B'.$i, $row["edicion"])
                        ->setCellValue('C'.$i, $row["version"])
                        ->setCellValue('D'.$i, $row["instalaciones"])
                        ->setCellValue('E'.$i, $row["Fisico"])
                        ->setCellValue('F'.$i, $row["Virtual"])
                        ->setCellValue('G'.$i, $row["Fisico"] + $row["Virtual"])
                        ->setCellValue('H'.$i, $row["instalaciones"] - ($row["Fisico"] + $row["Virtual"]));
            $i++;
        }
    }
    //fin cuerpo de la tabla                                    

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
?>