<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen2.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $familiaSelect = "";
    $edicionSelect = "";
    $nombre        = "";
    
    $vert = 0;
    if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
        $vert = $_POST["vert"];
    }
    
    $asig = "";
    if(isset($_POST["asig"])){
        $asig = $general->get_escape($_POST["asig"]);
    }
    
    $dup = "Si";
    if(isset($_POST["dup"]) && $_POST["dup"] == "No"){
        $dup = "No";
    }

    //inicio opción seleccionada para el gráfico
    switch($vert){
        case 0  : $familiaSelect = 'Windows'; $nombre = "WindowsOS.xlsx"; break;
        case 1  : $familiaSelect = 'Windows'; $edicionSelect = 'Standard'; $nombre = "WindowsOSStandard.xlsx"; break;
        case 2  : $familiaSelect = 'Windows'; $edicionSelect = 'Enterprise'; $nombre = "WindowsOSEnterprise.xlsx"; break;
        case 3  : $familiaSelect = 'Windows'; $edicionSelect = 'Professional'; $nombre = "WindowsOSProfessional.xlsx"; break;
        case 4  : $familiaSelect = 'Windows'; $edicionSelect = 'Otros'; $nombre = "WindowsOSOtros.xlsx"; break;
        case 5  : $familiaSelect = 'Office'; $edicionSelect = ''; $nombre = "Office.xlsx"; break;
        case 51 : $familiaSelect = 'Office'; $edicionSelect = 'Standard'; $nombre = "OfficeStandard.xlsx"; break;
        case 52 : $familiaSelect = 'Office'; $edicionSelect = 'Professional'; $nombre = "OfficeProfessional.xlsx"; break;
        case 53 : $familiaSelect = 'Office'; $edicionSelect = 'Otros'; $nombre = "OfficeOtros.xlsx"; break;
        case 6  : $familiaSelect = 'Project';$edicionSelect = ''; $nombre = "Project.xlsx"; break;
        case 61 : $familiaSelect = 'Project';$edicionSelect = 'Standard'; $nombre = "ProjectStandard.xlsx"; break;
        case 62 : $familiaSelect = 'Project'; $edicionSelect = 'Professional'; $nombre = "ProjectProfessional.xlsx"; break;
        case 63 : $familiaSelect = 'Project'; $edicionSelect = 'Otros'; $nombre = "ProjectOtros.xlsx"; break;
        case 7  : $familiaSelect = 'Visio'; $edicionSelect = ''; $nombre = "Visio.xlsx"; break;
        case 71 : $familiaSelect = 'Visio'; $edicionSelect = 'Standard'; $nombre = "VisioStandard.xlsx"; break;
        case 72 : $familiaSelect = 'Visio'; $edicionSelect = 'Professional'; $nombre = "VisioProfessional.xlsx"; break;
        case 73 : $familiaSelect = 'Visio'; $edicionSelect = 'Otros'; $nombre = "VisioOtros.xlsx"; break;
        case 8  : $familiaSelect = 'Windows Server'; $edicionSelect = ''; $nombre = "WindowsServer.xlsx"; break;
        case 81 : $familiaSelect = 'Windows Server'; $edicionSelect = ''; $nombre = "WindowsServerStandard.xlsx"; break;
        case 82 : $familiaSelect = 'Windows Server'; $edicionSelect = 'Enterprise'; $nombre = "WindowsServerEnterprise.xlsx"; break;
        case 83 : $familiaSelect = 'Windows Server'; $edicionSelect = 'Professional'; $nombre = "WindowsServerProfessional.xlsx"; break;
        case 84 : $familiaSelect = 'Windows Server'; $edicionSelect = 'Otros'; $nombre = "WindowsServerOtros.xlsx"; break;
        case 9  : $familiaSelect = 'SQL'; $edicionSelect = ''; $nombre = "SQL.xlsx"; break;
        case 91 : $familiaSelect = 'SQL'; $edicionSelect = 'Standard'; $nombre = "SQLStandard.xlsx"; break;
        case 92 : $familiaSelect = 'SQL'; $edicionSelect = 'Datacenter'; $nombre = "SQLDatacenter.xlsx"; break;
        case 93 : $familiaSelect = 'SQL'; $edicionSelect = 'Enterprise'; $nombre = "SQLEnterprise.xlsx"; break;
        case 94 : $familiaSelect = 'SQL'; $edicionSelect = 'Otros'; $nombre = "SQLOtros.xlsx"; break;
        case 10 : $familiaSelect = 'Others'; $edicionSelect = 'Visual Studio'; $nombre = "VisualStudio.xlsx"; break;
        case 11 : $familiaSelect = 'Others'; $edicionSelect = 'Exchange Server'; $nombre = "ExchangeServer.xlsx"; break;
        case 12 : $familiaSelect = 'Others'; $edicionSelect = 'Sharepoint Server'; $nombre = "SharepointServer.xlsx"; break;
        case 13 : $familiaSelect = 'Others'; $edicionSelect = 'Skype for Business'; $nombre = "SkypeforBusiness.xlsx"; break;
        case 14 : $familiaSelect = 'Others'; $edicionSelect = 'System Center'; $nombre = "SystemCenter.xlsx"; break;
    }
    //fin opción seleccionada para el gráfico

    $detalles = new  DetallesE_f();
    $resumen   = new Resumen_f();
    $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
    
    //inicio consultas de los diferentes gráficos
    if($vert==0 || $vert == 8){
        //$listar_equipos0 = $detalles->listar_todog0($_SESSION['client_id'], $_SESSION['client_empleado']);
        $listar_equipos0 = $detalles->listar_todog0Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    }//ver 0
    
    if($vert == 81){
        //$listar_equipos0 = $detalles->listar_todog1($_SESSION['client_id'], $_SESSION['client_empleado'], 'enterprise');
        $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Standard', $asig, $asignaciones);
    }//ver 81
    
    if($vert == 82){
        //$listar_equipos0 = $detalles->listar_todog1($_SESSION['client_id'], $_SESSION['client_empleado'], 'enterprise');
        $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Datacenter', $asig, $asignaciones);
    }//ver 82
    
    if($vert == 84){
        //$listar_equipos0 = $detalles->listar_todog1($_SESSION['client_id'], $_SESSION['client_empleado'], 'enterprise');
        $listar_equipos0 = $detalles->listar_todog3Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    }//ver 84

    if($vert==1){
        //$listar_equipos0 = $detalles->listar_todog1($_SESSION['client_id'], $_SESSION['client_empleado'], 'enterprise');
        $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Standard', $asig, $asignaciones);
    }//ver 1
    
    if($vert==2 || $vert83){
        //$listar_equipos0 = $detalles->listar_todog1($_SESSION['client_id'], $_SESSION['client_empleado'], 'enterprise');
        $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Enterprise', $asig, $asignaciones);
    }//ver 1

    if($vert==3){
        //$listar_equipos0 = $detalles->listar_todog1($_SESSION['client_id'], $_SESSION['client_empleado'], 'professional');
        $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Professional', $asig, $asignaciones);
    }// ver 2

    if($vert==4){
        //$listar_equipos0 = $detalles->listar_todog2($_SESSION['client_id'], $_SESSION['client_empleado'], 'enterprise','professional');
        $listar_equipos0 = $detalles->listar_todog4Asignacion($_SESSION['client_id'], 'Standard', 'Enterprise', 'Professional', $asig, $asignaciones);
    }// ver 3

    if($vert==5){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Office', '', $asig, $asignaciones, $dup);
    }//5
    
    if($vert==51){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office','Standard');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Office', 'Standard', $asig, $asignaciones, $dup);
    }//5

    if($vert==52){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office','Professional');	
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Office', 'Professional', $asig, $asignaciones, $dup);
    }//52

    if($vert==53){
        //$lista_calculo = $resumen->listar_datos7($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office', 'Standard', 'Professional');
        $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 'Office', 'Standard', 'Professional', $asig, $asignaciones, $dup);    
    }//53

    if($vert==6){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Project', '', $asig, $asignaciones, $dup);
    }//6
    
    if($vert==61){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','Standard');
       $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Project', 'Standard', $asig, $asignaciones, $dup);     
    }//61

    if($vert==62){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','Professional');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Project', 'Professional', $asig, $asignaciones, $dup);
    }//62

    if($vert==63){
        //$lista_calculo = $resumen->listar_datos7($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','Professional', 'Standard');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Project', 'Standard', 'Professional', $asig, $asignaciones, $dup);
    }//63

    if($vert==7){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'visio','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Visio', '', $asig, $asignaciones, $dup);
    }//7
    
    if($vert==71){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'visio','Standard');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Visio', 'Standard', $asig, $asignaciones, $dup);
    }//71

    if($vert==72){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'visio','Professional');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Visio', 'Professional', $asig, $asignaciones, $dup);
    }//72

    if($vert==73){
        //$lista_calculo = $resumen->listar_datos7($_SESSION['client_id'], $_SESSION['client_empleado'], 'visio','Professional','Standard');
        $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 'Visio', 'Standard', 'Professional', $asig, $asignaciones, $dup);
    }//73

    if($vert==9){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'SQL Server', '', $asig, $asignaciones, $dup);
    }//9
    
    if($vert==91){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Standard');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'SQL Server', 'Standard', $asig, $asignaciones, $dup);
    }//91

    if($vert==92){
       // $lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Datacenter');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'SQL Server', 'Datacenter', $asig, $asignaciones, $dup);
    }//9

    if($vert==93){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Datacenter');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'SQL Server', 'Enterprise', $asig, $asignaciones, $dup);
    }//9

    if($vert==94){
        //$lista_calculo = $resumen->listar_datos8($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Standard','Datacenter','Enterprise');
        $lista_calculo = $resumen->listar_datos8Asignacion($_SESSION['client_id'], 'SQL Server', 'Standard', 'Datacenter', 'Enterprise', $asig, $asignaciones, $dup);
    }//9

    if($vert==10){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Visual Studio','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Visual Studio', '', $asig, $asignaciones, $dup);
    }//10

    if($vert==11){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Exchange Server','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Exchange Server', '', $asig, $asignaciones, $dup);
    }//11

    if($vert==12){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Sharepoint Server','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Sharepoint Server', '', $asig, $asignaciones, $dup);
    }//12

    if($vert==13){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'Skype for Business','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Skype for Business', '', $asig, $asignaciones, $dup);
    }//13

    if($vert==14){
        //$lista_calculo = $resumen->listar_datos6($_SESSION['client_id'], $_SESSION['client_empleado'], 'System Center','');
        $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'System Center', '', $asig, $asignaciones, $dup);
    }//14	
    //fin consultas de los diferentes gráficos

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("");


    // Add some data

    //inicio cabecera de la tabla
    $i = 2;
    if($vert == 0 || $vert == 1 || $vert == 2 || $vert == 3 || $vert == 4 || $vert == 8 || $vert == 82 || $vert == 83 || $vert == 84){
        if($vert == 0 || $vert == 8){
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', '')
                        ->setCellValue('B1', '')
                        ->setCellValue('C1', 'Activo')
                        ->setCellValue('D1', '')
                        ->setCellValue('E1', '')
                        ->setCellValue('F1', '')
                        ->setCellValue('A2', '')
                        ->setCellValue('B2', 'Nombre Equipo')
                        ->setCellValue('C2', 'Tipo')
                        ->setCellValue('D2', 'Sistema Operativo')
                        ->setCellValue('E2', 'Activo AD')
                        ->setCellValue('F2', 'LA Tool')
                        ->setCellValue('G2', 'Usabilidad')
                        ->setCellValue('H2', 'Asignación');
            $i = 3;
        }
        else{
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', '')
                        ->setCellValue('B1', 'Nombre Equipo')
                        ->setCellValue('C1', 'Tipo')
                        ->setCellValue('D1', 'Sistema Operativo')
                        ->setCellValue('E1', 'Activo AD')
                        ->setCellValue('F1', 'LA Tool')
                        ->setCellValue('G1', 'Usabilidad')
                        ->setCellValue('H1', 'Asignación');
        }
    }

    if($vert == 5 || $vert == 51 || $vert == 52 || $vert == 53 || $vert == 6 || $vert == 61 || $vert == 62 || $vert == 63 
    || $vert == 7 || $vert == 71 || $vert == 72 || $vert == 73 || $vert == 9 || $vert == 91 || $vert == 92 || $vert == 93 
    || $vert == 94 || $vert == 10 || $vert == 11 || $vert == 12 || $vert == 13 || $vert == 14){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', '')
                    ->setCellValue('B1', 'Equipo')
                    ->setCellValue('C1', 'Tipo')
                    ->setCellValue('D1', 'Familia')
                    ->setCellValue('E1', 'Edición')
                    ->setCellValue('F1', 'Versión')
                    ->setCellValue('G1', 'Fecha Instalación')
                    ->setCellValue('H1', 'Usabilidad')
                    ->setCellValue('I1', 'Observación')
                    ->setCellValue('J1', 'Asignación');
    }
    //fin cabecera de la tabla

    //inicio cuerpo de la tabla
    if($vert==0 || $vert == 8){

        $tipo       = 1;
        $tipoNombre = "Cliente"; 
        if($vert == 8){
            $tipo       = 2;
            $tipoNombre = "Servidor"; 
        }

        if($listar_equipos0){
            $j = 1;
            foreach($listar_equipos0 as $reg_equipos0){

                /*$error = "No";
                if($reg_equipos0["LaTool"] == 'Ninguno'){
                    $error = 'Si';
                }*/

                $usabilidad = "En Uso";
                if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
                    $usabilidad = "Uso Probable";
                }
                else if($reg_equipos0["rango"] != 1){
                    $usabilidad = "Obsoleto";
                }



                if(($reg_equipos0["rango"] == 1 || $reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3) && $reg_equipos0["tipo"] == $tipo){

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                    ->setCellValue('C'.$i, $tipoNombre)
                    ->setCellValue('D'.$i, $reg_equipos0["os"])
                    ->setCellValue('E'.$i, "Si")
                    ->setCellValue('F'.$i, $reg_equipos0["LaTool"])
                    ->setCellValue('G'.$i, $usabilidad)
                    ->setCellValue('H'.$i, $reg_equipos0["asignacion"]);
                    $i++;
                    $j++;
                } 

            }

            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, "")
                        ->setCellValue('B'.$i, "")
                        ->setCellValue('C'.$i, "Inactivo")
                        ->setCellValue('D'.$i, "")
                        ->setCellValue('E'.$i, "")
                        ->setCellValue('F'.$i, "")
                        ->setCellValue('G'.$i, "");

            $i++;
            $k = 1;
            foreach($listar_equipos0 as $reg_equipos0){

                /*$error = "No";
                if($reg_equipos0["LaTool"] == 'Ninguno'){
                    $error = 'Si';
                }*/

                $usabilidad = "En Uso";
                if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
                    $usabilidad = "Uso Probable";
                }
                else if($reg_equipos0["rango"] != 1){
                    $usabilidad = "Obsoleto";
                }

                if($reg_equipos0["rango"]!=1 && $reg_equipos0["rango"]!=2 && $reg_equipos0["rango"]!=3 && $reg_equipos0["tipo"] == $tipo){

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $k)
                    ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                    ->setCellValue('C'.$i, $tipoNombre)
                    ->setCellValue('D'.$i, $reg_equipos0["os"])
                    ->setCellValue('E'.$i, "No")
                    ->setCellValue('F'.$i, $reg_equipos0["LaTool"])
                    ->setCellValue('G'.$i, $usabilidad)
                    ->setCellValue('H'.$i, $reg_equipos0["asignacion"]);
                     $i++;
                     $k++;
                }
            }
        }
    }

    if($vert == 1 || $vert == 2 || $vert == 3 || $vert == 82 || $vert == 83 || $vert == 84){

        $tipo       = 1;
        $tipoNombre = "Cliente"; 
        if($vert == 82 || $vert == 83 || $vert == 84){
            $tipo       = 2;
            $tipoNombre = "Servidor"; 
        }

        if($listar_equipos0){
            $j = 1;
            foreach($listar_equipos0 as $reg_equipos0){
                if($reg_equipos0["tipo"] == $tipo){
                    $activo = "No";
                    if($reg_equipos0["rango"] == 1 ||$reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
                        $activo = 'Si';
                    }

                    /*$error = "No";
                    if($reg_equipos0["errors"] == 'Ninguno'){
                        $error = 'Si';
                    }*/

                    $usabilidad = "En Uso";
                    if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
                        $usabilidad = "Uso Probable";
                    }
                    else if($reg_equipos0["rango"] != 1){
                        $usabilidad = "Obsoleto";
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                    ->setCellValue('C'.$i, $tipoNombre)
                    ->setCellValue('D'.$i, $reg_equipos0["os"])
                    ->setCellValue('E'.$i, "No")
                    ->setCellValue('F'.$i, $reg_equipos0["errors"])
                    ->setCellValue('G'.$i, $usabilidad)
                    ->setCellValue('H'.$i, $reg_equipos0["asignacion"]);

                    $i++;
                    $j++;
                }
            }
        }
    }

    if($vert == 5 || $vert == 51 || $vert == 52 || $vert == 53 || $vert == 6 || $vert == 61 || $vert == 62 || $vert == 63 
    || $vert == 7 || $vert == 71 || $vert == 72 || $vert == 73 || $vert == 9 || $vert == 91 || $vert == 92 || $vert == 93 
    || $vert == 94 || $vert == 10 || $vert == 11 || $vert == 12 || $vert == 13 || $vert == 14){
        if($lista_calculo){
            $j = 1;
            foreach($lista_calculo as $reg_calculo){
                /*$usabilidad = "En Uso";
                if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
                    $usabilidad = "Uso Probable";
                }
                else if($reg_calculo["rango"] != 1){
                    $usabilidad = "Obsoleto";
                }*/

                $duplicado = "";
                if($resumen->duplicado($_SESSION['client_id'], $_SESSION['client_empleado'], $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
                    $duplicado = "Duplicado";
                }
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $j)
                            ->setCellValue('B'.$i, $reg_calculo["equipo"])
                            ->setCellValue('C'.$i, $reg_calculo["tipo"])
                            ->setCellValue('D'.$i, $reg_calculo["familia"])
                            ->setCellValue('E'.$i, $reg_calculo["edicion"])
                            ->setCellValue('F'.$i, $reg_calculo["version"])
                            ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                            ->setCellValue('H'.$i, $reg_calculo["rango"])
                            ->setCellValue('I'.$i, $duplicado)
                            ->setCellValue('J'.$i, $reg_calculo["asignacion"]);
                $i++;
                $j++;
            }	
        }
    }
    //fin cuerpo de la tabla                                    

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesion($mensaje);
}