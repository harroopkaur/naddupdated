<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"
    $detalles1 = new DetallesE_f($conn);
    $balance2  = new Balance_f($conn);

    $vert1 = 0;
    if(isset($_GET["vert1"]) && filter_var($_GET['vert1'], FILTER_VALIDATE_INT) !== false){
        $vert1 = $_GET["vert1"];
    }
    
    $fecha = $detalles1->fechaSAMArchivo($vert1);

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Balanza");


    $clientTotalOfficeCompras = 0;
    $clientTotalOfficeInstal = 0;
    $clientOfficeNeto = 0;

    //inicio Windows OS Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows OS Enterprise');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias6SamArchivo($vert1, "Windows", "Enterprise");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 0);
    //fin Windows OS Enterprise


    //inicio Windows OS Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows OS Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias6SamArchivo($vert1, "Windows", "Professional");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 1);
    //fin Windows OS Professional


    //inicio Windows OS Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows OS Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias10SamArchivo($vert1, "Windows");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 2);
    //fin Windows OS Otros


    //inicio Office Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Office", "Standard");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 3);
    //fin Office Standard


    //inicio Office Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Office", "Professional");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 4);
    //fin Office Professional


    //inicio Office Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias9SamArchivo($vert1, "Office");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 5);
    //fin Office Otros


    //inicio Project Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Project", "Standard");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 6);
    //fin Project Standard


    //inicio Project Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Project", "Professional");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 7);
    //fin Project Professional


    //inicio Project Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias9SamArchivo($vert1, "Project");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 8);
    //fin Project Otros


    //inicio Visio Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Visio", "Standard");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 9);
    //fin Visio Standard


    //inicio Visio Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Visio", "Professional");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 10);
    //fin Visio Professional


    //inicio Visio Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias9SamArchivo($vert1, "Visio");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 11);
    //fin Visio Otros


    //inicio Windows Server Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Windows Server", "Standard");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 12);
    //fin Windows Server Standard


    //inicio Windows Server Datacenter
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Datacenter');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Windows Server", "Datacenter");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 13);
    //fin Windows Server Datacenter


    //inicio Windows Server Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Enterprise');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "Windows Server", "Enterprise");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 14);
    //fin Windows Server Enterprise


    //inicio Windows Server Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias9SamArchivo($vert1, "Windows Server");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 15);
    //fin Windows Server Otros


    //inicio SQL Server Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "SQL Server", "Standard");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 16);
    //fin SQL Server Standard


    //inicio SQL Server Datacenter
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Datacenter');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "SQL Server", "Datacenter");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 17);
    //fin SQL Server Datacenter


    //inicio SQL Server Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Enterprise');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $listar_Of = $balance2->listar_todo_familias1SamArchivo($vert1, "SQL Server", "Enterprise");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 18);
    //fin SQL Server Enterprise


    //inicio SQL Server Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias9SamArchivo($vert1, "SQL Server");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 19);
    //fin SQL Server Otros


    //inicio Visual Studio
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visual Studio');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias7SamArchivo($vert1, "Visual Studio");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 20);
    //fin Visual Studio


    //inicio Exchange Server
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Exchange Server');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias7SamArchivo($vert1, "Exchange");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 21);
    //fin Exchange Server


    //inicio Sharepoint Server
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Sharepoint Server');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias7SamArchivo($vert1, "Sharepoint");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 22);
    //fin Sharepoint Server


    //inicio Skype for Business
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Skype for Business');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias7SamArchivo($vert1, "Skype");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 23);
    //fin Skype for Business


    //inicio System Center
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'System Center');
    // Add some data

    $myWorkSheet->setCellValue('A1', 'Producto')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Compras')
                ->setCellValue('F1', 'Neto')
                ->setCellValue('G1', 'Precio');

    $listar_Of = $balance2->listar_todo_familias7SamArchivo($vert1, "System Center");

    $i = 2;
    foreach ($listar_Of as $reg_equipos) {
        $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                    ->setCellValue('B' . $i, $reg_equipos["office"])
                    ->setCellValue('C' . $i, $reg_equipos["version"])
                    ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                    ->setCellValue('E' . $i, round($reg_equipos["compra"], 0))
                    ->setCellValue('F' . $i, round($reg_equipos["balance"], 0))
                    ->setCellValue('G' . $i, round($reg_equipos["balancec"], 0));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 24);
    //fin System Center

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="excelBalanza' . $fecha . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesionReporte($mensaje);
}
?>
