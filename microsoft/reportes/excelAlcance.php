<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $detalles1 = new DetallesE_f();
    $detalles2 = new DetallesE_f();
    $balance2  = new Balance_f();
    $balance   = new Balance_f();

    $total_1LAc       = 0;
    $total_2DVc       = 0;
    $total_1LAs       = 0;
    $total_2DVs       = 0;
    $reconciliacion1c = 0;
    $reconciliacion1s = 0;

    $exito  = 0;
    $error  = 0;
    $exito2 = 0;
    $error2 = 0;
    $exito3 = 0;
    $error3 = 0;

    $total_1  = 0;
    $total_2  = 0;
    $total_3  = 0;
    $total_4  = 0;
    $total_5  = 0;
    $total_6  = 0;
    $tclient  = 0;
    $tserver  = 0;
    $activosc = 0;
    $activoss = 0;

    $vert = 0;
    if(isset($_GET["vert"]) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false){
        $vert = $_GET['vert'];
    }

    $vert1 = 0;
    if(isset($_GET["vert1"]) && filter_var($_GET['vert1'], FILTER_VALIDATE_INT) !== false){
        $vert1 = $_GET["vert1"];
    }
    
    $fecha = $detalles1->fechaSAMArchivo($vert1);

    if ($vert == 0) {
        $listar_equipos = $detalles1->listar_todoSAMArchivo1($vert1);

        if ($listar_equipos) {
            foreach ($listar_equipos as $reg_equipos) {

                if ($reg_equipos["tipo"] == 1) {//cliente
                    $tclient = ($tclient + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $total_1LAc = ($total_1LAc + 1);
                    }

                    if ($reg_equipos["rango"] == 1) {
                        $total_2DVc = ($total_2DVc + 1);
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_2DVc = ($total_2DVc + 1);
                    }
                } else {//server
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $total_1LAs = ($total_1LAs + 1);
                    }

                    $tserver = ($tserver + 1);

                    if ($reg_equipos["rango"] == 1) {
                        $total_2DVs = ($total_2DVs + 1);
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_2DVs = ($total_2DVs + 1);
                    }
                }
            }
            $reconciliacion1c = $total_2DVc - $total_1LAc;
            $reconciliacion1s = $total_2DVs - $total_1LAs;
        }
    }//0
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Alcance");


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '')
                ->setCellValue('B1', '')
                ->setCellValue('C1', 'Alcance')
                ->setCellValue('D1', '')
                ->setCellValue('E1', '');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Tipo')
                ->setCellValue('B2', 'Activo AD')
                ->setCellValue('C2', 'Reconciliación')
                ->setCellValue('D2', 'LATool')
                ->setCellValue('E2', 'Cobertura');

    $t1 = $total_2DVc - $total_1LAc;
    $pot11 = 0;
    if($total_2DVc != 0){
        $pot11 = ($total_1LAc / $total_2DVc) * 100;
    }

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A3', 'Cliente')
                ->setCellValue('B3', $total_2DVc)
                ->setCellValue('C3', $t1)
                ->setCellValue('D3', $total_1LAc)
                ->setCellValue('E3', round($pot11));

    $t2 = $total_2DVs - $total_1LAs;
    $pot12 = 0;
    if($total_2DVs != 0){
        $pot12 = ($total_1LAs / $total_2DVs) * 100;
    }

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A4', 'Servidor')
                ->setCellValue('B4', $total_2DVs)
                ->setCellValue('C4', $t2)
                ->setCellValue('D4', $total_1LAs)
                ->setCellValue('E4', round($pot12));

    $to1 = $total_1LAc + $total_1LAs;
    $to2 = $total_2DVc + $total_2DVs;
    $pot13 = 0;
    if($to2 != 0){
       $pot13 = ($to1 / $to2) * 100; 
    }

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A5', 'Total')
                ->setCellValue('B5', ($total_2DVc + $total_2DVs))
                ->setCellValue('C5', ($reconciliacion1c + $reconciliacion1s))
                ->setCellValue('D5', ($total_1LAc + $total_1LAs))
                ->setCellValue('E5', round($pot13));

    $listar_equipos2 = $detalles2->listar_todoSAMArchivo1($vert1);
    if ($listar_equipos2) {
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', '')
                    ->setCellValue('B7', 'Nombre Equipo')
                    ->setCellValue('C7', 'Tipo')
                    ->setCellValue('D7', 'Activo AD')
                    ->setCellValue('E7', 'LA Tool');
        $i = 8;
        $j = 1;
        foreach ($listar_equipos2 as $reg_equipos2) {
            if ($reg_equipos2["tipo"] == 1) {
                $tipo = 'Cliente';
            } else {
                $tipo = 'Servidor';
            }

            if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                $activoAD = 'Si';
            } else {
                $activoAD = 'No';
            }

            if ($reg_equipos2["errors"] == 'Ninguno') {
                $latool = 'Si';
            } else {
                $latool = 'No';
            }

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $j)
                        ->setCellValue('B' . $i, $reg_equipos2["equipo"])
                        ->setCellValue('C' . $i, $tipo)
                        ->setCellValue('D' . $i, $activoAD)
                        ->setCellValue('E' . $i, $latool);

            $i++;
            $j++;
        }
    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="excelAlcance' . $fecha . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}