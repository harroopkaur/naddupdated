<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../../../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"
    $balance2  = new Balance_f();

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Balance");

    $asig = "";
    if(isset($_POST["asig"])){
        $asig = $_POST["asig"];
    }
    
    $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
    
    $clientTotalOfficeCompras = 0;
    $clientTotalOfficeInstal = 0;
    $clientOfficeNeto = 0;

    //inicio Windows OS Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows OS Enterprise');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias6($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows", "Enterprise");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Windows", "Enterprise", $asig, $asignaciones);

    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 0);
    //fin Windows OS Enterprise


    //inicio Windows OS Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows OS Professional');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias6($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows", "Professional");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Windows", "Professional", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 1);
    //fin Windows OS Professional


    //inicio Windows OS Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows OS Others');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias10($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows");
    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION["client_id"], "Windows", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 2);
    //fin Windows OS Otros


    //inicio Office Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Standard');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Office", "Standard");
    $listar_Of = $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Office", "Standard", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 3);
    //fin Office Standard


    //inicio Office Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Professional');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Office", "Professional");
    $listar_Of = $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Office", "Professional", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 4);
    //fin Office Professional


    //inicio Office Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Others');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias9($_SESSION["client_id"], $_SESSION["client_empleado"], "Office");
    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION["client_id"], "Office", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 5);
    //fin Office Otros


    //inicio Project Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Standard');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Project", "Standard");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Project", "Standard", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 6);
    //fin Project Standard


    //inicio Project Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Professional');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Project", "Professional");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Project", "Professional", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 7);
    //fin Project Professional


    //inicio Project Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Others');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias9($_SESSION["client_id"], $_SESSION["client_empleado"], "Project");
    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION["client_id"], "Project", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 8);
    //fin Project Otros


    //inicio Visio Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Standard');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Visio", "Standard");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Visio", "Standard", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 9);
    //fin Visio Standard


    //inicio Visio Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Professional');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Visio", "Professional");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Visio", "Professional", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 10);
    //fin Visio Professional


    //inicio Visio Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Others');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias9($_SESSION["client_id"], $_SESSION["client_empleado"], "Visio");
    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION["client_id"], "Visio", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 11);
    //fin Visio Otros


    //inicio Windows Server Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Standard');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows Server", "Standard");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Windows Server", "Standard", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 12);
    //fin Windows Server Standard


    //inicio Windows Server Datacenter
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Datacenter');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows Server", "Datacenter");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Windows Server", "Datacenter", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 13);
    //fin Windows Server Datacenter


    //inicio Windows Server Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Enterprise');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows Server", "Enterprise");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Windows Server", "Enterprise", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 14);
    //fin Windows Server Enterprise


    //inicio Windows Server Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Others');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias9($_SESSION["client_id"], $_SESSION["client_empleado"], "Windows Server");
    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION["client_id"], "Windows Server", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 15);
    //fin Windows Server Otros


    //inicio SQL Server Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Standard');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "SQL Server", "Standard");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "SQL Server", "Standard", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 16);
    //fin SQL Server Standard


    //inicio SQL Server Datacenter
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Datacenter');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "SQL Server", "Datacenter");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "SQL Server", "Datacenter", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 17);
    //fin SQL Server Datacenter


    //inicio SQL Server Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Enterprise');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias1($_SESSION["client_id"], $_SESSION["client_empleado"], "SQL Server", "Enterprise");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "SQL Server", "Enterprise", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 18);
    //fin SQL Server Enterprise


    //inicio SQL Server Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Others');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias9($_SESSION["client_id"], $_SESSION["client_empleado"], "SQL Server");
    $listar_Of = $balance2->balanzaAsignacionOtros($_SESSION["client_id"], "SQL Server", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 19);
    //fin SQL Server Otros


    //inicio Visual Studio
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visual Studio');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias7($_SESSION["client_id"], $_SESSION["client_empleado"], "Visual Studio");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Visual Studio", "", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 20);
    //fin Visual Studio


    //inicio Exchange Server
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Exchange Server');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias7($_SESSION["client_id"], $_SESSION["client_empleado"], "Exchange");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Exchange Server", "", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 21);
    //fin Exchange Server


    //inicio Sharepoint Server
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Sharepoint Server');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias7($_SESSION["client_id"], $_SESSION["client_empleado"], "Sharepoint");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Sharepoint Server", "", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 22);
    //fin Sharepoint Server


    //inicio Skype for Business
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Skype for Business');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias7($_SESSION["client_id"], $_SESSION["client_empleado"], "Skype");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "Skype for Business", "", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 23);
    //fin Skype for Business


    //inicio System Center
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'System Center');
    // Add some data

    $balance2->cabeceraReporteBalanza($myWorkSheet, $_SESSION["idioma"]);

    //$listar_Of = $balance2->listar_todo_familias7($_SESSION["client_id"], $_SESSION["client_empleado"], "System Center");
    $listar_Of = $balance2->balanzaAsignacion($_SESSION["client_id"], "System Center", "", $asig, $asignaciones);
    
    $balance2->detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, 24);
    //fin System Center

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="excelAllBalance.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesionReporte($mensaje);
}