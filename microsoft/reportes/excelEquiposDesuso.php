<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    $detalles         = new DetallesE_f();

    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"


    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Servidores");

    $vert1 = 0;
    if(isset($_GET["vert1"]) && filter_var($_GET['vert1'], FILTER_VALIDATE_INT) !== false){
        $vert1 = $_GET["vert1"];
    }
    
    $fecha = $detalles->fechaSAMArchivo($vert1);

    // Add some data
    
    $totalEquiposOS = 0;
    $totalEquiposWindowsServer = 0;

    $cantDesusoOS = 0;
    $cantDesusoWindowsServer = 0;

    $desusoOS = $detalles->listaProductosOSDesusoSAM($vert1, "Windows");
    $desusoWindowsServer = $detalles->listaProductosOSDesusoSAM($vert1, "Windows Server");
    $desusoOffice = $detalles->listaProductosDesusoSAM($vert1, "Office", "Windows");
    $desusoVisio = $detalles->listaProductosDesusoSAM($vert1, "Visio", "Windows");
    $desusoProject = $detalles->listaProductosDesusoSAM($vert1, "Project", "Windows");
    $desusoVisualStudio = $detalles->listaProductosDesusoSAM($vert1, "Visual Studio", "Windows");
    $desusoSQLServer = $detalles->listaProductosDesusoSAM($vert1, "SQL Server", "Windows Server");

    $familia = array("Office", "Visio", "Project", "Visual Studio", "SQL Server");
    $desusoOtros = $detalles->listaProductosOtrosDesusoSAM($vert1, $familia, "Windows");

    foreach($desusoOS as $row){
        if($row["rango"] > 1){
            $cantDesusoOS++;
        }
        $totalEquiposOS++; 
    }

    foreach($desusoWindowsServer as $row){
        if($row["rango"] > 1){
            $cantDesusoWindowsServer++;
        }
        $totalEquiposWindowsServer++;
    }

    $cantDesusoOffice = count($desusoOffice);
    $cantAux = $cantDesusoOffice;

    $cantDesusoVisio = count($desusoVisio);      
    if($cantDesusoVisio > $cantAux){
        $cantAux = $cantDesusoVisio;
    }

    $cantDesusoProject = count($desusoProject);
    if($cantDesusoProject > $cantAux){
        $cantAux = $cantDesusoProject;
    }

    $cantDesusoVisualStudio = count($desusoVisualStudio);
    if($cantDesusoVisualStudio > $cantAux){
        $cantAux = $cantDesusoVisualStudio;
    }

    $cantDesusoSQLServer = count($desusoSQLServer);
    $cantDesusoOtros =  count($desusoOtros);
    if($cantDesusoOtros > $cantAux){
        $cantAux = $cantDesusoOtros;
    }

    $porcentajeDesusoCliente = round($cantAux * 100 / $cantDesusoOS, 0);
    $porcentajeDesusoServidor = round($cantDesusoSQLServer * 100 / $totalEquiposWindowsServer, 0);

    $productosDesusoCliente = $detalles->productosDesusoClienteSam($vert1);
    $productosDesusoServidor = $detalles->productosDesusoServidorSam($vert1);

    
    
    $nombre = "desusoMicrosoft.xlsx";
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '')
            ->setCellValue('B1', 'OS')
            ->setCellValue('C1', 'Office')
            ->setCellValue('D1', 'Visio')
            ->setCellValue('E1', 'Project')
            ->setCellValue('F1', 'Visual')
            ->setCellValue('G1', 'Otros')
            ->setCellValue('H1', '%');

    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Clientes')
            ->setCellValue('B2', $cantDesusoOS)
            ->setCellValue('C2', $cantDesusoOffice)
            ->setCellValue('D2', $cantDesusoVisio)
            ->setCellValue('E2', $cantDesusoProject)
            ->setCellValue('F2', $cantDesusoVisualStudio)
            ->setCellValue('G2', $cantDesusoOtros)
            ->setCellValue('H2', $porcentajeDesusoCliente);    

    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', '')
            ->setCellValue('B4', 'Windows Server')
            ->setCellValue('C4', 'SQL Server')
            ->setCellValue('D4', '%');

    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A5', 'Servidores')
            ->setCellValue('B5', $cantDesusoWindowsServer)
            ->setCellValue('C5', $cantDesusoSQLServer)
            ->setCellValue('D5', $porcentajeDesusoCliente);

    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A7', '')
            ->setCellValue('B7', '')
            ->setCellValue('C7', 'Clientes')
            ->setCellValue('D7', '')
            ->setCellValue('E7', '');

    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A8', 'Familia')
            ->setCellValue('B8', 'Edición')
            ->setCellValue('C8', 'Versión')
            ->setCellValue('D8', 'Cantidad')
            ->setCellValue('E8', '%');

    $i = 9;
    foreach($productosDesusoCliente as $row){
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, $row['familia'])
            ->setCellValue('B' . $i, $row['edicion'])
            ->setCellValue('C' . $i, $row['version'])
            ->setCellValue('D' . $i, $row['cantidad'])
            ->setCellValue('E' . $i, round($row["cantidad"] * 100 / $cantDesusoOS, 0));
        $i++;
    }

    $i++;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, '')
            ->setCellValue('B' . $i, '')
            ->setCellValue('C' . $i, 'Servidores')
            ->setCellValue('D' . $i, '')
            ->setCellValue('E' . $i, '');

    $i++;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, 'Familia')
            ->setCellValue('B' . $i, 'Edición')
            ->setCellValue('C' . $i, 'Versión')
            ->setCellValue('D' . $i, 'Cantidad')
            ->setCellValue('E' . $i, '%');

    $i++;
    foreach($productosDesusoServidor as $row){
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, $row['familia'])
            ->setCellValue('B' . $i, $row['edicion'])
            ->setCellValue('C' . $i, $row['version'])
            ->setCellValue('D' . $i, $row['cantidad'])
            ->setCellValue('E' . $i, round($row["cantidad"] * 100 / $cantDesusoWindowsServer, 0));
        $i++;
    }      
    
    
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="SoftwareDesuso' . $fecha . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
?>