<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_vs.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_msdn.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    $vs = new desarrolloPruebaVS();
    $msdn = new desarrolloPruebaMSDN();
    $detalles         = new DetallesE_f();

    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"


    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Prueba/Desarrollo");

    $vert1 = 0;
    if(isset($_GET["vert1"]) && filter_var($_GET['vert1'], FILTER_VALIDATE_INT) !== false){
        $vert1 = $_GET["vert1"];
    }
    
    $fecha = $detalles->fechaSAMArchivo($vert1);

    // Add some data
    //inicio Desarrollo/Prueba Visual Studio
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visual Studio');
    $myWorkSheet->setCellValue('A1', 'Equipo')
                ->setCellValue('B1', 'Tipo')
                ->setCellValue('C1', 'Familia')
                ->setCellValue('D1', 'Edición')
                ->setCellValue('E1', 'Versión')
                ->setCellValue('F1', 'Fecha Instalación')
                ->setCellValue('G1', 'Usuario')
                ->setCellValue('H1', 'Equipo Usuario');
    $tabla  = $vs->listadoDesarrolloPruebaSam($vert1);
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["equipo"])
                    ->setCellValue('B'.$i, $row["tipo"])
                    ->setCellValue('C'.$i, $row["familia"])
                    ->setCellValue('D'.$i, $row["edicion"])
                    ->setCellValue('E'.$i, $row["version"])
                    ->setCellValue('F'.$i, $row["fechaInstalacion"])
                    ->setCellValue('G'.$i, $row["usuario"])
                    ->setCellValue('H'.$i, $row["equipoUsuario"]);
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 0);
    //fin Desarrollo/Prueba Visual Studio


    //inicio Desarrollo/Prueba MSDN
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'MSDN');
    $myWorkSheet->setCellValue('A1', 'Equipo')
                ->setCellValue('B1', 'Tipo')
                ->setCellValue('C1', 'Familia')
                ->setCellValue('D1', 'Edición')
                ->setCellValue('E1', 'Versión')
                ->setCellValue('F1', 'Fecha Instalación')
                ->setCellValue('G1', 'Usuario')
                ->setCellValue('H1', 'Equipo Usuario')
                ->setCellValue('H1', 'MSDN');
    $tabla = $msdn->listadoDesarrolloPruebaMSDNSam($vert1);
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["equipo"])
                    ->setCellValue('B'.$i, $row["tipo"])
                    ->setCellValue('C'.$i, $row["familia"])
                    ->setCellValue('D'.$i, $row["edicion"])
                    ->setCellValue('E'.$i, $row["version"])
                    ->setCellValue('F'.$i, $row["fechaInstalacion"])
                    ->setCellValue('G'.$i, $row["usuario"])
                    ->setCellValue('H'.$i, $row["equipoUsuario"])
                    ->setCellValue('H'.$i, $row["msdn"]);
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 1);
    //fin Desarrollo/Prueba MSDN

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Desarrollo/Pruebas' . $fecha . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
?>