<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    $moduloServidores = new moduloServidores();
    $detalles         = new DetallesE_f();

    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"


    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Servidores");

    $vert1 = 0;
    if(isset($_GET["vert1"]) && filter_var($_GET['vert1'], FILTER_VALIDATE_INT) !== false){
        $vert1 = $_GET["vert1"];
    }
    
    $fecha = $detalles->fechaSAMArchivo($vert1);

    // Add some data
    //inicio Windows Server Fisico
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Físico');
    $myWorkSheet->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Familia')
                ->setCellValue('C1', 'Edición')
                ->setCellValue('D1', 'Versión')
                ->setCellValue('E1', 'Tipo')
                ->setCellValue('F1', 'Procesadores')
                ->setCellValue('G1', 'Cores')
                ->setCellValue('H1', 'Lic Srv')
                ->setCellValue('I1', 'Lic Proc')
                ->setCellValue('J1', 'Lic Core');
    $tabla  = $moduloServidores->windowServerClienteSamArchivo($vert1, "Fisico");
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["equipo"])
                    ->setCellValue('B'.$i, $row["familia"])
                    ->setCellValue('C'.$i, $row["edicion"])
                    ->setCellValue('D'.$i, $row["version"])
                    ->setCellValue('E'.$i, $row["tipo"])
                    ->setCellValue('F'.$i, $row["cpu"])
                    ->setCellValue('G'.$i, $row["cores"])
                    ->setCellValue('H'.$i, $row["licSrv"])
                    ->setCellValue('I'.$i, $row["licProc"])
                    ->setCellValue('J'.$i, $row["licCore"]);
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 0);
    //fin Windows Server Fisico


    //inicio Windows Server Virtual
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Virtual');
    $myWorkSheet->setCellValue('A1', 'Cluster')
                ->setCellValue('B1', 'Host')
                ->setCellValue('C1', 'Nombre')
                ->setCellValue('D1', 'Familia')
                ->setCellValue('E1', 'Edición')
                ->setCellValue('F1', 'Versión')
                ->setCellValue('G1', 'Tipo')
                ->setCellValue('H1', 'Procesadores')
                ->setCellValue('I1', 'Cores')
                ->setCellValue('J1', 'Lic Srv')
                ->setCellValue('K1', 'Lic Proc')
                ->setCellValue('L1', 'Lic Core');
    $tabla = $moduloServidores->windowServerAlineacionSamArchivo($vert1);
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["cluster"])
                    ->setCellValue('B'.$i, $row["host"])
                    ->setCellValue('C'.$i, $row["equipo"])
                    ->setCellValue('D'.$i, $row["familia"])
                    ->setCellValue('E'.$i, $row["edicion"])
                    ->setCellValue('F'.$i, $row["version"])
                    ->setCellValue('G'.$i, $row["tipo"])
                    ->setCellValue('H'.$i, $row["cpu"])
                    ->setCellValue('I'.$i, $row["cores"])
                    ->setCellValue('J'.$i, $row["licSrv"])
                    ->setCellValue('K'.$i, $row["licProc"])
                    ->setCellValue('L'.$i, $row["licCore"]);
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 1);
    //fin Windows Server Virtual


    //inicio Windows Server Balanza
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Balanza');
    $myWorkSheet->setCellValue('A1', 'Familia')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Físico')
                ->setCellValue('F1', 'Virtual')
                ->setCellValue('G1', 'Total')
                ->setCellValue('H1', 'Neto');
    $tabla = $moduloServidores->balanzaServerSamArchivo($vert1, "Windows Server");
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["familia"])
                    ->setCellValue('B'.$i, $row["edicion"])
                    ->setCellValue('C'.$i, $row["version"])
                    ->setCellValue('D'.$i, $row["instalaciones"])
                    ->setCellValue('E'.$i, $row["Fisico"])
                    ->setCellValue('F'.$i, $row["Virtual"])
                    ->setCellValue('G'.$i, $row["Fisico"] + $row["Virtual"])
                    ->setCellValue('H'.$i, $row["instalaciones"] - ($row["Fisico"] + $row["Virtual"]));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 2);
    //fin Windows Server Balanza



    //inicio SQL Server Fisico
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Físico');
    $myWorkSheet->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Familia')
                ->setCellValue('C1', 'Edición')
                ->setCellValue('D1', 'Versión')
                ->setCellValue('E1', 'Tipo')
                ->setCellValue('F1', 'Procesadores')
                ->setCellValue('G1', 'Cores')
                ->setCellValue('H1', 'Lic Srv')
                ->setCellValue('I1', 'Lic Proc')
                ->setCellValue('J1', 'Lic Core');
    $tabla = $moduloServidores->sqlServerClienteSamArchivo($vert1, "Fisico");
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["equipo"])
                    ->setCellValue('B'.$i, $row["familia"])
                    ->setCellValue('C'.$i, $row["edicion"])
                    ->setCellValue('D'.$i, $row["version"])
                    ->setCellValue('E'.$i, $row["tipo"])
                    ->setCellValue('F'.$i, $row["cpu"])
                    ->setCellValue('G'.$i, $row["cores"])
                    ->setCellValue('H'.$i, $row["licSrv"])
                    ->setCellValue('I'.$i, $row["licProc"])
                    ->setCellValue('J'.$i, $row["licCore"]);
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 3);
    //fin SQL Server Fisico


    //inicio SQL Server Virtual
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Virtual');
    $myWorkSheet->setCellValue('A1', 'Cluster')
                ->setCellValue('B1', 'Host')
                ->setCellValue('C1', 'Nombre')
                ->setCellValue('D1', 'Familia')
                ->setCellValue('E1', 'Edición')
                ->setCellValue('F1', 'Versión')
                ->setCellValue('G1', 'Tipo')
                ->setCellValue('H1', 'Procesadores')
                ->setCellValue('I1', 'Cores')
                ->setCellValue('J1', 'Lic Srv')
                ->setCellValue('K1', 'Lic Proc')
                ->setCellValue('L1', 'Lic Core');
    $tabla = $moduloServidores->sqlServerAlineacionSamArchivo($vert1);
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["cluster"])
                    ->setCellValue('B'.$i, $row["host"])
                    ->setCellValue('C'.$i, $row["equipo"])
                    ->setCellValue('D'.$i, $row["familia"])
                    ->setCellValue('E'.$i, $row["edicion"])
                    ->setCellValue('F'.$i, $row["version"])
                    ->setCellValue('G'.$i, $row["tipo"])
                    ->setCellValue('H'.$i, $row["cpu"])
                    ->setCellValue('I'.$i, $row["cores"])
                    ->setCellValue('J'.$i, $row["licSrv"])
                    ->setCellValue('K'.$i, $row["licProc"])
                    ->setCellValue('L'.$i, $row["licCore"]);
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 4);
    //fin SQL Server Virtual


    //inicio SQL Server Balanza
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Balanza');
    $myWorkSheet->setCellValue('A1', 'Familia')
                ->setCellValue('B1', 'Edición')
                ->setCellValue('C1', 'Versión')
                ->setCellValue('D1', 'Instalaciones')
                ->setCellValue('E1', 'Físico')
                ->setCellValue('F1', 'Virtual')
                ->setCellValue('G1', 'Total')
                ->setCellValue('H1', 'Neto');
    $tabla = $moduloServidores->balanzaServerSamArchivo($vert1, "SQL Server");
    $i = 2;
    foreach($tabla as $row){
        $myWorkSheet->setCellValue('A'.$i, $row["familia"])
                    ->setCellValue('B'.$i, $row["edicion"])
                    ->setCellValue('C'.$i, $row["version"])
                    ->setCellValue('D'.$i, $row["instalaciones"])
                    ->setCellValue('E'.$i, $row["Fisico"])
                    ->setCellValue('F'.$i, $row["Virtual"])
                    ->setCellValue('G'.$i, $row["Fisico"] + $row["Virtual"])
                    ->setCellValue('H'.$i, $row["instalaciones"] - ($row["Fisico"] + $row["Virtual"]));
        $i++;
    }
    $objPHPExcel->addSheet($myWorkSheet, 5);
    //fin SQL Server Balanza

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Servidores' . $fecha . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
?>