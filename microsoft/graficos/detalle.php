<?php
$tituloWindows = 'Windows Operating System';

if($vert == 8 || $vert == 82 || $vert ==  83 || $vert == 84){
    $tituloWindows = 'Windows Server';
}
?>
<script type="text/javascript">
    $(function () {
        Highcharts.setOptions({
            colors: ['#DCDCDC', '#99BFDC', '#243C67', '#00AFF0', '#006FC0', '#95DEF8', '#579BCC', 
            '#344970', '#047FB3', '#05B5FE', '#81816A']
        });
<?php
//ver 0  ver 8
if($vert==0 /*|| $vert==8*/){
?>
        $('#container1').highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: '<?php echo $tituloWindows;?>'
            },
            subtitle: {
                text: 'ACTIVOS'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                        text: 'Unidades'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $uso1 + $uso2 + $uso3 ?></b> instalaciones<br/>'
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?>',
                colorByPoint: true,
                data: [{
                    name: 'Standard',
                    y: <?=$uso4?>,
                    color: '<?= $color4 ?>'    
                },
                 {
                    name: 'Enterprise',
                    y: <?=$uso1?>,
                    color: '<?= $color1 ?>'    
                },
                 {
                    name: 'Professional',
                    y: <?=$uso2?>,
                    color: '<?= $color2 ?>'    
                },
                {
                    name: 'Otros',
                    y: <?=$uso3?>,
                    color: '<?= $color3 ?>'    
                }]
            }]
        });
    });
        
    $(function () {        
        $('#container2').highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: '<?php echo $tituloWindows;?>'
            },
            subtitle: {
                text: 'INACTIVOS'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $nouso1 + $nouso2 + $nouso3 ?></b> instalaciones<br/>'
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?>',
                colorByPoint: true,
                data: [{
                    name: 'Standrad',
                    y: <?=$nouso4?>,
                    color: '<?= $color4 ?>'    
                },
                 {
                    name: 'Enterprise',
                    y: <?=$nouso1?>,
                    color: '<?= $color1 ?>'    
                },
                 {
                    name: 'Professional',
                    y: <?=$nouso2?>,
                    color: '<?= $color2 ?>'    
                },
                {
                    name: 'Otros',
                    y: <?=$nouso3?>,
                    color: '<?= $color3 ?>'    
                }]
            }]
        });
<?php
}//ver 0  ver 8

if($vert==8){
?>
        $('#container1').highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: '<?php echo $tituloWindows;?>'
            },
            subtitle: {
                text: 'ACTIVOS'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                        text: 'Unidades'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $uso1 + $uso2 + $uso3 + $uso4?></b> instalaciones<br/>'
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?>',
                colorByPoint: true,
                data: [{
                    name: 'Standard',
                    y: <?=$uso1?>,
                    color: '<?= $color1 ?>'    
                },
                 {
                    name: 'Datacenter',
                    y: <?=$uso2?>,
                    color: '<?= $color2 ?>'    
                },
                 {
                    name: 'Enterprise',
                    y: <?=$uso3?>,
                    color: '<?= $color3 ?>'    
                },
                {
                    name: 'Otros',
                    y: <?=$uso4?>,
                    color: '<?= $color4 ?>'    
                }]
            }]
        });
    });

    $(function () {
        $('#container2').highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: '<?php echo $tituloWindows;?>'
            },
            subtitle: {
                text: 'INACTIVOS'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $nouso1 + $nouso2 + $nouso3 + $nouso4?></b> instalaciones<br/>'
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?>',
                colorByPoint: true,
                data: [{
                    name: 'Standard',
                    y: <?=$nouso1?>,
                    color: '<?= $color1 ?>'    
                },
                 {
                    name: 'Datacenter',
                    y: <?=$nouso2?>,
                    color: '<?= $color2 ?>'    
                },
                {
                    name: 'Enterprise',
                    y: <?=$nouso3?>,
                    color: '<?= $color3 ?>'    
                },
                {
                    name: 'Otros',
                    y: <?=$nouso4?>,
                    color: '<?= $color4 ?>'    
                }]
            }]
        });
<?php
}//ver 8

//ver 81
if($vert==81){
?>
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: '<?php echo $tituloWindows;?> Standard'
            },
            subtitle: {
                text: 'ACTIVOS & INACTIVOS AD'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $uso1 + $nouso1 ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?> Standard',
                colorByPoint: true,
                data: [{
                    name:'Activos',
                    y:<?=$uso1?>,
                    color:'<?=$color1?>'
                },
                {
                    name:'Inactivos',
                    y:<?=$nouso1?>,
                    color:'<?=$color2?>'
                }]
            }]
        });
<?php
}//ver 81

//ver 82
if($vert==82){
?>
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: '<?php echo $tituloWindows;?> Datacenter'
            },
            subtitle: {
                text: 'ACTIVOS & INACTIVOS AD'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $uso1 + $nouso1 ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?> Datacenter',
                colorByPoint: true,
                data: [{
                    name:'Activos',
                    y:<?=$uso1?>,
                    color:'<?=$color1?>'
                },
                {
                    name:'Inactivos',
                    y:<?=$nouso1?>,
                    color:'<?=$color2?>'
                }]
            }]
        });
<?php
}//ver 82

if($vert==1){
?>
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: '<?php echo $tituloWindows;?> Standard'
            },
            subtitle: {
                text: 'ACTIVOS & INACTIVOS AD'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $uso4 + $nouso4 ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?> Standard',
                colorByPoint: true,
                data: [{
                    name:'Activos',
                    y:<?=$uso4?>,
                    color:'<?=$color1?>'
                },
                {
                    name:'Inactivos',
                    y:<?=$nouso4?>,
                    color:'<?=$color2?>'
                }]
            }]
        });
<?php
}//ver 1  ver 83

//ver 1  ver 82
if($vert==2 || $vert==83){
?>
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: '<?php echo $tituloWindows;?> Enterprise'
            },
            subtitle: {
                text: 'ACTIVOS & INACTIVOS AD'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $uso1 + $nouso1 ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?> Enterprise',
                colorByPoint: true,
                data: [{
                    name:'Activos',
                    y:<?=$uso1?>,
                    color:'<?=$color1?>'
                },
                {
                    name:'Inactivos',
                    y:<?=$nouso1?>,
                    color:'<?=$color2?>'
                }]
            }]
        });
<?php
}//ver 1  ver 82

//ver 2  ver 83
if($vert==3 /*|| $vert==83*/){
?>
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie' 
            },
            title: {
                text: '<?php echo $tituloWindows;?> Professional'
            },
            subtitle: {
                text: 'ACTIVOS & INACTIVOS AD'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $uso1 + $nouso1 ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?> Professional',
                colorByPoint: true,
                data: [{
                    name:'Activos',
                    y:<?=$uso1?>,
                    color:'<?=$color1?>'
                },
                {
                    name:'Inactivos',
                    y:<?=$nouso1?>,
                    color:'<?=$color2?>'
                }]
            }]
        });
<?php
}//ver 2  ver 83

//ver 3  ver 84
if($vert==4 || $vert==84){
?>
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: '<?php echo $tituloWindows;?> Others'
            },
            subtitle: {
                text: 'ACTIVOS & INACTIVOS AD'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b> <br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $uso3 + $nouso3 ?></b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                    showInLegend: true
                }
            },

            series: [
            {
                name: '<?php echo $tituloWindows;?> Others',
                colorByPoint: true,
                data: [{
                    name:'Activos',
                    y:<?=$uso3?>,
                    color:'<?=$color1?>'
                },
                {
                    name:'Inactivos',
                    y:<?=$nouso3?>,
                    color:'<?=$color2?>'
                }]
            }]
        });
<?php
}//ver 3  ver 84

//ver 5
if($vert==5){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Office'
            },
            subtitle: {
                text: 'Microsoft Office'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Office',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office','');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Office", "", $asig, $asignaciones);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 5

//ver 51
if($vert==51){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Office Standard'
            },
            subtitle: {
                text: 'Microsoft Office'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Office',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office','Standard');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Office", "Standard", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 51

//ver 52
if($vert==52){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Office Professional'
            },
            subtitle: {
                text: 'Microsoft Office'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Office',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office','Professional');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Office", "Professional", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 52

//ver 53
if($vert==53){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Office Otros'
            },
            subtitle: {
                text: 'Microsoft Office'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Office',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias2Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Office','Standard','Professional');
                    $lista_o = $resumen->listar_datos7AsignacionGrafico($_SESSION["client_id"], "Office", "Standard", "Professional", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 53

//ver 6
if($vert==6){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Project'
            },
            subtitle: {
                text: 'MS Project'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Project',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Project", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 6

//ver 61
if($vert==61){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Project Standard'
            },
            subtitle: {
                text: 'MS Project'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Project',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','Standard');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Project", "Standard", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 61

//ver 62
if($vert==62){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Project professional'
            },
            subtitle: {
                text: 'MS Project'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Project',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','Professional');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Project", "Professional", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 62

//ver 63
if($vert==63){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Project Others'
            },
            subtitle: {
                text: 'MS Project'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Project',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias2Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Project','Standard','Professional');
                    $lista_o = $resumen->listar_datos7AsignacionGrafico($_SESSION["client_id"], "Project", "Standard", "Professional", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 63

//ver 7
if($vert==7){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Visio'
            },
            subtitle: {
                text: 'MS Visio'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Visio',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Visio','');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Visio", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 7

//ver 71
if($vert==71){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Visio Standard'
            },
            subtitle: {
                text: 'MS Visio'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Visio',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Visio','Standard');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Visio", "Standard", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].''.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 71

//ver 72
if($vert==72){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Visio Professional'
            },
            subtitle: {
                text: 'MS Visio'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Visio',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Visio','Professional');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Visio", "Professional", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].''.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 72

//ver 73
if($vert==73){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Visio Others'
            },
            subtitle: {
                text: 'MS Visio'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                    enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Visio',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias2Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Visio','Standard','Professional');
                    $lista_o = $resumen->listar_datos7AsignacionGrafico($_SESSION["client_id"], "Visio", "Standard", "Professional", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].''.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 73

//ver 9
if($vert==9){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'SQL Server'
            },
            subtitle: {
                text: 'MS SQL'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'SQL Server',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "SQL Server", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 9

//ver 91
if($vert==91){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'SQL Server Standard'
            },
            subtitle: {
                text: 'MS SQL'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'SQL Server',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Standard');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "SQL Server", "Standard", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 91

//ver 92
if($vert==92){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'SQL Server Datacenter'
            },
            subtitle: {
                text: 'MS SQL'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Office',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Datacenter');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "SQL Server", "Datacenter", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 92

//ver 93
if($vert==93){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'SQL Server Enterprise'
            },
            subtitle: {
                text: 'MS SQL'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Office',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familiasGrafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Enterprise');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "SQL Server", "Enterprise", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 93

//ver 94
if($vert==94){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'SQL Server Others'
            },
            subtitle: {
                text: 'MS SQL'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'Office',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias3Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'SQL Server','Standard','Datacenter','Enterprise');
                    $lista_o = $resumen->listar_datos8AsignacionGrafico($_SESSION["client_id"], "SQL Server", "Standard", "Datacenter", "Enterprise", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 94

//ver 10
if($vert==10){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Visual Studio'
            },

            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'others',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias4Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Visual Studio');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Visual Studio", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 10

//ver 11
if($vert==11){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Exchange Server'
            },

            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'others',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias4Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Exchange Server');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Exchange Server", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 11

//ver 12
if($vert==12){
?>
   
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Sharepoint Server'
            },

            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'others',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias4Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Sharepoint Server');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Sharepoint Server", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });

<?php
}//ver 12

//ver 13
if($vert==13){
?>
    
        $('#container3').highcharts({
                chart: {
                type: 'column'
            },
            title: {
                text: 'Skype for Business'
            },

            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'others',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias4Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'Skype for Business');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "Skype for Business", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 13

//ver 14
if($vert==14){
?>
        $('#container3').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'System Center'
            },

            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Unidades'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> de <b><?= $total_i ?></b> instalaciones<br/>'
            },

            series: [{
                name: 'others',
                colorByPoint: true,
                data: [
                <?php
                    //$lista_o=$balance3g->listar_todo_familias4Grafico($_SESSION['client_id'], $_SESSION['client_empleado'], 'System Center');
                    $lista_o = $resumen->listar_datos6AsignacionGrafico($_SESSION["client_id"], "System Center", "", $asig, $asignaciones, $dup);
                    if($lista_o){
                        $i = 0;
                        foreach($lista_o as $reg_o){
                        ?>
                            {
                                name: '<?=$reg_o["office"].' '.$reg_o["version"]?>',
                                y: <?=$reg_o["instalaciones"]?>,
                                color: Highcharts.getOptions().colors[<?=$i?>]
                            }, 
                        <?php
                            $i++;
                        }
                    }
                ?>
                ]
            }],
        });
<?php
}//ver 14		
?>

    });
</script>