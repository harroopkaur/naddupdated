<script type="text/javascript">
    $(function () {
        Highcharts.setOptions({
            colors: ['#DCDCDC', '#99BFDC', '#243C67', '#00AFF0', '#006FC0', '#95DEF8', '#579BCC', 
            '#344970', '#047FB3', '#05B5FE', '#81816A']
        });
        <?php 
        if($vert == 0){
        ?>
        $('#container1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Client'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/>Computers: <b>{point.y:.0f}</b> of <b><?= $total_1LAc + $reconciliacion1c ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Computers',
                colorByPoint: true,
                data: [{
                    name: 'Not Scanned',
                    y: <?= $reconciliacion1c ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Scanned',
                    y: <?= $total_1LAc ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Server'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Computers: <b>{point.y:.0f}</b> of <b><?= $total_1LAs + $reconciliacion1s ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Computers',
                colorByPoint: true,
                data: [{
                    name: 'Not Scanned',
                    y: <?= $reconciliacion1s ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Scanned',
                    y: <?= $total_1LAs ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
        <?php
        }//0
        else if ($vert == 1 || $vert == 3 || $vert == 4) {
            if ($vert == 1) {
                $titulo = "Usability";
            } else if ($vert == 3){
                $titulo = "Optimization";
            } else if ($vert == 4){
                $titulo = "Unused Software";
            }
        ?>
                
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clients'
            },
            subtitle: {
                text: '<?php echo $titulo; ?>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Computers'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Computers: <b>{point.y:.0f}</b> of <b><?= $total_1 + $total_2 + $total_3 ?></b><br/>'
            },
            series: [{
                name: 'Clients',
                colorByPoint: true,
                data: [{
                    name: 'In Use',
                    y: <?= round($total_1, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Probably in Use',
                    y: <?= round($total_2, 0) ?>,
                    color: '<?= $color2 ?>'
                },
                {
                    name: 'Obsolete',
                    y: <?= round($total_3, 0) ?>,
                    color: '<?= $color3 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servers'
            },
            subtitle: {
                text: '<?php echo $titulo; ?>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Computers'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Computers: <b>{point.y:.0f}</b> of <b><?= $total_4 + $total_5 + $total_6 ?></b><br/>'
            },
            series: [{
                name: 'Servers',
                colorByPoint: true,
                data: [{
                    name: 'In Use',
                    y: <?= round($total_4, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Probably in Use',
                    y: <?= round($total_5, 0) ?>,
                    color: '<?= $color2 ?>'
                },
                {
                    name: 'Obsolete',
                    y: <?= round($total_6, 0) ?>,
                    color: '<?= $color3 ?>'
                }]
            }],
        });
        
        <?php
        }//1
        if ($vert == 2) {
        ?>

        $('#container3').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'SQL Server'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsolete',
                data: [0, <?= $servTotalSQLInact ?>, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Neto',
                data: [0, 0, <?= $servSQLNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Installations',
                data: [0, <?= $servTotalSQLInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Purchases',
                data: [<?= $servTotalSQLCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container4').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Windows Server'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsolete',
                data: [0, <?= $servTotalOSInact ?>, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Neto',
                data: [0, 0, <?= $servOSNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Installations',
                data: [0, <?= $servTotalOSInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Purchases',
                data: [<?= $servTotalOSCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container5').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Windows OS'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsolete',
                data: [0, <?= $clientTotalOSInact ?>, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Neto',
                data: [0, 0, <?= $clientOSNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Installations',
                data: [0, <?= $clientTotalOSInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Purchases',
                data: [<?= $clientTotalOSCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container6').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Office'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsolete',
                data: [0, <?= $clientTotalOfficeInact ?>, 0],
                color: '<?= $color4 ?>'
            },{
                name: 'Neto',
                data: [0, 0, <?= $clientOfficeNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Installations',
                data: [0, <?= $clientTotalOfficeInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Purchases',
                data: [<?= $clientTotalOfficeCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container7').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Others'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsolete',
                data: [0, <?= $clientTotalProductInact ?>, 0],
                color: '<?= $color4 ?>'
            },{
                name: 'Neto',
                data: [0, 0, <?= $clientProductNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Installations',
                data: [0, <?= $clientTotalProductInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Purchases',
                data: [<?= $clientTotalProductCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });
        
        <?php
        }
        
        if($vert == 5){
        ?>
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clients'
            },
            subtitle: {
                text: 'Detail per Device'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Software'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Software: <b>{point.y:.0f}</b> of <b><?= $totalMicrosoft ?></b><br/>'
            },
            series: [{
                name: 'Clients',
                colorByPoint: true,
                data: [{
                    name: 'Office',
                    y: <?= $cantOffice ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Visio',
                    y: <?= $cantVisio ?>,
                    color: '<?= $color2 ?>'
                },
                {
                    name: 'Project',
                    y: <?= $cantProject ?>,
                    color: '<?= $color3 ?>'
                },
                {
                    name: 'Visual Studio',
                    y: <?= $cantVisualStudio ?>,
                    color: '<?= $color4 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servers'
            },
            subtitle: {
                text: 'Detail per Device'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Software'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Software: <b>{point.y:.0f}</b> of <b><?= $cantWindowsServer ?></b><br/>'
            },
            series: [{
                name: 'Servers',
                colorByPoint: true,
                data: [{
                    name: 'Windows Server',
                    y: <?= $cantWindowsServer ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'SQL Server',
                    y: <?= $cantSQL ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
        <?php 
        }
        
        if($vert == 6){
        ?>
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clients'
            },
            subtitle: {
                text: 'Undiscovered Devices'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['In Use', 'Probably in Use', 'Obsolete']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nro. Computers'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top-right',
                y: 40,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [{
                name: 'Discovered',
                data: [<?= $listar_equipos["CActivoSi"] ?>, <?= $listar_equipos["CProbableSi"] ?>, <?= $listar_equipos["CObsoletoSi"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || '#000'
                }
            }, {
                name: 'Not Discovered',
                data: [<?= $listar_equipos["CActivoNo"] ?>, <?= $listar_equipos["CProbableNo"] ?>, <?= $listar_equipos["CObsoletoNo"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'red'
                }
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servers'
            },
            subtitle: {
                text: 'Undiscovered Devices'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['In Use', 'Probably in Use', 'Obsolete']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nro. Computers'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top-right',
                y: 40,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [{
                name: 'Discovered',
                data: [<?= $listar_equipos["SActivoSi"] ?>, <?= $listar_equipos["SProbableSi"] ?>, <?= $listar_equipos["SObsoletoSi"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || '#000'
                }
            }, {
                name: 'Not Discovered',
                data: [<?= $listar_equipos["SActivoNo"] ?>, <?= $listar_equipos["SProbableNo"] ?>, <?= $listar_equipos["SObsoletoNo"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'red'
                }
            }]
        });
        
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Client'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Computers: <b>{point.y:.0f}</b> of <b><?= $error462AC + $error70AC + $errorPingAC + $errorOtrosAC ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Computers',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Policy of Group',
                    y: <?= $error462AC ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70AC ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingAC ?>
                },
                {
                    name: 'Others',
                    y: <?= $errorOtrosAC ?>
                }]
            }]
        });
   
        $('#container4').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Server'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Devices: <b>{point.y:.0f}</b> of <b><?= $error462AS + $error70AS + $errorPingAS + $errorOtrosAS ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Computers',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Policy of Group',
                    y: <?= $error462AS ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70AS ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingAS ?>
                },
                {
                    name: 'Others',
                    y: <?= $errorOtrosAS ?>
                }]
            }]
        });
          
        $('#container5').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Client'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Computers: <b>{point.y:.0f}</b> of <b><?= $error462C + $error70C + $errorPingC + $errorOtrosC ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Computers',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Policy de Group',
                    y: <?= $error462C ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70C ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingC ?>
                },
                {
                    name: 'Others',
                    y: <?= $errorOtrosC ?>
                }]
            }]
        });
   
        $('#container6').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Server'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Computers: <b>{point.y:.0f}</b> of <b><?= $error462S + $error70S + $errorPingS + $errorOtrosS ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Computers',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Policy of Group',
                    y: <?= $error462S ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70S ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingS ?>
                },
                {
                    name: 'Others',
                    y: <?= $errorOtrosS ?>
                }]
            }]
        });
        <?php 
        } else if ($vert == 7){
        ?>
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clients'
            },
            subtitle: {
                text: 'Duplicate Software'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Software'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>'
            },
            series: [{
                name: 'Clients',
                colorByPoint: true,
                data: [{
                    name: 'Software',
                    y: <?= round($cantApliDuplicadoC, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Computers',
                    y: <?= round($cantEquiposDuplicadoC, 0) ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servers'
            },
            subtitle: {
                text: 'Duplicate Software'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Software'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>'
            },
            series: [{
                name: 'Servers',
                colorByPoint: true,
                data: [{
                    name: 'Software',
                    y: <?= round($cantApliDuplicadoS, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Computers',
                    y: <?= round($cantEquiposDuplicadoS, 0) ?>,
                    color: '<?= $color2 ?>'
                }]
            }],
        });
        <?php
        } else if($vert == 8){
        ?>
        $('#container1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Client'
            },
            subtitle: {
                text: 'Erroneous Installations'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Installations: <b>{point.y:.0f}</b> of <b><?= $totalErrorServidor ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Applications',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softErroneusServidor as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Server'
            },
            subtitle: {
                text: 'Erroneous Installations'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Applications: <b>{point.y:.0f}</b> of <b><?= $totalErrorCliente ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Applications',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softErroneusCliente as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
        <?php
        } else if($vert == 9){
        ?>
        $('#container1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Client'
            },
            subtitle: {
                text: 'Unsupported Software'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Computers: <b>{point.y:.0f}</b> of <b><?= $totalSinSoporteCliente ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Applications',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softSinSoporteCliente as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Server'
            },
            subtitle: {
                text: 'Unsupported Software'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Computers: <b>{point.y:.0f}</b> of <b><?= $totalSinSoporteServidor ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Applications',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softSinSoporteServidor as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
        <?php
        }//0
        ?>
    });
</script>