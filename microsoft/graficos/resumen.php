<script type="text/javascript">
    $(function () {
        Highcharts.setOptions({
            colors: ['#DCDCDC', '#99BFDC', '#243C67', '#00AFF0', '#006FC0', '#95DEF8', '#579BCC', 
            '#344970', '#047FB3', '#05B5FE', '#81816A']
        });
        <?php 
        if($vert == 0){
        ?>
        $('#container1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Cliente'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1LAc + $reconciliacion1c ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Equipos',
                colorByPoint: true,
                data: [{
                    name: 'No Levantado',
                    y: <?= $reconciliacion1c ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Levantado',
                    y: <?= $total_1LAc ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Servidor'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1LAs + $reconciliacion1s ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Equipos',
                colorByPoint: true,
                data: [{
                    name: 'No levantado',
                    y: <?= $reconciliacion1s ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Levantado',
                    y: <?= $total_1LAs ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
        <?php
        }//0
        else if ($vert == 1 || $vert == 3 || $vert == 4) {
            if ($vert == 1) {
                $titulo = "Usabilidad";
            } else if ($vert == 3){
                $titulo = "Optimización";
            } else if ($vert == 4){
                $titulo = "Software en Desuso";
            }
        ?>
                
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clientes'
            },
            subtitle: {
                text: '<?php echo $titulo; ?>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Equipos'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b><?= $total_1 + $total_2 + $total_3 ?></b><br/>'
            },
            series: [{
                name: 'Clientes',
                colorByPoint: true,
                data: [{
                    name: 'En Uso',
                    y: <?= round($total_1, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Probablemente en Uso',
                    y: <?= round($total_2, 0) ?>,
                    color: '<?= $color2 ?>'
                },
                {
                    name: 'Obsoleto',
                    y: <?= round($total_3, 0) ?>,
                    color: '<?= $color3 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servidores'
            },
            subtitle: {
                text: '<?php echo $titulo; ?>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Equipos'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Equipos: <b>{point.y:.0f}</b> de <b><?= $total_4 + $total_5 + $total_6 ?></b><br/>'
            },
            series: [{
                name: 'Servidores',
                colorByPoint: true,
                data: [{
                    name: 'En Uso',
                    y: <?= round($total_4, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Probablemente en Uso',
                    y: <?= round($total_5, 0) ?>,
                    color: '<?= $color2 ?>'
                },
                {
                    name: 'Obsoleto',
                    y: <?= round($total_6, 0) ?>,
                    color: '<?= $color3 ?>'
                }]
            }],
        });
        
        <?php
        }//1
        if ($vert == 2) {
        ?>

        $('#container3').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'SQL Server'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsoleto',
                data: [0, <?= $servTotalSQLInact ?>, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Neto',
                data: [0, 0, <?= $servSQLNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Instalaciones',
                data: [0, <?= $servTotalSQLInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $servTotalSQLCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container4').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Windows Server'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsoleto',
                data: [0, <?= $servTotalOSInact ?>, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Neto',
                data: [0, 0, <?= $servOSNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Instalaciones',
                data: [0, <?= $servTotalOSInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $servTotalOSCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container5').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Windows OS'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsoleto',
                data: [0, <?= $clientTotalOSInact ?>, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Neto',
                data: [0, 0, <?= $clientOSNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Instalaciones',
                data: [0, <?= $clientTotalOSInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $clientTotalOSCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container6').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Office'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsoleto',
                data: [0, <?= $clientTotalOfficeInact ?>, 0],
                color: '<?= $color4 ?>'
            },{
                name: 'Neto',
                data: [0, 0, <?= $clientOfficeNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Instalaciones',
                data: [0, <?= $clientTotalOfficeInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $clientTotalOfficeCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });

        $('#container7').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                 text: 'Otros'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['', '', ''],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    }
                }
            },
            legend: {
               reversed: true
            },
            plotOptions: {
                dataLabels: {
                    enabled: true
                },
                series: {
                   stacking: 'normal'
                }
            },
            tooltip: {
                headerFormat: ''
            },
            series: [{
                name: 'Obsoleto',
                data: [0, <?= $clientTotalProductInact ?>, 0],
                color: '<?= $color4 ?>'
            },{
                name: 'Neto',
                data: [0, 0, <?= $clientProductNeto ?>],
                color: '<?= $color3 ?>'
            }, {
                name: 'Instalaciones',
                data: [0, <?= $clientTotalProductInstal ?>, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Compras',
                data: [<?= $clientTotalProductCompras ?>, 0, 0],
                color: '<?= $color1 ?>'
            }]
        });
        
        <?php
        }
        
        if($vert == 5){
        ?>
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clientes'
            },
            subtitle: {
                text: 'Detalle por Equipo'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Software'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Software: <b>{point.y:.0f}</b> de <b><?= $totalMicrosoft ?></b><br/>'
            },
            series: [{
                name: 'Clientes',
                colorByPoint: true,
                data: [{
                    name: 'Office',
                    y: <?= $cantOffice ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Visio',
                    y: <?= $cantVisio ?>,
                    color: '<?= $color2 ?>'
                },
                {
                    name: 'Project',
                    y: <?= $cantProject ?>,
                    color: '<?= $color3 ?>'
                },
                {
                    name: 'Visual Studio',
                    y: <?= $cantVisualStudio ?>,
                    color: '<?= $color4 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servidores'
            },
            subtitle: {
                text: 'Detalle por Equipo'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nro. Software'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><br>Software: <b>{point.y:.0f}</b> de <b><?= $cantWindowsServer ?></b><br/>'
            },
            series: [{
                name: 'Servidores',
                colorByPoint: true,
                data: [{
                    name: 'Windows Server',
                    y: <?= $cantWindowsServer ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'SQL Server',
                    y: <?= $cantSQL ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
        <?php 
        }
        
        if($vert == 6){
        ?>
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clientes'
            },
            subtitle: {
                text: 'Equipos No Descubiertos'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['En Uso', 'Probablemente en Uso', 'Obsoleto']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nro. Equipos'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top-right',
                y: 40,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [{
                name: 'Descubiertos',
                data: [<?= $listar_equipos["CActivoSi"] ?>, <?= $listar_equipos["CProbableSi"] ?>, <?= $listar_equipos["CObsoletoSi"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || '#000'
                }
            }, {
                name: 'No Descubiertos',
                data: [<?= $listar_equipos["CActivoNo"] ?>, <?= $listar_equipos["CProbableNo"] ?>, <?= $listar_equipos["CObsoletoNo"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'red'
                }
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servidores'
            },
            subtitle: {
                text: 'Equipos No Descubiertos'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['En Uso', 'Probablemente en Uso', 'Obsoleto']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nro. Equipos'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top-right',
                y: 40,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [{
                name: 'Descubiertos',
                data: [<?= $listar_equipos["SActivoSi"] ?>, <?= $listar_equipos["SProbableSi"] ?>, <?= $listar_equipos["SObsoletoSi"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || '#000'
                }
            }, {
                name: 'No Descubiertos',
                data: [<?= $listar_equipos["SActivoNo"] ?>, <?= $listar_equipos["SProbableNo"] ?>, <?= $listar_equipos["SObsoletoNo"] ?>],
                dataLabels: {
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'red'
                }
            }]
        });
        
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Cliente'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $error462AC + $error70AC + $errorPingAC + $errorOtrosAC ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Equipos',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Politica de Grupo',
                    y: <?= $error462AC ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70AC ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingAC ?>
                },
                {
                    name: 'Otros',
                    y: <?= $errorOtrosAC ?>
                }]
            }]
        });
   
        $('#container4').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Servidor'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $error462AS + $error70AS + $errorPingAS + $errorOtrosAS ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Equipos',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Politica de Grupo',
                    y: <?= $error462AS ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70AS ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingAS ?>
                },
                {
                    name: 'Otros',
                    y: <?= $errorOtrosAS ?>
                }]
            }]
        });
          
        $('#container5').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Cliente'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $error462C + $error70C + $errorPingC + $errorOtrosC ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Equipos',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Politica de Grupo',
                    y: <?= $error462C ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70C ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingC ?>
                },
                {
                    name: 'Otros',
                    y: <?= $errorOtrosC ?>
                }]
            }]
        });
   
        $('#container6').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Servidor'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $error462S + $error70S + $errorPingS + $errorOtrosS ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Equipos',
                colorByPoint: true,
                data: [{
                    name: 'Firewall - Antivirus - Politica de Grupo',
                    y: <?= $error462S ?>
                },
                {
                    name: 'Permisología',
                    y: <?= $error70S ?>
                },
                {
                    name: 'Equipo apagado',
                    y: <?= $errorPingS ?>
                },
                {
                    name: 'Otros',
                    y: <?= $errorOtrosS ?>
                }]
            }]
        });
        <?php 
        } else if ($vert == 7){
        ?>
        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Clientes'
            },
            subtitle: {
                text: 'Software Duplicado'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Software'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>'
            },
            series: [{
                name: 'Clientes',
                colorByPoint: true,
                data: [{
                    name: 'Software',
                    y: <?= round($cantApliDuplicadoC, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Equipos',
                    y: <?= round($cantEquiposDuplicadoC, 0) ?>,
                    color: '<?= $color2 ?>'
                }]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Servidores'
            },
            subtitle: {
                text: 'Software Duplicado'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Software'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>'
            },
            series: [{
                name: 'Servidores',
                colorByPoint: true,
                data: [{
                    name: 'Software',
                    y: <?= round($cantApliDuplicadoS, 0) ?>,
                    color: '<?= $color1 ?>'
                },
                {
                    name: 'Equipos',
                    y: <?= round($cantEquiposDuplicadoS, 0) ?>,
                    color: '<?= $color2 ?>'
                }]
            }],
        });
        <?php
        } else if($vert == 8){
        ?>
        $('#container1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Cliente'
            },
            subtitle: {
                text: 'Instalaciones Erróneas'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Instalaciones: <b>{point.y:.0f}</b> de <b><?= $totalErrorServidor ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Aplicaciones',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softErroneusServidor as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Servidor'
            },
            subtitle: {
                text: 'Instalaciones Erróneas'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Aplicaciones: <b>{point.y:.0f}</b> de <b><?= $totalErrorCliente ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Aplicaciones',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softErroneusCliente as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
        <?php
        } else if($vert == 9){
        ?>
        $('#container1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Cliente'
            },
            subtitle: {
                text: 'Software Sin Soporte'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $totalSinSoporteCliente ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/}

                }
            },
            series: [{
                name: 'Aplicaciones',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softSinSoporteCliente as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
   
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                size: '75%'
            },
            title: {
                text: 'Servidor'
            },
            subtitle: {
                text: 'Software Sin Soporte'  
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b><?= $totalSinSoporteServidor ?>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        /*style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                         }*/
                    }
                }
            },
            series: [{
                name: 'Aplicaciones',
                colorByPoint: true,
                data: [ 
                    <?php 
                    foreach($softSinSoporteServidor as $row){
                    ?>
                        {
                            name: '<?= $row["familia"] ?>',
                            y: <?= $row["cantidad"] ?>
                        },
                    <?php
                    }
                    ?>
                ]
            }]
        });
        <?php
        }//0
        ?>
    });
</script>