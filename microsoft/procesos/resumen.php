<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");

$detalles1 = new DetallesE_f();
$detalles2 = new DetallesE_f();
$balance2  = new Balance_f();
$balance   = new Balance_f();
$moduloSam = new ModuloSam();
$compras = new Compras_f();
$tipoDespliegue = new clase_archivos_fabricantes();
$resumen = new Resumen_Of();
$general = new General();

$total_1LAc       = 0;
$total_1InacLAc   = 0;
$total_2DVc       = 0;
$total_1LAs       = 0;
$total_1InacLAs   = 0;
$total_2DVs       = 0;
$reconciliacion1c = 0;
$reconciliacion1s = 0;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$total_1 = 0;
$total_2 = 0;
$total_3 = 0;
$total_4 = 0;
$total_5 = 0;
$total_6 = 0;
$tclient = 0;
$tserver = 0;
$activosc = 0;
$activoss = 0;

$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';

$fabricante = 3;

$rowTipoDespliegue =  $tipoDespliegue->archivosDespliegue($_SESSION["client_id"], $_SESSION["client_empleado"], 3);

$vert = 0;
if (isset($_GET['vert']) && filter_var($_GET["vert"], FILTER_VALIDATE_INT) !== false) {
    $vert = $_GET['vert'];
}

$asig = "";
if(isset($_GET['asig'])){
    $asig = $general->get_escape($_GET["asig"]);
}

$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

if($vert == 0 || $vert == 7 || $vert == 8 || $vert == 9){
    if ($rowTipoDespliegue["tipoDespliegue"] == 0 || $rowTipoDespliegue["tipoDespliegue"] == 1) {
        //$listar_equipos = $detalles1->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $listar_equipos = $detalles1->listar_todo1Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    }
    else if($rowTipoDespliegue["tipoDespliegue"] == 2){
        //$listar_equipos = $detalles1->listar_segmentado1($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $listar_equipos = $detalles1->listar_segmentado1Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    }
    
    if ($listar_equipos) {
        foreach ($listar_equipos as $reg_equipos) {

            if ($reg_equipos["tipo"] == 1) {//cliente
                $tclient = ($tclient + 1);
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAc = ($total_1LAc + 1);
                }

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVc = ($total_2DVc + 1);
                }
                else{
                    $total_1InacLAc++;
                }
            } else {//server
                if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                    $total_1LAs = ($total_1LAs + 1);
                }

                $tserver = ($tserver + 1);

                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2DVs = ($total_2DVs + 1);
                }
                else{
                    $total_1InacLAs++;
                }
            }
        }
        $reconciliacion1c = $total_2DVc - $total_1LAc;
        $reconciliacion1s = $total_2DVs - $total_1LAs;
    }
}//0

if ($vert == 1 || $vert == 3 || $vert == 4 || $vert == 5) {
    //$listar_equipos = $detalles1->listar_todo1($_SESSION['client_id'], $_SESSION["client_empleado"]);
    $listar_equipos = $detalles1->listar_todo1Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    
    if ($listar_equipos) {
        foreach ($listar_equipos as $reg_equipos) {

            if ($reg_equipos["tipo"] == 1) {
                $tclient = ($tclient + 1);

                if ($reg_equipos["rango"] == 1) {
                    $total_1 = ($total_1 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activosc = ($activosc + 1);
                    }
                } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_2 = ($total_2 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activosc = ($activosc + 1);
                    }
                } else {
                    $total_3 = ($total_3 + 1);
                }
            } else {//server
                $tserver = ($tserver + 1);

                if ($reg_equipos["rango"] == 1) {
                    $total_4 = ($total_4 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activoss = ($activoss + 1);
                    }
                } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $total_5 = ($total_5 + 1);
                    if ($reg_equipos["errors"] == 'Ninguno') {
                        $activoss = ($activoss + 1);
                    }
                } else {
                    $total_6 = ($total_6 + 1);
                }
            }
        }
    }
}//1

if ($vert == 2) {
    $asig = "";
    if(isset($_GET['asig'])){
        $asig = $general->get_escape($_GET["asig"]);
    }
    
    $balanza = true;

    //$servTotalSQLCompras = 0;
    $servTotalSQLInstal = 0;
    $servTotalSQLInact = 0;
    //$servSQLNeto = 0;

    //$servTotalOSCompras = 0;
    $servTotalOSInstal = 0;
    $servTotalOSInact = 0;
    //$servOSNeto = 0;

    //$clientTotalOSCompras = 0;
    $clientTotalOSInstal = 0;
    $clientTotalOSInact = 0;
    //$clientOSNeto = 0;

    //$clientTotalOfficeCompras = 0;
    $clientTotalOfficeInstal = 0;
    $clientTotalOfficeInact = 0;
    //$clientOfficeNeto = 0;

    //$clientTotalProductCompras = 0;
    $clientTotalProductInstal = 0;
    $clientTotalProductInact = 0;
    //$clientProductNeto = 0;
    
    $listar_SQL = $resumen->instalResumenAsignacion($_SESSION['client_id'], "SQL Server", "", $asig, $asignaciones); 
    foreach ($listar_SQL as $reg_equipos) {
        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
            $servTotalSQLInstal += $reg_equipos["instalaciones"];
        } else{
            $servTotalSQLInact += $reg_equipos["instalaciones"];
        }
    }
    $servTotalSQLCompras = $compras->totalCompras($_SESSION['client_id'], "SQL Server", "", $asig, $asignaciones);
    $servSQLNeto = $servTotalSQLCompras - ($servTotalSQLInstal + $servTotalSQLInact);
    
    $listar_OS = $detalles1->totalWindowsAsignacion($_SESSION['client_id'], "Windows Server", "", $asig, $asignaciones); 
    foreach ($listar_OS as $reg_equipos) {
        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
            $servTotalOSInstal += $reg_equipos["instalaciones"];
        } else{
            //$servTotalOSInact += $reg_equipos["instalaciones"];
        }
    }
    $servTotalOSCompras = $compras->totalCompras($_SESSION['client_id'], "Windows Server", "", $asig, $asignaciones);
    $servOSNeto = $servTotalOSCompras - ($servTotalOSInstal + $servTotalOSInact);
    
    $listar_equipos = $detalles1->totalWindowsAsignacion($_SESSION['client_id'], "Windows", "", $asig, $asignaciones); 
    foreach ($listar_equipos as $reg_equipos) {
        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
            $clientTotalOSInstal += $reg_equipos["instalaciones"];
        } else{
            //$clientTotalOSInact += $reg_equipos["instalaciones"];
        }
    }
    $clientTotalOSCompras = $compras->totalCompras($_SESSION['client_id'], "Windows", "", $asig, $asignaciones);
    $clientOSNeto = $clientTotalOSCompras - ($clientTotalOSInstal + $clientTotalOSInact);
    
    $listar_Of = $resumen->instalResumenAsignacion($_SESSION['client_id'], "Office", "", $asig, $asignaciones); 
    foreach ($listar_Of as $reg_equipos) {
        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
            $clientTotalOfficeInstal += $reg_equipos["instalaciones"];
        } else{
            $clientTotalOfficeInact += $reg_equipos["instalaciones"];
        }
    }
        
    $clientTotalOfficeCompras = $compras->totalCompras($_SESSION['client_id'], "Office", "", $asig, $asignaciones);
    $clientOfficeNeto = $clientTotalOfficeCompras - ($clientTotalOfficeInstal + $clientTotalOfficeInact);
    
    $listar_prod = $resumen->instalResumenAsignacion1($_SESSION['client_id'], $asig, $asignaciones); 
    foreach ($listar_prod as $reg_equipos) {
        if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
            $clientTotalProductInstal += $reg_equipos["instalaciones"];
        } else{
            $clientTotalProductInact += $reg_equipos["instalaciones"];
        }
    }
    
    $clientTotalProductCompras = $compras->totalCompras1($_SESSION['client_id'], $asig, $asignaciones);
    $clientProductNeto = $clientTotalProductCompras - ($clientTotalProductInstal + $clientTotalProductInact);
    /*$listar_SQL = $balance2->listar_todo_cliente2($_SESSION['client_id'], $_SESSION["client_empleado"], "SQL");
    if ($listar_SQL) {
        foreach ($listar_SQL as $reg_equipos) {
            //if($reg_equipos->tipo==2){//servidor
            $servTotalSQLCompras += $reg_equipos["compra"];
            $servTotalSQLInstal += $reg_equipos["instalaciones"];
            //}
        }
        $servSQLNeto = $servTotalSQLCompras - $servTotalSQLInstal;
    }
    

    $listar_OS = $balance2->listar_todo_cliente2($_SESSION['client_id'], $_SESSION["client_empleado"], "Windows");
    if ($listar_OS) {
        foreach ($listar_OS as $reg_equipos) {
            if ($reg_equipos["tipo"] == 1) {//cliente
                $clientTotalOSCompras += $reg_equipos["compra"];
                //$clientTotalOSInstal += $reg_equipos->instalaciones;
            } else {//servidor
                $servTotalOSCompras += $reg_equipos["compra"];
                //$servTotalOSInstal += $reg_equipos->instalaciones;
            }
        }
    }
    
    $listar_equipos = $detalles1->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);
    if ($listar_equipos) {
        foreach ($listar_equipos as $reg_equipos) {

            if ($reg_equipos["tipo"] == 1) {//cliente
                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $clientTotalOSInstal++;
                }
                else{
                    $total_1InacLAc++;
                }
            } else {//server
                if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                    $servTotalOSInstal++;
                }
                else{
                    $total_1InacLAs++;
                }
            }
        }
        $servOSNeto = $servTotalOSCompras - $servTotalOSInstal;
        $clientOSNeto = $clientTotalOSCompras - $clientTotalOSInstal;
    }
    
    

    $listar_Of = $balance2->listar_todo_cliente2($_SESSION['client_id'], $_SESSION["client_empleado"], "Office");
    if ($listar_Of) {
        foreach ($listar_Of as $reg_equipos) {
            //if ($reg_equipos->tipo == 1) {//cliente
                $clientTotalOfficeCompras += $reg_equipos["compra"];
                $clientTotalOfficeInstal += $reg_equipos["instalaciones"];
            //}
        }
        $clientOfficeNeto = $clientTotalOfficeCompras - $clientTotalOfficeInstal;
    }

    $listar_prod = $balance2->listar_todo_cliente1($_SESSION['client_id'], $_SESSION["client_empleado"]);
    if ($listar_prod) {
        foreach ($listar_prod as $reg_equipos) {
            //if ($reg_equipos->tipo == 1) {//cliente
                $clientTotalProductCompras += $reg_equipos["compra"];
                $clientTotalProductInstal += $reg_equipos["instalaciones"];
            //}
        }
        $clientProductNeto = $clientTotalProductCompras - $clientTotalProductInstal;
    }*/
    
    
}

if($vert == 4){
    $totalEquiposOS = 0;
    $totalEquiposWindowsServer = 0;
    
    $cantDesusoOS = 0;
    $cantDesusoWindowsServer = 0;
        
    $desusoOS = $detalles1->listaProductosOSDesusoAsignacion($_SESSION["client_id"], "Windows", $asig, $asignaciones);
    $desusoWindowsServer = $detalles1->listaProductosOSDesusoAsignacion($_SESSION["client_id"], "Windows Server", $asig, $asignaciones);
    $desusoOffice = $detalles1->listaProductosDesusoAsignacion($_SESSION["client_id"], "Office", "Windows", $asig, $asignaciones);
    $desusoVisio = $detalles1->listaProductosDesusoAsignacion($_SESSION["client_id"], "Visio", "Windows", $asig, $asignaciones);
    $desusoProject = $detalles1->listaProductosDesusoAsignacion($_SESSION["client_id"], "Project", "Windows", $asig, $asignaciones);
    $desusoVisualStudio = $detalles1->listaProductosDesusoAsignacion($_SESSION["client_id"], "Visual Studio", "Windows", $asig, $asignaciones);
    $desusoSQLServer = $detalles1->listaProductosDesusoAsignacion($_SESSION["client_id"], "SQL Server", "Windows Server", $asig, $asignaciones);
    
    $familia = array("Office", "Visio", "Project", "Visual Studio", "SQL Server");
    $desusoOtros = $detalles1->listaProductosOtrosDesusoAsignacion($_SESSION["client_id"], $familia, "Windows", $asig, $asignaciones);
    
    foreach($desusoOS as $row){
        if($row["rango"] > 1){
            $cantDesusoOS++;
        }
        $totalEquiposOS++; 
    }
    
    foreach($desusoWindowsServer as $row){
        if($row["rango"] > 1){
            $cantDesusoWindowsServer++;
        }
        $totalEquiposWindowsServer++;
    }
    
    $cantDesusoOffice = count($desusoOffice);
    $cantAux = $cantDesusoOffice;
    
    $cantDesusoVisio = count($desusoVisio);      
    if($cantDesusoVisio > $cantAux){
        $cantAux = $cantDesusoVisio;
    }
    
    $cantDesusoProject = count($desusoProject);
    if($cantDesusoProject > $cantAux){
        $cantAux = $cantDesusoProject;
    }
    
    $cantDesusoVisualStudio = count($desusoVisualStudio);
    if($cantDesusoVisualStudio > $cantAux){
        $cantAux = $cantDesusoVisualStudio;
    }
    
    $cantDesusoSQLServer = count($desusoSQLServer);
    $cantDesusoOtros =  count($desusoOtros);
    if($cantDesusoOtros > $cantAux){
        $cantAux = $cantDesusoOtros;
    }
    
    $porcentajeDesusoCliente = 0;
    if($cantDesusoOS > 0){
        $porcentajeDesusoCliente = round($cantAux * 100 / $cantDesusoOS, 0);
    }
    
    $porcentajeDesusoServidor = 0;
    if($totalEquiposWindowsServer > 0){
        $porcentajeDesusoServidor = round($cantDesusoSQLServer * 100 / $totalEquiposWindowsServer, 0);
    }
    
    $productosDesusoCliente = $detalles1->productosDesusoClienteAsignacion($_SESSION["client_id"], $asig, $asignaciones);
    $productosDesusoServidor = $detalles1->productosDesusoServidorAsignacion($_SESSION["client_id"], $asig, $asignaciones);
}
else if($vert == 5){
    $detalleEquiposCliente = $detalles1->listar_detalleEquipoAsignacion($_SESSION['client_id'], $asig, $asignaciones);
    $detalleEquiposServidor = $detalles1->listar_detalleEquipoServidorAsignacion($_SESSION['client_id'], $asig, $asignaciones);

    //grafica
    $cantOffice = $detalles1->total_equiposMicrosoftAsignacion($_SESSION['client_id'], 'Windows', 'Office', $asig, $asignaciones);
    $cantVisio = $detalles1->total_equiposMicrosoftAsignacion($_SESSION['client_id'], 'Windows', 'Visio', $asig, $asignaciones);
    $cantProject = $detalles1->total_equiposMicrosoftAsignacion($_SESSION['client_id'], 'Windows', 'Project', $asig, $asignaciones);
    $cantVisualStudio = $detalles1->total_equiposMicrosoftAsignacion($_SESSION['client_id'], 'Windows', 'Visual Studio', $asig, $asignaciones);
    $totalMicrosoft = $cantOffice + $cantVisio + $cantProject + $cantVisualStudio; 

    $winServer = $detalles1->totalWindowsAsignacion($_SESSION['client_id'], 'Windows Server', "", $asig, $asignaciones);
    $cantWindowsServer = 0;
    foreach($winServer as $row){
        $cantWindowsServer += $row["instalaciones"];
    }
    
    $cantSQL = $detalles1->total_equiposMicrosoftAsignacion($_SESSION['client_id'], 'Windows Server', 'SQL Server', $asig, $asignaciones);
}
else if($vert == 6){
    $listar_equipos = $detalles1->listarDescubiertosAsignacion($_SESSION['client_id'], $asig, $asignaciones);
    $escaneo = new Scaneo_f();
    $tablaEscaneo = $escaneo->listar_equiposNoDescubiertosAsignacion($_SESSION["client_id"], $asig, $asignaciones);
    $tablaEscaneoActivos = $escaneo->listar_equiposNoDescubiertosActivosAsignacion($_SESSION["client_id"], $asig, $asignaciones);
    
    //inicio grafica Activos
    $error462AC = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 1, "Error 462 - The remote server machine does not exist or is unavailable", $asig, $asignaciones);
    $error70AC = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 1, "Error 70 - Permission denied", $asig, $asignaciones);
    $errorPingAC = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 1, "Ping Sin Respuesta", $asig, $asignaciones);
    $errorOtrosAC = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 1, "Otros", $asig, $asignaciones);
    
    $error462AS = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 2, "Error 462 - The remote server machine does not exist or is unavailable", $asig, $asignaciones);
    $error70AS = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 2, "Error 70 - Permission denied", $asig, $asignaciones);
    $errorPingAS = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 2, "Ping Sin Respuesta", $asig, $asignaciones);
    $errorOtrosAS = $escaneo->totalNoDescubiertosActivosGraficoAsignacion($_SESSION["client_id"], 2, "Otros", $asig, $asignaciones);
    //fin grafica Activos
    
    //inicio grafica total
    $error462C = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 1, "Error 462 - The remote server machine does not exist or is unavailable", $asig, $asignaciones);
    $error70C = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 1, "Error 70 - Permission denied", $asig, $asignaciones);
    $errorPingC = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 1, "Ping Sin Respuesta", $asig, $asignaciones);
    $errorOtrosC = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 1, "Otros", $asig, $asignaciones);
    
    $error462S = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 2, "Error 462 - The remote server machine does not exist or is unavailable", $asig, $asignaciones);
    $error70S = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 2, "Error 70 - Permission denied", $asig, $asignaciones);
    $errorPingS = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 2, "Ping Sin Respuesta", $asig, $asignaciones);
    $errorOtrosS = $escaneo->totalNoDescubiertosGraficoAsignacion($_SESSION["client_id"], 2, "Otros", $asig, $asignaciones);
    //fin grafica total
} else if($vert == 7){
    $softDuplicado = $resumen->softwareDuplicate($_SESSION["client_id"], $asig, $asignaciones);
    
    //inicio grafico Cliente
    $cantApliDuplicadoC = count($resumen->softwareDuplicate($_SESSION["client_id"], $asig, $asignaciones, "cliente"));
    $cantEquiposDuplicadoC = count($resumen->softwareDuplicateEquiposGrafico($_SESSION["client_id"], "cliente", $asig, $asignaciones));
    //fin grafico Cliente
    
    //inicio grafico Servidor
    $cantApliDuplicadoS = count($resumen->softwareDuplicate($_SESSION["client_id"], $asig, $asignaciones, "servidor"));
    $cantEquiposDuplicadoS = count($resumen->softwareDuplicateEquiposGrafico($_SESSION["client_id"], "servidor", $asig, $asignaciones));
    //fin grafico Servidor
} else if($vert == 8){
    $softErroneus = $detalles1->erroneus($_SESSION["client_id"]);
    
    //inicio grafico Cliente
    $softErroneusCliente = $detalles1->erroneusCantidad($_SESSION["client_id"], "cliente");
    $totalErrorCliente = 0;
    foreach($softErroneusCliente as $row){
        $totalErrorCliente += $row["cantidad"];
    }
    //fin grafico Cliente
    
    //inicio grafico Servidor
    $softErroneusServidor = $detalles1->erroneusCantidad($_SESSION["client_id"], "servidor");
    $totalErrorServidor = 0;
    foreach($softErroneusServidor as $row){
        $totalErrorServidor += $row["cantidad"];
    }
    //fin grafico Servidor
} else if($vert == 9){
    $softSinSoporte = $resumen->productosSinSoporte($_SESSION["client_id"]);
    
    //inicio grafico Cliente
    $softSinSoporteCliente = $resumen->productosSinSoporteGrafico($_SESSION["client_id"], "cliente");
    $totalSinSoporteCliente = 0;
    foreach($softSinSoporteCliente as $row){
        $totalSinSoporteCliente += $row["cantidad"];
    }
    //fin grafico Cliente
    
    //inicio grafico Servidor
    $softSinSoporteServidor = $resumen->productosSinSoporteGrafico($_SESSION["client_id"], "servidor");
    $totalSinSoporteServidor = 0;
    foreach($softSinSoporteServidor as $row){
        $totalSinSoporteServidor += $row["cantidad"];
    }
    //fin grafico Servidor
}