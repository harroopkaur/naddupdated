<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

$permisos = new ModuloSam();
$acceso = 0;
if($permisos->existe_permiso($_SESSION['client_id'], 7) > 0){ 
    $acceso = 1;
}