<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

// Clases 
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo_troubleshooting.php");

// Objetos
$escaneo = new Scaneo_f();
$escaneoTrouble = new escaneoTroubleshoot();
$general = new General();

$tablaEscaneo = $escaneo->listar_equiposNoDescubiertos($_SESSION["client_id"], $_SESSION["client_empleado"]);

$tablaEscaneoTrouble = array();
$exito = 0;
$error = 0;

$buscarResultado = 0;
if(isset($_POST["buscarResultado"]) && $_POST["buscarResultado"] == 1){
    $buscarResultado = 1;
}

if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"]) && $buscarResultado == 1){
    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];

    // Validaciones
    if($nombre_imagen!=""){
        $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
        $long = count($extension) - 1;
        if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
        // Permitir subir solo imagenes JPG,
    }else{
        $error=2;	
    }

    if(!file_exists($GLOBALS['app_root'] . "/tmp/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'])){
        mkdir($GLOBALS['app_root'] . "/tmp/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'], 0777, true);
    }
    
    $rar_file = rar_open($_FILES['archivo']['tmp_name'], 'InfoLA2015**') or die("Can't open Rar archive");

    $entries = rar_list($rar_file);
    
    foreach ($entries as $entry) {
        if($entry->getName() == "Resultados_Escaneo.csv"){
            $entry->extract('tmp/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/");
        }
    }

    rar_close($rar_file);

    //inicio escaneo
    if(file_exists("tmp/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv")){
        $archivo = "tmp/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";
        
        if($general->obtenerSeparador($archivo) === true){
            if (($fichero = fopen($archivo, "r")) !== FALSE) {
                $i=1;
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    if($i == 2 && ($datos[0] != "Hostname" ||  $datos[1] != "Status" || $datos[2] != "Error")){
                        $error = 4;
                        break;
                    }
                    $i++;
                }
                fclose($fichero);
            }
        }else{
            $error = 5;
        }
        
        if($error == 0){
            if($general->obtenerSeparador($archivo) === true){   
                if (($fichero = fopen( $archivo, "r")) !== FALSE) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if ($i > 2) {
                            $host = explode('.', $datos[0]);
                            if(filter_var($datos[0], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[0];
                            }

                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", "
                                . ":errors" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", "
                                . ":errors" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION["client_id"];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"]; 
                            $bloqueValores[":equipo" . $j] = $host[0];
                            $bloqueValores[":status" . $j] = $datos[1];
                            $bloqueValores[":errors" . $j] = $datos[2];

                            if($j == $general->registrosBloque){
                                if(!$escaneoTrouble->insertarEnBloque($bloque, $bloqueValores)){ 
                                }
                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $i++;
                        $exito = 1;
                    }

                    if($insertarBloque === true){
                        if(!$escaneoTrouble->insertarEnBloque($bloque, $bloqueValores)){    
                        }
                    }
                }
            }

            $tablaEscaneoTrouble = $escaneoTrouble->listar_equiposNoDescubiertos($_SESSION["client_id"], $_SESSION["client_empleado"]);
        } 
    } else {
        $error = 3;
    }
    //fin escaneo
}