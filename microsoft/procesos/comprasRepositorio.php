<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

// Objetos
$compra_datos   = new Compras_f();
$compra_datos2  = new Compras_f();
$resumen_o      = new Resumen_Of();
$resumen_o2     = new Resumen_Of();
$resumen_s      = new Resumen_Of();
$resumen_s2     = new Resumen_Of();
$resumen_ot     = new Resumen_Of();
$resumen_ot2    = new Resumen_Of();
$balance_datos  = new Balance_f();
$balance_datos2 = new Balance_f();
$office         = new Equivalencias();
$office2        = new Equivalencias();
$general        = new General();
$detalle        = new DetallesE_f();
$nueva_funcion  = new funcionesSam();
$log = new log();

$query = $nueva_funcion->listadoContratosCliente($_SESSION["client_id"], $_SESSION["client_empleado"], 3);

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if(isset($_POST['insertarRepoF']) && $_POST["insertarRepoF"] == 1) {

    if ($error == 0) {

        $compra_datos->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        $balance_datos2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

        $tablaCompraRepo = array();
        if (isset($_POST["check"])) {
            $tablaCompraRepo = $_POST["check"];

            for ($i = 0; $i < count($tablaCompraRepo); $i++) {
                $newArray = explode("*", $tablaCompraRepo[$i]);
                $familia  = $general->get_escape($newArray[0]);
                $edicion  = $general->get_escape($newArray[1]);
                $version  = $general->get_escape($newArray[2]);
                $tipo     = $general->get_escape($newArray[3]);
                $cantidad = 0;
                if(filter_var($newArray[4], FILTER_VALIDATE_FLOAT) !== false){
                    $cantidad = $newArray[4];
                }
                $precio = 0;
                if(filter_var($newArray[5], FILTER_VALIDATE_FLOAT) !== false){
                    $precio = $general->get_escape($newArray[5]);
                }
                $asignacion = $general->get_escape($newArray[6]);
                if($general->validarAsignacion($_SESSION["client_id"], $_SESSION["client_empleado"], $asignacion)){
                    if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], trim($familia), trim($edicion), trim($version), $tipo, $cantidad, $precio, $asignacion)) {
                        echo $compra_datos->error;
                    }
                }
                $exito2 = 1;
            }//WhILE
        }
    }

    if ($exito2 == 1) {

        //$lista_compras = $compra_datos2->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);
        /*$lista_office = $office->listar_todo(3); //3 es el id fabricante Microsoft
        if ($lista_office) {
            foreach ($lista_office as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;
                $tipoCompra = "";
                
                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
                
                
                if ($compra_datos2->datos3($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion) > 0) {
                    $compra = $compra_datos2->compra;
                    $precio = $compra_datos2->precio;
                    $tipoCompra = $compra_datos2->tipo;
                }
                
                if($precio == 0 || $precio == ""){
                    $precio = $reg_o["precio"];
                }

                if (($newFamilia == 'Office') || ($newFamilia == 'Visio') || ($newFamilia == 'Project') || ($newFamilia == 'Visual Studio')) {
                    $instalaciones = $resumen_o->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                }//resumen offices

                if (($newFamilia == 'Exchange Server') || ($newFamilia == 'Sharepoint Server') || ($newFamilia == 'Skype for Business') /* ||($newFamilia=='Windows Server') */ /*|| ($newFamilia == 'SQL Server')) {
                    $instalaciones = $resumen_s->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                }//resumen offices

                if (($newFamilia == 'System Center')) {
                    $instalaciones = $resumen_ot->listar_datos2($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, $newVersion);
                }//resumen offices
                
                //inicio Windows OS
                if (($newFamilia == 'Windows')) {
                    $instalaciones = $detalle->listar_WindowsOS($_SESSION['client_id'], $_SESSION["client_empleado"], $newEdicion, $newVersion);
                }//fin Windows OS 
                
                //inicio Windows Server
                if (($newFamilia == 'Windows Server')) {
                    $instalaciones = $detalle->listar_WindowsServer($_SESSION['client_id'], $_SESSION["client_empleado"], $newEdicion, $newVersion);
                }//fin Windows Server 

                $balancec1 = $compra - $instalaciones;
                //$balancec2=$balancec1*$reg_o->precio;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                if (strpos($newFamilia, "Windows Server") !== false) {
                    $tipo = 2;
                } else if (strpos($newFamilia, "Windows") !== false) {
                    $tipo = 1;
                } else if ($balancec2 < 0) {
                    $tipo = 2;
                } else {
                    $tipo = 1;
                }
                
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, $newEdicion, 
                $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo, $tipoCompra);
            }//office
        }//office*/
        
        $lista_compras = $balance_datos->listarBalanzaAgregar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        foreach($lista_compras as $reg_o){
            $instalaciones = $reg_o["instalacion"];
            $compra = $reg_o["compra"];
            $tipoCompra = $reg_o["tipo"];
            $newFamilia = $reg_o["familia"];
            $newEdicion = $reg_o["edicion"];
            $newVersion = $reg_o["version"];
            $precio = $balance_datos->obtenerPrecio($_SESSION["client_id"], $_SESSION["client_empleado"], $newFamilia,
            $newEdicion, $newVersion);
            $balancec1=$compra-$instalaciones;
            $balancec2=$balancec1*$precio;
            $asig = $reg_o["asignacion"];

            if(strpos($newFamilia, "Windows Server") !== false){
                $tipo=2;	
            }
            else if(strpos($newFamilia, "Windows") !== false){
                $tipo=1;	
            }
            else if($balancec2<0){
                $tipo=2;	
            }else{
                $tipo=1;	
            }
            if(!$balance_datos->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $newFamilia, 
            $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo, 
            $tipoCompra, $asig)){
                echo $balance_datos->error . "<br>";
            }
        }

        $exito2 = 1;
        
        $log->insertar(2, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Compras Repositorio");
    }
}