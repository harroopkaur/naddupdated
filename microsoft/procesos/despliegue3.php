<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

// Objetos
$detalles     = new DetallesE_f();
$general = new General();
$log = new log();
$fabricante = 3;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$exito4 = 0;
$error3 = 0;

$opcionDespliegue = "";
if(isset($_POST["opcionDespliegue"])){
    $opcionDespliegue = $general->get_escape($_POST["opcionDespliegue"]);
    $_SESSION["archivosCargados"] = "";
    $_SESSION["archCargados"] = array();
    $_SESSION["archivosCargadosLAE"] = "";
    $_SESSION["archCargadosLAE"] = array();
}

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones
        if ($nombre_imagen != "") {
            $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
                $error3 = 1;
            }  // Permitir subir solo imagenes JPG,
        } else {
            $error3 = 2;
        }

        $carpetaBase = "archivos_csvf5/";
        $imagen = $_SESSION['client_id'] . $_SESSION['client_empleado'] . "_asignacion" . date("dmYHis") . ".csv";

        if($general->obtenerSeparadorUniversal($temporal_imagen, 1, 2) === true){
            if (($fichero = fopen($temporal_imagen, "r")) !== FALSE) {
                $i = 1;
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    if($i == 1 && ($datos[0] != "equipo" || $datos[1] != "asignacion")){
                        $error3 = 18;
                        break;
                    }
                    $i++;
                }
            }
        }else{
            $error = 19;
        }
    } else{
        $error3 = -1;
    }

    if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
        for($i = 0; $i < count($_SESSION["archCargadosSubsidiaria"]); $i++){
            if($_SESSION["archCargadosSubsidiaria"][$i] == $nombre_imagen){
                $error3 = 15;
                break;
            }
        }
    }
    
    if ($error3 == 0) {
        move_uploaded_file($temporal_imagen, $carpetaBase . $imagen);
        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
            $_SESSION["archCargadosSubsidiaria"][] = $nombre_imagen;
            arsort($_SESSION["archCargadosSubsidiaria"]);
            
            $_SESSION["archivosCargadosSubsidiaria"] = "";
            foreach ($_SESSION["archCargadosSubsidiaria"] as $key => $val) {
                $_SESSION["archivosCargadosSubsidiaria"] .= $val . "<br>";
            }
        } else{
            $_SESSION["archivosCargadosSubsidiaria"] = "";
        }
        
        if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
            if(!$detalles->limpiarAsignacion($_SESSION['client_id'], $_SESSION["client_empleado"])){
                echo "Asignacion: " . $detalles->error . "<br>";
            }
        }
        
        if($general->obtenerSeparadorUniversal($carpetaBase . $imagen, 1, 2) === true){
            if (($fichero = fopen($carpetaBase . $imagen, "r")) !== FALSE) {
                $i = 1;
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    if ($i > 1) {
                        if(!$detalles->actualizarAsignacion($_SESSION["client_id"], $_SESSION["client_empleado"], $datos[0], $general->truncarString(utf8_encode($datos[1]), 50))){
                            echo "asignacion " . $detalles->error . "<br>";
                        }
                    }
                    $i++;
                    $exito3 = 1;
                }
            }
        }
        
        $log->insertar(1, 2, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Asignación LAE");
    }
}