<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_windows.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_sql.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_vs.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_msdn.php");

$general = new General();
$moduloServidores = new moduloServidores();
$desarrolloPruebaWindows = new desarrolloPruebaWindows();
$desarrolloPruebaSQL = new desarrolloPruebaSQL();
$resumen = new Resumen_Of();
$desarrolloPrueba = new desarrolloPruebaVS();
$desarrolloPruebaMSDN = new desarrolloPruebaMSDN();

$agregarVS = 0;
$exitoAgrVS = 0;
$actualVS = 0;
$exitoActualVS = 0;

$agregarWindows = 0;
$exitoAgrWindows = 0;
$actualWindows = 0;
$exitoActualWindows = 0;

$agregarSQL = 0;
$exitoAgrSQL = 0;
$actualSQL = 0;
$exitoActualSQL = 0;

$agregarMSDN = 0;
$exitoAgrMSDN = 0;
$actualMSDN = 0;
$exitoActualMSDN = 0;

if($desarrolloPruebaWindows->existeDesarrolloPruebaWindows($_SESSION["client_id"], $_SESSION["client_empleado"]) == 0){
    $listadoWindowsServer = $moduloServidores->listaDesarrolloPruebaWindowsServer($_SESSION["client_id"], $_SESSION["client_empleado"]);
    foreach($listadoWindowsServer as $row){
        $desarrolloPruebaWindows->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $row["equipo"], 
        $row["tipo"], $row["familia"], $row["edicion"], $row["version"], "0000-00-00");
    }
}

if($desarrolloPruebaSQL->existeDesarrolloPruebaSQL($_SESSION["client_id"], $_SESSION["client_empleado"]) == 0){
    $listadoSQLServer = $moduloServidores->listaDesarrolloPruebaSQLServer($_SESSION["client_id"], $_SESSION["client_empleado"]);
    foreach($listadoSQLServer as $row){
        $desarrolloPruebaSQL->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $row["equipo"], 
        $row["tipo"], $row["familia"], $row["edicion"], $row["version"], "0000-00-00");
    }
}

if($desarrolloPrueba->existeDesarrolloPruebaVS($_SESSION["client_id"], $_SESSION["client_empleado"]) == 0){    
    $listado = $resumen->listaDesarrolloPruebaVS($_SESSION["client_id"], $_SESSION["client_empleado"]);
    foreach($listado as $row){
        $desarrolloPrueba->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $row["equipo"], 
        $row["tipo"], $row["familia"], $row["edicion"], $row["version"], $row["fecha_instalacion"]);
    }
}

//inicio agregar registros Windows
if(isset($_POST["agrWindows"]) && $_POST["agrWindows"] == 1){
    $agregarWindows = 1;
    $servidorWindows = $_POST["servidorWindows"];
    $tipoWindows = $_POST["tipoWindows"];
    $edicionWindows = $_POST["edicionWindows"];
    $versionWindows = $_POST["versionWindows"];
    $fechaWindows = $_POST["fechaWindows"];

    $j = 0;
    for($i = 0; $i < count($servidorWindows); $i++){
        $servidorWindowsAux = $general->get_escape($servidorWindows[$i]);
        $tipoWindowsAux = $general->get_escape($tipoWindows[$i]);
        $edicionWindowsAux = $general->get_escape($edicionWindows[$i]);
        $versionWindowsAux = $general->get_escape($versionWindows[$i]);
        $fechaWindowsAux = $general->reordenarFecha($fechaWindows[$i], "/");
       
        if($desarrolloPruebaWindows->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $servidorWindowsAux, 
        $tipoWindowsAux, "Windows Server", $edicionWindowsAux, $versionWindowsAux, $fechaWindowsAux)){
            $j++;
        }
    }
    
    if($j == $i){
        $exitoAgrWindows = 1;
    } else if($j < $i) {
        $exitoAgrWindows = 2;
    } 
}
//fin agregar registros Windows

//inicio actualizacion registros Windows
if(isset($_POST["actualWindows"]) && $_POST["actualWindows"] == 1){
    $actualWindows = 1;
    
    $idWindows = array();
    if(isset($_POST["idWindows"]) && is_array($_POST["idWindows"])){
        $idWindows = $_POST["idWindows"];
    }
    
    $usuarioWindows = array();
    if(isset($_POST["usuarioWindows"]) && is_array($_POST["usuarioWindows"])){
        $usuarioWindows = $_POST["usuarioWindows"];
    }
    
    $equipoWindows = array();
    if(isset($_POST["equipoWindows"]) && is_array($_POST["equipoWindows"])){
        $equipoWindows = $_POST["equipoWindows"];
    }
    
    for($i = 0; $i < count($idWindows); $i++){
        $id = 0;
        if(filter_var($idWindows[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idWindows[$i];
        }
        
        $usuario = $general->get_escape($usuarioWindows[$i]);
        $equipo = $general->get_escape($equipoWindows[$i]);
        
        $j = 0;
        $k = 0;
        if($usuario != "" && $equipo != ""){
            if($desarrolloPruebaWindows->actualizar($id, $usuario, $equipo)){
                $k++;
            }
            $j++;
        }
        
        if($j == $k){
            $exitoActualWindows = 1;
        } else if($j > $k){
            $exitoActualWindows = 2;
        }
    }
}
//fin actualizacion registros Windows

//inicio agregar registros SQL
if(isset($_POST["agrSQL"]) && $_POST["agrSQL"] == 1){
    $agregarSQL = 1;
    $servidorSQL = $_POST["servidorSQL"];
    $tipoSQL = $_POST["tipoSQL"];
    $edicionSQL = $_POST["edicionSQL"];
    $versionSQL = $_POST["versionSQL"];
    $fechaSQL = $_POST["fechaSQL"];

    $j = 0;
    for($i = 0; $i < count($servidorSQL); $i++){
        $servidorSQLAux = $general->get_escape($servidorSQL[$i]);
        $tipoSQLAux = $general->get_escape($tipoSQL[$i]);
        $edicionSQLAux = $general->get_escape($edicionSQL[$i]);
        $versionSQLAux = $general->get_escape($versionSQL[$i]);
        $fechaSQLAux = $general->reordenarFecha($fechaSQL[$i], "/");
       
        if($desarrolloPruebaSQL->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $servidorSQLAux, 
        $tipoSQLAux, "SQL Server", $edicionSQLAux, $versionSQLAux, $fechaSQLAux)){
            $j++;
        }
    }
    
    if($j == $i){
        $exitoAgrSQL = 1;
    } else if($j < $i) {
        $exitoAgrSQL = 2;
    } 
}
//fin agregar registros SQL

//inicio actualizacion registros SQL
if(isset($_POST["actualSQL"]) && $_POST["actualSQL"] == 1){
    $actualSQL = 1;
    
    $idSQL = array();
    if(isset($_POST["idSQL"]) && is_array($_POST["idSQL"])){
        $idSQL = $_POST["idSQL"];
    }
    
    $usuarioSQL = array();
    if(isset($_POST["usuarioSQL"]) && is_array($_POST["usuarioSQL"])){
        $usuarioSQL = $_POST["usuarioSQL"];
    }
    
    $equipoSQL = array();
    if(isset($_POST["equipoSQL"]) && is_array($_POST["equipoSQL"])){
        $equipoSQL = $_POST["equipoSQL"];
    }
    
    for($i = 0; $i < count($idSQL); $i++){
        $id = 0;
        if(filter_var($idSQL[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idSQL[$i];
        }
        
        $usuario = $general->get_escape($usuarioSQL[$i]);
        $equipo = $general->get_escape($equipoSQL[$i]);
        
        $j = 0;
        $k = 0;
        if($usuario != "" && $equipo != ""){
            if($desarrolloPruebaSQL->actualizar($id, $usuario, $equipo)){
                $k++;
            }
            $j++;
        }
        
        if($j == $k){
            $exitoActualSQL = 1;
        } else if($j > $k){
            $exitoActualSQL = 2;
        }
    }
}
//fin actualizacion registros SQL

//inicio agregar registros VS
if(isset($_POST["agrVS"]) && $_POST["agrVS"] == 1){
    $agregarVS = 1;
    $servidorVS = $_POST["servidorVS"];
    $tipoVS = $_POST["tipoVS"];
    $edicionVS = $_POST["edicionVS"];
    $versionVS = $_POST["versionVS"];
    $fechaVS = $_POST["fechaVS"];

    $j = 0;
    for($i = 0; $i < count($servidorVS); $i++){
        $servidorVSAux = $general->get_escape($servidorVS[$i]);
        $tipoVSAux = $general->get_escape($tipoVS[$i]);
        $edicionVSAux = $general->get_escape($edicionVS[$i]);
        $versionVSAux = $general->get_escape($versionVS[$i]);
        $fechaVSAux = $general->reordenarFecha($fechaVS[$i], "/");
       
        if($desarrolloPrueba->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $servidorVSAux, 
        $tipoVSAux, "Visual Studio", $edicionVSAux, $versionVSAux, $fechaVSAux)){
            $j++;
        }
    }
    
    if($j == $i){
        $exitoAgrVS = 1;
    } else if($j < $i) {
        $exitoAgrVS = 2;
    } 
}
//fin agregar registros VS

//inicio actualizacion registros VS
if(isset($_POST["actualVS"]) && $_POST["actualVS"] == 1){
    $actualVS = 1;
    
    $idVS = array();
    if(isset($_POST["idVS"]) && is_array($_POST["idVS"])){
        $idVS = $_POST["idVS"];
    }
    
    $usuarioVS = array();
    if(isset($_POST["usuarioVS"]) && is_array($_POST["usuarioVS"])){
        $usuarioVS = $_POST["usuarioVS"];
    }
    
    $equipoVS = array();
    if(isset($_POST["equipoVS"]) && is_array($_POST["equipoVS"])){
        $equipoVS = $_POST["equipoVS"];
    }

    $desarrolloPruebaMSDN->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);
    
    for($i = 0; $i < count($idVS); $i++){
        $id = 0;
        if(filter_var($idVS[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idVS[$i];
        }
        
        $usuario = $general->get_escape($usuarioVS[$i]);
        $equipo = $general->get_escape($equipoVS[$i]);
        
        $j = 0;
        $k = 0;
        if($usuario != "" && $equipo != ""){
            if($desarrolloPrueba->actualizar($id, $usuario, $equipo)){
                $k++;
            }
            $j++;
        }
        
        if($j == $k){
            $exitoActualVS = 1;
        } else if($j > $k){
            $exitoActualVS = 2;
        }
        
        $row = $desarrolloPrueba->desarrolloPruebaEspecifica($id);
        if($row["usuario"] != ""){
            $desarrolloPruebaMSDN->insertar($id, $_SESSION["client_id"], $_SESSION["client_empleado"], $row["equipo"], 
            $row["tipo"], $row["familia"], $row["edicion"], $row["version"], $row["fechaInstalacion"], 
            $row["usuario"], $row["equipoUsuario"]);
        }
    }
}
//fin actualizacion registros VS


//inicio agregar registros MSDN
if(isset($_POST["agrMSDN"]) && $_POST["agrMSDN"] == 1){
    $agregarMSDN = 1;
    $servidorMSDN = $_POST["servidorMSDN"];
    $tipoMSDN = $_POST["tipoMSDN"];
    $edicionMSDN = $_POST["edicionMSDN"];
    $versionMSDN = $_POST["versionMSDN"];
    $fechaMSDN = $_POST["fechaMSDN"];

    $j = 0;
    for($i = 0; $i < count($servidorMSDN); $i++){
        $servidorMSDNAux = $general->get_escape($servidorMSDN[$i]);
        $tipoMSDNAux = $general->get_escape($tipoMSDN[$i]);
        $edicionMSDNAux = $general->get_escape($edicionMSDN[$i]);
        $versionMSDNAux = $general->get_escape($versionMSDN[$i]);
        $fechaMSDNAux = $general->reordenarFecha($fechaMSDN[$i], "/");
       
        if($desarrolloPrueba->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $servidorMSDNAux, 
        $tipoMSDNAux, "Visual Studio", $edicionMSDNAux, $versionMSDNAux, $fechaMSDNAux)){
            $id = $desarrolloPrueba->obtenerUltId();
            if($desarrolloPruebaMSDN->insertar($id, $_SESSION["client_id"], $_SESSION["client_empleado"], $servidorMSDNAux, 
            $tipoMSDNAux, "Visual Studio", $edicionMSDNAux, $versionMSDNAux, $fechaMSDNAux)){
                $j++;
            }
        }
        
    }
    
    if($j == $i){
        $exitoAgrMSDN = 1;
    } else if($j < $i) {
        $exitoAgrMSDN = 2;
    } 
}
//fin agregar registros MSDN

//inicio actualizacion registros MSDN
if(isset($_POST["actualMSDN"]) && $_POST["actualMSDN"] == 1){
    $actualMSDN = 1;
    
    $idMSDN = array();
    if(isset($_POST["idMSDN"]) && is_array($_POST["idMSDN"])){
        $idMSDN = $_POST["idMSDN"];
    }
    
    $usuarioMSDN = array();
    if(isset($_POST["usuarioMSDN"]) && is_array($_POST["usuarioMSDN"])){
        $usuarioMSDN = $_POST["usuarioMSDN"];
    }
    
    $equipoMSDN = array();
    if(isset($_POST["equipoMSDN"]) && is_array($_POST["equipoMSDN"])){
        $equipoMSDN = $_POST["equipoMSDN"];
    }
    
    $msdn = array();
    if(isset($_POST["msdn"]) && is_array($_POST["msdn"])){
        $msdn = $_POST["msdn"];
    }
    
    for($i = 0; $i < count($idMSDN); $i++){
        $id = 0;
        if(filter_var($idMSDN[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idMSDN[$i];
        }
        
        $usuario = $general->get_escape($usuarioMSDN[$i]);
        $equipo = $general->get_escape($equipoMSDN[$i]);
        
        $cantmsdn = 0;
        if(filter_var($msdn[$i], FILTER_VALIDATE_INT) !== false){
            $cantmsdn = $msdn[$i];
        }
        
        $j = 0;
        $k = 0;
        if($usuario != "" && $equipo != ""){
            if($desarrolloPruebaMSDN->actualizar($id, $usuario, $equipo, $cantmsdn)){
                $k++;
            }
            $j++;
        }
        
        if($j == $k){
            $exitoActualMSDN = 1;
        } else if($j > $k){
            $exitoActualMSDN = 2;
        }
    }
}
//fin actualizacion registros MSDN

$listadoWindows = $desarrolloPruebaWindows->listadoDesarrolloPrueba($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listadoSQL = $desarrolloPruebaSQL->listadoDesarrolloPrueba($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listadoVS = $desarrolloPrueba->listadoDesarrolloPrueba($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listadoMSDN = $desarrolloPruebaMSDN->listadoDesarrolloPruebaMSDN($_SESSION["client_id"], $_SESSION["client_empleado"]);