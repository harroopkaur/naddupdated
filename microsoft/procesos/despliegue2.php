<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_filepcs2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_filepcsAux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_conversion.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_tipo_equipo.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_increm_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

// Objetos
$detalles     = new DetallesE_f();
$detalles2    = new DetallesE_f();
$tablac       = new TablaC();
$filec        = new Filepc_f();
$filec2       = new Filepc_f();
$filepcs      = new Filepc_f();
$fileAux      = new FilepcAux();
$general      = new General();
$conversion   = new TablaC();
$conversion2  = new TablaC();
$escaneo      = new Scaneo_f();
$escaneo2     = new Scaneo_f();
$procesadores = new ConsolidadoProcesadores();
$tipoEquipo   = new ConsolidadoTipoEquipo();
$servidores   = new moduloServidores();
$config       = new configuraciones();
$archivosDespliegue = new clase_archivos_fabricantes();
$archivosIncremDespliegue = new clase_archivos_increm_fabricantes();
$log = new log();
$fabricante = 3;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$opcionDespliegue = "";

if(isset($_POST["opcionDespliegue"])){
    $opcionDespliegue = $general->get_escape($_POST["opcionDespliegue"]);
    $_SESSION["archivosCargados"] = "";
    $_SESSION["archCargados"] = array();
    $_SESSION["archivosCargadosSubsidiaria"] = "";
    $_SESSION["archCargadosSubsidiaria"] = array();
}

$incremArchivo = 0;
if(isset($_POST["incremArchivo"]) && filter_var($_POST["incremArchivo"], FILTER_VALIDATE_INT) !== false){
    $incremArchivo = $_POST["incremArchivo"];
}

$listaEdiciones = $config->listar_edicionTotal();
$listaVersiones = $config->listar_versionTotal();

$datosAux = array();
if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    foreach($_FILES['archivo']['tmp_name'] as $key => $tmp_name){
        if(isset($_FILES['archivo']['tmp_name'][$key]) && is_uploaded_file($_FILES['archivo']['tmp_name'][$key])){
            /*$nombre_imagen = $_FILES['archivo']['name'];
            $tipo_imagen = $_FILES['archivo']['type'];
            $tamano_imagen = $_FILES['archivo']['size'];
            $temporal_imagen = $_FILES['archivo']['tmp_name'];*/
            
            $nombre_imagen = $key.$_FILES['archivo']['name'][$key];
            $tipo_imagen = $_FILES['archivo']['type'][$key];
            $tamano_imagen = $_FILES['archivo']['size'][$key];
            $temporal_imagen = $_FILES['archivo']['tmp_name'][$key];
            $baseLAE = "archivos_csvf2/";

            // Validaciones
            if ($nombre_imagen != "") {
                $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
                $long = count($extension) - 1;
                if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
                    $error2 = 1;
                }  // Permitir subir solo imagenes JPG,
            } else {
                $error2 = 2;
            }

            /*if (!$filec->cargar_archivo($imagen, $temporal_imagen)) {
                $error2 = 5;
            } else {*/

                if($general->obtenerSeparadorLAE_Output($temporal_imagen) === true){
                    if (($fichero = fopen($temporal_imagen, "r")) !== false) {
                        $i = 1;
                        $iDN = $iobjectClass = $icn = $iuserAccountControl = $ilastLogon = $ipwdLastSet = $ioperatingSystem
                        = $ioperatingSystemVersion = $ilastLogonTimestamp = -1;
                        $procesarLAE = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "DN"){
                                    $iDN = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "objectClass"){
                                    $iobjectClass = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "cn"){
                                    $icn = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "userAccountControl"){
                                    $iuserAccountControl = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "lastLogon"){
                                    $ilastLogon = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "pwdLastSet"){
                                    $ipwdLastSet = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "operatingSystem"){
                                    $ioperatingSystem = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "operatingSystemVersion"){
                                    $ioperatingSystemVersion = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "lastLogonTimestamp"){
                                    $ilastLogonTimestamp = $k;
                                }
                            }
                            /*if($i == 1 && ($datos[0] != "DN" || $datos[1] != "objectClass" || $datos[2] != "cn" || $datos[3] != "userAccountControl" 
                            || $datos[4] != "lastLogon" || $datos[5] != "pwdLastSet" || $datos[6] != "operatingSystem" || $datos[7] != "operatingSystemVersion"
                            || $datos[8] != "lastLogonTimestamp")){
                                $error2 = 3;
                                break;
                            }*/
                            $i++;

                            break;
                        }
                    }
                    fclose($fichero);

                    if($iDN == -1 && $iobjectClass == -1 && $icn == -1 && $iuserAccountControl == -1 && $ilastLogon == -1 &&
                    $ipwdLastSet == -1 && $ioperatingSystem == -1 && $ioperatingSystemVersion == -1 && $ilastLogonTimestamp == -1){
                        $procesarLAE = false;
                    } else if($iDN == -1 || $iobjectClass == -1 || $icn == -1 || $iuserAccountControl == -1 || $ilastLogon == -1 ||
                    $ipwdLastSet == -1 || $ioperatingSystem == -1 || $ioperatingSystemVersion == -1 || $ilastLogonTimestamp == -1){
                        $error2 = 3;
                    }
                } else{
                    //$error2 = 6;
                    $procesarLAE = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoEscaneo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";
                } else {
                    $archivoEscaneo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv";
                }
                if($general->obtenerSeparador($archivoEscaneo) === true){
                    if (($fichero = fopen($archivoEscaneo, "r")) !== false) {
                        $i=1;
                        $jHostname = $jStatus = $jError = -1;
                        $procesarEscaneo = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Hostname"){
                                        $jHostname = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Status"){
                                        $jStatus = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Error"){
                                        $jError = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }
                            /*if($i == 2 && ($datos[0] != "Hostname" ||  $datos[1] != "Status" || $datos[2] != "Error")){
                                $error = 4;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($jHostname == -1 && $jStatus == -1 && $jError == -1){
                            $procesarEscaneo = false;
                        }
                    }
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoProcesadores = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Procesadores.csv";
                } else {
                    $archivoProcesadores = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Procesadores.csv";
                }
                if($general->obtenerSeparador($archivoProcesadores) === true){    
                    if (($fichero = fopen($archivoProcesadores, "r")) !== false) {
                        $i=1;
                        $kDatoControl = $kHostName = $kTipoCPU = $kCPUs = $kCores = $kProcesadoresLogicos = $kTipoEscaneo = -1;
                        $procesarProcesadores = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $kDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $kHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Tipo de CPU"){
                                        $kTipoCPU = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "CPUs"){
                                        $kCPUs = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Cores"){
                                        $kCores = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Procesadores Lógicos"){
                                        $kProcesadoresLogicos = $k;
                                    }

                                    if(strpos(trim(utf8_encode($datos[$k])), "Tipo de Escaneo") !== false){
                                        $kTipoEscaneo = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Tipo de CPU" || 
                            $datos[3] != "CPUs" || $datos[4] != "Cores" || utf8_encode($datos[5]) != "Procesadores Lógicos" || strpos($datos[6], "Tipo de Escaneo") === false)){
                                $error = 6;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($kDatoControl == -1 && $kHostName == -1 && $kTipoCPU == -1 && $kCPUs == -1 && $kCores == -1 
                        && $kProcesadoresLogicos == -1 && $kTipoEscaneo == -1){
                            $procesarProcesadores = false;
                        }
                    }
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoTipoEquipo = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Tipo de Equipo.csv";
                } else {
                    $archivoTipoEquipo = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Tipo de Equipo.csv";
                }
                if($general->obtenerSeparador($archivoTipoEquipo) === true){
                    if (($fichero = fopen($archivoTipoEquipo, "r")) !== false) {
                        $i=1;
                        $lDatoControl = $lHostName = $lFabricante = $lModelo = -1;
                        $procesarEquipos = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $lDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $lHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Fabricante"){
                                        $lFabricante = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Modelo"){
                                        $lModelo = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Fabricante" || $datos[3] != "Modelo")){
                                $error = 7;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($lDatoControl == -1 && $lHostName == -1 && $lFabricante == -1 && $lModelo == -1){
                            $procesarEquipos = false;
                        }
                    }
                }
            //}
        } else{
            $error2 = -1;
        }

        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
            for($i = 0; $i < count($_SESSION["archCargadosLAE"]); $i++){
                if($_SESSION["archCargadosLAE"][$i] == $nombre_imagen){
                    $error2 = 15;
                    break;
                }
            }
        }

        if ($error2 == 0) {
            if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                $_SESSION["archCargadosLAE"][] = $nombre_imagen;
                arsort($_SESSION["archCargadosLAE"]);

                $_SESSION["archivosCargadosLAE"] = "";
                foreach ($_SESSION["archCargadosLAE"] as $key => $val) {
                    $_SESSION["archivosCargadosLAE"] .= $val . "<br>";
                }
            } else{
                $_SESSION["archivosCargadosLAE"] = "";
            }

            $imagen = $_SESSION['client_id'] . $_SESSION['client_empleado'] . "_LAE_Output" . date("dmYHis") . ".csv";

            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                move_uploaded_file($temporal_imagen, 'archivos_csvf2/' . $imagen); 
                $filec2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);            
            } else {
                $baseLAE .= "incremento/";
                move_uploaded_file($temporal_imagen, 'archivos_csvf2/incremento/' . $imagen); 
                if($incremArchivo == 0){
                    $filec2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
                }
                $fileAux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            }

            if($general->obtenerSeparadorLAE_Output($baseLAE. $imagen) === true && $procesarLAE === true){
                if (($fichero = fopen($baseLAE . $imagen, "r")) !== false) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        $existeEquipo = $filec2->existeEquipo($_SESSION['client_id'], $general->truncarString($datos[$icn], 250));            
                        $sistema = $datos[$ioperatingSystem];

                        if ($conversion->codigo_existe($datos[$ioperatingSystem], 0)) {
                            $conversion2->datos2($datos[$ioperatingSystem]);
                            $sistema = $conversion2->os;
                        }

                        if ($i > 1 && $existeEquipo == 0) {

                            //if (!$filec->insertar($_SESSION['client_id'], addslashes($datos[0]), addslashes($datos[1]), addslashes($datos[2]), addslashes($datos[3]), addslashes($datos[4]), addslashes($datos[5]), addslashes($sistema), addslashes($datos[7]))) {
                            /*if (!$filec->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], $sistema, $datos[7])) {
                                echo $filec->error;
                            }*/
                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                                . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                                . ":os" . $j . ", :lastlogontimes" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                                . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                                . ":os" . $j . ", :lastlogontimes" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dn" . $j] = $general->truncarString($datos[$iDN], 250);
                            $bloqueValores[":objectclass" . $j] = $general->truncarString($datos[$iobjectClass], 250);
                            $bloqueValores[":cn" . $j] = $general->truncarString($datos[$icn], 250);
                            $bloqueValores[":useracountcontrol" . $j] = $general->truncarString($datos[$iuserAccountControl], 250);
                            $bloqueValores[":lastlogon" . $j] = $general->truncarString($datos[$ilastLogon], 250);
                            $bloqueValores[":pwdlastset" . $j] = $general->truncarString($datos[$ipwdLastSet], 250);
                            $bloqueValores[":os" . $j] = $general->truncarString($sistema, 250);
                            $bloqueValores[":lastlogontimes" . $j] = $general->truncarString($datos[$ilastLogonTimestamp], 250);

                            if($j == $general->registrosBloque){
                                if(!$filec->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo "LAE: " . $filec->error;
                                }

                                if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                                    $fileAux->insertarEnBloque($bloque, $bloqueValores);
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        } else if ($i > 1 && $existeEquipo > 0) {
                            if(!$filec->actualizarEquipo($_SESSION['client_id'], $general->truncarString($datos[$iDN], 250), $general->truncarString($datos[$iobjectClass], 250),
                            $general->truncarString($datos[$icn], 250), $general->truncarString($datos[$iuserAccountControl], 250), $general->truncarString($datos[$ilastLogon], 250), 
                            $general->truncarString($datos[$ipwdLastSet], 250), $general->truncarString($sistema, 250), $general->truncarString($datos[$ilastLogonTimestamp], 250))){
                                echo "LAE: " . $filec->error;
                            } 
                        }
                        $i++;
                        $exito2 = 1;
                    }
                    fclose($fichero);
                    
                    

                    if($insertarBloque === true){
                        if(!$filec->insertarEnBloque($bloque, $bloqueValores)){
                            echo "LAE: " . $filec->error;
                        }

                        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                            $fileAux->insertarEnBloque($bloque, $bloqueValores);
                        }
                    }
                    
                    $filec->eliminarEquiposDespliegue($_SESSION["client_id"]);
                    $fileAux->eliminarEquiposDespliegue($_SESSION["client_id"]);

                    if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                        if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 3) === false){
                            $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 3);
                        }

                        $archivosDespliegue->actualizarDespliegue2($_SESSION["client_id"], $_SESSION["client_empleado"], 3, $imagen);
                    } else {
                        $fecha = $archivosIncremDespliegue->obtenerUltFecha($_SESSION["client_id"], $_SESSION["client_empleado"], 3);
                        $archivosIncremDespliegue->actualizarDespliegue2($_SESSION["client_id"], $_SESSION["client_empleado"], $fecha, 3, $imagen);
                    }
                }
            }

            if($error2 == 0){
                //inicio eliminar equipos LAE
                $nombre = "";
                if(isset($_POST["eliminarEquipoLAE"])){
                    $nombre = $general->get_escape($_POST["eliminarEquipoLAE"]);
                }

                $ruta = $GLOBALS["app_root"] . "/microsoft/archivos_csvf3/" . $nombre;
                if($nombre != "" && file_exists($ruta)){
                    $k = 0;
                    $equiposElim = "";
                    /*if($general->obtenerSeparadorUniversal($ruta, 1, 1) === true){
                        if (($fichero = fopen($ruta, "r")) !== false) {
                            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                                if($k > 0){
                                    $equiposElim .= " OR ";
                                }
                                $equiposElim .= "cn = '" . $datos[0] . "'";
                                $k++;
                            }
    
                            if($equiposElim != ""){
                                if(!$filepcs->eliminarEquipos($_SESSION['client_id'], $_SESSION["client_empleado"], $equiposElim)){
                                    echo $filepcs->error . "<br>";
                                }
                                if(!$fileAux->eliminarEquipos($_SESSION['client_id'], $_SESSION["client_empleado"], $equiposElim)){
                                    echo $fileAux->error . "<br>";
                                }
                            }
                        }
                    }
                    fclose($fichero);*/
                    
                    if($general->obtenerSeparadorUniversal($ruta, 1, 1) === true){
                        if (($fichero = fopen($ruta, "r")) !== false) {
							$arrayEquipos = array(':cliente'=>$_SESSION["client_id"]);
                            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                                if($k > 0){
                                    $equiposElim .= " OR ";
                                }
                                $equiposElim .= "cn = :equipos" . $k . " ";
								$arrayEquipos[":equipos" . $k] = $datos[0];
                                $k++;
                            }
    
                            if($equiposElim != ""){
                                if(!$filepcs->eliminarEquipos($equiposElim, $arrayEquipos)){
                                    echo $filepcs->error . "<br>";
                                }
                                if(!$fileAux->eliminarEquipos($equiposElim, $arrayEquipos)){
                                    echo $fileAux->error . "<br>";
                                }
                            }
                        }
                    }
                    fclose($fichero);
                }
                //fin eliminar equipos LAE

                //inicio actualizar filepcs
                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado" || $incremArchivo == 0){
                    $detalles2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
                } 

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $lista_todos_files = $filepcs->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                } else {
                    $lista_todos_files = $fileAux->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                }

                if ($lista_todos_files) {
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    $fechaDespliegue = strtotime('now');

                    if($general->validarFecha($_POST["fechaDespliegue"], "/", "dd/mm/aaaa")){
                        $fechaDespliegue = strtotime($general->reordenarFecha($_POST["fechaDespliegue"], "/", "-"));
                    }

                    foreach ($lista_todos_files as $reg_f) {

                        $host = explode('.', $reg_f["cn"]);
                        if(filter_var($reg_f["cn"], FILTER_VALIDATE_IP)!== false){
                            $host[0] = $reg_f["cn"];
                        }

                        $sistema = $reg_f["os"];

                        if ($reg_f["lastlogon"] != "") {

                            $value = round(($reg_f["lastlogon"] - 116444735995904000) / 10000000);
                            $fecha1 = date('d/m/Y', $value);

                            $dias1 = $general->daysDiff($value, $fechaDespliegue);
                        } else {
                            $dias1 = NULL;
                        }

                        if ($reg_f["pwdlastset"] != "") {
                            $value2 = round(($reg_f["pwdlastset"] - 116444735995904000) / 10000000);
                            $fecha2 = date('d/m/Y', $value2);

                            $dias2 = $general->daysDiff($value2, $fechaDespliegue);
                        } else {
                            $dias2 = NULL;
                        }
                        if ($reg_f["lastlogontimes"] != "") {

                            $value3 = round(($reg_f["lastlogontimes"] - 116444735995904000) / 10000000);
                            $fecha3 = date('d/m/Y', $value3);

                            $dias3 = $general->daysDiff($value3, $fechaDespliegue);
                        } else {
                            $dias3 = NULL;
                        }

                        $minimos = $general->minimo($dias1, $dias2, $dias3);

                        $minimor = round(abs($minimos), 0);

                        if ($minimor <= 30) {
                            $minimo = 1;
                        } else if ($minimor <= 60) {
                            $minimo = 2;
                        } else if ($minimor <= 90) {
                            $minimo = 3;
                        } else if ($minimor <= 365) {
                            $minimo = 4;
                        } else {
                            $minimo = 5;
                        }

                        if ($minimo < 4) {
                            $activo = 1;
                        } else {
                            $activo = 0;
                        }

                        $tipos = $general->search_server($reg_f["os"]);

                        $tipo = $tipos[0];
                        $familia = "";
                        if(strpos($sistema, "Windows Server") !== false || (strpos($sistema, "Windows") !== false && strpos($sistema, "Server") !== false)){
                            $familia = "Windows Server";
                        }
                        else if(strpos($sistema, "Windows") !== false){
                            $familia = "Windows"; 
                        }

                        $edicion = "";
                        foreach($listaEdiciones as $rowEdiciones){
                            if(trim($rowEdiciones["nombre"]) != "" && strpos($sistema, trim($rowEdiciones["nombre"])) !== false && strlen(trim($rowEdiciones["nombre"])) > strlen($edicion)){
                                 $edicion = trim($rowEdiciones["nombre"]);    
                            }
                        }

                        if($edicion == "" || ($familia == "Windows Server" && $edicion == "Server") || $edicion == "Windows 2000 Server"){
                            $edicion = "Standard";
                        }
                        
                        if(strpos($sistema, "Embedded")){
                            $edicion = "Embedded";
                        }
                        
                        if(strpos($sistema, "Workstation")){
                            $edicion = "Workstation";
                        }

                        $version = "";
                        foreach($listaVersiones as $rowVersiones){
                            if(trim($rowVersiones["nombre"]) != "" && strpos($sistema, trim($rowVersiones["nombre"])) !== false && strlen(trim($rowVersiones["nombre"])) > strlen($version)){
                                 $version = trim($rowVersiones["nombre"]);    
                            }
                        } 

                        if($familia == ""){
                            $edicion = "";
                            $version = "";
                        }

                        /*if ($detalles->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $host[0], $sistema, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimor, $activo, $tipo, $minimo)) {
                            $exito2 = 1;
                        } else {
                            echo $detalles->error;
                        }*/

                        
                        if($j == 0){
                            $insertarBloque = true;
                            $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                            . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                            . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
                        } else {
                            $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                            . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                            . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
                        } 

                        $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                        $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                        $bloqueValores[":equipo" . $j] = $host[0];
                        $bloqueValores[":os" . $j] = $sistema;
                        $bloqueValores[":familia" . $j] = $familia;
                        $bloqueValores[":edicion" . $j] = $edicion;
                        $bloqueValores[":version" . $j] = $version;
                        $bloqueValores[":dias1" . $j] = $dias1;
                        $bloqueValores[":dias2" . $j] = $dias2;
                        $bloqueValores[":dias3" . $j] = $dias3;
                        $bloqueValores[":minimo" . $j] = $minimor;
                        $bloqueValores[":activo" . $j] = $activo;
                        $bloqueValores[":tipo" . $j] = $tipo;
                        $bloqueValores[":rango" . $j] = $minimo;

                        if($j == $general->registrosBloque){
                            if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo "Detalles: " . $detalles->error;
                            }
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }//foreach

                    if($insertarBloque === true){
                        if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){   
                            echo "Detalles: " . $detalles->error;
                        }
                    }
                }//if
                //fin actualizar filepcs

                //inicio actualizar escaneo
                $lista_equipos_scaneados = $escaneo->listar_todo2($_SESSION['client_id'], $_SESSION["client_empleado"]);
                foreach ($lista_equipos_scaneados as $reg_e) {
                    if(!$detalles->actualizar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_e["equipo"], $reg_e["errors"])){
                       echo $detalles->error;
                    }
                }
                //fin actualizar escaneo            
                $log->insertar(1, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue LAE");
            }
        }
    }
}