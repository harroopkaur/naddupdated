<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

// Clases 
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_officesAux.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_increm_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_tipo_equipo.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_vs.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usuario_equipo_microsoft.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");

// Objetos
$resumenOf     = new Resumen_Of();
$consolidadoOf = new Consolidado_Of();
$consolidadoAux = new ConsolidadoAux();
$tablaMaestra  = new tablaMaestra();
$archivosDespliegue = new clase_archivos_fabricantes();
$archivosIncremDespliegue = new clase_archivos_increm_fabricantes();
$general = new General();
$escaneo      = new Scaneo_f();
$detalles     = new DetallesE_f();
$procesadores = new ConsolidadoProcesadores();
$tipoEquipo   = new ConsolidadoTipoEquipo();
$desarrolloPrueba = new desarrolloPruebaVS();
$usuarioEquipo = new usuarioEquipoMicrosoft();
$escaneo      = new Scaneo_f();
$escaneo2     = new Scaneo_f();
$servidores   = new moduloServidores();
$log = new log();
$passLAD = new clase_pass();
$fabricante = 3;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$exito4 = 0;
$error3 = 0;
$lista = array();

$opcionDespliegue = "";
if(isset($_POST["opcionDespliegue"])){
    $opcionDespliegue = $general->get_escape($_POST["opcionDespliegue"]);
    $_SESSION["archivosCargadosLAE"] = "";
    $_SESSION["archCargadosLAE"] = array();
    $_SESSION["archivosCargadosSubsidiaria"] = "";
    $_SESSION["archCargadosSubsidiaria"] = array();
} else {
    $_SESSION["archivosCargados"] = "";
    $_SESSION["archCargados"] = array();
    $_SESSION["archivosCargadosLAE"] = "";
    $_SESSION["archCargadosLAE"] = array();
    $_SESSION["archivosCargadosSubsidiaria"] = "";
    $_SESSION["archCargadosSubsidiaria"] = array();
}
	
if(isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    foreach($_FILES['archivo']['tmp_name'] as $key => $tmp_name){
        if(isset($_FILES['archivo']['tmp_name'][$key]) && is_uploaded_file($_FILES['archivo']['tmp_name'][$key])){
            /*$nombre_imagen = $_FILES['archivo']['name'];
            $tipo_imagen = $_FILES['archivo']['type'];
            $tamano_imagen = $_FILES['archivo']['size'];
            $temporal_imagen = $_FILES['archivo']['tmp_name'];*/
            
            $nombre_imagen = $key.$_FILES['archivo']['name'][$key];
            $tipo_imagen = $_FILES['archivo']['type'][$key];
            $tamano_imagen = $_FILES['archivo']['size'][$key];
            $temporal_imagen = $_FILES['archivo']['tmp_name'][$key];

            // Validaciones
            if($nombre_imagen!=""){
                $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
                $long = count($extension) - 1;
                if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
                // Permitir subir solo imagenes JPG,
            }else{
                $error=2;	
            }

            if($error == 0){
                if(!file_exists($GLOBALS['app_root'] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'])){
                    mkdir($GLOBALS['app_root'] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'], 0755, true);
                }
    
                if(!file_exists($GLOBALS['app_root'] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/")){
                    mkdir($GLOBALS['app_root'] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/", 0755, true);
                }
            }
            
            if($error == 0){
                //$rar_file = rar_open($_FILES['archivo']['tmp_name'], 'InfoLA2015**') or die("Can't open Rar archive");
                /*$rar_file = rar_open($temporal_imagen, 'InfoLA2015**') or die("Can't open Rar archive");*/
                
                
                
                $listPassLAD = $passLAD->listar_todo();
                $pass = "";
               
                foreach($listPassLAD as $row){  
                    $psw = $passLAD->desencriptar($row["descripcion"]);
                    $rar_file = rar_open($temporal_imagen, $psw) or die("Can't open Rar archive");

                    $entries = rar_list($rar_file);   
                    
                    $direcEntry = $GLOBALS["app_root"] . '/tmp';

                    if(!file_exists($direcEntry)){
                        mkdir($direcEntry, 0755);
                    }
    
                    foreach ($entries as $entry) {
                        if($entry->extract($direcEntry)){
                            $pass = $psw;
                            $files = array_diff(scandir($direcEntry), array('.','..')); 
                            foreach ($files as $file) { 
                                unlink($direcEntry.'/'.$file); 
                            } 
                            rmdir($direcEntry);  
                        } 
                        
                        break;
                    }
                    
                    if($pass != ""){
                        break;
                    }
                }
                
                rar_close($rar_file);
                
                if($pass != ""){
                    $rar_file = rar_open($temporal_imagen, $pass) or die("Can't open Rar archive");
                    $entries = rar_list($rar_file);
        
                    $archivosExtraer = 6;
                    $archivosExtraidos = 0;
                    if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                        foreach ($entries as $entry) {
                            if($entry->getName() == "Consolidado Addremove.csv" || $entry->getName() == "Consolidado Procesadores.csv"
                            || $entry->getName() == "Consolidado Tipo de Equipo.csv" || $entry->getName() == "Consolidado Usuario-Equipo.csv"
                            || $entry->getName() == "Resultados_Escaneo.csv" || $entry->getName() == "Consolidado SQL.csv"){
                                if($entry->extract('archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/")){
                                    $archivosExtraidos++;
                                }
                            }
                        }
                    } else {
                        foreach ($entries as $entry) {
                            if($entry->getName() == "Consolidado Addremove.csv" || $entry->getName() == "Consolidado Procesadores.csv"
                            || $entry->getName() == "Consolidado Tipo de Equipo.csv" || $entry->getName() == "Consolidado Usuario-Equipo.csv"
                            || $entry->getName() == "Resultados_Escaneo.csv" || $entry->getName() == "Consolidado SQL.csv"){
                                if($entry->extract('archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/incremento/")){
                                    $archivosExtraidos++;
                                }
                            }
                        }
                    }
                    rar_close($rar_file);
        
                    if($archivosExtraer > $archivosExtraidos){
                        $error = 8;
                    }
                } else{
                    $error = -2;
                }
            }

            if($error == 0) {  
                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Addremove.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Addremove" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Procesadores.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Procesadores" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Tipo de Equipo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Tipo de Equipo" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Tipo de Equipo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado SQL" . date("dmYHis") . ".csv");

                    $archivoConsolidado = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Addremove.csv";
                } else {
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Addremove.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Addremove" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Procesadores.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Procesadores" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Tipo de Equipo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Tipo de Equipo" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado SQL.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado SQL" . date("dmYHis") . ".csv");

                    $archivoConsolidado = "archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Addremove.csv";
                }

                if($general->obtenerSeparador($archivoConsolidado) === true){
                    if (($fichero = fopen($archivoConsolidado, "r")) !== false) {
                        $i=1;
                        $iDatoControl = $iHostName = $iRegistro = $iEditor = $iVersion = $iDiaInstalacion = $iSoftware = -1;
                        $procesarAddRemove = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $iDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $iHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Registro"){
                                        $iRegistro = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Editor"){
                                        $iEditor = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Version" || trim(utf8_encode($datos[$k])) == "Versión"){
                                        $iVersion = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Día de Instalacion" || trim(utf8_encode($datos[$k])) == "Día de Instalación"){
                                        $iDiaInstalacion = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Software"){
                                        $iSoftware = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Registro" 
                            || $datos[3] != "Editor" || utf8_encode($datos[4]) != "Versión" || utf8_encode($datos[5]) != "Día de Instalación" || $datos[6] != "Software")){
                                $error = 3;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($iDatoControl == -1 && $iHostName == -1 && $iRegistro == -1 && $iEditor == -1 && 
                        $iVersion == -1 && $iDiaInstalacion == -1 && $iSoftware == -1){
                            $procesarAddRemove = false;
                        } else if($iDatoControl == -1 || $iHostName == -1 || $iRegistro == -1 || $iEditor == -1 || 
                        $iVersion == -1 || $iDiaInstalacion == -1 || $iSoftware == -1){
                            $error = 3;
                        }
                    }
                } else{
                    //$error = 10;
                    $procesarAddRemove = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoEscaneo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";
                } else {
                    $archivoEscaneo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv";
                }

                if($general->obtenerSeparador($archivoEscaneo) === true){
                    if (($fichero = fopen($archivoEscaneo, "r")) !== false) {
                        $i=1;
                        $jHostname = $jStatus = $jError = -1;
                        $procesarEscaneo = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Hostname"){
                                        $jHostname = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Status"){
                                        $jStatus = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Error"){
                                        $jError = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }
                            /*if($i == 2 && ($datos[0] != "Hostname" ||  $datos[1] != "Status" || $datos[2] != "Error")){
                                $error = 4;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($jHostname == -1 && $jStatus == -1 && $jError == -1){
                            $procesarEscaneo = false;
                        } else if($jHostname == -1 || $jStatus == -1 || $jError == -1){
                            $error = 4;
                        }
                    }
                } else{
                    //$error = 11;
                    $procesarEscaneo = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoProcesadores = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Procesadores.csv";
                } else { 
                    $archivoProcesadores = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Procesadores.csv";
                }

                if($general->obtenerSeparador($archivoProcesadores) === true){    
                    if (($fichero = fopen($archivoProcesadores, "r")) !== false) {
                        $i=1;
                        $kDatoControl = $kHostName = $kTipoCPU = $kCPUs = $kCores = $kProcesadoresLogicos = $kTipoEscaneo = -1;
                        $procesarProcesadores = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $kDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $kHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Tipo de CPU"){
                                        $kTipoCPU = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "CPUs"){
                                        $kCPUs = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Cores"){
                                        $kCores = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Procesadores Logicos" || trim(utf8_encode($datos[$k])) == "Procesadores Lógicos"){
                                        $kProcesadoresLogicos = $k;
                                    }

                                    if(strpos(trim(utf8_encode($datos[$k])), "Tipo de Escaneo") !== false){
                                        $kTipoEscaneo = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Tipo de CPU" || 
                            $datos[3] != "CPUs" || $datos[4] != "Cores" || utf8_encode($datos[5]) != "Procesadores Lógicos" || strpos($datos[6], "Tipo de Escaneo") === false)){
                                $error = 6;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($kDatoControl == -1 && $kHostName == -1 && $kTipoCPU == -1 && $kCPUs == -1 && $kCores == -1 
                        && $kProcesadoresLogicos == -1 && $kTipoEscaneo == -1){
                            $procesarProcesadores = false;
                        } else if($kDatoControl == -1 || $kHostName == -1 || $kTipoCPU == -1 || $kCPUs == -1 || $kCores == -1 
                        || $kProcesadoresLogicos == -1 || $kTipoEscaneo == -1){
                            $error = 6;
                        }
                    }
                } else{
                    //$error = 12;
                    $procesarProcesadores = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoEquipos = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Tipo de Equipo.csv";
                } else {
                    $archivoEquipos = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Tipo de Equipo.csv";
                }

                if($general->obtenerSeparador($archivoEquipos) === true){
                    if (($fichero = fopen($archivoEquipos, "r")) !== false) {
                        $i=1;
                        $lDatoControl = $lHostName = $lFabricante = $lModelo = -1;
                        $procesarEquipos = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $lDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $lHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Fabricante"){
                                        $lFabricante = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Modelo"){
                                        $lModelo = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Fabricante" || $datos[3] != "Modelo")){
                                $error = 7;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($lDatoControl == -1 && $lHostName == -1 && $lFabricante == -1 && $lModelo == -1){
                            $procesarEquipos = false;
                        } else if($lDatoControl == -1 || $lHostName == -1 || $lFabricante == -1 || $lModelo == -1){
                            $error = 7;
                        }
                    }
                } else{
                    //$error = 13;
                    $procesarEquipos = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoUsuarioEquipos = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado Usuario-Equipo.csv";
                } else {
                    $archivoUsuarioEquipos = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Usuario-Equipo.csv";
                }

                if($general->obtenerSeparador($archivoUsuarioEquipos) === true){
                    if (($fichero = fopen($archivoUsuarioEquipos, "r")) !== false) {
                        $i=1;
                        $mDatoControl = $mHostName = $mDominio = $mUsuario = $mIP = -1;
                        $procesarUsuarioEquipos = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $mDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $mHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Dominio"){
                                        $mDominio = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Usuario"){
                                        $mUsuario = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "IP"){
                                        $mIP = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Dominio" || $datos[3] != "Usuario" || $datos[4] != "IP")){
                                $error = 9;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($mDatoControl == -1 && $mHostName == -1 && $mDominio == -1 && $mUsuario == -1 && $mIP == -1){
                            $procesarUsuarioEquipos = false;
                        } else if($mDatoControl == -1 || $mHostName == -1 || $mDominio == -1 || $mUsuario == -1 || $mIP == -1){
                            $error = 9;
                        }
                    }
                } else{
                    //$error = 14;
                    $procesarUsuarioEquipos = false;
                }

                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    $archivoSQL = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Consolidado SQL.csv";
                } else {
                    $archivoSQL = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado SQL.csv";
                }

                if($general->obtenerSeparadorUniversal($archivoSQL, 2, 4) === true){
                    if (($fichero = fopen($archivoSQL, "r")) !== false) {
                        $i=1;
                        $nDatoControl = $nHostName = $nEdicionSQL = $nVersionSQL = -1;
                        $procesarSQL = true;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                            if($i == 2){
                                for($k = 0; $k < count($datos); $k++){
                                    if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                        $nDatoControl = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "HostName"){
                                        $nHostName = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Edicion de SQL:" || trim(utf8_encode($datos[$k])) == "Edición de SQL:"){
                                        $nEdicionSQL = $k;
                                    }

                                    if(trim(utf8_encode($datos[$k])) == "Version de SQL:" || trim(utf8_encode($datos[$k])) == "Versión de SQL:"){
                                        $nVersionSQL = $k;
                                    }
                                }
                            } else if($i > 2){
                                break;
                            }

                            /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || 
                            trim(utf8_encode($datos[3])) != "Edición de SQL:" || trim(utf8_encode($datos[4])) != "Versión de SQL:")){
                                $error = 16;
                                break;
                            }*/
                            $i++;
                        }
                        fclose($fichero);

                        if($nDatoControl == -1 && $nHostName == -1 && $nEdicionSQL == -1 && $nVersionSQL == -1){
                            $procesarSQL = false;
                        } else if($nDatoControl == -1 || $nHostName == -1 || $nEdicionSQL == -1 || $nVersionSQL == -1){
                            $error = 16;
                        }
                    }
                } else{
                    //$error = 17;
                    $procesarSQL = false;
                }
            }
        }
        else{
            $error = -1;
        }

        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
            for($i = 0; $i < count($_SESSION["archCargados"]); $i++){
                if($_SESSION["archCargados"][$i] == $nombre_imagen){
                    $error = 15;
                    break;
                }
            }
        }

        if($error == 0) {   
            if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                $_SESSION["archCargados"][] = $nombre_imagen;
                arsort($_SESSION["archCargados"]);

                $_SESSION["archivosCargados"] = "";
                foreach ($_SESSION["archCargados"] as $key => $val) {
                    $_SESSION["archivosCargados"] .= $val . "<br>";
                }
            } else{
                $_SESSION["archivosCargados"] = "";
            }

            $nombreArchivo = 'LAD_Output' . date('dmYHis') . '.rar';
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                move_uploaded_file($temporal_imagen, 'archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/" . $nombreArchivo); 
            } else {
                move_uploaded_file($temporal_imagen, 'archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/incremento/" . $nombreArchivo); 
            }

            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante) === false){
                    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante);
                }

                $tipoDespliegue = 0;
                if($opcionDespliegue == "completo"){
                    $tipoDespliegue = 1;
                }
                else if($opcionDespliegue == "segmentado"){
                    $tipoDespliegue = 2; 
                }

                $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante, $nombreArchivo, $tipoDespliegue);

                $consolidadoOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            } else{
                $archivosIncremDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante);

                $tipoDespliegue = 0;
                if($opcionDespliegue == "completo"){
                    $tipoDespliegue = 1;
                }
                else if($opcionDespliegue == "segmentado"){
                    $tipoDespliegue = 2; 
                }

                $fecha = $archivosIncremDespliegue->obtenerUltFecha($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante);
                $archivosIncremDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], $fecha, $fabricante, $nombreArchivo, $tipoDespliegue);
                $consolidadoAux->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            }

            if($general->obtenerSeparador($archivoConsolidado) === true && $procesarAddRemove === true){
                if (($fichero = fopen($archivoConsolidado, "r")) !== false) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {                 
                        if($i > 2 && isset($datos[$iHostName]) && $datos[$iHostName] != "" && $datos[$iHostName] != "HostName"){
                            //if(!$consolidadoOf->insertar($_SESSION['client_id'],$datos[0],$datos[1],addslashes($datos[2]),$datos[3],$datos[4],$datos[5], addslashes($datos[6]))){
                            /*if(!$consolidadoOf->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], $datos[6])){    
                            }*/
                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                                . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                                . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dato_control" . $j] = $general->truncarString($datos[$iDatoControl], 250);
                            $bloqueValores[":host_name" . $j] = $general->truncarString($datos[$iHostName], 250);
                            $bloqueValores[":registro" . $j] = $datos[$iRegistro];
                            $bloqueValores[":editor" . $j] = $general->truncarString($datos[$iEditor], 250);
                            $bloqueValores[":version" . $j] = $general->truncarString($datos[$iVersion], 250);
                            $bloqueValores[":fecha_instalacion" . $j] = $general->truncarString($datos[$iDiaInstalacion], 8);
                            if(filter_var($datos[$iDiaInstalacion], FILTER_VALIDATE_INT) === false){
                                $bloqueValores[":fecha_instalacion" . $j] = 0;
                            } 
                            $bloqueValores[":sofware" . $j] = $general->truncarString(utf8_encode($datos[$iSoftware]), 250);

                            if($j == $general->registrosBloque){
                                if(!$consolidadoOf->insertarEnBloque($bloque, $bloqueValores)){ 
                                    //echo $consolidadoOf->error;
                                }

                                if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                                    $consolidadoAux->insertarEnBloque($bloque, $bloqueValores);
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $exito=1;	

                        $i++; 
                    }

                    if($insertarBloque === true && !empty($bloqueValores)){
                        if(!$consolidadoOf->insertarEnBloque($bloque, $bloqueValores)){    
                        }

                        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
                            $consolidadoAux->insertarEnBloque($bloque, $bloqueValores);
                        }
                    }

                    fclose($fichero);
                }
            }

            //inicio resumen office
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $resumenOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            }

            $tablaMaestra->listadoProductosMaestraActivo($fabricante);
            foreach($tablaMaestra->listaProductos as $rowProductos){
                if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                    if(strpos($rowProductos['descripcion'], 'Windows') !== false || strpos($rowProductos['descripcion'], 'SQL') !== false){
                        continue;
                    }
                    $lista = $consolidadoOf->listarProductosMicrosoft($_SESSION['client_id'], $_SESSION["client_empleado"], 
                    $rowProductos["campo3"], $rowProductos["campo2"]);
                } else {
                    if(strpos($rowProductos['descripcion'], 'Windows') !== false || strpos($rowProductos['descripcion'], 'SQL') !== false){
                        continue;
                    }
                    $lista = $consolidadoAux->listarProductosMicrosoft($_SESSION['client_id'], $_SESSION["client_empleado"],
                    $rowProductos["campo3"], $rowProductos["campo2"]);
                }

                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;

                foreach($lista as $reg_r){
                    $edicion='0';
                    $host=explode('.',$reg_r["host_name"]);
                    if(filter_var($reg_r["host_name"], FILTER_VALIDATE_IP)!== false){
                        $host[0] = $reg_r["host_name"];
                    }

                    $version = $tablaMaestra->buscarVersion($reg_r["sofware"]);

                    $f3 = substr($reg_r["fecha_instalacion"], 6, 2);
                    $f2 = substr($reg_r["fecha_instalacion"], 4, -2);
                    $f1 = substr($reg_r["fecha_instalacion"], 0, 4);

                    if($reg_r["fecha_instalacion"] == 0){
                        $fecha_instalacion='0000-00-00';
                    } else{
                        $fecha_instalacion=$f1.'-'.$f2.'-'.$f3;
                    }


                    $producto = $reg_r["sofware"];

                    //$resumenOf->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $host[0],'Office', $tablaMaestra->filtrarProducto($producto, $version, 1, 2), $version, $fecha_instalacion);
                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                        . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                    } else {
                        $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                        . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                    } 

                    $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                    $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                    $bloqueValores[":equipo" . $j] = $host[0];
                    $bloqueValores[":familia" . $j] = $rowProductos["descripcion"];
                    $bloqueValores[":edicion" . $j] = $tablaMaestra->filtrarProducto($producto, $version, $rowProductos["idForaneo"], 3); //3 es el fabricante Microsoft
                    $bloqueValores[":version" . $j] = $version;
                    $bloqueValores[":fecha_instalacion" . $j] = $fecha_instalacion;

                    if($j == $general->registrosBloque){
                        if(!$resumenOf->insertarEnBloque($bloque, $bloqueValores)){ 
                            //echo $resumenOf->error;
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }

                if($insertarBloque === true && !empty($bloqueValores)){
                    if(!$resumenOf->insertarEnBloque($bloque, $bloqueValores)){  
                        //echo $resumenOf->error;
                    }
                }
            }
            //fin resumen office

            //inicio agregar consolidado SQL
            if($general->obtenerSeparador($archivoSQL) === true && $procesarSQL === true){
                if (($fichero = fopen($archivoSQL, "r")) !== false) {
                    $i=1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i > 2 && $datos[$nDatoControl] != "Dato de Control"){
                            $host=explode('.',$datos[$nHostName]);
                            if(filter_var($datos[$nHostName], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[$nHostName];
                            }

                            $edicion = trim(str_replace("Edition", "", $datos[$nEdicionSQL]));
                            $version = $tablaMaestra->convertirVersionSQL($datos[$nVersionSQL]);
                            if ($version == null){
                                $version = "";
                            } 

                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                                . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                                . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":equipo" . $j] = $host[0];
                            $bloqueValores[":familia" . $j] = "SQL Server";
                            $bloqueValores[":edicion" . $j] = $edicion;
                            $bloqueValores[":version" . $j] = $version;
                            $bloqueValores[":fecha_instalacion" . $j] = "0000-00-00";

                            if($j == $general->registrosBloque){
                                if(!$resumenOf->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo $resumenOf->error;
                                }
                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $i++;
                    }
                    fclose($fichero);

                    if($insertarBloque === true && !empty($bloqueValores)){
                        if(!$resumenOf->insertarEnBloque($bloque, $bloqueValores)){  
                            echo $resumenOf->error;
                        }
                    }
                }
            }
            //fin agregar consolidado SQL

            //inicio escaneo
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $archivo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";
                $escaneo2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);                
            } else {
                $archivo = "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/incremento/Resultados_Escaneo.csv";
            }


            if($general->obtenerSeparador($archivo) === true && $procesarEscaneo === true){   
                if (($fichero = fopen( $archivo, "r")) !== false) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;

                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if ($i > 2 && $datos[$jHostname] != "Hostname" &&  $datos[$jStatus] != "Status" && $datos[$jError] != "Error") {
                            $host = explode('.', $datos[$jHostname]);
                            if(filter_var($datos[$jHostname], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[$jHostname];
                            }





                            if($j == 0){
                            $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":equipo" . $j] = $host[0];
                            $bloqueValores[":status" . $j] = $datos[$jStatus];
                            $bloqueValores[":errors" . $j] = utf8_encode($datos[$jError]);

                            if($j == $general->registrosBloque){
                                if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo "Escaneo: " . $escaneo->error;
                                }
                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;

                        }
                        $i++;
                    }
                    fclose($fichero);

                    if($insertarBloque === true && !empty($bloqueValores)){
                        if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){   
                            echo "Escaneo: " . $escaneo->error;
                        }
                    }
                    $exito = 1;
                    
                    //inicio actualizar escaneo
                    $lista_equipos_scaneados = $escaneo->listar_todo2($_SESSION['client_id'], $_SESSION["client_empleado"]);
                    foreach ($lista_equipos_scaneados as $reg_e) {
                        if(!$detalles->actualizar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_e["equipo"], $reg_e["errors"])){
                           echo $detalles->error;
                        }
                    }
                    //fin actualizar escaneo            
                }
            }
            //fin escaneo

            //inicio cargar consolidado procesadores
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Procesadores.csv";
                $procesadores->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            } else {
                $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Procesadores.csv";
            }

            if (($fichero = fopen("archivos_csvf1/".$imagen, "r")) !== false && $procesarProcesadores === true) {
                $i=1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;

                while (($datos = fgetcsv($fichero, 1000)) !== false) {
                    $datosAux[0] = "";
                    $datosAux[1] = "";
                    $datosAux[2] = "";
                    $datosAux[3] = 0;  
                    $datosAux[4] = 0;
                    $datosAux[5] = 0;
                    $datosAux[6] = "";
                    if($i > 2){       
                        if($datos[$kDatoControl] != 'Dato de Control' && isset($datos[$kTipoEscaneo]) && $datos[$kTipoEscaneo] != ""){
                            $datosAux[0] = $datos[$kDatoControl];

                            $host = explode('.', $datos[$kHostName]);
                            if(filter_var($datos[$kHostName], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[$kHostName];
                            }





                            if(isset($datos[$kTipoCPU])){
                                $datosAux[2] = $datos[$kTipoCPU];
                            }

                            if(isset($datos[$kCPUs]) && filter_var($datos[$kCPUs], FILTER_VALIDATE_INT) !== false){
                                $datosAux[3] = $datos[$kCPUs];
                            }

                            if(isset($datos[$kCores]) && filter_var($datos[$kCores], FILTER_VALIDATE_INT) !== false){
                                $datosAux[4] = $datos[$kCores];
                            }

                            if(isset($datos[$kProcesadoresLogicos]) && filter_var($datos[$kProcesadoresLogicos], FILTER_VALIDATE_INT) !== false){
                                $datosAux[5] = $datos[$kProcesadoresLogicos];
                            }

                            if(isset($datos[$kTipoEscaneo])){
                                $datosAux[6] = $datos[$kTipoEscaneo];
                            }

                            if($datosAux[0] != "" && isset($host[0]) && $host[0] != "" && $datosAux[2] != "" && $datosAux[6] != ""){




                                if($j == 0){
                                $insertarBloque = true;
                                    $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                    . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                    . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                                } else {
                                    $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                    . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                    . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                                } 

                                $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                                $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                                $bloqueValores[":dato_control" . $j] = $datosAux[0];
                                $bloqueValores[":host_name" . $j] = $host[0];
                                $bloqueValores[":tipo_CPU" . $j] = $datosAux[2];
                                $bloqueValores[":cpu" . $j] = $datosAux[3];
                                $bloqueValores[":cores" . $j] = $datosAux[4];
                                $bloqueValores[":procesadores_logicos" . $j] = $datosAux[5];
                                $bloqueValores[":tipo_escaneo" . $j] = $general->truncarString($datosAux[6], 250);

                                if($j == $general->registrosBloque){
                                    if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){ 
                                        echo "Procesadores: " . $procesadores->error;
                                    }
                                    $bloque = "";
                                    $bloqueValores = array();
                                    $j = -1;
                                    $insertarBLoque = false; 
                                }
                                $j++;
                            }
                        }
                    }
                    $i++;
                }
                fclose($fichero);

                if($insertarBloque === true && !empty($bloqueValores)){
                    if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){   
                        echo "Procesadores: " . $procesadores->error;
                    }
                }

                $exito=1;	
            }
            //fin cargar consolidado procesadores

            //inicio cargar consolidado tipo de equipo
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Consolidado Tipo de Equipo.csv";
                $tipoEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            } else {
                $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/incremento/Consolidado Tipo de Equipo.csv";
            }


            if (($fichero = fopen("archivos_csvf1/".$imagen, "r")) !== false && $procesarEquipos === true) {
                $i=1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;

                while (($datos = fgetcsv($fichero, 2000)) !== false) {
                    if($i!=1){
                        $datosAux[0] = "";
                        $datosAux[1] = "";
                        $datosAux[2] = "";
                        $datosAux[3] = "";  
                        if(isset($datos[$lDatoControl])){
                            $datosAux[0] = $datos[$lDatoControl];
                        }
                        if($datosAux[0] != 'Dato de Control'){



                            $host = explode('.', $datos[$lHostName]);
                            if(filter_var($datos[$lHostName], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[$lHostName];
                            }

                            if(isset($datos[$lFabricante])){
                                $datosAux[2] = $datos[$lFabricante];
                            }
                            if(isset($datos[$lModelo])){
                                $datosAux[3] = $datos[$lModelo];
                            }





                            if($j == 0){
                            $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dato_control" . $j] = $datosAux[0];
                            $bloqueValores[":host_name" . $j] = $host[0];
                            $bloqueValores[":fabricante" . $j] = $datosAux[2];
                            $bloqueValores[":modelo" . $j] = $datosAux[3];

                            if($j == $general->registrosBloque){
                                if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo "Tipo Equipo: " . $tipoEquipo->error;
                                }
                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                    }
                    $i++;
                }
                fclose($fichero);

                if($insertarBloque === true && !empty($bloqueValores)){
                    if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){   
                        echo "Tipo Equipo: " . $tipoEquipo->error;
                    }
                }

                $exito=1;	
            }
            //fin cargar consolidado tipo de equipo

            //inicio usuario-equipo office
            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $usuarioEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
            }

            if($general->obtenerSeparador($archivoUsuarioEquipos) === true && $procesarUsuarioEquipos === true){
                if (($fichero = fopen($archivoUsuarioEquipos, "r")) !== false) {
                    $i = 1;
                    $j = 0;
                    $bloque = "";
                    $bloqueValores = array();
                    $insertarBloque = false;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {                   
                        if($i > 2 && $datos[$mDatoControl] != "Dato de Control"){
                            if($j == 0){
                                $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", :usuario" . $j . ", :ip" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", :usuario" . $j . ", :ip" . $j . ")";
                            } 

                            $host=explode('.',$datos[$mHostName]);
                            if(filter_var($datos[$mHostName], FILTER_VALIDATE_IP)!== false){
                                $host[0] = $datos[$mHostName];
                            }

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dato_control" . $j] = $datos[$mDatoControl];
                            $bloqueValores[":host_name" . $j] = $host[0];
                            $bloqueValores[":dominio" . $j] = $datos[$mDominio];
                            $bloqueValores[":usuario" . $j] = $datos[$mUsuario];
                            if(isset($datos[$mIP])){
                                $bloqueValores[":ip" . $j] = $datos[$mIP];
                            } else{
                                $bloqueValores[":ip" . $j] = null;
                            }


                            if($j == $general->registrosBloque){
                                if(!$usuarioEquipo->insertarEnBloque($bloque, $bloqueValores)){ 
                                    //echo $usuarioEquipo->error;
                                }

                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                        $exito=1;	

                        $i++; 
                    }
                    fclose($fichero);

                    if($insertarBloque === true && !empty($bloqueValores)){
                        if(!$usuarioEquipo->insertarEnBloque($bloque, $bloqueValores)){    
                            //echo $usuarioEquipo->error;
                        }
                    }
                }
            }
            //fin usuario-equipo office

            $exito=1;  
            //}//continue

            if($opcionDespliegue == "completo" || $opcionDespliegue == "segmentado"){
                $servidores->eliminarAlineaionWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $servidores->eliminarAlineaionSqlServer($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $servidores->eliminarWindowServer1($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $servidores->eliminarSqlServer1($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $desarrolloPrueba->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $log->insertar(1, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue LAD");
            } else{
                $log->insertar(1, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue LAD Incremental");
            }
        }
    }
}