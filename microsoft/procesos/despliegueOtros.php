<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_vs.php");

$general = new General();
$tablaMaestra  = new tablaMaestra();
$archivosDespliegue = new clase_archivos_fabricantes();
$servidores   = new moduloServidores();
$desarrolloPrueba = new desarrolloPruebaVS();

$insertar = 0;
$exitoConsolidado = 0;
if(isset($_POST["insertarConsolidado"]) && filter_var($_POST["insertarConsolidado"], FILTER_VALIDATE_INT) !== false){
    $insertar = 1;
    $exitoConsolidado = 1;
}

