<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php"); 
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_desarrollo_prueba_vs.php");

$general = new General();
$tablaMaestra  = new tablaMaestra();
$archivosDespliegue = new clase_archivos_fabricantes();
$servidores   = new moduloServidores();
$desarrolloPrueba = new desarrolloPruebaVS();

if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION["client_empleado"], 3) === false){
    $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], 3);
}

$exitoConsolidado = 0;
$exitoUsuarioEquipo = 0;
$exitoProcesadores = 0;
$exitoTipoEquipo = 0;
$exitoEquipo = 0;
$exitoEscaneo = 0;

/*inicio consolidado*/
if(isset($_POST['insertarConsolidado']) && $_POST["insertarConsolidado"] == 1) {
    require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_offices.php");
    require_once($GLOBALS["app_root"] . "/clases/clase_resumen_offices.php");
    
    $consolidadoOf = new Consolidado_Of();
    $resumenOf     = new Resumen_Of();
   
    $iDatoControl = 0;
    $iHostName = 0;
    $iRegistro = 0;
    $iEditor = 0;
    $iVersion = 0;
    $iDiaInstalacion = 0;
    $iSoftware = 0;
    $exitoConsolidado = 1;

    
    $archivoConsolidado = "";
    if(isset($_POST["nombreArchivoConsolidado"])){
        $archivoConsolidado = $general->get_escape($_POST["nombreArchivoConsolidado"]);
    }

    $datoControl = "";
    if(isset($_POST["datoControl"])){
        $datoControl = $general->get_escape($_POST["datoControl"]);
    }

    $hostName = "";
    if(isset($_POST["hostName"])){
        $hostName = $general->get_escape($_POST["hostName"]);
    }

    $registro = "";
    if(isset($_POST["registro"])){
        $registro = $general->get_escape($_POST["registro"]);
    }

    $editor = "";
    if(isset($_POST["editor"])){
        $editor = $general->get_escape($_POST["editor"]);
    }

    $version = "";
    if(isset($_POST["version"])){
        $version = $general->get_escape($_POST["version"]);
    }

    $diaInstalacion = "";
    if(isset($_POST["diaInstalacion"])){
        $diaInstalacion = $general->get_escape($_POST["diaInstalacion"]);
    }

    $software = "";
    if(isset($_POST["software"])){
        $software = $general->get_escape($_POST["software"]);
    }
    
    //$archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION["client_empleado"], 3, $nombreArchivo, $tipoDespliegue);

    $consolidadoOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
    
    $archivoConsolidado = $GLOBALS["app_root"] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/despliegueOtros/" . $archivoConsolidado;
    if($general->obtenerSeparador($archivoConsolidado) === true){
        if (($fichero = fopen($archivoConsolidado, "r")) !== FALSE) {
            $i = 0;
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                if($i == 0){
                    for($k = 0; $k < count($datos); $k++){
                        if(utf8_encode($datos[$k]) == $datoControl){
                            $iDatoControl =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $hostName){
                            $iHostName =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $registro){
                            $iRegistro =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $editor){
                            $iEditor =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $version){
                            $iVersion =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $diaInstalacion){
                            $iDiaInstalacion =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $software){
                            $iSoftware =  $k;
                        }
                    }
                } else if($i > 0 && isset($datos[$iDatoControl]) && $datos[$iDatoControl] != "" && trim($datos[$iDatoControl]) != "HostName"){
                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                        . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                    } else {
                        $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                        . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                    } 

                    $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                    $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                    $bloqueValores[":dato_control" . $j] = $datos[$iDatoControl];
                    $bloqueValores[":host_name" . $j] = $datos[$iHostName];
                    $bloqueValores[":registro" . $j] = $datos[$iRegistro];
                    $bloqueValores[":editor" . $j] = $datos[$iEditor];
                    $bloqueValores[":version" . $j] = $datos[$iVersion];
                    $bloqueValores[":fecha_instalacion" . $j] = $datos[$iDiaInstalacion];
                    if(filter_var($datos[$iDiaInstalacion], FILTER_VALIDATE_INT) === false){
                        $bloqueValores[":fecha_instalacion" . $j] = 0;
                    } 
                    $bloqueValores[":sofware" . $j] = $datos[$iSoftware];

                    if($j == $general->registrosBloque){
                        if(!$consolidadoOf->insertarEnBloque($bloque, $bloqueValores)){ 
                            echo $consolidadoOf->error;
                            $exitoConsolidado = 2;
                        }

                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }
                $exito=1;	

                $i++; 
            }

            if($insertarBloque === true){
                if(!$consolidadoOf->insertarEnBloque($bloque, $bloqueValores)){    
                    echo $consolidadoOf->error;
                    $exitoConsolidado = 2;
                }
            }
        }
    }
    
    //inicio resumen office
    $resumenOf->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

    $tablaMaestra->listadoProductosMaestra(3);
    foreach($tablaMaestra->listaProductos as $rowProductos){
        $lista = $consolidadoOf->listarProductosMicrosoft($_SESSION['client_id'], $_SESSION["client_empleado"], 
        $rowProductos["campo3"], $rowProductos["campo2"]);

        $j = 0;
        $bloque = "";
        $bloqueValores = array();
        $insertarBloque = false;

        foreach($lista as $reg_r){
            $edicion='0';
            $host=explode('.',$reg_r["host_name"]);
            if(filter_var($reg_r["host_name"], FILTER_VALIDATE_IP)!== false){
                $host[0] = $reg_r["host_name"];
            }

            $version = $consolidadoOf->buscarVersion($reg_r["sofware"]);

            $f3 = substr($reg_r["fecha_instalacion"], 6, 2);
            $f2 = substr($reg_r["fecha_instalacion"], 4, -2);
            $f1 = substr($reg_r["fecha_instalacion"], 0, 4);

            if($reg_r["fecha_instalacion"] == 0){
                $fecha_instalacion='0000-00-00';
            } else{
                $fecha_instalacion=$f1.'-'.$f2.'-'.$f3;
            }


            $producto = $reg_r["sofware"];

            //$resumenOf->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $host[0],'Office', $tablaMaestra->filtrarProducto($producto, $version, 1, 2), $version, $fecha_instalacion);
            if($j == 0){
                $insertarBloque = true;
                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
            } else {
                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
            } 

            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
            $bloqueValores[":equipo" . $j] = $host[0];
            $bloqueValores[":familia" . $j] = $rowProductos["descripcion"];
            $bloqueValores[":edicion" . $j] = $tablaMaestra->filtrarProducto($producto, $version, $rowProductos["idForaneo"], 3); //3 es el fabricante Microsoft
            $bloqueValores[":version" . $j] = $version;
            $bloqueValores[":fecha_instalacion" . $j] = $fecha_instalacion;

            if($j == $general->registrosBloque){
                if(!$resumenOf->insertarEnBloque($bloque, $bloqueValores)){ 
                    echo $resumenOf->error;
                    $exitoConsolidado = 3;
                }
                $bloque = "";
                $bloqueValores = array();
                $j = -1;
                $insertarBLoque = false; 
            }
            $j++;
        }

        if($insertarBloque === true){
            if(!$resumenOf->insertarEnBloque($bloque, $bloqueValores)){  
                echo $resumenOf->error;
                $exitoConsolidado = 3;
            }
        }
    }
    //fin resumen office
    
    /*inicio servidores*/
    $servidores->eliminarAlineaionWindowServer($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $servidores->eliminarAlineaionSqlServer($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $servidores->eliminarWindowServer1($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $servidores->eliminarSqlServer1($_SESSION["client_id"], $_SESSION["client_empleado"]);
    //fin eliminar servidores

    /*inicio eliminar Desarrollo Prueba VS*/
    $desarrolloPrueba->eliminar($_SESSION["client_id"], $_SESSION["client_empleado"]);
    /*fin eliminar Desarrollo Prueba VS*/
}
/*fin consolidado*/

//inicio cargar consolidado procesadores
if(isset($_POST['insertarProcesadores']) && $_POST["insertarProcesadores"] == 1) {
    require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_procesadores.php");
    
    $procesadores = new ConsolidadoProcesadores();
    
    $iDatoControl = 0;
    $iHostName = 0;
    $iTipoCPU = 0;
    $iCPUs = 0;
    $iCores = 0;
    $iProcesadoresLogicos = 0;
    $iTipoEscaneo = 0;
    $exitoProcesadores = 1;
    
    $archivoProcesadores = "";
    if(isset($_POST["nombreArchivoProcesadores"])){
        $archivoProcesadores = $general->get_escape($_POST["nombreArchivoProcesadores"]);
    }

    $datoControl = "";
    if(isset($_POST["datoControlProcesadores"])){
        $datoControl = $general->get_escape($_POST["datoControlProcesadores"]);
    }

    $hostName = "";
    if(isset($_POST["hostNameProcesadores"])){
        $hostName = $general->get_escape($_POST["hostNameProcesadores"]);
    }

    $tipoCPU = "";
    if(isset($_POST["tipoCPU"])){
        $tipoCPU = $general->get_escape($_POST["tipoCPU"]);
    }

    $CPUs = "";
    if(isset($_POST["CPUs"])){
        $CPUs = $general->get_escape($_POST["CPUs"]);
    }

    $cores = "";
    if(isset($_POST["cores"])){
        $cores = $general->get_escape($_POST["cores"]);
    }

    $procesadoresLogicos = "";
    if(isset($_POST["procesadoresLogicos"])){
        $procesadoresLogicos = $general->get_escape($_POST["procesadoresLogicos"]);
    }

    $tipoEscaneo = "";
    if(isset($_POST["tipoEscaneo"])){
        $tipoEscaneo = $general->get_escape($_POST["tipoEscaneo"]);
    }
    
    
    $procesadores->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
          
    $archivoProcesadores = $GLOBALS["app_root"] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/despliegueOtros/" . $archivoProcesadores;
    if($general->obtenerSeparador($archivoProcesadores) === true){   
        if (($fichero = fopen($archivoProcesadores, "r")) !== FALSE) {
            $i=0;
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                $datosAux[0] = "";
                $datosAux[1] = "";
                $datosAux[2] = "";
                $datosAux[3] = 0;  
                $datosAux[4] = 0;
                $datosAux[5] = 0;
                $datosAux[6] = "";
                if($i == 0){
                    for($k = 0; $k < count($datos); $k++){
                        if(utf8_encode($datos[$k]) == $datoControl){
                            $iDatoControl =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $hostName){
                            $iHostName =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $tipoCPU){
                            $iTipoCPU =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $CPUs){
                            $iCPUs =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $cores){
                            $iCores =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $procesadoresLogicos){
                            $iProcesadoresLogicos =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $tipoEscaneo){
                            $iTipoEscaneo =  $k;
                        }
                    }
                } else {
                    if(isset($datos[$iDatoControl]) && $datos[$iDatoControl] != 'Dato de Control' && isset($datos[$iTipoEscaneo]) && $datos[$iTipoEscaneo] != ""){
                        $datosAux[0] = $datos[$iDatoControl];

                        $host = explode('.', $datos[$iHostName]);
                        if(filter_var($datos[$iHostName], FILTER_VALIDATE_IP)!== false){
                            $host[0] = $datos[$iHostName];
                        }

                        /*if(isset($datos[1])){
                            $datosAux[1] = $datos[1];
                        }*/

                        if(isset($datos[$iTipoCPU])){
                            $datosAux[2] = $datos[$iTipoCPU];
                        }
                        
                        if(isset($datos[$iCPUs]) && filter_var($datos[$iCPUs], FILTER_VALIDATE_INT) !== false){
                            $datosAux[3] = $datos[$iCPUs];
                        }

                        if(isset($datos[$iCores]) && filter_var($datos[$iCores], FILTER_VALIDATE_INT) !== false){
                            $datosAux[4] = $datos[$iCores];
                        }

                        $datosAux[$iProcesadoresLogicos] = 0;
                        if(isset($datos[$iProcesadoresLogicos]) && filter_var($datos[$iProcesadoresLogicos], FILTER_VALIDATE_INT) !== false){
                            $datosAux[5] = $datos[$iProcesadoresLogicos];
                        }

                        if(isset($datos[$iTipoEscaneo])){
                            $datosAux[6] = $datos[$iTipoEscaneo];
                        }

                        if($datosAux[0] != "" && isset($host[0]) && $host[0] != "" && $datosAux[2] != "" && $datosAux[3] != "" && $datosAux[4] != "" && $datosAux[5] != "" && $datosAux[6] != ""){
                            /*if(!$procesadores->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datosAux[0],$datosAux[1],$datosAux[2],$datosAux[3],$datosAux[4],$datosAux[5],$datosAux[6])){
                                //echo $procesadores->error;
                            }*/

                            if($j == 0){
                            $insertarBloque = true;
                                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                            } else {
                                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                                . ":host_name" . $j . ", :tipo_CPU" . $j . ", :cpu" . $j . ", :cores" . $j . ", "
                                . ":procesadores_logicos" . $j . ", :tipo_escaneo" . $j . ")";
                            } 

                            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                            $bloqueValores[":dato_control" . $j] = $datosAux[0];
                            $bloqueValores[":host_name" . $j] = $host[0];
                            $bloqueValores[":tipo_CPU" . $j] = $datosAux[2];
                            $bloqueValores[":cpu" . $j] = $datosAux[3];
                            $bloqueValores[":cores" . $j] = $datosAux[4];
                            $bloqueValores[":procesadores_logicos" . $j] = $datosAux[5];
                            $bloqueValores[":tipo_escaneo" . $j] = $datosAux[6];

                            if($j == $general->registrosBloque){
                                if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){ 
                                    echo "Procesadores: " . $procesadores->error;
                                    $exitoProcesadores = 2;
                                }
                                $bloque = "";
                                $bloqueValores = array();
                                $j = -1;
                                $insertarBLoque = false; 
                            }
                            $j++;
                        }
                    }
                }
                $i++;
            }

            if($insertarBloque === true){
                if(!$procesadores->insertarEnBloque($bloque, $bloqueValores)){   
                    echo "Procesadores: " . $procesadores->error;
                    $exitoProcesadores = 2;
                }
            }
        }
    }
}
//fin cargar consolidado procesadores

//inicio cargar consolidado tipo de equipo
if(isset($_POST['insertarTipoEquipo']) && $_POST["insertarTipoEquipo"] == 1) {
    require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_tipo_equipo.php");
    
    $tipoEquipo   = new ConsolidadoTipoEquipo();
    
    $iDatoControl = 0;
    $iHostName = 0;
    $iFabricante = 0;
    $iModelo = 0;
    $exitoTipoEquipo = 1;
    
    $archivoTipoEquipo = "";
    if(isset($_POST["nombreArchivoTipoEquipo"])){
        $archivoTipoEquipo = $general->get_escape($_POST["nombreArchivoTipoEquipo"]);
    }

    $datoControl = "";
    if(isset($_POST["datoControlTipoEquipo"])){
        $datoControl = $general->get_escape($_POST["datoControlTipoEquipo"]);
    }

    $hostName = "";
    if(isset($_POST["hostNameTipoEquipo"])){
        $hostName = $general->get_escape($_POST["hostNameTipoEquipo"]);
    }

    $fabricante = "";
    if(isset($_POST["fabricante"])){
        $fabricante = $general->get_escape($_POST["fabricante"]);
    }

    $modelo = "";
    if(isset($_POST["modelo"])){
        $modelo = $general->get_escape($_POST["modelo"]);
    }
          
    $archivoTipoEquipo = $GLOBALS["app_root"] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/despliegueOtros/" . $archivoTipoEquipo;
    $tipoEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

    if($general->obtenerSeparador($archivoTipoEquipo) === true){
        if (($fichero = fopen($archivoTipoEquipo, "r")) !== FALSE) {
            $i=0;
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            while (($datos = fgetcsv($fichero, 2000)) !== FALSE) {
                if($i == 0){
                    for($k = 0; $k < count($datos); $k++){
                        if(utf8_encode($datos[$k]) == $datoControl){
                            $iDatoControl =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $hostName){
                            $iHostName =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $fabricante){
                            $iFabricante =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $modelo){
                            $iModelo =  $k;
                        }
                    }
                } else {
                    $datosAux[0] = "";
                    $datosAux[1] = "";
                    $datosAux[2] = "";
                    $datosAux[3] = "";  
                    if(isset($datos[$iDatoControl])){
                        $datosAux[0] = $datos[$iDatoControl];
                    }
                    if($datosAux[$iDatoControl] != 'Dato de Control'){
                        /*if(isset($datos[1])){
                        $datosAux[1] = $datos[1];
                        }*/
                        $host = explode('.', $datos[$iHostName]);
                        if(filter_var($datos[$iHostName], FILTER_VALIDATE_IP)!== false){
                            $host[0] = $datos[$iHostName];
                        }

                        if(isset($datos[$iFabricante])){
                            $datosAux[2] = $datos[$iFabricante];
                        }
                        if(isset($datos[$iModelo])){
                            $datosAux[3] = $datos[$iModelo];
                        }

                        /*if(!$tipoEquipo->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datosAux[0],$datosAux[1],$datosAux[2],$datosAux[3])){
                            echo $tipoEquipo->error;
                        }*/

                        if($j == 0){
                        $insertarBloque = true;
                            $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                            . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                        } else {
                            $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", "
                            . ":host_name" . $j . ", :fabricante" . $j . ", :modelo" . $j . ")";
                        } 

                        $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                        $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                        $bloqueValores[":dato_control" . $j] = $datosAux[0];
                        $bloqueValores[":host_name" . $j] = $host[0];
                        $bloqueValores[":fabricante" . $j] = $datosAux[2];
                        $bloqueValores[":modelo" . $j] = $datosAux[3];

                        if($j == $general->registrosBloque){
                            if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo "Tipo Equipo: " . $tipoEquipo->error;
                                $exitoTipoEquipo = 2;
                            }
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }
                }
                $i++;
            }

            if($insertarBloque === true){
                if(!$tipoEquipo->insertarEnBloque($bloque, $bloqueValores)){   
                    echo "Tipo Equipo: " . $tipoEquipo->error;
                    $exitoTipoEquipo = 2;
                }
            }	
        }
    }
}
//fin cargar consolidado tipo de equipo

/*inicio UsuarioEquipo*/
if(isset($_POST['insertarUsuarioEquipo']) && $_POST["insertarUsuarioEquipo"] == 1) {
    require_once($GLOBALS["app_root"] . "/clases/clase_usuario_equipo_microsoft.php");
    
    $usuarioEquipo = new usuarioEquipoMicrosoft();

    $iDatoControl = 0;
    $iHostName = 0;
    $iDominio = 0;
    $iUsuario = 0;
    $iIP = 0;
    $exitoUsuarioEquipo = 1;

    
    $archivoUsuarioEquipo = "";
    if(isset($_POST["nombreArchivoUsuarioEquipo"])){
        $archivoUsuarioEquipo = $general->get_escape($_POST["nombreArchivoUsuarioEquipo"]);
    }

    $datoControl = "";
    if(isset($_POST["datoControlUsuarioEquipo"])){
        $datoControl = $general->get_escape($_POST["datoControlUsuarioEquipo"]);
    }

    $hostName = "";
    if(isset($_POST["hostNameUsuarioEquipo"])){
        $hostName = $general->get_escape($_POST["hostNameUsuarioEquipo"]);
    }

    $dominio = "";
    if(isset($_POST["dominio"])){
        $dominio = $general->get_escape($_POST["dominio"]);
    }

    $usuario = "";
    if(isset($_POST["usuario"])){
        $usuario = $general->get_escape($_POST["usuario"]);
    }

    $ip = "";
    if(isset($_POST["ip"])){
        $ip = $general->get_escape($_POST["ip"]);
    }
    
    $usuarioEquipo->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
    
    $archivoUsuarioEquipo = $GLOBALS["app_root"] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/despliegueOtros/" . $archivoUsuarioEquipo;
    if($general->obtenerSeparador($archivoUsuarioEquipo) === true){
        if (($fichero = fopen($archivoUsuarioEquipo, "r")) !== FALSE) {
            $i = 0;
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;
            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {$pruebas = explode(",", $datos[0], -1);                    
                if($i == 0){
                    for($k = 0; $k < count($datos); $k++){
                        if(utf8_encode($datos[$k]) == $datoControl){
                            $iDatoControl =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $hostName){
                            $iHostName =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $dominio){
                            $iDominio =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $usuario){
                            $iUsuario =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $ip){
                            $iIP =  $k;
                        }
                    }
                } else if($i > 0 && $datos[$iDatoControl] != "Dato de Control"){
                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", :usuario" . $j . ", :ip" . $j . ")";
                    } else {
                        $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", :usuario" . $j . ", :ip" . $j . ")";
                    } 

                    $host=explode('.',$datos[$iHostName]);
                    if(filter_var($datos[$iHostName], FILTER_VALIDATE_IP)!== false){
                        $host[0] = $datos[$iHostName];
                    }

                    $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                    $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                    $bloqueValores[":dato_control" . $j] = $datos[$iDatoControl];
                    $bloqueValores[":host_name" . $j] = $host[0];
                    $bloqueValores[":dominio" . $j] = $datos[$iDominio];
                    $bloqueValores[":usuario" . $j] = $datos[$iUsuario];
                    if(isset($datos[4])){
                        $bloqueValores[":ip" . $j] = $datos[$iIP];
                    } else{
                        $bloqueValores[":ip" . $j] = null;
                    }


                    if($j == $general->registrosBloque){
                        if(!$usuarioEquipo->insertarEnBloque($bloque, $bloqueValores)){ 
                            echo $usuarioEquipo->error;
                        }

                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }
                $exito=1;	

                $i++; 
            }

            if($insertarBloque === true){
                if(!$usuarioEquipo->insertarEnBloque($bloque, $bloqueValores)){    
                    echo $usuarioEquipo->error;
                }
            }
        }
    }
}
/*fin UsuarioEquipo*/

/*inicio Equipo*/
if(isset($_POST['insertarEquipo']) && $_POST["insertarEquipo"] == 1) {
    require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
    require_once($GLOBALS["app_root"] . "/clases/clase_filepcs2.php");
    require_once($GLOBALS["app_root"] . "/clases/clase_tabla_conversion.php");
    require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
    
    $detalles     = new DetallesE_f();
    $detalles2    = new DetallesE_f();
    $tablac       = new TablaC();
    $filec        = new Filepc_f();
    $filec2       = new Filepc_f();
    $filepcs      = new Filepc_f();
    $conversion   = new TablaC();
    $conversion2  = new TablaC();
    $config       = new configuraciones();
    
    $listaEdiciones = $config->listar_edicionTotal();
    $listaVersiones = $config->listar_versionTotal();

    $iDN = 0;
    $iObjectClass = 0;
    $iCn = 0;
    $iUserAccountControl = 0;
    $iLastLogon = 0;
    $iPwdLastSet = 0;
    $iOperatingSystem = 0;
    $iOperatingSystemVersion = 0;
    $iLastLogonTimestamp = 0;
    $iDescription = 0;
    $exitoEquipo = 1;

    
    $archivoEquipo = "";
    if(isset($_POST["nombreArchivoEquipo"])){
        $archivoEquipo = $general->get_escape($_POST["nombreArchivoEquipo"]);
    }

    $DN = "";
    if(isset($_POST["DN"])){
        $DN = $general->get_escape($_POST["DN"]);
    }

    $objectClass = "";
    if(isset($_POST["objectClass"])){
        $objectClass = $general->get_escape($_POST["objectClass"]);
    }

    $cn = "";
    if(isset($_POST["cn"])){
        $cn = $general->get_escape($_POST["cn"]);
    }

    $userAccountControl = "";
    if(isset($_POST["userAccountControl"])){
        $userAccountControl = $general->get_escape($_POST["userAccountControl"]);
    }

    $lastLogon = "";
    if(isset($_POST["lastLogon"])){
        $lastLogon = $general->get_escape($_POST["lastLogon"]);
    }
    
    $pwdLastSet = "";
    if(isset($_POST["pwdLastSet"])){
        $pwdLastSet = $general->get_escape($_POST["pwdLastSet"]);
    }

    $operatingSystem = "";
    if(isset($_POST["operatingSystem"])){
        $operatingSystem = $general->get_escape($_POST["operatingSystem"]);
    }

    $operatingSystemVersion = "";
    if(isset($_POST["operatingSystemVersion"])){
        $operatingSystemVersion = $general->get_escape($_POST["operatingSystemVersion"]);
    }

    $lastLogonTimestamp = "";
    if(isset($_POST["lastLogonTimestamp"])){
        $lastLogonTimestamp = $general->get_escape($_POST["lastLogonTimestamp"]);
    }

    $description = "";
    if(isset($_POST["description"])){
        $description = $general->get_escape($_POST["description"]);
    }
    
    $filec2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
    $archivoEquipo = $GLOBALS["app_root"] . "/microsoft/archivos_csvf2/despliegueOtros/" . $archivoEquipo;
       
    if($general->obtenerSeparador($archivoEquipo) === true){
        if (($fichero = fopen($archivoEquipo, "r")) !== FALSE) {
            $i = 0;
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                if($i == 0){
                    for($k = 0; $k < count($datos); $k++){
                        if(utf8_encode($datos[$k]) == $DN){
                            $iDN =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $objectClass){
                            $iObjectClass =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $cn){
                            $iCn =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $userAccountControl){
                            $iUserAccountControl =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $lastLogon){
                            $iLastLogon =  $k;
                        }
                        
                        if(utf8_encode($datos[$k]) == $pwdLastSet){
                            $iPwdLastSet =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $operatingSystem){
                            $iOperatingSystem =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $operatingSystemVersion){
                            $iOperatingSystemVersion =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $lastLogonTimestamp){
                            $iLastLogonTimestamp =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $description){
                            $iDescription =  $k;
                        }
                    }
                } else {
                    $sistema = $datos[$iOperatingSystem];

                    if ($conversion->codigo_existe($datos[$iOperatingSystem], 0)) {
                        $conversion2->datos2($datos[$iOperatingSystem]);
                        $sistema = $conversion2->os;
                    }

                    //if (!$filec->insertar($_SESSION['client_id'], addslashes($datos[0]), addslashes($datos[1]), addslashes($datos[2]), addslashes($datos[3]), addslashes($datos[4]), addslashes($datos[5]), addslashes($sistema), addslashes($datos[7]))) {
                    /*if (!$filec->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], $sistema, $datos[7])) {
                        echo $filec->error;
                    }*/
                    if($j == 0){
                        $insertarBloque = true;
                        $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                        . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                        . ":os" . $j . ", :lastlogontimes" . $j . ")";
                    } else {
                        $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :dn" . $j . ", :objectclass" . $j . ", "
                        . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                        . ":os" . $j . ", :lastlogontimes" . $j . ")";
                    } 

                    $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                    $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                    $bloqueValores[":dn" . $j] = $datos[$iDN];
                    $bloqueValores[":objectclass" . $j] = $datos[$iObjectClass];
                    $bloqueValores[":cn" . $j] = $datos[$iCn];
                    $bloqueValores[":useracountcontrol" . $j] = $datos[$iUserAccountControl];
                    $bloqueValores[":lastlogon" . $j] = $datos[$iLastLogon];
                    $bloqueValores[":pwdlastset" . $j] = $datos[$iPwdLastSet];
                    $bloqueValores[":os" . $j] = $sistema;
                    $bloqueValores[":lastlogontimes" . $j] = $datos[$iLastLogonTimestamp];

                    if($j == $general->registrosBloque){
                        if(!$filec->insertarEnBloque($bloque, $bloqueValores)){ 
                            echo "LAE: " . $filec->error;
                            $exitoEquipo = 2;
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;
                }
                $i++;
            }

            if($insertarBloque === true){
                if(!$filec->insertarEnBloque($bloque, $bloqueValores)){
                    echo "LAE: " . $filec->error;
                    $exitoEquipo = 2;
                }
            }
        }
    }

    
    //inicio actualizar filepcs
    $detalles2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);

    $lista_todos_files = $filepcs->listar_todo($_SESSION['client_id'], $_SESSION["client_empleado"]);

    if ($lista_todos_files) {
        $j = 0;
        $bloque = "";
        $bloqueValores = array();
        $insertarBloque = false;

        foreach ($lista_todos_files as $reg_f) {

            $host = explode('.', $reg_f["cn"]);
            if(filter_var($reg_f["cn"], FILTER_VALIDATE_IP)!== false){
                $host[0] = $reg_f["cn"];
            }

            $sistema = $reg_f["os"];

            if ($reg_f["lastlogon"] != "") {

                $value = round(($reg_f["lastlogon"] - 116444735995904000) / 10000000);

                $fecha1 = date('d/m/Y', $value);

                $dias1 = $general->daysDiff($value, strtotime('now'));
            } else {
                $dias1 = NULL;
            }

            if ($reg_f["pwdlastset"] != "") {

                $value2 = round(($reg_f["pwdlastset"] - 116444735995904000) / 10000000);

                $fecha2 = date('d/m/Y', $value2);

                $dias2 = $general->daysDiff($value2, strtotime('now'));
            } else {
                $dias2 = NULL;
            }
            if ($reg_f["lastlogontimes"] != "") {

                $value3 = round(($reg_f["lastlogontimes"] - 116444735995904000) / 10000000);

                $fecha3 = date('d/m/Y', $value3);

                $dias3 = $general->daysDiff($value3, strtotime('now'));
            } else {
                $dias3 = NULL;
            }

            $minimos = $general->minimo($dias1, $dias2, $dias3);

            $minimor = round(abs($minimos), -1);

            if ($minimor <= 30) {
                $minimo = 1;
            } else if ($minimor <= 60) {
                $minimo = 2;
            } else if ($minimor <= 90) {
                $minimo = 3;
            } else if ($minimor <= 365) {
                $minimo = 4;
            } else {
                $minimo = 5;
            }

            if ($minimo < 4) {
                $activo = 1;
            } else {
                $activo = 0;
            }

            $tipos = $general->search_server($reg_f["os"]);

            $tipo = $tipos[0];
            $familia = "";
            if(strpos($sistema, "Windows Server") !== false || (strpos($sistema, "Windows") !== false && strpos($sistema, "Server") !== false)){
                $familia = "Windows Server";
            }
            else if(strpos($sistema, "Windows") !== false){
                $familia = "Windows"; 
            }

            $edicion = "";
            foreach($listaEdiciones as $rowEdiciones){
                if(trim($rowEdiciones["nombre"]) != "" && strpos($sistema, trim($rowEdiciones["nombre"])) !== false && strlen(trim($rowEdiciones["nombre"])) > strlen($edicion)){
                     $edicion = trim($rowEdiciones["nombre"]);    
                }
            }
            if($edicion == "" || ($familia == "Windows Server" && $edicion == "Server") || $edicion == "Windows 2000 Server"){
                $edicion = "Standard";
            }

            $version = "";
            foreach($listaVersiones as $rowVersiones){
                if(trim($rowVersiones["nombre"]) != "" && strpos($sistema, trim($rowVersiones["nombre"])) !== false && strlen(trim($rowVersiones["nombre"])) > strlen($version)){
                     $version = trim($rowVersiones["nombre"]);    
                }
            } 

            if($familia == ""){
                $edicion = "";
                $version = "";
            }

            /*if ($detalles->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $host[0], $sistema, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimor, $activo, $tipo, $minimo)) {
                $exito2 = 1;
            } else {
                echo $detalles->error;
            }*/

            if($j == 0){
                $insertarBloque = true;
                $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
            } else {
                $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :os" . $j . ", "
                . ":familia" . $j . ", :edicion" . $j . ", :version" . $j . ", :dias1" . $j . ", :dias2" . $j .  ", "
                . ":dias3" . $j . ", :minimo" . $j . ", :activo" . $j . ",:tipo" . $j . ", :rango" . $j . ")";
            } 

            $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
            $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
            $bloqueValores[":equipo" . $j] = $host[0];
            $bloqueValores[":os" . $j] = $sistema;
            $bloqueValores[":familia" . $j] = $familia;
            $bloqueValores[":edicion" . $j] = $edicion;
            $bloqueValores[":version" . $j] = $version;
            $bloqueValores[":dias1" . $j] = $dias1;
            $bloqueValores[":dias2" . $j] = $dias2;
            $bloqueValores[":dias3" . $j] = $dias3;
            $bloqueValores[":minimo" . $j] = $minimor;
            $bloqueValores[":activo" . $j] = $activo;
            $bloqueValores[":tipo" . $j] = $tipo;
            $bloqueValores[":rango" . $j] = $minimo;

            if($j == $general->registrosBloque){
                if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){ 
                    echo "Detalles: " . $detalles->error;
                    $exitoEquipo = 3;
                }
                $bloque = "";
                $bloqueValores = array();
                $j = -1;
                $insertarBLoque = false; 
            }
            $j++;
        }//foreach

        if($insertarBloque === true){
            if(!$detalles->insertarEnBloque($bloque, $bloqueValores)){   
                echo "Detalles: " . $detalles->error;
                $exitoEquipo = 3;
            }
        }
    }//if
    //fin actualizar filepcs
}
/*fin Equipo*/

/*inicio Escaneo*/
if(isset($_POST['insertarEscaneo']) && $_POST["insertarEscaneo"] == 1) {
    require_once($GLOBALS["app_root"] . "/clases/clase_escaneo2.php");
    require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
    
    $detalles     = new DetallesE_f();
    $escaneo      = new Scaneo_f();
    $escaneo2     = new Scaneo_f();
    
    $iHostName = 0;
    $iStatus = 0;
    $iError = 0;
    $exitoEscaneo = 1;
    
    $archivoEscaneo = "";
    if(isset($_POST["nombreArchivoEscaneo"])){
        $archivoEscaneo = $general->get_escape($_POST["nombreArchivoEscaneo"]);
    }

    $hostName = "";
    if(isset($_POST["hostNameEscaneo"])){
        $hostName = $general->get_escape($_POST["hostNameEscaneo"]);
    }

    $status = "";
    if(isset($_POST["status"])){
        $status = $general->get_escape($_POST["status"]);
    }

    $error = "";
    if(isset($_POST["error"])){
        $error = $general->get_escape($_POST["error"]);
    }
    
    $escaneo2->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);                
    $archivoEscaneo = $GLOBALS["app_root"] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/despliegueOtros/" . $archivoEscaneo;
    
    if($general->obtenerSeparador($archivoEscaneo) === true){   
        if (($fichero = fopen( $archivoEscaneo, "r")) !== FALSE) {
            $i = 0;
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                if($i == 0){
                    for($k = 0; $k < count($datos); $k++){
                        if(utf8_encode($datos[$k]) == $hostName){
                            $iHostName =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $status){
                            $iStatus =  $k;
                        }

                        if(utf8_encode($datos[$k]) == $error){
                            $iError =  $k;
                        }                    }
                } else {
                    $host = explode('.', $datos[0]);
                    if(filter_var($datos[$iHostName], FILTER_VALIDATE_IP)!== false){
                        $host[0] = $datos[$iHostName];
                    }

                    /*if (!$escaneo->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $host[0], $datos[1], $datos[2])) {
                       echo $escaneo->error;
                    }*/

                    if($j == 0){
                    $insertarBloque = true;
                        $bloque .= "(:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                    } else {
                        $bloque .= ", (:cliente" . $j . ", :empleado" . $j . ", :equipo" . $j . ", :status" . $j . ", :errors" . $j . ")";
                    } 

                    $bloqueValores[":cliente" . $j] = $_SESSION['client_id'];
                    $bloqueValores[":empleado" . $j] = $_SESSION["client_empleado"];
                    $bloqueValores[":equipo" . $j] = $host[0];
                    $bloqueValores[":status" . $j] = $datos[$iStatus];
                    $bloqueValores[":errors" . $j] = $datos[$iError];

                    if($j == $general->registrosBloque){
                        if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){ 
                            echo "Escaneo: " . $escaneo->error;
                            $exitoEscaneo = 2;
                        }
                        $bloque = "";
                        $bloqueValores = array();
                        $j = -1;
                        $insertarBLoque = false; 
                    }
                    $j++;

                }
                $i++;
            }

            if($insertarBloque === true){
                if(!$escaneo->insertarEnBloque($bloque, $bloqueValores)){   
                    echo "Escaneo: " . $escaneo->error;
                    $exitoEscaneo = 2;
                }
            }
        }
    }

    //inicio actualizar escaneo
    $lista_equipos_scaneados = $escaneo->listar_todo2($_SESSION['client_id'], $_SESSION["client_empleado"]);
    if ($lista_equipos_scaneados) {
        foreach ($lista_equipos_scaneados as $reg_e) {
            if(!$detalles->actualizar($_SESSION['client_id'], $_SESSION["client_empleado"], $reg_e["equipo"], $reg_e["errors"])){
               echo $detalles->error;
               $exitoEscaneo = 3;
            }
        }
    }
    //fin actualizar escaneo
}
/*fin Escaneo*/