<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/oracle/procesos/compras.php");
require_once($GLOBALS['app_root'] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 4;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="divMenuContenido">
            <?php
            $menuOracle = 3;
            include_once($GLOBALS['app_root'] . "/oracle/plantillas/menu.php");
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php 
                include_once($GLOBALS['app_root'] . "/oracle/plantillas/compras.php");
                ?>  
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS['app_root'] . "/plantillas/pie.php");
?>