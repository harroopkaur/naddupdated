<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $optimizacion = new DetallesOracle();
            $opcion = "";
            if(isset($_POST["opcion"])){
                $opcion = $general->get_escape($_POST['opcion']);
            }

            $os = "Windows";
            if($opcion == "servidor"){
                $os = "Windows Server"; 
            }
            
            $asig = "";
            if(isset($_POST["asig"])){
                $asig = $general->get_escape($_POST['asig']);
            }
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $tabla = '<div style="width:98%; max-height:400px; overflow-x:auto; overflow-y:hidden;">
                <table style="width:2400px;" class="tablap" id="tablaOptimizacion">
                <thead>
                    <tr style="background:#333; color:#fff;">
                        <th align="center" valign="middle"><span>&nbsp;</span></th>
                        <th align="center" valign="middle"><span>Equipo</span></th>
                        <th align="center" valign="middle"><span>Oracle Application Server</span></th>
                        <th align="center" valign="middle"><span>Oracle Enterprise Repository</span></th>
                        <th align="center" valign="middle"><span>Plumtree Portal</span></th>
                        <th align="center" valign="middle"><span>Service Oriented Architecture</span></th>
                        <th align="center" valign="middle"><span>WebLogic BEA</span></th>
                        <th align="center" valign="middle"><span>WebLogic Oracle</span></th>
                        <th align="center" valign="middle"><span>Oracle DataBase</span></th>
                        <th align="center" valign="middle"><span>Oracle Enterprise Manager</span></th>
                        <th align="center" valign="middle"><span>Business Process Management</span></th>
                        <th align="center" valign="middle"><span>racle EBS</span></th>
                        <th align="center" valign="middle"><span>Oracle Forms & Reports</span></th>
                        <th align="center" valign="middle"><span>Usabilidad</span></th>
                        <th align="center" valign="middle"><span>Observaci&oacute;n</span></th>
                    </tr>
                </thead>
                <tbody >';

            $i = 1;
            $listado = $optimizacion->listar_optimizacionAsignacion($_SESSION['client_id'], $os, $asig, $asignaciones);
            foreach ($listado as $reg_equipos) {
                $tabla .= '<tr>
                            <td>' . $i . '</td>
                            <td>' . $reg_equipos["equipo"] . '</td>
                            <td>' . $reg_equipos["OracleApplicationServer"] . '</td>
                            <td>' . $reg_equipos["OracleEnterpriseRepository"] . '</td>
                            <td>' . $reg_equipos["PlumtreePortal"] . '</td>
                            <td>' . $reg_equipos["ServiceOrientedArchitecture"] . '</td>
                            <td>' . $reg_equipos["WebLogicBEA"] . '</td>
                            <td>' . $reg_equipos["WebLogicOracle"] . '</td>
                            <td>' . $reg_equipos["OracleDataBase"] . '</td>
                            <td>' . $reg_equipos["OracleEnterpriseManager"] . '</td>
                            <td>' . $reg_equipos["BusinessProcessManagement"] . '</td>
                            <td>' . $reg_equipos["OracleEBS"] . '</td>
                            <td>' . $reg_equipos["OracleFormsReports"] . '</td>
                            <td>' . $reg_equipos["usabilidad"] . '</td>
                            <td>' . $reg_equipos["duplicado"] . '</td>
                        </tr>';
                $i++;
            }

            $tabla .= '</tbody>
                    </table>
                <script>
                    $(document).ready(function(){
                        $("#tablaOptimizacion").tablesorter();
                        $("#tablaOptimizacion").tableHeadFixer();
                    });
                </script>';
            $array = array(0 => array('sesion'=>$sesion, 'mensaje'=>'', 'tabla' => $tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);