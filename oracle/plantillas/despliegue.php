<input type="hidden" id="nombreArchivo">

<div style="width:98%; padding:20px; overflow:hidden;">
    <h1 class="textog negro" style="text-align:center; line-height:35px;" id="principal">Despliegue</h1>

    <br><br>

    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, favor realizar los siguientes desde el Controlador de Dominio.  Para cualquier versi&oacute;n de Sistema Operativo Windows:</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar en una carpeta dentro del Controlador de Dominio el archivo <a class="link1" href="<?= $GLOBALS['domain_root'] ?>/LATool_Oracle.rar">LATool_Oracle.rar</a></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Descomprimir y ejecutar Renombrar.bat</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Hacer click derecho sobre el archivo OracleInstallations_LA.vbs y seleccionar "Abrir con el s&iacute;mbolo del sistema" u "Open with Command Prompt" como indica la imagen</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Se iniciar&aacute; una ventana de Command Prompt donde debe introducir el nombre de dominio y extensi&oacute;n para obtener los resultados</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>El script tomar&aacute; un tiempo variable por cada servidor conectado al dominio para obtener toda la informaci&oacute;n necesaria y crear&aacute; un archivo llamado "LAD_Oracle.rar"</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Favor subir el archivo "LAD_Oracle.rar" generado.</p><br>
    <!--<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>-->
    <br>
    
    <fieldset class="fieldset">
        <legend style=" font-weight:bold; margin-left:15px;">LAD_Oracle</legend>
        
        <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="despliegue1.php" >
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <input name="opcionDespliegue" id="opcionCompleto" type="hidden" value="completo" >
            <div class="contenedor_archivo">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivo" id="archivo" accept=".rar">
                    <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar">
                </div>
                <div class="contenedor_boton_archivo">
                    <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                </div>
            </div>
        </form>

        <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
        <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
        <?php if($exito=='1'){ ?>
            <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }else{ ?>

        <?php }
        if($error== -1 ){ ?>
            <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error== -2){ ?>
            <div class="error_archivo">Contraseña Incorrecta</div>
            <script>
                $.alert.open('warning', 'Alerta', 'Contraseña Incorrecta', {'Aceptar' : 'Aceptar'}, function(){
                    $("body").scrollTop(1000);
                });
            </script>
        <?php }
        if($error=='1'){ ?>
            <div class="error_archivo"><?= $general->getMensajeRar($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error=='2'){ ?>
            <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error == 3){
        ?>
            <div class="error_archivo"><?= $general->getMensajeReporteFinal($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeReporteFinal($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php
        }
        if($error == 4){
        ?>
            <div class="error_archivo"><?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php
        }
        if($error == 5){ ?>
            <div class="error_archivo"><?=$consolidado->error?></div>
            <script>
                $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'});
            </script>
        <?php } 
        if($error == 6){ ?>
            <div class="error_archivo"><?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php } ?>
    </fieldset>
    
    <br />

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p><br />
    
    <fieldset class="fieldset">
        <legend style=" font-weight:bold; margin-left:15px;">LAE_Output</legend>
        
        <form enctype="multipart/form-data" name="form_e2" id="form_e2" method="post" action="despliegue2.php" >
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <input name="opcionDespliegue" id="opcionSegmentado" type="hidden" value="completo" >
            <div class="contenedor_archivo">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_file" disabled id="url_file1" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivo" id="archivo1" accept=".csv">
                    <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar">
                    <input type="hidden" id="fechaDespliegueCompleto" name="fechaDespliegue">
                </div>
                <div class="contenedor_boton_archivo">
                    <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="mostrarFechaDespliegue('completo');" class="botones_m5"/ />
                </div>
            </div>
        </form>

        <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
        <div id="cargando2" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
        <?php if($exito2=='1'){ ?>
            <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
            <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>" . $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }else{ ?>

        <?php }
        if($error2== -1){ ?>
            <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error2=='1'){ ?>
            <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error2=='2'){ ?>
            <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error2==3){ ?>
            <div class="error_archivo"><?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error2 > 5){ ?>
            <div class="error_archivo"><?=$consolidado->error?></div>
            <script>
                $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'});
            </script>
        <?php } ?>
    </fieldset>
    <br />
    
    <div id="subsidiaria-completo">
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">Subsidiaria.csv</strong>".</p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Nota: </span>Favor tomar en cuenta que la Asignacion del formulario debe ser idéntica a la Asignacion del Modulo SAM - Contratos - Detalle de Compras</p><br />
        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">Subsidiaria</legend>

            <form enctype="multipart/form-data" name="form_e3" id="form_e3" method="post" action="despliegue3.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="subsidiariaOpcionCompleto" type="hidden" value="<?= $opcionDespliegue ?>" >
                <div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file4" value="Nombre del Archivo...">
                        <input type="file" class="archivo"  name="archivo" id="archivo4" accept=".csv">                            
                        <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando3');document.getElementById('form_e3').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>


            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargando3" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>
            <?php if($exito3==1){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>" . $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }else{ ?>

            <?php }
            if($error3== -1){ ?>
                <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php }
            else if($error3==1){ ?>
                <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php } else if($error3==2){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
                </script>
            <?php } else if($error3 == 18){
            ?>
                <div class="error_archivo"><?= $general->getMensajeAsignacion($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeAsignacion($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error3 == 19){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorAsignacion($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorAsignacion($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            ?>
        </fieldset>
        <br />
    </div>

    <div style="float:right;"><div class="botones_m2" id="boton1" onClick="location.href='compras.php';">Compras</div></div>
</div>

<!--inicio modal fecha despliegue-->
<div class="modal-personal modal-personal-md" id="modal-fechaDespliegue">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Fecha <p style="color:#06B6FF; display:inline">Despliegue</p></h1>
    </div>
    <div class="modal-personal-body text-center">
        <input type="hidden" id="fechaOpcion">
        <span class="bold">Fecha: </span><input type="text" id="fechaD" name="fechaD">
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirFechaDespliegue">Salir</div>
        <div class="boton1 botones_m2" style="float:right;" id="procesarLAE_Output">Procesar</div>
    </div>
</div>
<!--fin modal fecha despliegue-->

<!--inicio modal lista personalizada-->
<div class="modal-personal" id="modal-listaPersonalizada">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Equipos Personalizada</p></h1>
    </div>

    <div class="modal-personal-body">
        <div style="width:400px; margin:0 auto;">
            <div style="float:left;">
                <span class="bold">Equipos:</span> 
            </div>
            <div style="float:left; margin-left:15px;">
                <textarea id="equipo" name="equipo" style="width:200%; height:300px; border:1px solid;" ></textarea>
            </div>
        </div>
    </div>

    <div class="modal-personal-footer">
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="$('#fondo1').hide(); $('#modal-listaPersonalizada').hide();">Salir</div></div>
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="generarListadoPersonal()">Generar Archivo</div></div>
    </div>
</div>
<!--fin modal lista personalizada-->
    
<script>
    $(document).ready(function(){
        $("#fechaD").datepicker();
        
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
        
        $("#archivo1").change(function(e){
            $("#nombreArchivo").val("#url_file1");
            addArchivo(e);
        });
        
        //inicio fecha despliegue
        $("#salirFechaDespliegue").click(function(){
            $("#fechaOpcion").val("");
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        
        $("#archivo4").change(function(e){
            $("#nombreArchivo").val("#url_file4");
            addArchivo(e);
        });
        
        $("#procesarLAE_Output").click(function(){
            if($("#fechaD").val() === ""){
                $.alert.open('warning', 'Alert', "Debe seleccionar la fecha del despliegue", {"Aceptar" : "Aceptar"});
                return false;
            }
       
            if($("#fechaOpcion").val() === "completo"){
                showhide('cargando2');                
                $("#fechaDespliegueCompleto").val($("#fechaD").val());
                document.getElementById('form_e2').submit();
            } else if($("#fechaOpcion").val() === "segmentado"){
                showhide('cargandoSegmentado2');
                $("#fechaDespliegueSegmentado").val($("#fechaD").val());
                document.getElementById('form_seg2').submit();
            }
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        //fin fecha despliegue
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
    
    function mostrarFechaDespliegue(opcion){
        if(opcion === "completo" && $("#archivo1").val() === ""){
            $.alert.open('warning', 'Alert', "Debe seleccionar el archivo LAE_Output.csv", {"Aceptar" : "Aceptar"});
            return false;
        } else if(opcion === "segmentado" && $("#archivo3").val() === ""){
            $.alert.open('warning', 'Alert', "Debe seleccionar el archivo LAE_Output.csv", {"Aceptar" : "Aceptar"});
            return false;
        }
        
        $("#fechaOpcion").val(opcion);
        $("#fondo1").show();
        $("#modal-fechaDespliegue").show();
    }
    
    function listaPersonalizada(){
        $("#fondo1").show();
        $("#modal-listaPersonalizada").show();
        $("#equipo").focus();
    }
    
    function generarListadoPersonal(){
        $("#fondo1").hide();
        $("#modal-listaPersonalizada").hide();
        $("#fondo").show();
        
        if($("#equipo").val() === ""){
            $.alert.open('warning', 'Alert', "No existen equipos", {"Aceptar" : "Aceptar"}, function() {
                $("#fondo1").show();
                $("#modal-listaPersonalizada").show();
            });
            return false;
        }    
        
        $.post("<?= $GLOBALS["domain_root"] ?>/usabilidad/ajax/generarLista.php", { equipo : $("#equipo").val(), token : localStorage.licensingassuranceToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
            $("#fondo").hide();
            if(data[0].result === 0){
                $.alert.open('warning', 'Alert', "No se pudo crear la carpeta del cliente", {"Aceptar" : "Aceptar"});
            } else if(data[0].result === 1){
                location.href = "<?= $GLOBALS["domain_root"] ?>/" + data[0].archivo;
                $("#equipo").val("");
            } else if(data[0].result === 2){
                $.alert.open('warning', 'Alert', "No existen equipos", {"Aceptar" : "Aceptar"});
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
        });
    }
</script>