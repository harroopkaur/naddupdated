<script type="text/javascript">
    $(document).ready(function ()
    {
        $(".filtra").click(function () {
            var valor = parseInt($("input[name='filt']:checked").val());

            if (valor === 1) {
                window.location.href = "<?= $GLOBALS['domain_root'] ?>/oracle/resumen.php";
            }
            if (valor === 2) {
                window.location.href = "<?= $GLOBALS['domain_root'] ?>/oracle/resumen.php?vert=1";
            }
            if (valor === 3) {
                window.location.href = "<?= $GLOBALS['domain_root'] ?>/oracle/resumen.php?vert=2";
            }
            if (valor === 4) {
                window.location.href = "<?= $GLOBALS['domain_root'] ?>/oracle/resumen.php?vert=3";
            }
        });
    });

    function MM_jumpMenu(targ, selObj, restore) { //v3.0
        eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
        if (restore)
            selObj.selectedIndex = 0;
    }
</script>
    


<div style="width:99%; float:left; margin:0px; padding:0px; min-height:300px; overflow:hidden;">
    <?php
    if ($vert == 0) {
    ?>
        <div id="container1" style="height:400px; width:45%; max-width: 45%; margin:10px; float:left;"></div>
        <div id="container2" style="height:400px; width:45%; max-width: 45%; margin:10px; float:left;"></div>
        <div  style=" width:45%; max-width: 45%; margin:10px; float:left; text-align:center;"><a onClick="showhide('ttabla1');" style="cursor:pointer;" >Ver Detalles</a></div>
        <div  style=" width:45%; max-width: 45%; margin:10px; float:left; text-align:center;"><a onClick="showhide('ttabla1');" style="cursor:pointer;" >Ver Detalles</a></div>
    <?php
    }

    if ($vert == 1 || $vert == 3) {
    ?>
        <div id="container2" style="height:400px; width:45%; max-width: 45%; margin:10px; float:left;"></div>
        <div id="container1" style="height:400px; width:45%; max-width: 45%; margin:10px; float:left;"></div>
        <div  style=" width:45%; max-width: 45%; margin:10px; float:left; text-align:center;"><a <?php if ($vert == 1) { echo 'onclick="showhide(\'ttabla1\')";'; } else { echo 'id="verOptimizacionCliente"'; } ?> style="cursor:pointer;" >Ver Detalles</a></div>
        <div  style=" width:45%; max-width: 45%; margin:10px; float:left; text-align:center;"><a <?php if ($vert == 1) { echo 'onclick="showhide(\'ttabla1\')";'; } else { echo 'id="verOptimizacionServidor"'; } ?> style="cursor:pointer;" >Ver Detalles</a></div>
    <?php
    }

    if ($vert == 2) {
    ?>
        <div style="width:98%; overflow-x:auto; overflow-y:hidden;">
            <div id="contenedorGraficos" style="width:100%; overflow-x: auto; overflow-y:hidden;">
                <div style="width:1024px; float:left; text-align:center;">

                    <div id="container3" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container4" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container5" style="height:250px; width:300px; margin:10px; float:left;"></div>
                </div>
            </div>
        </div>	
    <?php
    }
    ?>
    <br>
    <div style="width:99%; margin:0 auto; padding:0px; padding-bottom: 20px; overflow:hidden; clear:both; margin-top:15px;">
        <span style="color:#9F9F9F;">Seleccione el Reporte</span><br> 
        <div>Asignaci&oacute;n:<strong style="color:#000; font-weight:bold;">
            <?= $asig ?>

            <br>
            <select onchange="MM_jumpMenu('self',this,0)">
                <option value="resumen.php" selected>Seleccione..</option>
                <option value="resumen.php?vert=<?= $vert ?>">Todo</option>
                <?php 
                foreach($asignaciones as $row){
                ?>
                    <option value="resumen.php?vert=<?= $vert ?>&asig=<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        
        <div style="margin:10px; float:left;"><input type="radio" name="filt" id="filt1" value="1" class="filtra"   <?php if ($vert == 0) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Alcance</strong></div>
        <div style="margin:10px; float:left;"><input type="radio" name="filt" id="filt2" value="2" class="filtra"   <?php if ($vert == 1) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Usabilidad</strong></div>
        <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt3" value="3" class="filtra"  <?php if ($vert == 2) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Balanza</strong></div>
        <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt4" value="4" class="filtra"  <?php if ($vert == 3) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Optimizaci&oacute;n</strong></div>

        <div style="float:right;">
            <form id="formExportar" name="formExportar" method="post" action="reportes/excelOracleResumen.php">
                <input type="hidden" id="familiaExcel" name="familiaExcel">
                <input type="hidden" id="edicionExcel" name="edicionExcel">
                <input type="hidden" id="vertExportar" name="vert" value = "<?= $vert ?>">
                <input type="hidden" id="asig" name="asig" value = "<?= $asig ?>">
                <input type="hidden" id="opcion" name="opcion"><!-- Usado para saber que exportar en optimizacion-->
                <input type="submit" id="exportar" name="exportar" style="display:none">
                <div class="botones_m2 boton1" id="export" style="display:none">Exportar Excel</div>
            </form>
        </div>
        
        <div style="float:right;">
            <div class="botones_m2 boton1" id="exportarTodo" style="display:none;">Exportar Todos los Productos</div>
        </div>
        
        <div style="float:right; <?php if (!$moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 1)) { echo 'display:none;'; } ?>">
            <div class="botones_m2 boton1" id="actualizarRepo">Actualizar Repositorio</div>
        </div>   
        <br><br>

        <div style="<?php if (!$balanza){ echo 'display:none;'; } ?>">
            <br>
            <div style="width:25%; margin:0px; padding:0px; overflow:hidden; float:left;">
                <span style="color:#9F9F9F;">Seleccione el Producto</span><br><br> 
                <div>Familia: 
                    <br> 

                    <select id="familia" name="familia">
                        <option value="">Seleccione..</option>
                        <?php 
                        foreach($tablaMaestra->lista as $row){
                        ?>
                            <option value="<?= $row['campo1']?>"><?= $row["campo1"] ?></option>
                        <?php
                        }
                        ?>
                    </select>

                </div><br>
                <div>Edici&oacute;n:
                    <br> 
                    <select id="edicion" name="edicion" style="max-width:150px;">
                        <option value="Todo">Seleccione..</option>
                    </select>				
                </div>
            </div>

            <div style="width:73%; margin:0px; padding:0px; overflow:hidden; float:left;">
                <div id="containerDetalle">

                </div>
            </div>

            <br style="clear:both;"><br>
            <div id="divTabla" style="display:none;">
                <table class="tablap">
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th class="text-center">Producto</th>
                            <th class="text-center">Edici&oacute;n</th>
                            <th class="text-center">Versi&oacute;n</th>
                            <th class="text-center">Asignaciones</th>
                            <th class="text-center">Instalaciones</th>
                            <th class="text-center">Compras</th>
                            <th class="text-center">Disponible</th>
                            <th class="text-center">Precio</th>
                        </tr>
                    </thead>
                    <tbody id="tablaDetalle">

                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <?php
    if ($vert == 0) {
    ?>
        <div id="ttabla1"  style=" display:none; width:99%; margin:10px; float:left;">

            <table class="tablap">
                <tr style="background:#333; color:#fff;">
                    <th align="center" valign="middle">Tipo</th>
                    <th align="center"  valign="middle">Activo AD</th>
                    <th align="center"  valign="middle">Inactivo AD</th>
                    <th align="center" valign="middle">Reconciliaci&oacute;n</th>
                    <th align="center" valign="middle">LA Tool</th>
                    <th align="center" valign="middle">Cobertura</th>
                </tr>
                <tr>
                    <th align="left" valign="middle">Clientes</th>
                    <td align="center" valign="middle"><?= $total_2DVc ?></td>
                    <td align="center" valign="middle"><?= $total_1InacLAc ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $t1 = $total_2DVc - $total_1LAc;
                        echo $t1;
                        ?>
                    </td>
                    <td align="center" valign="middle"><?= $total_1LAc ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $pot11 = 0;
                        if($total_2DVc != 0){
                            $pot11 = ($total_1LAc / $total_2DVc) * 100;
                        }
                        echo round($pot11);
                        ?>
                    </td>
                </tr>
                <tr>
                    <th align="left" valign="middle">Servidores</th>
                    <td align="center" valign="middle"><?= $total_2DVs ?></td>
                    <td align="center" valign="middle"><?= $total_1InacLAs ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $t2 = $total_2DVs - $total_1LAs;
                        echo $t2;
                        ?>
                    </td>
                    <td align="center" valign="middle"><?= $total_1LAs ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $pot12 = 0;
                        if($total_2DVs != 0){
                            $pot12 = ($total_1LAs / $total_2DVs) * 100;
                        }
                        echo round($pot12);
                        ?>
                    </td>
                </tr>



                <tr>
                    <th align="left" valign="middle"><strong style="font-weight:bold;">Total</strong></th>
                    <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $total_2DVc + $total_2DVs ?></strong></td>
                    <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $total_1InacLAc + $total_1InacLAs ?></strong></td>
                    <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $reconciliacion1c + $reconciliacion1s ?></strong></td>
                    <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $total_1LAc + $total_1LAs ?></strong></td>
                    <td align="center" valign="middle"><strong style="font-weight:bold;">

                        <?php
                        $to1 = $total_1LAc + $total_1LAs;
                        $to2 = $total_2DVc + $total_2DVs;
                        $pot13 = 0;
                        if($to2 != 0){
                            $pot13 = ($to1 / $to2) * 100;
                        }
                        echo round($pot13);
                        ?>
                    </strong></td>

                </tr>
            </table>

            <br>
            <div style="max-height:400px; overflow-x:auto; overflow-y:hidden;">
                <table class="tablap" id="tablaAlcance" style="margin-top:-5px;">
                    <thead>
                    <tr style="background:#333; color:#fff;">
                        <th  align="center" valign="middle"><span>&nbsp;</span></th>
                        <th  align="center" valign="middle"><span>Nombre Equipo</span></th>
                        <th  align="center" valign="middle"><span>Tipo</span></th>
                        <th  align="center" valign="middle"><span>Activo AD</span></th>
                        <th  align="center" valign="middle"><span>LA Tool</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach ($listado as $reg_equipos2) {
                    ?>
                        <tr>
                            <td align="center"><?= $i ?></td>
                            <td ><?= $reg_equipos2["equipo"] ?></td>
                            <td align="center">
                                <?php
                                if ($reg_equipos2["tipo"] == 1) {
                                    echo 'Cliente';
                                } else {
                                    echo 'Servidor';
                                }
                                ?>

                            </td>
                            <td align="center">
                                <?php
                                if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                                    echo 'Si';
                                } else {
                                    echo 'No';
                                }
                                ?>


                            </td>
                            <td align="center">
                                <?php
                                if ($reg_equipos2["errors"] == 'Ninguno') {
                                    echo 'Si';
                                } else {
                                    echo 'No';
                                }
                                ?>

                            </td>
                        </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        <?php
        }
        ?>

        </div>

    <?php
    if ($vert == 1) {
    ?>
        <div id="ttabla1"  style=" display:none; width:99%; margin:10px; float:left;">

            <table class="tablap">
                <tr style="background:#333; color:#fff;">
                    <th align="center" valign="middle">Usabilidad</th>
                    <th align="center"  valign="middle">Clientes</th>
                    <th align="center" valign="middle">%</th>
                    <th align="center" valign="middle">Servidores</th>
                    <th align="center" valign="middle">%</th>
                    <!--<th align="center" valign="middle">Activo</th>-->
                </tr>
                <tr>
                    <td align="center" valign="middle">En Uso</td>
                    <td align="center" valign="middle"><?= $total_1 ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $porct1 = 0;
                        if($tclient > 0){
                            $porct1 = ($total_1 / $tclient) * 100;
                        }
                        echo round($porct1);
                        ?>
                    </td>
                    <td align="center" valign="middle"><?= $total_4 ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $porct11 = 0;
                        if($tserver > 0){
                            $porct11 = ($total_4 / $tserver) * 100;
                        }                                                            
                        echo round($porct11);
                        ?>
                    </td>
                    <!--<td align="center" valign="middle">Si</td>-->
                </tr>
                <tr>
                    <td align="center" valign="middle">Probablemente en Uso</td>
                    <td align="center" valign="middle"><?= $total_2 ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $porct2 = 0;
                        if($tclient > 0){
                            $porct2 = ($total_2 / $tclient) * 100;
                        }
                        echo round($porct2);
                        ?>
                    </td>
                    <td align="center" valign="middle"><?= $total_5 ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $porct22 = 0;
                        if($tserver > 0){
                            $porct22 = ($total_5 / $tserver) * 100;
                        }
                        echo round($porct22);
                        ?>
                    </td>
                    <!--<td align="center" valign="middle">Si</td>-->
                </tr>
                <tr>
                    <td align="center" valign="middle">Obsoleto</td>
                    <td align="center" valign="middle"><?= $total_3 ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $porct3 = 0;
                        if($tclient > 0){
                            $porct3 = ($total_3 / $tclient) * 100;
                        }
                        echo round($porct3);
                        ?>
                    </td>
                    <td align="center" valign="middle"><?= $total_6 ?></td>
                    <td align="center" valign="middle">
                        <?php
                        $porct33 = 0;
                        if($tserver > 0){
                            $porct33 = ($total_6 / $tserver) * 100;
                        }
                        echo round($porct33);
                        ?>
                    </td>
                    <!--<td align="center" valign="middle">No</td>-->
                </tr>
                <tr>
                    <td align="center" valign="middle"><strong style="font-weight:bold;">Gran total</strong></td>
                    <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $tclient ?></strong></td>
                    <td align="center" valign="middle"></td>
                    <td align="center" valign="middle"><strong style="font-weight:bold;"><?= $tserver ?></strong></td>
                    <td align="center" valign="middle"></td>
                    <!--<td align="center" valign="middle"></td>-->
                </tr>
            </table>
            
            <br>
            
            <div style="max-height:400px; overflow-x:auto; overflow-y:hidden;">
                <table class="tablap" id="tablaAlcance" style="margin-top:-5px">
                    <thead>
                    <tr style="background:#333; color:#fff;">
                        <th  align="center" valign="middle"><span>&nbsp;</span></th>
                        <th  align="center" valign="middle"><span>Nombre Equipo</span></th>
                        <th  align="center" valign="middle"><span>Tipo</span></th>
                        <th  align="center" valign="middle"><span>Usabilidad</span></th>
                        <th  align="center" valign="middle"><span>Escaneado</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach ($listado as $reg_equipos2) {
                    ?>
                        <tr>
                            <td align="center"><?= $i ?></td>
                            <td ><?= $reg_equipos2["equipo"] ?></td>
                            <td align="center">
                                <?php
                                if ($reg_equipos2["tipo"] == 1) {
                                    echo 'Cliente';
                                } else {
                                    echo 'Servidor';
                                }
                                ?>

                            </td>
                            <td align="center">
                                <?php
                                if ($reg_equipos2["rango"] == 1) {
                                    echo 'En Uso';
                                } 
                                else if ($reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                                    echo 'Probablemente en uso';
                                }else {
                                    echo 'Obsoleto';
                                }
                                ?>


                            </td>
                            <td align="center">
                                <?php
                                if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
                                    echo 'Si';
                                } else {
                                    echo 'No';
                                }
                                ?>

                            </td>
                        </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

    <?php
    }//0
    if ($vert == 3) {
    ?>
        <div id="ttabla1"  style="display:none; width:99%; margin:10px; float:left; overflow:auto; max-height:400px;">

        </div>

    <?php
    }//0
    ?>									
</div>


                               
<script>
    var vertAux = "";
    
    $(document).ready(function () {
        $("#tablaAlcance").tablesorter();
        $("#tablaAlcance").tableHeadFixer();
        
        $("#btnServidores").click(function () {
            $("#server").css("color", "#FFFFFF");
            $("#server").css("background-color", "#06B6FF");
            $("#client").css("color", "#06B6FF");
            $("#client").css("background-color", "#FFFFFF");
            $("#contenedorGraficos").css("width", "100%");
            $("#graficosClientes").hide();
            $("#graficosServidores").show();
        });

        $("#btnClientes").click(function () {
            $("#client").css("color", "#FFFFFF");
            $("#client").css("background-color", "#06B6FF");
            $("#server").css("color", "#06B6FF");
            $("#server").css("background-color", "#FFFFFF");
            $("#contenedorGraficos").css("width", "1000px");
            $("#graficosClientes").show();
            $("#graficosServidores").hide();
        });

        $("#export").click(function () {
            $("#exportar").click();
        });
        
        $("#verOptimizacionServidor").click(function(){
            mostrarTabla(3, "", "servidor");
        });
        
        $("#verOptimizacionCliente").click(function(){
             mostrarTabla(3, "", "cliente");
        });

        $("#familia").change(function () {
            $("#edicion").val("Todo");
            realizarBusqueda();
        });
        
        $("#edicion").change(function () {
            realizarBusqueda();
        });
        
        $("#actualizarRepo").click(function(){
            $("#fondo").show();
            $.post("<?=$GLOBALS['domain_root']; ?>/sam/actRepoDespliegue.php", { tabla : 4, token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#fondo").hide();
                if(data[0].result === 0){
                    $.alert.open('alert', "No se pudo actualizar el repositorio de despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
                else if(data[0].result === 2){
                    $.alert.open('alert', "No se actualizó por completo el repositorio de despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
                else if(data[0].result === 1){
                    $.alert.open('alert', "Repositorio de despliegue actualizado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $("#exportarTodo").click(function(){
            window.open("<?= $GLOBALS["domain_root"] ?>/oracle/reportes/excelBalanzaTodos.php?asig=<?= $asig ?>");
        });
    });
    
    function realizarBusqueda(){
        if ($("#familia").val() === "") {
            $.alert.open('alert', "Debe seleccionar una familia", {'Aceptar' : 'Aceptar'}, function() {
                $("#export").hide();
                return false;
            });
        }

        $("#export").show();
        $("#exportarTodo").show();
        $("#fondo").show();
        $.post("ajax/balanzaDetalle.php", {familia: $("#familia").val(), edicion: $("#edicion").val(), asig : '<?= $asig ?>', token : localStorage.licensingassuranceToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#containerDetalle").empty();
            $("#tablaDetalle").empty();
            $("#edicion").empty();
            $("#edicion").append(data[0].edicion);
            $("#familiaExcel").val($("#familia").val());
            $("#edicionExcel").val($("#edicion").val());
            //$("#edicion").val("");

            serie = [{
                        name: 'Obsoleto',
                        data: [0, data[0].sinUso, 0],
                        color: '<?= $color4 ?>'
                    }, {
                        name: 'Neto',
                        data: [0, 0, data[0].neto],
                        color: '<?= $color3 ?>'
                    }, {
                        name: 'En Uso',
                        data: [0, data[0].uso, 0],
                        color: '<?= $color2 ?>'
                    }, {
                        name: 'Compras',
                        data: [parseInt(data[0].compra), 0, 0],
                        color: '<?= $color1 ?>'
                    }];

            $(function () {
                $('#containerDetalle').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                         text: data[0].titulo
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: ['', '', ''],
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                         stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    legend: {
                       reversed: true
                    },
                    plotOptions: {
                        dataLabels: {
                            enabled: true
                        },
                        series: {
                           stacking: 'normal'
                        }
                    },
                    tooltip: {
                        headerFormat: ''
                    },
                    series: serie
                });
            });

            $("#tablaDetalle").append(data[0].tabla);
            $("#divTabla").show();
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
    function mostrarTabla(vert, ordenar, opcion){
        $("#fondo").show();
        vertAux = vert;
        $("#opcion").val(opcion);
        $.post("ajax/optimizacionDetalle.php", { opcion : opcion, asig : '<?= $asig ?>', token : localStorage.licensingassuranceToken }, function(data){
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $('#ttabla1').empty();
            $('#ttabla1').append(data[0].tabla);
            $('#ttabla1').show();
            $("#export").show();
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
    }
</script>