<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $detalles = new DetallesOracle($conn);

    $total_1  = 0;
    $total_2  = 0;
    $total_3  = 0;
    $total_4  = 0;
    $total_5  = 0;
    $total_6  = 0;
    $tclient  = 0;
    $tserver  = 0;
    $activosc = 0;
    $activoss = 0;

    $vert = 0;
    if(isset($_GET['vert']) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false){
        $vert = $_GET['vert'];
    }

    $vert1 = 0;
    if(isset($_GET["vert1"]) && filter_var($_GET['vert1'], FILTER_VALIDATE_INT) !== false){
        $vert1 = $_GET["vert1"];
    }
    
    $fecha = $detalles->fechaSAMArchivo($vert1);

    if ($vert == 0) {
        if ($detalles->listar_todoSAMArchivo($vert1)) {
            foreach ($detalles->lista as $reg_equipos) {

                if ($reg_equipos["tipo"] == 1) {
                    $tclient = ($tclient + 1);

                    if ($reg_equipos["rango"] == 1) {
                        $total_1 = ($total_1 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activosc = ($activosc + 1);
                        }
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_2 = ($total_2 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activosc = ($activosc + 1);
                        }
                    } else {
                        $total_3 = ($total_3 + 1);
                    }
                } else {//server
                    $tserver = ($tserver + 1);

                    if ($reg_equipos["rango"] == 1) {
                        $total_4 = ($total_4 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activoss = ($activoss + 1);
                        }
                    } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                        $total_5 = ($total_5 + 1);
                        if ($reg_equipos["errors"] == 'Ninguno') {
                            $activoss = ($activoss + 1);
                        }
                    } else {
                        $total_6 = ($total_6 + 1);
                    }
                }
            }
        }
    }//0
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                ->setTitle("Usabilidad");


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '')
                ->setCellValue('B1', '')
                ->setCellValue('C1', 'Usabilidad')
                ->setCellValue('D1', '')
                ->setCellValue('E1', '')
                ->setCellValue('F1', '');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Usabilidad')
                ->setCellValue('B2', 'Clientes')
                ->setCellValue('C2', '%')
                ->setCellValue('D2', 'Servidores')
                ->setCellValue('E2', '%')
                ->setCellValue('F2', 'Activo');

    $porct1 = 0;
    $porct2 = 0;
    $porct3 = 0;
    if($tclient > 0){
        $porct1 = ($total_1 / $tclient) * 100;
        $porct2 = ($total_2 / $tclient) * 100;
        $porct3 = ($total_3 / $tclient) * 100;
    }
    
    $porct11 = 0;
    $porct22 = 0;
    $porct33 = 0;
    if($tserver > 0){
        $porct11 = ($total_4 / $tserver) * 100;
        $porct22 = ($total_5 / $tserver) * 100;
        $porct33 = ($total_6 / $tserver) * 100;
    }

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A3', 'En Uso')
                ->setCellValue('B3', $total_1)
                ->setCellValue('C3', round($porct1))
                ->setCellValue('D3', $total_4)
                ->setCellValue('E3', round($porct11))
                ->setCellValue('F3', 'Si');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A4', 'Probablemente en Uso')
                ->setCellValue('B4', $total_2)
                ->setCellValue('C4', round($porct2))
                ->setCellValue('D4', $total_5)
                ->setCellValue('E4', round($porct22))
                ->setCellValue('F4', 'Si');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A5', 'Obsoleto')
                ->setCellValue('B5', $total_3)
                ->setCellValue('C5', round($porct3))
                ->setCellValue('D5', $total_6)
                ->setCellValue('E5', round($porct33))
                ->setCellValue('F5', 'No');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A6', 'Gran total')
                ->setCellValue('B6', $tclient)
                ->setCellValue('C6', '')
                ->setCellValue('D6', $tserver)
                ->setCellValue('E6', '')
                ->setCellValue('F6', '');

    /*$objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A8', 'Estado')
                ->setCellValue('B8', 'Dispositivos')
                ->setCellValue('C8', '%')
                ->setCellValue('D8', 'Dispositivos')
                ->setCellValue('E8', '%')
                ->setCellValue('F8', '');

    $t1 = $total_1 + $total_2 + $total_3;
    $pot1 = ($activosc / $t1) * 100;

    $t11 = $total_4 + $total_5 + $total_6;
    $pot11 = ($activoss / $t11) * 100;

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A9', 'Escaneado')
                ->setCellValue('B9', $activosc)
                ->setCellValue('C9', round($pot1))
                ->setCellValue('D9', $activoss)
                ->setCellValue('E9', round($pot11))
                ->setCellValue('F9', '');

    $t1  = $total_1 + $total_2 + $total_3;
    $nt1 = $t1 - $activosc;

    $t1   = $total_1 + $total_2 + $total_3;
    $pot2 = ($nt1 / $t1) * 100;

    $t11  = $total_4 + $total_5 + $total_6;
    $nt11 = $t11 - $activoss;

    $t11   = $total_4 + $total_5 + $total_6;
    $pot22 = ($nt11 / $t11) * 100;

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A10', 'No Escaneado')
                ->setCellValue('B10', $nt1)
                ->setCellValue('C10', round($pot2))
                ->setCellValue('D10', $nt11)
                ->setCellValue('E10', round($pot22))
                ->setCellValue('F10', '');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A11', 'Total')
                ->setCellValue('B11', $total_1 + $total_2 + $total_3)
                ->setCellValue('C11', '')
                ->setCellValue('D11', $total_4 + $total_5 + $total_6)
                ->setCellValue('E11', '')
                ->setCellValue('F11', $total_4 + $total_5 + $total_6 + $total_1 + $total_2 + $total_3);

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A13', '')
                ->setCellValue('B13', 'Nombre Equipo')
                ->setCellValue('C13', 'Tipo')
                ->setCellValue('D13', 'Usabilidad')
                ->setCellValue('E13', 'Escaneado');

    $i = 14;
    $j = 1;
    foreach ($detalles->lista as $reg_equipos2) {
        if ($reg_equipos2["tipo"] == 1) {
            $tipo = 'Cliente';
        } else {
            $tipo = 'Servidor';
        }

        if ($reg_equipos2["rango"] == 1) {
            $uso = 'En Uso';
        } else if ($reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
            $uso = 'Probablemente en uso';
        } else {
            $uso = 'Obsoleto';
        }

        if ($reg_equipos2["rango"] == 1 || $reg_equipos2["rango"] == 2 || $reg_equipos2["rango"] == 3) {
            $escaneado = 'Si';
        } else {
            $escaneado = 'No';
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $j)
                    ->setCellValue('B' . $i, $reg_equipos2["equipo"])
                    ->setCellValue('C' . $i, $tipo)
                    ->setCellValue('D' . $i, $uso)
                    ->setCellValue('E' . $i, $escaneado);

        $i++;
        $j++;
    }*/


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="excelUsabilidadOracle' . $fecha . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
?>
