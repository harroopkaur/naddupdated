<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_filepcs_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_escaneo_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_conversion.php");
require_once($GLOBALS["app_root"] . "/clases/clase_configuraciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$detalles     = new DetallesOracle();
$filepcs      = new filepcsOracle();
$general      = new General();
$conversion   = new TablaC();
$conversion2  = new TablaC();
$escaneo      = new escaneoOracle();
$config       = new configuraciones();
$archivosDespliegue = new clase_archivos_fabricantes();
$log = new log();

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$listaEdiciones = $config->listar_edicionTotal();
$listaVersiones = $config->listar_versionTotal();

$datosAux = array();
if (isset($_POST['insertar']) && $_POST['insertar'] == 1) {
    if(isset($_FILES['archivo']['tmp_name']) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones
        if ($nombre_imagen != "") {
            $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
                $error2 = 1;
            }  // Permitir subir solo imagenes JPG,
        } else {
            $error2 = 2;
        }
        
        $imagen = $_SESSION['client_id'] . $_SESSION['client_empleado'] . "_LAE_Output" . date("dmYHis") . ".csv";
        if (!$filepcs->cargar_archivo($imagen, $temporal_imagen)) {
            $error2 = 5;
        } else {
            if($general->obtenerSeparadorLAE_Output("archivos_csvf2/" . $imagen) === true){
                if (($fichero = fopen("archivos_csvf2/" . $imagen, "r")) !== FALSE) {
                    $i = 1;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i == 1 && ($datos[0] != "DN" || $datos[1] != "objectClass" || $datos[2] != "cn" || $datos[3] != "userAccountControl" 
                        || $datos[4] != "lastLogon" || $datos[5] != "pwdLastSet" || $datos[6] != "operatingSystem" || $datos[7] != "operatingSystemVersion"
                        || $datos[8] != "lastLogonTimestamp")){
                            $error2 = 3;
                            unlink("archivos_csvf2/" . $imagen);
                            break;
                        }
                        $i++;
                    }
                }
            }
        }
    } else{
        $error2 = -1;
    }
    
    if ($error2 == 0) {
        
        $filepcs->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
        if($general->obtenerSeparadorLAE_Output("archivos_csvf2/" . $imagen) === true){
            if (($fichero = fopen("archivos_csvf2/" . $imagen, "r")) !== FALSE) {
                $i = 1;
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    if ($i > 1) {

                        $sistema = $datos[6];

                        if ($conversion->codigo_existe($datos[6], 0)) {
                            $conversion2->datos2($datos[6]);
                            $sistema = $conversion2->os;
                        }


                        if (!$filepcs->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $general->truncarString($datos[0], 250), 
                        $general->truncarString($datos[1], 250), $general->truncarString($datos[2], 250), $general->truncarString($datos[3], 250), 
                        $general->truncarString($datos[4], 250), $general->truncarString($datos[5], 250), $general->truncarString($sistema, 250), 
                        $general->truncarString($datos[7], 250))) {
                            echo $filepcs->error;
                        }
                    }
                    $i++;
                    $exito2 = 1;
                }
            }
        }

        if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 4) === false){
            $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 4);
        }

        $archivosDespliegue->actualizarDespliegue2($_SESSION["client_id"], $_SESSION['client_empleado'], 4, $imagen);
           
        //inicio actualizar filepcs
        $detalles->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
        if($filepcs->listar_todo($_SESSION['client_id'], $_SESSION['client_empleado'])){
            foreach ($filepcs->listado as $reg_f) {

                $host = explode('.', $reg_f["cn"]);
                if(filter_var($reg_f["cn"], FILTER_VALIDATE_IP)!== false){
                    $host[0] = $reg_f["cn"];
                }

                $sistema = $reg_f["os"];

                if ($reg_f["lastlogon"] != "") {

                    $value = round(($reg_f["lastlogon"] - 116444735995904000) / 10000000);

                    $fecha1 = date('d/m/Y', $value);

                    $dias1 = $general->daysDiff($value, strtotime('now'));
                } else {
                    $dias1 = NULL;
                }

                if ($reg_f["pwdlastset"] != "") {

                    $value2 = round(($reg_f["pwdlastset"] - 116444735995904000) / 10000000);

                    $fecha2 = date('d/m/Y', $value2);

                    $dias2 = $general->daysDiff($value2, strtotime('now'));
                } else {
                    $dias2 = NULL;
                }
                if ($reg_f["lastlogontimes"] != "") {

                    $value3 = round(($reg_f["lastlogontimes"] - 116444735995904000) / 10000000);

                    $fecha3 = date('d/m/Y', $value3);

                    $dias3 = $general->daysDiff($value3, strtotime('now'));
                } else {
                    $dias3 = NULL;
                }

                $minimos = $general->minimo($dias1, $dias2, $dias3);

                $minimor = round(abs($minimos), -1);

                if ($minimor <= 30) {
                    $minimo = 1;
                } else if ($minimor <= 60) {
                    $minimo = 2;
                } else if ($minimor <= 90) {
                    $minimo = 3;
                } else if ($minimor <= 365) {
                    $minimo = 4;
                } else {
                    $minimo = 5;
                }

                if ($minimo < 4) {
                    $activo = 1;
                } else {
                    $activo = 0;
                }

                $tipos = $general->search_server($reg_f["os"]);

                $tipo = $tipos[0];
                $familia = "";
                if(strpos($sistema, "Windows Server") !== false){
                    $familia = "Windows Server";
                }
                else if(strpos($sistema, "Windows") !== false){
                    $familia = "Windows"; 
                }

                $edicion = "";
                foreach($listaEdiciones as $rowEdiciones){
                    if(trim($rowEdiciones["nombre"]) != "" && strpos($sistema, trim($rowEdiciones["nombre"])) !== false && strlen(trim($rowEdiciones["nombre"])) > strlen($edicion)){
                         $edicion = trim($rowEdiciones["nombre"]);    
                    } 
                }
                if($edicion == "" || ($familia == "Windows Server" && $edicion == "Server")){
                    $edicion = "Standard";
                }

                $version = "";
                foreach($listaVersiones as $rowVersiones){
                    if(trim($rowVersiones["nombre"]) != "" && strpos($sistema, trim($rowVersiones["nombre"])) !== false && strlen(trim($rowVersiones["nombre"])) > strlen($version)){
                        $version = trim($rowVersiones["nombre"]);    
                    }
                } 

                if($familia == ""){
                    $edicion = "";
                    $version = "";
                }

                if ($detalles->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $host[0], $sistema, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimor, $activo, $tipo, $minimo)) {
                    $exito2 = 1;
                } else {
                    echo $detalles->error;
                }
            }//foreach
        }//if
        //fin actualizar filepcs
        //
        //inicio escaneo
        $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv";

        $escaneo->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
        if (($fichero = fopen("archivos_csvf1/" . $imagen, "r")) !== FALSE) {
            $i = 1;
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                if ($i != 1) {
                    $host = explode('.', $datos[0]);
                    if(filter_var($datos[0], FILTER_VALIDATE_IP)!== false){
                        $host[0] = $datos[0];
                    }

                    if (!$escaneo->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], addslashes($host[0]), $datos[1])) {

                    }
                }
                $i++;
                $exito2 = 1;
            }
        }
        //fin escaneo


        //inicio actualizar escaneo
        if ($escaneo->listar_todo2($_SESSION['client_id'], $_SESSION['client_empleado'])){
            foreach ($escaneo->listado as $reg_e) {
                $detalles->actualizar($_SESSION['client_id'], $_SESSION['client_empleado'],  addslashes($reg_e["equipo"]), $reg_e["errors"]);
            }
        }
        //fin actualizar escaneo
        
        $log->insertar(12, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue LAE");
    }//cago
}