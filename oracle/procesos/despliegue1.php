<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");

$resumen      = new resumenOracle();
$consolidado  = new consolidadoOracle();
$tablaMaestra = new tablaMaestra();
$archivosDespliegue = new clase_archivos_fabricantes();
$general = new General();
$log = new log();
$passLAD = new clase_pass();

$exito=0;
$error=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;
$exito4=0;
$error4=0;
$exito5=0;
$error5=0;

if(isset($_POST['insertar']) && $_POST['insertar'] == 1) {
    if(isset($_FILES['archivo']['tmp_name']) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
             if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
            // Permitir subir solo imagenes JPG,
            }else{
            $error=2;	
        }

        if(!file_exists($GLOBALS['app_root'] . "/oracle/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'])){
            mkdir($GLOBALS['app_root'] . "/oracle/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'], 0755, true);
        }
        
        if($error == 0){
            $listPassLAD = $passLAD->listar_todo();
            $pass = "";
           
            foreach($listPassLAD as $row){  
                $psw = $passLAD->desencriptar($row["descripcion"]);
                $rar_file = rar_open($temporal_imagen, $psw) or die("Can't open Rar archive");

                $entries = rar_list($rar_file);   
                
                $direcEntry = $GLOBALS["app_root"] . '/tmp';

                if(!file_exists($direcEntry)){
                    mkdir($direcEntry, 0755);
                }

                foreach ($entries as $entry) {
                    if($entry->extract($direcEntry)){
                        $pass = $psw;
                        $files = array_diff(scandir($direcEntry), array('.','..')); 
                        foreach ($files as $file) { 
                            unlink($direcEntry.'/'.$file); 
                        } 
                        rmdir($direcEntry);  
                    } 
                    
                    break;
                }
                
                if($pass != ""){
                    break;
                }
            }
            
            rar_close($rar_file);
            
            if($pass != ""){
                $rar_file = rar_open($temporal_imagen, $pass) or die("Can't open Rar archive");
                $entries = rar_list($rar_file);
    
                $archivosExtraer = 2;
                $archivosExtraidos = 0;
                foreach ($entries as $entry) {            
                    if($entry->getName() == "ReporteFinal.txt"){
                        if($entry->extract('archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/")){
                            $archivosExtraidos++;
                        }
                    }
                    if($entry->getName() == "Resultados_escaneo.txt"){
                        if($entry->extract('archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/")){
                            $archivosExtraidos++;
                        }
                    }
                }
        
                rar_close($rar_file);
        
                if($archivosExtraer > $archivosExtraidos){
                    $error = 6;
                } else{
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/ReporteFinal.txt", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/ReporteFinal" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/ReporteFinal.txt", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/ReporteFinal.csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_escaneo.txt", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo" . date("dmYHis") . ".csv");
                    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_escaneo.txt", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv");
                }
                if (($fichero = fopen("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/Resultados_Escaneo.csv", "r")) !== FALSE) {
                    $i=1;
                    while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                        if($i == 2 && ($datos[0] != "Equipo" || $datos[1] != "Error")){
                            echo $datos[0] ." != Equipo  || " . $datos[1] . " != Error";
                            $error = 4;
                            break;
                        }
                        $i++;
                    }
                }
            } else{
                $error = -2;
            }
        }
    }
    else{
        $error = -1;
    }

    if($error == 0) {
        $nombreArchivo = 'LAD_Oracle' . date("dmYHis") . '.rar';
        move_uploaded_file($_FILES['archivo']['tmp_name'], 'archivos_csvf1/' . $_SESSION["client_id"] . '/' . $_SESSION['client_empleado'] . "/" . $nombreArchivo); 

        if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 4) === false){
            $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 4);
        }

        $tipoDespliegue = 0;
        
        $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION['client_empleado'], 4, $nombreArchivo, $tipoDespliegue);

        $imagen = $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/ReporteFinal.csv";

        $consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
        if(($fichero = fopen("archivos_csvf1/".$imagen, "r")) !== FALSE) {
            $i=1;
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                if($i == 1 && ($datos[0] != "Hostname" || $datos[1] != "Product" || $datos[2] != "Type" 
                || $datos[3] != "Description" || $datos[4] != "File")){
                    $error = 3;
                    break;
                }
                if($i > 1){
                    if(!$consolidado->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $general->truncarString($datos[0], 250), 
                    $general->truncarString($datos[1], 250), $general->truncarString($datos[2], 250), $general->truncarString($datos[3], 250), $general->truncarString($datos[4], 250))){
                        echo $consolidado->error;
                    }
                }
                $i++;
                $exito=1;	
            }
        }

        if($error == 0){
            //inicio resumen Oracle
            $resumen->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);

            $tablaMaestra->listadoProductosMaestra(4);
            foreach($tablaMaestra->listaProductos as $rowProductos){
                if($consolidado->listarProductos($_SESSION['client_id'], $_SESSION['client_empleado'], $rowProductos["descripcion"])){
                    foreach($consolidado->lista as $reg_r){  
                        $producto = "";

                        if($tablaMaestra->productosSustituir(4, $reg_r["product"])){
                            $producto  = $tablaMaestra->sustituir;
                            $idForaneo = $tablaMaestra->idForaneo;
                        }

                        $edicion = "";
                        $version = ""; 

                        $host = explode('.', $reg_r["host_name"], -1);
                        if(!isset($host[0])){
                            $host[0] = $reg_r["host_name"]; 
                        }      
                        if(filter_var($reg_r["host_name"], FILTER_VALIDATE_IP)!== false){
                            $host[0] = $reg_r["host_name"];
                        }

                        if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                            if($tablaMaestra->sustituir == null){
                                $version = "";
                            }
                            else{
                                $version = $tablaMaestra->sustituir;
                            }
                        }

                        $tablaMaestra->sustituir = "";
                        if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 5)){
                            if($tablaMaestra->sustituir == null){
                                $edicion = "";
                            }
                            else{
                                $edicion = $tablaMaestra->sustituir;
                            }
                        }

                        if(!$resumen->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $host[0], $producto, $edicion, $version)){
                            echo $resumen->error;
                        }
                    }  
                }
            }
        }
        //fin resumen Oracle

        $exito=1;  
        $log->insertar(12, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "Despliegue LAD");
    }
}