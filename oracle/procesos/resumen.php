<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_oracle.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

$tablaMaestra = new tablaMaestra();
$detalles = new DetallesOracle();
$balance  = new balanceOracle();
$resumen  = new resumenOracle();
$compras  = new comprasOracle();
$general = new General();

$tablaMaestra->productosSalida(4);
$total_1LAc       = 0;
$total_1InacLAc   = 0;
$total_2DVc       = 0;
$total_1LAs       = 0;
$total_1InacLAs   = 0;
$total_2DVs       = 0;
$reconciliacion1c = 0;
$reconciliacion1s = 0;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

$total_1 = 0;
$total_2 = 0;
$total_3 = 0;
$total_4 = 0;
$total_5 = 0;
$total_6 = 0;
$tclient = 0;
$tserver = 0;
$activosc = 0;
$activoss = 0;

$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';

$vert = 0;
if (isset($_GET['vert']) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false) {
    $vert = $_GET['vert'];
}

$asig = "";
if (isset($_GET['asig'])) {
    $asig = $_GET['asig'];
}

$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

if ($vert == 0) {
    $listado = $detalles->listar_todoAsignacion($_SESSION['client_id'], $asig, $asignaciones);
    foreach ($listado as $reg_equipos) {

        if ($reg_equipos["tipo"] == 1) {//cliente
            $tclient = ($tclient + 1);
            if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                $total_1LAc = ($total_1LAc + 1);
            }

            if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_2DVc = ($total_2DVc + 1);
            }
            else{
                $total_1InacLAc++;
            }
        } else {//server
            if (($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) && $reg_equipos["errors"] == 'Ninguno') {
                $total_1LAs = ($total_1LAs + 1);
            }

            $tserver = ($tserver + 1);

            if ($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_2DVs = ($total_2DVs + 1);
            }
            else{
                $total_1InacLAs++;
            }
        }
    }
    $reconciliacion1c = $total_2DVc - $total_1LAc;
    $reconciliacion1s = $total_2DVs - $total_1LAs;
}//0


if ($vert == 1 || $vert == 3) {
    $listado = $detalles->listar_todoAsignacion($_SESSION['client_id'], $asig, $asignaciones);
    foreach ($listado as $reg_equipos) {

        if ($reg_equipos["tipo"] == 1) {
            $tclient = ($tclient + 1);

            if ($reg_equipos["rango"] == 1) {
                $total_1 = ($total_1 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activosc = ($activosc + 1);
                }
            } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_2 = ($total_2 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activosc = ($activosc + 1);
                }
            } else {
                $total_3 = ($total_3 + 1);
            }
        } else {//server
            $tserver = ($tserver + 1);

            if ($reg_equipos["rango"] == 1) {
                $total_4 = ($total_4 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activoss = ($activoss + 1);
                }
            } else if ($reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3) {
                $total_5 = ($total_5 + 1);
                if ($reg_equipos["errors"] == 'Ninguno') {
                    $activoss = ($activoss + 1);
                }
            } else {
                $total_6 = ($total_6 + 1);
            }
        }
    }
}//1

if ($vert == 2) {
    $balanza = true;
    
    //$TotalApplicationCompras = 0;
    $TotalApplicationUsar    = 0;
    $TotalApplicationSinUsar = 0;
    
    //$TotalMiddlewareCompras = 0;
    $TotalMiddlewareUsar    = 0;
    $TotalMiddlewareSinUsar = 0;
   
    //$TotalDatabaseCompras = 0;
    $TotalDatabaseUsar    = 0;
    $TotalDatabaseSinUsar = 0;

    //$clientTotalProductCompras = 0;
    $clientTotalProductInstal  = 0;
    $clientProductNeto         = 0;
    
    //Application
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Application")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalApplicationCompras += $reg_equipos["compra"];
            //$TotalAdobeAcrobatInstal  += $reg_equipos["instalaciones"];
        }
    }
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], "Application")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalApplicationUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalApplicationSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalApplicationInstal = $TotalApplicationUsar + $TotalApplicationSinUsar;
    $ApplicationNeto = $TotalApplicationCompras - $TotalApplicationInstal;*/
    
    $listar_Application = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Application", "Todo", $asig, $asignaciones); 
    foreach ($listar_Application as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalApplicationUsar += $reg_equipos["conteo"];
        } else{
            $TotalApplicationSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalApplicationCompras = $compras->totalCompras($_SESSION['client_id'], "Application", "Todo", $asig, $asignaciones);
    $ApplicationNeto = $TotalApplicationCompras - ($TotalApplicationUsar + $TotalApplicationSinUsar);
    
    
    //Middleware
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Middleware")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalMiddlewareCompras += $reg_equipos["compra"];
            //$TotalCreativeCloudInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], "Middleware")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalMiddlewareUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalMiddlewareSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
    $TotalMiddlewareInstal = $TotalMiddlewareUsar + $TotalMiddlewareSinUsar;
    $MiddlewareNeto         = $TotalMiddlewareCompras - $TotalMiddlewareInstal;*/
    $listar_Middleware = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Middleware", "Todo", $asig, $asignaciones); 
    foreach ($listar_Middleware as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalMiddlewareUsar += $reg_equipos["conteo"];
        } else{
            $TotalMiddlewareSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalMiddlewareCompras = $compras->totalCompras($_SESSION['client_id'], "Middleware", "Todo", $asig, $asignaciones);
    $MiddlewareNeto = $TotalMiddlewareCompras - ($TotalMiddlewareUsar + $TotalMiddlewareSinUsar);
    
    //Database
    /*if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Database")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalDatabaseCompras += $reg_equipos["compra"];
            //$TotalCreativeSuiteInstal  += $reg_equipos["instalaciones"];
        }
    }
    
    if($resumen->graficoBalanza($_SESSION['client_id'], $_SESSION['client_empleado'], "Database")) {
        foreach ($resumen->lista as $reg_equipos) {
            if($reg_equipos["rango"] == 1 || $reg_equipos["rango"] == 2 || $reg_equipos["rango"] == 3){
                $TotalDatabaseUsar  += $reg_equipos["conteo"];
            }
            else{
                $TotalDatabaseSinUsar += $reg_equipos["conteo"];
            }
            
        }
    }
  
    $TotalDatabaseInstal = $TotalDatabaseUsar + $TotalDatabaseSinUsar;
    $DatabaseNeto         = $TotalDatabaseCompras - $TotalDatabaseInstal;*/
    $listar_Database = $resumen->graficoBalanzaAsignacion($_SESSION['client_id'], "Database", "Todo", $asig, $asignaciones); 
    foreach ($listar_Database as $reg_equipos) {
        if($reg_equipos["rango"] == 1){
            $TotalDatabaseUsar += $reg_equipos["conteo"];
        } else{
            $TotalDatabaseSinUsar += $reg_equipos["conteo"];
        }
    }
    $TotalDatabaseCompras = $compras->totalCompras($_SESSION['client_id'], "Database", "Todo", $asig, $asignaciones);
    $DatabaseNeto = $TotalDatabaseCompras - ($TotalDatabaseUsar + $TotalDatabaseSinUsar);
}