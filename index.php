<?php
require_once("configuracion/inicio.php");

$_SESSION["idioma"] = 1;
if (isset($_GET["idioma"]) && filter_var($_GET["idioma"], FILTER_VALIDATE_INT) !== false){
    $idiomaAux = $_GET["idioma"];
    if ($idiomaAux > 0 && $idiomaAux <= 2){
        $_SESSION["idioma"] = $_GET["idioma"];
    }   
} 
?>
<!DOCTYPE HTML>
<html>
    <head>
        <style>
            body {
                background:url(imagenes/inicio/bg_body.jpg) top left no-repeat !important;	

            }
        </style>
        <title>.:Licensing Assurance Webtool:.</title>
        <link rel="shortcut icon" href="img/Logo.ico">
        <!-- Custom Theme files -->
        <link href="css/style3.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <meta name="keywords" content="trial, sofware" />
        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
    </head>
    <body>
        <?php
        if ($_SESSION["idioma"] == 1){
            echo $base_url_parts['host'];
            echo $GLOBALS["app_root"];
            include_once($GLOBALS["app_root"] . "/plantillas/index.php"); 
        } else if($_SESSION["idioma"] == 2){
            include_once($GLOBALS["app_root"] . "/plantillas/idioma/ingles/index.php"); 
        }