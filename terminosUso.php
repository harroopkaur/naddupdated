<?php require("configuracion/inicio.php"); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <style>
            body {
                background:url(imagenes/inicio/bg_body.jpg) top left no-repeat !important;	

            }
        </style>
        <title>.:Licensing Assurance Webtool:.</title>
        <link rel="shortcut icon" href="../img/Logo.ico">
        <!-- Custom Theme files -->
        <link href="css/style3.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <meta name="keywords" content="trial, sofware" />
        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
    </head>
    <body>        
        <div class="contenedorTerminosCondiciones">
            <div class="text-center">
                <img src="../img/LA_Logo.png" style="width:150px">
            </div>
            <h1 class="tituloTerminosCondiciones">T&Eacute;RMINOS Y CONDICIONES SOBRE EL USO DEL SITIO WEB "WEBTOOL"</h1>

            <div class="parrafoTerminosCondiciones">
                <p>Los siguientes t&eacute;rminos y condiciones (los <span class="bold">"T&eacute;rminos y Condiciones"</span>) rigen el uso 
                que usted le de a este Sitio Web (La <span class="bold">"Webtool"</span>), y a cualquiera de los contenidos disponibles por o a trav&eacute;s 
                del mismo, incluyendo cualquier contenido derivado del referido Sitio Web. Licensing Assurance, LLC., <span class="bold">("Licensing Assurance, LLC.,")</span> 
                ha puesto a su disposici&oacute;n la <span class="bold">WEBTOOL</span>.  <span class="bold">Licensing Assurance, LLC.</span>, puede cambiar los T&eacute;rminos y Condiciones 
                cuando lo crea pertinente y a beneficio de su seguridad jurídica, en cualquier momento sin ninguna notificación 
                previa, s&oacute;lo publicando los cambios en la WEBTOOL. AL USAR EL WEBTOOL, USTED (El USUARIO) ACEPTA Y ESTÁ DE 
                ACUERDO CON ESTOS TÉRMINOS Y CONDICIONES EN LO QUE SE REFIERE AL USO Y CONTENIDOS DERIVADOS DE LA WEBTOOL. 
                Si usted no está de acuerdo con estos Términos y Condiciones, no puede tener acceso al servicio proporcionado 
                por Licensing Assurance, LLC.</p>
                
                <br>
                
                <ul class="listaNumerica">
                    <li>
                        <p>Derechos de Propiedad: Entre <span class="bold">El Usuario</span> y <span class="bold">Licensing Assurance, 
                        LLC., Licensing Assurance, LLC.</span>, es dueño &uacute;nico y exclusivo, de todos los derechos, t&iacute;tulo e 
                        intereses en y de la <span class="bold">WEBTOOL</span>, de todo el contenido (incluyendo, por ejemplo, audio, fotograf&iacute;as, 
                        ilustraciones, gr&aacute;ficos, otros medios visuales, videos, copias, textos, t&iacute;tulos, etc.), c&oacute;digos, 
                        datos y materiales del mismo, el aspecto y el ambiente, el diseño y la organizaci&oacute;n de la <span class="bold">WEBTOOL</span>, 
                        y la compilaci&oacute;n de los contenidos, c&oacute;digos, datos y los materiales en la <span class="bold">WEBTOOL</span>, incluyendo pero 
                        no limitado a, cualesquiera derechos de autor, derechos de patente, derechos de base de datos, derechos 
                        morales, derechos sui generis y otras propiedades intelectuales y derechos patrimoniales del mismo. 
                        Su uso de la <span class="bold">WEBTOOL</span>, no le otorga propiedad de ninguno de los contenidos, c&oacute;digos, datos o  materiales 
                        a los que pueda acceder en o a trav&eacute;s de la <span class="bold">WEBTOOL</span>.</p>
                    </li>
                    
                    <br>
                   
                    <li>
                        <p>Uso de la WebTool: El acceso y uso de la <span class="bold">WEBTOOL</span> se proporciona una 
                        vez que se efect&uacute;a consensualmente la contrataci&oacute;n de los servicios que ofrece <span class="bold">Licensing Assurance, 
                        LLC.</span>, entre el <span class="bold">Usuario</span> y <span class="bold">Licensing Assurance, LLC.</span>, para la administraci&oacute;n y optimizaci&oacute;n de sus 
                        activos de software (SAM as a Service). La <span class="bold">WEBTOOL</span> forma parte del servicio Licensing WebTool, 
                        facilitando el proceso del Licenciamiento. El Usuario descarga la herramienta de descubrimiento, 
                        y sube el resultado junto con la informaci&oacute;n de compras. De manera que, a trav&eacute;s de la <span class="bold">WEBTOOL</span> 
                        puede visualizar la usabilidad de equipos, detalle de instalaciones, y balanza de licenciamiento.</p>
                        
                        <ul class="listaNumerica">
                            <br>
                            
                            <li>
                                <p>Usted puede acceder y disponer de la <span class="bold">WEBTOOL</span>, desde su computadora 
                                o desde cualquier otro equipo que sea parte del servicio ajustado a sus necesidades; y su 
                                contrataci&oacute;n y precios ser&aacute;n supeditados a un contrato a priori a este acuerdo que El Usuario 
                                ya acept&oacute; en solicitud de los servicios de <span class="bold">Licensing Assurance, LLC.</span>, Cualquier distribuci&oacute;n, 
                                publicaci&oacute;n o explotaci&oacute;n comercial o promocional de la <span class="bold">WEBTOOL</span>, o de cualquiera de los contenidos, 
                                c&oacute;digos, datos o materiales de la <span class="bold">WEBTOOL</span>, est&aacute; estrictamente prohibida, a menos de que usted haya 
                                recibido el previo permiso expreso por escrito del personal autorizado de <span class="bold">Licensing Assurance, LLC.</span>, 
                                A no ser como est&aacute; expresamente permitido de acuerdo al servicio que contrat&oacute;, y a los t&eacute;rminos y 
                                condiciones de uso que aqu&iacute; se estipulan, usted no puede descargar, informar, exponer, publicar, 
                                copiar, transmitir, ejecutar, difundir, transferir, crear trabajos derivados de, vender o de 
                                cualquier otra manera explotar cualquiera de los contenidos, c&oacute;digos, datos o materiales en o 
                                disponibles a trav&eacute;s de la <span class="bold">WebTool</span>.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Si usted hace otro uso de la <span class="bold">WEBTOOL</span>, o de los contenidos, 
                                c&oacute;digos, datos o materiales que ah&iacute; se encuentren o que estén disponibles a trav&eacute;s 
                                de la <span class="bold">WEBTOOL</span>, a no ser como se ha estipulado anteriormente, usted puede violar las leyes de 
                                derechos de autor y otras leyes de los Estados Unidos, as&iacute; como las leyes estatales aplicables, 
                                y puede ser sujeto a responsabilidad legal por dicho uso no autorizado.</p>
                            </li>
                        </ul>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p >Derecho de Confidencialidad: El usuario al aceptar los t&eacute;rminos 
                        y condiciones declara que solicita a <span class="bold">Licensing Assurance, LLC.</span>, la habilitaci&oacute;n 
                        de Licensing WebTool, (El <span class="bold">"SERVICIO"</span>), para lo cual debe compartir 
                        informaci&oaute;n que tendr&aacute; car&aacute;cter confidencial (<span class="bold">"INFORMACI&Oacute;N CONFIDENCIAL"</span>). 
                        Por lo tanto, las partes mantendr&aacute;n toda la <span class="bold">INFORMACIÓN CONFIDENCIAL</span> 
                        como reservada, privilegiada y confidencial, y en consecuencia, se obligan a no revelar la misma 
                        a ning&uacute;n tercero.</p>
                        
                        <br>
                        
                        <ul class="listaNumerica">
                            <li>
                                <p>La <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span> podr&aacute; ser 
                                proporcionada a <span class="bold">LICENSING ASSURANCE, LLC</span> directamente 
                                por el <span class="bold">USUARIO</span> o por sus representantes.  La <span class="bold">INFORMACI&Oacute;N 
                                CONFIDENCIAL</span> podr&aacute; incluir, como ejemplo y sin ser limitativo, tecnolog&iacute;a, 
                                desarrollos de software, m&eacute;todos, posici&oacute;n de licenciamiento, licencias compradas, 
                                migraciones, desinstalaciones, instalaciones, nombre de dominios, nombres de computadoras, usuarios, 
                                y clientes, as&iacute; como tambi&eacute;n incluye los t&eacute;rminos del presente Contrato y de 
                                los Servicios.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>La <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span> s&oacute;lo podrá darse 
                                a conocer a aquellos trabajadores, contratistas, asesores o compañ&iacute;as afiliadas a 
                                <span class="bold">LICENSING ASSURANCE, LLC.</span>, que tengan necesidad de conocerla 
                                para hacer recomendaciones para la realizaci&oacute;n del <span class="bold">SERVICIO</span>. 
                                En caso de que una parte o toda la <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span> 
                                deba ser entregada a dichos trabajadores, contratistas, asesores o compañ&iacute;as afiliadas, 
                                <span class="bold">LICENSING ASSURANCE, LLC</span> advertir&aacute; a dichas personas del car&aacute;cter 
                                privado de la <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span> y deber&aacute; darles 
                                a conocer los presentes t&eacute;rminos y condiciones, a efectos de que estas personas los acepten 
                                y se adhieran a los mismos antes de recibirla. En cualquier caso, <span class="bold">LICENSING ASSURANCE, LLC.</span>, 
                                será responsable de cualquier incumplimiento del presente Acuerdo realizado por sus trabajadores, 
                                contratistas o asesores, o por sus compañías afiliadas.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Las partes declaran y garantizan que la divulgaci&oacute;n y entrega de cualquier 
                                informaci&oacute;n, documentos, software y otros materiales y el uso del mismo, no infringir&aacute; 
                                o violar&aacute; cualquier derecho de propiedad de terceros, incluyendo, sin limitaci&oacute;n, 
                                cualquier derecho de autor, patente conocida o el comercio justo.</p>
                            </li>
                        </ul>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p>Veracidad de la Informaci&oacute;n y Garant&iacute;a del Servicio: La veracidad de la 
                        informaci&oacute;n que resulta de los procesos establecidos y diseñados por <span class="bold">LICENSING ASSURANCE, LLC.</span>, 
                        es garantizada y respaldada, siempre que el <span class="bold">USUARIO</span> brinde la informaci&oacute;n 
                        correcta e implemente las mejores pr&aacute;cticas sugeridas por <span class="bold">LICENSING ASSURANCE, LLC.</span> 
                        Debido a que el licenciamiento es cambiante, <span class="bold">EL USUARIO</span> entiende que este servicio no garantiza 
                        el licenciamiento a futuro.</p>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p><span class="bold">LICENSING ASSURANCE, LLC</span> se compromete a</p>
                        
                        <br>
                        
                        <ul class="listaAlfabetica">
                            <li>
                                <p>Mantener la <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span> en estricta 
                                confidencialidad y utilizarla solo para el <span class="bold">SERVICIO</span> y 
                                para ning&uacute;n otro fin que sea sin el previo consentimiento escrito del <span class="bold">USUARIO</span>;</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>y de modo inmediato a solicitud del <span class="bold">USUARIO</span>, destruir 
                                o devolver a toda <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span> junto 
                                con todas las copias permitidas de la misma.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Aplicar al menos, los mismos procedimientos y salvaguardas que se utilizan en conexi&oacute;n 
                                con su propia informaci&oacute;n confidencial con el fin de evitar la revelaci&oacute;n de 
                                <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span> a terceros.</p>
                            </li>
                        </ul>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p>Limitaciones de Uso y Revelaci&oacute;n de Informaci&oacute;n: Las limitaciones sobre el uso 
                        y la revelaci&oacute;n de Informaci&oacute;n no aplicar&aacute;n a ninguna parte de la Informaci&oacute;n 
                        Confidencial en el caso:</p>
                        
                        <br>
                        
                        <ul class="listaAlfabetica">
                            <li>
                                <p>Es de acceso p&uacute;blico o de dominio p&uacute;blico en el momento de divulgada.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Es o se convierte a disposici&oacute;n del p&uacute;blico o entra en el dominio 
                                p&uacute;blico por causas ajenas a <span class="bold">LICENSING ASSURANCE, LLC</span>.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Es leg&iacute;timamente comunicada a <span class="bold">LICENSING ASSURANCE, LLC.</span>, 
                                por parte de personas que no est&aacute;n vinculadas por las obligaciones de confidencialidad 
                                con respecto a la misma.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Ya est&aacute; en posesi&oacute;n de <span class="bold">LICENSING ASSURANCE, LLC.</span>, 
                                libre de cualquier obligaci&oacute;n de confidencialidad con respecto a la misma.</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Sea desarrollada independientemente por <span class="bold">LICENSING ASSURANCE, LLC.</span>, 
                                sin el uso de cualquier informaci&oacute;n confidencial o</p>
                            </li>
                            
                            <br>
                            
                            <li>
                                <p>Est&aacute; aprobado para su liberaci&oacute;n o la divulgaci&oacute;n por parte del <span class="bold">USUARIO</span> 
                                por escrito y sin restricci&oacute;n.</p>
                            </li>
                        </ul>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p>No obstante, de lo señalado, las partes tienen derecho a hacer referencia a la otra parte 
                        ante terceros como cliente o proveedor, seg&uacute;n corresponda, y/o describir el trabajo terminado 
                        en virtud de la prestaci&oacute;n del <span class="bold">SERVICIO</span>, sin revelar ninguna 
                        informaci&oacute;n de car&aacute;cter confidencial.</p>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p>Si <span class="bold">LICENSING ASSURANCE, LLC.</span>, es consciente de alguna violaci&oacute;n 
                        al tratamiento de la <span class="bold">INFORMACI&Oacute;N CONFIDENCIAL</span>, notificar&aacute; 
                        inmediatamente por escrito al <span class="bold">USUARIO</span>, dando todos los detalles de los 
                        que dispone y a su propio costo. Bajo la direcci&oacute;n del <span class="bold">USUARIO</span>, 
                        el <span class="bold">LICENSING ASSURANCE, LLC.</span>, tomar&aacute; las medidas que el 
                        <span class="bold">USUARIO</span> pueda requerir con el fin de minimizar la p&eacute;rdida que de otro 
                        modo &eacute;ste pueda sufrir como resultado de dicha violaci&oacute;n.</p>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p>Leyes Aplicables: <span class="bold">LICENSING ASSURANCE, LLC.</span>, controla y opera 
                        el <span class="bold">WEBTOOL</span> desde sus oficinas en los Estados Unidos de Am&eaute;rica. 
                        No representa a los materiales en el <span class="bold">WEBTOOL</span> como apropiados o disponibles 
                        para su uso en otros lugares. Las personas que escojan acceder al <span class="bold">WEBTOOL</span> 
                        desde otros lugares lo har&aacute;n por su propia iniciativa y son responsables del cumplimiento de 
                        las leyes locales, si y al grado en que las leyes locales sean aplicables. Todas las partes sujetas 
                        a estos t&eacute;rminos y condiciones renuncian a sus derechos respectivos a un juicio con jurado.</p>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p><span class="bold">LICENSING ASSURANCE, LLC.</span>, puede terminar, cambiar, suspender 
                        o descontinuar cualquier aspecto del <span class="bold">WEBTOOL</span> o de los servicios 
                        del <span class="bold">WEBTOOL</span> en cualquier momento. <span class="bold">LICENSING ASSURANCE, LLC.</span>, 
                        puede restringir, suspender o terminar su acceso al <span class="bold">WEBTOOL</span> y/o 
                        a sus servicios si cree que el <span class="bold">USUARIO</span> est&aacute; en incumplimiento 
                        de los presentes t&eacute;rminos y condiciones; o de la ley aplicable, o por cualquier otra 
                        raz&oacute;n sin notificaci&oacute;n o responsabilidad. <span class="bold">LICENSING ASSURANCE, LLC.</span>, 
                        mantiene una pol&iacute;tica que estipula la terminaci&oacute;n, en circunstancias apropiadas, 
                        de los privilegios de uso del <span class="bold">WEBTOOL</span> para usuarios que son violadores 
                        repetitivos de los derechos de propiedad intelectual.</p>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p><span class="bold">LICENSING ASSURANCE, LLC.</span>, se reserva el derecho, a su sola discreci&oacute;n, 
                        de cambiar, modificar, añadir o quitar cualquier porci&oacute;n de los T&eacute;rminos y Condiciones, 
                        toda o en parte, en cualquier momento. Los cambios en los T&eacute;rminos y Condiciones ser&aacute;n 
                        efectivos cuando se publiquen. La continuaci&oacute;n del uso de los servicios puestos a disposici&oacute;n 
                        a trav&eacute;s de la <span class="bold">"Webtool"</span> despu&eacute;s de haber sido publicado cualquier 
                        cambio, ser&aacute; considerado como aceptaci&oacute;n de esos cambios.</p>
                    </li>
                    
                    <br>
                    
                    <li>
                        <p>Los T&eacute;rminos y Condiciones y la relaci&oacute;n entre el <span class="bold">USUARIO</span> 
                        y <span class="bold">LICENSING ASSURANCE, LLC.</span>, ser&aacute;n regidos por las leyes del 
                        Estado de Florida, Estados Unidos de Am&eacute;rica, sin consideraci&oacute;n a conflictos de 
                        disposiciones de ley. El <span class="bold">USUARIO</span> se obliga a que cualquier causa de 
                        acci&oacute;n legal que resulte bajo los T&eacute;rminos y Condiciones ser&aacute; iniciada y 
                        o&iacute;da en la corte apropiada en el Estado Delaware, Estados Unidos de Am&eacute;rica. El 
                        Usuario se obliga a someterse a la jurisdicci&oacute;n personal y exclusiva de las cortes localizadas 
                        dentro del Estado de Delaware. La falla al ejercer y hacer valer cualquier derecho o disposici&oacute;n 
                        de los T&eacute;rminos y Condiciones de <span class="bold">LICENSING ASSURANCE, LLC.</span>, no 
                        constituir&aacute; una renuncia a dicho derecho o disposici&oacute;n. Si cualquier disposici&oacute;n 
                        de los T&eacute;rminos y Condiciones es encontrada inv&aacute;lida por una corte de jurisdicci&oacute;n 
                        competente, las partes, est&aacute;n de acuerdo en que la corte deber&aacute; esforzarse en aplicar 
                        las intenciones de las partes como est&aacute;n reflejadas en la disposici&oacute;n, y las otras 
                        disposiciones de los T&eacute;rminos y Condiciones permanecen vigentes.</p>
                    </li>
                </ul>
                
                <br>
                
                <p>Estos T&eacute;rminos y Condiciones fueron corregidos 1/02/2017.</p>
            </div>
        </div>
    </body>
</html>