<?php
require_once("../configuracion/inicio.php");
require_once("sesion.php");
require_once("../clases/clase_general.php");
require_once("../sam/clases/funciones.php");

//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>'false', 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = 'true';
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $sesion = "true";
            $contratos = $_POST["checkContratos"];
            $fabricante = "";
            if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }
            
            $funciones = new funcionesSam();
            $query = $funciones->listadoContratoLicenciasCliente($_SESSION["client_id"], $fabricante, $contratos);
           
            $i = 0;
            $tabla = "";
            foreach($query as $row){
                $tabla .= '<tr style="border-bottom: 1px solid #ddd">
                    <td style="text-align:center;"><input type="checkbox" id="check' . $i .'" name="check[]" value="';
                if($fabricante == 3){
                    $tabla .= $row["producto"]."*".$row["edicion"]."*".$row["version"]."*".$row["tipo"]."*".$row["cantidad"]."*".$row["precio"]."*".$row["asignacion"];
                } else{
                    $tabla .= $row["producto"]."*".$row["edicion"]."*".$row["version"]."*".$row["cantidad"]."*".$row["precio"]."*".$row["asignacion"];
                }
                    
                    $tabla .= '" checked="checked"></td>
                    <td>' . $row["nombre"] . '</td>
                    <td>' . $row["numero"] . '</td>
                    <td>' . $row["producto"] . '</td>
                    <td>' . $row["edicion"] . '</td>
                    <td>' . $row["version"] . '</td>';
                if($fabricante == 3){
                    $tabla .= '<td>' . $general->transformarTipoCompra($row["tipo"]) . '</td>';
                }
                    $tabla .= '<td>' . $row["cantidad"] . '</td>
                    <td>' . $row["asignacion"] . '</td>
                </tr>';
                
                $i++;
            }        
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'tabla'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);