<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Correo</title>
        <style type="text/css">
            body{
                width: 450px;
            }
            
            .head{
                width: 100%;
                min-height: 100px;
                background: #01b0f1;
            }
            
            .img-head{
                margin-left:10px;
                height:100px; 
                width:30%;
                float:left;
            }
            
            .titulo{
                font-size: 22px;
                color: #ffffff;
                font-weight: bold;
                text-align: center;
                float:left;
                width:60%;
                height: 100px;
                line-height:32px;
            }
            
            span{
                font-size: 20px;
            }
            
            .img-redes{
                width:40px; 
                height:auto;
                float:left;
            }
            
            .contenido{
                width:100%;
            }
            
            .reunion{
                float:left;
            }
        </style>
    </head>

    <body>      
        <img src="https://vieja-webtool.licensingassurance.com/img/webinar/cabeceraSAP.png" style="width:100%; height:auto;">
        
        <div class="contenido"><br><br>
            <p>Gracias por registrarse en el Webinar: Licenciamiento SAP</p>
            <p>No se preocupe si no tiene instalada la aplicación para reuniones, se instalará automáticamente un controlador web para que disfrute de la conferencia.</p>
            <p>Ingrese al webinar el Jueves 8 de Noviembre a las 10:00 am Hora Miami en el enlace a continuación:</p>
            
            <div class="reunion">
                <p><a href="https://meet.lync.com/netorg686527-licensingassurance/paola/ZRVL4V1R"><span>Join Skype Meeting</span><br>Trouble Joining? Try Skype Web App</a></p>
            </div>
            
            <a href="https://www.linkedin.com/company/9211105/" target="_blank"><img src="https://www.licensingassurance.com/img/webinar/linkedin.png" class="img img-responsive img-redes"></a>
            <a href="https://www.facebook.com/licensingassurance/" target="_blank"><img src="https://www.licensingassurance.com/img/webinar/facebook.png" class="img img-responsive img-redes"></a>
            <a href="https://twitter.com/LicAssurance"><img src="https://www.licensingassurance.com/img/webinar/twitter.png" class="img img-responsive img-redes"></a>
            <a href="https://www.youtube.com/channel/UCZMnfzMegbU4vIrNbLKnzew"><img src="https://www.licensingassurance.com/img/webinar/youtube.png" class="img img-responsive img-redes"></a>
        </div>
    </body>
</html>