<?php if($imgDashboard != ""){ ?>
    <div style="width:450px; font-size:18px; margin: 0 auto;">
        <span class="bold">Ver Detalle </span>
        <select id="presentaciones" name="presentaciones" class="text-center" style="width:300px; height:30px; font-size:18px;">
            <option value=""></option>
            <option value="todas">Todas</option>
            <?php
            foreach($listado as $row){
                if(file_exists($urlPresentacion1 . $row["presentacion"])){
                    ?>
                    <option value="<?= $row["presentacion"] ?>"><?= $row["presentacion"] ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </div>

    <br>
    <div style="width:90%; height:auto; margin:0 auto;">
        <img src="dashboard/<?= $imgDashboard ?>" style="width:100%; height:auto;">
    </div>
<?php 
}
?>

<!--Inicio Graficos-->
<script>
    $(document).ready(function () {
        $("#presentaciones").change(function(){
            if($("#presentaciones").val() === "todas"){
                if("<?= $presentacion1 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion1 ?>');
                }
                
                if("<?= $presentacion2 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion2 ?>');
                }
                
                if("<?= $presentacion3 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion3 ?>');
                }
                
                if("<?= $presentacion4 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion4 ?>');
                }
                
                if("<?= $presentacion5 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion5 ?>');
                }
                
                if("<?= $presentacion6 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion6 ?>');
                }
                
                if("<?= $presentacion7 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion7 ?>');
                }
                
                if("<?= $presentacion8 ?>" !== ""){
                    window.open('<?= $urlPresentacion . $presentacion8 ?>');
                }
            } else if($("#presentaciones").val() !== ""){
                window.open('<?= $urlPresentacion ?>' + $("#presentaciones").val());
            }
        });
        
        <?php
        /*if ($IBMActivo == 1){
            echo $graficos->graficoAlcance("containerMicrosoft", "Cliente", $reconciliacionMicrosoftC + $total_MicrosoftActC,
            $reconciliacionMicrosoftC, $total_MicrosoftActC, $color1, $color2);
            
            echo $graficos->graficoAlcance("containerMicrosoft2", "Servidor", $reconciliacionMicrosoftS + $total_MicrosoftActS,
            $reconciliacionMicrosoftS, $total_MicrosoftActS, $color1, $color2);
        }
        
        if ($adobeActivo == 1){
            echo $graficos->graficoAlcance("containerAdobe", "Cliente", $reconciliacionAdobeC + $total_AdobeActC,
            $reconciliacionAdobeC, $total_AdobeActC, $color1, $color2);
            
            echo $graficos->graficoAlcance("containerAdobe2", "Servidor", $reconciliacionAdobeS + $total_AdobeActS,
            $reconciliacionAdobeS, $total_AdobeActS, $color1, $color2);
        }
        
        if ($IBMActivo == 1){
            echo $graficos->graficoAlcance("containerIBM", "Cliente", $reconciliacionIBMC + $total_IBMActC,
            $reconciliacionIBMC, $total_IBMActC, $color1, $color2);
            
            echo $graficos->graficoAlcance("containerIBM2", "Servidor", $reconciliacionIBMS + $total_IBMActS,
            $reconciliacionIBMS, $total_IBMActS, $color1, $color2);
        }
        
        if ($OracleActivo == 1){
            echo $graficos->graficoAlcance("containerOracle", "Cliente", $reconciliacionOracleC + $total_OracleActC,
            $reconciliacionOracleC, $total_OracleActC, $color1, $color2);
            
            echo $graficos->graficoAlcance("containerOracle2", "Servidor", $reconciliacionOracleS + $total_OracleActS,
            $reconciliacionOracleS, $total_OracleActS, $color1, $color2);
        }
        
        if ($SPLAActivo == 1){
            echo $graficos->graficoAlcance("containerSPLA", "Cliente", $reconciliacionSPLAC + $total_SPLAActC,
            $reconciliacionSPLAC, $total_SPLAActC, $color1, $color2);
            
            echo $graficos->graficoAlcance("containerSPLA2", "Servidor", $reconciliacionSPLAS + $total_SPLAActS,
            $reconciliacionSPLAS, $total_SPLAActS, $color1, $color2);
        }*/
        ?>
    });
</script>
<!--Fin Graficos-->

<?php 
/*if ($MicrosoftActivo == 1){
    echo $dashboard->contenedorGrafico("containerMicrosoft", "containerMicrosoft2", "Microsoft", $fechaUltEscaneoMicrosoft, $porcMicrosoft, 
    $disponibleMicrosoft["sobrante"], $disponibleMicrosoft["faltante"], $fechaProximaMicrosoft, $optionAsignaciones);
}

if ($adobeActivo == 1){
    echo $dashboard->contenedorGrafico("containerAdobe", "containerAdobe2", "Adobe", $fechaUltEscaneoAdobe, $porcAdobe, 
    $disponibleAdobe["sobrante"], $disponibleAdobe["faltante"], $fechaProximaAdobe, $optionAsignaciones);
}

if ($IBMActivo == 1){
    echo $dashboard->contenedorGrafico("containerIBM", "containerIBM2", "Windows-IBM", $fechaUltEscaneoIBM, $porcIBM, 
    $disponibleIBM["sobrante"], $disponibleIBM["faltante"], $fechaProximaIBM, $optionAsignaciones);
}

if ($OracleActivo == 1){
    echo $dashboard->contenedorGrafico("containerOracle", "containerOracle2", "Windows-Oracle", $fechaUltEscaneoOracle, $porcOracle, 
    $disponibleOracle["sobrante"], $disponibleOracle["faltante"], $fechaProximaOracle, $optionAsignaciones);
}

if ($SPLAActivo == 1){
    echo $dashboard->contenedorGrafico("containerSPLA", "containerSPLA2", "SPLA", $fechaUltEscaneoSPLA, $porcSPLA, 
    "", "", $fechaProximaSPLA, "");
}

$j = 0;

for ($i = 0; $i < $controlFabSinGrafico; $i++){
    if ($j == 0){
    ?>
        <div class='contenedorDashboard'>
    <?php
    }
    
    if ($j < 2){
        if ($SAPMostrado == 0){
            echo $dashboard->contenedorInfo("SAP", $fechaUltEscaneoSAP, $disponibleSAP["sobrante"], $disponibleSAP["faltante"], $fechaProximaSAP, $j);
            $SAPMostrado =  1;
        } else if ($VMWareMostrado == 0){
            echo $dashboard->contenedorInfo("VMWare", $fechaUltEscaneoVMWare, $disponibleVMWare["sobrante"], $disponibleVMWare["faltante"], $fechaProximaVMWare, $j);
            $VMWareMostrado = 1;
        } else if ($UNIXIBMMostrado == 0){
            echo $dashboard->contenedorInfo("UNIX-IBM", $fechaUltEscaneoUnixIBM, $disponibleUnixIBM["sobrante"], $disponibleUnixIBM["faltante"], $fechaProximaUnixIBM, $j);
            $UNIXIBMMostrado = 1;
        } else if ($UNIXOracleMostrado == 0){
            echo $dashboard->contenedorInfo("UNIX-Oracle", $fechaUltEscaneoUnixOracle, $disponibleUnixOracle["sobrante"], $disponibleUnixOracle["faltante"], $fechaProximaUnixOracle, $j);
            $UNIXIBMMostrado = 1;
        }
        
        $j++;
        
        if ($j == 2){
            $j = -1;
        }
    } 
    
    if ($j == -1){
        $j++;
    ?>
        </div>
        <br>
    <?php
    }
}*/
?>

<script>
    /*function buscarGraficos(fabricante, asignacion){   
        $("#fondo").show();
        $.post("<?//= $GLOBALS['domain_root'] ?>/ajax/buscarGraficoDashboard.php", { fabricante: fabricante, 
        asignacion : asignacion, token : localStorage.licensingassuranceToken }, function(data) {
            $("#fondo").hide();
            <?php //require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
            
            $("#alcance" + fabricante).empty();
            $("#alcance" + fabricante).append("Alcance: " + data[0].porc + " %");
            $('#' + data[0].containerC).highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    size: '75%'
                },
                title: {
                    text: 'Cliente'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b>' + (data[0].reconciliacionC + data[0].total_ActC)
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        }
                    }
                },
                series: [{
                    name: 'Equipos',
                    colorByPoint: true,
                    data: [{
                        name: 'No Levantado',
                        y: data[0].reconciliacionC,
                        color: '" . $color1 . "'
                    },
                    {
                        name: 'Levantado',
                        y: data[0].total_ActC,
                        color: '" . $color2 . "'
                    }]
                }]
            });
           
            $('#' + data[0].containerS).highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    size: '75%'
                },
                title: {
                    text: 'Servidor'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b>' + (data[0].reconciliacionS + data[0].total_ActS)
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        }
                    }
                },
                series: [{
                    name: 'Equipos',
                    colorByPoint: true,
                    data: [{
                        name: 'No Levantado',
                        y: data[0].reconciliacionS,
                        color: '" . $color1 . "'
                    },
                    {
                        name: 'Levantado',
                        y: data[0].total_ActS,
                        color: '" . $color2 . "'
                    }]
                }]
            });
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    }*/
</script>