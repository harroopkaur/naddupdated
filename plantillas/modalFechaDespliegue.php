<!--inicio modal fecha despliegue-->
<div class="modal-personal modal-personal-md" id="modal-fechaDespliegue">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Fecha <p style="color:#06B6FF; display:inline">Despliegue</p></h1>
    </div>
    <div class="modal-personal-body text-center">
        <input type="hidden" id="fechaOpcion">
        <span class="bold">Fecha: </span><input type="text" id="fechaD" name="fechaD">
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirFechaDespliegue">Salir</div>
        <div class="boton1 botones_m2" style="float:right;" id="procesarLAE_Output">Procesar</div>
    </div>
</div>
<!--fin modal fecha despliegue-->