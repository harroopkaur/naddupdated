<!--inicio modal de ayuda despliegue-->
<div class="modal-personal" id="modal-ayudaDespliegue">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Ayuda <p style="color:#06B6FF; display:inline">Despliegue</p></h1>
    </div>
    <div class="modal-personal-body">
        <p class="text-center bold" style="font-size:25px;">Cabecera Consolidado Addremove</p>
        <p class="text-justify">La cabecera debe ir en la segunda fila del archivo Consolidado Addremove.csv</p>
        <table class="tablap">
            <tr>
                <th>Dato de Control</th>
                <th>HostName</th>
                <th>Registro</th>
                <th>Editor</th>
                <th>Versión</th>
                <th>Día de Instalación</th>
                <th>Software</th>
            </tr>
        </table>
        <br>
        <br>

        <?php if($fabricante == 3){ ?>
        <p class="text-center bold" style="font-size:25px;">Cabecera Consolidado Procesadores</p>
        <p class="text-justify">La cabecera debe ir en la segunda fila del archivo Consolidado Procesadores.csv</p>
        <table class="tablap">
            <tr>
                <th>Dato de Control</th>
                <th>HostName</th>
                <th>Tipo de CPU</th>
                <th>CPUs</th>
                <th>Cores</th>
                <th>Procesadores Lógicos</th>
                <th>Tipo de Escaneo2 F/V</th>
            </tr>
        </table>
        <p class="bold">Nota: El últmo campo puede variar, puede ser Tipo de Escaneo1 F/V, Tipo de Escaneo2 F/V, etc</p>
        <br>
        <br>
        <?php } ?>
        
        <?php if($fabricante == 3){ ?>
        <p class="text-center bold" style="font-size:25px;">Cabecera Consolidado Tipo de Equipo</p>
        <p class="text-justify">La cabecera debe ir en la segunda fila del archivo Consolidado Tipo de Equipo.csv</p>
        <table class="tablap">
            <tr>
                <th>Dato de Control</th>
                <th>HostName</th>
                <th>Fabricante</th>
                <th>Modelo</th>
            </tr>
        </table>
        <br>
        <br>
        <?php } ?>

        <p class="text-center bold" style="font-size:25px;">Cabecera Resultados_Escaneo</p>
        <p class="text-justify">La cabecera debe ir en la segunda fila del archivo Resultados_Escaneo.csv</p>
        <table class="tablap">
            <tr>
                <th>Hostname</th>
                <th>Status</th>
                <th>Error</th>
            </tr>
        </table>
        <br>
        <br>

        <p class="text-center bold" style="font-size:25px;">Cabecera LAE_Output</p>
        <p class="text-justify">La cabecera debe ir en la primera fila del archivo Consolidado LAE_Output.csv</p>
        <div style="overflow-x:auto;">
            <table class="tablap">
                <tr>
                    <th>DN</th>
                    <th>objectClass</th>
                    <th>cn</th>
                    <th>userAccountControl</th>
                    <th>lastLogon</th>
                    <th>pwdLastSet</th>
                    <th>operatingSystem</th>
                    <th>operatingSystemVersion</th>
                    <th>lastLogonTimestamp</th>
                    <th>description</th>
                </tr>
            </table>
        </div>
        <br>
        <br>

        <p class="bold">Nota: Los archivos .csv deben estar codificados en ANSI</p>
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirAyudaDespliegue">Salir</div>
    </div>
</div>
<!--fin modal de ayuda despliegue-->