<div style="width:98%; padding:10px; overflow:hidden;">
    <style>
        .contenedorLeftInicio{
            width:18%;
            overflow: hidden;
        }
        
        .contenedorCenterInicio{
            border:1px solid #FFFFFF;
            overflow: hidden;
            border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            height: 196px;
            font-size:13px;
        }
        
        .contenedorLeftInicio1{
            border:1px solid #FFFFFF;
            overflow: hidden;
            border-top-left-radius: 10px;
            -moz-border-top-left-radius: 10px;
            -webkit-border-top-left-radius: 10px;
            height: 60px;
            font-size:13px;
        }
        
        .contenedorLeftInicio2{
            border:1px solid #FFFFFF;
            overflow: hidden;
            height: 60px;
            padding-top:5px;
            padding-left:5px;
            font-weight: bold;
            font-size:13px;
        }
        
        .contenedorLeftInicio3{
            border:1px solid #FFFFFF;
            overflow: hidden;
            border-bottom-left-radius: 10px;
            -moz-border-bottom-left-radius: 10px;
            -webkit-border-bottom-left-radius: 10px;
            height: 60px;
            padding-top:5px;
            padding-left:5px;
            font-weight: bold;
            font-size:13px;
        }
        
        .contenedorRightInicio{
            width: 100%; /*81%;*/
            border:1px solid #FFFFFF;
            overflow: hidden;
        }
        
        .contenedorRightInicio1{
            border:1px solid #FFFFFF;
            overflow: hidden;
            border-top-right-radius: 10px;
            -moz-border-top-right-radius: 10px;
            -webkit-border-top-right-radius: 10px;
            height: 60px;
            font-size:13px;
        }
        
        .contenedorRightInicio2{
            border:1px solid #FFFFFF;
            overflow: hidden;
            height: 60px;
            padding-top:5px;
            padding-left:5px;
            font-size:13px;
        }
        
        .contenedorRightInicio3{
            border:1px solid #FFFFFF;
            overflow: hidden;
            border-bottom-right-radius: 10px;
            -moz-border-bottom-right-radius: 10px;
            -webkit-border-bottom-right-radius: 10px;
            height: 60px;
            padding-top:5px;
            padding-left:0px;
            font-size:13px;
        }
        
        .contenedorProcesos{
            overflow: hidden;
            height: 60px;
            /*padding-top:5px;
            padding-left:5px;*/
            font-weight: bold;
        }
        
        .contTituloProceso{
            font-weight: bold;
            border:1px solid #000000; 
            padding:4px;
            width:60px;
            position:absolute;
            text-align: center;
            height:25px;
        }
        
        .circuloProcesos {
            width: 60px;
            height: 32px;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            background: #5cb85c;
            border: 1px solid;
            overflow:hidden;
            font-weight: bold;
            line-height: 32px;
            text-align: center;
            margin-top:-10px;
            margin-left:3px;
        }
        
        .contenedorVertical{
            font-size: 15px;
            line-height: 30px;
            overflow: hidden;
            width: 30px;
            height: 196px;
            text-align: center;
            writing-mode: vertical-lr;
            transform: rotate(180deg);
            background-color: #FFFFFF;
            display: fixed;
            border: 1px solid #000000;
        }
        
        .contenedorVertical1{
            height: 196px;
            width: 18%;
        }
        
        .contenedorVertical2{
            height: 196px;
            width: calc(81% - 30px);
        }
        
        .contenedorVertical3{
            height: 196px;
            width: calc(99% - 30px);
        }
        
        .contenedorVerticalProcesos{
            height: 196px;
            width: calc(99% - 30px);
        }
        
        .contResumenInicio{
            border-top-left-radius: 10px;
            -moz-border-top-left-radius: 10px;
            -webkit-border-top-left-radius: 10px;
            height:60px;
        }
        
        .cuerpo{
            width: 75px;
            height: 44px;
            float: left;
            top:50%;
            margin-top: -16px;
            position:relative;
        }

        .cuerpo1{
            width: 75px;
            height: 44px;
            float: left;
            top: 50%;
            margin-top: -16px;
            position:relative;
            overflow:hidden;
        }
        
        .cuerpoLetrasLeft{
            width: 90px;
            height: 30px;
            float: left;
            top:50%;
            margin-top: -29.8px;
            position:relative;
        }
        
        .cuerpoLetrasProcesos{
            width: 90px;
            height: 30px;
            float: left;
            margin-left:-16px;
            top:50%;
            margin-top: -29.8px;
            position:relative;
            border: 0px solid;
        }

        .cuerpoFlecha{
            width: 43px;
            height: 0px;
            border-top: 15px solid blue;
            border-bottom: 15px solid blue;
            border-left: 15px solid transparent;
            float:left;
            color: #FFFFFF;
            text-align: center;
            line-height: 0px;
        }
        
        .puntaFlecha{
            width: 0;
            height: 0;
            border-top: 15px solid transparent;
            border-left: 15px solid blue;
            border-bottom: 15px solid transparent;
            float:left;
        }
        
        .centrarCalendario{
            width:910px;
            margin:0 auto;  
        }
        
        .contFabric{
            width:120px;
            float:left;
            line-height: 60px;
            /*border:1px solid;*/
        }
        
        .contFabricLogo{
            width:120px;
            float:left;
            line-height: 60px;
            height: 55px;
            overflow: hidden;
            position: relative;
            /*border:1px solid;*/
        }
        
        .lineaVertical{
            width:50%;
            height:25px; 
            border-right:1px solid black; 
            padding:0; 
            border-top:0; 
            border-bottom:0; 
            border-left:0; 
            margin-top:calc(50% - 5px);
        }
        
        .lineaVertical1{
            width:50%;
            height:25px; 
            border-right:1px solid black; 
            padding:0; 
            border-top:0; 
            border-bottom:0; 
            border-left:0; 
            margin-top:-3px;
            margin-left:-5px;
        }
        
        .colorIncio{
            color: #000000;
            background-color: #DCDCDC;
        }
        
        .colorIncio1{
            color: #000000;
            background-color: #99BFDC;
        }
        
        .colorIncio2{
            color: #000000;
            background-color: #243C67;
        }
        
        .colorIncio3{
            color: #000000;
            background-color: #00AFF0;
        }
        
        .colorIncio4{
            color: #000000;
            background-color: #006FC0;
        }
        
        .colorIncio5{
            color: #000000;
            background-color: #95DEF8;
        }
        
        .colorIncio6{
            color: #000000;
            background-color: #579BCC;
        }
        
        .colorIncio7{
            color: #000000;
            background-color: #344970;
        }
        
        .colorIncio8{
            color: #000000;
            background-color: #047FB3;
        }
        
        .colorIncio9{
            color: #000000;
            background-color: #05B5FE;
        }
        
        .colorIncio10{
            color: #000000;
            background-color: #81816A;
        }
    </style>
    
    <div>
        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/logoInicio.png" style="width:150px; height:auto; margin-top:-15px; margin-left:100px; float:left;">
        <p class="titulo1" style="line-height: 65px;">Bienvenido al Servicio de Manejo de Licenciamiento en la nube</p>
    </div>
    <br style="clear:both;"><br>
    
    <div class="contenedorFlex" >
        <!--<div class="contenedorLeftInicio text-center">
            <img src="<?//= $GLOBALS["domain_root"] ?>/imagenes/inicio/logoInicio.png" style="width:150px; height:auto;">
            
            <br><br><br>
            
            <p class="titulo1" style="line-height: 35px;">Bienvenido al Servicio de Manejo de Licenciamiento en la nube</p>
        </div>-->
               
        <div class="contenedorRightInicio backgroundColor0">
            <div class="contenedorFlex">
                <div class="contenedorVertical">            
                    Resumen
                </div>
                
                <div class="contenedorVertical1">
                    <div class="contenedorLeftInicio1 backgroundColor6">
                            &nbsp;
                    </div>

                    <div class="contenedorLeftInicio2 backgroundColor6">
                        <p>Último Levantamiento</p>
                        <p>% Alcance</p>
                    </div>
                    
                    <div class="contenedorLeftInicio3 backgroundColor6">
                        <p>Renovación o Aniversario</p>
                        <p>Monto del Pago</p>
                    </div>
                </div>
                
                <div class="contenedorVertical2">
                    <div class="contenedorRightInicio1 backgroundColor6">
                        <?php 
                        foreach($listarFabricantes as $row){
                        ?>
                            <div class="contFabricLogo text-center pointer bold" onclick="mostrarFabricante(<?= $row["idFabricante"] ?>)" style="height:100%">
                                <?php 
                                //echo $row["nombre"] 
                                if($row["idFabricante"] == 1){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/Adobe_Logo.png" style="width:auto; height:85%; top:50%; margin-top:-27.5px; margin-left:-19px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 2){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/Windows-IBM_Logo.png" style="width:85%; height:auto; top:50%; margin-top:-16px; margin-left:-52px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 3){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/Microsoft_Logo.png" style="width:auto; height:85%; top:50%; margin-top:-27.5px; margin-left:-35px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 4){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/Windows-Oracle_Logo.png" style="width:85%; height:auto; top:50%; margin-top:-5px; margin-left:-50px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 5){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/SAP_Logo.png" style="width:85%; height:auto; top:50%; margin-top:-26px; margin-left:-50px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 6){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/VMware_Logo.png" style="width:85%; height:auto; top:50%; margin-top:-13px; margin-left:-50px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 7){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/UNIX-IBM_Logp.png" style="width:auto; height:85%; top:50%; margin-top:-25px; margin-left:-50px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 8){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/UNIX-Oracle_Logo.png" style="width:85%; height:auto; top:50%; margin-top:-10px; margin-left:-50px; position:absolute;">
                                <?php
                                } else if($row["idFabricante"] == 10){
                                ?>
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/Windows-IBM_Logo.png" style="width:85%; height:auto; top:50%; margin-top:-16px; margin-left:-50px; position:absolute;">
                                <?php
                                }
                                ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>

                    <div class="contenedorRightInicio2 backgroundColor6" style="padding-left:0; padding-right:0;">
                        <?php 
                        foreach($listarFabricantes as $row){
                        ?>
                            <div class="contFabric text-center pointer" style="line-height: 30px;">
                                <?php
                                //echo $general->muestraFechaHora1($row["fechaRegistro"]) . "<br>";
                                echo $fechaLevant[$row["idFabricante"]] . "<br>";
                                
                                if($row["idFabricante"] == 1){
                                    echo $porcAdobe . "%";
                                } else if($row["idFabricante"] == 2){
                                    echo $porcIBM . "%";
                                } else if($row["idFabricante"] == 3){
                                    echo $porcMicrosoft . "%";
                                } else if($row["idFabricante"] == 4){
                                    echo $porcOracle . "%";
                                } else if($row["idFabricante"] == 10){
                                    echo $porcSPLA . "%";
                                }
                                ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>

                    <div class="contenedorRightInicio3 backgroundColor6">
                        <?php
                        foreach($listarFabricantes as $row){
                        ?>
                            <div class="contFabric text-center pointer" style="line-height: 30px;">
                                <?php
                                if(isset($fechaPagoCliente[$row["idFabricante"]])){
                                    if($fechaPagoCliente[$row["idFabricante"]]["aniversario"] != ""){
                                        echo $general->muestrafecha($fechaPagoCliente[$row["idFabricante"]]["aniversario"]) . "<br>";
                                    } else{
                                        echo $general->muestrafecha($fechaPagoCliente[$row["idFabricante"]]["renovacion"]) . "<br>";
                                    }
                                    echo $fechaPagoCliente[$row["idFabricante"]]["monto"];
                                }
                                ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            
            
            
            <div class="contenedorFlex">
                <div class="contenedorVertical">            
                    Procesos
                </div>
                
                <div class="contenedorVerticalProcesos">
                    <div class="contenedorLeftInicio1 centrarCalendario">
                        <div class="cuerpo">      
                            <?php 
                            if(count($ene) > 0){
                            ?>    
                                <span class="contTituloProceso <?= $ene[1] ?>"><?= $ene[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                                if(count($feb) > 0){
                            ?>
                                <span class="contTituloProceso <?= $feb[1] ?>"><?= $feb[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                              
                        <div class="cuerpo1">      
                            <?php
                            if(count($mar) > 0){
                            ?> 
                                <span class="contTituloProceso <?= $mar[1] ?>"><?= $mar[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php 
                            if(count($abr) > 0){
                            ?> 
                                <span class="contTituloProceso <?= $abr[1] ?>"><?= $abr[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                            if(count($may) > 0){
                            ?> 
                                <span class="contTituloProceso <?= $may[1] ?>"><?= $may[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                            if(count($jun) > 0){
                            ?>    
                                <span class="contTituloProceso <?= $jun[1] ?>"><?= $jun[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                            if(count($jul) > 0){
                            ?>    
                                <span class="contTituloProceso <?= $jul[1] ?>"><?= $jul[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                            if(count($ago) > 0){
                            ?>
                                <span class="contTituloProceso <?= $ago[1] ?>"><?= $ago[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                            if(count($sep) > 0){
                            ?>    
                                <span class="contTituloProceso <?= $sep[1] ?>"><?= $sep[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                            if(count($oct) > 0){
                            ?>
                                <span class="contTituloProceso <?= $oct[1] ?>"><?= $oct[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">      
                            <?php
                            if(count($nov) > 0){
                            ?>
                                <span class="contTituloProceso <?= $nov[1] ?>"><?= $nov[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpo1">
                            <?php
                            if(count($dic) > 0){
                            ?>
                                <span class="contTituloProceso <?= $dic[1] ?>"><?= $dic[0] ?></span>
                                <div class="lineaVertical"></div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>

                    <div class="contenedorProcesos centrarCalendario">
                        <div class="cuerpo">
                            <div class="cuerpoFlecha">Ene</div>
                            <div class="puntaFlecha"></div>
                        </div>
		
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Feb</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Mar</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Abr</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">May</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Jun</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Jul</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Ago</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Sep</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Oct</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Nov</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                        <div class="cuerpo1">
                            <div class="cuerpoFlecha">Dic</div>
                            <div class="puntaFlecha"></div>
                        </div>
                        
                    </div>
                    
                    <div class="contenedorLeftInicio1 centrarCalendario">
                        <div class="cuerpoLetrasLeft">
                            <?php 
                            if(count($eneP) > 0){
                            ?>   
                            <div class="lineaVertical1" style="margin-left:-8px;"></div>
                            <div class="circuloProcesos <?= $eneP[1] ?>" style="margin-left:5px;"><?= $eneP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
		
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($febP) > 0){
                            ?>   
                            <div class="lineaVertical1"></div>
                            <div class="circuloProcesos <?= $febP[1] ?>" style="margin-left:8px;"><?= $febP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($marP) > 0){
                            ?>   
                            <div class="lineaVertical1"></div>
                            <div class="circuloProcesos <?= $marP[1] ?>" style="margin-left:8px;"><?= $marP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos" >
                            <?php 
                            if(count($abrP) > 0){
                            ?>   
                            <div class="lineaVertical1"></div>
                            <div class="circuloProcesos <?= $abrP[1] ?>" style="margin-left:8px;"><?= $abrP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($mayP) > 0){
                            ?>   
                            <div class="lineaVertical1"></div>
                            <div class="circuloProcesos <?= $mayP[1] ?>" style="margin-left:8px;"><?= $mayP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($junP) > 0){
                            ?>   
                            <div class="lineaVertical1" style="margin-left:-2px;"></div>
                            <div class="circuloProcesos <?= $junP[1] ?>" style="margin-left:10px;"><?= $junP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($julP) > 0){
                            ?>   
                            <div class="lineaVertical1" style="margin-left:-2px;"></div>
                            <div class="circuloProcesos <?= $julP[1] ?>" style="margin-left:10px;"><?= $julP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($agoP) > 0){
                            ?>   
                            <div class="lineaVertical1" style="margin-left:0px;"></div>
                            <div class="circuloProcesos <?= $agoP[1] ?>" style="margin-left:12px;"><?= $agoP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($sepP) > 0){
                            ?>   
                            <div class="lineaVertical1" style="margin-left:0px;"></div>
                            <div class="circuloProcesos <?= $sepP[1] ?>" style="margin-left:12px;"><?= $sepP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($octP) > 0){
                            ?>   
                            <div class="lineaVertical1" style="margin-left:0px;"></div>
                            <div class="circuloProcesos <?= $octP[1] ?>" style="margin-left:13px;"><?= $octP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($novP) > 0){
                            ?>   
                           <div class="lineaVertical1" style="margin-left:0px;"></div>
                            <div class="circuloProcesos <?= $novP[1] ?>" style="margin-left:13px;"><?= $novP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                        
                        <div class="cuerpoLetrasProcesos">
                            <?php 
                            if(count($dicP) > 0){
                            ?>   
                            <div class="lineaVertical1" style="margin-left:0px;"></div>
                            <div class="circuloProcesos <?= $dicP[1] ?>" style="margin-left:13px;"><?= $dicP[0] ?></div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="contenedorFlex">
                <div class="contenedorVertical">            
                    Entregables
                </div>
                
                <div class="contenedorVertical3">
                    <div class="contenedorCenterInicio backgroundColor6" style="padding-top:15px;">
                        <div style="width:1100px; overflow:hidden; margin:0 auto;">
                        <?php 
                        foreach($listarFabricantes as $row){
                            if($row["idFabricante"] == 1){
                                $balanza = $GLOBALS["domain_root"] . "/adobe/resumen.php?vert=2";
                                $despliegue = $GLOBALS["domain_root"] . "/adobe/despliegue1.php";
                                $optimizacion = $GLOBALS["domain_root"] . "/adobe/resumen.php?vert=3";
                            } else if($row["idFabricante"] == 2){
                                $balanza = $GLOBALS["domain_root"] . "/ibm/resumen.php?vert=2";
                                $despliegue = $GLOBALS["domain_root"] . "/ibm/despliegue1.php";
                                $optimizacion = $GLOBALS["domain_root"] . "/ibm/resumen.php?vert=3";
                            } else if($row["idFabricante"] == 3){
                                $balanza = $GLOBALS["domain_root"] . "/microsoft/resumen.php?vert=2";
                                $despliegue = $GLOBALS["domain_root"] . "/microsoft/despliegue1.php";
                                $optimizacion = $GLOBALS["domain_root"] . "/microsoft/resumen.php?vert=3";
                            } else if($row["idFabricante"] == 4){
                                $balanza = $GLOBALS["domain_root"] . "/oracle/resumen.php?vert=2";
                                $despliegue = $GLOBALS["domain_root"] . "/oracle/despliegue1.php";
                                $optimizacion = $GLOBALS["domain_root"] . "/oracle/resumen.php?vert=3";
                            } else if($row["idFabricante"] == 5){
                                $balanza = "#";
                                $despliegue = $GLOBALS["domain_root"] . "/sap/despliegue1.php";
                                $optimizacion = "#";
                            } else if($row["idFabricante"] == 6){
                                $balanza = $GLOBALS["domain_root"] . "/VMWare/resumen.php?vert=1";
                                $despliegue = $GLOBALS["domain_root"] . "/VMWare/despliegue1.php";
                                $optimizacion = "#";
                            } else if($row["idFabricante"] == 7){
                                $balanza = "#";
                                $despliegue = $GLOBALS["domain_root"] . "/unixIbm/despliegue1.php";
                                $optimizacion = "#";
                            } else if($row["idFabricante"] == 8){
                                $balanza = "#";
                                $despliegue = $GLOBALS["domain_root"] . "/unixOracle/despliegue1.php";
                                $optimizacion = "#";
                            } else if($row["idFabricante"] == 10){
                                $balanza = $GLOBALS["domain_root"] . "/spla/resumen.php?vert=2";
                                $despliegue = $GLOBALS["domain_root"] . "/spla/despliegue1.php";
                                $optimizacion = $GLOBALS["domain_root"] . "/spla/resumen.php?vert=3";
                            }
                            
                        ?>
                            <div class="contFabric text-center pointer bold" style="line-height:20px; margin:0 auto;">
                                <p><?= $row["nombre"] ?></p><br>
                                <a href="<?= $balanza ?>" class="link1">Balanza</a><br>
                                <a href="<?= $despliegue ?>" class="link1">Despliegue</a><br>
                                <a href="<?= $optimizacion ?>" class="link1">Optimización</a><br>
                                <a href="#" class="link1">Adquisición</a><br>
                                <a href="#" class="link1">Negociación</a><br>
                                <a href="#" class="link1">Presupuesto</a>
                            </div>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<script>
    function mostrarFabricante(id){
        if(id === 1){
            location.href = "<?= $GLOBALS["domain_root"] ?>/adobe/";
        } else if(id === 2){
            location.href = "<?= $GLOBALS["domain_root"] ?>/ibm/";
        } else if(id === 3){
            location.href = "<?= $GLOBALS["domain_root"] ?>/microsoft/";
        } else if(id === 4){
            location.href = "<?= $GLOBALS["domain_root"] ?>/oracle/";
        } else if(id === 5){
            location.href = "<?= $GLOBALS["domain_root"] ?>/sap/";
        } else if(id === 6){
            location.href = "<?= $GLOBALS["domain_root"] ?>/VMWare/";
        } else if(id === 7){
            location.href = "<?= $GLOBALS["domain_root"] ?>/unixIbm/";
        } else if(id === 8){
            location.href = "<?= $GLOBALS["domain_root"] ?>/unixOracle/";
        } else if(id === 9){
            
        } else if(id === 10){
            location.href = "<?= $GLOBALS["domain_root"] ?>/spla/";
        }
    }
</script>