        <div style="width:100%; position:relative; text-align:center;">
            <div class="app" style=" display:inline-block; overflow:hidden;">
                <h1 style="font-size:24px;color:#fff;">Bienvenido al Servicio de Manejo de Licenciamiento en la nube </h1>
                <br>
                <!--<div style="overflow:hidden; padding:20px;">-->
                    <img src="imagenes/inicio/logo2.png"  style="margin:0 auto; padding:0;">
                <!--</div>-->
                <div style="width:100%; height:150px; min-height:150px;">&nbsp;</div>
            </div>

            <div class="app" style=" display:inline-block; margin-left:25px; overflow:hidden;   padding-top:30px;padding-bottom: 30px;">
                <div style="overflow:hidden; width:60%; margin:0 auto; background:#F7F7F7; padding:20px; border-radius:10px;">

                    <h1 style="font-size:24px;color:#000; text-align:left;">Inicio de Sesion</h1> <br>

                    <form id="form1" name="form1" method="post" action="autenticar.php">
                        <input type="hidden" name="entrar" id="entrar" value="1" />
                        <input type="text" name="login" id="login" autofocus placeholder="Usuario" />
                        <input type="password" name="contrasena" id="contrasena"  placeholder="Clave" />
                        <div style="margin-top: -15px; margin-bottom: -10px">
                            <img src="../img/spainroundflag.png" alt="spanish" class="img-responsive pointer" style="width:50px; margin-top:10px;" onclick="location.href='index.php?idioma=1';">
                            <img src="../img/usaroundflag.png" alt="english" class="img-responsive pointer" style="width:50px; margin-top:10px;" onclick="location.href='index.php?idioma=2'">
                        </div>
                        <input type="submit" value="Entrar" />
                    </form>
                    <?php if (isset($_GET['error']) == 1) { ?>
                        <div align="center" class="error_prog">Login o Contrase&ntilde;a inv&aacute;lidos</div>
                    <?php } ?>
                    <div style="overflow:hidden; width:90%; margin:0 auto; text-align:left !important;">
                        <p><a href="olvido.php">&iquest;Olvido Contrase&ntilde;a?</a></p>
                        <p><a href="registro.php">Crear Cuenta</a></p>
                    </div>
                </div>
                <div style="width:100%; height:20px; min-height:20px; clear:both;">&nbsp;</div>
            </div>
        </div>
        <div class="copyright" >
            <p>Todos Los Derechos Reservados <?= date("Y") ?> - <a href="https://www.licensingassurance.com"> www.licensingassurance.com </a></p>
        </div>
        <div class="text-center">
            <a href="terminosUso.php" target="_blank">T&eacute;rminos de Uso</a>
        </div>
    </body>
</html>

