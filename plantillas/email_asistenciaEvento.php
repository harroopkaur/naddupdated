<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Correo</title>
        <style type="text/css">

            td img {display: block;}td img {display: block;}
        </style>
    </head>

    <body>
        <table width="800" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="https://vieja-webtool.licensingassurance.com/plantillas/cabecera_mail.jpg" /></td>
            </tr>
            <tr>
                <td>
                    <table width="95%" align="center">
                        <tr>
                            <td colspan="2"><p>La siguiente persona ha confirmado su asistencia al evento:</p>
                                    <p><strong>Nombre:</strong> #NOMBRE#</p>
                                    <p><strong>Correo:</strong> #CORREO#</p>
                                    <p><strong>Tel&eacute;fono:</strong> #TELEFONO#</p>
                                    <p><strong>Pa&iacute;s:</strong> #PAIS#</p>
                                    <p><strong>Empresa:</strong> #EMPRESA#</p>
                            </td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><img src="https://vieja-webtool.licensingassurance.com/plantillas/pie_mail.jpg" /></td>
            </tr>
        </table>
    </body>
</html>