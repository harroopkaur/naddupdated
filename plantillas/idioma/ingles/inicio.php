<div style="width:98%; padding:10px; overflow:hidden;">
    <style>
        .contPrincipalInicio{
            width:1000px;  
            margin:0 auto; 
            overflow:hidden; 
        }
        
        .contImagInicio{
            background-color:#B3EDF7; 
            width:230px; 
            height:220px; 
            -moz-border-radius: 10px; 
            -webkit-border-radius: 10px; 
            border-radius: 10px; 
            text-align:center; 
        }
    </style>
    
    <div>
        <p class="titulo1 colorAzulWebtool">Welcome to the Cloud Licensing Management Service</p>
    </div>
    
    <br>
    <div class="colorAzulWebtool contPrincipalInicio">
        <span class="colorAzulWebtool bold float-lt" style="width:70px;">Name:&nbsp;&nbsp;</span>
        <p class="colorAzulWebtool float-lt"><?= $_SESSION['client_nombre'].' '.$_SESSION['client_apellido'] ?></p><br style="clear:both;">
        <span class="colorAzulWebtool bold float-lt" style="width:70px;">Company:&nbsp;&nbsp;</span>
        <p class="float-lt"><?= $_SESSION['client_empresa'] ?></p>
    </div>
    
    <br>
    <div class="contPrincipalInicio text-center">
        <div class="contImagInicio float-lt pointer" onclick="location.href='dashboard.php';" title="Muestra el Dashboard">
            <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/dashboard.png" style="margin-top:30px; height: 100px; width:auto;">
            <br><br>
            <p class="colorAzulWebtool" style="font-size:35px;">DASHBOARD</p>
        </div> 
        
        <div class="contImagInicio float-lt pointer" style="margin-left: 25px;" onclick="location.href='ganttInicio.php';" title="Muestra el resumen de Fechas Clave">
            <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/imgGANTT.png" style="margin-top:30px; height: 100px; width:auto;">
            <br><br>
            <p class="colorAzulWebtool" style="font-size:35px;">GANTT</p>
        </div>  
    
        <div class="contImagInicio float-lt pointer" style="margin-left: 25px;"  onclick="location.href='inventario.php';" title="Actualizacion y/o Detalle de Despliegue y Balanzas">
            <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/imgInventario.png" style="margin-top:30px; height: 100px; width:auto;">
            <br><br>
            <p class="colorAzulWebtool" style="font-size:35px;">INVENTORY</p>
        </div>  
    
        <div class="contImagInicio float-rt pointer"  onclick="location.href='sam/';" title="Muestra Repositorio de Compras, Invetario y Procesos">
            <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/imgSAM.png" style="margin-top:30px; height: 100px; width:auto;">
            <br><br>
            <p class="colorAzulWebtool" style="font-size:35px;">SAM</p>
        </div>  
        
        <br style="clear:both">
        <span class="colorAzulWebtool bold float-lt">Last Entry:&nbsp;&nbsp;</span><p class="colorAzulWebtool float-lt"><?= $fecha ?></p>
    </div>
</div>