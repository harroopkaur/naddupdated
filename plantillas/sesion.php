<?php
// Verificar inicio sesion
if(!$_SESSION['client_autorizado']) {
	echo '<script language="javascript" type="text/javascript">';
	echo '	alert("¡Usted debe Iniciar Sesión!");';
	echo '	location.href="'.$GLOBALS['domain_root'].'/index.php";';
	echo '</script>';
}

// Verificar tiempo de sesion
$tiempo_sesion = time() - $_SESSION['client_tiempo'];
if($tiempo_sesion > $TIEMPO_MAXIMO_SESION) {
	echo '<script language="javascript" type="text/javascript">';
	echo '	alert("¡Usted pasó mucho tiempo inactivo!");';
	echo '	location.href="'.$GLOBALS['domain_root'].'/plantillas/salida.php";';
	echo '</script>';
} else {
	$_SESSION['client_tiempo'] = time();
}
?>
