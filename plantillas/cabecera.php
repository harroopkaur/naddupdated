<header id="cabecera" style="position:fixed; z-index:1200;">
    <div style="float:left; position:relative;">
        <img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/logo3.png" style=" width:300px; margin-left: 5px;">
        <div style="margin-top:0px; width:150px; overflow:hidden;">
            <img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/ITIL_logo.png" style=" width:50px; margin-top:-20px; margin-left:120px; position:fixed;">
            <img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/IAITAM_logo.png" style=" width:25px; margin-top:-20px; margin-left:160px; position:fixed;">
            <img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/ISO_logo.png" style=" width:25px; margin-top:-20px; margin-left:200px; position:fixed;">
        </div>
    </div>
    <div style="float:left; margin:25px;">
        <div style="font-size:20px; color:#fff; font-weight:bold; margin-left:20px; margin-right:10px; float:left;">Effective</div>
        <div style="font-size:20px; color:#fff; font-weight:bold; margin-left:10px; margin-right:10px; float:left;">Low Cost</div>
        <div style="font-size:20px; color:#fff; font-weight:bold; margin-left:10px; margin-right:10px; float:left;">Convenient</div>
    </div>
    
    <div style="float:right; margin:10px; color:#fff; margin-right:25px; font-weight:bold;" >
        <p><?= "Level of contracted service: " . $_SESSION["nivelServicio"] ?></p>
        
        <div style="float:right; margin:0px; cursor:pointer; margin-top:2px; margin-left:10px;" onClick="showhide('verop');"><img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/flecha.png"></div>
        <p class="uppercase" style="float:right;"><?=$_SESSION['client_nombre'].' '.$_SESSION['client_apellido'].' & '.$_SESSION['client_empresa']?></p>
    </div>
</header>
<div id="verop" style=" display:none;position:fixed; right:20px; top:50px; width:200px; background:#AAA; border-radius:4px; padding:10px; overflow:hidden; z-index:1300;">
    <div style="margin:5px;"><a href="<?=$GLOBALS['domain_root']?>/inicio.php">Inicio</a></div>
    <div style="margin:5px;"><a href="<?=$GLOBALS['domain_root']?>/plantillas/salida.php">Cerrar Sesi&oacute;n</a></div>
</div>

<br>

<div class="botonDesplegableHead">
    <div class="btn-desplegable">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
 
    <div class="linksBotones">
        <a href="<?= $GLOBALS["domain_root"] ?>/ayuda/">Centro de Ayuda</a>
        <a href="<?= $GLOBALS["domain_root"] ?>/certificados/">Certificado de Cumplimiento</a>
        <a href="<?= $GLOBALS["domain_root"] ?>/cotizacion/licensing/">Cotizaci&oacute;n (Nuestros Servicios)</a>
        <a href="<?= $GLOBALS["domain_root"] ?>/cotizacion/proveedor/">Cotizaci&oacute;n (Proveedor de Software)</a>
    </div>
</div>

<br><br><br>