<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Correo</title>
        <style>
            .tablap {     
                font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
                font-size: 12px; 
                margin: 0 auto;   
                width: 99%; 
                text-align: left;    
                border-collapse: collapse; 
            }

            .tablap th {     
                font-size: 13px;     
                font-weight: normal;     
                padding: 8px;     
                background: #aabcfe;
                border-top: 4px solid #aabcfe;    
                border-bottom: 1px solid #fff; 
                color: #039; 
                vertical-align: middle;
            }

            .tablap td {    
                padding: 8px;     
                background: #e8edff;     
                border-bottom: 1px solid #fff;
                color: #669;    
                border-top: 1px solid transparent; 
                vertical-align: middle;
            }
        </style>
    </head>

    <body>
        #TABLA#
    </body>
</html>