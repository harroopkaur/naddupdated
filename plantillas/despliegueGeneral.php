<style>
    .input_archivo{ width:325px; float:left;position:relative;}
    .contenedor_archivo{ width:870px; margin:0 auto; }
    .contenedor_archivo1{ width:590px; overflow:hidden; margin:0 auto;}
</style>
<input type="hidden" id="nombreArchivo">
<div style="width:98%; padding:20px; overflow:hidden;">                                
    <div class="contenedorFlex contBtnGeneralDespliegue">
        <div class="contBtnDespliegue">
            <div class="botonDespliegueAct" onclick="desplegarCompleto()" title="Herramienta LA Tool AD">Despliegue Completo</div>
            <div class="divDespliegue">Seleccionar para un levantamiento completo de todo su universo</div>
            
            <br>
            
            <div class="botonDespliegueAct" onclick="desplegarIncremCompleto()" title="Herramienta LA Tool AD">Incrementar Despliegue Completo</div>
            <div class="divDespliegue">Incrementar el alcance en relación a su anterior levantamiento</div>
            
            <?php if($fabricante == 3){?>
            <br>
            
            <div class="botonDespliegueAct" onclick="location.href='despliegueOtros.php';">Otros Despliegues</div>
            <div class="divDespliegue">Despliegue Realizado con Otras Herramientas</div>
            <?php } ?>
        </div>
        
        <div class="contDespliegueCentral">
            <h1 class="textog negro" style="text-align:center; line-height:35px; <?php if($opcionDespliegue != ""){ echo "display:none"; }?>" id="principal">Despliegue</h1>
            <h1 class="textog negro" style="text-align:center; line-height:35px; <?php if($opcionDespliegue == "" || $opcionDespliegue != "completo"){ echo "display:none"; }?>" id="completo">Despliegue Completo</h1>
            <h1 class="textog negro" style="text-align:center; line-height:35px; <?php if($opcionDespliegue == "" || $opcionDespliegue != "segmentado"){ echo "display:none"; }?>" id="segmentado">Despliegue Segmentado</h1>
            <h1 class="textog negro" style="text-align:center; line-height:35px; <?php if($opcionDespliegue == "" || $opcionDespliegue != "incremCompleto"){ echo "display:none"; }?>" id="incremCompleto">Incremento Despliegue Completo</h1>
            <h1 class="textog negro" style="text-align:center; line-height:35px; <?php if($opcionDespliegue == "" || $opcionDespliegue != "incremSegmentado"){ echo "display:none"; }?>" id="incremSegmentado">Incremento Despliegue Segmentado</h1>
            <h1 class="bold link1 pointer" style="text-align:center; font-size:20px;" id="ayudaDespliegue">Ayuda</h1>
            <br>
            <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/disenoTecnicoLATool.jpg" id="imgDiagrama" class="pointer" style="width:100%; height:auto;">
        </div>
        
        <div class="contBtnDespliegue">
            <div class="botonDespliegueAct" onclick="desplegarSegmentado()" title="Herramienta LA Tool Local">Despliegue Segmentado</div>
            <div class="divDespliegue">Seleccionar para un levantamiento segmentado de su universo</div>
            
            <br>
            
            <div class="botonDespliegueAct" onclick="desplegarIncremSegmentado()" title="Herramienta LA Tool Local">Incrementar Despliegue Segmentado</div>
            <div class="divDespliegue">Incrementar el alcance en relación a su anterior levantamiento</div>
        </div>
    </div>

    <br>
    
    <div id="despliegueCompleto" style="margin-right:20px; <?php if($opcionDespliegue == "" || $opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado"){ echo 'display:none'; }?>">
        <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] . "/" . $general->ToolAD ?>"><?= $general->ToolAD ?></a></p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
        <br>
        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">LAD_Output</legend>
            
            <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="despliegue1.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionCompleto" type="hidden" value="<?= $opcionDespliegue ?>" >
                <!--<div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo[]" id="archivo" accept=".rar" <?php //if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                        <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <!--<input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />-->
                        <!--<input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                    </div>
                </div>-->
                <div class="contenedor_archivo1">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo[]" id="archivo" accept=".rar" <?php if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                    </div>
                    
                    <div class="contenedor_boton_archivo">
                        <input type="button" id="boton1" class="botones_m5 float-lt" value="Buscar" onclick="$('#archivo').click();">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>

            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
            <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>
            <div class="text-center bold" style="color: #2d6ba4;">
                <?php if($_SESSION["archivosCargados"] != ""){ echo $_SESSION["archivosCargados"]; } ?>
            </div>
            <?php if($exito==1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }else{ ?>
            <?php }
            if($error== -1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error== -2){ ?>
                <div class="error_archivo">Contraseña Incorrecta</div>
                <script>
                    $.alert.open('warning', 'Alerta', 'Contraseña Incorrecta', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                <div class="error_archivo"><?= $general->getMensajeRar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==2 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                  $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                      $("body").scrollTop(1000);
                  });
                </script>
            <?php }
            else if($error == 3 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 4 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 5 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'});
                </script>
            <?php } 
            else if($error == 6 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 7 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 8 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 9 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 10 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 11 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 12 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 13 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 14 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 15 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 16 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 17 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } 
            ?>
        </fieldset>

        <br />
        
        <div id="LAE-completo">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p><br />
            <fieldset class="fieldset">
                <legend style=" font-weight:bold; margin-left:15px;">LAE_Output</legend>

                <form enctype="multipart/form-data" name="form_e2" id="form_e2" method="post" action="despliegue2.php" >
                    <input name="insertar" id="insertar" type="hidden" value="1" >
                    <input name="opcionDespliegue" id="LAEopcionCompleto" type="hidden" value="<?= $opcionDespliegue ?>" >
                    <div class="contenedor_archivo">
                        <!--<div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file1" value="Nombre del Archivo...">
                            <input type="file" class="archivo"  name="archivo[]" id="archivo1" accept=".csv" <?php if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                            <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar">
                            <input type="hidden" id="fechaDespliegueCompleto" name="fechaDespliegue">
                            <input type="hidden" id="eliminarEquipoLAECom" name="eliminarEquipoLAE">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="eliminarEquipos" type="button" id="eliminarEquiposCom" value="Eliminar Equipos" class="botones_m5" />
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="mostrarFechaDespliegue('completo');" class="botones_m5" />
                        </div>-->
                        
                        <div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file1" value="Nombre del Archivo...">
                            <input type="file" class="archivo"  name="archivo[]" id="archivo1" accept=".csv" <?php if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                            <input type="hidden" id="fechaDespliegueCompleto" name="fechaDespliegue">
                            <input type="hidden" id="eliminarEquipoLAECom" name="eliminarEquipoLAE">
                        </div>
                        
                        <div class="contenedor_boton_archivo">
                            <input type="button" id="boton1" class="botones_m5 float-lt" value="Buscar" onclick="$('#archivo1').click();">
                        </div>
                     
                        <div class="contenedor_boton_archivo">
                            <input name="eliminarEquipos" type="button" id="eliminarEquiposCom" value="Eliminar Equipos" class="botones_m5" />
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Sustituir LAE" onclick="$('#incremArchivo').val(0); mostrarFechaDespliegue('completo');" class="botones_m5" />
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input type="hidden" id="incremArchivo" name="incremArchivo">
                            <input name="insertar" type="button" id="insertar" value="Incrementar" onclick="$('#incremArchivo').val(1); mostrarFechaDespliegue('completo');" class="botones_m5" />
                        </div>
                    </div>
                </form>

            
                <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Sustituir LAE"</p>
                <div id="cargando2" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>
                <div class="text-center bold" style="color: #2d6ba4;">
                    <?php if($_SESSION["archivosCargadosLAE"] != ""){ echo $_SESSION["archivosCargadosLAE"]; } ?>
                </div>
                <?php if($exito2==1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }else{ ?>
                <?php }
                if($error2== -1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2==1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2==2 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2==3 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2 == 5 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?=$consolidado->error?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?=$consolidado->error?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php } else if($error2 == 6 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getSeparadorLAE($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getSeparadorLAE($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php } else if($error2 == 15 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
                ?>
                    <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php
                }?>
            </fieldset>
            <br />
        </div>
        
        <div id="subsidiaria-completo">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Descargar <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/reportes/generarAsignacion.php?fabricante=<?= $fabricante ?>" target="_blank">Formulario Asignacion</a>.</p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">10.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">Subsidiaria.csv</strong>".</p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Nota: </span>Favor tomar en cuenta que la Asignacion del formulario debe ser idéntica a la Asignacion del Modulo SAM - Contratos - Detalle de Compras</p><br />
            <fieldset class="fieldset">
                <legend style=" font-weight:bold; margin-left:15px;">Subsidiaria</legend>

                <form enctype="multipart/form-data" name="form_e3" id="form_e3" method="post" action="despliegue3.php" >
                    <input name="insertar" id="insertar" type="hidden" value="1" >
                    <input name="opcionDespliegue" id="subsidiariaOpcionCompleto" type="hidden" value="<?= $opcionDespliegue ?>" >
                    <!--<div class="contenedor_archivo">
                        <div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file4" value="Nombre del Archivo...">
                            <input type="file" class="archivo"  name="archivo" id="archivo4" accept=".csv">   
                            <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando3');document.getElementById('form_e3').submit();" class="botones_m5" />
                        </div>
                    </div>-->
                    
                    <div class="contenedor_archivo1">
                        <div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file4" value="Nombre del Archivo...">
                            <input type="file" class="archivo"  name="archivo" id="archivo4" accept=".csv">   
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input type="button" id="boton1" class="botones_m5 float-lt" value="Buscar" onclick="$('#archivo4').click();">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando3');document.getElementById('form_e3').submit();" class="botones_m5" />
                        </div>
                    </div>
                </form>

            
                <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
                <div id="cargando3" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>
                <div class="text-center bold" style="color: #2d6ba4;">
                    <?php if($_SESSION["archivosCargadosSubsidiaria"] != ""){ echo $_SESSION["archivosCargadosSubsidiaria"]; } ?>
                </div>
                <?php if($exito3==1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                    <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>" . $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php }else{ ?>

                <?php }
                if($error3== -1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php }
                else if($error3==1 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php } else if($error3==2 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php } else if($error3 == 15 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
                ?>
                    <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php
                } else if($error3 == 18 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
                ?>
                    <div class="error_archivo"><?= $general->getMensajeAsignacion($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeAsignacion($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php
                } else if($error3 == 19 && ($opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto")){
                ?>
                    <div class="error_archivo"><?= $general->getSeparadorAsignacion($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getSeparadorAsignacion($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php
                }
                ?>
            </fieldset>
            <br />
        </div>

        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='compras.php';">Compras</div></div>
    </div>

    <div id="despliegueSegmentado" style="margin-right:20px; <?php if($opcionDespliegue == "" || $opcionDespliegue == "completo" || $opcionDespliegue == "incremCompleto"){ echo 'display:none'; }?>">
        <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS["domain_root"] . "/" . $general->ToolLocal ?>"><?= $general->ToolLocal ?></a></p><br />
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Ejecutar haciendo click sobre el archivo llamado "<strong style="font-weight:bold;">Renombrar</strong>"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Configurar el rango de IPs para ejecutar la herramienta.  Puede configurarlo de dos formas</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.1-&nbsp;</span>Generar lista por rango de IPs:</p><br>
        <p style="margin-left:30px;">- Se generar&aacute; un archivo llamado "Equipos.zip", guarda ese archivo en el escritorio, descomprimirlo y reemplazar el archivo Equipo.txt guardado en el paso 3.</p>
        <p style="float:left; margin-left:30px;">Rango de Ips, Desde: </p><input type="text" id="desdeIp" name="desdeIp" style="float:left"><p style="float:left"> - Hasta: </p><input type="text" id="hastaIp" name="hastaIp" style="float:left"><input name="actualizIp" type="button" id="actualizIp" value="Generar Archivo" onclick="actualizIp()" class="botones_m5" style="float:left"><br style="clear:both;"><br>
        <p style="margin-left:30px;">- Confirmar que el archivo nuevo Equipos.txt fue reemplazado correspondiente abriendo el mismo y confirmar que se encuentra el rango de IP s citado y no se encuentra solamente la máquina "<span class="bold">Localhost</span>"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.2-&nbsp;</span>Generar lista personalizada de IPs:</p><br>
        <p style="float:left; margin-left:30px;">- Favor colocar los nombres de equipos uno debajo de otro sin puntos ni comas</p><input name="listaPersonalizada" type="button" value="Generar lista personalizada" class="botones_m5" onclick="listaPersonalizada();"/><br style="clear:both"><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>En el paso anterior se generará un archivo llamado Equipos.zip, descomprimir el mismo y guardarlo en la misma carpeta en la que se encuentran los componentes de la herramienta reemplazando el archivo original Equipos.txt por el nuevo.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.1 -&nbsp;</span>Confirmar que el archivo nuevo Equipos.txt fue reemplazado correspondiente abriendo el mismo y confirmar que se encuentra el rango de IP s citado y no se encuentra solamente la máquina "Localhost"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Click derecho sobre el archivo "<strong style="font-weight:bold;">LA_Tool.vbe</strong>". Seleccionar la opci&oacute;n "<strong style="font-weight:bold;">Open with Command Prompt</strong>" o "<strong>Ejecutar con Simbolo de Sistema</strong>"</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>Se  iniciar&aacute; una ventana de Command Prompt que muestra el progreso de la ejecuci&oacute;n  del escaneo de equipos. Esta ventana se cerrar&aacute; autom&aacute;ticamente una vez  finalice el escaneo.  Esto puede  demorar unos minutos en finalizar, dependiendo de la cantidad de m&aacute;quinas  conectadas a la red y de la velocidad de conexi&oacute;n</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Se generar&aacute; un (1) archivo llamado <strong style="font-weight:bold;">"LAE_Output.csv"</strong> y una (1) carpeta llamada <strong style="font-weight:bold;">Resultados</strong>, dentro de la misma se encontrar&aacute; un archivo llamado <strong style="font-weight:bold;">"LAD_Output[fechaejecucion].rar"</strong> con la fecha de ejecuci&oacute;n de la herramienta.</p><br>
        <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAD_Output.rar</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p>
        <br>
        
        <fieldset class="fieldset">
            <legend style=" font-weight:bold; margin-left:15px;">LAD_Output</legend>
        
            <form enctype="multipart/form-data" name="form_seg" id="form_seg" method="post" action="despliegue1.php" >
                <input name="insertar" id="insertar" type="hidden" value="1" >
                <input name="opcionDespliegue" id="opcionSegmentado" type="hidden" value="<?= $opcionDespliegue ?>" >
                <!--<div class="contenedor_archivo">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file2" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo[]" id="archivo2" accept=".rar" <?php //if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                        <input type="button" id="boton1" class="botones_m5" value="Buscar">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargandoSegmentado');document.getElementById('form_seg').submit();" class="botones_m5" />
                    </div>
                </div>-->
                
                <div class="contenedor_archivo1">
                    <div class="input_archivo">
                        <input type="text" class="url_file" name="url_file" disabled id="url_file2" value="Nombre del Archivo...">
                        <input type="file" class="archivo" name="archivo[]" id="archivo2" accept=".rar" <?php if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                    </div>
                    <div class="contendor_boton_archivo">
                        <input type="button" id="boton1" class="botones_m5" value="Buscar" onclick="$('#archivo2').click();">
                    </div>
                    <div class="contenedor_boton_archivo">
                        <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargandoSegmentado');document.getElementById('form_seg').submit();" class="botones_m5" />
                    </div>
                </div>
            </form>
            <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>

            <div id="cargandoSegmentado" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>
            <div class="text-center bold" style="color: #2d6ba4;">
                <?php if($_SESSION["archivosCargados"] != ""){ echo $_SESSION["archivosCargados"]; } ?>
            </div>
            <?php if($exito==1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }else{ ?>
            <?php }
            if($error== -1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                <div class="error_archivo"><?= $general->getMensajeRar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error==2 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php }
            else if($error == 3 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabAddRemove($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 4 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabEscaneo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 5 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                <div class="error_archivo"><?=$consolidado->error?></div>
                <script>
                    $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php } 
            else if($error == 6 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabProcesadores($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 7 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabTipoEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 8 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeFaltanArchivos($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            }
            else if($error == 9 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabUsuarioEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 10 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorAddRemove($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 11 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorEscaneo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 12 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorProcesadores($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 13 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorTipoEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 14 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorUsuarioEquipo($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 15 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 16 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getMensajeSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } else if($error == 17 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
            ?>
                <div class="error_archivo"><?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?></div>
                <script>
                    $.alert.open('warning', 'Alert', '<?= $general->getSeparadorSQL($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                        $("body").scrollTop(1000);
                    });
                </script>
            <?php
            } 
            ?>
        </fieldset>

        <br />
        
        <div id="LAE-segmentado">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">10.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">LAE_Output.csv</strong>".<strong style="font-weight:bold">&nbsp;Nota:</strong> El almacenamiento de los datos puede tardar, por favor no cierre esta ventana y espere el mensaje "<strong style="font-weight:bold">Archivo cargado con &eacute;xito</strong>"</p><br />

            <fieldset class="fieldset">
                <legend style=" font-weight:bold; margin-left:15px;">LAE_Output</legend>

                <form enctype="multipart/form-data" name="form_seg2" id="form_seg2" method="post" action="despliegue2.php" >
                    <input name="insertar" id="insertar" type="hidden" value="1" >
                    <input name="opcionDespliegue" id="LAEopcionSegmentado" type="hidden" value="<?= $opcionDespliegue ?>" >
                    <div class="contenedor_archivo">
                        <!--<div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file3" value="Nombre del Archivo...">
                            <input type="file" class="archivo" name="archivo[]" id="archivo3" accept=".csv" <?php //if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                            <input type="button" id="boton1" class="botones_m5" value="Buscar">
                            <input type="hidden" id="fechaDespliegueSegmentado" name="fechaDespliegue">
                            <input type="hidden" id="eliminarEquipoLAESeg" name="eliminarEquipoLAE">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="eliminarEquipos" type="button" id="eliminarEquiposSeg" value="Eliminar Equipos" class="botones_m5" />
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="mostrarFechaDespliegue('segmentado');" class="botones_m5"/>
                        </div>-->
                        
                        <div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file3" value="Nombre del Archivo...">
                            <input type="file" class="archivo" name="archivo[]" id="archivo3" accept=".csv" <?php if ($opcionDespliegue == "incremCompleto" || $opcionDespliegue == "incremSegmentado"){ echo "multiple"; } ?>>
                            <input type="hidden" id="fechaDespliegueSegmentado" name="fechaDespliegue">
                            <input type="hidden" id="eliminarEquipoLAESeg" name="eliminarEquipoLAE">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input type="button" id="boton1" class="botones_m5" value="Buscar" onclick="$('#archivo3').click();">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="eliminarEquipos" type="button" id="eliminarEquiposSeg" value="Eliminar Equipos" class="botones_m5" />
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Sustituir LAE" onclick="$('#incremArchivoSeg').val(0); mostrarFechaDespliegue('segmentado');" class="botones_m5"/>
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input type="hidden" id="incremArchivoSeg" name="incremArchivo">
                            <input name="insertar" type="button" id="insertar" value="Incrementar" onclick="$('#incremArchivoSeg').val(1); mostrarFechaDespliegue('segmentado');" class="botones_m5"/>
                        </div>
                    </div>
                </form>

                <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Sustituir LAE"</p>
                <div id="cargandoSegmentado2" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>
                <div class="text-center bold" style="color: #2d6ba4;">
                    <?php if($_SESSION["archivosCargadosLAE"] != ""){ echo $_SESSION["archivosCargadosLAE"]; } ?>
                </div>
                <?php if($exito2==1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div>
                        <?php if($_SESSION["archivosCargados"] != ""){ echo $_SESSION["archivosCargados"]; } ?>
                    </div>
                    <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                    <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>" . $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }else{ ?>

                <?php }
                if($error2== -1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2==1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2==2 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2==3 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabLAE($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php }
                else if($error2 == 5 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?=$consolidado->error?></div>
                    <script>
                        $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php } else if($error2 == 6 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getSeparadorLAE($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getSeparadorLAE($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php } else if($error2 == 15 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
                ?>
                    <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php
                } ?>
            </fieldset>
            <br />
        </div>
        
        <div id="subsidiaria-segmentado">
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">11.-&nbsp;</span>Descargar <a class="link1" href="<?= $GLOBALS["domain_root"] ?>/reportes/generarAsignacion.php?fabricante=<?= $fabricante ?>" target="_blank">Formulario Asignacion</a>.</p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">12.-&nbsp;</span>Subir el archivo "<strong style="font-weight:bold;">Subsidiaria.csv</strong>".</p><br />
            <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">Nota: </span>Favor tomar en cuenta que la Asignación del formulario debe ser idéntica a la Asignación del Modulo SAM - Contratos - Detalle de Compras</p><br />
            <fieldset class="fieldset">
                <legend style=" font-weight:bold; margin-left:15px;">Subsidiaria</legend>

                <form enctype="multipart/form-data" name="form_seg3" id="form_seg3" method="post" action="despliegue3.php" >
                    <input name="insertar" id="insertar" type="hidden" value="1" >
                    <input name="opcionDespliegue" id="subsidiariaOpcionSegmentado" type="hidden" value="<?= $opcionDespliegue ?>" >
                    <!--<div class="contenedor_archivo">
                        <div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file5" value="Nombre del Archivo...">
                            <input type="file" class="archivo"  name="archivo" id="archivo5" accept=".csv">                            
                            <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargandoSegmentado3');document.getElementById('form_seg3').submit();" class="botones_m5" />
                        </div>
                    </div>-->
                    
                    <div class="contenedor_archivo1">
                        <div class="input_archivo">
                            <input type="text" class="url_file" name="url_file" disabled id="url_file5" value="Nombre del Archivo...">
                            <input type="file" class="archivo"  name="archivo" id="archivo5" accept=".csv">                            
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input type="button" id="boton1" class="botones_m5 float-rt" value="Buscar" onclick="$('#archivo5').click();">
                        </div>
                        <div class="contenedor_boton_archivo">
                            <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargandoSegmentado3');document.getElementById('form_seg3').submit();" class="botones_m5" />
                        </div>
                    </div>
                </form>

            
                <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
                <div id="cargandoSegmentado3" style="display:none;text-align:center; color:#2D6BA4; margin:5px;">Procesando...</div>
                <div class="text-center bold" style="color: #2d6ba4;">
                    <?php if($_SESSION["archivosCargadosSubsidiaria"] != ""){ echo $_SESSION["archivosCargadosSubsidiaria"]; } ?>
                </div>
                <?php if($exito3==1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
                    <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>" . $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php }else{ ?>

                <?php }
                if($error3== -1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getAlertNoCargado($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertNoCargado($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php }
                else if($error3==1 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeCsv($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeCsv($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php } else if($error3==2 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){ ?>
                    <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(10000);
                        });
                    </script>
                <?php } else if($error3 == 15 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
                ?>
                    <div class="error_archivo"><?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getAlertArchCargado($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php
                } else if($error3 == 18 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
                ?>
                    <div class="error_archivo"><?= $general->getMensajeAsignacion($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getMensajeAsignacion($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php
                } else if($error3 == 19 && ($opcionDespliegue == "segmentado" || $opcionDespliegue == "incremSegmentado")){
                ?>
                    <div class="error_archivo"><?= $general->getSeparadorAsignacion($_SESSION["idioma"], $nombre_imagen) ?></div>
                    <script>
                        $.alert.open('warning', 'Alert', '<?= $general->getSeparadorAsignacion($_SESSION["idioma"], $nombre_imagen) ?>', {'Aceptar' : 'Aceptar'}, function(){
                            $("body").scrollTop(1000);
                        });
                    </script>
                <?php
                }
                ?>
            </fieldset>
            <br />
        </div>

        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='compras.php';">Compras</div></div>
    </div>
</div>

<form id="formEliminarEquiLAE" name="formEliminarEquiLAE" method="post">
    <div class="modal-personal" id="modal-eliminarLAE">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Eliminar <p style="color:#06B6FF; display:inline">Equipos del LAE</p></h1>
        </div>
        <div class="modal-personal-body">
            <input type="hidden" id="tokenEliminarEquLAE" name="token">
            <input type="file" id="equipElim" name="equipElim" class="hide">
            <input type="hidden" id="fab" name="fab" value="<?= $fabricante ?>">
            <table style="width:60%; margin-top:10px;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
                <thead>
                    <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                        <th  align="center" valign="middle" >Equipo</th>
                    </tr>
                </thead>
                <tbody id="bodyTable">
                </tbody>
            </table>           
        </div>
        <div class="modal-personal-footer">
            <div class="boton1 botones_m2" style="float:right;" id="salirBuscarEliminar">Salir</div>
            <div class="boton1 botones_m2" id="cancelarEliminar" style="float:right;">Cancelar</div>
            <div class="boton1 botones_m2" id="buscarEliminar" style="float:right;">Buscar</div>
        </div>
    </div>
</form>

<?php 
include_once($GLOBALS["app_root"] . "/plantillas/modalAyuda.php");
include_once($GLOBALS["app_root"] . "/plantillas/modalFechaDespliegue.php");
include_once($GLOBALS["app_root"] . "/plantillas/modalDiagrama.php");
include_once($GLOBALS["app_root"] . "/plantillas/modalListaPersonalizada.php");
?>

<script>
    $(document).ready(function(){
        $("#fechaD").datepicker();
        
        $("#archivo").change(function(e){
            $(".exito_archivo").hide();
            $(".error_archivo").hide();
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
        
        $("#archivo1").change(function(e){
            $("#nombreArchivo").val("#url_file1");
            addArchivo(e);
        });
        
        $("#archivo2").change(function(e){
            $(".exito_archivo").hide();
            $(".error_archivo").hide();
            $("#nombreArchivo").val("#url_file2");
            addArchivo(e);
        });
        
        $("#archivo3").change(function(e){
            $("#nombreArchivo").val("#url_file3");
            addArchivo(e);
        });
        
        $("#archivo4").change(function(e){
            $("#nombreArchivo").val("#url_file4");
            addArchivo(e);
        });
        
        $("#archivo5").change(function(e){
            $("#nombreArchivo").val("#url_file4");
            addArchivo(e);
        });
        
        /*inicio funcion del modal ayuda despliegue*/
        $("#ayudaDespliegue").click(function(){
            $("#fondo1").show();
            $("#modal-ayudaDespliegue").show();
        });
        
        $("#salirAyudaDespliegue").click(function(){
            $("#fondo1").hide();
            $("#modal-ayudaDespliegue").hide();
        });
        //fin funcion del modadl ayuda despliegue
        
        //inicio fecha despliegue
        $("#salirFechaDespliegue").click(function(){
            $("#fechaOpcion").val("");
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        
        $("#procesarLAE_Output").click(function(){
            if($("#fechaD").val() === ""){
                $.alert.open('warning', 'Alert', "Debe seleccionar la fecha del despliegue", {"Aceptar" : "Aceptar"});
                return false;
            }
       
            if($("#fechaOpcion").val() === "completo"){
                showhide('cargando2');                
                $("#fechaDespliegueCompleto").val($("#fechaD").val());
                document.getElementById('form_e2').submit();
            } else if($("#fechaOpcion").val() === "segmentado"){
                showhide('cargandoSegmentado2');
                $("#fechaDespliegueSegmentado").val($("#fechaD").val());
                document.getElementById('form_seg2').submit();
            }
            $("#fondo1").hide();
            $("#modal-fechaDespliegue").hide();
        });
        //fin fecha despliegue
        
        //inicio mostrar diagrama
        $("#imgDiagrama").click(function(){
            $("#fondo1").show();
            $("#modal-diagrama").show();
        });
        
        $("#diagrama").click(function(){
            $("#fondo1").hide();
            $("#modal-diagrama").hide();
        });
        //fin mostrar diagrama
        
        //inicio modal eliminar equipos LAE
        $("#eliminarEquiposCom, #eliminarEquiposSeg").click(function(){
            $("#fondo1").show();
            $("#modal-eliminarLAE").show();
        });
        
        $("#salirBuscarEliminar").click(function(){
            $("#fondo1").hide();
            $("#modal-eliminarLAE").hide();
        });
        
        $("#cancelarEliminar").click(function(){
            $("#salirBuscarEliminar").click();
            var nombreArchivo = null;
            if($("#opcionCompleto").val() === "completo" || $("#LAEopcionCompleto").val() === "completo"
            || $("#opcionSegmentado").val() === "completo" || $("#LAEopcionSegmentado").val() === "completo"
            || $("#opcionCompleto").val() === "incremCompleto" || $("#LAEopcionCompleto").val() === "incremCompleto"
                        || $("#opcionSegmentado").val() === "incremCompleto" || $("#LAEopcionSegmentado").val() === "incremCompleto"){
                nombreArchivo = $("#eliminarEquipoLAECom").val();
            } else {
                nombreArchivo = $("#eliminarEquipoLAESeg").val();
            }
            $.post("<?= $GLOBALS['domain_root'] ?>/ajax/deleteFileEquiposLAE.php", { equipElim : nombreArchivo, 
            fab : $("#fab").val(), token : localStorage.licensingassuranceToken }, function(data){
                if(data[0].result === 0){
                    $.alert.open("warning", "Alerta", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"});
                } else if(data[0].result === 1){
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function(){
                        $("#bodyTable").empty();
                        $("#equipElim").val("");
                    });
                }    
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });
        
        $("#buscarEliminar").click(function(){
            $("#equipElim").click();
        });
        
        $("#equipElim").change(function(){
            $("#salirBuscarEliminar").click();
            $("#fondo").show();
            $("#tokenEliminarEquLAE").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formEliminarEquiLAE")[0]);
            $.ajax({
                type: "POST",
                url: "<?= $GLOBALS['domain_root'] ?>/ajax/eliminarEquiposLAE.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    $("#fondo").hide();
                    <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    $("#bodyTable").empty();
                    if(data[0].result === 0){
                        $.alert.open("warning", "Alerta", "No se pudo cargar el archivo", {"Aceptar" : "Aceptar"}, function(){
                            $("#cancelarEliminar").click();
                        });
                    } else if(data[0].result === 1){
                        $("#bodyTable").append(data[0].tabla);
                        if($("#opcionCompleto").val() === "completo" || $("#LAEopcionCompleto").val() === "completo"
                        || $("#opcionSegmentado").val() === "completo" || $("#LAEopcionSegmentado").val() === "completo" || $("#opcionCompleto").val() === "incremCompleto" || $("#LAEopcionCompleto").val() === "incremCompleto"
                        || $("#opcionSegmentado").val() === "incremCompleto" || $("#LAEopcionSegmentado").val() === "incremCompleto"){
                            $("#eliminarEquipoLAECom").val(data[0].nombre);
                        } else {
                            $("#eliminarEquipoLAESeg").val(data[0].nombre);
                        }
                        $("#fondo1").show();
                        $("#modal-eliminarLAE").show();
                    } else if(data[0].result === 2){
                        $.alert.open("warning", "Alerta", '<?= $general->getSeparadorGeneral($_SESSION["idioma"]) ?>', {"Aceptar" : "Aceptar"}, function(){
                            $("#cancelarEliminar").click();
                        });
                    }
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    $("#cancelarEliminar").click();
                });
            });
        });
        //fin modal eliminar equipos LAE
        
        $("#archivo").bind('change',function(){
            var pesoArch = 0;
            var filesObj = archivo.files;

            var filesArray = Object.keys(filesObj).map(function(key){
              return filesObj[key];
            });

            filesArray.forEach(function(file){
                pesoArch += file.size;
            });

            if (pesoArch / 1024 > 10240){ 
                $.alert.open('warning', 'Alert', "El peso de los archivos supera los 10 MB", {"Aceptar" : "Aceptar"}, function(){
                    $("#url_file").val("Nombre del Archivo...");
                    $("#archivo").val("");
                    $("#archivo").replaceWith($("#archivo").clone(true));
                });
                
                return false;
            } 
        });
        
        $("#archivo1").bind('change',function(){
            var pesoArch = 0;
            var filesObj = archivo.files;

            var filesArray = Object.keys(filesObj).map(function(key){
              return filesObj[key];
            });

            filesArray.forEach(function(file){
                pesoArch += file.size;
            });

            if (pesoArch / 1024 > 10240){ 
                $.alert.open('warning', 'Alert', "El peso de los archivos supera los 10 MB", {"Aceptar" : "Aceptar"}, function(){
                    $("#url_file1").val("Nombre del Archivo...");
                    $("#archivo1").val("");
                    $("#archivo1").replaceWith($("#archivo1").clone(true));
                });
                
                return false;
            } 
        });
        
        $("#archivo2").bind('change',function(){
            var pesoArch = 0;
            var filesObj = archivo.files;

            var filesArray = Object.keys(filesObj).map(function(key){
              return filesObj[key];
            });

            filesArray.forEach(function(file){
                pesoArch += file.size;
            });

            if (pesoArch / 1024 > 10240){ 
                $.alert.open('warning', 'Alert', "El peso de los archivos supera los 10 MB", {"Aceptar" : "Aceptar"}, function(){
                    $("#url_file2").val("Nombre del Archivo...");
                    $("#archivo2").val("");
                    $("#archivo2").replaceWith($("#archivo2").clone(true));
                });
                
                return false;
            } 
        });
        
        $("#archivo3").bind('change',function(){
            var pesoArch = 0;
            var filesObj = archivo.files;

            var filesArray = Object.keys(filesObj).map(function(key){
              return filesObj[key];
            });

            filesArray.forEach(function(file){
                pesoArch += file.size;
            });

            if (pesoArch / 1024 > 10240){ 
                $.alert.open('warning', 'Alert', "El peso de los archivos supera los 10 MB", {"Aceptar" : "Aceptar"}, function(){
                    $("#url_file3").val("Nombre del Archivo...");
                    $("#archivo3").val("");
                    $("#archivo3").replaceWith($("#archivo3").clone(true));
                });
                
                return false;
            } 
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
    
    function desplegarCompleto() {
        $("#principal").hide();
        $("#segmentado").hide();
        $("#incremCompleto").hide();
        $("#incremSegmentado").hide();
        $("#completo").show();
        $("#despliegueSegmentado").hide();
        $("#despliegueCompleto").show();
        $("#opcionCompleto").val("completo");
        $("#LAEopcionCompleto").val("completo");
        $("#subsidiariaOpcionCompleto").val("completo");
        $("#LAE-completo").show();
        $("#LAE-segmentado").hide();
        $("#subsidiaria-completo").show();
        $("#subsidiaria-segmentado").hide();
        $("#archivo").removeAttr("multiple");
        $("#archivo1").removeAttr("multiple");
    }

    function desplegarSegmentado() {
        $("#principal").hide();
        $("#completo").hide();
        $("#incremCompleto").hide();
        $("#incremSegmentado").hide();
        $("#segmentado").show();
        $("#despliegueCompleto").hide();
        $("#despliegueSegmentado").show();
        $("#opcionSegmentado").val("segmentado");
        $("#LAEopcionSegmentado").val("segmentado");
        $("#subsidiariaOpcionSegmentado").val("segmentado");
        $("#LAE-segmentado").show();
        $("#LAE-completo").hide();
        $("#subsidiaria-segmentado").show();
        $("#subsidiaria-completo").hide();
        $("#archivo2").removeAttr("multiple");
        $("#archivo3").removeAttr("multiple");
    }

    function desplegarIncremCompleto() {
        $("#principal").hide();
        $("#segmentado").hide();
        $("#incremCompleto").show();
        $("#incremSegmentado").hide();
        $("#completo").hide();
        $("#despliegueSegmentado").hide();
        $("#despliegueCompleto").show();
        $("#opcionCompleto").val("incremCompleto");
        $("#LAEopcionCompleto").val("incremCompleto");
        $("#subsidiariaOpcionCompleto").val("incremCompleto");
        $("#archivo").attr("multiple", "true");
        $("#archivo1").attr("multiple", "true");
        $("body").scrollTop(1000);
        
        eliminarDespliegue();
    }

    function desplegarIncremSegmentado() {
        $("#principal").hide();
        $("#segmentado").hide();
        $("#incremCompleto").hide();
        $("#incremSegmentado").show();
        $("#completo").hide();
        $("#despliegueCompleto").hide();
        $("#despliegueSegmentado").show();
        $("#opcionSegmentado").val("incremSegmentado");
        $("#LAEopcionSegmentado").val("incremSegmentado");
        $("#subsidiariaOpcionSegmentado").val("incremSegmentado");
        $("#archivo2").attr("multiple", "true");
        $("#archivo3").attr("multiple", "true");
        $("body").scrollTop(1000);
        
        eliminarDespliegue();
    }

    function despliegue(opcion){
        $("#tituloDespliegue").empty();
        $("#servidor").empty();
        if(opcion === "solaris"){
            $("#tituloDespliegue").append("Despliegue Solaris");
            $("#servidor").append("Solaris");
        }
        else if(opcion === "aix"){
            $("#tituloDespliegue").append("Despliegue AIX");
            $("#servidor").append("AIX");
        }
        else{
            $("#tituloDespliegue").append("Despliegue Linux");
            $("#servidor").append("Linux");
        }
        $("#opcionDespliegue").val(opcion);
        $(".exito_archivo").hide();
        $(".error_archivo").hide();
    }
    
    function actualizIp() {
        $.post("<?= $GLOBALS['domain_root'] ?>/ipClientes/generarArchivo.php", { desdeIp: $("#desdeIp").val(), hastaIp: $("#hastaIp").val() }, function (data) {
            if (data[0].result === 1) {
                window.open(data[0].archivo);
            } else {
                $.alert.open('warning', 'Alert', "el rango de ip no es compatibles", {"Aceptar" : "Aceptar"});
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
        });
    }
    
    function mostrarFechaDespliegue(opcion){
        if(opcion === "completo" && $("#archivo1").val() === ""){
            $.alert.open('warning', 'Alert', "Debe seleccionar el archivo LAE_Output.csv", {"Aceptar" : "Aceptar"});
            return false;
        } else if(opcion === "segmentado" && $("#archivo3").val() === ""){
            $.alert.open('warning', 'Alert', "Debe seleccionar el archivo LAE_Output.csv", {"Aceptar" : "Aceptar"});
            return false;
        }
        
        $("#fechaOpcion").val(opcion);
        $("#fondo1").show();
        $("#modal-fechaDespliegue").show();
    }
    
    function listaPersonalizada(){
        $("#fondo1").show();
        $("#modal-listaPersonalizada").show();
        $("#equipo").focus();
    }
    
    function generarListadoPersonal(){
        $("#fondo1").hide();
        $("#modal-listaPersonalizada").hide();
        $("#fondo").show();
        
        if($("#equipo").val() === ""){
            $.alert.open('warning', 'Alert', "No existen equipos", {"Aceptar" : "Aceptar"}, function() {
                $("#fondo1").show();
                $("#modal-listaPersonalizada").show();
            });
            return false;
        }    
        
        $.post("<?= $GLOBALS["domain_root"] ?>/usabilidad/ajax/generarLista.php", { equipo : $("#equipo").val(), token : localStorage.licensingassuranceToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
            $("#fondo").hide();
            if(data[0].result === 0){
                $.alert.open('warning', 'Alert', "No se pudo crear la carpeta del cliente", {"Aceptar" : "Aceptar"});
            } else if(data[0].result === 1){
                location.href = "<?= $GLOBALS["domain_root"] ?>/" + data[0].archivo;
                $("#equipo").val("");
            } else if(data[0].result === 2){
                $.alert.open('warning', 'Alert', "No existen equipos", {"Aceptar" : "Aceptar"});
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
        });
    }
    
    function eliminarDespliegue(){
        $("#fondo").hide();
        $.alert.open("confirm", "Confirmar: ", "Desea eliminar el Despliegue", {"Si" : "Si", "No" : "No"}, function(button) {
            if (button === "Si"){
                $("#fondo").show();
                $.post("ajax/limpiarDespliegue.php", { token : localStorage.licensingassuranceToken }, function (data) {
                    <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
                    if(data[0].result === 0){
                        $.alert.open('alert', "No se pudo eliminar el despliegue, por favor vuelva a intentarlo");
                    } else if(data[0].result === 1){
                        $.alert.open('info', "Despliegue eliminado con éxito", {'Aceptar' : 'Aceptar'});
                    } else{
                        $.alert.open('alert', "No se eliminó por completo el despliegue, por favor vuelva a intentarlo");
                    }
                    $("#fondo").hide();

                }, "json")
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                    });
                });
            }
        });
    }
</script>