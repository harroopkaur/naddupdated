<!--inicio modal lista personalizada-->
<div class="modal-personal" id="modal-listaPersonalizada">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Equipos Personalizada</p></h1>
    </div>

    <div class="modal-personal-body">
        <div style="width:400px; margin:0 auto;">
            <div style="float:left;">
                <span class="bold">Equipos:</span> 
            </div>
            <div style="float:left; margin-left:15px;">
                <textarea id="equipo" name="equipo" style="width:200%; height:300px; border:1px solid;" ></textarea>
            </div>
        </div>
    </div>

    <div class="modal-personal-footer">
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="$('#fondo1').hide(); $('#modal-listaPersonalizada').hide();">Salir</div></div>
        <div style="float:right;"><div class="botones_m2" id="boton1" onclick="generarListadoPersonal()">Generar Archivo</div></div>
    </div>
</div>
<!--fin modal lista personalizada-->