<div style="width:98%; padding:10px; overflow:hidden;">
    <table class="tablap" id="listadoAsigUsuarioEquipo" style="margin-top:-5px;">
        <thead>
            <tr>
                <th class="text-center">Nombre del Equipo</th>
                <th class="text-center">Nombre del Usuario</th>
                <th class="text-center">Centro de Costos</th>
                <th class="text-center">Comentarios</th>
            </tr>
        </thead>
        <tbody>
        <?php    
        $NombreEquipoAux = "";
        $centroAux = "";
        for($i = 0; $i < count($listado); $i++){
        ?>        
            <tr>
                <td style="<?php if(isset($listado[$i + 1][2]) && $listado[$i][2] != $listado[$i + 1][2] && $i > 0){ 
                echo 'border-bottom: 1px solid #999;'; } ?>"><?php if($NombreEquipoAux != $listado[$i][0]){ echo $listado[$i][0]; } ?></td>
                <td style="<?php if(isset($listado[$i + 1][2]) && $listado[$i][2] != $listado[$i + 1][2] && $i > 0){ 
                echo 'border-bottom: 1px solid #999;'; } if($NombreEquipoAux == $listado[$i][0]){
                    echo 'color: red;';
                }?>"><?= $listado[$i][1] ?></td>
                <td style="<?php if(isset($listado[$i + 1][2]) && $listado[$i][2] != $listado[$i + 1][2] && $i > 0){ 
                echo 'border-bottom: 1px solid #999;'; } ?>"><?php if($centroAux != $listado[$i][2]){ echo $listado[$i][2]; } ?></td>
                <td style="<?php if(isset($listado[$i + 1][2]) && $listado[$i][2] != $listado[$i + 1][2] && $i > 0){ 
                echo 'border-bottom: 1px solid #999;'; } if($NombreEquipoAux == $listado[$i][0]){
                    echo 'color: red;';
                }?>"><?php if($NombreEquipoAux == $listado[$i][0]){
                    echo 'Duplicado';
                }?></td>
            </tr>
            
            <?php
                if($NombreEquipoAux != $listado[$i][0]){
                    $NombreEquipoAux = $listado[$i][0];
                }
                
                if($centroAux != $listado[$i][2]){
                    $centroAux = $listado[$i][2];
                }
            }
            ?>
        </tbody>
    </table>
</div>

<br>

<div style="float:right;" class="botonesSAM boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/centroCostos/reportes/usuarioEquipo.php';">Exportar</div> 

<script>
    $(document).ready(function(){
        $("#listadoAsigUsuarioEquipo").tableHeadFixer();
    });
</script>