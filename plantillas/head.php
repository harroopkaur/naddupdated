<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <title>.:Licensing Assurance Webtool:.</title>

        <!-- Custom Theme files -->
        
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="author" content=" Licensing Assurance"/>
        <meta name="robots" content="noodp,noydir"/>

        <!-- Custom Theme files -->
        <link rel="shortcut icon" href="<?=$GLOBALS['domain_root']?>/img/Logo.ico">
        <link href="<?=$GLOBALS['domain_root']?>/plugin-alert/css/alert.min.css" rel="stylesheet" />
        <link href="<?=$GLOBALS['domain_root']?>/css/style_login.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?=$GLOBALS['domain_root']?>/css/style3.css" rel="stylesheet" type="text/css" media="all"/>

        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <link href="<?=$GLOBALS['domain_root']?>/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?=$GLOBALS["domain_root"]?>/font-awesome/css/font-awesome.min.css" type="text/css" />

        <style>
        @import 'https://code.highcharts.com/5/css/highcharts.css';
        </style>

        <link rel="stylesheet" href="<?= $GLOBALS['domain_root'] ?>/css/example.css" type="text/css" />
        <link rel="stylesheet" href="<?= $GLOBALS['domain_root'] ?>/css/jquery.countdown.timer.css" type="text/css" />

        <style>
            body {
                background: #00B0F0 url(<?= $GLOBALS['domain_root'] ?>/imagenes/inicio/bg_body.jpg) top left no-repeat !important;
                padding:0px !important;	

            }

            div.box {font-size:108.3%;margin:2px 0 15px;padding:20px 15px 20px 65px;-moz-border-radius:6px;-webkit-border-radius:6px;border-radius:6px;/*behavior:url(http://www.yourinspirationweb.com/tf/bolder/wp-content/themes/bolder/PIE.htc);*/}
            div.success-box {background:url("<?= $GLOBALS['domain_root'] ?>/img/check.png") no-repeat 15px center #ebfab6;border:1px solid #bbcc5b;color:#599847;}

            div.error-box {
                background:#00B0F0 url("<?= $GLOBALS['domain_root'] ?>/img/error.png") no-repeat 15px center #fdd2d1;
                border: 1px solid #f6988f;
                color: #883333;
            }
            
            #clock { width: 240px; height: 45px; }
        </style>

        <script language="javascript" type="text/javascript" src="<?=$GLOBALS['domain_root']?>/js/funciones_generales.js"></script>
        <script type="text/javascript" src="<?=$GLOBALS['domain_root']?>/js/jquery-1.8.2.min.js"></script>

        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        
        <?php if($_SESSION["idioma"] == 1){ 
        ?>
            <script src="<?=$GLOBALS['domain_root']?>/js/datepicker-es.js"></script>
        <?php
        }
        ?>

        <script src="<?=$GLOBALS['domain_root']?>/js/jquery.numeric.js"></script>

        <script type="text/javascript" src="<?=$GLOBALS['domain_root']?>/js/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="<?=$GLOBALS['domain_root']?>/js/tableHeadFixer.js"></script>
        <script src="<?=$GLOBALS['domain_root']?>/plugin-alert/js/alert.js"></script>
        <script src="<?=$GLOBALS['domain_root']?>/js/cabecera.js"></script>
        
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/5/js/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="<?=$GLOBALS['domain_root']?>/js/jquery.pluginCountdown.min.js"></script>
        <script src="<?=$GLOBALS['domain_root']?>/js/jquery.countdown.min.js"></script>
    </head>
    <body>
        <div class="fondo" id="fondo" style="display:none;">
            <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" alt="Cargando" style="width:50px; height:50px; top:50%; margin-top:-25px; left:50%; margin-left:-25px; position:absolute;">
        </div>
        
        <div class="fondo1" id="fondo1"></div>
        
        <audio id="player" src="<?= $domain_root?>/sonidos/ping.mp3"></audio>
        <div id="clock" class="hide"></div>
        
        <script>
            $(function () {
                var shortly = new Date(); 
                shortly.setSeconds(shortly.getSeconds() + <?= $general->tiempoLimite ?>); 
                $('#clock').countdown({until: shortly, onExpiry: liftOff}); 
            });
            
            function liftOff() { 
                $.alert.open('confirm', 'Confirmar', '<?= $general->getAlertFinSesionInactivo($_SESSION["idioma"]) ?>', {'Si' : 'Si', 'No' : 'No'}, function(button){
                    if(button === 'No'){
                        $("#fondo").show();
                        $.post("<?= $GLOBALS["domain_root"] ?>/ajax/finalizarSesion.php", "", function(){
                            location.href="<?= $GLOBALS["domain_root"] ?>";
                        }, 'json')
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function(){
                                location.href="<?= $GLOBALS["domain_root"] ?>";
                            });
                        });
                    } else{
                        $("#fondo").show();
                        $.post("<?= $GLOBALS["domain_root"] ?>/ajax/renovarSesion.php", "", function(){
                            $("#fondo").hide();
                            $.alert.open('info', "Sesión renovada", {'Aceptar' : 'Aceptar'});
                            iniciarClock();
                        }, 'json')
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'});
                        });
                    }
                });
                document.getElementById('player').play();
            }
            
            function iniciarClock(){
                shortly = new Date(); 
                shortly.setSeconds(shortly.getSeconds() + <?= $general->tiempoLimite ?>); 
                $('#clock').countdown('option', {until: shortly}); 
            }
        </script>
