<?php
require_once($RAIZ . "/clases/clase_general.php");
require_once ($RAIZ . "/clases/clase_moduloSam.php");
$moduloSam = new ModuloSam();

$ps = 0;
?>

<div style="position:fixed;">
    <div style="position:absolute;">
        <?php if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 5)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 2) { echo 'activado2'; } ?> " id="boton1"  onclick="location.href='<?= $GLOBALS['domain_root'] ?>/adobe/';">
                Adobe
            </div>
        <?php } 
        
        if ($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 16)) { ?>
            <div class="botones_m <?php if ($opcionm1 == 13) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/gantt/';">
                Gantt
            </div>
        <?php
        }
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 7)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 1) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS['domain_root'] ?>/microsoft/';">
                Microsoft
            </div>
        <?php }
       
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 13)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 9) { echo 'activado2'; } ?> " id="boton1" onclick="">
                Otros
            </div>
        <?php }
        
        if ($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 1)) { ?>
            <div class="botones_m <?php if ($opcionm1 == 12) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/';">
                SAM
            </div>
        <?php
        }
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 9)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 5) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS['domain_root'] ?>/sap/';">
                SAP
            </div>
        <?php }
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 14)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 10) { echo 'activado2'; } ?> " id="boton1"  onclick="location.href='<?= $GLOBALS['domain_root'] ?>/spla/';">
                SPLA
            </div>
        <?php }
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 11)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 7) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS['domain_root'] ?>/unixIbm/';">
                UNIX-IBM
            </div>
        <?php } 
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 12)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 8) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS['domain_root'] ?>/unixOracle/';">
                UNIX-Oracle
            </div>
        <?php }
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 15)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 11) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/usabilidad/';">
                Usabilidad
            </div>
        <?php }
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 10)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 6) { echo 'activado2'; } ?> " id="boton1"  onclick="location.href='<?= $GLOBALS['domain_root'] ?>/VMWare/';">
                VMWare
            </div>
        <?php } 
        
        if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 6)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 3) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS['domain_root'] ?>/ibm/';">
                Windows-IBM
            </div>
        <?php }
        
         if($moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 8)){ ?>
            <div class="botones_m <?php if ($opcionm1 == 4) { echo 'activado2'; } ?> " id="boton1" onclick="location.href='<?= $GLOBALS['domain_root'] ?>/oracle/';">
                Windows-Oracle
            </div>
        <?php }
        ?>
    </div>
</div>

<!-- This code must be installed within the body tags -->
<script type="text/javascript">
    var lhnAccountN = "32819-1";
    var lhnButtonN = 17;
    var lhnInviteEnabled = 1;
    var lhnWindowN = 0;
    var lhnDepartmentN = 38267;
    var lhnChatPosition = 'custom';
    var lhnChatPositionXVal = 10;
    var lhnChatPositionYVal = 75;
</script>
<a href="http://www.livehelpnow.net/help-desk-software" target="_blank" style="font-size:10px;" id="lhnHelp">help desk software</a>
<script src="//www.livehelpnow.net/lhn/widgets/chatbutton/lhnchatbutton-current.min.js" type="text/javascript" id="lhnscript"></script>