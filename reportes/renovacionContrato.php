<?php
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/configuracion/inicio.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_general.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/sam/clases/funciones.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_email.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_recordatorios.php");

$general = new General();
$nueva_funcion = new funcionesSam();
$recordatorios = new clase_recordatorios();
$email = new email();

//inicio alerta de renovacion de contrato
$alertaContrato = $nueva_funcion->alertaContratosActivos1();
/*$conteo = count($alertaContrato);*/

foreach($alertaContrato as $row){
     $tiempoExpiracion = 0;
    if ($row["tiempoExpiracion"] > 0){
        $tiempoExpiracion = $row["tiempoExpiracion"];
    }
    
    $tiempoProxima = 0;
    if ($row["tiempoProxima"] > 0){
        $tiempoProxima = $row["tiempoProxima"];
    }
    
    if($row["tiempoExpiracion"] == 90 || $row["tiempoExpiracion"] == 60 || $row["tiempoExpiracion"] == 30
    || $row["tiempoProxima"] == 90 || $row["tiempoProxima"] == 60 || $row["tiempoProxima"] == 30){
        $emailsCliente = $recordatorios->emailsCliente(1, $row["idCliente"]);
        
        $correos = "<";
        if(count($emailsCliente) > 0){
            foreach($emailsCliente as $row1){
                if($correos != "<"){
                    $correos .= ">, <";
                }
                
                $correos .= $row1["correo"];
            }
            
            $correos .= ">";
        }else{
            $correos .= $row["correo"] . ">";
        }
        
        if($row["tiempoExpiracion"] == 90){
            $email->enviar_alerta_renovacion1($correos, $row["nombre"], $row["apellido"], utf8_decode("faltan " . $tiempoExpiracion . " días para la Anualidad y " . $tiempoProxima . " días para la Renovación del contrato número: ") . $row["numero"] . ", Fabricante: " . $row["fabric"]);
            //$nueva_funcion->actulizarCorreoExpiracion90($row["idContrato"]);
        }

        if($row["tiempoExpiracion"] == 60){
            $email->enviar_alerta_renovacion1($correos, $row["nombre"], $row["apellido"], utf8_decode("faltan " . $tiempoExpiracion . " días para la Anualidad y " . $tiempoProxima . " días para la Renovación del contrato número: ") . $row["numero"] . ", Fabricante: " . $row["fabric"]);
            //$nueva_funcion->actulizarCorreoExpiracion60($row["idContrato"]);
        }

        if($row["tiempoExpiracion"] == 30){
            $email->enviar_alerta_renovacion1($correos, $row["nombre"], $row["apellido"], utf8_decode("faltan " . $tiempoExpiracion . " días para la Anualidad y " . $tiempoProxima . " días para la Renovación del contrato número: ") . $row["numero"] . ", Fabricante: " . $row["fabric"]);
            //$nueva_funcion->actulizarCorreoExpiracion30($row["idContrato"]);
        }

        if($row["tiempoProxima"] == 90){
            $email->enviar_alerta_renovacion1($correos, $row["nombre"], $row["apellido"], utf8_decode("faltan " . $tiempoExpiracion . " días para la Anualidad y " . $tiempoProxima . " días para la Renovación del contrato número: ") . $row["numero"] . ", Fabricante: " . $row["fabric"]);
            //$nueva_funcion->actulizarCorreoProxima90($row["idContrato"]);
        }

        if($row["tiempoProxima"] == 60){
            $email->enviar_alerta_renovacion1($correos, $row["nombre"], $row["apellido"], utf8_decode("faltan " . $tiempoExpiracion . " días para la Anualidad y " . $tiempoProxima . " días para la Renovación del contrato número: ") . $row["numero"] . ", Fabricante: " . $row["fabric"]);
            //$nueva_funcion->actulizarCorreoProxima60($row["idContrato"]);
        }

        if($row["tiempoProxima"] == 30){
            $email->enviar_alerta_renovacion1($correos, $row["nombre"], $row["apellido"], utf8_decode("faltan " . $tiempoExpiracion . " días para la Anualidad y " . $tiempoProxima . " días para la Renovación del contrato número: ") . $row["numero"] . ", Fabricante: " . $row["fabric"]);
            //$nueva_funcion->actulizarCorreoProxima30($row["idContrato"]);
        }
    }
}
//fin alerta de renovacion de contrato
?>