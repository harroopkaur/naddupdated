<?php
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/configuracion/inicio.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_general.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_visitas.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_email.php");

$general = new General();
$visitas = new clase_visitas();
$email = new email();

//$correo = "<m_acero_n@hotmail.com>";

$result = ""; 

//inicio Modulo SPLA
$SPLA = $visitas->visitasSPLA();
$tabla = "";
foreach($SPLA as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<maribelperozo@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo SPLA Todos")){
    $result .= "SPLA enviado\r\n";
} else{
	$result .= "SPLA No enviado\r\n";
}
//fin Modulo SPLA

//inicio Modulo SAM as a Service Colombia Panama
$SAM = $visitas->visitasSAMColomPanama();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Diana@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo SAM as a Service Colombia, Panama")){
    $result .= "SAM Colombia, Panama enviado\r\n";
} else{
	$result .= "SAM Colombia, Panama No enviado\r\n";
}
//fin Modulo SAM as a Service Colombia Panama

//inicio Modulo SAM as a Service Ecuador Costa Rica Republica Dominicana
$SAM = $visitas->visitasSAMEcuadCostaRicaDominicana();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Yasmildevia@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo SAM as a Service Ecuador, Costa Rica, Republica Dominicana")){
    $result .= "SAM Ecuador, Costa Rica, Republica Dominicana enviado\r\n";
} else{
	$result .= "SAM Ecuador, Costa Rica, Republica Dominicana No enviado\r\n";
}
//fin Modulo SAM as a Service Ecuador Costa Rica Republica Dominicana

//inicio Modulo SAM as a Service Mexico Bolivia
$SAM = $visitas->visitasSAMMexicoBolivia();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Delyissubero@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo SAM as a Service Mexico, Bolivia")){
    $result .= "SAM Mexico, Bolivia enviado\r\n";
} else{
	$result .= "SAM Mexico, Bolivia No enviado\r\n";
}
//fin Modulo SAM as a Service Mexico Bolivia

//inicio Modulo SAM as a Service Guatemala, El Salvador, Trinidad, Honduras, Nicaragua
$SAM = $visitas->visitasSAMGuateSalvaTriniHondurNicara();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Vivianabrito@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo SAM as a Service Guatemala, El Salvador, Trinidad, Honduras, Nicaragua")){
    $result .= "SAM Guatemala, El Salvador, Trinidad, Honduras, Nicaragua enviado\r\n";
} else{
	$result .= "SAM Guatemala, El Salvador, Trinidad, Honduras, Nicaragua No enviado\r\n";
}
//fin Modulo SAM as a Service Guatemala, El Salvador, Trinidad, Honduras, Nicaragua

//inicio Modulo SAM as a Service Chile, Argentina, Peru
$SAM = $visitas->visitasSAMChileArgentPeru();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Maria@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo SAM as a Service Chile, Argentina, Peru")){
	$result .= "SAM Chile, Argentina, Peru enviado\r\n";
} else{
	$result .= "SAM Chile, Argentina, Peru No enviado\r\n";
}
//fin Modulo SAM as a Service Chile, Argentina, Peru

//inicio Modulo Audit Defense Colombia Panama
$SAM = $visitas->visitasAuditDefenseColombiaPanama();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Diana@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo Audit Defense Colombia, Panama")){
    $result .= "Audit Defense Colombia, Panama enviado\r\n";
} else{
	$result .= "Audit Defense Colombia, Panama No enviado\r\n";
}
//fin Modulo Audit Defense Colombia Panama

//inicio Modulo Audit Defense Ecuador Costa Rica Republica Dominicana
$SAM = $visitas->visitasAuditDefenseEcuadCostaRicaDominicana();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Yasmildevia@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo Audit Defense Ecuador, Costa Rica, Republica Dominicana")){
    $result .= "Audit Defense Ecuador, Costa Rica, Republica Dominicana enviado\r\n";
} else{
	$result .= "Audit Defense Ecuador, Costa Rica, Republica Dominicana No enviado\r\n";
}
//fin Modulo Audit Defense Ecuador Costa Rica Republica Dominicana

//inicio Modulo Audit Defense Mexico Bolivia
$SAM = $visitas->visitasAuditDefenseMexicoBolivia();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Delyissubero@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo Audit Defense Mexico, Bolivia")){
    $result .= "Audit Defense Mexico, Bolivia enviado\r\n";
} else{
	$result .= "Audit Defense Mexico, Bolivia No enviado\r\n";
}
//fin Modulo Audit Defense Mexico Bolivia

//inicio Modulo Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua
$SAM = $visitas->visitasAuditDefenseGuateSalvaTriniHondurNicara();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Vivianabrito@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua")){
    $result .= "Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua enviado\r\n";
} else{
	$result .= "Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua No enviado\r\n";
}
//fin Modulo Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua

//inicio Modulo Audit Defense Chile, Argentina, Peru
$SAM = $visitas->visitasAuditDefenseChileArgentPeru();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Maria@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo Audit Defense Chile, Argentina, Peru")){
    $result .= "Audit Defense Chile, Argentina, Peru enviado\r\n";
} else{
	$result .= "Audit Defense Chile, Argentina, Peru No enviado\r\n";
}
//fin Modulo Audit Defense Chile, Argentina, Peru

//inicio Modulo Deployment Diagnostic
$SAM = $visitas->visitasDeploymentDiagnostic();
$tabla = "";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$correo = "<Lucelvimelo@licensingassurance.com>";
if($email->enviar_reporteVisitas($correo, "", $tabla, "Visitas Módulo Deployment Diagnostic Todos")){
    $result .= "Deployment Diagnostic enviado\r\n";
} else{
	$result .= "Deployment Diagnostic No enviado\r\n";
}
//fin Modulo Deployment Diagnostic

//inicio correo a Mariana de Resultados
$correo = "<marianaperozo@licensingassurance.com>";
$headers = 'From: Webtool <info@licensingassurance.com>' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
if(mail($correo, utf8_encode("Resultados Envío Correo Visitas"), $result, $headers)){
    $result .= "Copia de Resultados a Mariana Enviado\r\n";
} else{
    $result .= "Copia de Resultados a Mariana No Enviado\r\n";
};

echo $result;
?>