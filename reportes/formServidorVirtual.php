<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
/** Error reporting */
//error_reporting(E_ALL);

require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
//require_once($GLOBALS["app_root"] . "/clases/clase_resultados_general.php");

$general     = new General();
$moduloServidores = new moduloServidores();
//$claseResultadosGeneral = new clase_resultados_general();

if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];



    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../assets/lib/Excel/PHPExcel.php';
    // Create new PHPExcel object
   
    $objPHPExcel = new PHPExcel();
    // Set document properties

    $objPHPExcel->getProperties()->setCreator("Licensing Assurance")
                                 ->setTitle("Formulario Servidores Virtuales");
    
    
    $vert = 0;
    if(isset($_GET["vert"]) && is_numeric($_GET["vert"])){
        $vert = $_GET["vert"];
    }
    
    if($vert == 0){
        $producto = 5;
        $nombProducto = "Windows Server";
        $existe = $moduloServidores->existeWindowServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
        if($existe > 0){
            $tablaVirtual = $moduloServidores->windowServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
        }
        else{
            $tablaVirtual = $moduloServidores->windowServer($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
            //$tablaVirtual = $claseResultadosGeneral->windowServer("microsoft", $_SESSION["client_id"], "Virtual");
        }
    }
    
    if($vert == 1){
        $producto = 13;
        $nombProducto = "SQL Server";
        $existeAlineacionFisico = $moduloServidores->existeSqlServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
        if($existeAlineacionFisico > 0){
            $tablaVirtual  = $moduloServidores->sqlServerCliente($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
        }
        else{
            $tablaVirtual  = $moduloServidores->SQLServer($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
        }
    }
    
    $fabricante = 3;
    $ediciones = $moduloServidores->obtenerEdicProducto($fabricante, $producto); //3 es el id de fabricante de Microsoft
        
    //Se crean las hojas de trabajo
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Físico');
    $myWorkSheet1 = new PHPExcel_Worksheet($objPHPExcel, 'Valores');
    
    //Se agregan las hojas de Trabajo al Excel
    $objPHPExcel->addSheet($myWorkSheet, 0);
    $objPHPExcel->addSheet($myWorkSheet1, 1);
    
    //Se activa la Hoja Valores para agregar los datos de trabajo dinámico   
    $objPHPExcel->setActiveSheetIndex(1);
    
    //Se llenan los datos de la Hoja Valores
    $edicionColoumn = "A";
    $columnVersion = "C";
  
    $i = 1;
    $l = 3;
    foreach($ediciones as $row){
        //var_dump($edicionColoumn . $i);
        $edi = str_replace(" ", "_", $row["nombre"]);
        $edi = str_replace("-", ".", $edi);
        $objPHPExcel->getActiveSheet()
                    ->setCellValue("A" . $i, $edi); 
        //Se debe realizar el cambio de espacios en blanco ya que los nombres de rango no soporta los espacios
 
        $k = 1;
        $arrayVersiones = array();
        $versiones = $moduloServidores->obtenerVersionEquivalencia($fabricante, $nombProducto, $row["nombre"]);
        foreach($versiones as $row1){
            $objPHPExcel->getActiveSheet()
                        ->setCellValue($general->letraExcel($l) . $k, $row1["nombre"]);
            $arrayVersiones[] = array(0=>$row1["nombre"]); 
            $k++;
        }
  
        $i++;
        $l++;
    }    
    
    //Datos MSDN
    $objPHPExcel->getActiveSheet()
                ->setCellValue("B1", "Si"); 
    $objPHPExcel->getActiveSheet()
                ->setCellValue("B2", "No"); 
    
    //Se agrega el nombre de Rango MSDN para el uso en fórmula
    $objPHPExcel->addNamedRange(
        new PHPExcel_NamedRange(
            'MSDN', 
            $objPHPExcel->getActiveSheet(), 'B1:B2'
        )
    );
   
    //Se agrega el nombre de Rango Ediciones para el uso en fórmula
    $objPHPExcel->addNamedRange(
        new PHPExcel_NamedRange(
            'Ediciones', 
            $objPHPExcel->getActiveSheet(), 'A1:' . 'A' . count($ediciones)
        )
    );
    
    //Se agrega el nombre de Rango de cada Edición para el uso en fórmula 
    $l = 3;
    foreach($ediciones as $row){
        $versiones = $moduloServidores->obtenerVersionEquivalencia($fabricante, $nombProducto, $row["nombre"]);

        $edi = str_replace(" ", "_", $row["nombre"]);
        $edi = str_replace("-", ".", $edi);
        $objPHPExcel->addNamedRange(
            new PHPExcel_NamedRange(
                $edi, //Se debe realizar el cambio de espacios en blanco ya que
                                                       //los nombres de rango no soporta los espacios
                $objPHPExcel->getActiveSheet(), $general->letraExcel($l) . '1:' . $general->letraExcel($l) . count($versiones)
            )
        );
        $l++;
    }    
    
    //Se posiciona en la hoja principal
    $objPHPExcel->setActiveSheetIndex(0);
    
    //Se llena la cabecera de la Hoja
    $objPHPExcel->getActiveSheet()->setCellValue("A1", "Cluster");
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Host");
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Nombre");
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Familia");
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Edición");
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Versión");
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "MSDN");
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Tipo");
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Centro Costos");
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Procesadores");
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Cores");
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Lic Srv/CAL");
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Lic Proc");
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Lic Core");
    
    //llenando la data y validaciones de los combos
    $i = 2;
    foreach($tablaVirtual as $row){
        $objPHPExcel->getActiveSheet()->setCellValue("A" . $i, $row["cluster"]);
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $i, $row["host"]);
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $i, $row["equipo"]);
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $i, $row["familia"]);
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $i, $row["edicion"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $i, $row["version"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $i, $row["MSDN"]);
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $i, $row["tipo"]);
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $i, $row["centroCosto"]);                       
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $i, $row["cpu"]);                           
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $i, $row["cores"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L" . $i, $row["licSrv"]);
        $objPHPExcel->getActiveSheet()->setCellValue("M" . $i, $row["licProc"]);
        $objPHPExcel->getActiveSheet()->setCellValue("N" . $i, $row["licCore"]);
        
        $objValidation = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getDataValidation();
        $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
        $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('El valor no esta en la lista.');
        $objValidation->setFormula1('=Ediciones'); 
        
        $objValidation = $objPHPExcel->getActiveSheet()
            ->getCell('F' . $i)
            ->getDataValidation();

        $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST )
            ->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION )
            ->setAllowBlank(false)
            ->setShowInputMessage(true)
            ->setShowErrorMessage(true)
            ->setShowDropDown(true)
            ->setErrorTitle('Input error')
            ->setError('La versión no está en la lista')
            ->setFormula1('=INDIRECT($E$' . $i . ')');
        
        $objValidation = $objPHPExcel->getActiveSheet()->getCell('G' . $i)->getDataValidation();
        $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
        $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('El valor no esta en la lista.');
        $objValidation->setFormula1('=MSDN');
        
        $i++;
    }                         
                                
    $objPHPExcel->getSheetByName('Valores')
    ->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
    
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Formulario.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}