<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$general     = new General();

if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "隆Usted debe Iniciar Sesi贸n!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    require_once dirname(__FILE__) . '/../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"
    
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Asignaciones");
    
    $fabricante = 0;
    if ($_GET["fabricante"] && is_numeric($_GET["fabricante"])){
        $fabricante = $_GET["fabricante"];
    }

    $equipos = array();
    
    if ($fabricante == 1) {
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_adobe.php");
        $detalles = new DetallesAdobe();
        $equipos = $detalles->listar_equipos($_SESSION["client_id"]); 
    } else if ($fabricante == 2){
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo_ibm.php");
        $detalles = new DetallesIbm();
        $equipos = $detalles->listar_equipos($_SESSION["client_id"]);
    } else if ($fabricante == 3){
        require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
        $detalles = new DetallesE_f();
        $equipos = $detalles->listar_equipos($_SESSION["client_id"]);
    }
    
    $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
        
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Asignaciones');
    $myWorkSheet->setCellValue('A1', 'equipo')
                ->setCellValue('B1', 'asignacion');
    
    
    //inicio hoja de trabajo para las asignaciones
    $myWorkSheet1 = new PHPExcel_Worksheet($objPHPExcel, 'Valores');
    $myWorkSheet1->setCellValue('A1', 'asignacion');
    
    $i = 2;
    foreach($asignaciones as $row){
        $myWorkSheet1->setCellValue('A' . $i, $row["asignacion"]);
        $i++;
    }
    //fin hoja de trabajo para las asignaciones
    

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->addSheet($myWorkSheet, 0);
    $objPHPExcel->addSheet($myWorkSheet1, 1);
    $objPHPExcel->setActiveSheetIndex(0);
    
    $i = 2;
    foreach($equipos as $row){
        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(0, $i, $row["equipo"], PHPExcel_Cell_DataType::TYPE_STRING);
        
        $objValidation = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getDataValidation();
        $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
        $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('El valor no esta en la lista.');
        $objValidation->setFormula1('Valores!$A$2:$A$' . (count($asignaciones) + 1)); 
        $i++;
    }    
       
    $objPHPExcel->getSheetByName('Valores')
    ->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
    
    // Redirect output to a client鈥檚 web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Asignacion.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    //$objWriter->save(dirname(__FILE__) . '../Asignacion.xlsx');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}