<?php
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/configuracion/inicio.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_general.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_visitas.php");
require_once("/home/vamoscloud2014/public_html/vieja-webtool.licensingassurance.com/clases/clase_email.php");

$general = new General();
$visitas = new clase_visitas();
$email = new email();

/*$correo = "<m_acero_n@hotmail.com>";
$copy = "";*/

$correo = "<dimitri@licensingassurance.com>";
$copy = "<anto@licensingassurance.com>, <giovannam@licensingassurance.com>, <marianaperozo@licensingassurance.com>";

//inicio Modulo SPLA
$SPLA = $visitas->visitasSemanales("SAM as a Service for SPLA", "");
$tabla = "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>SAM as a Service for SPLA</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SPLA as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo SPLA

//inicio Modulo SAM as a Service Colombia Panama
$SAM = $visitas->visitasSemanales("SAM as a Service", "'Colombia', 'Panama'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>SAM as a Service Colombia, Panama</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo SAM as a Service Colombia Panama

//inicio Modulo SAM as a Service Ecuador Costa Rica Republica Dominicana
$SAM = $visitas->visitasSemanales("SAM as a Service", "'Ecuador', 'Costa Rica', 'Republica Dominicana'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>SAM as a Service Ecuador, Costa Rica, Republica Dominicana</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo SAM as a Service Ecuador Costa Rica Republica Dominicana

//inicio Modulo SAM as a Service Mexico Bolivia
$SAM = $visitas->visitasSemanales("SAM as a Service", "'Mexico', 'Bolivia'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>SAM as a Service Mexico, Bolivia</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo SAM as a Service Mexico Bolivia

//inicio Modulo SAM as a Service Guatemala, El Salvador, Trinidad, Honduras, Nicaragua
$SAM = $visitas->visitasSemanales("SAM as a Service", "'Guatemala', 'El Salvador', 'Trinidad', 'Honduras', 'Nicaragua'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>SAM as a Service Guatemala, El Salvador, Trinidad, Honduras, Nicaragua</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo SAM as a Service Guatemala, El Salvador, Trinidad, Honduras, Nicaragua

//inicio Modulo SAM as a Service Chile, Argentina, Peru
$SAM = $visitas->visitasSemanales("SAM as a Service", "'Chile', 'Argentina', 'Peru'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>SAM as a Service Chile, Argentina, Peru</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo SAM as a Service Chile, Argentina, Peru

//inicio Modulo Audit Defense Colombia Panama
$SAM = $visitas->visitasSemanales("Audit Defense", "'Colombia', 'Panama'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>Audit Defense Colombia, Panama</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo Audit Defense Colombia Panama

//inicio Modulo Audit Defense Ecuador Costa Rica Republica Dominicana
$SAM = $visitas->visitasSemanales("Audit Defense", "'Ecuador', 'Costa Rica', 'Republica Dominicana'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>Audit Defense Ecuador, Costa Rica, Republica Dominicana</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo Audit Defense Ecuador Costa Rica Republica Dominicana

//inicio Modulo Audit Defense Mexico Bolivia
$SAM = $visitas->visitasSemanales("Audit Defense", "'Mexico', 'Bolivia'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>Audit Defense Mexico, Bolivia</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo Audit Defense Mexico Bolivia

//inicio Modulo Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua
$SAM = $visitas->visitasSemanales("Audit Defense", "'Guatemala', 'El Salvador', 'Trinidad', 'Honduras', 'Nicaragua'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo Audit Defense Guatemala, El Salvador, Trinidad, Honduras, Nicaragua

//inicio Modulo Audit Defense Chile, Argentina, Peru
$SAM = $visitas->visitasSemanales("Audit Defense", "'Chile', 'Argentina', 'Peru'");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>Audit Defense Chile, Argentina, Peru</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo Audit Defense Chile, Argentina, Peru

//inicio Modulo Deployment Diagnostic
$SAM = $visitas->visitasSemanales("Deployment Diagnostic", "");
$tabla .= "<table class='tablap' width='800' align='center' cellpadding='0' cellspacing='0'>
            <tr>
                <th colspan='4' style='text-align: center;'>Deployment Diagnostic</th>
            </tr>
            <tr>
                <th>M&oacute;dulo</th>
                <th>IP</th>
                <th>Pa&iacute;s</th>
                <th>Fecha</th>
            </tr>";
foreach($SAM as $row){
    $tabla .= "<tr>"
        . "<td>" . $row["modulo"] . "</td>"
        . "<td>" . $row["ip"] . "</td>"
        . "<td>" . $row["pais"] . "</td>"
        . "<td>" . $row["fecha"] . "</td>"
    . "</tr>"; 
}

$tabla .= "</table><br>";
//fin Modulo Deployment Diagnostic*/

if($email->enviar_reporteVisitasSemanal($correo, $copy, $tabla, "Visitas Módulo Website")){
    echo "Visitas Módulo Website enviado\r\n";
}
?>