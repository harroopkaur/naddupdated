<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");

$general     = new General();
$equivalencia = new Equivalencias();
$funciones = new funcionesSam();

if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    require_once dirname(__FILE__) . '/../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"
    
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("Empleados");

    $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
    
    /*$asig = '"';
    foreach($asignaciones as $row){
        if($asig != '"'){
            $asig .= ', ';
        }
        $asig .= $row["asignacion"]; 
    }
    $asig .= '"';*/
    
    //inicio hoja de trabajo para las asignaciones
    $myWorkSheet1 = new PHPExcel_Worksheet($objPHPExcel, 'Valores');
    $myWorkSheet1->setCellValue('A1', 'asignacion');
    
    $i = 2;
    foreach($asignaciones as $row){
        $myWorkSheet1->setCellValue('A' . $i, $row["asignacion"]);
        $i++;
    }
    //fin hoja de trabajo para las asignaciones
    
    $fabricantesEmpleado = $funciones->obtenerFabricantesEmpleado($_SESSION["client_empleado"]);
    $i = 1;
    $iAux;
    
    $objPHPExcel->addSheet($myWorkSheet1, 20);
    foreach($fabricantesEmpleado as $row1){
    //for($i = 1; $i < 9; $i++){
        
        if ($row1["idFabricante"] == 10){
            continue;
        } 
        
        if ($row1["idFabricante"] == 3){
            $iAux = $i - 1;
        } 
        
        $listado = $equivalencia->listar_todo($row1["idFabricante"]);
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, $row1["nombre"]);
        
        
        $myWorkSheet->setCellValue('A1', 'Producto')
                    ->setCellValue('B1', 'Edición')
                    ->setCellValue('C1', 'Versión')
                    ->setCellValue('D1', 'SKU')
                    ->setCellValue('E1', 'Tipo')
                    ->setCellValue('F1', 'Cantidades')
                    ->setCellValue('G1', 'Precio Unitario')
                    ->setCellValue('H1', 'Asignación');
        
        $j = 2;
        foreach($listado as $row){
            $myWorkSheet->setCellValue('A' . $j, $row["familia"])
                        ->setCellValue('B' . $j, $row["edicion"])
                        ->setCellValueExplicit('C' . $j, $row["version"], PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValue('D' . $j, '')
                        ->setCellValue('E' . $j, '')
                        ->setCellValue('F' . $j, '')
                        ->setCellValue('G' . $j, '');
            $j++;
        }
        
        $objPHPExcel->addSheet($myWorkSheet, $i - 1);
        $objPHPExcel->setActiveSheetIndex($i - 1);
        
        $j = 2;
        foreach($listado as $row){
            if($i == 3){
                $objValidation = $objPHPExcel->getActiveSheet()->getCell('E' . $j)->getDataValidation();
                $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
                $objValidation->setAllowBlank(false);
                $objValidation->setShowInputMessage(true);
                $objValidation->setShowErrorMessage(true);
                $objValidation->setShowDropDown(true);
                $objValidation->setErrorTitle('Input error');
                $objValidation->setError('El valor no esta en la lista.');
                $objValidation->setFormula1('"Perpetuo,Subscripción,Software Assurance,Otro"');            
            } else{
                $objValidation = $objPHPExcel->getActiveSheet()->getCell('E' . $j)->getDataValidation();
                $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
                $objValidation->setAllowBlank(false);
                $objValidation->setShowInputMessage(true);
                $objValidation->setShowErrorMessage(true);
                $objValidation->setShowDropDown(true);
                $objValidation->setErrorTitle('Input error');
                $objValidation->setError('El valor no esta en la lista.');
                $objValidation->setFormula1('"Perpetuo,Subscripción,Otro"');       
            }
            
            $objValidation1 = $objPHPExcel->getActiveSheet()->getCell('H' . $j)->getDataValidation();
            $objValidation1->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
            $objValidation1->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
            $objValidation1->setAllowBlank(false);
            $objValidation1->setShowInputMessage(true);
            $objValidation1->setShowErrorMessage(true);
            $objValidation1->setShowDropDown(true);
            $objValidation1->setErrorTitle('Input error');
            $objValidation1->setError('El valor no esta en la lista.');
            $objValidation1->setFormula1('Valores!$A$2:$A$' . (count($asignaciones) + 1));
            
            $j++;
        }
        $i++;
    }

    $objPHPExcel->getSheetByName('Valores')
                ->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
    
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex($iAux);

    

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Formulario de Compras.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    //$objWriter->save(dirname(__FILE__) . '../Asignacion.xlsx');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}