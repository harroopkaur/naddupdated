<?php
if ($vert == 1) {
?>
    <script type="text/javascript">
        $(function () {
            $('#container3').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'vCenter Server'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Neto',
                    data: [0, 0, <?= $vCenterServerNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalvCenterServerUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalvCenterServerCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container4').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'vCenter'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },
                series: [{
                    name: 'Neto',
                    data: [0, 0, <?= $vCenterNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalvCenterUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalvCenterCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container5').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'vSphere'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },

                series: [{
                    name: 'Neto',
                    data: [0, 0, <?= $vSphereNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalvSphereUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalvSphereCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });

            $('#container6').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                     text: 'Horizon Flex'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ['', '', ''],
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                     stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                legend: {
                   reversed: true
                },
                plotOptions: {
                    dataLabels: {
                        enabled: true
                    },
                    series: {
                       stacking: 'normal'
                    }
                },
                tooltip: {
                    headerFormat: ''
                },

                series: [{
                    name: 'Neto',
                    data: [0, 0, <?= $HorizonFlexNeto ?>],
                    color: '<?= $color3 ?>'
                }, {
                    name: 'En Uso',
                    data: [0, <?= $TotalHorizonFlexUsar ?>, 0],
                    color: '<?= $color2 ?>'
                }, {
                    name: 'Compras',
                    data: [<?= $TotalHorizonFlexCompras ?>, 0, 0],
                    color: '<?= $color1 ?>'
                }]
            });
        });
    </script>
<?php
}
?>
