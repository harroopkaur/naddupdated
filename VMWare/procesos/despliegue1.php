<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$resumen      = new resumenVMWare();
$consolidado  = new consolidadoVMWare();
$tablaMaestra = new tablaMaestra();
$general      = new General();
$archivosDespliegue = new clase_archivos_fabricantes();
$log = new log();

$exito=0;
$error=0;
$exito2=0;
$error2=0;
$exito3=0;
$error3=0;
$exito4=0;
$error4=0;
$exito5=0;
$error5=0;

if(isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    if(is_uploaded_file($_FILES["archivo"]["tmp_name"])){	
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones   
        if(!file_exists($GLOBALS['app_root'] . "/VMWare/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'])){
            mkdir($GLOBALS['app_root'] . "/VMWare/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'], 0777, true);
        }

        $imagen = "licenciasESXiVcenter" . date("dmYHis") . ".csv";
        move_uploaded_file($_FILES['archivo']['tmp_name'], $GLOBALS['app_root'] . "/VMWare/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/" . $imagen);
    }
    else{
        $error = 1;
    }
    //copy("archivos_csvf1/" . $_SESSION["client_id"] . "/Resultados_Escaneo.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/Resultados_Escaneo" . date("dmYHis") . ".csv");

    if($error == 0) {
        $consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);

        if($general->obtenerSeparador("archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/" . $imagen) ===  true){
            if(($fichero = fopen("archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/" . $imagen, "r")) !== false) {
                $i=1;
                $iLicenseKey = -1;
                $iProduct = -1;
                $iUsage = -1;
                $iCapacity = -1;
                $iLabel = -1;
                $iExpires = -1;
                $iAssigned = -1;
                
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    if($i == 1){
                        for($k = 0; $k < count($datos); $k++){
                            if($iLicenseKey == -1 && trim(utf8_encode($datos[$k])) == "License Key"){ 
                                $iLicenseKey = $k; 
                            }
                            
                            if($iProduct == -1 && strtoupper(trim(utf8_encode($datos[$k]))) == "PRODUCT"){ 
                                $iProduct = $k; 
                            }
                            
                            if($iUsage == -1 && trim(utf8_encode($datos[$k])) == "Usage"){ 
                                $iUsage = $k; 
                            }
                            
                            if($iCapacity == -1 && strtoupper(trim(utf8_encode($datos[$k]))) == "CAPACITY"){ 
                                $iCapacity = $k; 
                            }
                            
                            if($iLabel == -1 && strtoupper(trim(utf8_encode($datos[$k]))) == "LABEL"){ 
                                $iLabel = $k; 
                            }
                            
                            if($iExpires == -1 && strtoupper(trim(utf8_encode($datos[$k]))) == "EXPIRES"){ 
                                $iExpires = $k; 
                            }
                            
                            if($iAssigned == -1 && strtoupper(trim(utf8_encode($datos[$k]))) == "ASSIGNED"){ 
                                $iAssigned = $k; 
                            }
                        }
                    } else if($i > 1){
                        break;
                    }
                    $i++;
                }
                
                if($iProduct == -1 || $iCapacity == -1 || $iLabel == -1 || $iExpires == -1 || $iAssigned == -1){
                    $error = 3;
                    unlink($GLOBALS['app_root'] . "/VMWare/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION['client_empleado'] . "/" . $imagen);
                }
                
                fclose($fichero);
            }
        }else{
            $error = 6;
        }
                
                
        if($error == 0){
            if($iLicenseKey > -1){
                if($general->obtenerSeparador("archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/" . $imagen) ===  true){
                    if(($fichero = fopen("archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/" . $imagen, "r")) !== false) {   
                        $i = 1;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                            if($i > 1){
                                if(isset($datos[$iProduct]) && !$consolidado->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $licen, $datos[$iProduct], 
                                $uso, $datos[$iCapacity], $datos[$iLabel], $datos[$iExpires], $datos[$iAssigned])){
                                    echo $consolidado->error;
                                }
                            }
                            $i++;
                            $exito=1;	
                        }
                        fclose($fichero);

                        if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 6) === false){
                            $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 6);
                        }

                        $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION['client_empleado'], 6, $imagen, 0);

                    }
                }
            } else{
                if($general->obtenerSeparador("archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/" . $imagen) ===  true){
                    if(($fichero = fopen("archivos_csvf1/" . $_SESSION['client_id'] . "/" . $_SESSION['client_empleado'] . "/" . $imagen, "r")) !== false) {   
                        $i = 1;
                        $bandCab = false;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                            if($i > 1){
                                if(strpos($datos[0], "VMware") !== false || strpos($datos[0], "vCenter") !== false 
                                || strpos($datos[0], "vSphere") !== false || strpos($datos[0], "Horizon") !== false){
                                    $producto = $datos[0];
                                    $uso = $datos[1];
                                    $capacidad = $datos[2];
                                    $label = $datos[3];
                                }
                                
                                if(filter_var($datos[0], FILTER_VALIDATE_IP) === false && (strpos($datos[0], "VMware") === false 
                                && strpos($datos[0], "vCenter") === false && strpos($datos[0], "vSphere") === false 
                                && strpos($datos[0], "Horizon") === false)){
                                    $licen = $datos[0];
                                    $expires = $datos[4];
                                    $asignado = $datos[1];
                                    
                                    if(!$consolidado->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $licen, $producto, 
                                    $uso, $capacidad, $label, $expires, $asignado)){
                                        echo $consolidado->error;
                                    }
                                }
                            }
                            $i++;
                            $exito=1;	
                        }
                        fclose($fichero);

                        if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 6) === false){
                            $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 6);
                        }

                        $archivosDespliegue->actualizarDespliegue1($_SESSION["client_id"], $_SESSION['client_empleado'], 6, $imagen, 0);

                    }
                }
            }
            
            //inicio resumen Oracle
            $resumen->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);

            $tablaMaestra->listadoProductosMaestra(6);
            foreach($tablaMaestra->listaProductos as $rowProductos){
                if($consolidado->listarProductos($_SESSION['client_id'], $_SESSION['client_empleado'], $rowProductos["descripcion"], $rowProductos["campo2"])){
                    foreach($consolidado->lista as $reg_r){  
                        $producto = "";

                        if($tablaMaestra->productosSustituir(6, $reg_r["product"])){
                            $producto  = $tablaMaestra->sustituir;
                            $idForaneo = $tablaMaestra->idForaneo;
                        }

                        $edicion = "";
                        $version = "";               

                        if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                            if($tablaMaestra->sustituir == null){
                                $version = "";
                            }
                            else{
                                $version = $tablaMaestra->sustituir;
                            }
                        }

                        $tablaMaestra->sustituir = "";
                        if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 5)){
                            if($tablaMaestra->sustituir == null){
                                $edicion = "";
                            }
                            else{
                                $edicion = $tablaMaestra->sustituir;
                            }
                        }

                        if($reg_r["usages"] == ""){
                            $uso[0] = null;
                            $uso[1] = null;
                        }else {
                            $uso = array();
                            if(explode(" ", $reg_r["usages"], -1)){
                                $uso = explode(" ", $reg_r["usages"]);  
                            }
                            else{
                                $uso[0] = $reg["usages"];
                                $uso[1] = "";
                            }
                        }
                        
                        $resumen->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $reg_r["licenseKey"], $producto, $edicion, $version, $uso[0], $uso[1], $reg_r["capacity"], $reg_r["labels"], $reg_r["assigned"]);
                    }  
                }
            }
            //fin resumen Oracle

            $exito=1;

            $log->insertar(25, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
        }
    }
}