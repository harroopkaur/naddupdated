<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalencias.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_VMWare.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$compra_datos  = new comprasVMWare();
$resumen       = new resumenVMWare();
$balance_datos = new balanceVMWare();
$equivalencia  = new Equivalencias();
$tablaMaestra  = new tablaMaestra();
$general       = new General();
$archivosDespliegue   = new clase_archivos_fabricantes();
$log = new log();

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

if (isset($_POST['insertar']) && $_POST['insertar'] == 1) {

    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];

    // Validaciones
    $extension = explode(".", $nombre_imagen);  // Obtener tipo de archivo
    $long = count($extension) - 1;
    if (($extension[$long] != "csv") && ($extension[$long] != "CSV")) {
        $error = 1;
    }  // Permitir subir solo imagenes JPG,

    if ($error == 0) {

        $imagen = $_SESSION['client_id'] . $_SESSION['client_empleado'] . "_License_Keys" . date("dmYHis") . ".csv";

        if (!$compra_datos->cargar_archivo($imagen, $temporal_imagen)) {
            $error = 5;
        } else {

            $compra_datos->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            $compra_datos->eliminarDetalle($_SESSION['client_id'], $_SESSION['client_empleado']);
            $balance_datos->eliminar($_SESSION['client_id'], $_SESSION['client_empleado']);
            
            if($general->obtenerSeparador("archivos_csvf4/" . $imagen) === true){
                if (($fichero = fopen("archivos_csvf4/" . $imagen, "r")) !== FALSE) {
                    $i = 0;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        /*if($i == 6 && ($datos[0] != "Account Number" || $datos[1] != "Account Name" || ($datos[2] != "Product" ) || 
                        $datos[3] != "License Key" || $datos[4] != "Qty" || $datos[5] != "Unit of Measure" || $datos[6] != "Folder Name and Path" ||
                        $datos[7] != "License Key Notes" || $datos[18] != "Type" || $datos[19] != "Number" || $datos[20] != "PO Number" ||
                        $datos[21] != "Order Date" || $datos[22] != "Order Qty" || $datos[23] != "Support Level" || $datos[24] != "License Coverage End Date")){*/
                        
                        if($i == 6){
                            $iPONumber = -1;
                            if($datos[20] == "PO Number"){
                                $iPONumber = 20;
                            }

                            $orderQtyAprob = false;
                            $iOrderQty = -1;
                            if($datos[22] == "Order Qty"){
                                $iOrderQty = 22;
                                $orderQtyAprob = true;
                            }
                            if($datos[20] == "Order Quantity"){
                                $iOrderQty = 20;
                                $orderQtyAprob = true;
                            }
                            
                            $supportLevelAprob = false;
                            $iSupportLevel = -1;
                            if($datos[23] == "Support Level"){
                                $iOrderQty = 23;
                                $supportLevelAprob = true;
                            }
                            if($datos[24] == "Support Level"){
                                $iOrderQty = 24;
                                $supportLevelAprob = true;
                            }
                            
                            $licenseCoverageEndDateAprob = false;
                            $iLicenseCoverageEndDate = -1;
                            if($datos[24] == "License Coverage End Date"){
                                $iLicenseCoverageEndDate = 24;
                                $licenseCoverageEndDateAprob = true;
                            }
                            if($datos[23] == "License Coverage End Date"){
                                $iLicenseCoverageEndDate = 23;
                                $licenseCoverageEndDateAprob = true;
                            }
                            
                            if($datos[0] != "Account Number" || $datos[1] != "Account Name" || strpos($datos[2], "Product") === false || 
                            $datos[3] != "License Key" || (strpos($datos[4], "Qty") === false && strpos($datos[4], "Quantity") === false) || $datos[5] != "Unit of Measure" || strpos($datos[6], "Folder") === false ||
                            $datos[7] != "License Key Notes" || $datos[18] != "Type" || $datos[19] != "Number" || 
                            $datos[21] != "Order Date" || $orderQtyAprob === false || $supportLevelAprob === false || $licenseCoverageEndDateAprob === false){  
                                $error = 3;
                                unlink ("archivos_csvf4/" . $imagen); 
                                break;
                            }
                        }else if($i > 6){
                            break;
                        }
                        
                        $i++;
                    }    
                    
                    fclose($fichero);             
                }
            }else{
                $error = 4;
            }
            
            if($error == 0){
                if($general->obtenerSeparador("archivos_csvf4/" . $imagen) === true){
                    if (($fichero = fopen("archivos_csvf4/" . $imagen, "r")) !== FALSE) {
                        $i = 0;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                            if($i > 6 && isset($datos[2]) && !empty($datos[2]) && ord($datos[2]) != 160){
                                if(filter_var($datos[0], FILTER_VALIDATE_INT) === false){
                                    $datos[0] = 0;
                                }                          
                                $datos[1] = $general->get_escape($datos[1]);
                                $datos[2] = $general->get_escape($datos[2]);
                                $datos[3] = $general->get_escape($datos[3]); 
                                if(filter_var($datos[4], FILTER_VALIDATE_INT) === false){
                                    $datos[4] = 0;
                                }    
                                $datos[5] = $general->get_escape($datos[5]);
                                $datos[6] = $general->get_escape($datos[6]); 
                                $datos[7] = $general->get_escape($datos[7]);
                                $datos[18] = $general->get_escape($datos[18]); 
                                if(filter_var($datos[19], FILTER_VALIDATE_INT) === false){
                                    $datos[19] = 0;
                                }   

                                $datos[20] = null;
                                if($iPONumber > -1){
                                    $datos[20] = $general->get_escape($datos[20]);
                                }

                                $fechaOrden = "0000-00-00";
                                if(isset($datos[21]) && $datos[21] != ""){
                                    $fechaOrden = $general->cambiarfechasFormato($general->get_escape($datos[21]));
                                }

                                if(filter_var($datos[$iOrderQty], FILTER_VALIDATE_INT) === false){
                                    $datos[22] = 0;
                                }   
                                
                                $datos[23] = null;
                                if($iSupportLevel > -1){
                                    $datos[23] = $general->get_escape($datos[$iSupportLevel]); 
                                }
                                
                                $fechaFinLicencia = "0000-00-00";
                                if(isset($datos[$iLicenseCoverageEndDate]) && $datos[$iLicenseCoverageEndDate] != ""){
                                    $datos[24] = $general->cambiarfechasFormato($general->get_escape($datos[$iLicenseCoverageEndDate]));
                                }

                                if (!$compra_datos->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $datos[0], $datos[1], $datos[2], 
                                    $datos[3], $datos[4], $datos[5], $datos[6], $datos[7], $datos[18], $datos[19], 
                                    $datos[20], $fechaOrden, $datos[22], $datos[23], $fechaFinLicencia)) {
                                    echo $compra_datos->error;
                                } else {

                                    $exito = 1;
                                }//exito
                            }
                            $i++;
                        }//WhILE

                        if($archivosDespliegue->existeArchivos($_SESSION["client_id"], $_SESSION['client_empleado'], 6) === false){
                            $archivosDespliegue->insertar($_SESSION["client_id"], $_SESSION['client_empleado'], 6);
                        }
                        $archivosDespliegue->actualizarCompras($_SESSION["client_id"], $_SESSION['client_empleado'], 6, $imagen);                      
                        
                        fclose($fichero);
                    }//FICHERO
                }
            }
        }//cago
    }//is error


    if ($exito == 1){
        $tablaMaestra->listadoProductosMaestra(6);
        foreach($tablaMaestra->listaProductos as $rowProductos){
            if($compra_datos->listarProductos($_SESSION['client_id'], $_SESSION['client_empleado'], $rowProductos["descripcion"], $rowProductos["campo2"])){
                foreach($compra_datos->listado as $reg_r){  
                    $producto = "";

                    if($tablaMaestra->productosSustituir(6, $reg_r["product"])){
                        $producto  = $tablaMaestra->sustituir;
                        $idForaneo = $tablaMaestra->idForaneo;
                    }

                    $edicion = "";
                    $version = "";               

                    if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 4)){
                        if($tablaMaestra->sustituir == null){
                            $version = "";
                        }
                        else{
                            $version = $tablaMaestra->sustituir;
                        }
                    }

                    $tablaMaestra->sustituir = "";
                    if($tablaMaestra->buscarSustituirMaestra($reg_r["product"], $idForaneo, 5)){
                        if($tablaMaestra->sustituir == null){
                            $edicion = "";
                        }
                        else{
                            $edicion = $tablaMaestra->sustituir;
                        }
                    }
                    
                    $compra_datos->insertarDetalle($_SESSION['client_id'], $_SESSION['client_empleado'], $producto, $edicion, $version, 
                    $reg_r["licenseKey"], $reg_r["Qty"], $reg_r["unitMeasure"], $reg_r["type"], 
                    $reg_r["number"], $reg_r["orderDate"], $reg_r["supportLevel"], $reg_r["licenseCoverageEndDate"]);
                    echo $compra_datos->error;
                }  
            }
        }
        
        $listaEquivalencia = $equivalencia->listar_todo(6); //6 es el id fabricante VMWare
        if ($listaEquivalencia) {
            foreach ($listaEquivalencia as $reg_o) {
                $instalaciones = 0;

                $compra = 0;
                $precio = 0;

                $newFamilia = trim($reg_o["familia"]);
                $newEdicion = trim($reg_o["edicion"]);
                $newVersion = trim($reg_o["version"]);
               
                if($compra_datos->datos3($_SESSION['client_id'], $_SESSION['client_empleado'], $newFamilia, $newEdicion, $newVersion)) {
                    $resultCompra = $compra_datos->row;
                    $compra       = $resultCompra["compra"];
                    $precio       = 0;//$resultCompra["precio"];
                }
                
                if ($resumen->listar_datos2($_SESSION['client_id'], $_SESSION['client_empleado'], $newFamilia, $newEdicion, $newVersion)) {
                    foreach ($resumen->lista as $ref_d) {
                        $instalaciones = $ref_d["total"];
                    }//foreach resumen
                }// ifresumen

                $balancec1 = $compra - $instalaciones;
                $balancec2 = $balancec1 * $precio;

                //if($balancec2<0){
                $tipo = 1;
                
                $balance_datos->insertar($_SESSION['client_id'], $_SESSION['client_empleado'], $newFamilia, $newEdicion, $newVersion, $precio, $instalaciones, $compra, $balancec1, $balancec2, $tipo);
                
            }//office
        }//office

        $exito2 = 1;
        
        $log->insertar(26, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
    }
}