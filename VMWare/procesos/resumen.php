<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

$tablaMaestra = new tablaMaestra();
$balance  = new balanceVMWare();
$compras  = new comprasVMWare();
$resumen  = new resumenVMWare();
$general = new General();

$tablaMaestra->productosSalida(6);
$balanza = false;
$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';

$vert = 0;
if (isset($_GET['vert']) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false) {
    $vert = $_GET['vert'];
}

if($vert == 0){
    $listadoContrato = $compras->listarDetalleCompra($_SESSION["client_id"], $_SESSION['client_empleado']);
}
if ($vert == 1) {
    $balanza = true;
    
    $TotalvCenterServerCompras = 0;
    $TotalvCenterServerUsar    = 0;
    
    $TotalvCenterCompras = 0;
    $TotalvCenterUsar    = 0;
   
    $TotalvSphereCompras = 0;
    $TotalvSphereUsar    = 0;
    
    $TotalHorizonFlexCompras = 0;
    $TotalHorizonFlexUsar    = 0;

    $clientTotalProductCompras = 0;
    $clientTotalProductInstal  = 0;
    $clientProductNeto         = 0;
    
    //vCenter Server
    if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "vCenter Server")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalvCenterServerCompras += $reg_equipos["compra"];
        }
    }
    
    $listavCenterServer = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "vCenter Server");
    foreach ($listavCenterServer as $reg_equipos) {
        $TotalvCenterServerUsar  += $reg_equipos["cantidad"];
    }
    
    $TotalvCenterServerInstal = $TotalvCenterServerUsar;
    $vCenterServerNeto        = $TotalvCenterServerCompras - $TotalvCenterServerInstal;
    
    
    //vCenter
    if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "vCenter")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalvCenterCompras += $reg_equipos["compra"];
        }
    }
    
    $listavCenter = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "vCenter");
    foreach ($listavCenter as $reg_equipos) {
        $TotalvCenterUsar  += $reg_equipos["cantidad"];
    }
    
    $TotalvCenterInstal = $TotalvCenterUsar;
    $vCenterNeto        = $TotalvCenterCompras - $TotalvCenterInstal;
    
    //vSphere
    if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "vSphere")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalvSphereCompras += $reg_equipos["compra"];
        }
    }
    
    $listavSphere = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "vSphere");
    foreach ($listavSphere as $reg_equipos) {
        $TotalvSphereUsar  += $reg_equipos["cantidad"];
    }
  
    $TotalvSphereInstal = $TotalvSphereUsar;
    $vSphereNeto        = $TotalvSphereCompras - $TotalvSphereInstal;
    
    //Horizon Flex
    if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], "Horizon Flex")) {
        foreach ($balance->lista as $reg_equipos) {
            $TotalHorizonFlexCompras += $reg_equipos["compra"];
        }
    }
    
    $listaHorizonFlex = $resumen->listar_datosAgrupado($_SESSION['client_id'], $_SESSION['client_empleado'], "Horizon Flex");
    foreach ($listaHorizonFlex as $reg_equipos) {
        $TotalHorizonFlexUsar  += $reg_equipos["cantidad"];
    }
    
    $TotalHorizonFlexInstal = $TotalHorizonFlexUsar;
    $HorizonFlexNeto        = $TotalHorizonFlexCompras - $TotalHorizonFlexInstal;
}