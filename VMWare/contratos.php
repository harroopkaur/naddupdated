<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/VMWare/procesos/contratos.php");
require_once($GLOBALS['app_root'] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 6;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    <div class="divContenedorRight">
        <div class="divMenuContenido">
            <?php
            $menuVMWare = 3;
            include_once($GLOBALS['app_root'] . "/VMWare/plantillas/menu.php");
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php 
                include_once($GLOBALS['app_root'] . "/VMWare/plantillas/contratos.php");
                ?>  
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS['app_root'] . "/plantillas/pie.php");