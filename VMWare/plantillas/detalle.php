<script>
    function MM_jumpMenu(targ,selObj,restore){ //v3.0
        eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
        if (restore) selObj.selectedIndex=0;
    }
</script>
    
<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:20px; margin-bottom:20px;">
    <div style="width:20%; margin:0px; padding:0px; overflow:hidden; float:left;">
        <span style="color:#9F9F9F;">Seleccione el Producto</span><br><br> 

        <div>Familia: <strong style="color:#000; font-weight:bold;">
            <?php
                $tablaMaestra->productosSalida(6);
                foreach($tablaMaestra->lista as $row){
                    switch($vert){
                        case $row["campo1"] : echo $row["campo1"]; break;
                    }
                }
            ?>
            </strong>
            <br> 

            <select onchange="MM_jumpMenu('self',this,0)">
                <option value="detalle.php" selected>Seleccione..</option>
                <?php 
                foreach($tablaMaestra->lista as $row){
                ?>
                    <option value="detalle.php?vert=<?= $row['campo1']?>"><?= $row["campo1"] ?></option>
                <?php
                }
                ?>
            </select>

        </div><br><br>

        <div>Edici&oacute;n:<strong style="color:#000; font-weight:bold;">
            <?php
            foreach($ediciones as $row){
                switch($vert1){  
                    case $row["nombre"]: echo $row["nombre"]; break;
                }
            }
            ?></strong>
            <br> 

            <select onchange="MM_jumpMenu('self',this,0)" style="max-width:150px;">
                <option value="detalle.php" selected>Seleccione..</option>
                <?php 
                foreach($ediciones as $row){
                ?>
                    <option value="detalle.php?vert=<?= $vert ?>&vert1=<?= $row['nombre']?>"><?= $row["nombre"] ?></option>
                <?php
                }
                ?>
            </select>

            <br>
            <br>
            <div style='margin-left:-10px;'>
                <form id="formExportar" name="formExportar" method="post" action="reportes/excelVMWareDetalle.php">
                    <input type="hidden" id="vert" name="vert" value="<?php echo $vert; ?>">
                    <input type="hidden" id="vert1" name="vert1" value="<?php echo $vert1; ?>">
                    <input type="submit" id="exportar" name="exportar" style="display:none">
                    <div class="botones_m2Alterno boton1" id="export">Exportar Excel</div>
                </form>
            </div>

        </div>

    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; min-height:450px; overflow:hidden;">
        <div id="container3" height:400px; width:95%; max-width: 95%; margin:10px; float:left;"></div>
        <div  style=" width:98%; max-width: 98%; margin:10px; float:left; text-align:center;"><a onclick="mostrarTabla();" style="cursor:pointer;" >Ver Detalles</a></div>
    </div>

    <div id="ttabla1" class="tablap" style="display:none; width:99%; margin:10px; float:left; height:400px; overflow:auto;">
        <table style="width:1280px; margin-top: -5px;" class="tablap" id="tablaOracleDetalle">
            <thead>
                <tr style="background:#333; color:#fff;">
                    <th valign="middle"><span>&nbsp;</span></th>
                    <th valign="middle"><span>Llave de Licencia</span></th>
                    <th valign="middle"><span>Producto</span></th>
                    <th valign="middle"><span>Edici&oacute;n</span></th>
                    <th valign="middle"><span>Versi&oacute;n</span></th>
                    <th valign="middle"><span>Uso</span></th>
                    <th valign="middle"><span>M&eacute;trica</span></th>
                    <th valign="middle"><span>Capacidad</span></th>
                    <th valign="middle"><span>Etiqueta</span></th>
                    <th valign="middle"><span>Asignado</span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($listado) > 0){
                    $i = 1;
                    foreach($listado as $reg_calculo){
                    ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $reg_calculo["licenseKey"] ?></td>
                            <td><?= $reg_calculo["familia"] ?></td>
                            <td><?= utf8_encode($reg_calculo["edicion"]) ?></td>
                            <td><?= $reg_calculo["version"] ?></td>
                            <td><?= $reg_calculo["uso"] ?></td>
                            <td><?= $reg_calculo["metrica"] ?></td>
                            <td><?= $reg_calculo["capacidad"] ?></td>
                            <td><?= $reg_calculo["etiqueta"] ?></td>
                            <td><?= $reg_calculo["asignado"] ?></td>
                        </tr>
                    <?php
                        $i++;
                    }	
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
                                
<script>
    var vertAux = "";
    var opcAux  = "";
    $(document).ready(function(){
        $("#export").click(function(){
            $("#exportar").click();
        });
        
        $("#tablaOracleDetalle").tablesorter();
        $("#tablaOracleDetalle").tableHeadFixer();
    });
    
    function mostrarTabla(){
        if($('#ttabla1').is(':visible')){
            $('#ttabla1').hide();
        }
        else{
            $('#ttabla1').show();
        }
    }
</script>