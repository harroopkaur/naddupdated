<input type="hidden" id="nombreArchivo"> 

<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:10px; margin-bottom:20px;">
    <h1 class="textog negro" style="margin:20px; text-align:center;">Contratos</h1>

    <br>

    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1.-&nbsp;</span>Para exportar y cotejar las licencias adquiridas con VMware se debe al portal de licenciamiento <a class="link1" href="https://my.vmware.com" target="_blank">https://my.vmware.com</a>. Ser&aacute;n necesarias las credenciales del usuario con rol "SuperUser" en el portal (correo que se provee a la hora de la compra de las licencias)</p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Seleccionar la opci&oacute;n "Manage License Keys"</p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Se ven todos los contratos, incluyendo aquellos que tienen soporte vencido. Presionar "Exporta all to CSV" para obtener un reporte de todos los contratos adquiridos con Vmware</p>
    </p><br/>

    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4. -&nbsp;</span>Subir archivo <strong style="font-weight:bold;">CSV </strong>guardado</p><br />
    
    <fieldset class="fieldset">
        <legend style=" font-weight:bold; margin-left:15px;">Compras</legend>
        <form enctype="multipart/form-data" name="form_c" id="form_c" method="post" action="contratos.php" >
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <div class="contenedor_archivo">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivo" id="archivo" accept=".csv">
                    <input type="button" id="boton1" class="botones_m5" value="Buscar">
                </div>
                <div class="contenedor_boton_archivo">
                    <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargandoc');document.getElementById('form_c').submit();" class="botones_m5" />
                </div>
            </div>
        </form>

        <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
        <div id="cargandoc" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>                
        <?php if ($exito2 == '1') { ?>
            <div class="exito_archivo">Archivo cargado con &eacute;xito</div>
            <script>
                $.alert.open('info', "Archivo cargado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php } else { ?>

        <?php }
        if ($error == '1') {
        ?>
            <div class="error_archivo">Debe ser un archivo con extension CSV</div>
            <script>
                $.alert.open('alert', "Debe ser un archivo con extensión CSV", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if ($error == '2') {
        ?>
            <div class="error_archivo">Debe seleccionar un archivo</div>
            <script>
                $.alert.open('alert', "Debe seleccionar un archivo", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error==3){ ?>
            <div class="error_archivo">Los campos del archivo <br> no son compatibles</div>
            <script>
                $.alert.open('alert', "Los campos del archivo no son compatibles", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if ($error == '4') {
        ?>
            <div class="error_archivo"><?= $general->error ?></div>
            <script>
                $.alert.open('alert', "<?= $general->error ?>", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if ($error > 5) {
        ?>
            <div class="error_archivo"><?= $consolidado->error ?></div>
            <script>
                $.alert.open('alert', "<?= $consolidado->error ?>", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php } ?>
    </fieldset>

    <br>
    <div style="float:right;"><div class="botones_m2" id="boton1" onClick="location.href='detalle.php'">Detalle</div></div>
</div>

<script>
    $(document).ready(function(){
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
</script>