<script type="text/javascript">
    $(document).ready(function ()
    {
        $(".filtra").click(function () {
            var valor = parseInt($("input[name='filt']:checked").val());

            if (valor === 1) {
                window.location.href = "resumen.php";
            }
            if (valor === 2) {
                window.location.href = "resumen.php?vert=1";
            }
        });

    });

    function MM_jumpMenu(targ, selObj, restore) { //v3.0
        eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
        if (restore)
            selObj.selectedIndex = 0;
    }
</script>
   
<div style="width:99%; float:left; margin:0px; padding:0px; min-height:300px; overflow:hidden; margin-bottom:20px;">
    <?php
    if ($vert == 0) {
    ?>

    <?php
    }
    if ($vert == 1) {
    ?>
        <div style="width:98%; overflow-x:auto; overflow-y:hidden;">
            <div id="contenedorGraficos" style="width:100%; overflow-x: auto; overflow-y:hidden;">
                <div style="width:1324px; float:left; text-align:center;">

                    <div id="container3" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container4" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container5" style="height:250px; width:300px; margin:10px; float:left;"></div>
                    <div id="container6" style="height:250px; width:300px; margin:10px; float:left;"></div>
                </div>
            </div>
        </div>	
    <?php
    }
    ?>
    <br>
    <div style="width:99%; margin:0 auto; padding:0px; overflow:hidden; clear:both; margin-top:15px;">
        <span style="color:#9F9F9F;">Seleccione el Reporte</span><br> 
        <div style="margin:10px; float:left;"><input type="radio" name="filt" id="filt1" value="1" class="filtra"   <?php if ($vert == 0) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Resumen Contratos</strong></div>
        <div style="margin:10px; float:left;"><input type="radio" name="filt"  id="filt2" value="2" class="filtra"  <?php if ($vert == 1) { echo 'checked'; } ?>  > <strong style="color:#000; font-weight:bold;">Balanza</strong></div>

        <div style="float:right;">
            <form id="formExportar" name="formExportar" method="post" action="reportes/excelVMWareResumen.php">
                <input type="hidden" id="familiaExcel" name="familiaExcel">
                <input type="hidden" id="edicionExcel" name="edicionExcel">
                <input type="hidden" id="vertExportar" name="vert" value = "<?= $vert ?>">
                <input type="hidden" id="opcion" name="opcion"><!-- Usado para saber que exportar en optimizacion-->
                <input type="submit" id="exportar" name="exportar" style="display:none">
                <div class="botones_m2Alterno boton1" id="export" <?php if($vert != 0){ echo 'style="display:none"'; } ?>>Exportar Excel</div>
            </form>
        </div>
        
        <div style="float:right;">
            <div class="botones_m2Alterno boton1" id="exportarTodo" style="display:none;">Exportar Todos los Productos</div>
        </div>
        
        <div style="float:right; <?php if (!$moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 1)) { echo 'display:none;'; } ?>">
            <div class="botones_m2Alterno boton1" id="actualizarRepo">Actualizar Repositorio</div>
        </div>   
        <br><br>

        <div style="<?php if (!$balanza){ echo 'display:none;'; }?>">
            <br>
            <div style="width:25%; margin:0px; padding:0px; overflow:hidden; float:left;">
                <span style="color:#9F9F9F;">Seleccione el Producto</span><br><br> 
                <div>Familia: 
                    <br> 

                    <select id="familia" name="familia">
                        <option value="">Seleccione..</option>
                        <?php 
                        foreach($tablaMaestra->lista as $row){
                        ?>
                            <option value="<?= $row['campo1']?>"><?= $row["campo1"] ?></option>
                        <?php
                        }
                        ?>
                    </select>

                </div><br>
                <div>Edici&oacute;n:
                    <br> 
                    <select id="edicion" name="edicion" style="max-width:150px;">
                        <option value="">Seleccione..</option>
                    </select>				
                </div>
            </div>

            <div style="width:73%; margin:0px; padding:0px; overflow:hidden; float:left;">
                <div id="containerDetalle">

                </div>
            </div>

            <br style="clear:both;">
            <div id="divTabla" style="display:none;">
                <table class="tablap">
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th class="text-center">Producto</th>
                            <th class="text-center">Edici&oacute;n</th>
                            <th class="text-center">Versi&oacute;n</th>
                            <th class="text-center">Instalaciones</th>
                            <th class="text-center">Compras</th>
                            <th class="text-center">Disponible</th>
                        </tr>
                    </thead>
                    <tbody id="tablaDetalle">

                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <?php
    if ($vert == 0) {
    ?>
        <br>
        <div id="ttabla1"  style="width:99%; height:400px; overflow:auto; margin:auto;">
            <table id="tablaContratos" style="width:1536px; padding:0;margin:0;" class="tablap">
                <thead>
                    <tr style="background:#333; color:#fff;">
                        <th align="center" valign="middle"><span>&nbsp;</span></th>
                        <th align="center"  valign="middle"><span>Producto</span></th>
                        <th align="center"  valign="middle"><span>Edici&oacute;n</span></th>
                        <th align="center" valign="middle"><span>Versi&oacute;n</span></th>
                        <th align="center" valign="middle"><span>Llave</span></th>
                        <th align="center" valign="middle"><span>Cantidad</span></th>
                        <th align="center" valign="middle"><span>Medida</span></th>
                        <th align="center" valign="middle"><span>Tipo</span></th>
                        <th align="center" valign="middle"><span>N&uacute;mero</span></th>
                        <th align="center" valign="middle"><span>Fecha de Orden</span></th>
                        <th align="center" valign="middle"><span>Soporte</span></th>
                        <th align="center" valign="middle"><span>Fecha Fin Cobertura</span></th>
                    </tr>
                </thead>

                <tbody>
                    <?php 
                    if($listadoContrato){
                        $i=1;
                        foreach($compras->listado as $row){
                    ?>
                            <tr>
                                <td align="center" valign="middle"><?= $i ?></td>
                                <td align="center" valign="middle"><?= $row["producto"] ?></td>
                                <td align="center" valign="middle"><?= $row["edicion"] ?></td>
                                <td align="center" valign="middle"><?= $row["version"] ?></td>
                                <td align="center" valign="middle"><?= $row["llave"] ?></td>
                                <td align="center" valign="middle"><?= $row["cantidad"] ?></td>
                                <td align="center" valign="middle"><?= $row["medida"] ?></td>
                                <td align="center" valign="middle"><?= $row["tipo"] ?></td>
                                <td align="center" valign="middle"><?= $row["numero"] ?></td>
                                <td align="center" valign="middle"><?= $row["fechaOrden"] ?></td>
                                <td align="center" valign="middle"><?= $row["soporte"] ?></td>
                                <td align="center" valign="middle"><?= $row["fechaFinCobertura"] ?></td>
                            </tr>

                    <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>

        <?php
        }
        ?>

        </div>

    <?php
    if ($vert == 1) {
    ?>

    <?php 
    }
    ?>							
</div>

<script>
    var vertAux = "";
    
    $(document).ready(function () {
        $("#tablaContratos").tablesorter();

        $("#export").click(function () {
            $("#exportar").click();
        });

        $("#familia").change(function () {
            $("#edicion").val("");
            realizarBusqueda();
        });
        
        $("#edicion").change(function () {
            realizarBusqueda();
        });
        
        $("#actualizarRepo").click(function(){
            $("#fondo").show();
            $.post("<?=$GLOBALS['domain_root']; ?>/sam/actRepoDespliegue.php", { tabla : 6, token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#fondo").hide();
                if(data[0].result === 0){
                    $.alert.open('alert', "No se pudo actualizar el repositorio de despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
                else if(data[0].result === 2){
                    $.alert.open('alert', "No se actualizó por completo el repositorio de despliegue, por favor vuelva a intentarlo", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
                else if(data[0].result === 1){
                    $.alert.open('alert', "Repositorio de despliegue actualizado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
    });
    
    function realizarBusqueda(){
        if ($("#familia").val() === "") {
            $.alert.open('alert', "Debe seleccionar una familia", {'Aceptar' : 'Aceptar'}, function() {
                $("#export").hide();
                return false;
            });
        }

        $("#export").show();
        $("#exportarTodo").show();
        $("#fondo").show();
        $.post("ajax/balanzaDetalle.php", {familia: $("#familia").val(), edicion: $("#edicion").val(), token : localStorage.licensingassuranceToken}, function (data) {
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#containerDetalle").empty();
            $("#tablaDetalle").empty();
            $("#edicion").empty();
            $("#edicion").append(data[0].edicion);
            $("#familiaExcel").val($("#familia").val());
            $("#edicionExcel").val($("#edicion").val());
            $("#edicion").val("");

            serie = [{
                        name: 'Neto',
                        data: [0, 0, data[0].neto],
                        color: '<?= $color3 ?>'
                    }, {
                        name: 'En Uso',
                        data: [0, data[0].uso, 0],
                        color: '<?= $color2 ?>'
                    }, {
                        name: 'Compras',
                        data: [data[0].compra, 0, 0],
                        color: '<?= $color1 ?>'
                    }];

            $(function () {
                $('#containerDetalle').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                         text: data[0].titulo
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: ['', '', ''],
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                         stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    legend: {
                       reversed: true
                    },
                    plotOptions: {
                        dataLabels: {
                            enabled: true
                        },
                        series: {
                           stacking: 'normal'
                        }
                    },
                    tooltip: {
                        headerFormat: ''
                    },
                    series: serie
                });
            });

            $("#tablaDetalle").append(data[0].tabla);
            $("#divTabla").show();
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
            });
        });
        
        $("#exportarTodo").click(function(){
            window.open("<?= $GLOBALS["domain_root"] ?>/VMWare/reportes/excelBalanzaTodos.php");
        });
    }
</script>