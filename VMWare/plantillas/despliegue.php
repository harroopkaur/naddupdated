<input type="hidden" id="nombreArchivo">

<div style="width:98%; padding:20px; overflow:hidden;">
    <div style="width:auto;">
        <h1 class="textog negro" style="text-align:center; line-height:35px;" id="principal">Despliegue</h1>
    </div>
    <br><br>

    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, favor extraer licencias de ESXi administrados por vCenter Server</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1.-&nbsp;</span>Se ejecuta vSphere Client y accede a vCenter Server utilizando las credenciales Administrador. En este caso utiliza el usuario por defecto <strong style="font-weight:bold;">Administrator@vsphere.local</strong></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Una vez dentro seleccionar Home</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>En el panel de Administraci&oacute;n, seleccionar Licensing</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>Seleccionar Export para extraer la lista en formato CSV:</p><br>
    <!--<p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">NOTA:&nbsp;</span>Si existen ESXi hosts que no est&aacute;n siendo administrados por vCenter se debe realizar el proceso manual para cada uno de ellos explicado en la tercera parte: Extraer licencias de ESXi no administrados por vCenter Server.</p><br>-->
    <br>

    <fieldset class="fieldset">
        <legend style=" font-weight:bold; margin-left:15px;">LAD_Output</legend>
        
        <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="despliegue1.php" >
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <input name="opcionDespliegue" id="opcionCompleto" type="hidden" value="completo" >
            <div class="contenedor_archivo">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivo" id="archivo" accept=".csv">
                    <input type="button" id="boton1" class="botones_m5" value="Buscar">
                </div>
                <div class="contenedor_boton_archivo">
                    <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="showhide('cargando');document.getElementById('form_e').submit();" class="botones_m5" />
                </div>
            </div>
        </form>

        <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
        <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
        <?php if($exito=='1'){ ?>
            <div class="exito_archivo"><?= $general->getMensajeExito($_SESSION["idioma"]) ?></div>
            <div class="text-center " style="color: #2d6ba4;"><?= $general->getAdverCompras($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('info', '<?= $general->getMensajeExito($_SESSION["idioma"]) . "<br>". $general->getAdverCompras($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }else{ ?>

        <?php }
        if($error=='1'){ ?>
            <div class="error_archivo"><?= $general->getMensajeRar($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeRar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'});
            </script>
        <?php }
        if($error=='2'){ ?>
            <div class="error_archivo"><?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeSeleccionar($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error==3){ ?>
            <div class="error_archivo"><?= $general->getMensajeCabVMWare($_SESSION["idioma"]) ?></div>
            <script>
                $.alert.open('warning', 'Alert', '<?= $general->getMensajeCabVMWare($_SESSION["idioma"]) ?>', {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error == 5){ ?>
            <div class="error_archivo"><?=$consolidado->error?></div>
            <script>
                $.alert.open('warning', 'Alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error == 6){ ?>
            <div class="error_archivo"><?=$general->error?></div>
            <script>
                $.alert.open('warning', 'Alert', "<?=$general->error?>", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php } ?>
    </fieldset>
    
    <br />

    <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='contratos.php';">Contratos</div></div>
</div>

<script>
    $(document).ready(function(){
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
    });
    
    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }
    
    function fileOnload(e) {
        result=e.target.result;
    }
</script>