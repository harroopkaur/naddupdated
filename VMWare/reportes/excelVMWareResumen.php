<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_VMWare.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
            die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"
    $vert = 0;
    if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
        $vert = $_POST["vert"];
    }
    
    if($vert == 0){
        $compras  = new comprasVMWare();
        $listadoContrato = $compras->listarDetalleCompra($_SESSION["client_id"], $_SESSION['client_empleado']);
    } else if($vert == 1){
        $balance = new balanceVMWare();
        
        $familia = "";
        if(isset($_POST["familiaExcel"])){
            $familia = $general->get_escape($_POST["familiaExcel"]);
        }
        
        $edicion = "";
        if(isset($_POST["edicionExcel"])){
            $edicion = $general->get_escape($_POST["edicionExcel"]);
        }
    }

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Licensingassurance")
                                 ->setTitle("");


    // Add some data
    if($vert == 0){
        $nombre = "excelVMWareResumenContratos.xlsx";
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', '')
                    ->setCellValue('B1', 'Producto')
                    ->setCellValue('C1', 'Edición')
                    ->setCellValue('D1', 'Versión')
                    ->setCellValue('E1', 'Llave')
                    ->setCellValue('F1', 'Cantidad')
                    ->setCellValue('G1', 'Medida')
                    ->setCellValue('H1', 'Tipo')
                    ->setCellValue('I1', 'Número')
                    ->setCellValue('J1', 'Fecha de Orden')
                    ->setCellValue('K1', 'Soporte')
                    ->setCellValue('L1', 'Fecha Fin Cobertura');
        
        if($listadoContrato){
            $i=2;
            $j=1;
            foreach($compras->listado as $row){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $j)
                    ->setCellValue('B' . $i, $row["producto"])
                    ->setCellValue('C' . $i, $row["edicion"])
                    ->setCellValue('D' . $i, $row["version"])
                    ->setCellValue('E' . $i, $row["llave"])
                    ->setCellValue('F' . $i, $row["cantidad"])
                    ->setCellValue('G' . $i, $row["medida"])
                    ->setCellValue('H' . $i, $row["tipo"])
                    ->setCellValue('I' . $i, $row["numero"])
                    ->setCellValue('J' . $i, $row["fechaOrden"])
                    ->setCellValue('K' . $i, $row["soporte"])
                    ->setCellValue('L' . $i, $row["fechaFinCobertura"]);
                $i++;
                $j++;
            }
        }
            
    } else if($vert == 1){      
        $titulo       = "";

        $nombre = "excelVMWareResumen.xlsx";
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Producto')
                    ->setCellValue('B1', 'Edición')
                    ->setCellValue('C1', 'Versión')
                    ->setCellValue('D1', 'Instalaciones')
                    ->setCellValue('E1', 'Compras')
                    ->setCellValue('F1', 'Neto');

        $i = 2;
        
        if($familia != "Otros" && $edicion == ""){
            $titulo = $familia;
            if($balance->listar_todo_familias2($_SESSION['client_id'], $_SESSION['client_empleado'], $familia)) {
                foreach ($balance->lista as $reg_equipos) {
                    $balance->imprimirTablaExcelBalanza($balance->lista, $objPHPExcel, $i);
                }
            }
        }
        else if($familia != "Otros" && $edicion != ""){
            $titulo = $familia . " " . $edicion;
            if($balance->listar_todo_familias($_SESSION['client_id'], $_SESSION['client_empleado'], $familia, $edicion)) {
                foreach ($balance->lista as $reg_equipos) {
                    $balance->imprimirTablaExcelBalanza($balance->lista, $objPHPExcel, $i);
                }
            }
        }
        else if($familia == "Otros"){
            $titulo    = $edicion;
            if($balance->listar_todo_familias1($_SESSION['client_id'], $_SESSION['client_empleado'], $edicion)) {
                foreach ($balance->lista as $reg_equipos) {
                    $balance->imprimirTablaExcelBalanza($balance->lista, $objPHPExcel, $i);
                }
            }
        }
    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesion($mensaje);
}