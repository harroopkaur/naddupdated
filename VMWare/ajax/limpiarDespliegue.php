<?php
require_once("../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_consolidado_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_archivos_fabricantes.php");
require_once($GLOBALS["app_root"] . "/clases/clase_balance_VMWare.php");
require_once($GLOBALS["app_root"] . "/clases/clase_compras_VMWare.php");

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $resumen      = new resumenVMWare();
            $consolidado  = new consolidadoVMWare();
            $archivosDespliegue = new clase_archivos_fabricantes();

            $compra_datos  = new comprasVMWare();
            $balance_datos = new balanceVMWare();
            
            $i = 6;
            $j = 0;
            if($consolidado->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($resumen->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
           
            if($archivosDespliegue->eliminarArchivosFabricante($_SESSION["client_id"], $_SESSION["client_empleado"], 6)){
                $j++;
            }
            
            if($compra_datos->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($compra_datos->eliminarDetalle($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            if($balance_datos->eliminar($_SESSION['client_id'], $_SESSION['client_empleado'])){
                $j++;
            }
            
            $result = 0;
            if($i > $j){
                $result = 2;
            } else if($i == $j){
                $result = 1;
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);