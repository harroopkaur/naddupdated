<?php
require("configuracion/inicio.php");
require("clases/clase_general.php");
require("clases/clase_validator.php");
require("clases/clase_clientes.php");
require("clases/clase_email.php");

$usuario = new Clientes($conn);
$general = new General();
$validator = new validator("form1");
$email = new email();


$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    $login = "";
    if(isset($_POST['login'])){
        $login = $general->get_escape($_POST['login']);
    }
    
    if (!$usuario->login_existe($login, 0)) {
        $error = 2;
    }  // Email duplicado

    if ($error == 0) {
        if ($usuario->recuperar_contrasena($login)) {

            $email->recuperar_clave($usuario->nombre, $usuario->apellido, $login, $usuario->correo, $usuario->clave);
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}


$validator->create_message("msj_login", "login", "Campo Obligatorio", 0);
?>
<!DOCTYPE HTML>
<html>
    <head>

        <?php
        require("plantillas/head.php");

        if ($exito == true) {
        ?>
            &nbsp;
            <script type="text/javascript">

                $.msgBox({
                    title: "Reporte",
                    content: "Recuperacion de contrase�a con exito",
                    buttons: [{value: "Aceptar"}],
                    success: function () {
                        location.href = '<?= $GLOBALS['domain_root'] ?>/index.php';
                    }
                });

            </script>
        <?php 
        }
        ?>

        <style>
            body {
                background:url(imagenes/inicio/bg_body.jpg) top left no-repeat !important;	
            }
        </style>
        <title>.:Licensing Assurance:.</title>
        <!-- Custom Theme files -->
        <link href="css/style3.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <meta name="keywords" content="trial, sofware" />
        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
    </head>
    <body>
        <!--sign up form start here-->
        <div style="width:100%; position:relative; text-align:center;">
            <div class="app" style=" display:inline-block; overflow:hidden;">
                <h1 style="font-size:24px;color:#fff;">Bienvenido al Servicio de Manejo de Licenciamiento en la nube </h1>
                <br>
                <div style="overflow:hidden; padding:20px;">
                    <img src="imagenes/inicio/logo2.png"  style="margin:0 auto; display:block;">
                </div>

            </div>

            <div class="app" style=" display:inline-block; margin-left:25px; overflow:hidden;   padding-top:30px;padding-bottom: 30px;">
                <div style="overflow:hidden; width:60%; margin:0 auto; background:#F7F7F7; padding:20px; border-radius:10px;">

                    <h1 style="font-size:24px;color:#000; text-align:left;">Recuperar Contrase&ntilde;a</h1> <br>

                    <form id="form1" name="form1" method="post" action="olvido.php" enctype="multipart/form-data">
                        <input type="hidden" name="insertar" id="insertar" value="1" />
                        <?php $validator->print_script(); ?>

                        <input type="text" name="login" id="login"  placeholder="Login" />
                        <input type="button" onClick="validate();" value="Enviar" />
                    </form>
                    <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                        echo $usuario->error;
                    } ?></font></div>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?><?php if($error == 2){ echo "No existe ese Login"; }?></font></div>

                </div>
                <div style="width:100%; height:20px; min-height:20px; clear:both;">&nbsp;</div>
            </div>
        </div>
        <div class="copyright" >
            <p>Todos Los Derechos Reservados 2016 - <a href="https://www.licensingassurance.com"> www.licensingassurance.com </a></p>
        </div>
        <!--sign up form end here-->
    </body>
</html>