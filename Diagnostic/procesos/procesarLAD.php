<?php
$_SESSION["idioma"] = 2;
// Clases 
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_consolidado.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_resumen.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php"); 
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_escaneo.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_detalles_equipo.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_consolidado_procesadores.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_consolidado_tipo_equipo.php");
//require_once($GLOBALS["app_root1"] . "/SAMDiagnostic/clases/clase_usuario_equipo_microsoft.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/SAMDiagnostic.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");

// Objetos
$consolidado = new ConsolidadoSAMDiagnostic();
$resumen = new ResumenSAMDiagnostic();
$tablaMaestra  = new tablaMaestra();
$general = new General();
$escaneo = new ScaneoSAMDiagnostic();
$detalles = new DetallesSAMDiagnostic();
$procesadores = new ConsolidadoProcSAMDiagnostic();
$tipoEquipo = new ConsTipoEquipoSAMDiagnostic();
$cabecera = new SAMDiagnostic();
$passLAD = new clase_pass();
$fabricante = 3;

$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$idDiagnosticResult = 0;
$lista = array();

$opcionDespliegue = "";

if(isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
        $nombre_imagen = $_FILES['archivo']['name'];
        $tipo_imagen = $_FILES['archivo']['type'];
        $tamano_imagen = $_FILES['archivo']['size'];
        $temporal_imagen = $_FILES['archivo']['tmp_name'];

        // Validaciones
        if($nombre_imagen!=""){
            $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
            // Permitir subir solo imagenes JPG,
        }else{
            $error=2;	
        }

        if($error == 0){
            $listPassLAD = $passLAD->listar_todo();
            $pass = "";
           
            foreach($listPassLAD as $row){  
                $psw = $passLAD->desencriptar($row["descripcion"]);
                $rar_file = rar_open($temporal_imagen, $psw) or die("Can't open Rar archive");

                $entries = rar_list($rar_file);   
                
                $direcEntry = $GLOBALS["app_root"] . '/tmp';

                if(!file_exists($direcEntry)){
                    mkdir($direcEntry, 0755);
                }

                foreach ($entries as $entry) {
                    if($entry->extract($direcEntry)){
                        $pass = $psw;
                        $files = array_diff(scandir($direcEntry), array('.','..')); 
                        foreach ($files as $file) { 
                            unlink($direcEntry.'/'.$file); 
                        } 
                        rmdir($direcEntry);  
                    } 
                    
                    break;
                }
                
                if($pass != ""){
                    break;
                }
            }
            
            rar_close($rar_file);
            
            if($pass != ""){
                $rar_file = rar_open($temporal_imagen, $pass) or die("Can't open Rar archive");
                $entries = rar_list($rar_file);
    
                $archivosExtraer = 5;
                $archivosExtraidos = 0;
        
                $base = $GLOBALS["app_root1"] . '/Diagnostic/archivosLAD/';
                $archivoConsolidado = "Consolidado Addremove" . date("dmYHis") . ".csv";
                $archivoProcesadores = "Consolidado Procesadores" . date("dmYHis") . ".csv";
                $archivoEquipos = "Consolidado Tipo de Equipo" . date("dmYHis") . ".csv";
                //$archivoUsuarioEquipo = "Consolidado Usuario-Equipo" . date("dmYHis") . ".csv";
                $archivoEscaneo = "Resultados_Escaneo" . date("dmYHis") . ".csv";
                $archivoSQL = "Consolidado SQL" . date("dmYHis") . ".csv";
                foreach ($entries as $entry) {
                    if($entry->getName() == "Consolidado Addremove.csv" && $entry->extract(false, $base . $archivoConsolidado)){
                        $archivosExtraidos++;
                    }
                    
                    if($entry->getName() == "Consolidado Procesadores.csv" && $entry->extract(false, $base . $archivoProcesadores)){
                        $archivosExtraidos++;
                    }
         
                    if($entry->getName() == "Consolidado Tipo de Equipo.csv" && $entry->extract(false, $base . $archivoEquipos)){
                        $archivosExtraidos++;
                    }
                   
                    if($entry->getName() == "Resultados_Escaneo.csv" && $entry->extract(false, $base . $archivoEscaneo)){
                        $archivosExtraidos++;
                    }
                    
                    if($entry->getName() == "Consolidado SQL.csv" && $entry->extract(false, $base . $archivoSQL)){
                        $archivosExtraidos++;
                    }
                }
                rar_close($rar_file);
        
                if($archivosExtraer > $archivosExtraidos){
                    $error = 8;
                }
            } else{
                $error = -2;
            }
        }
        
        if($error == 0) {  
            if($general->obtenerSeparador($base . $archivoConsolidado) === true){
                if (($fichero = fopen($base . $archivoConsolidado, "r")) !== false) {
                    $i=1;
                    $iDatoControl = $iHostName = $iRegistro = $iEditor = $iVersion = $iDiaInstalacion = $iSoftware = -1;
                    $procesarAddRemove = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                    $iDatoControl = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "HostName"){
                                    $iHostName = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Registro"){
                                    $iRegistro = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Editor"){
                                    $iEditor = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Version" || trim(utf8_encode($datos[$k])) == "Versión"){
                                    $iVersion = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Día de Instalacion" || trim(utf8_encode($datos[$k])) == "Día de Instalación"){
                                    $iDiaInstalacion = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Software"){
                                    $iSoftware = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }
                        /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Registro" 
                        || $datos[3] != "Editor" || utf8_encode($datos[4]) != "Versión" || utf8_encode($datos[5]) != "Día de Instalación" || $datos[6] != "Software")){
                            $error = 3;
                            break;
                        }*/
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($iDatoControl == -1 && $iHostName == -1 && $iRegistro == -1 && $iEditor == -1 && 
                    $iVersion == -1 && $iDiaInstalacion == -1 && $iSoftware == -1){
                        $procesarAddRemove = false;
                    } else if($iDatoControl == -1 || $iHostName == -1 || $iRegistro == -1 || $iEditor == -1 && 
                    $iVersion == -1 || $iDiaInstalacion == -1 || $iSoftware == -1){
                        $error = 3;
                    }
                }
            } else{
                //$error = 10;
                $procesarAddRemove = false;
            }

            if($general->obtenerSeparador($base . $archivoEscaneo) === true){
                if (($fichero = fopen($base . $archivoEscaneo, "r")) !== false) {
                    $i=1;
                    $jHostname = $jStatus = $jError = -1;
                    $procesarEscaneo = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Hostname"){
                                    $jHostname = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Status"){
                                    $jStatus = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Error"){
                                    $jError = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }
                        /*if($i == 2 && ($datos[0] != "Hostname" ||  $datos[1] != "Status" || $datos[2] != "Error")){
                            $error = 4;
                            break;
                        }*/
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($jHostname == -1 && $jStatus == -1 && $jError == -1){
                        $procesarEscaneo = false;
                    } else if($jHostname == -1 || $jStatus == -1 || $jError == -1){
                        $error = 4;
                    }
                }
            } else{
                //$error = 11;
                $procesarEscaneo = false;
            }

            if($general->obtenerSeparador($base . $archivoProcesadores) === true){    
                if (($fichero = fopen($base . $archivoProcesadores, "r")) !== false) {
                    $i=1;
                    $kDatoControl = $kHostName = $kTipoCPU = $kCPUs = $kCores = $kProcesadoresLogicos = $kTipoEscaneo = -1;
                    $procesarProcesadores = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                    $kDatoControl = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "HostName"){
                                    $kHostName = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Tipo de CPU"){
                                    $kTipoCPU = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "CPUs"){
                                    $kCPUs = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "Cores"){
                                    $kCores = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "Procesadores Logicos" || trim(utf8_encode($datos[$k])) == "Procesadores Lógicos"){
                                    $kProcesadoresLogicos = $k;
                                }
                                
                                if(strpos(trim(utf8_encode($datos[$k])), "Tipo de Escaneo") !== false){
                                    $kTipoEscaneo = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }
                        /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Tipo de CPU" || 
                        $datos[3] != "CPUs" || $datos[4] != "Cores" || utf8_encode($datos[5]) != "Procesadores Lógicos" || strpos($datos[6], "Tipo de Escaneo") === false)){
                            $error = 6;
                            break;
                        }*/
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($kDatoControl == -1 && $kHostName == -1 && $kTipoCPU == -1 && $kCPUs == -1 && $kCores == -1 
                    && $kProcesadoresLogicos == -1 && $kTipoEscaneo == -1){
                        $procesarProcesadores = false;
                    } else if($kDatoControl == -1 || $kHostName == -1 || $kTipoCPU == -1 || $kCPUs == -1 || $kCores == -1 
                    || $kProcesadoresLogicos == -1 || $kTipoEscaneo == -1){
                        $error = 6;
                    }
                }
            } else{
                //$error = 12;
                $procesarProcesadores = false;
            }

            if($general->obtenerSeparador($base . $archivoEquipos) === true){
                if (($fichero = fopen($base . $archivoEquipos, "r")) !== false) {
                    $i=1;
                    $lDatoControl = $lHostName = $lFabricante = $lModelo = -1;
                    $procesarEquipos = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                    $lDatoControl = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "HostName"){
                                    $lHostName = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Fabricante"){
                                    $lFabricante = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "Modelo"){
                                    $lModelo = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }
                        /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || $datos[2] != "Fabricante" || $datos[3] != "Modelo")){
                            $error = 7;
                            break;
                        }*/
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($lDatoControl == -1 && $lHostName == -1 && $lFabricante == -1 && $lModelo == -1){
                        $procesarEquipos = false;
                    } else if($lDatoControl == -1 || $lHostName == -1 || $lFabricante == -1 || $lModelo == -1){
                        $error = 7;
                    }
                }
            } else{
                //$error = 13;
                $procesarEquipos = false;
            }

            if($general->obtenerSeparadorUniversal($base . $archivoSQL, 2, 4) === true){
                if (($fichero = fopen($base . $archivoSQL, "r")) !== false) {
                    $i=1;
                    $nDatoControl = $nHostName = $nEdicionSQL = $nVersionSQL = -1;
                    $procesarSQL = true;
                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                        if($i == 2){
                            for($k = 0; $k < count($datos); $k++){
                                if(trim(utf8_encode($datos[$k])) == "Dato de Control"){
                                    $nDatoControl = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "HostName"){
                                    $nHostName = $k;
                                }

                                if(trim(utf8_encode($datos[$k])) == "Edicion de SQL:" || trim(utf8_encode($datos[$k])) == "Edición de SQL:"){
                                    $nEdicionSQL = $k;
                                }
                                
                                if(trim(utf8_encode($datos[$k])) == "Version de SQL:" || trim(utf8_encode($datos[$k])) == "Versión de SQL:"){
                                    $nVersionSQL = $k;
                                }
                            }
                        } else if($i > 2){
                            break;
                        }
                        /*if($i == 2 && ($datos[0] != "Dato de Control" || $datos[1] != "HostName" || 
                        trim(utf8_encode($datos[3])) != "Edición de SQL:" || trim(utf8_encode($datos[4])) != "Versión de SQL:")){
                            $error = 16;
                            break;
                        }*/
                        $i++;
                    }
                    fclose($fichero);
                    
                    if($nDatoControl == -1 && $nHostName == -1 && $nEdicionSQL == -1 && $nVersionSQL == -1){
                        $procesarSQL = false;
                    } else if($nDatoControl == -1 || $nHostName == -1 || $nEdicionSQL == -1 || $nVersionSQL == -1){
                        $error = 16;
                    }
                }
            } else{
                //$error = 17;
                $procesarSQL = false;
            }
        }
    }
    else{
        $error = -1;
    }

    $idDiagnostic = 0;
    if(isset($_POST["idDiagnostic"]) && filter_var($_POST["idDiagnostic"], FILTER_VALIDATE_INT) !== false){
        $idDiagnostic = $_POST["idDiagnostic"];
    }
    
    if($error == 0 && $idDiagnostic == 0){
        $nombreArchivo = 'LAD_Output' . date('dmYHis') . '.rar';
        if(!$cabecera->insertar($nombreArchivo)){
            $error = -2;
        }
        else{
            $idDiagnostic = $cabecera->ultId();
        }
    }
    
    if($error == 0) {   
        move_uploaded_file($_FILES['archivo']['tmp_name'], $base . $nombreArchivo);
        
        $consolidado->eliminar($idDiagnostic);
        
        if($general->obtenerSeparador($base . $archivoConsolidado) === true && $procesarAddRemove === true){
            if (($fichero = fopen($base . $archivoConsolidado, "r")) !== false) {
                $i = 1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;
                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {                 
                    if($i > 2 && isset($datos[$iHostName]) && $datos[$iHostName] != "" && $datos[$iHostName] != "HostName"){
                        if($j == 0){
                            $insertarBloque = true;
                            $bloque .= "(:idDiagnostic" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                            . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                        } else {
                            $bloque .= ", (:idDiagnostic" . $j . ", :dato_control" . $j . ",:host_name" . $j . ", "
                            . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
                        } 
                                                
                        $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                        $bloqueValores[":dato_control" . $j] = $general->truncarString($datos[$iDatoControl], 250);
                        $bloqueValores[":host_name" . $j] = $general->truncarString($datos[$iHostName], 250);
                        $bloqueValores[":registro" . $j] = $datos[$iRegistro];
                        $bloqueValores[":editor" . $j] = $general->truncarString($datos[$iEditor], 250);
                        $bloqueValores[":version" . $j] = $general->truncarString($datos[$iVersion], 250);
                        $bloqueValores[":fecha_instalacion" . $j] = $datos[$iDiaInstalacion];
                        if(filter_var($datos[$iDiaInstalacion], FILTER_VALIDATE_INT) === false){
                            $bloqueValores[":fecha_instalacion" . $j] = 0;
                        } 
                        $bloqueValores[":sofware" . $j] = $general->truncarString(utf8_encode($datos[$iSoftware]), 250);
                        
                        if($j == $general->registrosBloque){
                            if(!$consolidado->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo "addRemove: " . $consolidado->error."<br>";
                            }
                            
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }
                    $exito=1;	

                    $i++; 
                }
                
                if($insertarBloque === true){
                    if(!$consolidado->insertarEnBloque($bloque, $bloqueValores)){  
                        echo "addRemove: " . $consolidado->error."<br>";
                    }
                }
            }
        }

        //inicio resumen office       
        $resumen->eliminar($idDiagnostic);
        $tablaMaestra->listadoProductosMaestraActivo($fabricante);
        foreach($tablaMaestra->listaProductos as $rowProductos){
            if(strpos($rowProductos['descripcion'], 'Windows') !== false || strpos($rowProductos['descripcion'], 'SQL') !== false){
                continue;
            }            
            $lista = $consolidado->listarProductosMicrosoft($idDiagnostic, $rowProductos["campo3"], $rowProductos["campo2"]); 
            
            $j = 0;
            $bloque = "";
            $bloqueValores = array();
            $insertarBloque = false;

            foreach($lista as $reg_r){
                $edicion='0';
                $host=explode('.',$reg_r["host_name"]);
                if(filter_var($reg_r["host_name"], FILTER_VALIDATE_IP)!== false){
                    $host[0] = $reg_r["host_name"];
                }
                
                $version = $tablaMaestra->buscarVersion($reg_r["sofware"]);

                $f3 = substr($reg_r["fecha_instalacion"], 6, 2);
                $f2 = substr($reg_r["fecha_instalacion"], 4, -2);
                $f1 = substr($reg_r["fecha_instalacion"], 0, 4);
                
                if($reg_r["fecha_instalacion"] == 0){
                    $fecha_instalacion='0000-00-00';
                } else{
                    $fecha_instalacion=$f1.'-'.$f2.'-'.$f3;
                }
                

                $producto = $reg_r["sofware"];

                //$resumenOf->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $host[0],'Office', $tablaMaestra->filtrarProducto($producto, $version, 1, 2), $version, $fecha_instalacion);
                if($j == 0){
                    $insertarBloque = true;
                    $bloque .= "(:idDiagnostic" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                    . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                } else {
                    $bloque .= ", (:idDiagnostic" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                    . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                }
                
                $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                $bloqueValores[":equipo" . $j] = $host[0];
                $bloqueValores[":familia" . $j] = $rowProductos["descripcion"];
                $bloqueValores[":edicion" . $j] = $tablaMaestra->filtrarProducto($producto, $version, $rowProductos["idForaneo"], 3); //3 es el fabricante Microsoft
                $bloqueValores[":version" . $j] = $version;
                $bloqueValores[":fecha_instalacion" . $j] = $fecha_instalacion;
                
                if($j == $general->registrosBloque){
                    if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){ 
                        echo "Resumen: " . $resumen->error . "<br>";
                    }
                    $bloque = "";
                    $bloqueValores = array();
                    $j = -1;
                    $insertarBLoque = false; 
                }
                $j++;
            }

            if($insertarBloque === true){
                if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){  
                    echo "Resumen: " . $resumen->error . "<br>";
                }
            }
        }
        //fin resumen office
        
        //inicio agregar consolidado SQL
        if($general->obtenerSeparadorUniversal($base . $archivoSQL, 2, 4) === true && $procesarSQL === true){
            if (($fichero = fopen($base . $archivoSQL, "r")) !== FALSE) {
                $i=1;
                $j = 0;
                $bloque = "";
                $bloqueValores = array();
                $insertarBloque = false;

                while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                    if($i > 2 && $datos[$nDatoControl] != "Dato de Control"){
                        $host=explode('.',$datos[$nHostName]);
                        if(filter_var($datos[$nHostName], FILTER_VALIDATE_IP)!== false){
                            $host[0] = $datos[$nHostName];
                        }

                        $edicion = trim(str_replace("Edition", "", $datos[$nEdicionSQL]));
                        $version = $tablaMaestra->convertirVersionSQL($datos[$nVersionSQL]);

                        if($j == 0){
                            $insertarBloque = true;
                            $bloque .= "(:idDiagnostic" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                            . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                        } else {
                            $bloque .= ", (:idDiagnostic" . $j . ", :equipo" . $j . ", :familia" . $j . ", "
                            . ":edicion" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ")";
                        } 

                        $bloqueValores[":idDiagnostic" . $j] = $idDiagnostic;
                        $bloqueValores[":equipo" . $j] = $host[0];
                        $bloqueValores[":familia" . $j] = "SQL Server";
                        $bloqueValores[":edicion" . $j] = $edicion;
                        $bloqueValores[":version" . $j] = $version;
                        $bloqueValores[":fecha_instalacion" . $j] = "0000-00-00";

                        if($j == $general->registrosBloque){
                            if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){ 
                                echo $resumen->error;
                            }
                            $bloque = "";
                            $bloqueValores = array();
                            $j = -1;
                            $insertarBLoque = false; 
                        }
                        $j++;
                    }
                    $i++;
                }
                fclose($fichero);

                if($insertarBloque === true){
                    if(!$resumen->insertarEnBloque($bloque, $bloqueValores)){  
                        echo $resumen->error;
                    }
                }
            }
        }
        //fin agregar consolidado SQL
        $exito=1;  
    }
}