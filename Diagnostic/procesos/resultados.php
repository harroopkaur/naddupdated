<?php
// Clases
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_detalles_equipo.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/clase_resumen.php");
require_once($GLOBALS["app_root1"] . "/Diagnostic/clases/SAMDiagnostic.php");
require_once($GLOBALS["app_root"] . "/clases/clase_email.php");

// Objetos
$detalles = new DetallesSAMDiagnostic();
$resumen = new ResumenSAMDiagnostic();
$cabecera = new SAMDiagnostic();
$general = new General();

$error = 0;
$idDiagnostic = 0;
if(isset($_POST["idDiagnostic"]) && filter_var($_POST["idDiagnostic"], FILTER_VALIDATE_INT)){
    $idDiagnostic = $_POST["idDiagnostic"];
}

$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $general->get_escape($_POST["nombre"]);
}

$email = "";
if(isset($_POST["email"])){
    $email = $general->get_escape($_POST["email"]);
}

$reportePDF = 0;
if(isset($_POST["reportePDF"]) && filter_var($_POST["reportePDF"], FILTER_VALIDATE_INT) !== false){
    $reportePDF = $_POST["reportePDF"];
}
$fallas = 10;
$primero = false;
$segundo = false;
$tercero = false;
$cuarto = false;
$quinto = false;
$sexto = false;
$septimo = false;
$octavo = false;
$noveno = false;
$decimo = false;

if($idDiagnostic > 0){  
    $fallas = 0;
    $totalEquiposActivos = $detalles->totalEquiposActivos($idDiagnostic);
    $totalEquiposLevantados = $detalles->totalEquiposLevantados($idDiagnostic);
    $totalEquipos = $detalles->totalEquipos($idDiagnostic);
    
    $porcLevantado = 0;
    if($totalEquiposActivos > 0){
        $porcLevantado = round(($totalEquiposLevantados / $totalEquiposActivos) * 100, 0);
    }
    
    if($porcLevantado > 90){
        $segundo = true;
    }else{
        $fallas++;
    }
    
    $porcActivos = 0;
    if($totalEquipos > 0){
        $porcActivos = round(($totalEquiposActivos / $totalEquipos) * 100, 0);
        //$porcLevantado = round(($scanChallenges / $totalEquipos) * 100, 0);
    }
    
    if($porcActivos <= 90){
        $fallas++;
    } else{
        $primero = true;
    }
    
    /*$scanChallenges = $detalles->scanChallenges($idDiagnostic);
    if($scanChallenges > 1){
        $fallas++;
    } else{
        $segundo = true;
    }*/
    
    $totalEquiposUso = $detalles->totalEquiposUso($idDiagnostic);
    /*$porcUso = round(($totalEquiposUso / $totalEquipos) * 100, 0);
    if($porcUso < 75){
        $fallas++;
    } else{
        $tercero = true;
    }*/
    $UnusedDevice = $totalEquipos - $totalEquiposActivos;
    //$porcEstUnusedDevice = round(($UnusedDevice / $totalEquipos) * 100, 0);
    $averageDevice = 0;
    if($totalEquiposLevantados > 0){
        $averageDevice = round(($detalles->totalAplicaciones($idDiagnostic) / $totalEquiposLevantados), 0); 
    }
    
    if($UnusedDevice > 1){
        $fallas++;
    } else{
        $tercero = true;
    }
    
    $unusedSW = $detalles->unusedSW($idDiagnostic);
    if($unusedSW == 0){
        $cuarto = true;
    } else{
        $fallas++;
    }
    //$porcEstApplications = round(($unusedSW / $totalEquipos) * 100, 0);
    $porcEstApplications = 0;
    if($porcLevantado > 0){
        $porcEstApplications = round(($unusedSW / $porcLevantado) * 100, 0);
    }
   
    //$softwareDuplicate = count($detalles->softwareDuplicate($idDiagnostic));
    
    $tabla = $detalles->softwareDuplicate($idDiagnostic);
    $dup = 0;
    foreach($tabla as $row){
        $dup += $row["cantidad"];
    }
    
    $softwareDuplicate = $dup;
    if($softwareDuplicate == 0){
        $quinto = true; 
    } else{
        $fallas++;
    }
    //$porcEstDuplicate = round(($softwareDuplicate / $totalEquipos) * 100, 0);  
    $porcEstDuplicate = 0;
    if($porcLevantado > 0){
        $porcEstDuplicate = round(($softwareDuplicate / $porcLevantado) * 100, 0); 
    }
    
    $erroneus = $detalles->erroneus($idDiagnostic);
    if($erroneus == 0){
        $sexto = true;
    } else{
        $fallas++;
    }
    //$porcEstErroneus = round(($erroneus / $totalEquipos) * 100, 0);
    $porcEstErroneus = 0;
    if($porcLevantado > 0){
        $porcEstErroneus = round(($erroneus / $porcLevantado) * 100, 0);  
    }
    
    $totalOffice = $detalles->totalOffice($idDiagnostic);
    $totalOffice365 = $detalles->totalOffice365($idDiagnostic);
    
    $cloud = 0;
    if($totalOffice > 0){
        $cloud = round(($totalOffice365 / ($totalOffice + $totalOffice365)) * 100, 0);
    }
    
    if($cloud >= 50){
        $septimo = true;
    } else{
        $fallas++;
    }
    
    $totalServidorFisico = $detalles->totalServidorFisico($idDiagnostic);
    $totalServidorVirtual = $detalles->totalServidorVirtual($idDiagnostic);   
    if($totalServidorFisico == 0){
        $relacionServidores = 0;
    } else{
        $relacionServidores = round($totalServidorVirtual / $totalServidorFisico, 0);
    }
    /*if($relacionServidores >= 3){
        $octavo = true;
    } else{
        $fallas++;
    }*/
    
    if($totalServidorVirtual > $totalServidorFisico){
        $octavo = true;
    } else{
        $fallas++;
    }
    
    $productosSinSoporte = $resumen->productosSinSoporte($idDiagnostic);
    if($productosSinSoporte == 0){
        $noveno = true;
    } else{
        $fallas++;
    }
    
    $productosSinSoporte1 = $resumen->productosSinSoporte1($idDiagnostic);
    $soft = "";
    /*foreach($productosSinSoporte1 as $row){
        $soft .= $row["familia"] . ": " . $row["cantidad"] . "    ";
    }*/
    
    $i = 0;
    foreach($productosSinSoporte1 as $row){
        if($row["familia"] == "Windows Server"){
            $i++;
        }
        $soft = $row["familia"];
    }
    
    if($i > 0){
        $soft = "Windows Server";
    }
    
    if(count($productosSinSoporte1) > 1){
        $soft .= " and others";
    }
    
    $decimo = false;
    
    
    if($reportePDF == 0){     
        ob_start();
        $html = ob_get_clean();
        $html = utf8_encode($html);
        $html .= '<div style="width:100%; height:auto; overflow:hidden;" id="contPOC">
            <h1 class="tituloSAMDiagnostic1 colorBlanco">Deployment Diagnostic Report</h1>

            <br>

            <style>
                .tituloSAMDiagnostic{
                    font-size:20px;
                    color: #000000;
                    font-weight: bold;
                }

                .tituloSAMDiagnostic2{
                    font-size:18px;
                    color: #000000;
                    font-weight: bold;
                }

                .tituloSAMDiagnostic1{
                    font-size:35px;
                    text-align: center;
                    font-weight: bold;
                    color: #000000;
                }

                .tituloImagenes{
                    font-size:25px; 
                    line-height:82px; 
                    font-weight:bold; 
                    padding-top:-30px;
                    color: #000000;
                }

                .tablapS {     
                    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
                    font-size: 15px;   
                    margin: 0 auto;   
                    width: 90%; 
                    text-align: left;    
                    border-collapse: collapse; 
                }

                .tablapS thead th {     
                    font-size: 25px;     
                    font-weight: bold;     
                    padding: 15px;     
                    background: #377EBA;
                    border-bottom: 2px solid #fff;    
                    color: #ffffff; 
                    text-align: center; 
                }

                .tablapS thead th.transTopLeft{
                    background-color: transparent;
                }

                .tablapS thead th.trans{
                    background-color: transparent;
                }

                .tablapS tbody td.trans{
                    background-color: transparent;
                    font-size: 20px;
                }

                .tablapS tbody td.trans1{
                    background-color: transparent;
                    border: 0;
                }

                .tablapS tbody td.trans2{
                    background-color: transparent;
                    border: 0;
                    font-size: 20px;
                }

                .tablapS tbody td.titulo{
                    font-size: 20px;
                }

                .tablapS tbody th {  
                    padding: 8px;     
                    background: #5C96C6;     
                    border: 2px solid #fff;
                    color: #000000;  
                    text-align: center;
                    vertical-align: middle;
                    font-weight: normal;
                }

                .tablapS tbody td {    
                    padding: 8px;     
                    background: #A0C3DF;     
                    border: 2px solid #fff;
                    color: #000000;    
                    text-align: center;
                    vertical-align: middle;
                }

                .tablapS tbody td.sinBottom{
                    border-bottom: 0;
                }

                .bold{
                    font-weight: bold;
                }
            </style>
            <table class="tablapS">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Metric</th>
                        <th>Description</th>
                        <th>Result</th>
                        <th colspan="2">Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="bold" rowspan="2">Overview</td>
                        <td>Active Devices</td>
                        <td>> 90% Active Devices</td>
                        <td><img src="'; 
                            if($primero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td>Active Devices: ' . $porcActivos . '%</td>
                        <td>Devices: ' . $totalEquipos . '</td>
                    </tr>
                    <tr>
                        <td>Discovery</td>
                        <td>> 90% Discovery</td>
                        <td><img src="';
                            if($segundo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td>Scanned Devices: ' . $porcLevantado . '%</td>
                        <td>Active Devices: ' . $totalEquiposActivos . '</td>
                    </tr>
                    <tr>
                        <th class="bold" rowspan="4" style="font-weight:bold;">Optimization</th>
                        <th>Unused Devices?</th>
                        <th>1 or more unused devices detected</th>
                        <th><img src="';
                            if($tercero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; }
                        $html .= '"></th>
                        <th colspan="2">Unused Devices: ' . $UnusedDevice . '</th>
                    </tr>
                    <tr>
                        <th>Unused Software?</th>
                        <th>1 or more unused software detected</th>
                        <th><img src="'; 
                            if($cuarto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th>Unused Software: ' . $unusedSW . '</th>
                        <th>Est Applications: ' . $porcEstApplications . '</th>
                    </tr>
                    <tr>
                        <th>Duplicate Installations?</th>
                        <th>1 or more duplicate installations detected</th>
                        <th><img src="';
                            if($quinto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <th>Duplicate Installations: ' . $softwareDuplicate . '</th>
                        <th>Est Applications: ' . $porcEstDuplicate . '</th>
                    </tr>
                    <tr>
                        <th>Erroneus Installations?</th>
                        <th>Incorrect installations detected</th>
                        <th><img src="'; 
                            if($sexto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <th>Erroneus Installations: ' . $erroneus . '</th>
                        <th>Est Erroneous: ' . $porcEstErroneus . '</th>
                    </tr>
                    <tr>
                        <td class="bold" rowspan="2">Maximization</td>
                        <td>Cloud Deployment?</td>
                        <td>> 50% Cloud</td>
                        <td><img src="'; 
                            if($septimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td>O365: ' . $totalOffice365 . '</td>
                        <td>Office: ' . $totalOffice . '</td>
                    </tr>
                    <tr>
                        <td>Optimized Virtualization?</td>
                        <td>> 50% Virtualized</td>
                        <td><img src="'; 
                            if($octavo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td>VM: ' . $totalServidorVirtual . '</td>
                        <td>Servers: ' . $totalServidorFisico . '</td>
                    </tr>
                    <tr>
                        <th class="bold" rowspan="2" style="font-weight:bold;">Security</th>
                        <th>Unsupported Software?</th>
                        <th>Unsupported software detected</th>
                        <th><img src="'; 
                            if($noveno == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th colspan="2">' . $soft . '</th>
                    </tr>
                    <tr>
                        <th>Vulnerability Exposure?</th>
                        <th>Vulnerability exposure detected</th>
                        <th><img src="'; 
                            if($decimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></th>
                        <th colspan="2">Windows Server</th>
                    </tr>
                    <tr>
                        <td class="bold" rowspan="2">Recommendation</td>
                        <td class="bold">Optimization Opportunity?</td>
                        <td class="bold">3 or more areas of improvement</td>
                        <td><img src="'; 
                            if($fallas < 3){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } 
                            else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                        $html .= '"></td>
                        <td colspan="2" class="bold">';
                            if($fallas < 3){ $html .= 'Low SAM Benefit'; } 
                            else if($fallas >= 3 && $fallas < 6){ $html .= 'Medium SAM Benefit'; } 
                            else{ $html .= 'High SAM Benefit'; } 
                        $html .= '</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <br>

        <table style="width:100%;">
            <tr>
                <td style="width:13%;">' . date("m/d/Y") . '</td>
                <td style="width:73%;text-align:center;">https://www.licensingassurance.com/Diagnostic/</td>
                <td style="width:13%;"><img style="height:60px;" align="right" src="../img/LA_Logo.png"></img></td>
            </tr>
        </table>';

        $path = $GLOBALS["app_root1"] . "/Diagnostic/resultados/";
        $nameArchivo = "Diagnostic" . date("dmYHms") . ".pdf";
        include($GLOBALS["app_root1"] . "/mpdf60/mpdf.php");
        $mpdf = new mPDF('', 'A3-L', 0, '', 15, 15, 16, 16, 15, 9, 'L');
        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = "UTF-8";
        $mpdf->WriteHTML($html);
        $mpdf->output($path . $nameArchivo, 'F');
        
        $ip = $general->getRealIP();
        $geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
        $city = $geo["geoplugin_city"];
        $country = $geo["geoplugin_countryName"]; 
        
        $mensaje = utf8_decode("IP Address: " . $ip . " "
            . "País: " . $country . " "
            . "Ciudad: " . $city);
        
        $claseEnvio = new email();
        if($claseEnvio->mail_file("Dimitri@licensingassurance.com", "soportetecnico@licensingassurance.com", 
        "SAM Diagnostic: Nombre: " . $nombre . "   Email: " . $email, $mensaje, $path . $nameArchivo, $nameArchivo)){
            try{
                $cabecera->actualizar($idDiagnostic, $nombre, $email, $nameArchivo);
            }catch(PDOException $e){
                $error = 3;
            }
        } else{
            $error = 2;
        }
                
        /*$cliente_email = "Dimitri@licensingassurance.com";
        $copia_email   = "<m_acero_n@hotmail.com>,<paola@licensingassurance.com>";
        $mensaje       = "IP Address: " . $ip . " "
            . "País: " . $country . " "
            . "Ciudad: " . $city . " "
            . "envió información por el SAM Diagnostic";    

        $asunto = "SAM Diagnostic: Nombre: " . $nombre . "   Email: " . $email; 
        $eol  = PHP_EOL;
        $desde  = "From:<" . $cliente_email . ">".$eol;
        $desde .= "cc:" . $copia_email;
        if(mail($cliente_email,$asunto,$mensaje,$desde)){
            try{
                $cabecera->actualizar($idDiagnostic, $nombre, $email, $nameArchivo);
            }catch(PDOException $e){
                $error = 3;
            }
        } else{
            $error = 2;
        }*/
    }
} else{
    $error = 1;
}