<?php
class ConsTipoEquipoSAMDiagnostic extends General{
    ########################################  Atributos  ########################################
    public $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoTipoEquipoSAMDiagnostic (idDiagnostic, dato_control, host_name, fabricante, modelo) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($idDiagnostic) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM consolidadoTipoEquipoSAMDiagnostic WHERE idDiagnostic = :idDiagnostic");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}