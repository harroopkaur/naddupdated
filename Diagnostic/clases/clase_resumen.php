<?php
class ResumenSAMDiagnostic extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO resumen_SAMDiagnostic(idDiagnostic, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES " . $bloque;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($idDiagnostic) {
        $this->conexion();
        $query = "DELETE FROM resumen_SAMDiagnostic WHERE idDiagnostic = :idDiagnostic";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productosSinSoporte($idDiagnostic){
        $this->conexion();
        $query = "SELECT COUNT(tabla.id) AS cantidad
            FROM (SELECT resumen_SAMDiagnostic.id
                FROM resumen_SAMDiagnostic
                    INNER JOIN detalleMaestra ON resumen_SAMDiagnostic.familia = detalleMaestra.descripcion AND
                    resumen_SAMDiagnostic.edicion = detalleMaestra.campo1 AND resumen_SAMDiagnostic.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE resumen_SAMDiagnostic.idDiagnostic = :idDiagnostic
                GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version
                
                UNION 
                
                SELECT detalles_equipoSAMDiagnostic.id
                FROM detalles_equipoSAMDiagnostic
                    INNER JOIN detalleMaestra ON detalles_equipoSAMDiagnostic.familia = detalleMaestra.descripcion AND
                    detalles_equipoSAMDiagnostic.edicion = detalleMaestra.campo1 AND detalles_equipoSAMDiagnostic.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic
                GROUP BY detalles_equipoSAMDiagnostic.equipo, detalles_equipoSAMDiagnostic.familia, detalles_equipoSAMDiagnostic.edicion, detalles_equipoSAMDiagnostic.version) tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function productosSinSoporte1($idDiagnostic){
        $this->conexion();
        $query = "SELECT tabla.familia,
                    COUNT(tabla.id) AS cantidad
            FROM (SELECT resumen_SAMDiagnostic.familia,
                resumen_SAMDiagnostic.id
                FROM resumen_SAMDiagnostic
                    INNER JOIN detalleMaestra ON resumen_SAMDiagnostic.familia = detalleMaestra.descripcion AND
                    resumen_SAMDiagnostic.edicion = detalleMaestra.campo1 AND resumen_SAMDiagnostic.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE resumen_SAMDiagnostic.idDiagnostic = :idDiagnostic
                GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version
                
                UNION 
                
                SELECT detalles_equipoSAMDiagnostic.familia,
                detalles_equipoSAMDiagnostic.id
                FROM detalles_equipoSAMDiagnostic
                    INNER JOIN detalleMaestra ON detalles_equipoSAMDiagnostic.familia = detalleMaestra.descripcion AND
                    detalles_equipoSAMDiagnostic.edicion = detalleMaestra.campo1 AND detalles_equipoSAMDiagnostic.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic
                GROUP BY detalles_equipoSAMDiagnostic.equipo, detalles_equipoSAMDiagnostic.familia, detalles_equipoSAMDiagnostic.edicion, detalles_equipoSAMDiagnostic.version) tabla
            GROUP BY tabla.familia";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}