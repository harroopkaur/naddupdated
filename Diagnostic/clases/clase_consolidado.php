<?php
class ConsolidadoSAMDiagnostic extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar     
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_SAMDiagnostic (idDiagnostic, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($idDiagnostic) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_SAMDiagnostic WHERE idDiagnostic = :idDiagnostic');
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($idDiagnostic) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM consolidado_SAMDiagnostic WHERE idDiagnostic = :idDiagnostic ORDER BY id');
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosMicrosoft($idDiagnostic, $in = null, $out = null) {        
        try{
            $this->conexion();
            
            $agregar = "";
            if($in != null){
                $inAux = explode(",", $in);
            }
            
            if(trim($inAux[0]) == "Office" || trim($inAux[0]) == "Visio" || trim($inAux[0]) == "Visual" || trim($inAux[0]) == "Microsoft Exchange"
            || trim($inAux[0]) == "SharePoint" || trim($inAux[0]) == "SQL"){
                $agregar .= " AND sofware LIKE '%" . trim($inAux[0]) . "%' AND (";
            
                for($i = 1; $i < count($inAux); $i++){
                    if($i > 1){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            } else{
                $agregar .= " AND (";
            
                for($i = 0; $i < count($inAux); $i++){
                    if($i > 0){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            }
                
            $quitar = "";
            if($out != null){
                $outAux = explode(",", $out);
            }
            for($i = 0; $i < count($outAux); $i++){
                if($i > 0){
                    $quitar .= " OR ";
                }
                
                $quitar .= "sofware LIKE '%" . trim($outAux[$i]) . "%'"; 
            }
            
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				FROM consolidado_SAMDiagnostic
				WHERE idDiagnostic = :idDiagnostic " . $agregar . "
				ORDER BY fecha_instalacion DESC) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_SAMDiagnostic
				  WHERE idDiagnostic = :idDiagnostic AND (" . $quitar . ")
			)");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function buscarVersion($software) {
        $version = "";
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
                    FROM versiones
                    WHERE id NOT IN (46) AND id NOT IN (SELECT id FROM versiones WHERE id > 140 AND id <= 1330)
                    ORDER BY nombre");
            $sql->execute();
            $tabla = $sql->fetchAll();
            foreach($tabla as $row){
                if (strpos($software, $row["nombre"]) !== false && strlen($row["nombre"]) > strlen($version)) {
                    $version = $row["nombre"];
                }
            }
            return $version;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $version;
        }
    }
}