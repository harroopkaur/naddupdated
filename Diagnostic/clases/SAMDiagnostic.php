<?php
class SAMDiagnostic extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($LAD) {
        $query = "INSERT INTO SAMDiagnostic (fecha, LAD) ";
        $query .= "VALUES (NOW(), :LAD)";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':LAD'=>$LAD));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function actualizar($id, $nombre, $email, $archivo){
        $query = "UPDATE SAMDiagnostic SET nombre = :nombre, email = :email, resultado = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre, ':email'=>$email, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        } 
    }
    
    function actualizarLAE($id, $archivo){
        $query = "UPDATE SAMDiagnostic SET LAE = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        } 
    }
    
    function ultId(){
        $query = "SELECT MAX(id) AS id FROM SAMDiagnostic";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return 0;
        } 
    }
}