<?php
class DetallesSAMDiagnostic extends General{
    ########################################  Atributos  ########################################
    public $error = null;
    
    #######################################  Operaciones  #######################################
    // Insertar     
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipoSAMDiagnostic (idDiagnostic, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($idDiagnostic) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM detalles_equipoSAMDiagnostic WHERE idDiagnostic = :idDiagnostic");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar
    function actualizar($idDiagnostic, $equipo, $errors) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipoSAMDiagnostic SET errors = :errors "
            . "WHERE equipo = :equipo AND idDiagnostic = :idDiagnostic");
            $sql->execute(array(':equipo'=>$equipo, ':idDiagnostic'=>$idDiagnostic, ':errors'=>$errors));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function totalEquiposActivos($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(idDiagnostic) AS cantidad
                FROM detalles_equipoSAMDiagnostic
                WHERE idDiagnostic = :idDiagnostic AND rango IN (1, 2, 3)");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquiposUso($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(idDiagnostic) AS cantidad
                FROM detalles_equipoSAMDiagnostic
                WHERE idDiagnostic = :idDiagnostic AND rango = 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquiposLevantados($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(idDiagnostic) AS cantidad
                FROM detalles_equipoSAMDiagnostic
                WHERE idDiagnostic = :idDiagnostic AND rango IN (1, 2, 3) AND errors = 'Ninguno'");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function scanChallenges($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM detalles_equipoSAMDiagnostic
                WHERE idDiagnostic = :idDiagnostic AND equipo NOT IN (
                    SELECT equipo FROM detalles_equipoSAMDiagnostic 
                    WHERE idDiagnostic = :idDiagnostic AND errors = 'Ninguno' GROUP BY equipo)
                AND NOT errors IS NULL");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalEquipos($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(id) AS cantidad
                FROM detalles_equipoSAMDiagnostic
                WHERE idDiagnostic = :idDiagnostic");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function unusedSW($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.familia) AS cantidad
                FROM (SELECT resumen_SAMDiagnostic.familia
                    FROM detalles_equipoSAMDiagnostic
                        INNER JOIN resumen_SAMDiagnostic ON detalles_equipoSAMDiagnostic.idDiagnostic = resumen_SAMDiagnostic.idDiagnostic
                        AND detalles_equipoSAMDiagnostic.equipo = resumen_SAMDiagnostic.equipo AND
                        resumen_SAMDiagnostic.familia IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
                    WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic 
                    AND detalles_equipoSAMDiagnostic.rango NOT IN (1)
                    GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalAplicaciones($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM (SELECT resumen.id
                FROM detalles_equipoSAMDiagnostic
                    INNER JOIN resumen_SAMDiagnostic AS resumen ON detalles_equipoSAMDiagnostic.equipo = resumen.equipo
                    AND resumen.familia IN ('Office', 'Visio', 'Project', 'Visual Studio') AND resumen.idDiagnostic = detalles_equipoSAMDiagnostic.idDiagnostic
                WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.rango IN (1)
                GROUP BY resumen_MSCloud.equipo, resumen.familia, resumen.edicion, resumen.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function softwareDuplicate($idDiagnostic){
        try{
            $this->conexion();
            /*$sql = $this->conn->prepare("SELECT resumen_SAMDiagnostic.id,
                    resumen_SAMDiagnostic.equipo,
                    resumen_SAMDiagnostic.familia,
                    resumen_SAMDiagnostic.edicion,
                    resumen_SAMDiagnostic.version,
                    resumen_SAMDiagnostic.fecha_instalacion
                FROM detalles_equipoSAMDiagnostic
                    INNER JOIN resumen_SAMDiagnostic ON detalles_equipoSAMDiagnostic.idDiagnostic = resumen_SAMDiagnostic.idDiagnostic
                    AND detalles_equipoSAMDiagnostic.equipo = resumen_SAMDiagnostic.equipo AND
                    resumen_SAMDiagnostic.familia IN ('Office', 'Visio', 'Project', 'Visual Studio')
                WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.tipo = 1
                GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia
                HAVING COUNT(resumen_SAMDiagnostic.equipo) > 1");*/
            $sql = $this->conn->prepare("SELECT tabla.equipo,
                    tabla.familia,
                    tabla.edicion,
                    COUNT(tabla.equipo) AS cantidad
                FROM (SELECT resumen_SAMDiagnostic.equipo,
                        resumen_SAMDiagnostic.familia,
                        resumen_SAMDiagnostic.edicion
                    FROM resumen_SAMDiagnostic
                    WHERE resumen_SAMDiagnostic.idDiagnostic = :idDiagnostic 
                    GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion,
                    resumen_SAMDiagnostic.version) tabla
                GROUP BY tabla.equipo, tabla.familia
                HAVING COUNT(tabla.equipo) > 1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function erroneus($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(equipo) AS cantidad
                FROM (SELECT equipo
                    FROM (SELECT resumen_SAMDiagnostic.equipo,
                        resumen_SAMDiagnostic.familia,
                        resumen_SAMDiagnostic.edicion,
                        resumen_SAMDiagnostic.version
                        FROM detalles_equipoSAMDiagnostic
                           INNER JOIN resumen_SAMDiagnostic ON detalles_equipoSAMDiagnostic.idDiagnostic = resumen_SAMDiagnostic.idDiagnostic
                           AND detalles_equipoSAMDiagnostic.equipo = resumen_SAMDiagnostic.equipo AND (resumen_SAMDiagnostic.familia IN ('SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                               OR resumen_SAMDiagnostic.edicion LIKE '%Server%') AND resumen_SAMDiagnostic.edicion NOT LIKE '%Endpoint Protection%' AND resumen_SAMDiagnostic.edicion NOT LIKE '%Express Edition%' AND resumen_SAMDiagnostic.edicion NOT LIKE '%MUI%' AND resumen_SAMDiagnostic.edicion NOT LIKE '%Update%'
                        WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.tipo = 1
                        GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version

                        UNION
  
                        SELECT resumen_SAMDiagnostic.equipo,
                        resumen_SAMDiagnostic.familia,
                        resumen_SAMDiagnostic.edicion,
                        resumen_SAMDiagnostic.version
                        FROM detalles_equipoSAMDiagnostic
                           INNER JOIN resumen_SAMDiagnostic ON detalles_equipoSAMDiagnostic.idDiagnostic = resumen_SAMDiagnostic.idDiagnostic
                           AND detalles_equipoSAMDiagnostic.equipo = resumen_SAMDiagnostic.equipo AND
                           (resumen_SAMDiagnostic.familia NOT IN ('Visual Studio', 'SQL Server', 'Exchange Server', 'Sharepoint Server', 'System Center')
                               AND NOT resumen_SAMDiagnostic.edicion LIKE '%Server%') AND resumen_SAMDiagnostic.edicion NOT LIKE '%MUI%' AND resumen_SAMDiagnostic.edicion NOT LIKE '%Update%'
                        WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.tipo = 2
                        GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version) tabla
                    GROUP BY equipo, familia, edicion, version) tabla1");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    /*function totalOffice($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT resumen_SAMDiagnostic.equipo
                    FROM detalles_equipoSAMDiagnostic
                        INNER JOIN resumen_SAMDiagnostic ON detalles_equipoSAMDiagnostic.idDiagnostic = resumen_SAMDiagnostic.idDiagnostic
                        AND detalles_equipoSAMDiagnostic.equipo = resumen_SAMDiagnostic.equipo AND
                        resumen_SAMDiagnostic.familia = 'Office'
                    WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.tipo = 1
                    GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalOffice365($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT resumen_SAMDiagnostic.equipo
                    FROM detalles_equipoSAMDiagnostic
                        INNER JOIN resumen_SAMDiagnostic ON detalles_equipoSAMDiagnostic.idDiagnostic = resumen_SAMDiagnostic.idDiagnostic
                        AND detalles_equipoSAMDiagnostic.equipo = resumen_SAMDiagnostic.equipo AND
                        resumen_SAMDiagnostic.familia = 'Office' AND resumen_SAMDiagnostic.edicion LIKE '%Office 365%'
                    WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.tipo = 1
                    GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function totalOffice($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT resumen_SAMDiagnostic.equipo
                    FROM resumen_SAMDiagnostic 
                        INNER JOIN detalles_equipoSAMDiagnostic ON resumen_SAMDiagnostic.equipo = detalles_equipoSAMDiagnostic.equipo 
                        AND resumen_SAMDiagnostic.idDiagnostic = detalles_equipoSAMDiagnostic.idDiagnostic 
                    WHERE resumen_SAMDiagnostic.idDiagnostic = :idDiagnostic AND resumen_SAMDiagnostic.familia LIKE '%Office%' AND NOT resumen_SAMDiagnostic.edicion LIKE '%365%'
                    GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version
                    ORDER BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function totalOffice365($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT resumen_SAMDiagnostic.equipo
                    FROM resumen_SAMDiagnostic 
                        INNER JOIN detalles_equipoSAMDiagnostic ON resumen_SAMDiagnostic.equipo = detalles_equipoSAMDiagnostic.equipo 
                        AND resumen_SAMDiagnostic.idDiagnostic = detalles_equipoSAMDiagnostic.idDiagnostic 
                    WHERE resumen_SAMDiagnostic.idDiagnostic = :idDiagnostic AND resumen_SAMDiagnostic.familia LIKE '%Office%' AND resumen_SAMDiagnostic.version LIKE '%365%'
                    GROUP BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version
                    ORDER BY resumen_SAMDiagnostic.equipo, resumen_SAMDiagnostic.familia, resumen_SAMDiagnostic.edicion, resumen_SAMDiagnostic.version) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    /*function totalServidorFisico($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad
                FROM (SELECT detalles_equipoSAMDiagnostic.equipo
                    FROM detalles_equipoSAMDiagnostic
                        LEFT JOIN consolidadoTipoEquipoSAMDiagnostic ON detalles_equipoSAMDiagnostic.equipo = consolidadoTipoEquipoSAMDiagnostic.host_name
                        AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoTipoEquipoSAMDiagnostic.idDiagnostic
                        LEFT JOIN consolidadoProcesadoresSAMDiagnostic ON consolidadoTipoEquipoSAMDiagnostic.host_name = consolidadoProcesadoresSAMDiagnostic.host_name
                        AND consolidadoProcesadoresSAMDiagnostic.cpu > 0 AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoProcesadoresSAMDiagnostic.idDiagnostic
                    WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.os LIKE '%Windows Server%'
                    AND NOT consolidadoTipoEquipoSAMDiagnostic.modelo LIKE '%Virtual%'
                    GROUP BY detalles_equipoSAMDiagnostic.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function totalServidorFisico($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(tabla.equipo) AS cantidad
FROM (SELECT detalles_equipoSAMDiagnostic.equipo
                    FROM detalles_equipoSAMDiagnostic
                        LEFT JOIN consolidadoTipoEquipoSAMDiagnostic ON detalles_equipoSAMDiagnostic.equipo = consolidadoTipoEquipoSAMDiagnostic.host_name AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoTipoEquipoSAMDiagnostic.idDiagnostic
                        LEFT JOIN consolidadoProcesadoresSAMDiagnostic ON consolidadoTipoEquipoSAMDiagnostic.host_name = consolidadoProcesadoresSAMDiagnostic.host_name AND consolidadoProcesadoresSAMDiagnostic.cpu > 0 AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoProcesadoresSAMDiagnostic.idDiagnostic
                    WHERE detalles_equipoSAMDiagnostic.os LIKE "%Windows Server%" AND detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND (IF(consolidadoTipoEquipoSAMDiagnostic.modelo LIKE "%Virtual%", "Virtual", "Fisico")) = "Fisico" AND detalles_equipoSAMDiagnostic.rango IN (1, 2, 3) AND detalles_equipoSAMDiagnostic.errors = "Ninguno"
                    GROUP BY detalles_equipoSAMDiagnostic.equipo, consolidadoProcesadoresSAMDiagnostic.cpu, consolidadoProcesadoresSAMDiagnostic.cores) tabla');
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    /*function totalServidorVirtual($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.equipo) AS cantidad 
                FROM (SELECT detalles_equipoSAMDiagnostic.equipo
                    FROM detalles_equipoSAMDiagnostic
                        INNER JOIN consolidadoTipoEquipoSAMDiagnostic ON detalles_equipoSAMDiagnostic.equipo = consolidadoTipoEquipoSAMDiagnostic.host_name
                        AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoTipoEquipoSAMDiagnostic.idDiagnostic
                        INNER JOIN consolidadoProcesadoresSAMDiagnostic ON consolidadoTipoEquipoSAMDiagnostic.host_name = consolidadoProcesadoresSAMDiagnostic.host_name
                        AND consolidadoProcesadoresSAMDiagnostic.cpu > 0 AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoProcesadoresSAMDiagnostic.idDiagnostic
                    WHERE detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND detalles_equipoSAMDiagnostic.os LIKE '%Windows Server%'
                    AND consolidadoTipoEquipoSAMDiagnostic.modelo LIKE '%Virtual%'
                    GROUP BY detalles_equipoSAMDiagnostic.equipo) tabla");
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/
    
    function totalServidorVirtual($idDiagnostic){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(tabla.equipo) AS cantidad
FROM (SELECT detalles_equipoSAMDiagnostic.equipo
                    FROM detalles_equipoSAMDiagnostic
                        LEFT JOIN consolidadoTipoEquipoSAMDiagnostic ON detalles_equipoSAMDiagnostic.equipo = consolidadoTipoEquipoSAMDiagnostic.host_name AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoTipoEquipoSAMDiagnostic.idDiagnostic
                        LEFT JOIN consolidadoProcesadoresSAMDiagnostic ON consolidadoTipoEquipoSAMDiagnostic.host_name = consolidadoProcesadoresSAMDiagnostic.host_name AND consolidadoProcesadoresSAMDiagnostic.cpu > 0 AND detalles_equipoSAMDiagnostic.idDiagnostic = consolidadoProcesadoresSAMDiagnostic.idDiagnostic
                    WHERE detalles_equipoSAMDiagnostic.os LIKE "%Windows Server%" AND detalles_equipoSAMDiagnostic.idDiagnostic = :idDiagnostic AND (IF(consolidadoTipoEquipoSAMDiagnostic.modelo LIKE "%Virtual%", "Virtual", "Fisico")) = "Virtual" AND detalles_equipoSAMDiagnostic.rango IN (1, 2, 3) AND detalles_equipoSAMDiagnostic.errors = "Ninguno"
                    GROUP BY detalles_equipoSAMDiagnostic.equipo, consolidadoProcesadoresSAMDiagnostic.cpu, consolidadoProcesadoresSAMDiagnostic.cores) tabla');
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
}