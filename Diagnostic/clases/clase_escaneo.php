<?php
class ScaneoSAMDiagnostic extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $equipo;
    var $status;
    var $errors;
    var $error = NULL;
  
    #######################################  Operaciones  #######################################
    // Insertar     
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO escaneo_equiposSAMDiagnostic (idDiagnostic, equipo, status, errors) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($idDiagnostic) {
        $this->conexion();
        $query = "DELETE FROM escaneo_equiposSAMDiagnostic WHERE idDiagnostic = :idDiagnostic";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_todo2($idDiagnostic) {
        $this->conexion();
        $query = "SELECT id,
                idDiagnostic,
                equipo,
                status,
                errors
            FROM escaneo_equiposSAMDiagnostic
            WHERE idDiagnostic = :idDiagnostic AND errors = 'Ninguno'
            GROUP BY equipo

            UNION

            SELECT id,
                idDiagnostic,
                equipo,
                status,
                errors
            FROM escaneo_equiposSAMDiagnostic
            WHERE idDiagnostic = :idDiagnostic AND errors != '' AND NOT errors IS NULL
            AND equipo NOT IN (SELECT equipo FROM escaneo_equiposSAMDiagnostic WHERE idDiagnostic = :idDiagnostic AND errors = 'Ninguno' GROUP BY equipo)
            GROUP BY equipo";
        //$query = "SELECT * FROM escaneo_equipos2 WHERE cliente = :cliente AND empleado = :empleado GROUP BY equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idDiagnostic'=>$idDiagnostic));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equiposNoDescubiertos($cliente, $empleado){
        $query = "SELECT *
            FROM escaneo_equiposSAMDiagnostic
            WHERE cliente = :cliente AND empleado = :empleado AND equipo NOT IN (SELECT equipo
                FROM escaneo_equiposSAMDiagnostic
                WHERE cliente = :cliente AND empleado = :empleado AND errors = 'Ninguno'
                GROUP BY equipo)";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
           return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
}