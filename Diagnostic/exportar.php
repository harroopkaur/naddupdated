<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS['app_root1'] . "/Diagnostic/procesos/resultados.php");

ob_start();
$html = ob_get_clean();
$html = utf8_encode($html);
$html .= '<div style="width:100%; height:auto; overflow:hidden;" id="contPOC">
    <h1 class="tituloSAMDiagnostic1 colorBlanco">Deployment Diagnostic Report</h1>

    <br>

    <style>
        .tituloSAMDiagnostic{
            font-size:20px;
            color: #000000;
            font-weight: bold;
        }

        .tituloSAMDiagnostic2{
            font-size:18px;
            color: #000000;
            font-weight: bold;
        }

        .tituloSAMDiagnostic1{
            font-size:35px;
            text-align: center;
            font-weight: bold;
            color: #000000;
        }

        .tituloImagenes{
            font-size:25px; 
            line-height:82px; 
            font-weight:bold; 
            padding-top:-30px;
            color: #000000;
        }

        .tablapS {     
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
            font-size: 15px;   
            margin: 0 auto;   
            width: 90%; 
            text-align: left;    
            border-collapse: collapse; 
        }

        .tablapS thead th {     
            font-size: 25px;     
            font-weight: bold;     
            padding: 15px;     
            background: #377EBA;
            border-bottom: 2px solid #fff;    
            color: #ffffff; 
            text-align: center; 
        }

        .tablapS thead th.transTopLeft{
            background-color: transparent;
        }

        .tablapS thead th.trans{
            background-color: transparent;
        }

        .tablapS tbody td.trans{
            background-color: transparent;
            font-size: 20px;
        }

        .tablapS tbody td.trans1{
            background-color: transparent;
            border: 0;
        }

        .tablapS tbody td.trans2{
            background-color: transparent;
            border: 0;
            font-size: 20px;
        }

        .tablapS tbody td.titulo{
            font-size: 20px;
        }

        .tablapS tbody th {  
            padding: 8px;     
            background: #5C96C6;     
            border: 2px solid #fff;
            color: #000000;  
            text-align: center;
            vertical-align: middle;
            font-weight: normal;
        }

        .tablapS tbody td {    
            padding: 8px;     
            background: #A0C3DF;     
            border: 2px solid #fff;
            color: #000000;    
            text-align: center;
            vertical-align: middle;
        }

        .tablapS tbody td.sinBottom{
            border-bottom: 0;
        }
        
        .bold{
            font-weight: bold;
        }
    </style>
    <table class="tablapS">
        <thead>
            <tr>
                <th>Category</th>
                <th>Metric</th>
                <th>Description</th>
                <th>Result</th>
                <th colspan="2">Details</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold" rowspan="2">Overview</td>
                <td>Active Devices</td>
                <td>> 90% Active Devices</td>
                <td><img src="'; 
                    if($primero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td>Active Devices: ' . $porcActivos . '%</td>
                <td>Devices: ' . $totalEquipos . '</td>
            </tr>
            <tr>
                <td>Discovery</td>
                <td>> 90% Discovery</td>
                <td><img src="';
                    if($segundo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td>Scanned Devices: ' . $porcLevantado . '%</td>
                <td>Active Devices: ' . $totalEquiposActivos . '</td>
            </tr>
            <tr>
                <th class="bold" rowspan="4" style="font-weight:bold;">Optimization</th>
                <th>Unused Devices?</th>
                <th>1 or more unused devices detected</th>
                <th><img src="';
                    if($tercero == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; }
                $html .= '"></th>
                <th colspan="2">Unused Devices: ' . $UnusedDevice . '</th>
            </tr>
            <tr>
                <th>Unused Software?</th>
                <th>1 or more unused software detected</th>
                <th><img src="'; 
                    if($cuarto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th>Unused Software: ' . $unusedSW . '</th>
                <th>Est Applications: ' . $porcEstApplications . '</th>
            </tr>
            <tr>
                <th>Duplicate Installations?</th>
                <th>1 or more duplicate installations detected</th>
                <th><img src="';
                    if($quinto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <th>Duplicate Installations: ' . $softwareDuplicate . '</th>
                <th>Est Applications: ' . $porcEstDuplicate . '</th>
            </tr>
            <tr>
                <th>Erroneus Installations?</th>
                <th>Incorrect installations detected</th>
                <th><img src="'; 
                    if($sexto == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <th>Erroneus Installations: ' . $erroneus . '</th>
                <th>Est Erroneous: ' . $porcEstErroneus . '</th>
            </tr>
            <tr>
                <td class="bold" rowspan="2">Maximization</td>
                <td>Cloud Deployment?</td>
                <td>> 50% Cloud</td>
                <td><img src="'; 
                    if($septimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td>O365: ' . $totalOffice365 . '</td>
                <td>Office: ' . $totalOffice . '</td>
            </tr>
            <tr>
                <td>Optimized Virtualization?</td>
                <td>> 50% Virtualized</td>
                <td><img src="'; 
                    if($octavo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td>VM: ' . $totalServidorVirtual . '</td>
                <td>Servers: ' . $totalServidorFisico . '</td>
            </tr>
            <tr>
                <th class="bold" rowspan="2" style="font-weight:bold;">Security</th>
                <th>Unsupported Software?</th>
                <th>Unsupported software detected</th>
                <th><img src="'; 
                    if($noveno == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th colspan="2">' . $soft . '</th>
            </tr>
            <tr>
                <th>Vulnerability Exposure?</th>
                <th>Vulnerability exposure detected</th>
                <th><img src="'; 
                    if($decimo == true){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></th>
                <th colspan="2">Windows Server</th>
            </tr>
            <tr>
                <td class="bold" rowspan="2">Recommendation</td>
                <td class="bold">Optimization Opportunity?</td>
                <td class="bold">3 or more areas of improvement</td>
                <td><img src="'; 
                    if($fallas < 3){ $html .= $GLOBALS["domain_root"] . '/imagenes/check2.png'; } 
                    else{ $html .= $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } 
                $html .= '"></td>
                <td colspan="2" class="bold">';
                    if($fallas < 3){ $html .= 'Low SAM Benefit'; } 
                    else if($fallas >= 3 && $fallas < 6){ $html .= 'Medium SAM Benefit'; } 
                    else{ $html .= 'High SAM Benefit'; } 
                $html .= '</td>
            </tr>
        </tbody>
    </table>
</div>

<br>

<table style="width:100%;">
    <tr>
        <td style="width:13%;">' . date("m/d/Y") . '</td>
        <td style="width:73%;text-align:center;">https://www.licensingassurance.com/Diagnostic/</td>
        <td style="width:13%;"><img style="height:60px;" align="right" src="../img/LA_Logo.png"></img></td>
    </tr>
</table>';
                
include($GLOBALS["app_root1"] . "/mpdf60/mpdf.php");
$mpdf = new mPDF('', 'A3-L', 0, '', 15, 15, 16, 16, 15, 9, 'L');
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in = "UTF-8";
$mpdf->WriteHTML($html);
$mpdf->output();
exit();