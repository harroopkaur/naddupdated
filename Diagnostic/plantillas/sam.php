        <div class="contenedorDiagnostic">
            <h1 class="tituloSAMDiagnostic1">SAM ALTERNATIVES</h1>
            <br>
            
            <table class="tablapS">
                <thead>
                    <tr>
                        <th class='trans'></th>
                        <th style="width:230px;">OPT OUT</th>
                        <th class='trans'></th>
                        <th style="width:230px;">CLOUD SAM</th>
                        <th class='trans'></th>
                        <th style="width:230px;">SAM AS A SERVICE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class='trans3'>Objective</td>
                        <td class='titulo'>Do not continue</td>
                        <td class='trans'></td>
                        <td class='titulo'>Migrate to the Cloud</td>
                        <td class='trans'></td>
                        <td class='titulo'>Continous SAM</td>
                    </tr>
                    <tr>
                        <td class='trans3'>Optimization</td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png"></td>
                    </tr>
                    <tr>
                        <td class='trans3'>Audit Mitigation</td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png"></td>
                    </tr>
                    <tr>
                        <td class='trans3'>Service</td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/guion.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png"></td>
                    </tr>
                    <tr>
                        <td class='trans3'>Personalized</td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/guion.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png"></td>
                        <td class='trans'></td>
                        <td><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png"></td>
                    </tr>
                    <tr>
                        <td class='trans2'>ISO Process</td>
                        <td class='sinBottom'><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/guion.png"></td>
                        <td class='trans1'></td>
                        <td class='sinBottom'><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/delete2.png"></td>
                        <td class='trans1'></td>
                        <td class='sinBottom'><img src="<?= $GLOBALS["domain_root"] ?>/imagenes/check2.png"></td>
                    </tr>
                    
                    <tr>
                        <td class='trans1'></td>
                        <td class='trans1'></td>
                        <td class='trans1'></td>
                        <td class='trans1'></td>
                        <td class='trans1'></td>
                        <td class='trans1'><div class='btnStart' onclick='window.open("https://licensingassurance.com/Español/Presentacion.pdf");'>START NOW</div></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>