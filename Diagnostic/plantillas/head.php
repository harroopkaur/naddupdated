<!DOCTYPE HTML>
<html>
    <head>
        <title>.:Deployment Diagnostic – LA:.</title>
        <!-- Custom Theme files -->
        <link rel="shortcut icon" href="../img/Logo.ico">
        
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="keywords" content="trial, sofware" />
        <meta name="author" content=" Licensing Assurance"/>
        <meta name="robots" content="noodp,noydir"/>
        <!--Google Fonts-->
        
        <link href="<?= $GLOBALS["domain_root"] ?>/css/style3.css" rel="stylesheet" type="text/css"/>
        <link href="<?=$GLOBALS['domain_root']?>/plugin-alert/css/alert.min.css" rel="stylesheet" />
        <link href="<?=$GLOBALS['domain_root']?>/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        <!--<link rel="stylesheet" href="<?//= $GLOBALS["domain_root"] ?>/css/exampleSam.css" type="text/css" />-->
        <link href="<?=$GLOBALS['domain_root1']?>/css/bootstrap.min.css" rel="stylesheet">
        <style>
            body {
                background: #00B0F0 url(<?= $GLOBALS['domain_root'] ?>/imagenes/inicio/bg_body.jpg) top left no-repeat !important;
                padding:0px !important;	
                color: #000000;
                /*
                font-family: 'Open Sans', sans-serif;
                font-size: 100%;*/
            }
            
            .fondo{
                position:fixed;
                top:0;
                right:0;
                bottom:0;
                left:0;
                z-index:1500;
                background-color:#000;
                filter:alpha(opacity=0.7);
                opacity:0.7;
                display:none;
            }
            
            .tituloSAMDiagnostic{
                font-size:20px;
                color: #000000;
                font-weight: bold;
            }
            
            .tituloSAMDiagnostic2{
                font-size:18px;
                color: #000000;
                font-weight: bold;
            }
            
            .tituloSAMDiagnostic1{
                font-size:35px;
                text-align: center;
                font-weight: bold;
                color: #000000;
            }
            
            .tituloImagenes{
                font-size:25px; 
                line-height:82px; 
                font-weight:bold; 
                padding-top:-30px;
                color: #000000;
            }
            
            .pointer{
                cursor: pointer;
            }
            
            .contenedorMenu{
                width:80%; 
                margin:0 auto; 
                padding:0px; 
                overflow:hidden;
            }
            
            .contenedorDiagnostic{
                width:85%; 
                margin:0 auto; 
                padding:20px;
                overflow:hidden;
                background: rgba(139,194,228,1);
                background: -moz-linear-gradient(left, rgba(139,194,228,1) 0%, rgba(245,245,245,1) 100%);
                background: -webkit-gradient(left top, right top, color-stop(0%, rgba(139,194,228,1)), color-stop(100%, rgba(245,245,245,1)));
                background: -webkit-linear-gradient(left, rgba(139,194,228,1) 0%, rgba(245,245,245,1) 100%);
                background: -o-linear-gradient(left, rgba(139,194,228,1) 0%, rgba(245,245,245,1) 100%);
                background: -ms-linear-gradient(left, rgba(139,194,228,1) 0%, rgba(245,245,245,1) 100%);
                background: linear-gradient(to right, rgba(139,194,228,1) 0%, rgba(245,245,245,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8bc2e4', endColorstr='#f5f5f5', GradientType=1 );
            }
            
            .contenedorInterno{
                width:600px; 
                margin:0 auto; 
                font-weight:bold;
                overflow: hidden;
                float:left;
            }
            
            .botonDiagnostic{
                color:#ffffff;
                background-color:#000000;
                min-width:150px;
            }
            
            .botonDiagnostic.focus,.botonDiagnostic:focus{
                color:#333;
                background-color:#e6e6e6;
                border-color:#8c8c8c;
            }
            
            .bold{
                font-weight: bold;
            }
            
            .botonDiagnostic:hover{
                color:#000000;
                background-color:#e6e6e6;
                border-color:#adadad;
            }
            
            .row{
                width:100%;
                overflow:hidden;
                padding:0;
                margin:0;
            }
            
            .input{
                width:180px;
                height:38px; 
                margin-top:10px;
                -moz-border-radius: 5px; 
                -webkit-border-radius: 5px; 
                border-radius: 5px; 
                border: 0;
                font-size:18px;
            }
            
            .hide{
                display:none;
            }
            
            .tablapS {     
                font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;                      
                font-size: 15px;   
                margin: 0 auto;   
                width: 90%; 
                text-align: left;    
                border-collapse: collapse; 
            }

            .tablapS thead th {     
                font-size: 25px;     
                font-weight: bold;     
                padding: 15px;     
                background: #377EBA;
                border-bottom: 2px solid #fff;    
                color: #ffffff; 
                text-align: center; 
            }
            
            .tablapS thead th.transTopLeft{
                background-color: transparent;
            }
            
            .tablapS thead th.trans{
                background-color: transparent;
            }
                      
            .tablapS tbody td.trans{
                background-color: transparent;
                font-size: 20px;
            }
            
            .tablapS tbody td.trans1{
                background-color: transparent;
                border: 0;
            }
            
            .tablapS tbody td.trans2{
                background-color: transparent;
                border: 0;
                font-size: 20px;
            }
            
            .tablapS tbody td.trans3{
                background-color: transparent;
                border-left: 0;
                border-right: 0;
                font-size: 20px;
            }
            
            .tablapS tbody td.titulo{
                font-size: 20px;
            }

            .tablapS tbody th {  
                padding: 8px;     
                background: #5C96C6;     
                border: 2px solid #fff;
                color: #000000;  
                text-align: center;
                vertical-align: middle;
            }

            .tablapS tbody td {    
                padding: 8px;     
                background: #A0C3DF;     
                border: 2px solid #fff;
                color: #000000;    
                text-align: center;
                vertical-align: middle;
            }
            
            .tablapS tbody td.sinBottom{
                border-bottom: 0;
            }

            .btnSAMDiagnostic{
                margin:5px;
                margin-left:10px;
                float:right;	
                width:110px;
                padding:10px;
                border-radius:10px;
                text-align:center;
                cursor:pointer;
                background-color: #5C96C6; 
                cursor: pointer;
                font-weight: bold;
                color:#ffffff;
            }
            
            .btnStart{
                width:150px; 
                background-color:#03306E; 
                color:#ffffff; 
                padding:10px; 
                margin:0 auto; 
                margin-top: -20px;
                -moz-border-radius: 5px; 
                -webkit-border-radius: 5px; 
                border-radius: 5px; 
                cursor: pointer;
            }
            
            .botones_m2{
                margin:5px;
                margin-bottom:0px;
                margin-left:10px;
                float:left;	
                min-width:100px;
                padding:10px;
                -moz-border-radius: 10px; 
                -webkit-border-radius: 10px; 
                border-radius: 10px; 
                text-align:center;
                cursor:pointer;
                font-size:12px;
            }
            
            .navbar, .navbar-nav{
                background-color:#000000;
                height:71px;
            }
            
            ::-webkit-input-placeholder {
                text-align: center;
            }

            :-moz-placeholder { /* Firefox 18- */
                text-align: center;  
            }

            ::-moz-placeholder {  /* Firefox 19+ */
                text-align: center;  
            }

            :-ms-input-placeholder {  
                text-align: center; 
            }
        </style>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?=$GLOBALS["domain_root"]?>/font-awesome/css/font-awesome.min.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="<?= $GLOBALS["domain_root"] ?>/plugin-alert/js/alert.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script src="<?= $GLOBALS["domain_root1"] ?>/js/bootstrap.min.js"></script>
    </head>
    <body>	        
        <header id="cabecera" style="position:fixed; z-index:1200; padding:10px;">
            <div style="width:62%; float:left">
                <p style="color:#ffffff; text-align:center; margin-top:5px; font-size:33px; font-weight:bold;">Deployment Diagnostic</p>
            </div>
            <div style="width:37%; float:left">
                <p style="float:left; color:#0DADEB; font-weight:bold; line-height:18px; text-align:center; margin-top:12px;">LICENSING<br>ASSURANCE</p>
                <img src="<?= $GLOBALS["domain_root1"] ?>/img/logo-transparent.png" alt="Logo LA" style="margin-left:10px;height:60px; width:auto; float:left">
                <p style="float:left; color:#0DADEB; font-weight:bold; margin-top:3px; margin-left:10px; text-align:center; line-height: 18px;">SAM<br>as a<br>Service</p>
            </div>
        </header>
        
        <div class="fondo" id="fondo" style="display:none;">
            <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" alt="Loading" style="width:50px; height:50px; top:50%; margin-top:-25px; left:50%; margin-left:-25px; position:absolute;">
        </div>
        
        <div class="fondo1" id="fondo1"></div>
        
        <section id="contenedor">
            <div style="overflow:hidden; margin:0 auto; margin:20px; margin-right:10px; width:95%; margin-top:100px;">