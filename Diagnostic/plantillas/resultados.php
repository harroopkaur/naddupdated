        <div class="contenedorDiagnostic">   
            <h1 class="tituloSAMDiagnostic1">DEPLOYMENT DIAGNOSTIC REPORT</h1>
            <br>

            <table class="tablapS">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Metric</th>
                        <th>Description</th>
                        <th>Result</th>
                        <th colspan="2">Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="bold" rowspan="2">Overview</td>
                        <td>Active Devices</td>
                        <td>> 90% Active Devices</td>
                        <td><img src="<?php if($primero == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                        <td><?= 'Active Devices: ' . $porcActivos . "%" ?></td>
                        <td><?= 'Devices: ' . $totalEquipos ?></td>
                    </tr>
                    <tr>
                        <td>Discovery</td>
                        <td>> 90% Discovery</td>
                        <td><img src="<?php if($segundo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                        <td><?= 'Scanned Devices: ' . $porcLevantado . "%" ?></td>
                        <td><?= 'Active Devices: ' . $totalEquiposActivos ?></td>
                    </tr>

                    <tr>
                        <th class="bold" rowspan="4">Optimization</th>
                        <th>Unused Devices?</th>
                        <th>1 or more unused devices detected</th>
                        <th><img src="<?php if($tercero == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                        <th colspan="2"><?= 'Unused Devices: ' . $UnusedDevice ?></th>
                    </tr>
                    <tr>
                        <th>Unused Software?</th>
                        <th>1 or more unused software detected</th>
                        <th><img src="<?php if($cuarto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                        <th><?= 'Unused Software: ' . $unusedSW ?></th>
                        <th><?= 'Est Applications: ' . $porcEstApplications ?></th>
                    </tr>
                    <tr>
                        <th>Duplicate Installations?</th>
                        <th>1 or more duplicate installations detected</th>
                        <th><img src="<?php if($quinto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                        <th><?= 'Duplicate Installations: ' . $softwareDuplicate ?></th>
                        <th><?= 'Est Applications: ' . $porcEstDuplicate ?></th>
                    </tr>
                    <tr>
                        <th>Erroneus Installations?</th>
                        <th>Incorrect installations detected</th>
                        <th><img src="<?php if($sexto == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                        <th><?= 'Erroneus Installations: ' . $erroneus ?></th>
                        <th><?= 'Est Erroneous: ' . $porcEstErroneus ?></th>
                    </tr>

                    <tr>
                        <td class="bold" rowspan="2">Maximization</td>
                        <td>Cloud Deployment?</td>
                        <td>> 50% Cloud</td>
                        <td><img src="<?php if($septimo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                        <td><?= 'O365: ' . $totalOffice365 ?></td>
                        <td><?= 'Office: ' . $totalOffice ?></td>
                    </tr>
                    <tr>
                        <td>Optimized Virtualization?</td>
                        <td>> 50% Virtualized</td>
                        <td><img src="<?php if($octavo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></td>
                        <td><?= 'VM: ' . $totalServidorVirtual ?></td>
                        <td><?= 'Servers: ' . $totalServidorFisico ?></td>
                    </tr>

                    <tr>
                        <th class="bold" rowspan="2">Security</th>
                        <th>Unsupported Software?</th>
                        <th>Unsupported software detected</th>
                        <th><img src="<?php if($noveno == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                        <th colspan="2"><?= $soft ?></th>
                    </tr>
                    <tr>
                        <th>Vulnerability Exposure?</th>
                        <th>Vulnerability exposure detected</th>
                        <th><img src="<?php if($decimo == true){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>"></th>
                        <th colspan="2"><?= 'Windows Server' ?></th>
                    </tr>

                    <tr>
                        <td class="bold" rowspan="2">Recommendation</td>
                        <td class="bold">Optimization Opportunity?</td>
                        <td class="bold">3 or more areas of improvement</td>
                        <td><img src="<?php if($fallas < 3){ echo $GLOBALS["domain_root"] . '/imagenes/check2.png'; } 
                        else{ echo $GLOBALS["domain_root"] . '/imagenes/delete2.png'; } ?>" title="<?php if($fallas < 3){ echo 'Low SAM Benefit'; } 
                        else if($fallas >= 3 && $fallas < 6){ echo 'Medium SAM Benefit'; } else{ echo 'High SAM Benefit'; } ?>"></td>
                        <td colspan="2" class="bold"><?php if($fallas < 3){ echo 'Low SAM Benefit'; } 
                        else if($fallas >= 3 && $fallas < 6){ echo 'Medium SAM Benefit'; } else{ echo 'High SAM Benefit'; } ?></td>
                    </tr>
                </tbody>
            </table>

            <br>

            <div style="width:800px; margin: 0 auto; overflow:hidden;">
                <form id="form1" name="form1" method="post" action="exportar.php" target="_blank">
                    <input type="hidden" id="idDiagnostic" name="idDiagnostic" value="<?= $idDiagnostic ?>">
                    <input type="hidden" id="reportePDF" name="reportePDF" value="1">
                    <div class="btn botonDiagnostic float-lt" onclick="document.getElementById('form1').submit();">
                        <i class="fa fa-download fa-lg" aria-hidden="true"> Download</i>
                    </div>
                    <div class="btn botonDiagnostic float-rt" onclick="location.href='sam.php';">
                        <i class="fa fa-lg" aria-hidden="true">NEXT STEP</i>
                    </div>
                </form>
            </div>
        </div>
    </div>                    
</section>