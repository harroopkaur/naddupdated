<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_usabilidad_Microsoft.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$usabilidad = new UsabilidadMicrosoft();
$general    = new General();
$log = new log();

$exito=0;
$error=0;
$lista = array();

if(isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    $nombre_imagen = $_FILES['archivo']['name'];
    $tipo_imagen = $_FILES['archivo']['type'];
    $tamano_imagen = $_FILES['archivo']['size'];
    $temporal_imagen = $_FILES['archivo']['tmp_name'];

    // Validaciones        
    if($nombre_imagen!=""){
        $extension = explode(".",$nombre_imagen);  // Obtener tipo de archivo
        $long = count($extension) - 1;
        if(($extension[$long] != "rar") && ($extension[$long] != "RAR")) { $error = 1; }  
        // Permitir subir solo imagenes JPG,
    }else{
        $error=2;	
    }

    if(!file_exists($GLOBALS['app_root'] . "/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
        mkdir($GLOBALS['app_root'] . "/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true);
    }

    $rar_file = rar_open($_FILES['archivo']['tmp_name'], 'InfoLA2015**') or die("Can't open Rar archive");

    $entries = rar_list($rar_file);

    foreach ($entries as $entry) {
        $entry->extract('archivos_csvf1/' . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . '/');
    }

    rar_close($rar_file);


    copy("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/LA_usage_results.csv", "archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/LA_usage_results" . date("dmYHis") . ".csv");     

    if (($fichero = fopen("archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/LA_usage_results.csv", "r")) !== FALSE) {
        $i=1;
        while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
            if($i == 2 && ($datos[0] != "Equipo" || $datos[1] != "Proceso" || $datos[2] != "PID" || $datos[3] != "Creado" 
            || $datos[4] != "Hora de Inicio" || $datos[5] != "Finalizado" || $datos[6] != "Hora Fin" || utf8_encode($datos[7]) != "Duración")){
                $error = 3;
                break;
            }
            $i++;
        }
    }

    if($error == 0) {               
        $imagen = $_SESSION['client_id'] . "/" . $_SESSION["client_empleado"] . "/LA_usage_results.csv";

        $usabilidad->eliminar($_SESSION['client_id'], $_SESSION["client_empleado"]);
        if (($fichero = fopen("archivos_csvf1/".$imagen, "r")) !== FALSE) {
            $i=1;
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {                    
                if($i > 2){
                    if(!$usabilidad->insertar($_SESSION['client_id'], $_SESSION["client_empleado"], $datos[0], $datos[1], $datos[2], $general->reordenarFecha($datos[3], "/"),
                    $datos[4], $general->reordenarFecha($datos[5], "/"), $datos[6], $datos[7])){    
                        echo $usabilidad->error;
                    }
                }
                $exito=1;	

                $i++; 
            }
        }

        $exito=1;  
        
        $log->insertar(27, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
    }

}