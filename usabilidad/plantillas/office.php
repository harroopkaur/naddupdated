<div style="width:96%; margin:0 auto; margin:20px; overflow:auto; height:400px;">
    <table style="width:1550px; margin-top:-5px; margin-left: 0;" class="tablap" id="tablaUsabilidad">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th rowspan="2" align="center" valign="middle"><span>Equipo</span></th>
            <th rowspan="2" align="center" valign="middle"><span>Familia</span></th>
            <th rowspan="2" align="center" valign="middle"><span>Edición</span></th>
            <th rowspan="2" align="center" valign="middle"><span>Versión</span></th>
            <th rowspan="2" align="center" valign="middle"><span>Instalación</span></th>
            <th rowspan="2" align="center" valign="middle"><span>En Uso</span></th>
            <th colspan="7" align="center" valign="middle"><span>Usabilidad Procesos</span></th>
        </tr>
        <tr>
            <th align="center" valign="middle"><span>Word</span></th>
            <th align="center" valign="middle"><span>Excel</span></th>
            <th align="center" valign="middle"><span>Outlook</span></th>
            <th align="center" valign="middle"><span>Power P.</span></th>
            <th align="center" valign="middle"><span>OneNote</span></th>
            <th align="center" valign="middle"><span>Access</span></th>
            <th align="center" valign="middle"><span>Lync</span></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            foreach($office as $row){
                $tiempoWinword = explode(":", $row["tiempoWinword"]); 
                $tiempoExcel = explode(":", $row["tiempoExcel"]); 
                $tiempoOutlook = explode(":", $row["tiempoOutlook"]); 
                $tiempoPowerpnt = explode(":", $row["tiempoPowerpnt"]); 
                $tiempoOnenote = explode(":", $row["tiempoOnenote"]); 
                $tiempoAccess = explode(":", $row["tiempoAccess"]);
                $tiempoLync = explode(":", $row["tiempoLync"]); 
            ?>
                <tr>
                    <td align="center" valign="middle"><?= $row["equipo"] ?></td>
                    <td align="center" valign="middle"><?= $row["familia"] ?></td>
                    <td align="center" valign="middle"><?= $row["edicion"] ?></td>
                    <td align="center" valign="middle"><?= $row["version"] ?></td>
                    <td align="center" valign="middle"><?= $row["instalacion"] ?></td>
                    <td align="center" valign="middle"><?php if($row["usabilidad"] > 0){ echo "Si"; }else{ echo "No"; }?></td>
                    <td align="center" valign="middle"><?php echo intval($tiempoWinword[0]) . "H " . intval($tiempoWinword[1]) . "M " . intval($tiempoWinword[2]) . "S"; ?></td>
                    <td align="center" valign="middle"><?php echo intval($tiempoExcel[0]) . "H " . intval($tiempoExcel[1]) . "M " . intval($tiempoExcel[2]) . "S"; ?></td>
                    <td align="center" valign="middle"><?php echo intval($tiempoOutlook[0]) . "H " . intval($tiempoOutlook[1]) . "M " . intval($tiempoOutlook[2]) . "S"; ?></td>
                    <td align="center" valign="middle"><?php echo intval($tiempoPowerpnt[0]) . "H " . intval($tiempoPowerpnt[1]) . "M " . intval($tiempoPowerpnt[2]) . "S"; ?></td>
                    <td align="center" valign="middle"><?php echo intval($tiempoOnenote[0]) . "H " . intval($tiempoOnenote[1]) . "M " . intval($tiempoOnenote[2]) . "S"; ?></td>
                    <td align="center" valign="middle" style="<?php if($row["edicion"] == "Professional" && $row["tiempoAccess"] == "0:0:0"){ echo 'background-color:red;'; } ?>"><?php echo intval($tiempoAccess[0]) . "H " . intval($tiempoAccess[1]) . "M " . intval($tiempoAccess[2]) . "S"; ?></td>
                    <td align="center" valign="middle" style="<?php if($row["edicion"] == "Professional" && $row["tiempoLync"] == "0:0:0"){ echo 'background-color:red;'; } ?>"><?php echo intval($tiempoLync[0]) . "H " . intval($tiempoLync[1]) . "M " . intval($tiempoLync[2]) . "S"; ?></td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
    
    <div style="float:right; margin-bottom:20px; margin-right:20px;"><div class="botones_m2" id="boton1" onclick="location.href='visio.php'">Visio</div></div>
<script>
   $("#tablaUsabilidad").tableHeadFixer();
</script>