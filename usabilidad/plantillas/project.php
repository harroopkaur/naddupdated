<div style="width:96%; margin:0 auto; margin:20px; overflow:auto; height:400px;">
    <table style="width:1350px; margin-top:0; margin-left: 0;" class="tablap" id="tablaUsabilidad">
        <thead>
            <tr style="background:#333; color:#fff;">
                <th align="center" valign="middle"><span>Equipo</span></th>
                <th align="center" valign="middle"><span>Familia</span></th>
                <th align="center" valign="middle"><span>Edición</span></th>
                <th align="center" valign="middle"><span>Versión</span></th>
                <th align="center" valign="middle"><span>Instalación</span></th>
                <th align="center" valign="middle"><span>En Uso</span></th>
                <th align="center" valign="middle"><span>Proceso</span></th>
            </tr>            
        </thead>
        <tbody>
            <?php 
            foreach($office as $row){
                $tiempo = explode(":", $row["tiempo"]); 
            ?>
                <tr>
                    <td align="center" valign="middle"><?= $row["equipo"] ?></td>
                    <td align="center" valign="middle"><?= $row["familia"] ?></td>
                    <td align="center" valign="middle"><?= $row["edicion"] ?></td>
                    <td align="center" valign="middle"><?= $row["version"] ?></td>
                    <td align="center" valign="middle"><?= $row["instalacion"] ?></td>
                    <td align="center" valign="middle"><?php if($row["usabilidad"] > 0){ echo "Si"; }else{ echo "No"; }?></td>
                    <td align="center" valign="middle"><?php echo intval($tiempo[0]) . "H " . intval($tiempo[1]) . "M " . intval($tiempo[2]) . "S"; ?></td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
    
<div style="float:right; margin-right:20px; margin-bottom:20px;"><div class="botones_m2" id="boton1" onclick="location.href='visio.php'">Visio</div></div>

<script>
   $("#tablaUsabilidad").tableHeadFixer();
</script>