<input type="hidden" id="nombreArchivo">
<div style="width:98%; padding:20px; margin-bottom:20px; overflow:hidden;">
    <div class="fondo1" id="fondo1"></div>
    <div class="fondo" id="fondo" style="display:none;">
        <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" alt="Cargando" style="width:50px; height:50px; top:50%; margin-top:25px; left:50%; margin-left:25px; position:absolute;">
    </div>
    <!--inicio modal lista personalizada-->
    <div class="modal-personal" id="modal-listaPersonalizada">
        <div class="modal-personal-head">
            <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Equipos Personalizada</p></h1>
        </div>

        <div class="modal-personal-body">
            <div style="width:400px; margin:0 auto;">
                <div style="float:left;">
                    <span class="bold">Equipos:</span> 
                </div>
                <div style="float:left; margin-left:15px;">
                    <textarea id="equipo" name="equipo" style="width:200%; height:300px; border:1px solid;" ></textarea>
                </div>
            </div>
        </div>

        <div class="modal-personal-footer">
            <div style="float:right;"><div class="botones_m2" id="boton1" onclick="$('#fondo1').hide(); $('#modal-listaPersonalizada').hide();">Salir</div></div>
            <div style="float:right;"><div class="botones_m2" id="boton1" onclick="generarListadoPersonal()">Generar Archivo</div></div>
        </div>
    </div>
    <!--fin modal lista personalizada-->

    <h1 class="textog negro" style="text-align:center; line-height:35px;">Usabilidad de Microsoft Office</h1>

    <br>

    <p style="font-size:16px; color:#000; font-weight:300;">Estimado Cliente, por favor siga los pasos a continuaci&oacute;n:</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">1. -&nbsp;</span>Descargar archivo <a class="link1" href="<?= $GLOBALS['domain_root'] ?>/LA_UsageTool_v2.0.rar">LA_UsageTool_v2.0.rar</a></p><br />
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">2.-&nbsp;</span>Extraer los archivos contenidos en el controlador de dominio de su empresa dentro de una carpeta y ejecutar el archivo llamado "<span class="bold">Renombrar.bat</span>" haciendo doble click en el mismo.</p><br>
    <p style="float:left"><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">3.-&nbsp;</span>Generar lista de equipos a ser ejecutados:</p>
    <input name="listaWebtool" type="button" value="Generar lista desde Webtool" class="botones_m5" onclick="listaWebtool();"/>
    <input name="listaPersonalizada" type="button" value="Generar lista personalizada" class="botones_m5" onclick="listaPersonalizada();"/><br style="clear:both">
    <p style="float:left"><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">* &nbsp;</span>En caso de Elegir "Lista Personalizada" favor poner los nombres de equipos uno debajo de otro sin puntos ni comas</p><br><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">4.-&nbsp;</span>En el paso anterior se generar&aacute; un archivo llamado <span class="bold">Equipos.zip</span>, descomprimir el mismo y guardarlo en la misma carpeta en la que se encuentran los componentes de la herramienta reemplazando el archivo original Equipos.txt por el nuevo.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">5.-&nbsp;</span>Click derecho sobre el archivo "<span class="bold">LA_UsageTool.vbe</span>". Seleccionar la opci&oacute;n "Open with Command Prompt" o "Ejecutar con Símbolo de Sistema"</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">6.-&nbsp;</span>Se iniciar&aacute; una ventana de Command Prompt  donde muestra los procesos que se van a escanear. Aparecer&aacute; un mensaje donde debe introducir por cuantos minutos desea ejecutar el script y luego presionar "ENTER" para comenzar el proceso de escaneo, la herramienta puede ser ejecutada hasta por 720 diarios (12 horas).</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">7.-&nbsp;</span>El script empezara a monitorear los procesos especificados para todos los equipos que introdujo en el archivo "Equipos.txt". Si la conexi&oacute;n con el equipo es satisfactoria aparecer&aacute; un mensaje de "Monitoreando procesos…OK" y cuando la conexi&oacute;n con el equipo a escanear sea fallida indicar&aacute; un mensaje de Error y una descripci&oacute;n del Error y pasar&aacute; a la siguiente maquina.</p>
    <p>Una vez finalizado el escaneo aparecer&aacute; el siguiente mensaje "<span class="bold">Reporte de usabilidad generado exitosamente!</span>".</p>
    <br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">8.-&nbsp;</span>Se crear&aacute; un archivo .rar llamado "<span class="bold">LA_Usage_results.rar</span>" que contiene el reporte de detalle y resumen de los procesos escaneados en los equipos.</p><br>
    <p><span style="color:#2D6BA4; font-weight:bold; font-size:18px;">9.-&nbsp;</span>Subir el archivo "<span class="bold">LA_Usage_results.rar</span>"</p>
    <p>Nota* El almacenamiento de los datos puede tardar por favor no cierre esta ventana y espere al mensaje "Archivo cargado con &eacute;xito"</p>
    <br>

    <fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">LA_Usage_results</legend>

        <form enctype="multipart/form-data" name="form_e" id="form_e" method="post" action="despliegue1.php" >
            <input name="insertar" id="insertar" type="hidden" value="1" >
            <div class="contenedor_archivo">
                <div class="input_archivo">
                    <input type="text" class="url_file" name="url_file" disabled id="url_file" value="Nombre del Archivo...">
                    <input type="file" class="archivo" name="archivo" id="archivo" accept=".rar">
                    <input type="button" id="boton1" class="botones_m5" value="Buscar">
                </div>
                <div class="contenedor_boton_archivo">
                    <input name="insertar" type="button" id="insertar" value="Cargar Archivo" onclick="$('#cargando').show(); document.getElementById('form_e').submit();" class="botones_m5" />
                </div>
            </div>
        </form>

        <p style="text-align:center; color:#2D6BA4;">Buscar tu archivo, y subelo haciendo click en "Cargar Archivo"</p>
        <div id="cargando" style="display:none;text-align:center; color:#2D6BA4; width:80%; margin:5px;">Procesando...</div>
        <?php if($exito=='1'){ ?>
            <div class="exito_archivo">Archivo cargado con &eacute;xito</div>
            <script>
                $.alert.open('info', "Archivo cargado con éxito", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }else{ ?>

        <?php }
        if($error=='1'){ ?>
            <div class="error_archivo">Debe ser un archivo con extensi&oacute;n rar</div>
            <script>
                $.alert.open('alert', "Debe ser un archivo con extensión rar", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error=='2'){ ?>
            <div class="error_archivo">Debe seleccionar un archivo</div>
            <script>
                $.alert.open('alert', "Debe seleccionar un archivo", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php }
        if($error == 3){
        ?>
            <div class="error_archivo">Los campos del archivo <br> Consolidado Addremove.csv <br> no son compatibles</div>
            <script>
                $.alert.open('alert', "Los campos del archivo Consolidado Addremove.csv no son compatibles", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php
        }
        if($error == 4){
        ?>
            <div class="error_archivo">Los campos del archivo Resultados_Escaneo.csv <br> no son compatibles</div>
            <script>
                $.alert.open('alert', "Los campos del archivo Resultados_Escaneo.csv no son compatibles", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php
        }
        if($error > 5){ ?>
            <div class="error_archivo"><?=$consolidado->error?></div>
            <script>
                $.alert.open('alert', "<?=$consolidado->error?>", {'Aceptar' : 'Aceptar'}, function() {
                });
            </script>
        <?php } ?>
    </fieldset>

    <br />

    <div style="float:right;"><div class="botones_m2" id="boton1" onclick="location.href='office.php'">MS Office</div></div>
</div>

<script>
    $(document).ready(function(){
        $("#archivo").change(function(e){
            $("#nombreArchivo").val("#url_file");
            addArchivo(e);
        });
    });

    function addArchivo(e){
        file = e.target.files[0]; 
        reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        $($("#nombreArchivo").val()).val("");
        $($("#nombreArchivo").val()).val(file.name);
        return true;
    }

    function fileOnload(e) {
        result=e.target.result;
    }
    
    function listaWebtool(){
        $("#fondo").show();
        $.post("ajax/listaEquiposWebtool.php", { token : localStorage.licensingassuranceToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
            
            $("#fondo").hide();
            if(data[0].result === 0){
                $.alert.open("alert", "No se pudo crear la carpeta del cliente", {"Aceptar" : "Aceptar"}, function() {
                });
            }else if(data[0].result === 1){
                location.href = "<?= $GLOBALS["domain_root"] ?>/" + data[0].archivo;
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    }
    
    function listaPersonalizada(){
        $("#fondo1").show();
        $("#modal-listaPersonalizada").show();
        $("#equipo").focus();
    }
    
    function generarListadoPersonal(){
        $("#fondo1").hide();
        $("#modal-listaPersonalizada").hide();
        $("#fondo").show();
        
        if($("#equipo").val() === ""){
            $.alert.open("alert", "No existen equipos", {"Aceptar" : "Aceptar"}, function() {
                $("#fondo1").show();
                $("#modal-listaPersonalizada").show();
            });
            return false;
        }    
        
        $.post("ajax/generarLista.php", { equipo : $("#equipo").val(), token : localStorage.licensingassuranceToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
            $("#fondo").hide();
            if(data[0].result === 0){
                $.alert.open("alert", "No se pudo crear la carpeta del cliente", {"Aceptar" : "Aceptar"}, function() {
                });
            } else if(data[0].result === 1){
                location.href = "<?= $GLOBALS["domain_root"] ?>/" + data[0].archivo;
                $("#equipo").val("");
            } else if(data[0].result === 2){
                $.alert.open("alert", "No existen equipos", {"Aceptar" : "Aceptar"}, function() {
                });
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    }
</script>