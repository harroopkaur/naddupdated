<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];

        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $detalles = new DetallesE_f();       
            $listado = $detalles->listaEquiposOffice($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $result = 1;
            
            if(!file_exists($GLOBALS['app_root'] . "/usabilidad/tmp/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"]) && !mkdir($GLOBALS['app_root'] . "/usabilidad/tmp/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true)){
                $result = 0;
            } 
            
            if($result == 1){
                $fp = fopen("../tmp/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/Equipos.txt","w");   
                foreach($listado as $row){
                    fwrite($fp, $row["equipo"] . "\r\n");
                }
                fclose($fp);

                $zip = new ZipArchive;
                $zip->open("../tmp/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/Equipos.zip",ZipArchive::CREATE);
                $zip->addFile("../tmp/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/Equipos.txt", "Equipos.txt");
                $zip->close();
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'archivo'=>"usabilidad/tmp/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/Equipos.zip", 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);