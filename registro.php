<?php
require("configuracion/inicio.php");
require("clases/clase_general.php");
require("clases/clase_validator.php");
require("clases/clase_clientes.php");
require("clases/clase_pais.php");
require("clases/clase_email.php");

$usuario = new Clientes($conn);
$general = new General();
$validator = new validator("form1");
$paises = new Pais($conn);
$email = new email();


$error = 0;
$exito = 0;

if (isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    // Validaciones
    $email = "";
    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false){
        $email = $general->get_escape($_POST["email"]);
    }
    
    $nombre = "";
    if(isset($_POST["nombre"])){
        $nombre = $general->get_escape($_POST["nombre"]);
    }

    $apellido = "";
    if(isset($_POST["$apellido"])){
        $apellido = $general->get_escape($_POST["apellido"]);
    }

    $telefono = "";
    if(isset($_POST["telefono"])){
        $telefono = $general->get_escape($_POST['telefono']);
    }

    $empresa = "";
    if(isset($_POST['empresa'])){
        $empresa = $general->get_escape($_POST['empresa']);
    }

    $pais = 0;
    if(isset($_POST['pais']) && filter_var($_POST['pais'], FILTER_VALIDATE_INT) !== false){
        $pais = $_POST['pais'];
    }

    $login = "";
    if(isset($_POST['login'])){
        $login = $general->get_escape($_POST['login']);
    }

    $clave = "";
    if(isset($_POST['clave'])){
        $clave = $general->get_escape($_POST['clave']);
    }
    
    $clave2 = "";
    if(isset($_POST['clave2'])){
        $clave2 = $general->get_escape($_POST['clave2']);
    }
  
    if ($_POST['politica'] != '1') {
        $error = 4;
    }  // contrase?as no coinciden
    if ($error == 0) {
        if ($usuario->login_existe($email, 0)) {
            $error = 2;
        }  // Email duplicado
        if ($clave != $clave2) {
            $error = 1;
        }  // contrase?as no coinciden
        if (strlen($clave) < 6) {
            $error = 3;
        }  // Contrase?a m?nima de 6 caracteres
    }
    if ($error == 0) {        
        if ($usuario->insertar($nombre, $apellido, $email, $telefono, $empresa, $pais, $login, $clave)) {
            $nombreAux = $nombre . ' ' . $apellido;
            $email->enviar_suscripcion($email, $nombre, $apellido, $login);
            $email->enviar_suscripcionr($email, $nombreAux);
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}


$validator->create_message("msj_nombre", "nombre", " Obligatorio", 0);
$validator->create_message("msj_apellido", "apellido", " Obligatorio", 0);
$validator->create_message("msj_email", "email", " No es un e-mail valido", 3);
$validator->create_message("msj_empresa", "empresa", " Obligatorio", 0);
$validator->create_message("msj_telefono", "telefono", " Obligatorio", 0);
$validator->create_message("msj_pais", "pais", " Obligatorio", 0);
$validator->create_message("msj_login", "login", " Obligatorio", 0);
$validator->create_message("msj_clave", "clave", " Obligatorio", 0);
$validator->create_message("msj_clave2", "clave2", " Obligatorio", 0);
?>
<!DOCTYPE HTML>
<html>
    <head>
        <?php
        require("plantillas/head.php");
        ?>
        <style>
            body {
                background:url(imagenes/inicio/bg_body.jpg) top left no-repeat !important;	
            }
            #tablar th{
                font-size:12px;
                font-weight:bold;

            }
            #tablar #insertar{
                width: 50%;
                padding: 12px 28px;
                background:#000;
                color:#fff;
            }
        </style>
        <title>.:Licensing Assurance:.</title>
        <!-- Custom Theme files -->
        <link href="css/style3.css" rel="stylesheet" type="text/css" media="all"/>
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <meta name="keywords" content="trial, software" />
        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
    </head>
    <body>
        <?php if ($exito == true) { ?>
            <script type="text/javascript">
                $.alert.open('alert', "Registro Insertado con Éxito, pronto recibirá un correo de autorización de acceso a nuestra version trial", {'Aceptar' : 'Aceptar'}, function() {
                    location.href = '<?= $GLOBALS['domain_root'] ?>';
                });
            </script>
        <?php
        }
        ?>


        <!--sign up form start here-->
        <div style="width:100%; position:relative; text-align:center;">
            <div class="app" style=" display:inline-block; overflow:hidden;">
                <h1 style="font-size:24px;color:#fff;">Bienvenido al Servicio de Manejo de Licenciamiento en la nube </h1>
                <br>
                <div style="overflow:hidden; padding:20px;">
                    <img src="imagenes/inicio/logo2.png"  style="margin:0 auto; display:block;">
                </div>
                <div style="width:100%; height:120px; min-height:120px;">&nbsp;</div>
            </div>

            <div class="appr" style=" display:inline-block; margin-left:25px; overflow:hidden;   padding-top:20px;padding-bottom: 20px;">
                <div  style="overflow:hidden; width:80%; margin:0 auto; background:#fff; padding:20px; border-radius:10px;">

                    <h1 style="font-size:24px;color:#000; ">Registro cliente</h1> <br>

                    <p style="color:red;">Para registrarse y recibir su Usuario y Clave por favor contactenos Info@LicensingAssurance.com</p>

                    <br>

                    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="insertar" id="insertar" value="1" />
                        <?php $validator->print_script(); ?>
                        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                            echo $usuario->error;
                        } ?></font></div>

                        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" id="tablar">
                            <tr>
                                <th width="80" align="left" valign="top">Nombre:</th>
                                <td align="left"><input name="nombre" id="nombre" type="text" value="" size="30" maxlength="250"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nombre") ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">Apellido:</th>
                                <td align="left"><input name="apellido" id="apellido" type="text" value="" size="30" maxlength="250"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_apellido") ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">E-mail:</th>
                                <td align="left"><input name="email" id="email" type="text" value="" size="30" maxlength="250"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">Empresa</th>
                                <td align="left"><input name="empresa" id="empresa" type="text" value="" size="30" maxlength="250"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">Login:</th>
                                <td align="left"><input name="login" id="login" type="text" value="" size="30" maxlength="250"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?><?php if ($error == 2) {
                                    echo "El login ya existe";
                                } ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">Clave:</th>
                                <td align="left"><input name="clave" id="clave" type="password" value="" size="14" maxlength="20"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_clave") ?><?php if ($error == 3) {
                                    echo "Contrase?a m?nima de 6 caracteres";
                                } ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">Repita Clave:</th>
                                <td align="left"><input name="clave2" id="clave2" type="password" value="" size="14" maxlength="20"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_clave2") ?><?php if ($error == 1) {
                                    echo "la contrase?a no coinciden";
                                } ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">Tel&eacute;fono:</th>
                                <td align="left"><input name="telefono" id="telefono" type="text" value="" size="30" maxlength="250"  />
                                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_telefono") ?></font></div></td>
                            </tr>
                            <tr>
                                <th width="80" align="left" valign="top">Pais:</th>
                                <td align="left">
                                    <select name="pais" id="pais">
                                        <option value="" selected>Seleccione...</option>
                                        <?php
                                        $lista_p=$paises->listar_todo();
                                        if($lista_p){
                                            foreach($lista_p as $reg_p){
                                        ?>
                                                <option value="<?= $reg_p["id"] ?>"><?= $reg_p["nombre"] ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pais") ?></font></div></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">&nbsp;</td></tr>
                            <tr>
                            <tr>
                                <td colspan="2" align="center">   <label>
                                        <input type="checkbox" name="politica" id="politica" value="1"  />
                                    </label><em>Estoy de acuerdo con los T&eacute;rminos y Condiciones de Uso de la empresa.</em><font color="#FF0000"><?php if ($error == 4) {
                                    echo "Debe aceptar los terminos y condiciones";
                                } ?></font></td></tr>
                            <tr>
                            <tr>
                                <td colspan="2" align="center">&nbsp;</td></tr>
                            <tr>
                                <td colspan="2" align="center"><input name="insertar" type="button" id="insertar" value="Registrarse" onclick="validate();" class="boton" /></td>
                            </tr>
                        </table>
                    </form>

                </div>
                <div style="width:100%; height:20px; min-height:20px; clear:both;">&nbsp;</div>
            </div>
        </div>
        <div class="copyright" >
            <p>Todos Los Derechos Reservados 2016 - <a href="https://www.licensingassurance.com"> www.licensingassurance.com </a></p>
        </div>
        <!--sign up form end here-->
    </body>
</html>