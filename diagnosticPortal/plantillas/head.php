<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Diagnostic Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content=" Licensing Assurance"/>
    <meta name="robots" content="noodp,noydir"/>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="assets/styles/main.css"/>
</head>
<body>