<div class="barra">
    <div class="slides_container">
        <img class="theslides" src="assets/images/dc1.png">
        <img class="theslides" src="assets/images/microsoft1.png">
        <img class="theslides" src="assets/images/ibm1.png">
        <img class="theslides" src="assets/images/sap1.png">
        <img class="theslides" src="assets/images/oracle1.png">
    </div>
    <div class="selected_slide"></div>
    
    <div class="selectButton">
        <div class="dropdown">
            <div class="btn btn-default btn-opc" id="dropdownMenu2" data-toggle="dropdown">Select an option&nbsp;<b class="caret"></b></div>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" id="dropdown_dc">DC</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" id="dropdown_microsoft">MICROSOFT</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" id="dropdown_ibm">IBM</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" id="dropdown_sap">SAP</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" id="dropdown_oracle">ORACLE</a></li>
            </ul>
        </div>
    </div>
        
    <div class="slides_container-right">
        <img class="theslides-right" src="assets/images/licensingAssurance.png">
    </div>
</div>

<br><br>
<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-2">
            <div class="row">
                <div class="col-sm-9">
                    <div class="row">
                        <img src="assets/images/sidebar_left1.png" style="width:100%; height:auto;">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-9 col-sm-offset-1">
                    <div class="row">
                        <img src="assets/images/banner1.png" style="width:100%; height:auto;">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-sm-2">
            <div class="panel panel-default four_sidebar">
                <div class="panel-body four">
                    <h1>Get Now</h1>
                    <center><img src="assets/images/velocimetro.png" class="imgGetNow">
                    <img src="assets/images/pointer.png" class="imgGetNow">
                    <img src="assets/images/timer.png" class="imgGetNow"></center>
                    <button type="button" class="btn btn-primary four_button_one">Scope</button><br>
                    <button type="button" class="btn btn-primary four_button_two">Optimization</button>
                    <button type="button" class="btn btn-primary four_button_three">Cybersecurity</button>
                    <button type="button" class="btn btn-primary four_button_four">Recommendation</button>
                </div>
            </div>
            
            <center><img src="assets/images/btnApp.png" class="btnApp"></center>
        </div>
    </div>
</div>

<div class="col-sm-10 col-sm-offset-1">
    <div class="row">
        <div class="col-sm-3">
            <div class="col-sm-12 cuadro">
                <p class="titulo">Diagnostic W/Results & No time</p>
                <div class="row">
                    <div class="col-sm-3">
                        <img src="assets/images/look.png" class="imgGetNow">
                    </div>

                    <div class="col-sm-9 content">
                        <p>Tool simple & reliable that Diagnoses and capture areas to optimize and know your Software Assets</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="col-sm-12 cuadro">
                <p class="titulo">Step by Step...</p>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <img src="assets/images/step-by-step.png" data-toggle="modal" id="requirements_look" data-target="#technical_requirements_modal" style="width: 100%; height:auto; cursor:pointer;">
                        <!-- MODAL -->
                        <div class="modal fade " id="technical_requirements_modal" tabindex="-1" role="dialog" aria-labelledby="Technidal Requirements"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered " role="document">
                                <div class="modal-content modal_container">
                                    <div class="modal-body ">
                                        <center><iframe  src="https://www.youtube.com/embed/mU2l-zxURqk" class="modal_video" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="col-sm-12 cuadro">
                <p class="titulo">Optimization</p>
                <div class="row">
                    <div class="col-sm-3">
                        <img src="assets/images/optimization.png" class="imgGetNow">
                    </div>

                    <div class="col-sm-9 content">
                        <p>Unused Software<br>
                        Unused Devices<br>
                        Unused Users<br>
                        Duplicate Installations
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="col-sm-12 cuadro">
                <p class="titulo">Cybersecurity</p>
                <div class="row">
                    <div class="col-sm-3">
                        <img src="assets/images/cybersecurity.png" class="imgGetNow">
                    </div>

                    <div class="col-sm-9 content">
                        <p>Detects Unsupported<br>
                        Software & possible<br>
                        vulnerabilities</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>