<?php
########################################################################
# VARIABLES DE CONFIGURACION:                                          #
# approot:      Ubicacion fisica de la aplicacion                      #
# Domain_root:  Ubicacion HTTP de la aplicacion                        #
# DBuser:       Nombre de usuario de la base de datos                  #
# DBpass:       Password del usuario de la base de dato                #
# DBname:       Nombre de la base de datos                             #
# DBserver:     Nombre del servidor de bases de datos                  #
########################################################################

$configuracion = 1;

if($configuracion == 1) {
    #CONFIGURACION SERVER#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/diagnosticPortal";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/diagnosticPortal";
    $GLOBALS['domain_root'] = "https://".$_SERVER['HTTP_HOST']."/diagnosticPortal";
    $GLOBALS['domain_root1'] = "https://".$_SERVER['HTTP_HOST'];
    $DBusuario = "";
    $DBnombre = "";
    $DBservidor = "";
    $DBcontrasena = "";
    $TIEMPO_MAXIMO_SESION = 600; //3600;  // en segundos	
} else if($configuracion == 2) {
    #CONFIGURACION PRUEBA SERVER#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/diagnosticPortal";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/diagnosticPortal";
    $GLOBALS['domain_root'] = "https://".$_SERVER['HTTP_HOST']."/diagnosticPortal";
    $GLOBALS['domain_root1'] = "https://".$_SERVER['HTTP_HOST'];
    $DBusuario = "";
    $DBnombre = "";
    $DBservidor = "";
    $DBcontrasena = "";
    $TIEMPO_MAXIMO_SESION = 600; //3600;  // en segundos
    
} else if($configuracion == 3){
    #CONFIGURACION LOCAL#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/licensingassurance/diagnosticPortal";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT']."/licensingassurance";
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/licensingassurance/diagnosticPortal";
    $GLOBALS['domain_root'] = "http://".$_SERVER['HTTP_HOST']."/licensingassurance/diagnosticPortal";
    $GLOBALS['domain_root1'] = "http://".$_SERVER['HTTP_HOST']."/licensingassurance";
    $DBusuario = "";
    $DBnombre = "";
    $DBservidor = "";
    $DBcontrasena = "";
    $TIEMPO_MAXIMO_SESION = 600;  // en segundos
}