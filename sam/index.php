<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/procesos/indexFabricantes.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS['app_root'] . "/clases/clase_email.php");
require_once($GLOBALS['app_root'] . "/sam/clases/acceso.php");

$nueva_funcion = new funcionesSam();
$nuevo_acceso  = new accesoSam();

$email = new email();


require_once($GLOBALS['app_root'] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>
<script src="<?=$GLOBALS['domain_root']?>/js/cabecera.js"></script>
<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 12;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    
    <div class="divContenedorRight">
        <div class="divMenuContenido">
            
            <div style="margin-left:150px;" id="contenedor_ver2">
            <?php
            if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 3)){
                //echo $nueva_funcion->menu3("sam", $_SESSION["client_empleado"]);
                echo "<script>location.href='" . $GLOBALS["domain_root"] . "/sam/repositorios/'</script>";
            }
            else{
                if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 2)){ 
                    //echo $nueva_funcion->menu3("procesos", $_SESSION["client_empleado"]);
                    echo "<script>location.href='" . $GLOBALS["domain_root"] . "/sam/procesosSam/'</script>";
                }
                else{
                    if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 4)){
                        //echo $nueva_funcion->menu3("acceso", $_SESSION["client_empleado"]);
                        echo "<script>location.href='" . $GLOBALS["domain_root"] . "/sam/acceso/'</script>";
                    } else{
                        if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 17)){
                            //echo $nueva_funcion->menu3("acceso", $_SESSION["client_empleado"]);
                            echo "<script>location.href='" . $GLOBALS["domain_root"] . "/sam/centroCostos/'</script>";
                        }
                    }
                }
            }
            ?>
            </div>
        </div>
        
        <!--<div style="width:130px; margin:0; padding:0; padding-top: 50px; height:100%; float:left">
            <?= $nueva_funcion->menu2($_SESSION["client_empleado"]); ?>
        </div>

        <style>
            .bordeContenedor{
                display:block;
                width: calc(100% - 140px); 
                margin:0px; 
                padding:0px;
                background:#D9D9D9; 
                min-height:300px; 
                overflow:hidden;
                border-radius: 10px;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
            }
        </style>
        <div class="bordeContenedor">
            <div class="contenido" style="padding:10px;" id="contenedorCentral">
                <?php
                /*if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 3)){
                    include_once($GLOBALS['app_root'] . "/sam/plantillas/index.php");
                }
                else{
                    if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 2)){ 
                    ?>
                        <img src="<?= $GLOBALS['domain_root'] ?>/imagenes/sam/General.png" style="width:100%; height:auto;">
                    <?php
                    }
                    else{
                        if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 4)){
                            $tabla = $nuevo_acceso->datosAcceso($_SESSION["client_empleado"]);
                            $login = "";
                            $pass  = "";
                            $link  = "";
                            foreach($tabla as $row){
                                $login = $row["login"];
                                $pass  = $row["pass"];
                                $link  = $row["carpeta"];
                            }
                        ?>
                            <div style="float:left; width:19%; border:1px solid; padding:10px; text-align:center;">
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/nuebe.jpg" style="width:150px;">
                                    <br>
                                    <br>
                                    <p style="font-size:20px; padding:50px 0px 120px 0px;">Acceso para los usuarios de las pol&iacute;ticas y formularios</p> 
                            </div>

                            <input type="hidden" id="carpetaActual" name="carpetaActual" value="<?= $link ?>">

                            <div style="float:right; width:73%; padding:90px 0px 90px 0px; ">
                                    <div style="overflow:hidden; margin: 1em 25%;">
                                            <p style="float:left; line-height:30px; width:50px; text-align:right;">Usuario</p>
                                            <input type="text" id="login" name="login" style="float:left; margin-left:15px; height:25px; width:200px;" value="<?= $login ?>" maxlength="50">
                                            <br style="clear:left;">
                                            <br>
                                            <p style="float:left; line-height:30px; width:50px; text-align:right;">Clave</p>
                                            <input type="password" id="pass" name="pass" style="float:left; margin-left:15px; height:25px; width:200px;" value="<?= $pass ?>" maxlength="100">
                                            <br style="clear:left;">
                                            <br>
                                            <p style="float:left; line-height:30px; width:50px; text-align:right;">&nbsp;</p>
                                            <p style="float:left; margin-left:15px; height:25px; width:200px;">www.licensingassurance.com/</p>
                                            <br style="clear:left;">
                                            <p style="float:left; line-height:30px; width:50px; text-align:right;">Link</p>
                                            <input type="text" id="link" name="link" style="float:left; margin-left:15px; height:25px; width:200px;" value="<?= $link ?>" onkeyup = "return validarLink(this.value)" maxlength="50">
                                    </div>
                            </div>
                            <br style="clear:right; ">
                            <br>
                            <div style="float:right;" class="botonesSAM boton5" id="guardarAcceso">Guardar</div>
                        <?php
                            echo $nuevo_acceso->funcionBoton();
                        }
                    }
                }
                echo $nueva_funcion->getFuncionesMenuLateral();
                echo $nueva_funcion->getFuncionesMenuSuperior();*/

                ?>
            </div>
        </div>-->
    </div>
</section>

<?php
include_once($GLOBALS['app_root'] . "/plantillas/pie.php");