<?php
class repoDespliegueVMWare extends General{
    
    function agregarBalance($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_VMWareSam (archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo
                FROM `balance_VMWare`
                WHERE balance_VMWare.cliente = :cliente AND balance_VMWare.empleado = :empleado');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalleContrato($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleContrato_VMWareSam (archivo, cliente, empleado, producto, edicion, version, llave, cantidad, medida, tipo, numero, fechaOrden, soporte, fechaFinCobertura)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, producto, edicion, version, llave, cantidad, medida, tipo, numero, fechaOrden, soporte, fechaFinCobertura
                FROM `detalleContrato_VMWare`
                WHERE detalleContrato_VMWare.cliente = :cliente AND detalleContrato_VMWare.empleado = :empleado');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumen($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_VMWareSam (archivo, cliente, empleado, licenseKey, familia, edicion, version, uso, metrica, capacidad, etiqueta, asignado)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, licenseKey, familia, edicion, version, uso, metrica, capacidad, etiqueta, asignado
                FROM resumen_VMWare
                WHERE resumen_VMWare.cliente = :cliente AND resumen_VMWare.empleado = :empleado');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafVMWareSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function archivoArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encArchVMWareSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarEncSam($cliente, $empleado, $archivoDespliegue1, $archivoCompra){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encGrafVMWareSam (cliente, empleado, archivoDespliegue1, archivoCompra, '
            . 'fecha) VALUES (:cliente, :empleado, :archivoDespliegue1, :archivoCompra, NOW())');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado, ':archivoDespliegue1'=>$archivoDespliegue1, ':archivoCompra'=>$archivoCompra));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encArchVMWareSam (cliente, empleado) VALUES (:cliente, :empleado)');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function eliminarEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafVMWareSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarCustomEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchVMWareSam SET custom = NULL, fechaCustom = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarOtrosEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchVMWareSam SET otros = NULL, fechaOtros = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarCustomEncabSam($cliente, $empleado, $custom){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchVMWareSam SET custom = :custom, fechaCustom = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':custom'=>$custom));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarOtrosEncabSam($cliente, $empleado, $otros){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchVMWareSam SET otros = :otros, fechaOtros = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':otros'=>$otros));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    /*inicio metodos nueva forma de controlar el repositorio*/    
    function cantRepositorios($cliente, $empleado){
        $result = 0;
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM encGrafVMWareSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            if($row["cantidad"] !== ""){
                $result = $row["cantidad"];
            }
            return $result;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerEncSamArchivoElim($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafVMWareSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha ASC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function eliminarEncSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafVMWareSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSamArchivo($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafVMWareSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha DESC LIMIT 1');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function encSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafVMWareSam WHERE archivo = :archivo');
            $sql->execute(array('archivo' => $archivo));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    /*fin metodos nueva forma de controlar el repositorio*/
}