<?php
class politicasSam extends General{
    public $error = "ERROR: No se pudo realizar la consulta";

    function existPoliticas($cliente) {
        try {
            $this->conexion();
            $result = 1;
            $sql = $this->conn->prepare('SELECT * FROM politSam WHERE idCliente = :cliente');
            $sql->execute(array('cliente' => $cliente));
            $resultado = $sql->fetchAll();
            if (count($resultado) == 0) {
                $result = $this->agrPoliticas($cliente);
            }
            return $result;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function agrPoliticas($cliente, $carpeta) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO politSam (idCliente, carpeta) VALUES (:cliente, :carpeta)');
            $sql->execute(array('cliente' => $cliente, 'carpeta' => $carpeta));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function updateArchivo($cliente, $politica, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE politSam SET ' . $politica . ' = :archivo WHERE idCliente = :idCliente');
            $sql->execute(array('archivo' => $archivo, 'idCliente' => $cliente));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function politicas($cliente) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM politSam WHERE idCliente = :cliente');
            $sql->execute(array('cliente' => $cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return $error;
        }
    }
}	