<?php
class repoDespliegueUnixOracle extends General{
    
    function agregarBalance($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_UnixOracleSam (archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo
                FROM `balance_unixOracle`
                WHERE balance_unixOracle.cliente = :cliente AND balance_unixOracle.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumenSolaris($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_OracleSolarisSam (archivo, cliente, empleado, equipo, familia, edicion, version)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version
                FROM resumen_OracleSolaris
                WHERE resumen_OracleSolaris.cliente = :cliente AND resumen_OracleSolaris.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumenAIX($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_OracleAIXSam (archivo, cliente, empleado, equipo, familia, edicion, version)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version
                FROM resumen_OracleAIX
                WHERE resumen_OracleAIX.cliente = :cliente AND resumen_OracleAIX.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumenLinux($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_OracleLinuxSam (archivo, cliente, empleado, equipo, familia, edicion, version)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version
                FROM resumen_OracleLinux
                WHERE resumen_OracleLinux.cliente = :cliente AND resumen_OracleLinux.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalleSolaris($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_OracleSolarisSam (archivo, cliente, empleado, equipo, os, versionOs, virtual, 
            memoria, cpu, versionCpu, core)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, versionOs, virtual, memoria, cpu, versionCpu, core
            FROM detalles_equipo_OracleSolaris
            WHERE detalles_equipo_OracleSolaris.cliente = :cliente AND detalles_equipo_OracleSolaris.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalleAIX($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_OracleAIXSam (archivo, cliente, empleado, equipo, os, versionOs, virtual,  
            memoria, cpu, versionCpu, core)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, versionOs, virtual, memoria, cpu, versionCpu, core
            FROM detalles_equipo_OracleAIX
            WHERE detalles_equipo_OracleAIX.cliente = :cliente AND detalles_equipo_OracleAIX.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalleLinux($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_OracleLinuxSam (archivo, cliente, empleado, equipo, os, versionOs, virtual,  
            memoria, cpu, versionCpu, core)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, versionOs, virtual, memoria, cpu, versionCpu, core
            FROM detalles_equipo_OracleLinux
            WHERE detalles_equipo_OracleLinux.cliente = :cliente AND detalles_equipo_OracleLinux.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixOracleSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function archivoArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encArchUnixOracleSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarEncSam($cliente, $empleado, $archivoDespliegue1, $archivoDespliegue2, $archivoDespliegue3, $archivoCompra){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encGrafUnixOracleSam (cliente, empleado, archivoDespliegue1, archivoDespliegue2, '
            . 'archivoDespliegue3, archivoCompra, fecha) VALUES (:cliente, :empleado, :archivoDespliegue1, :archivoDespliegue2, :archivoDespliegue3, '
            . ':archivoCompra, NOW())');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivoDespliegue1'=>$archivoDespliegue1, ':archivoDespliegue2'=>$archivoDespliegue2, 
            ':archivoDespliegue3'=>$archivoDespliegue3, ':archivoCompra'=>$archivoCompra));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encArchUnixOracleSam (cliente) VALUES (:cliente, :empleado)');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function eliminarEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafUnixOracleSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarCustomEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixOracleSam SET custom = NULL, fechaCustom = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarOtrosEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixOracleSam SET otros = NULL, fechaOtros = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarCustomEncabSam($cliente, $empleado, $custom){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixOracleSam SET custom = :custom, fechaCustom = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':custom'=>$custom));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarOtrosEncabSam($cliente, $empleado, $otros){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixOracleSam SET otros = :otros, fechaOtros = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado,':otros'=>$otros));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    /*inicio metodos nueva forma de controlar el repositorio*/
    function cantRepositorios($cliente, $empleado){
        $result = 0;
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM encGrafUnixOracleSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            if($row["cantidad"] !== ""){
                $result = $row["cantidad"];
            }
            return $result;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerEncSamArchivoElim($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixOracleSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha ASC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function eliminarEncSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafUnixOracleSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSamArchivo($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixOracleSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha DESC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function encSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixOracleSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    /*fin metodos nueva forma de controlar el repositorio*/
}