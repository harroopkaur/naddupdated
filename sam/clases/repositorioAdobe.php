<?php
class repoDespliegueAdobe extends General{
    
    function agregarBalance($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_adobeSam (archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo
                FROM `balance_adobe`
                WHERE balance_adobe.cliente = :cliente AND balance_adobe.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumen($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_adobeSam (archivo, cliente, empleado, equipo, familia, edicion, version, fecha_instalacion)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version, fecha_instalacion
                FROM resumen_adobe
                WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalle($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_adobeSam (archivo, cliente, empleado, equipo, os, familia, edicion, 
            version, dias1, dias2, dias3, minimo, activo, tipo, rango, errors)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2, dias3, minimo, activo, tipo, 
            rango, errors
            FROM detalles_equipo_adobe
            WHERE detalles_equipo_adobe.cliente = :cliente AND detalles_equipo_adobe.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafAdobeSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function archivoArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encArchAdobeSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarEncSam($cliente, $empleado, $archivoDespliegue1, $archivoDespliegue2, $archivoCompra){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encGrafAdobeSam (cliente, empleado, archivoDespliegue1, archivoDespliegue2, '
            . 'archivoCompra, fecha) VALUES (:cliente, :empleado, :archivoDespliegue1, :archivoDespliegue2, :archivoCompra, NOW())');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivoDespliegue1'=>$archivoDespliegue1, ':archivoDespliegue2'=>$archivoDespliegue2, 
            ':archivoCompra'=>$archivoCompra));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encArchAdobeSam (cliente, empleado) VALUES (:cliente, :empleado)');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function eliminarEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafAdobeSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarCustomEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchAdobeSam SET custom = NULL, fechaCustom = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarOtrosEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchAdobeSam SET otros = NULL, fechaOtros = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarCustomEncabSam($cliente, $empleado, $custom){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchAdobeSam SET custom = :custom, fechaCustom = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':custom'=>$custom));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarOtrosEncabSam($cliente, $empleado, $otros){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchAdobeSam SET otros = :otros, fechaOtros = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':otros'=>$otros));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    /*inicio metodos nueva forma de controlar el repositorio*/
    function cantRepositorios($cliente, $empleado){
        $result = 0;
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM encGrafAdobeSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            if($row["cantidad"] !== ""){
                $result = $row["cantidad"];
            }
            return $result;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerEncSamArchivoElim($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafAdobeSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha ASC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function eliminarEncSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafAdobeSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSamArchivo($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafAdobeSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha DESC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function encSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafAdobeSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    /*fin metodos nueva forma de controlar el repositorio*/
}