<?php
class accesoSam extends General{
    public $error = "ERROR: No se pudo realizar la consulta";

    function datosAcceso($cliente) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT login, pass, carpeta FROM politSam WHERE idCliente = :cliente');
            $sql->execute(array('cliente' => $cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return $error;
        }
    }

    function validarAccesoCarpetas($login, $pass, $carpeta) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT politSam.idCliente,
					clientes.nombre,
					clientes.apellido,
					clientes.empresa
				FROM politSam 
					INNER JOIN clientes ON politSam.idCliente = clientes.id
				WHERE login = :login AND pass = SHA2(:pass, 224) AND carpeta = :carpeta');
            $sql->execute(array('login' => $login, 'pass' => $pass, 'carpeta' => $carpeta));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return $error;
        }
    }

    function validarLoginSam($login, $carpeta) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM politSam WHERE login = :login AND carpeta = :carpeta');
            $sql->execute(array('login' => $login, 'carpeta' => $carpeta));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return $error;
        }
    }

    function editarAcceso($cliente, $login, $pass, $link) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE politSam SET login = :login, pass = SHA2(:pass, 224), carpeta = :link WHERE idCliente = :cliente');
            $sql->execute(array('cliente' => $cliente, 'login' => $login, 'pass' => $pass, 'link' => $link));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function editarAcceso1($cliente, $login, $link) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE politSam SET login = :login, carpeta = :link WHERE idCliente = :cliente');
            $sql->execute(array('cliente' => $cliente, 'login' => $login, 'link' => $link));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function guardarAcceso($cliente, $login, $pass, $link) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO politSam (login, pass, carpeta, idCliente) VALUES (:login, SHA2(:pass, 224), :link, :cliente)');
            $sql->execute(array('cliente' => $cliente, 'login' => $login, 'pass' => $pass, 'link' => $link));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function validarLogin($login, $cliente) {
        $resultado = 1;
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE correo = :login');
            $sql->execute(array('login' => $login));
            $result = $sql->fetchAll();
            if (count($result) > 0) {
                return -2;
            }

            $sql = $this->conn->prepare('SELECT * FROM politSam WHERE login = :login AND idCliente != :cliente');
            $sql->execute(array('login' => $login, 'cliente' => $cliente));
            $result = $sql->fetchAll();
            if (count($result) > 0) {
                return -2;
            }
            return $resultado;
        } catch (PDOException $e) {
            return -1;
        }
    }
}	