<?php
class repoDespliegueSPLA extends General{
    
    function agregarBalance($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_SPLASam (archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo
                FROM `balance_SPLA`
                WHERE balance_SPLA.cliente = :cliente AND balance_SPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumen($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_SPLASam (archivo, cliente, empleado, equipo, familia, edicion, version, fecha_instalacion)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version, fecha_instalacion
                FROM resumen_SPLA
                WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalle($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_SPLASam (archivo, cliente, empleado, equipo, os, familia, edicion, 
            version, dias1, dias2, dias3, minimo, activo, tipo, rango, errors)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2, dias3, minimo, activo, tipo, 
            rango, errors
            FROM detalles_equipo_SPLA
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarWindowServerClienteSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO windowServerClientesSPLASam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM windowServerClientes_SPLA
                WHERE windowServerClientes_SPLA.idCliente = :cliente AND windowServerClientes_SPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionWindowServerSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionWindowsServidoresSPLASam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionWindowsServidoresSPLA
                WHERE alineacionWindowsServidoresSPLA.idCliente = :cliente AND alineacionWindowsServidoresSPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarSqlServerClienteSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO sqlServerClientesSPLA_Sam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM sqlServerClientes_SPLA
                WHERE sqlServerClientes_SPLA.idCliente = :cliente AND sqlServerClientes_SPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionSqlServerSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionSqlServidoresSPLASam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionSqlServidoresSPLA
                WHERE alineacionSqlServidoresSPLA.idCliente = :cliente AND alineacionSqlServidoresSPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSPLASam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarEncSam($cliente, $empleado, $archivoDespliegue1, $archivoDespliegue2, $archivoCompra){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encGrafSPLASam (cliente, empleado, archivoDespliegue1, archivoDespliegue2, '
            . 'archivoCompra, fecha) VALUES (:cliente, :empleado, :archivoDespliegue1, :archivoDespliegue2, :archivoCompra, NOW())');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivoDespliegue1'=>$archivoDespliegue1, ':archivoDespliegue2'=>$archivoDespliegue2, 
             ':archivoCompra'=>$archivoCompra));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function eliminarEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafSPLASam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    /*inicio metodos nueva forma de controlar el repositorio*/    
    function cantRepositorios($cliente, $empleado){
        $result = 0;
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM encGrafSPLASam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            if($row["cantidad"] !== ""){
                $result = $row["cantidad"];
            }
            return $result;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerEncSamArchivoElim($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSPLASam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha ASC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function eliminarEncSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafSPLASam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSamArchivo($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSPLASam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha DESC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function encSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSPLASam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarWindowServerClienteSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO windowServerClientesSPLASam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM windowServerClientes_SPLA
                WHERE windowServerClientes_SPLA.idCliente = :cliente AND windowServerClientes_SPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionWindowServerSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionWindowsServidoresSPLASam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionWindowsServidoresSPLA
                WHERE alineacionWindowsServidoresSPLA.idCliente = :cliente AND alineacionWindowsServidoresSPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarSqlServerClienteSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO sqlServerClientesSPLA_Sam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM sqlServerClientes_SPLA
                WHERE sqlServerClientes_SPLA.idCliente = :cliente AND sqlServerClientes_SPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionSqlServerSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionSqlServidoresSPLASam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionSqlServidoresSPLA
                WHERE alineacionSqlServidoresSPLA.idCliente = :cliente AND alineacionSqlServidoresSPLA.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    /*fin metodos nueva forma de controlar el repositorio*/
}	