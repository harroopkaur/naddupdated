<?php
class repoDespliegue extends General{
    public  $error;
   
    function agregarBalance($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_office2Sam (archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo, tipoCompra, cantidadGAP, totalGAP, balanceGAP)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo, tipoCompra, cantidadGAP, totalGAP, balanceGAP
                FROM `balance_office2`
                WHERE balance_office2.cliente = :cliente AND balance_office2.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumen($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_office2Sam (archivo, cliente, empleado, equipo, familia, edicion, version, fecha_instalacion)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version, fecha_instalacion
                FROM resumen_office2
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalle($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo2Sam (archivo, cliente, empleado, equipo, os, familia, edicion, 
            version, dias1, dias2, dias3, minimo, activo, tipo, rango, errors)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2, dias3, minimo, activo, tipo, 
            rango, errors
            FROM detalles_equipo2
            WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarWindowServerClienteSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO windowServerClientesSam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM windowServerClientes
                WHERE windowServerClientes.idCliente = :cliente AND windowServerClientes.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionWindowServerSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionWindowsServidoresSam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionWindowsServidores
                WHERE alineacionWindowsServidores.idCliente = :cliente AND alineacionWindowsServidores.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarSqlServerClienteSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO sqlServerClientesSam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM sqlServerClientes
                WHERE sqlServerClientes.idCliente = :cliente AND sqlServerClientes.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionSqlServerSam($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionSqlServidoresSam (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionSqlServidores
                WHERE alineacionSqlServidores.idCliente = :cliente AND alineacionSqlServidores.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function archivoArchEncSam($cliente, $empleado, $fabricante){
        try {
            $this->conexion();
            $tabla = "";
            if($fabricante == 1){
                $tabla = "encArchAdobeSam";
            }
            else if($fabricante == 2){
                $tabla = "encArchIbmSam";
            }
            else if($fabricante == 3){
                $tabla = "encArchSam";
            }
            else if($fabricante == 4){
                $tabla = "encArchOracleSam";
            }
            else if($fabricante == 5){
                $tabla = "encArchSapSam";
            }
            else if($fabricante == 6){
                $tabla = "encArchVMWareSam";
            }
            else if($fabricante == 7){
                $tabla = "encArchUnixIBMSam";
            }
            else if($fabricante == 8){
                $tabla = "encArchUnixOracleSam";
            }
            else if($fabricante == 10){
                $tabla = "encArchSPLASam";
            }
            $sql = $this->conn->prepare('SELECT * FROM ' . $tabla . ' WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarEncSam($cliente, $empleado, $archivoDespliegue1, $archivoDespliegue2, $archivoCompra){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encGrafSam (cliente, empleado, archivoDespliegue1, archivoDespliegue2, '
            . 'archivoCompra, fecha) VALUES (:cliente, :empleado, :archivoDespliegue1, :archivoDespliegue2, :archivoCompra, NOW())');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivoDespliegue1'=>$archivoDespliegue1, ':archivoDespliegue2'=>$archivoDespliegue2, 
            ':archivoCompra'=>$archivoCompra));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarArchEncSam($cliente, $empleado, $fabricante){
        try {
            $this->conexion();
            $tabla = "";
            if($fabricante == 1){
                $tabla = "encArchAdobeSam";
            }
            else if($fabricante == 2){
                $tabla = "encArchIbmSam";
            }
            else if($fabricante == 3){
                $tabla = "encArchSam";
            }
            else if($fabricante == 4){
                $tabla = "encArchOracleSam";
            }
            else if($fabricante == 5){
                $tabla = "encArchSapSam";
            }
            else if($fabricante == 6){
                $tabla = "encArchVMWareSam";
            }
            else if($fabricante == 7){
                $tabla = "encArchUnixIBMSam";
            }
            else if($fabricante == 8){
                $tabla = "encArchUnixOracleSam";
            }
            else if($fabricante == 10){
                $tabla = "encArchSPLASam";
            }
            $sql = $this->conn->prepare('INSERT INTO ' . $tabla . ' (cliente, empleado, fecha) VALUES (:cliente, :empleado, NOW())');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function eliminarEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    /*inicio metodos nueva forma de controlar el repositorio*/    
    function cantRepositorios($cliente, $empleado){
        $result = 0;
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM encGrafSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            if($row["cantidad"] !== ""){
                $result = $row["cantidad"];
            }
            return $result;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerEncSamArchivoElim($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha ASC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function eliminarEncSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSamArchivo($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha DESC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function encSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarWindowServerClienteSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO windowServerClientesSam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM windowServerClientes
                WHERE windowServerClientes.idCliente = :cliente AND windowServerClientes.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionWindowServerSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionWindowsServidoresSam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionWindowsServidores
                WHERE alineacionWindowsServidores.idCliente = :cliente AND alineacionWindowsServidores.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarSqlServerClienteSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO sqlServerClientesSam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM sqlServerClientes
                WHERE sqlServerClientes.idCliente = :cliente AND sqlServerClientes.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarAlineacionSqlServerSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionSqlServidoresSam (idCliente, empleado, archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore)
                SELECT idCliente, empleado, :archivo AS archivo, cluster, host, equipo, familia, edicion, version, tipo, cpu, cores, licSrv, licProc, licCore
                FROM alineacionSqlServidores
                WHERE alineacionSqlServidores.idCliente = :cliente AND alineacionSqlServidores.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDesarrolloPruebasVSSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO desarrolloPruebaVSSam (archivo, cliente, empleado, equipo, tipo, familia, edicion, version, fechaInstalacion, usuario, equipoUsuario)
                SELECT :archivo AS archivo, cliente, empleado, equipo, tipo, familia, edicion, version, fechaInstalacion, usuario, equipoUsuario
                FROM desarrolloPruebaVS
                WHERE desarrolloPruebaVS.cliente = :cliente AND desarrolloPruebaVS.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDesarrolloPruebasMSDNSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO desarrolloPruebaMSDNSam (archivo, cliente, empleado, equipo, tipo, familia, edicion, version, fechaInstalacion, usuario, equipoUsuario, msdn)
                SELECT :archivo AS archivo, cliente, empleado, equipo, tipo, familia, edicion, version, fechaInstalacion, usuario, equipoUsuario, msdn
                FROM desarrolloPruebaMSDN
                WHERE desarrolloPruebaMSDN.cliente = :cliente AND desarrolloPruebaMSDN.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarEscaneoSamArchivo($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO escaneo_equipos2Sam (archivo, cliente, empleado, equipo, status, errors)
                SELECT :archivo AS archivo, cliente, empleado, equipo, status, errors
                FROM escaneo_equipos2
                WHERE escaneo_equipos2.cliente = :cliente AND escaneo_equipos2.empleado = :empleado AND errors != "Ninguno"');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    /*fin metodos nueva forma de controlar el repositorio*/
    
    function limpiarCustomEncabSam($cliente, $empleado, $fabricante, $posicion = ""){
        try {
            $this->conexion();
            $tabla = "";
            if($fabricante == 1){
                $tabla = "encArchAdobeSam";
            }
            else if($fabricante == 2){
                $tabla = "encArchIbmSam";
            }
            else if($fabricante == 3){
                $tabla = "encArchSam";
            }
            else if($fabricante == 4){
                $tabla = "encArchOracleSam";
            }
            else if($fabricante == 5){
                $tabla = "encArchSapSam";
            }
            else if($fabricante == 6){
                $tabla = "encArchVMWareSam";
            }
            else if($fabricante == 7){
                $tabla = "encArchUnixIBMSam";
            }
            else if($fabricante == 8){
                $tabla = "encArchUnixOracleSam";
            }
            else if($fabricante == 10){
                $tabla = "encArchSPLASam";
            }
            $sql = $this->conn->prepare('UPDATE ' . $tabla . ' SET custom' . $posicion . ' = NULL, fechaCustom' . $posicion . ' = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarOtrosEncabSam($cliente, $empleado, $fabricante, $posicion = ""){
        try {
            $this->conexion();
            $tabla = "";
            if($fabricante == 1){
                $tabla = "encArchAdobeSam";
            }
            else if($fabricante == 2){
                $tabla = "encArchIbmSam";
            }
            else if($fabricante == 3){
                $tabla = "encArchSam";
            }
            else if($fabricante == 4){
                $tabla = "encArchOracleSam";
            }
            else if($fabricante == 5){
                $tabla = "encArchSapSam";
            }
            else if($fabricante == 6){
                $tabla = "encArchVMWareSam";
            }
            else if($fabricante == 7){
                $tabla = "encArchUnixIBMSam";
            }
            else if($fabricante == 8){
                $tabla = "encArchUnixOracleSam";
            }
            else if($fabricante == 10){
                $tabla = "encArchSPLASam";
            }
            $sql = $this->conn->prepare('UPDATE ' . $tabla . ' SET otros' . $posicion . ' = NULL, fechaOtros' . $posicion . ' = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarCustomEncabSam($cliente, $empleado, $custom, $fabricante, $posicion = ""){
        try {
            $this->conexion();
            $tabla = "";
            if($fabricante == 1){
                $tabla = "encArchAdobeSam";
            }
            else if($fabricante == 2){
                $tabla = "encArchIbmSam";
            }
            else if($fabricante == 3){
                $tabla = "encArchSam";
            }
            else if($fabricante == 4){
                $tabla = "encArchOracleSam";
            }
            else if($fabricante == 5){
                $tabla = "encArchSapSam";
            }
            else if($fabricante == 6){
                $tabla = "encArchVMWareSam";
            }
            else if($fabricante == 7){
                $tabla = "encArchUnixIBMSam";
            }
            else if($fabricante == 8){
                $tabla = "encArchUnixOracleSam";
            }
            else if($fabricante == 10){
                $tabla = "encArchSPLASam";
            }
            $sql = $this->conn->prepare('UPDATE ' . $tabla . ' SET custom' . $posicion . ' = :custom, fechaCustom' . $posicion . ' = NOW() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado , ':custom'=>$custom));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarOtrosEncabSam($cliente, $empleado, $otros, $fabricante, $posicion = ""){
        try {
            $this->conexion();
            $tabla = "";
            if($fabricante == 1){
                $tabla = "encArchAdobeSam";
            }
            else if($fabricante == 2){
                $tabla = "encArchIbmSam";
            }
            else if($fabricante == 3){
                $tabla = "encArchSam";
            }
            else if($fabricante == 4){
                $tabla = "encArchOracleSam";
            }
            else if($fabricante == 5){
                $tabla = "encArchSapSam";
            }
            else if($fabricante == 6){
                $tabla = "encArchVMWareSam";
            }
            else if($fabricante == 7){
                $tabla = "encArchUnixIBMSam";
            }
            else if($fabricante == 8){
                $tabla = "encArchUnixOracleSam";
            }
            else if($fabricante == 10){
                $tabla = "encArchSPLASam";
            }
            $sql = $this->conn->prepare('UPDATE ' . $tabla . ' SET otros' . $posicion . ' = :otros, fechaOtros' . $posicion . ' = NOW() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, 'otros'=>$otros));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
}