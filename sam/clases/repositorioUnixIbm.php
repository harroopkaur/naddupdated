<?php
class repoDespliegueUnixIbm extends General{
   
    function agregarBalance($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_UnixIBMSam (archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo
                FROM `balance_unixIBM`
                WHERE balance_unixIBM.cliente = :cliente AND balance_unixIBM.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumenSolaris($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_UnixSolarisSam (archivo, cliente, empleado, equipo, familia, edicion, version)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version
                FROM resumen_UnixSolaris
                WHERE resumen_UnixSolaris.cliente = :cliente AND resumen_UnixSolaris.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumenAIX($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_UnixAIXSam (archivo, cliente, empleado, equipo, familia, edicion, version)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version
                FROM resumen_UnixAIX
                WHERE resumen_UnixAIX.cliente = :cliente AND resumen_UnixAIX.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarResumenLinux($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_UnixLinuxSam (archivo, cliente, empleado, equipo, familia, edicion, version)
                SELECT ' . $archivo . ' AS archivo, cliente, empleado, equipo, familia, edicion, version
                FROM resumen_UnixLinux
                WHERE resumen_UnixLinux.cliente = :cliente AND resumen_UnixLinux.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalleSolaris($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_UnixSolarisSam (archivo, cliente, empleado, equipo, os, versionOs, virtual, 
            memoria, cpu, versionCpu, core)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, versionOs, virtual, memoria, cpu, versionCpu, core
            FROM detalles_equipo_UnixSolaris
            WHERE detalles_equipo_UnixSolaris.cliente = :cliente AND detalles_equipo_UnixSolaris.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalleAIX($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_UnixAIXSam (archivo, cliente, empleado, equipo, os, versionOs, virtual,  
            memoria, cpu, versionCpu, core)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, versionOs, virtual, memoria, cpu, versionCpu, core
            FROM detalles_equipo_UnixAIX
            WHERE detalles_equipo_UnixAIX.cliente = :cliente AND detalles_equipo_UnixAIX.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function agregarDetalleLinux($cliente, $empleado, $archivo) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_UnixLinuxSam (archivo, cliente, empleado, equipo, os, versionOs, virtual,  
            memoria, cpu, versionCpu, core)
            SELECT '. $archivo . ' AS archivo, cliente, empleado, equipo, os, versionOs, virtual, memoria, cpu, versionCpu, core
            FROM detalles_equipo_UnixLinux
            WHERE detalles_equipo_UnixLinux.cliente = :cliente AND detalles_equipo_UnixLinux.empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixIBMSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function archivoArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encArchUnixIBMSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarEncSam($cliente, $empleado, $archivoDespliegue1, $archivoDespliegue2, $archivoDespliegue3, $archivoCompra){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encGrafUnixIBMSam (cliente, empleado, archivoDespliegue1, archivoDespliegue2, '
            . 'archivoDespliegue3, archivoCompra, fecha) VALUES (:cliente, :empleado, :archivoDespliegue1, :archivoDespliegue2, '
            . ':archivoDespliegue3, :archivoCompra, NOW())');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':archivoDespliegue1'=>$archivoDespliegue1, ':archivoDespliegue2'=>$archivoDespliegue2, 
            ':archivoDespliegue3'=>$archivoDespliegue3, ':archivoCompra'=>$archivoCompra));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function agregarArchEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO encArchUnixIBMSam (cliente, empleado) VALUES (:cliente, :empleado)');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function eliminarEncSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafUnixIBMSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarCustomEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixIBMSam SET custom = NULL, fechaCustom = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function limpiarOtrosEncabSam($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixIBMSam SET otros = NULL, fechaOtros = NULL WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarCustomEncabSam($cliente, $empleado, $custom){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixIBMSam SET custom = :custom, fechaCustom = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':custom'=>$custom));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actualizarOtrosEncabSam($cliente, $empleado, $otros){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE encArchUnixIBMSam SET otros = :otros, fechaOtros = CURDATE() WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado, ':otros'=>$otros));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    /*inicio metodos nueva forma de controlar el repositorio*/    
    function cantRepositorios($cliente, $empleado){
        $result = 0;
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM encGrafUnixIBMSam WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            if($row["cantidad"] !== ""){
                $result = $row["cantidad"];
            }
            return $result;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerEncSamArchivoElim($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixIBMSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha ASC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function eliminarEncSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM encGrafUnixIBMSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function archivoEncSamArchivo($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixIBMSam WHERE cliente = :cliente AND empleado = :empleado ORDER BY fecha DESC LIMIT 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    
    function encSamArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM encGrafUnixIBMSam WHERE archivo = :archivo');
            $sql->execute(array(':archivo' => $archivo));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            return false;
        } 
    }
    /*fin metodos nueva forma de controlar el repositorio*/
}