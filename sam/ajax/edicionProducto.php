<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $fabricante = 0;
                if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                    $fabricante = $_POST["fabricante"];
                }
                
                $producto = 0;
                if(isset($_POST["producto"]) && filter_var($_POST["producto"], FILTER_VALIDATE_INT) !== false){
                    $producto = $_POST["producto"];
                }
                
                $nueva_edicion = new funcionesSam($conn);

                if($fabricante == 10){
                    $fabricante = 3;
                }
                $edicion = $nueva_edicion->obtenerEdicProducto($fabricante, $producto);

                $option = '<option value = "">--Seleccione--</option>';
                foreach ($edicion as $row) {
                        //$option .=  '<option value="' . $row["idEdicion"] . '">' . utf8_decode($row["nombre"]) . '</option>';
                        $option .=  '<option value="' . $row["idEdicion"] . '">' . $row["nombre"] . '</option>';
                }

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'option'=>$option, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);