<?php 
$pruebas = 3;
if($pruebas == 1){
    require_once($_SERVER['DOCUMENT_ROOT'] . "/licensingassurance.com/webtool/configuracion/variables.php");
}
else if($pruebas == 2){
    require_once($_SERVER['DOCUMENT_ROOT'] . "/webtoolTest/configuracion/variables.php");
}
else if($pruebas == 3){
    require_once($_SERVER['DOCUMENT_ROOT'] . "/webtool/configuracion/variables.php");
}

require_once($GLOBALS["app_root"] . "/configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/politicas.php");
$url = $GLOBALS["domain_root"] . "/sam/accesoCarpetasSam.php";
if(isset($_SESSION["carpeta"])){
	$pos = strpos($_SERVER['REQUEST_URI'], $_SESSION["carpeta"]);
	if ($pos === false) {
		session_unset();                 // Vacia las variables de sesion
		session_destroy();               // Destruye la sesion
		header("location: index.php");
	}	
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<style>
			body {
				background:url(<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/bg_body.jpg) top left no-repeat !important;	
			}
		</style>
		<title>.:Licensing Assurance:.</title>
		<!-- Custom Theme files -->
                <link href="<?= $GLOBALS["domain_root"] ?>/css/style3.css" rel="stylesheet" type="text/css" media="all"/>
                <link rel="stylesheet" href="<?= $GLOBALS["domain_root"] ?>/css/exampleSam.css" type="text/css" />
		<!-- Custom Theme files -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta name="keywords" content="trial, sofware" />
		<!--Google Fonts-->
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<!--Google Fonts-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	</head>
	<body>
	<?php
	if(!isset($_SESSION["usuLogueado"]) || $_SESSION["usuLogueado"] == false){	
	?>
		<br><br><br><br>
		<!--sign up form start here-->
		<div style="width:100%; position:relative; text-align:center;">
			<div class="app" style=" display:inline-block; overflow:hidden;">
				<br>
				<h1 style="font-size:24px;color:#fff;">Bienvenido al Servicio de Manejo de Licenciamiento en la nube </h1>
				<br>
				<div style="overflow:hidden; padding:20px;">
                                    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/logo2.png"  style="margin:0 auto; display:block;">
				</div>
				<div style="width:100%; height:150px; min-height:150px;">&nbsp;</div>
			</div>
			
			<div class="app" style=" display:inline-block; margin-left:25px; overflow:hidden;   padding-top:30px;padding-bottom: 30px;">
				<div style="overflow:hidden; width:60%; margin:0 auto; background:#F7F7F7; padding:20px; border-radius:10px;">
					
					<h1 style="font-size:24px;color:#000; text-align:left;">Inicio de Sesi&oacute;n</h1> <br><br><br><br>
					
					<form id="form1" name="form1" method="post">
						<input type="text" name="login" id="login"  placeholder="Usuario" />
						<input type="password" name="contrasena" id="contrasena"  placeholder="Clave" />
						<input type="submit" id="entrar" value="Entrar" />
					</form>
					
					<div align="center" class="error_prog" id="errorLogin" style="display:none;">Login inv&aacute;lido</div>
					
					<div align="center" class="error_prog" id="errorPass" style="display:none;">Contrase&ntilde;a inv&aacute;lida</div>
					<br><br><br><br>
				</div>
				<div style="width:100%; height:20px; min-height:20px; clear:both;">&nbsp;</div>
			</div>
		</div>
		<div class="copyright" >
			<p>Todos Los Derechos Reservados 2016 - <a href="https://www.licensingassurance.com"> www.licensingassurance.com </a></p>
		</div>
		<!--sign up form end here-->
	<?php 
	}
	else{
		//sign up form start here                
		$archivo1   = "";
		$archivo2   = "";
		$archivo3   = "";
		$archivo4   = "";
		$archivo5   = "";
		$archivo6   = "";
		$archivo7   = "";
		$archivo8   = "";
		$archivo9   = "";
		$archivo10  = "";
		$archivo11  = "";
		$archivo12  = "";
		
		$politica = new politicasSam($conn); 
		
		$tablaPoliticas = $politica->politicas($_SESSION["idClienteSam"]);
		
		foreach($tablaPoliticas as $row){
			$archivo1   = $row["adquisicion"];
			$archivo2   = $row["identificacion"];
			$archivo3   = $row["conformidad"];
			$archivo4   = $row["comunicacion"];
			$archivo5   = $row["disposicion"];
			$archivo6   = $row["documentacion"];
			$archivo7   = $row["financiera"];
			$archivo8   = $row["legislacion"];
			$archivo9   = $row["politicas"];
			$archivo10  = $row["programas"];
			$archivo11  = $row["proyectos"];
			$archivo12  = $row["proveedores"];
		}
		?>
		
		<input type="hidden" id="archivo1" value="<?= $archivo1 ?>">
		<input type="hidden" id="archivo2" value="<?= $archivo2 ?>">
		<input type="hidden" id="archivo3" value="<?= $archivo3 ?>">
		<input type="hidden" id="archivo4" value="<?= $archivo4 ?>">
		<input type="hidden" id="archivo5" value="<?= $archivo5 ?>">
		<input type="hidden" id="archivo6" value="<?= $archivo6 ?>">
		<input type="hidden" id="archivo7" value="<?= $archivo7 ?>">
		<input type="hidden" id="archivo8" value="<?= $archivo8 ?>">
		<input type="hidden" id="archivo9" value="<?= $archivo9 ?>">
		<input type="hidden" id="archivo10" value="<?= $archivo10 ?>">
		<input type="hidden" id="archivo11" value="<?= $archivo11 ?>">
		<input type="hidden" id="archivo12" value="<?= $archivo12 ?>">
		<header id="cabecera" >
			<img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/logo3.png" style=" width:300px;float:left; margin:10px;">
			<div style="float:left; margin:25px;">
				<div style="font-size:20px; color:#fff; font-weight:bold; margin-left:20px; margin-right:10px; float:left;">Efectivo</div>
				<div style="font-size:20px; color:#fff; font-weight:bold; margin-left:10px; margin-right:10px; float:left;">Bajo costo</div>
				<div style="font-size:20px; color:#fff; font-weight:bold; margin-left:10px; margin-right:10px; float:left;">Conveniente</div>
			</div>
			
			<div style="float:right; margin:25px; cursor:pointer; margin-left:0px; margin-top:23px;" onClick="showhide('verop');">
				<img src="<?=$GLOBALS['domain_root']?>/imagenes/inicio/flecha.png">
			</div>
			
			<div style="float:right; margin:20px; color:#fff; margin-right:5px; text-transform:uppercase;  font-weight:bold;" >
				<?=$_SESSION['nombreSam'].' '.$_SESSION['apellidoSam'].' & '.$_SESSION['empresaSam']?>
			</div>
			
			<div id="verop" style=" display:none;position:absolute; right:20px; top:50px; width:200px; background:#AAA; border-radius:4px; padding:10px; overflow:hidden; z-index:2;">
				<div style="margin:5px;">
					<a href="salidaSAM.php">Cerrar Sesi&oacute;n</a>
				</div>
			</div>
		</header>
		
		<section id="contenedor">
			<div style="overflow:hidden; margin:0 auto; margin:20px; margin-right:10px; width:95%;">
				<h1 class="blanco textog">Panel de Control</h1>
				<div style="float:left; width:15%;">
					<!--inicio modificado-->
					<div id="panelControl">
						<p class="blanco">1.-Selecciona Etapa del Ciclo de Vida</p>
						<p class="blanco">2.-Nivel de Servicio</p>
						<p class="blanco">3.-Completar los pasos</p>
					</div>
					<!--fin modificado-->
					
					<br><br>
				</div>
				
				<div style="width:85%; margin:0; padding:0px; float:left;">
					
					<div id="contenedor_ver1" style="float:left; width:120px; overflow:hidden;">
						<br><br><br><br><br>
						
						<div class="botones_m3 activado1">
							Accesos SAM
						</div>
					</div>
					
					<div style="float:left; width:85%; margin:0; padding:0px; overflow:hidden;  min-height:500px;">
						
						<div id="contenedor_ver2" style="width:100%; margin:0px; padding:0px; overflow:hidden; height:42px;">
							<div class="botones_m3 activado1">General</div>
						</div>
						
						<div id="contenedor_ver3" style=" display:block;width:100%; margin:0px; padding:0px;background:#D9D9D9; min-height:500px; overflow:hidden;">
							<div id="contenedor_ver4" style=" margin:0 auto; margin-top:20px; margin-bottom:20px; border-radius:8px; background:#F2F2F2; overflow:hidden;min-height:500px; width:95%;">
								
								<div style="width:calc(100%-50px); padding:10px 30px 10px 30px; overflow:hidden;" id="contenedorCentral">
									<div style="width:100%; height:auto; overflow:hidden;">
										<div style="width:calc(100%-10px); height:50px; border:3px solid; text-align:center; line-height:50px; font-size:24px; font-weight:bold; margin-bottom:10px; background-color:#428CC4;">
											Pol&iacute;ticas SAM
										</div>
										
										<table style="width:100%;">
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">1</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Adquisici&oacute;n</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Para comprar y adquirir el software necesario</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(1)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo1 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">2</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Identificaci&oacute;n</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Proceso para monitorear despliegue de instalaciones</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(2)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo2 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">3</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Conformidad</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Controlar con certeza su licenciamiento de software</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(3)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo3 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">4</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Comunicaci&oacute;n</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo31">La manera m&aacute;s efectivas de comunicar los procesos y pol&iacute;ticas de licenciamiento</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(4)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo4 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">5</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Disposici&oacute;n</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">La mejor manera de desechar los activos de software</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(5)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo5 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">6</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Documentaci&oacute;n</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Pol&iacute;ticas y procesos escritos</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(6)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo6 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">7</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Financiera</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Requisitos para el &aacute;rea de finanzas</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(7)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo7 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">8</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Legislaci&oacute;n</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo31">Pol&iacute;ticas y procesos para la administraci&oacute;n del licenciamiento</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(8)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo8 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">9</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Pol&iacute;tica</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Procesos establecidos para el licenciamiento</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(9)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo9 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">10</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Programas</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Proceso para mejorar los programas de licenciamiento</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(10)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo10 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">11</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Proyectos</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo3">Procesos y pol&iacute;ticas para el manejo de los proyectos</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(11)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo11 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
											
											<tr style="height:60px;">
												<td class="col1">
													<div class="div1">
														<p class="estilo1">12</p>
													</div>
												</td>
												<td class="col2">
													<div class="div2">
														<div class="alinearCirculo">
															<div class="circulo"></div>
														</div>
														<p class="estilo2">Proveedores</p>
													</div>
												</td>
												<td class="col3">
													<div class="div3">
														<p class="estilo31">Procesos y pol&iacute;ticas en manejo efectivo de los proveedores</p>
													</div>
												</td>
												<td class="col4">
													<div class="div4" onclick="abrirArchivo(12)">
														<p class="estilo4 activado1" style="cursor:<?php if($archivo12 != "") echo "pointer"; else echo "default"; ?>">Pol&iacute;tica/Formulario</p>
													</div>
												</td>
											</tr>
										</table>										
									</div>
								</div>
								
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</section>
	<?php
		require_once($GLOBALS["app_root"] . "/plantillas/pie.php");
	}
	?>
	</body>
</html>
<script>
	$(document).ready(function(){
		$("#entrar").click(function(e){
			e.preventDefault();
			$("#errorLogin").hide();
			$("#errorPass").hide();
			link = window.location + "";
			cadena = link.split("/");
			carpeta = cadena[cadena.length-2];
			$.post("<?= $url ?>", { login : $("#login").val(), pass : $("#contrasena").val(), carpeta : carpeta}, function(data){
				if(data[0].result == 0){
					$("#errorPass").show();
				}
				else if(data[0].result == 1){
					location.href = 'index.php';
				}
				else{
					$("#errorLogin").show();
				}
			}, "json")
			.fail(function(){
				alert("Hubo un error");
			});
		});
	});
	
	function showhide(what) {
		var seccion= document.getElementById(what);
		
		if (seccion.style.display=='none') {
			  seccion.style.display='block';		
		} else {
			seccion.style.display='none';
		}
	}
	
	function abrirArchivo(indice){
		if($("#archivo"+indice).val() != ""){
			window.open($("#archivo"+indice).val(), "_blank");
		}
	}
</script>