<?php
require_once("../configuracion/inicio.php");
require_once("../plantillas/sesion.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");

//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
include_once("../plantillas/middleware.php");
//fin middleware

$nuevo_contrato = new funcionesSam();
$array = "";

	$idFabricante    = "";
	$numero          = "";
	$tipo            = "";
	$fechaEfectiva   = "";
	$fechaExpiracion = "";
	$fechaProxima    = "";
	$comentarios     = "";
	$opcion          = "";
	
	if(isset($_POST["fabricante"])){
		$idFabricante = $_POST["fabricante"];
	}
	
	if(isset($_POST["numContrato"])){
		$numero = $_POST["numContrato"];
	}

	if(isset($_POST["tipoContrato"])){
		$tipo = $_POST["tipoContrato"];
	}
	
	if(isset($_POST["fechaEfectiva"])){
		$fechaEfectiva = $_POST["fechaEfectiva"];
	}
	
	if(isset($_POST["fechaExpiracion"])){
		$fechaExpiracion = $_POST["fechaExpiracion"];
	}
	
	if(isset($_POST["fechaProxima"])){
		$fechaProxima    = $_POST["fechaProxima"];
	}
	
	if(isset($_POST["comentario"])){
		$comentarios = $_POST["comentario"];
	}
	
	if(isset($_POST["bandOpcion"])){
		$opcion = $_POST["bandOpcion"];
	}
	
	if($opcion == "agregar"){
		$archivo1    = "";
		$archivo2    = "";
		$archivo3    = "";
		$archivo4    = "";
		$archivo5    = "";
	}
	else{
		$archivo1    = "";
		$archivo2    = "";
		$archivo3    = "";
		$archivo4    = "";
		$archivo5    = "";
		
		if(isset($_POST["archivoActual1"])){
			$archivo1 = $_POST["archivoActual1"];
		}
		
		if(isset($_POST["archivoActual2"])){
			$archivo2 = $_POST["archivoActual2"];
		}
		
		if(isset($_POST["archivoActual3"])){
			$archivo3 = $_POST["archivoActual3"];
		}
		
		if(isset($_POST["archivoActual4"])){
			$archivo4    = $_POST["archivoActual4"];
		}
		
		if(isset($_POST["archivoActual5"])){
			$archivo5    = $_POST["archivoActual5"];
		}
	}

	if($opcion == "agregar"){
		$result = $nuevo_contrato->guardarContrato($_SESSION["client_id"], $_SESSION["client_empleado"], $idFabricante, $numero, $tipo, $fechaEfectiva, $fechaExpiracion, $fechaProxima, $comentarios, $archivo1, $archivo2, $archivo3, $archivo4, $archivo5);
	}
	else{
		$contrato = "";
		if(isset($_POST["contrato"])){
			$contrato = $_POST["contrato"];
		}
		
		$result = $nuevo_contrato->editarContrato($contrato, $idFabricante, $numero, $tipo, $fechaEfectiva, $fechaExpiracion, $fechaProxima, $comentarios, $archivo1, $archivo2, $archivo3, $archivo4, $archivo5);
	}
	
	if($result == 1){
		if($opcion == "agregar"){
			$contrato = $nuevo_contrato->getUltimoContrato();
		}
		
		if(isset($_FILES["fileArchivo1"]["tmp_name"]) && is_uploaded_file($_FILES["fileArchivo1"]["tmp_name"]) ){	
			if($archivo1 != "" && $opcion == "editar"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo1)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo1);
				}
			}
			$direccion1 = $contrato . "." . $_FILES["fileArchivo1"]["name"];
			$archivo1   = $direccion1;
			move_uploaded_file($_FILES['fileArchivo1']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion1); 
		}
		else{
			if(isset($_POST["bandElimArchivo1"]) && $_POST["bandElimArchivo1"] == "true"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo1)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo1);
				}
				$archivo1 = "";
			}
		}
		
		if(isset($_FILES["fileArchivo2"]["tmp_name"]) && is_uploaded_file($_FILES["fileArchivo2"]["tmp_name"]) ){	
			if($archivo2 != "" && $opcion == "editar"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo2)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo2);
				}
			}
			$direccion2 = $contrato . "." . $_FILES["fileArchivo2"]["name"];
			$archivo2   = $direccion2;
			move_uploaded_file($_FILES['fileArchivo2']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion2);
		}
		else{
			if(isset($_POST["bandElimArchivo2"]) && $_POST["bandElimArchivo2"] == "true"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo2)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo2);
				}
				$archivo2 = "";
			}
		}
		
		if(isset($_FILES["fileArchivo3"]["tmp_name"]) && is_uploaded_file($_FILES["fileArchivo3"]["tmp_name"]) ){	
			if($archivo3 != "" && $opcion == "editar"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo3)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo3);
				}
			}
			$direccion3 = $contrato . "." . $_FILES["fileArchivo3"]["name"];
			$archivo3   = $direccion3;
			move_uploaded_file($_FILES['fileArchivo3']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion3);
		}
		else{
			if(isset($_POST["bandElimArchivo3"]) && $_POST["bandElimArchivo3"] == "true"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo3)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo3);
				}
				$archivo3 = "";
			}
		}
		
		if(isset($_FILES["fileArchivo4"]["tmp_name"]) && is_uploaded_file($_FILES["fileArchivo4"]["tmp_name"]) ){	
			if($archivo4 != "" && $opcion == "editar"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo4)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo4);
				}
			}
			$direccion4 = $contrato . "." . $_FILES["fileArchivo4"]["name"];
			$archivo4   = $direccion4;
			move_uploaded_file($_FILES['fileArchivo4']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion4);
		}
		else{
			if(isset($_POST["bandElimArchivo4"]) && $_POST["bandElimArchivo4"] == "true"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo4)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo4);
				}
				$archivo4 = "";
			}
		}
		
		if(isset($_FILES["fileArchivo5"]["tmp_name"]) && is_uploaded_file($_FILES["fileArchivo5"]["tmp_name"]) ){	
			if($archivo5 != "" && $opcion == "editar"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo5)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo5);
				}
			}
			$direccion5 = $contrato . "." . $_FILES["fileArchivo5"]["name"];
			$archivo5   = $direccion5;
			move_uploaded_file($_FILES['fileArchivo5']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion5);
		}
		else{
			if(isset($_POST["bandElimArchivo5"]) && $_POST["bandElimArchivo5"] == "true"){
				if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo5)){
					unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo5);
				}
				$archivo5 = "";
			}
		}
		
		$result = $nuevo_contrato->actualizarArchivosContrato($contrato, $archivo1, $archivo2, $archivo3, $archivo4, $archivo5);
		
		if($opcion == "editar"){
			$nuevo_contrato->eliminarDetalleContrato($contrato);
		}
		
		$aux = 0;
		if(isset($_POST["totalRegistros"])){
			for($i=0; $i < $_POST["totalRegistros"]; $i++){
				if(isset($_POST["producto" . $i])){
					$idProducto = $_POST["producto" . $i]; 
					$idEdicion  = $_POST["edicion" . $i];
					$version    = $_POST["version" . $i];
					$sku        = $_POST["SKU" . $i];
					$tipo       = $_POST["tipo" . $i];
					$cantidad   = $_POST["cantidad" . $i];
					$precio     = $_POST["precio" . $i];
					$asignacion = $_POST["asignacion" . $i];
					
					$result = $nuevo_contrato->guardarDetalleContrato($contrato, $idProducto, $idEdicion, $version, $sku, $tipo, $cantidad, $precio, $asignacion);
					if($result == 1){
						$aux++;
					}
				}
				else{
					
				}
			}
		}
		
		if(isset($_POST["totalFilaTabla"]) && $aux == $_POST["totalFilaTabla"]){
			$result = 1;
		}
		else if($aux == 0){
			$result = 2;
		}
		else{
			$result = 3;
		}
		
		if($result == 0){
			echo "<script>alert('No se guardó el contrato');
				window.close();
			</script>";
		}
		else if($result == 1){
			echo "<script>alert('Se guardó el contrato con éxito');
				window.close();
			</script>";
		}
		else if($result == 2){
			echo "<script>alert('No se guardó el detalle del contrato');
				window.close();
			</script>";
		}
		else{
			echo "<script>alert('Se guardó parcialmente el detalle del contrato');
				window.close();
			</script>";
		}
		
	}
?>