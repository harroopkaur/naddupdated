<?php
require_once("../configuracion/inicio.php");
require_once("../clases/clase_general.php");
require_once("clases/repositorio.php");

//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>'false', 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = 'true';
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $result  = 0;
            $fecha   = "";
            $archivo = "";
            $repositorio = new repoDespliegue();
           
            $idFab = 0;
            if(isset($_POST["idFab"]) && filter_var($_POST["idFab"], FILTER_VALIDATE_INT) !== false){
                $idFab = $_POST["idFab"];
            }

            if($idFab == 1){
                $carpeta = "adobe";
            }
            else if($idFab == 2){
                $carpeta = "ibm";
            }
            else if($idFab == 3){
                $carpeta = "microsoft";
            }
            else if($idFab == 4){
                $carpeta = "oracle";
            }
            else if($idFab == 5){
                $carpeta = "sap";
            }
            else if($idFab == 6){
                $carpeta = "vmware";
            }
            else if($idFab == 7){
                $carpeta = "unixibm";
            }
            else if($idFab == 8){
                $carpeta = "unixoracle";
            }
            else if($idFab == 10){
                $carpeta = "spla";
            }
            $tabla = $repositorio->archivoArchEncSam($_SESSION["client_id"], $_SESSION["client_empleado"], $idFab);
            if(count($tabla) > 0 &&  $tabla["otros"] != ""){
                if(file_exists($GLOBALS['app_root'] . "/sam/" . $carpeta . "/otros/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"]. "/" . $tabla["otros"])){
                    unlink($GLOBALS['app_root'] . "/sam/" . $carpeta . "/otros/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $tabla["otros"]);
                    $repositorio->limpiarOtrosEncabSam($_SESSION["client_id"], $_SESSION["client_empleado"], $idFab);
                }
            }
            else{

                $repositorio->agregarArchEncSam($_SESSION["client_id"], $_SESSION["client_empleado"], $idFab);
            }

            if(is_uploaded_file($_FILES["otros"]["tmp_name"]) ){	
                $archivo = $_FILES["otros"]["name"]; 
                if(file_exists($GLOBALS['app_root'] . "/sam/" . $carpeta . "/otros/".$_SESSION["client_id"] . "/" . $_SESSION["client_empleado"])){
                    move_uploaded_file($_FILES['otros']['tmp_name'], $GLOBALS['app_root'] . "/sam/" . $carpeta . "/otros/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $archivo);
                    $repositorio->actualizarOtrosEncabSam($_SESSION["client_id"], $_SESSION["client_empleado"], $archivo, $idFab);
                    $fecha = date("d/m/Y h:i:s A");
                    $result = 1;
                }
                else{
                    if(!mkdir($GLOBALS['app_root'] . "/sam/" . $carpeta . "/otros/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"], 0777, true)) {
                        $result = 2;
                    }
                    else{
                        move_uploaded_file($_FILES['otros']['tmp_name'], $GLOBALS['app_root'] . "/sam/" . $carpeta . "/otros/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $archivo);
                        $repositorio->actualizarOtrosEncabSam($_SESSION["client_id"], $_SESSION["client_empleado"], $archivo, $idFab);
                        $fecha = date("d/m/Y h:i:s A");
                        $result = 1;
                    }
                }  
            }
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'fecha'=>$fecha, 
            'archivo'=>$archivo, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);