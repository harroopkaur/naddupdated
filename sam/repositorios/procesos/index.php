<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_email.php");
$general = new General();
$nueva_funcion = new funcionesSam();
$email = new email();

//inicio alerta de renovacion de contrato
$alertaContrato = $nueva_funcion->alertaContratosActivos($_SESSION["client_id"], $_SESSION["client_empleado"]);
$conteo = count($alertaContrato);

$i = 0;
$mensajeAlerta = "";
foreach($alertaContrato as $row){
    /*if($row["tiempoExpiracion"] == 90 || $row["tiempoExpiracion"] == 60 || $row["tiempoExpiracion"] == 30
    || $row["tiempoProxima"] == 90 || $row["tiempoProxima"] == 60 || $row["tiempoProxima"] == 30){
        
        if($row["tiempoExpiracion"] == 90 && $row["correoExpiracion90"] == ""){
            $email->enviar_alerta_renovacion($row["correo"], $row["nombre"], $row["apellido"], utf8_decode("faltan " . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número: ") . $row["numero"]);
            $nueva_funcion->actulizarCorreoExpiracion90($row["idContrato"]);
        }
        
        if($row["tiempoExpiracion"] == 60 && $row["correoExpiracion60"] == ""){
            $email->enviar_alerta_renovacion($row["correo"], $row["nombre"], $row["apellido"], utf8_decode("faltan " . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número: ") . $row["numero"]);
            $nueva_funcion->actulizarCorreoExpiracion60($row["idContrato"]);
        }
        
        if($row["tiempoExpiracion"] == 30 && $row["correoExpiracion30"] == ""){
            $email->enviar_alerta_renovacion($row["correo"], $row["nombre"], $row["apellido"], utf8_decode("faltan " . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número: ") . $row["numero"]);
            $nueva_funcion->actulizarCorreoExpiracion30($row["idContrato"]);
        }
        
        if($row["tiempoProxima"] == 90 && $row["correoProxima90"] == ""){
            $email->enviar_alerta_renovacion($row["correo"], $row["nombre"], $row["apellido"], utf8_decode("faltan " . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número: ") . $row["numero"]);
            $nueva_funcion->actulizarCorreoProxima90($row["idContrato"]);
        }
        
        if($row["tiempoProxima"] == 60 && $row["correoProxima60"] == ""){
            $email->enviar_alerta_renovacion($row["correo"], $row["nombre"], $row["apellido"], utf8_decode("faltan " . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número: ") . $row["numero"]);
            $nueva_funcion->actulizarCorreoProxima60($row["idContrato"]);
        }
        
        if($row["tiempoProxima"] == 30 && $row["correoProxima30"] == ""){
            $email->enviar_alerta_renovacion($row["correo"], $row["nombre"], $row["apellido"], utf8_decode("faltan " . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número: ") . $row["numero"]);
            $nueva_funcion->actulizarCorreoProxima30($row["idContrato"]);
        }
    }*/
    
    
    if($i == 0){
        $mensajeAlerta .= "Estimado cliente faltan...<br> " . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número " . $row["numero"] . ".";
    }
    else{
        $mensajeAlerta .= "<br>" . $row["tiempoExpiracion"] . " días para la Anualidad y " . $row["tiempoProxima"] . " días para la Renovación del contrato número " . $row["numero"] . ".";
    }
    $i++;
}
//fin alerta de renovacion de contrato

