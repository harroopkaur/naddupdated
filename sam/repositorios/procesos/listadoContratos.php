<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();
$general = new General();

$total = $nueva_funcion->totalRegistrosContratos($_SESSION["client_id"], $_SESSION["client_empleado"]);
$limite = $general->limit_paginacion;

if($total < $limite){
        $pagina = $total;
}
else{
        $pagina = $limite;
}

$query = $nueva_funcion->listadoContratos($_SESSION["client_id"], $_SESSION["client_empleado"], 1, $pagina, "numero", "asc");
$inicio = 0;
if(count($query) > 0){
        $inicio = 1;
}