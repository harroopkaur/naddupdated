<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_moduloSam.php");

$nueva_funcion = new funcionesSam();
$general = new General();
$opcion = "";
if(isset($_GET["opcion"])){
    $opcion = $general->get_escape($_GET["opcion"]);
}

if($opcion == "consulta"){
    $consultar_contrato = new funcionesSam();
    $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
    
    $contrato = 0;
    if(isset($_GET["contrato"]) && filter_var($_GET["contrato"], FILTER_VALIDATE_INT) !== false){
        $contrato = $_GET["contrato"];
    }

    $tablaCabecera  = $consultar_contrato->verCabeceraContrato($contrato);
    $tablaDetalle   = $consultar_contrato->verDetalleContrato($contrato, $asignaciones);
    
    $cantidad = 0;
    $precio = 0;
    
    foreach($tablaCabecera as $row){
        $idFabricante    = $row["idFabricante"];
        $numero          = $row["numero"];
        $tipo            = $row["tipo"];
        $fechaEfectiva   = $row["fechaEfectiva"];
        $fechaExpiracion = $row["fechaExpiracion"];
        $fechaProxima    = $row["fechaProxima"];
        $archivo1        = $row["archivo1"];
        $archivo2        = $row["archivo2"];
        $archivo3        = $row["archivo3"];
        $archivo4        = $row["archivo4"];
        $archivo5        = $row["archivo5"];
        $comentarios     = $row["comentarios"];
        //$cantidad        = $row["cantidad"];
        //$precio          = $row["precio"];
        $subsidiaria     = $row["subsidiaria"];
        $proveedor       = $row["proveedor"];
    }

    $idFabAux = $idFabricante;
    if($idFabricante == 10){
        $idFabAux = 3;
    }
    $prodFabricante = $consultar_contrato->obtenerProdFabricante($idFabAux);
    //$asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
}

$nuevo_producto = new funcionesSam();
$resultado = $nuevo_producto->obtenerFabricantesEmpleado($_SESSION["client_empleado"]);


$moduloSam = new ModuloSam();
$permModif = $moduloSam->existe_permisoEmpleado($_SESSION['client_empleado'], 19);