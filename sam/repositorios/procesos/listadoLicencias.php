<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();
$general = new General();

//$listadoAsignacion = $nueva_funcion->listadoAsignaciones($_SESSION["client_id"], $_SESSION["client_empleado"]);
$listadoAsignacion = $nueva_funcion->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
$fabricantes = $nueva_funcion->obtenerFabricantes();

$total = $nueva_funcion->totalRegistrosLicencias($_SESSION["client_id"], $_SESSION["client_empleado"], "", "", $listadoAsignacion);
if($total < 25){
        $pagina = $total;
}
else{
        $pagina = 25;
}

$listadoLicencias = $nueva_funcion->listadoLicencias($_SESSION["client_id"], $_SESSION["client_empleado"], "", $listadoAsignacion, "", 1, $pagina, "fabricantes.nombre, detalleContrato.idDetalleContrato", "asc");
$inicio = 0;
if(count($listadoLicencias) > 0){
    $inicio = 1;
}