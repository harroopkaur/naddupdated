<h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Compras y Facturas</p></h1>

<input type="hidden" id="pagina" value="1">
<input type="hidden" id="direccion" name="direccion" value="asc">

<div style="float:left">
    <div class="activado1" style="border:1px solid; width:70px; height:25px; font-size:12px; float:left; text-align:center; cursor:pointer; line-height:25px;" onclick="return borrar()">Borrar</div>
    <div class="activado1" style="border:1px solid; float:left; width:100px; height:25px; font-size:12px; margin-left:10px; text-align:center; line-height:25px; cursor:pointer;" onclick="location.href='contrato.php?opcion=agregar';">A&ntilde;adir Nuevo</div>
</div>

<input type="hidden" id="limite" name="limite" value="<?= $limite ?>">

<div style="float:right; margin-right:10px;">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' de ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ echo 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ echo 'display:none'; } ?>" id="ultimo">
</div>

<br>
<br>

<div style="clear: both;">
    <table id="listaContratos" style="width:100%; border:1px solid;">
        <thead style="background-color:#C1C1C1;">
            <tr>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkAll" name="checkAll"></th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">N&uacute;mero de Contrato</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fabricante</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fecha Efectiva</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fecha de Expiraci&oacute;n</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fecha Pr&oacute;xima</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Estatus</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Comentarios</th>
            </tr>
        </thead>
        <tbody id="tablaContratos">
            <?php
            $i = 0;
            foreach($query as $row){
            ?>
                <tr style="border-bottom: 1px solid #ddd" id="<?= $i ?>"><input type="hidden" id="idContrato<?= $i ?>" name="idContrato<?= $i ?>" value="<?= $row["idContrato"] ?>">
                    <td style="text-align:center; border-bottom: 1px solid #ddd;"><input type="checkbox" id="check<?= $i ?>" name="check<?= $i ?>"></td>
                    <td style="border-bottom: 1px solid #ddd;"><a href="#" style="color:blue;" onclick="verContrato(<?= $row["idContrato"] ?>, 'consulta')"><?= $row["numero"] ?></a></td>
                    <td style="border-bottom: 1px solid #ddd;"><?= $row["nombre"] ?></td>
                    <td style="border-bottom: 1px solid #ddd;"><?= $row["fechaEfectiva"] ?></td>
                    <td style="border-bottom: 1px solid #ddd;"><?= $row["fechaExpiracion"] ?></td>
                    <td style="border-bottom: 1px solid #ddd;"><?= $row["fechaProxima"] ?></td>
                    <td style="border-bottom: 1px solid #ddd;"><?= $row["estado"] ?></td>
                    <td style="border-bottom: 1px solid #ddd;"><?= $row["comentarios"] ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>
<br>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/repositorios/';">Atras</div>

<script>
var pagina = 1;
$(document).ready(function(){
    $("#listaContratos").tablesorter();
    
    $("#checkAll").click(function(){
        tam = $("#tablaContratos tr").length;
        if($("#checkAll").prop("checked")){
            for(i=0; i < tam; i++){
                if($("#check"+i).length > 0){
                    $("#check"+i).attr("checked", true);
                }
            }
        }
        else{
            for(i=0; i < tam; i++){
                if($("#check"+i).length > 0){
                    $("#check"+i).attr("checked", false);
                }
            }
        }
    });

    $("#primero").click(function(){
        $("#direccion").val("asc");
        pagina = 1;
        //$("#pagina").val(pagina);
        $("#tablaContratos").empty();
        $("#numPaginas").empty();
        $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoContratos.php", { pagina : pagina, limite : $("#limite").val(), orden : "numero", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#tablaContratos").append(data[0].div);
            $("#numPaginas").append(data[0].paginacion);
            $("#primero").hide();
            $("#atras").hide();
            if(data[0].sinPaginacion === "no"){
                $("#siguiente").show();
                $("#ultimo").show();
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    });

    $("#ultimo").click(function(){
        $("#direccion").val("asc");
        $("#tablaContratos").empty();
        $("#numPaginas").empty();
        //$("#pagina").val("ultima");
        $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoContratos.php", { pagina : "ultima", limite : $("#limite").val(), orden : "numero", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#tablaContratos").append(data[0].div);
            $("#numPaginas").append(data[0].paginacion);
            if(data[0].sinPaginacion === "no"){
                $("#primero").show();
                $("#atras").show();
            }
            $("#siguiente").hide();
            $("#ultimo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    });

    $("#siguiente").click(function(){
        $("#direccion").val("asc");
        pagina += 1;
        //$("#pagina").val(pagina);
        $("#tablaContratos").empty();
        $("#numPaginas").empty();
        $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoContratos.php", { pagina : pagina, limite : $("#limite").val(), orden : "numero", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#tablaContratos").append(data[0].div);
            $("#numPaginas").append(data[0].paginacion);
            if(data[0].sinPaginacion === "no"){
                $("#primero").show();
                $("#atras").show();
            }
            if(data[0].ultimo === "si"){
                $("#siguiente").hide();
                $("#ultimo").hide();
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    });

    $("#atras").click(function(){
        $("#direccion").val("asc");
        pagina -= 1;
        //$("#pagina").val(pagina);
        $("#tablaContratos").empty();
        $("#numPaginas").empty();
        $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoContratos.php", { pagina : pagina, limite : $("#limite").val(), orden : "numero", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $("#tablaContratos").append(data[0].div);
            $("#numPaginas").append(data[0].paginacion);
            if(data[0].primero === "si"){
                $("#primero").hide();
                $("#atras").hide();
            }
            if(data[0].sinPaginacion === "no"){
                $("#siguiente").show();
                $("#ultimo").show();
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    });
});

function verContrato(contrato, opcion){
    location.href="<?= $GLOBALS['domain_root'] ?>/sam/repositorios/contrato.php?contrato="+contrato+"&opcion="+opcion;
}

function borrar(){
    tam = $("#tablaContratos tr").length;
    $aux = 0;
    for(i=0; i < tam; i++){
        if($("#check"+i).prop("checked")){
            $aux++;
        }
    }

    if($aux === 0){
        return false;
    }
    
    $.alert.open("confirm", "Desea eliminar los contratos?", {"Aceptar" : "Aceptar", "Cancelar" : "Cancelar"}, function(btn) {
        if(btn === "Cancelar"){
            return false;
        }else{
            tam = $("#tablaContratos tr").length;
            j = 0;
            z = 0;
            for(i=0; i < tam; i++){
                if($("#check"+i).prop("checked")){
                    z++;
                    $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/eliminarContrato.php", { contrato : $("#idContrato"+i).val(), token : localStorage.licensingassuranceToken }, function(data){
                        <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>; 

                        if(data[0].row === 1){
                            j++;
                        }
                    }, "json")
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                }
            }

            location.href = "listadoContratos.php";
        }
    });
}
</script>