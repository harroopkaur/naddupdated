<h1 class="textog negro" style="margin:20px; text-align:center;">Repositorio de <p style="color:#06B6FF; display:inline">Despliegue</p></h1>

<div style="float:left; width:19%;">

</div>

<div style="float:right; width:73%;">
    <p style="float:left; font-size:20px; ">&nbsp;&nbsp;Historial de despliegue de software</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid; float:right;" id="selectFabricante">
        <option value="">Fabricante</option>
        <?php
        foreach($tablaFabri as $row){
        ?>    
            <option value="<?= $row["idFabricante"] ?>" <?php if($fab == $row["idFabricante"]){ echo 'selected="selected"'; } ?>><?= $row["nombre"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<br><br>	

<style>
    .filaDesplegue{
        text-align    : center; 
        border-left   : 1px solid #FFFFFF; 
        border-right  : 1px solid #FFFFFF; 
        border-bottom : 2px solid #FFFFFF;
        color         : #FFFFFF;
    }
</style>
<div style="float:right; width:98%; padding:10px; <?php if($fab == ""){ echo 'display:none;'; } ?>" id="contenidoDespliegue">';
    <?php
    if($fab != ""){
        $tablaAux = $nueva_funcion->mostrarDespliegueSam($fab); 
        $tabla = explode("*", $tablaAux);
        echo $tabla[0];
    }
    ?>
</div>

<br style="clear:right; ">
<br>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='listadoLicencias.php';">Atras</div>

<script>
    $(document).ready(function(){
        /*$("#buscarCustom").click(function(){
            $("#custom").click();
        });

        $("#buscarOtros").click(function(){
            $("#otros").click();
        });

        $("#custom").change(function(){
            $("#fondo").show();
            $("#tokenCustom").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#subirCustom")[0]);	
            $.ajax({
                type: "POST",
                url: "' . $GLOBALS['domain_root'] . '/sam/subirCustom.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    if(data[0].resultado === false){
                        location.href = "' . $GLOBALS["domain_root"] . '";
                        return false;
                    }
                    if(data[0].sesion === "false"){
                        $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                            location.href = "' . $GLOBALS['domain_root'] . '";
                            return false;
                        });
                    }
                    result = data[0].result;
                    if(result == 0){
                        alert("No se cargó el archivo");
                    }
                    else if(result == 1){
                        alert("Se guardó el archivo con éxito");
                        $("#fechaCustom").empty();
                        $("#fechaCustom").append(data[0].fecha);
                        $("#archivoCustom").val(data[0].archivo);
                        $("#mostrarCustom").show();
                        $("#eliminarCustom").show();
                    }
                    else if(result == 2){
                        alert("No se creo la carpeta del usuario");
                    }
                    $("#fondo").hide();
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#otros").change(function(){
            $("#fondo").show();
            $("#tokenOtros").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#subirOtros")[0]);	
            $.ajax({
                type: "POST",
                url: "' . $GLOBALS['domain_root'] . '/sam/subirOtros.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    if(data[0].resultado === false){
                        location.href = "' . $GLOBALS["domain_root"] . '";
                        return false;
                    }
                    if(data[0].sesion === "false"){
                        $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                            location.href = "' . $GLOBALS['domain_root'] . '";
                            return false;
                        });
                    }
                    result = data[0].result;
                    if(result == 0){
                        alert("No se cargó el archivo");
                    }
                    else if(result == 1){
                        alert("Se guardó el archivo con éxito");
                        $("#fechaOtros").empty();
                        $("#fechaOtros").append(data[0].fecha);
                        $("#archivoOtros").val(data[0].archivo);
                        $("#mostrarOtros").show();
                        $("#eliminarOtros").show();
                    }
                    else if(result == 2){
                        alert("No se creo la carpeta del usuario");
                    }
                    $("#fondo").hide();
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });*/

        $("#elimRepoSam").click(function(){
            var r = confirm("Desea el despliegue del repositorio");
            if(r){
                $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/eliminarDespliegue.php", { fabricante : $("#selectFabricante").val(), token : localStorage.licensingassuranceToken }, function(data){
                    <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                            
                    if(data[0].result == 0){
                        $.alert.open("alert", "No se pudo eliminar el despliegue", {"Aceptar" : "Aceptar"}, function() {
                        });
                    }
                    if(data[0].result == 1){
                        alert("Despliegue eliminado con éxito");
                        $("#alcance").hide();
                        $("#usabilidad").hide();
                        $("#balanza").hide();
                        $("#listadoContrato").hide();
                        $("#optimizacion").hide();
                        $("#detalle").hide();
                        $("#servidor").hide();
                        $("#elimRepoSam").hide();
                    }
                }, "json")
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    });
                });
            }
        });

        $("#selectFabricante").change(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/despliegueFabricante.php", { fabricante : $("#selectFabricante").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#elimRepoSam").hide();
                $("#contenidoDespliegue").empty();
                $("#contenidoDespliegue").append(data[0].div);
                $("#contenidoDespliegue").show();
                if(data[0].eliminar > 1){
                    $("#elimRepoSam").show();
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });
    });

    function repDetalle(archivo){
        if($("#selectFabricante").val() == 3){
            location.href = "<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelDetalle.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 10){
            location.href = "<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelDetalle.php?vert1=" + archivo;
        }
    }

    function repFabricante(archivo){
        if($("#selectFabricante").val() == 1){
            location.href = "<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelDetalle.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 2){
            location.href = "<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelDetalle.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 4){
            location.href = "<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelDetalle.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 5){
            location.href = "<?= $GLOBALS['domain_root'] ?>/sap/reportes/excelDetalle.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 6){
            location.href = "<?= $GLOBALS['domain_root'] ?>/VMWare/reportes/excelDetalle.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 7){
            location.href = "<?= $GLOBALS['domain_root'] ?>/unixIbm/reportes/excelDetalle.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 8){
            location.href = "<?= $GLOBALS['domain_root'] ?>/unixOracle/reportes/excelDetalle.php?vert1=" + archivo;
        }
    }

    function repAlcance(archivo){
        if($("#selectFabricante").val() == 1){
            location.href="<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelAlcance.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 2){
            location.href="<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelAlcance.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 3){
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelAlcance.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelEquiposNoDescubiertos.php?vert=0&vert1=" + archivo);
        }
        else if($("#selectFabricante").val() == 4){
            location.href="<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelAlcance.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 10){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelAlcance.php?vert=0&vert1=" + archivo;
        }
    }

    function repUsabilidad(archivo){                
        if($("#selectFabricante").val() == 1){
            location.href="<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 2){
            location.href="<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 3){
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelEquiposDesuso.php?vert=0&vert1=" + archivo);
        }
        else if($("#selectFabricante").val() == 4){
            location.href="<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 5){
            location.href="<?= $GLOBALS['domain_root'] ?>/sap/reportes/repoUsabilidad.php?vert=" + archivo;
        }
        else if($("#selectFabricante").val() == 10){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelUsabilidad.php?vert=0&vert1=" + archivo;
        }
    }

    function repBalanza(archivo){
        if($("#selectFabricante").val() == 1){
            location.href="<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 2){
            location.href="<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 3){
            location.href="<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 4){
            location.href="<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 5){
            location.href="<?= $GLOBALS['domain_root'] ?>/sap/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 6){
            location.href="<?= $GLOBALS['domain_root'] ?>/VMWare/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 7){
            location.href="<?= $GLOBALS['domain_root'] ?>/unixIbm/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 8){
            location.href="<?= $GLOBALS['domain_root'] ?>/unixOracle/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 10){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelBalanza.php?vert=0&vert1=" + archivo;
        }
    }

    function repOptimizacion(archivo){
        if($("#selectFabricante").val() == 1){
            location.href="<?= $GLOBALS['domain_root'] ?>/adobe/reportes/excelOptimizacion.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 2){
            location.href="<?= $GLOBALS['domain_root'] ?>/ibm/reportes/excelOptimizacion.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 3){
            location.href="<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelOptimizacion.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 4){
            location.href="<?= $GLOBALS['domain_root'] ?>/oracle/reportes/excelOptimizacion.php?vert1=" + archivo;
        }
        else if($("#selectFabricante").val() == 10){
            location.href="<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelOptimizacion.php?vert1=" + archivo;
        }
    }

    function listadoContrato(archivo){
        location.href="<?= $GLOBALS['domain_root'] ?>/VMWare/reportes/excelListadoContrato.php?vert1=" + archivo;
    }

    function repServidor(archivo){
        if($("#selectFabricante").val() == 3){
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelServidor.php?vert1=" + archivo);
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelPruebaDesarrollo.php?vert1=" + archivo);
        }
        else if($("#selectFabricante").val() == 10){
            location.href = "<?= $GLOBALS['domain_root'] ?>/spla/reportes/excelServidor.php?vert1=" + archivo;
        }
    }

    function repEquipo(archivo){
        if($("#selectFabricante").val() == 3){
            window.open("<?= $GLOBALS['domain_root'] ?>/microsoft/reportes/excelDetalleEquipo.php?vert1=" + archivo);
        }
    }

    function bajarCustom(cliente, empleado, carpeta, posicion = ""){
        window.open("<?= $GLOBALS['domain_root'] ?>/sam/"+ carpeta +"/custom/" + cliente + "/" + empleado + "/" + $("#archivoCustom"+posicion).val());
    }

    function bajarOtros(cliente, empleado, carpeta, posicion = ""){
        window.open("<?= $GLOBALS['domain_root'] ?>/sam/"+ carpeta +"/otros/" + cliente + "/" + empleado + "/" + $("#archivoOtros"+posicion).val());
    }

    function eliminarCustom(posicion = ""){
        var r = confirm("Desea eliminar el archivo");
        if(r){
            $("#fondo").show();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/eliminarCustom.php", { fabricante : $("#selectFabricante").val(), posicion : posicion, token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#fondo").hide();
                if(data[0].result == 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                } else if(data[0].result == 1){
                    $.alert.open("alert", "Archivo eliminado", {"Aceptar" : "Aceptar"}, function() {
                    });
                    $("#mostrarCustom"+posicion).hide();
                    $("#fechaCustom"+posicion).hide();
                    $("#eliminarCustom"+posicion).hide();
                }

            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        }
    }

    function archivosDespliegueSAM(archivo){
        $("#fondo").show();
        $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/buscarArchivosDespliegue.php", { fabricante : $("#selectFabricante").val(), archivo : archivo, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            $.each( data[0].mostrar, function(key , value ) {
                window.open(value);
            });
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    }

    function eliminarOtros(posicion = ""){
        var r = confirm("Desea eliminar el archivo");
        if(r){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/eliminarOtros.php", { fabricante : $("#selectFabricante").val(), posicion : posicion, token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                if(data[0].result == 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                if(data[0].result == 1){
                    $.alert.open("alert", "Archivo eliminado", {"Aceptar" : "Aceptar"}, function() {
                    });
                    $("#mostrarOtros"+posicion).hide();
                    $("#fechaOtros"+posicion).hide();
                    $("#eliminarOtros"+posicion).hide();
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        }
    }

    function eliminarRepoSAM(archivo){
        $("#fondo").show();
        $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/eliminarArchivoDespliegue.php", { fabricante : $("#selectFabricante").val(), archivo : archivo, token : localStorage.licensingassuranceToken }, function(data){
            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
            if(data[0].result == 0){
                $("#fondo").hide();
                $.alert.open("alert", "No se pudo eliminar el repositorio", {"Aceptar" : "Aceptar"}, function() {
                    return false;
                });
            }
            if(data[0].result == 1){
                $("#fondo").hide();
                $.alert.open("alert", "Repositorio eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                    $("." + archivo).empty();
                    return false;
                });
            }
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
            });
        });
    }
</script>