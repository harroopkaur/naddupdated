<h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <span style="color:#06B6FF; display:inline">Licencias</span></h1>

<input type="hidden" id="pagina" value="1">

<div style="float:left;">
    <p style="float:left; line-height:30px;">Asignación</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="asignacion" name="asignacion">
        <option value="">--Seleccione--</option>
        <?php
        foreach($listadoAsignacion as $row){
        ?>
            <option value="<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="float:left;">
    <p style="float:left; margin-left:20px; line-height:30px;">Software</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="fabricante" name="fabricante">
        <option value="">--Seleccione--</option>
        <?php
        foreach($fabricantes as $row){
        ?>
            <option value="<?= $row["idFabricante"] ?>"><?= $row["nombre"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Exportar Excel</div>

<br>
<br>

<div style="float:left">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center;" id="numPaginas"><?= $inicio . '-' . $pagina . ' de ' . $total ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; <?php if($total <= $pagina){ $div .= 'display:none'; } ?>" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Mostrar</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">por p&aacute;gina</p>
</div>

<br>
<br>
<div id="ttabla1"  style="width:99%; max-height: 400px; overflow-y:hidden; overflow-x:auto; 
    clear:both; margin:0 auto; padding:0;">
    <table id="listaLicencias" style="width:1500px; border:1px solid;">
        <thead style="background-color:#C1C1C1;">
            <tr>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkAll" name="checkAll"></th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fabricante</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">N&uacute;mero de contrato</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Proveedor</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fecha Efectiva</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fecha Expiración</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Producto</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Edici&oacute;n</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Versi&oacute;n</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Cantidades</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Precio</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Asignaci&oacute;n</th>
            </tr>
        </thead>
        <tbody id="tablaLicencias">
        <?php
        $i = 0;
        foreach($listadoLicencias as $row){
        ?>
            <tr style="border-bottom: 1px solid #ddd;">
                <td style="text-align:center; border-bottom: 1px solid #ddd;"><input type="checkbox" id="check<?= $i ?>" name="check<?= $i ?>"></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["nombre"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["numero"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["proveedor"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $general->reordenarFecha($row["fechaEfectiva"], "-", "/") ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $general->reordenarFecha($row["fechaExpiracion"], "-", "/") ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["producto"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["edicion"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["version"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["cantidad"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["precio"] ?></td>
                <td style="border-bottom: 1px solid #ddd;"><?= $row["asignacion"] ?></td>
            </tr>
            <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
</div>
<br>
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='listadoContratos.php'">Atras</div>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listaLicencias").tablesorter();
        $("#listaLicencias").tableHeadFixer();
        
        $("#checkAll").click(function(){
            if($("#checkAll").prop("checked")){
                for(i=0; i < $("#tablaLicencias tr").length; i++){
                    if($("#check"+i).length > 0){
                        $("#check"+i).attr("checked", true);
                    }
                }
            }
            else{
                for(i=0; i < $("#tablaLicencias tr").length; i++){
                    if($("#check"+i).length > 0){
                        $("#check"+i).attr("checked", false);
                    }
                }
            }
        });

        $("#borrar").click(function(){
            for(i=0; i < $("#tablaLicencias tr").length; i++){
                if($("#check"+i).prop("checked")){
                    $("#"+i).remove();
                }
            }
            $("#checkAll").attr("checked", false);
        });

        $("#asignacion").change(function(){
            $("#fondo").show();
            $("#direccion").val("asc");
            $("#tablaLicencias").empty();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoLicencias.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), orden : "nombre", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#tablaLicencias").append(data[0].div);
                $("#listaLicencias").trigger("update");
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                    $("#primero").hide();
                    $("#atras").hide();
                    if(data[0].sinPaginacion === "no"){
                        $("#siguiente").show();
                        $("#ultimo").show();
                    }
                
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#fabricante").change(function(){
            $("#fondo").show();
            $("#direccion").val("asc");
            $("#tablaLicencias").empty();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoLicencias.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), orden : "nombre", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                $("#tablaLicencias").append(data[0].div);
                $("#listaLicencias").trigger("update");
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#limite").change(function(){
            $("#fondo").show();
            $("#direccion").val("asc");
            pagina = 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoLicencias.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), orden : "nombre", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                $("#tablaLicencias").append(data[0].div);
                $("#listaLicencias").trigger("update");
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#primero").click(function(){
            $("#fondo").show();
            $("#direccion").val("asc");
            pagina = 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoLicencias.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), orden : "nombre", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                $("#tablaLicencias").append(data[0].div);
                $("#listaLicencias").trigger("update");
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#ultimo").click(function(){
            $("#fondo").show();
            $("#direccion").val("asc");
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoLicencias.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : "ultima", limite : $("#limite").val(), orden : "nombre", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#tablaLicencias").append(data[0].div);
                $("#listaLicencias").trigger("update");
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#siguiente").click(function(){
            $("#fondo").show();
            $("#direccion").val("asc");
            pagina += 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoLicencias.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), orden : "nombre", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#tablaLicencias").append(data[0].div);
                $("#listaLicencias").trigger("update");
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#atras").click(function(){
            $("#fondo").show();
            $("#direccion").val("asc");
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/listadoLicencias.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), orden : "nombre", direccion : $("#direccion").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#tablaLicencias").append(data[0].div);
                $("#listaLicencias").trigger("update");
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });	

        $("#exportar").click(function(){
            window.open("<?= $GLOBALS['domain_root'] ?>/sam/exportarExcel.php?vert="+$("#fabricante").val()+"&vert1="+$("#asignacion").val());
        });
    });
</script>