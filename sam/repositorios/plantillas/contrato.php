<style>
    .asteriscoObligatorio{
        margin-left:5px; 
        top: 10px; 
        font-size:30px; 
        color:red; 
        position:relative;
    }
    
    .inputSAM{
        height:25px; 
        width:150px;
    }
</style>
<form id="formContratoSAM" name="formContratoSAM" method="post" enctype="multipart/form-data" action="' . $GLOBALS['domain_root'] . '/sam/guardarContratoAjax.php" target="_blank">
    <input type="hidden" id="token" name="token">
    <input type="hidden" id="bandOpcion" name="bandOpcion" value="<?= $opcion ?>">
    <input type="hidden" id="cargarArchivo"><!--se usa para saber el nombre del archivo que se esta cargando y el lugar donde debe ir-->
    <input type="hidden" id="cargarArchivoOtros"><!--Se usa para saber cual es el archivo de compras de Otros campos-->

    <?php /*inicio controla los archivos que ya se han agregado*/
    if($opcion == "consulta"){
    ?> 
        <input type="hidden" id="contrato" name="contrato" value="<?= $contrato ?>">
        <input type="hidden" id="archivoActual1" name="archivoActual1" value="<?= $archivo1 ?>">
        <input type="hidden" id="archivoActual2" name="archivoActual2" value="<?= $archivo2 ?>">
        <input type="hidden" id="archivoActual3" name="archivoActual3" value="<?= $archivo3 ?>">
        <input type="hidden" id="archivoActual4" name="archivoActual4" value="<?= $archivo4 ?>">
        <input type="hidden" id="archivoActual5" name="archivoActual5" value="<?= $archivo5 ?>">
    <?php
    }
    /*fin controla los archivos que ya se han agregado*/ 
    ?>

    <input type="hidden" id="totalRegistros" name="totalRegistros" value="<?php if($opcion == 'consulta'){ echo count($tablaDetalle); } ?>"><!--se usa para controlar el total de licencias que se estan agregando y saber si las agregó todas-->
    <input type="hidden" id="totalFilaTabla" name="totalFilaTabla" value="<?php if($opcion == 'consulta'){ echo count($tablaDetalle); } ?>"><!--se usa para controlar el total de filas que ha tenido la tabla de licencias-->

    <h1 class="textog negro" style="margin:20px; text-align:center;">Detalle de <span style="color:#06B6FF; display:inline">Compras</span></h1>

    <!--inicio tabla de datos de la cabecera del contrato-->
    <table style="width:100%">
        <tr style="">
            <td style="width:20%; text-align:right; padding-right:10px; line-height:40px;">Fabricante</td>
            <td style="width:20%; text-align:left; padding-left:10px; line-height:40px;">
                <select class="inputSAM" id="fabricante" name="fabricante" onchange="$('#errorFabricante').hide();" <?php if($opcion == "consulta"){ echo 'disabled="disabled"'; } ?>>
                    <option value="">--Seleccione--</option>
                    <?php
                    foreach ($resultado as $row) {
                    ?>
                        <option value="<?= $row["idFabricante"] ?>" <?php if($opcion == "consulta" && $row["idFabricante"] == $idFabricante){ echo 'selected="selected"'; } ?>><?= $row["nombre"] ?></option>
                    <?php
                    }
                    ?>
                </select><span class="asteriscoObligatorio">*</span>
                <div class="error_archivo" id="errorFabricante" style="display:none;">Debe Seleccionar el Fabricante</div>
            </td>
            <td style="width:20% "></td>
            <td style="width:20%; text-align:right; padding-right:10px; line-height:40px;">Fecha de expiraci&oacute;n</td>
            <td style="width:20%; text-align:left; padding-left:10px; line-height:40px;">
                <input type="text" class="inputSAM" id="fechaExpiracion" name="fechaExpiracion" <?php if($opcion == 'consulta'){ echo 'value="' . $fechaExpiracion . '" readonly'; } ?>><span class="asteriscoObligatorio">*</span>
                <div class="error_archivo" id="errorFechaExpiracion" style="display:none;"></div>
            </td>
        </tr>

        <tr>
            <td style="text-align:right; padding-right:10px; line-height:40px;">N&uacute;mero de contrato</td>
            <td style="padding-left:10px; line-height:40px;">
                <input type="text" class="inputSAM" id="numContrato" name="numContrato" onkeyup="$('#errorNumContrato').hide();" <?php if($opcion == 'consulta'){ echo 'value="' . $numero . '" readonly'; } ?>><span class="asteriscoObligatorio">*</span>
                <div class="error_archivo" id="errorNumContrato" style="display:none;">Debe llenar el n&uacute;mero de contrato</div>
            </td>
            <td></td>
            <td style="text-align:right; padding-right:10px; line-height:40px;">Fecha pr&oacute;xima</td>
            <td style="text-align:left; padding-left:10px; line-height:40px;">
                <input type="text" class="inputSAM" id="fechaProxima" name="fechaProxima" <?php if($opcion == 'consulta'){ echo 'value="' . $fechaProxima . '" readonly'; } ?>><span class="asteriscoObligatorio">*</span>
                <div class="error_archivo" id="errorFechaProxima" style="display:none;">Debe seleccionar la fecha pr&oacute;xima</div>
            </td>
        </tr>

        <tr>
            <td style="text-align:right; padding-right:10px;">Tipo de contrato</td>
            <td style="padding-left:10px;">
                <input type="text" class="inputSAM" id="tipoContrato" name="tipoContrato" onkeyup="$('#errorTipoContrato').hide();" <?php if($opcion == 'consulta'){ echo 'value="' . $tipo . '" readonly'; } ?>><span class="asteriscoObligatorio">*</span>
                <div class="error_archivo" id="errorTipoContrato" style="display:none;">Debe llenar el tipo de contrato</div>
            </td>
            <td></td>
            <td style="text-align:right; padding-right:10px; line-height:40px;">Comentarios</td>
            <td style="text-align:left; padding-left:10px; line-height:40px;">
                <input type="text" style="height:25px; width:150px;" id="comentario" name="comentario" <?php if($opcion == 'consulta'){ echo 'value="' . $comentarios . '" readonly'; } ?>>
            </td>
        </tr>

        <tr>
            <td style="text-align:right; padding-right:10px; line-height:40px;">Fecha efectiva</td>
            <td style="padding-left:10px; line-height:40px;">
                <input type="text" class="inputSAM" id="fechaEfectiva" name="fechaEfectiva" <?php if($opcion == 'consulta'){ echo 'value="' . $fechaEfectiva . '" readonly'; } ?>><span class="asteriscoObligatorio">*</span>
                <div class="error_archivo" id="errorFechaEfectiva" style="display:none;">Debe seleccionar la fecha efectiva</div>
            </td>
            <td></td>
            <td style="text-align:right; padding-right:10px; line-height:40px;">Subsidiaria</td>
            <td style="text-align:left; padding-left:10px; line-height:40px;">
                <input type="text" class="inputSAM" id="subsidiaria" name="subsidiaria" <?php if($opcion == 'consulta'){ echo 'value="' . $subsidiaria . '" readonly'; } ?> maxlength="70"><span class="asteriscoObligatorio">*</span>
                <div class="error_archivo" id="errorSubsidiaria" style="display:none;">Debe llenar la subsidiaria</div>
            </td>
        </tr>
        
        <tr>
            <td style="text-align:right; padding-right:10px; line-height:40px;">Proveedor</td>
            <td style="padding-left:10px; line-height:40px;">
                <input type="text" class="inputSAM" id="proveedor" name="proveedor" <?php if($opcion == 'consulta'){ echo 'value="' . $proveedor . '" readonly'; } ?>>
            </td>
            <td></td>
            <td style="text-align:right; padding-right:10px; line-height:40px;"></td>
            <td style="text-align:left; padding-left:10px; line-height:40px;"></td>
        </tr>
    </table>
    <!--fin tabla de datos de la cabecera del contrato-->

    <hr>

    <!--inicio datos de los archivos que se va a cargar-->
    <h1 class="textog negro" style="margin:20px; text-align:center;">Documentaci&oacute;n</h1>
    <p style="text-align:center; margin-top:-10px; font-size:20px;">Adjunte sus archivos</p>
    <br>

    <div style="overflow:hidden;">
        <div style="width:610px; margin:0 auto;">
            <div style="display:inline-block;">
                <!--<div class="botonesSAM boton2" style="float:left">Adjuntar Archivo</div>-->
                <div style="width:100px; height:40px; background-color:#DDDDDD; text-align:center; line-height:40px; float:left; border:1px solid; cursor:pointer; overflow:hidden;" id="archivo1">        
                    <?php    
                    if($opcion == "consulta" && $archivo1 != ""){
                        echo $consultar_contrato->renombrarArchivo($archivo1);
                    }
                    else{
                        echo 'Archivo 1';
                    }
                    ?>
                </div>
                <div style="width:100px; height:40px; background-color:#DDDDDD; text-align:center; line-height:40px; float:left; margin-left:20px; border:1px solid; cursor:pointer; overflow:hidden;" id="archivo2">
                    <?php 
                    if($opcion == "consulta" && $archivo2 != ""){
                        echo $consultar_contrato->renombrarArchivo($archivo2);
                    }
                    else{
                        echo 'Archivo 2';
                    }
                    ?>
                </div>
                <div style="width:100px; height:40px; background-color:#DDDDDD; text-align:center; line-height:40px; float:left; margin-left:20px; border:1px solid; cursor:pointer; overflow:hidden;" id="archivo3">
                   <?php
                    if($opcion == "consulta" && $archivo3 != ""){
                        echo $consultar_contrato->renombrarArchivo($archivo3);
                    }
                    else{
                        echo 'Archivo 3';
                    }
                    ?>
                </div>
                <div style="width:100px; height:40px; background-color:#DDDDDD; text-align:center; line-height:40px; float:left; margin-left:20px; border:1px solid; cursor:pointer; overflow:hidden;" id="archivo4">
                    <?php
                    if($opcion == "consulta" && $archivo4 != ""){
                        echo $consultar_contrato->renombrarArchivo($archivo4);
                    }
                    else{
                        echo 'Archivo 4';
                    }
                    ?>
                </div>
                <div style="width:100px; height:40px; background-color:#DDDDDD; text-align:center; line-height:40px; float:left; margin-left:20px; border:1px solid; cursor:pointer; overflow:hidden;" id="archivo5">
                    <?php
                    if($opcion == "consulta" && $archivo5 != ""){
                        echo $consultar_contrato->renombrarArchivo($archivo5);
                    }
                    else{
                        echo 'Archivo 5';
                    }
                    ?>
                </div>
                <input type="file" id="fileArchivo1" name="fileArchivo1" style="display:none;" accept=".pdf">
                <input type="file" id="fileArchivo2" name="fileArchivo2" style="display:none;" accept=".pdf">
                <input type="file" id="fileArchivo3" name="fileArchivo3" style="display:none;" accept=".pdf">
                <input type="file" id="fileArchivo4" name="fileArchivo4" style="display:none;" accept=".pdf">
                <input type="file" id="fileArchivo5" name="fileArchivo5" style="display:none;" accept=".pdf">

                <input type="hidden" id="bandElimArchivo1" name="bandElimArchivo1" value="false">
                <input type="hidden" id="bandElimArchivo2" name="bandElimArchivo2" value="false">
                <input type="hidden" id="bandElimArchivo3" name="bandElimArchivo3" value="false">
                <input type="hidden" id="bandElimArchivo4" name="bandElimArchivo4" value="false">
                <input type="hidden" id="bandElimArchivo5" name="bandElimArchivo5" value="false">
            </div>
            <div style="display:inline-block; <?php if(!$permModif){ echo 'display:none;'; } ?>">
                <div style="margin-left:0; <?php if($opcion != "consulta" || ($opcion == "consulta" && $archivo1 == "")){ echo 'visibility: hidden;'; } ?>" 
                id="elimArchivo1" class="btnSamElim <?php if($opcion == "consulta" && $archivo1 != ""){ echo 'boton2'; } else { echo ' boton5'; } ?>">Eliminar</div>
                <div style="<?php if($opcion != "consulta" || ($opcion == "consulta" && $archivo2 == "")){ echo 'visibility: hidden;'; } ?>"
                id="elimArchivo2" class="btnSamElim <?php if($opcion == "consulta" && $archivo2 != ""){ echo 'boton2'; } else { echo ' boton5'; } ?>">Eliminar</div>
                <div style="<?php if($opcion != "consulta" || ($opcion == "consulta" && $archivo3 == "")){ echo 'visibility: hidden;'; } ?>" 
                id="elimArchivo3" class="btnSamElim <?php if($opcion == "consulta" && $archivo3 != ""){ echo 'boton2'; } else { echo ' boton5'; } ?>">Eliminar</div>
                <div style="<?php if($opcion != "consulta" || ($opcion == "consulta" && $archivo4 == "")){ echo 'visibility: hidden;'; } ?>" 
                id="elimArchivo4" class="btnSamElim <?php if($opcion == "consulta" && $archivo4 != ""){ echo 'boton2'; } else { echo ' boton5'; } ?>">Eliminar</div>
                <div style="<?php if($opcion != "consulta" || ($opcion == "consulta" && $archivo5 == "")){ echo 'visibility: hidden;'; } ?>" 
                id="elimArchivo5" class="btnSamElim <?php if($opcion == "consulta" && $archivo5 != ""){ echo ' boton2'; } else{ echo ' boton5'; } ?>">Eliminar</div>
            </div>
        </div>
    </div>
    <!--fin datos de los archivos que se va a cargar-->

    <br>

    <hr>

    <!--inicio licencias que se va a agregar, editar o eliminar-->
    <h1 class="textog negro" style="margin:20px; text-align:center;">Licencias</h1>

    <div style="overflow:hidden">
        <div id="borrar" class="botonesSAM <?php if($opcion == "consulta"){ echo 'boton2'; } else { echo ' boton5'; } ?>" style="float:left;">Borrar Licencias</div>
        <div id="descargar" class="botonesSAM boton5" style="float:left;" onclick="window.open('<?= $GLOBALS["domain_root"] ?>/formulario_compras_clientes.xlsx')">Descargar</div>
        <div id="subir" class="botonesSAM <?php if($opcion == "consulta"){ echo 'boton2'; } else { echo ' boton5'; } ?>" style="float:left;">Subir</div>
        <div id="subirOtros" class="botonesSAM <?php if($opcion == "consulta"){ echo 'boton2'; } else{ echo ' boton5'; } ?>" style="float:left;">Subir Otros</div>
        <div id="editar" class="botonesSAM <?php if($opcion == "consulta"){ echo 'boton5'; } else{ echo ' boton2'; } ?>" style="<?php if(!$permModif){ echo 'display:none;'; } ?>">Editar</div>
        <div style="float:right;" id="agregarLicencia" class="botonesSAM <?php if($opcion == "consulta"){ echo 'boton2'; } else{ echo 'boton5'; } ?>" onclick="$('#errorTabla').hide();agrLicencia();">Agregar Licencias</div>

        <br class="both">
        <br>
        <div style="left:400px; position:relative; width:120px; overflow:hidden; background:#7F7F7F;">
            <progress id="barraProgreso" style="clear:both;top:30px; width:100%;" value="0" max="100"></progress>
        </div>
    </div>

    <br>

    <div style="clear: both; overflow:hidden;">
        <div style="max-height:400px; overflow:auto;">
            <table id="detalleContrato" style="width:100%; border:1px solid;">
                <thead style="background-color:#C1C1C1;">
                    <tr>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkAll" name="checkAll" <?php if($opcion == "consulta"){ echo ' disabled'; } ?>></th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Producto</th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Edici&oacute;n</th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Versi&oacute;n</th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">SKU</th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Tipo</th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">Cantidades</th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF;">Precio Unitario</th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF;">Asignaci&oacute;n</th>
                    </tr>
                </thead>
                <tbody id="contenidoTabla">
                    <?php
                    if($opcion == "consulta"){
                        $i = 0;
                        foreach($tablaDetalle as $rowDetalle){
                        ?>    
                            <tr style="border-bottom: 1px solid #ddd" id="<?= $i ?>">
                                <td style="text-align:center;"><input type="checkbox" id="check<?= $i ?>" name="check[]" disabled></td>
                                <td><select id="producto<?= $i ?>" name="producto[]" style="height:30px;" onchange="edicProducto(this.value, <?= $i ?>)" disabled>
                                    <option value = "">--Seleccione--</option>
                                    <?php
                                    foreach ($prodFabricante as $row) {
                                    ?>
                                        <option value="<?= $row["idProducto"] ?>" <?php if($row["idProducto"] == $rowDetalle["idProducto"]){ echo 'selected="selected"'; } ?>><?= $row["nombre"] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select></td>
                                <td style="border-bottom: 1px solid #ddd"><select id="edicion<?= $i ?>" name="edicion[]" onchange="edicProductoVersion(this.value, <?= $i ?>)" style="height:30px;" disabled>
                                    <option value = "">--Seleccione--</option>
                                    <?php
                                    $ediciones = $consultar_contrato->obtenerEdicProducto($idFabAux, $rowDetalle["idProducto"]);
                                    foreach($ediciones as $rowEdiciones){
                                    ?>
                                        <option value = "<?= $rowEdiciones["idEdicion"] ?>" <?php if($rowEdiciones["idEdicion"] == $rowDetalle["idEdicion"]){ echo 'selected="selected"'; } ?>><?= $rowEdiciones["nombre"] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select></td>
                                <td>
                                    <select id="version<?= $i ?>" name="version[]" onchange="validarRegistrosDuplicados(<?= $i ?>)" style="height:30px;" disabled>
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        $versiones = $consultar_contrato->obtenerEdicProductoVersion($idFabAux, $rowDetalle["idProducto"], $rowDetalle["idEdicion"]);
                                        foreach($versiones as $rowVersiones){
                                        ?>    
                                            <option value="<?= $rowVersiones["id"] ?>" <?php if($rowDetalle["version"] == $rowVersiones["id"]){ echo 'selected="selected"'; } ?>><?= $rowVersiones["nombre"] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select></td>
                                <td><input type="text" id="SKU<?= $i ?>" name="SKU[]" style="height:30px;" value="<?= $rowDetalle["sku"] ?>" readonly></td>
                                <td><select id="tipo<?= $i ?>" name="tipo[]" style="height:30px;" disabled>
                                    <option value = "">--Seleccione--</option>
                                    <option value = "perpetuo" <?php if("perpetuo" == $rowDetalle["tipo"]){ echo 'selected="selected"'; } ?>>Perpetuo</option>
                                    <option value = "software assurance" <?php if("software assurance" == $rowDetalle["tipo"]){ echo 'selected="selected"'; } ?>>Software Assurance</option>
                                    <option value = "subscripcion" <?php if("subscripcion" == $rowDetalle["tipo"]){ echo 'selected="selected"'; } ?>>Subscripci&oacute;n</option>
                                    <option value = "otro" <?php if("otro" == $rowDetalle["tipo"]){ echo 'selected="selected"'; } ?>>Otro</option>
                                </select></td>
                                <td><input type="text" id="cantidad<?= $i ?>" name="cantidad[]" style="width:70px; height:30px; text-align:right" maxlength=7 onblur="sumarPrecio()" value="<?= $rowDetalle["cantidad"] ?>" readonly></td>
                                <td><input type="text" id="precio<?= $i ?>" name="precio[]" style="width:70px; height:30px; text-align:right" maxlength=10 onblur="sumarPrecio()" value="<?= $rowDetalle["precio"] ?>" readonly></td>
                                <td>
                                    <select id="asignacion<?= $i ?>" name="asignacion[]" onchange="validarRegistrosDuplicados(<?= $i ?>)" style="height:30px;" disabled>
                                        <option value="">--Seleccione--</option>';
                                        <?php foreach($asignaciones as $row){
                                        ?>
                                            <option value="<?= $row["asignacion"] ?>" <?php if($rowDetalle["asignacion"] == $row["asignacion"]){ echo "selected='selected'"; } ?>><?= $row["asignacion"] ?></option>
                                        <?php
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <?php 
                            $cantidad += $rowDetalle["cantidad"];
                            $precio += round($rowDetalle["cantidad"] * $rowDetalle["precio"], 0);
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <div style="text-align:center">
            <p style="display:inline">Total Cantidad: </p>
            <input type="text" id="totalCantidad" style="display:inline; height:25px; width:100px; text-align:right"  value="<?php if($opcion == "consulta"){ echo $cantidad; } else { echo '0'; } ?>" readonly>
            <p style="display:inline">Total Precio:</p>
            <input type="text" id="totalPrecio" style="display:inline; height:25px; width:100px; text-align:right" value="<?php if($opcion == "consulta"){ echo $precio; } else{ echo '0.00'; } ?>" readonly>
        </div>
        <div class="error_archivo" id="errorTabla" style="display:none; "></div>
    </div>
    <!--fin licencias que se va a agregar, editar o eliminar-->

    <div style="float:right; <?php if(!$permModif){ echo 'display:none;'; } ?>" class="botonesSAM <?php if($opcion == "consulta"){ echo 'boton2'; } else{ echo 'boton5'; } ?>" id="guardarContrato">Guardar</div>
    <div style="float:right;" class="botonesSAM boton5" onclick="location.href='listadoContratos.php';">Atras</div>
</form> 

<form id="subirArchivo" name="subirArchivo" method="post" enctype="multipart/form-data">
    <input type="hidden" id="tokenCargarArchivo" name="token">
    <input type="file" id="archivoCompra" name="archivoCompra" style="display:none" accept=".csv">
    <input type="hidden" id="idFab" name="idFab">
</form>

<form id="subirArchivoOtros" name="subirArchivoOtros" method="post" enctype="multipart/form-data">
    <input type="hidden" id="tokenCargarArchivoOtros" name="token">
    <input type="file" id="archivoCompraOtros" name="archivoCompraOtros" style="display:none" accept=".csv">
    <input type="hidden" id="idFabOtros" name="idFabOtros">
</form>

<div class="modal-personal modal-personal-md" id="modal-campos">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Campos Archivo de <p style="color:#06B6FF; display:inline">Compras</p></h1>
    </div>
    
    <style>
        tr{
            line-height:30px;
        }
    </style>
    <div class="modal-personal-body">
        <table style="width:100%;" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap1">
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Producto: </span></th>
                <td><select id="productoOtros" name="productoOtros"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Edición: </span></th>
                <td><select id="edicionOtros" name="edicionOtros"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Versión: </span></th>
                <td><select id="versionOtros" name="versionOtros"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>SKU: </span></th>
                <td><select id="SKUOtros" name="SKUOtros"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Tipo: </span></th>
                <td><select id="tipoOtros" name="tipoOtros"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Cantidades: </span></th>
                <td><select id="cantidadesOtros" name="cantidadesOtros"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Precio Unitario: </span></th>
                <td><select id="precioUnitarioOtros" name="precioUnitarioOtros"></select></td>
            </tr>
            <tr>
                <th class="text-right bold" style="width:50%;padding-right:10px;"><span>Asignación: </span></th>
                <td><select id="asignacionOtros" name="asignacionOtros"></select></td>
            </tr>
        </table>         
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2" style="float:right;" id="salirListado">Salir</div>
        <div class="boton1 botones_m2" id="procesar" style="float:right;">Procesar</div>
    </div>
</div>

<script>
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var filaLicencias = 0;
    $(document).ready(function(){
        $("#fechaExpiracion").datepicker({changeMonth: true, changeYear: true});
        $("#fechaProxima").datepicker({changeMonth: true, changeYear: true});
        $("#fechaEfectiva").datepicker({changeMonth: true, changeYear: true});
        //$("#detalleContrato").tableHeadFixer();

        $("#checkAll").click(function(){
            if($("#checkAll").prop("checked")){
                for(i=0; i < filaLicencias; i++){
                    if($("#check"+i).length > 0){
                        $("#check"+i).attr("checked", true);
                    }
                }
            }
            else{
                for(i=0; i < filaLicencias; i++){
                    if($("#check"+i).length > 0){
                        $("#check"+i).attr("checked", false);
                    }
                }
            }
        });

        $("#borrar").click(function(){
            if($("#bandOpcion").val() === "consulta"){
                return false;
            }

            for(i=0; i < filaLicencias; i++){
                if($("#check"+i).prop("checked")){
                    $("#"+i).remove();
                }
            }
            $("#checkAll").attr("checked", false);
            sumarCantidad();
            sumarPrecio();
        });

        $("#archivo1").click(function(){
            if(($("#bandOpcion").val() === "agregar" || $("#bandOpcion").val() === "editar") && $("#fileArchivo1").val() === ""){
                $("#cargarArchivo").val("#archivo1");
                $("#fileArchivo1").click();
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual1").val() !== ""){
                window.open("<?= $GLOBALS['domain_root'] ?>/sam/contratos/"+$('#archivoActual1').val(), "_blank");
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual1").val() === ""){
                return false;
            }				
            else{
                $.alert.open('alert', 'Ya cargó el archivo', {'Aceptar' : 'Aceptar'}, function() {
                });
            }
        });

        $("#archivo2").click(function(){
            if(($("#bandOpcion").val() === "agregar" || $("#bandOpcion").val() === "editar") && $("#fileArchivo2").val() === ""){
                $("#cargarArchivo").val("#archivo2");
                $("#fileArchivo2").click();
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual2").val() !== ""){
                window.open("<?= $GLOBALS['domain_root'] ?>/sam/contratos/"+$('#archivoActual2').val(), "_blank");
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual2").val() === ""){
                return false;
            }				
            else{
                $.alert.open('alert', 'Ya cargó el archivo', {'Aceptar' : 'Aceptar'}, function() {
                });
            }
        });

        $("#archivo3").click(function(){
            if(($("#bandOpcion").val() === "agregar" || $("#bandOpcion").val() === "editar") && $("#fileArchivo3").val() === ""){
                $("#cargarArchivo").val("#archivo3");
                $("#fileArchivo3").click();
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual3").val() !== ""){
                window.open("<?= $GLOBALS['domain_root'] ?>/sam/contratos/"+$('#archivoActual3').val(), "_blank");
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual3").val() === ""){
                return false;
            }				
            else{
                $.alert.open('alert', 'Ya cargó el archivo', {'Aceptar' : 'Aceptar'}, function() {
                });
            }
        });

        $("#archivo4").click(function(){
            if(($("#bandOpcion").val() === "agregar" || $("#bandOpcion").val() === "editar") && $("#fileArchivo4").val() === ""){
                $("#cargarArchivo").val("#archivo4");
                $("#fileArchivo4").click();
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual4").val() !== ""){
                window.open("<?= $GLOBALS['domain_root'] ?>/sam/contratos/"+$('#archivoActual4').val(), "_blank");
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual4").val() === ""){
                return false;
            }
            else{
                $.alert.open('alert', 'Ya cargó el archivo', {'Aceptar' : 'Aceptar'}, function() {
                });
            }
        });

        $("#archivo5").click(function(){
            if(($("#bandOpcion").val() === "agregar" || $("#bandOpcion").val() === "editar") && $("#fileArchivo5").val() === ""){
                $("#cargarArchivo").val("#archivo5");
                $("#fileArchivo5").click();
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual5").val() !== ""){
                window.open("<?= $GLOBALS['domain_root'] ?>/sam/contratos/"+$('#archivoActual5').val(), "_blank");
            }
            else if($("#bandOpcion").val() === "consulta" && $("#archivoActual5").val() === ""){
                return false;
            }				
            else{
                $.alert.open('alert', 'Ya cargó el archivo', {'Aceptar' : 'Aceptar'}, function() {
                });
            }
        });

        $("#fileArchivo1").change(function(e){
            if (msie > 0){
                $("#archivo1").empty();
                $("#archivo1").append("Cargado");
                $("#elimArchivo1").css("visibility","");
            }else{
                if(addArchivo(e) === true){
                    $("#elimArchivo1").css("visibility","");
                }
                else{
                    $("#fileArchivo1").val("");
                    $("#fileArchivo1").replaceWith($("#fileArchivo1").clone(true));
                }	
            }
        });

        $("#fileArchivo2").change(function(e){
            if (msie > 0){
                $("#archivo2").empty();
                $("#archivo2").append("Cargado");
                $("#elimArchivo2").css("visibility","");
            }else{
                if(addArchivo(e) === true){
                    $("#elimArchivo2").css("visibility","");
                }
                else{
                    $("#fileArchivo2").val("");
                    $("#fileArchivo2").replaceWith($("#fileArchivo2").clone(true));
                }
            }
        });

        $("#fileArchivo3").change(function(e){
            if (msie > 0){
                $("#archivo3").empty();
                $("#archivo3").append("Cargado");
                $("#elimArchivo3").css("visibility","");
            }else{
                if(addArchivo(e) === true){
                    $("#elimArchivo3").css("visibility","");
                }
                else{
                    $("#fileArchivo3").val("");
                    $("#fileArchivo3").replaceWith($("#fileArchivo3").clone(true));
                }
            }
        });

        $("#fileArchivo4").change(function(e){
            if (msie > 0){
                $("#archivo4").empty();
                $("#archivo4").append("Cargado");
                $("#elimArchivo4").css("visibility","");
            }else{
                if(addArchivo(e) === true){
                    $("#elimArchivo4").css("visibility","");
                }
                else{
                    $("#fileArchivo4").val("");
                    $("#fileArchivo4").replaceWith($("#fileArchivo4").clone(true));
                }
            }
        });

        $("#fileArchivo5").change(function(e){
            if (msie > 0){
                $("#archivo5").empty();
                $("#archivo5").append("Cargado");
                $("#elimArchivo5").css("visibility","");
            }else{
                if(addArchivo(e) === true){
                    $("#elimArchivo5").css("visibility","");
                }
                else{
                    $("#fileArchivo5").val("");
                    $("#fileArchivo5").replaceWith($("#fileArchivo5").clone(true));
                }
            }
        });

        $("#editar").click(function(){
            if($("#bandOpcion").val() === "agregar"){
                return false;
            }
            $("#bandOpcion").val("editar");
            $("#editar").removeClass("boton5").addClass("boton2");
            $("#subir").removeClass("boton2").addClass("boton5");
            $("#subirOtros").removeClass("boton2").addClass("boton5");
            $("#agregarLicencia").removeClass("boton2").addClass("boton5");
            $("#borrar").removeClass("boton2").addClass("boton5");
            $("#guardarContrato").removeClass("boton2").addClass("boton5");
            $("#elimArchivo1").removeClass("boton2").addClass("boton5");
            $("#elimArchivo2").removeClass("boton2").addClass("boton5");
            $("#elimArchivo3").removeClass("boton2").addClass("boton5");
            $("#elimArchivo4").removeClass("boton2").addClass("boton5");
            $("#elimArchivo5").removeClass("boton2").addClass("boton5");
            $("#fabricante").removeAttr("disabled");
            $("#numContrato").removeAttr("readonly");
            $("#tipoContrato").removeAttr("readonly");
            $("#comentario").removeAttr("readonly");
            $("#subsidiaria").removeAttr("readonly");
            $("#proveedor").removeAttr("readonly");
            $("#checkAll").removeAttr("disabled");
            for(i=0; i < $("#totalRegistros").val(); i++){
                $("#"+i).removeAttr("disabled");
                $("#check"+i).removeAttr("disabled");
                $("#producto"+i).removeAttr("disabled");
                $("#edicion"+i).removeAttr("disabled");
                $("#version"+i).removeAttr("disabled");
                $("#SKU"+i).removeAttr("readonly");
                $("#tipo"+i).removeAttr("disabled");
                $("#cantidad"+i).removeAttr("readonly");
                $("#precio"+i).removeAttr("readonly");
                $("#asignacion"+i).removeAttr("disabled");
            }
            filaLicencias = $("#contenidoTabla tr").length;
        });

        $("#fabricante").change(function(e){
            $("#fechaExpiracion").val("");
            $("#numContrato").val("");
            $("#fechaProxima").val("");
            $("#tipoContrato").val("");
            $("#comentario").val("");
            $("#subsidiaria").val("");
            $("#fechaEfectiva").val("");

            $("#archivo1").empty().append("Archivo 1");
            $("#archivo2").empty().append("Archivo 2");
            $("#archivo3").empty().append("Archivo 3");
            $("#archivo4").empty().append("Archivo 4");
            $("#archivo5").empty().append("Archivo 5");

            $("#fileArchivo1").replaceWith($("#fileArchivo1").clone(true));
            $("#fileArchivo2").replaceWith($("#fileArchivo2").clone(true));
            $("#fileArchivo3").replaceWith($("#fileArchivo3").clone(true));
            $("#fileArchivo4").replaceWith($("#fileArchivo4").clone(true));
            $("#fileArchivo5").replaceWith($("#fileArchivo5").clone(true));

            $("#contenidoTabla").empty();
            $("#totalRegistros").val(0);
            $("#totalFilaTabla").val(0);
            $("#totalCantidad").val(0);
            $("#totalPrecio").val("0.00");
        });

        $("#elimArchivo1").click(function(){
            if($("#bandOpcion").val() === "consulta"){
                return false;
            }
            $("#archivo1").empty().append("Archivo 1");
            $("#fileArchivo1").val("");
            $("#fileArchivo1").replaceWith($("#fileArchivo1").clone(true));
            $("#elimArchivo1").css("visibility", "hidden");
            $("#bandElimArchivo1").val("true");
        });

        $("#elimArchivo2").click(function(){
            if($("#bandOpcion").val() === "consulta"){
                return false;
            }
            $("#archivo2").empty().append("Archivo 2");
            $("#fileArchivo2").val("");
            $("#fileArchivo2").replaceWith($("#fileArchivo2").clone(true));
            $("#elimArchivo2").css("visibility", "hidden");
            $("#bandElimArchivo2").val("true");
        });

        $("#elimArchivo3").click(function(){
            if($("#bandOpcion").val() === "consulta"){
                return false;
            }
            $("#archivo3").empty().append("Archivo 3");
            $("#fileArchivo3").val("");
            $("#fileArchivo3").replaceWith($("#fileArchivo3").clone(true));
            $("#elimArchivo3").css("visibility", "hidden");
            $("#bandElimArchivo3").val("true");
        });

        $("#elimArchivo4").click(function(){
            if($("#bandOpcion").val() === "consulta"){
                return false;
            }
            $("#archivo4").empty().append("Archivo 4");
            $("#fileArchivo4").val("");
            $("#fileArchivo4").replaceWith($("#fileArchivo4").clone(true));
            $("#elimArchivo4").css("visibility", "hidden");
            $("#bandElimArchivo4").val("true");
        });

        $("#elimArchivo5").click(function(){
            if($("#bandOpcion").val() === "consulta"){
                return false;
            }
            $("#archivo5").empty().append("Archivo 5");
            $("#fileArchivo5").val("");
            $("#fileArchivo5").replaceWith($("#fileArchivo5").clone(true));
            $("#elimArchivo5").css("visibility", "hidden");
            $("#bandElimArchivo5").val("true");
        });

        $("#guardarContrato").click(function(){
            if($("#bandOpcion").val() === "consulta"){
                return false;
            }

            if($("#guardarContrato").hasClass("boton2")){
                return false;
            }

            $("#errorTabla").hide();
            if($("#fabricante").val() === ""){
                $("#errorFabricante").show();
                $("#fabricante").focus();
                $("#cargar").hide();
                return false;
            }

            if($("#numContrato").val() === ""){
                $("#errorNumContrato").show();
                $("#numContrato").focus();
                $("#cargar").hide();
                return false;
            }

            if($("#tipoContrato").val() === ""){
                $("#errorTipoContrato").show();
                $("#tipoContrato").focus();
                $("#cargar").hide();
                return false;
            }

            if($("#fechaEfectiva").val() === ""){
                $("#errorFechaEfectiva").show();
                $("#fechaEfectiva").focus();
                $("#cargar").hide();
                return false;
            }

            if($("#fechaExpiracion").val() === ""){
                $("#errorFechaExpiracion").empty();
                $("#errorFechaExpiracion").append("Debe seleccionar la fecha de expiración");
                $("#errorFechaExpiracion").show();
                $("#fechaExpiracion").focus();
                $("#cargar").hide();
                return false;
            }
            
            arrayFechaExp = $("#fechaExpiracion").val().split("/");
            arrayFechaEfe = $("#fechaEfectiva").val().split("/");
            fechaExp = new Date(arrayFechaExp[2], arrayFechaExp[1], arrayFechaExp[0]);
            fechaEfe = new Date(arrayFechaEfe[2], arrayFechaEfe[1], arrayFechaEfe[0]);

            if(fechaExp <= fechaEfe){
                $("#errorFechaExpiracion").empty();
                $("#errorFechaExpiracion").append("La fecha de expiración debe ser mayor que la efectiva");
                $("#errorFechaExpiracion").show();
                $("#fechaExpiracion").focus();
                $("#cargar").hide();
                return false;
            }

            if($("#fechaProxima").val() === ""){
                $("#errorFechaProxima").show();
                $("#fechaProxima").focus();
                $("#cargar").hide();
                return false;
            }
            
            if($("#subsidiaria").val() === ""){
                $("#errorSubsidiaria").show();
                $("#subsidiaria").focus();
                $("#cargar").hide();
                return false;
            }

            if($("#contenidoTabla tr").length === 0){
                $("#errorTabla").empty();
                $("#errorTabla").append("Debe agregar al menos un registro");
                $("#errorTabla").show();
                $("#cargar").hide();
                return false;
            }

            error = 0;
            for(i=0; i < filaLicencias; i++){
                if($("#check"+i).length > 0){
                    if($("#producto" + i).val() === ""){
                        //error = 1;
                        $.alert.open('alert', 'Existen registros sin producto', {'Aceptar' : 'Aceptar'}, function() {
                            $("#producto" + i).val("");
                            $("#producto" + i).focus();
                        });
                        return false;
                    }
                    if($("#edicion" + i).val() === ""){
                        //error = 2;
                        $.alert.open('alert', 'Existen registros sin edición', {'Aceptar' : 'Aceptar'}, function() {
                            $("#edicion" + i).val("");
                            $("#edicion" + i).focus();
                        });
                        return false;
                    }
                    if($("#version" + i).val() === ""){
                        //error = 3;
                        $.alert.open('alert', 'Existen registros sin versión', {'Aceptar' : 'Aceptar'}, function() {
                            $("#version" + i).val("");
                            $("#version" + i).focus();
                        });
                        return false;
                    }
                    if($("#SKU" + i).val() === ""){
                        //error = 4;
                        $.alert.open('alert', 'Existen registros sin SKU', {'Aceptar' : 'Aceptar'}, function() {
                            $("#SKU" + i).val("");
                            $("#SKU" + i).focus();
                        });
                        return false;
                    }
                    if($("#tipo" + i).val() === ""){
                        //error = 5;
                        $.alert.open('alert', 'Existen registros sin tipo', {'Aceptar' : 'Aceptar'}, function() {
                            $("#tipo" + i).val("");
                            $("#tipo" + i).focus();
                        });
                        return false;
                    }if($("#cantidad" + i).val() === "" || parseInt($("#cantidad" + i).val()) === 0){
                        //error = 6;
                        $.alert.open('alert', 'Existen registros sin cantidad', {'Aceptar' : 'Aceptar'}, function() {
                            $("#cantidad" + i).val("");
                            $("#cantidad" + i).focus();
                        });
                        return false;
                    }if($("#precio" + i).val() === "" || parseInt($("#precio" + i).val()) === 0){
                        //error = 7;
                        $.alert.open('alert', 'Existen registros sin precio', {'Aceptar' : 'Aceptar'}, function() {
                            $("#precio" + i).val("");
                            $("#precio" + i).focus();
                        });
                        return false;
                    }
                    if($("#asignacion" + i).val() === ""){
                        //error = 8;
                        $.alert.open('alert', 'Existen registros sin asignación', {'Aceptar' : 'Aceptar'}, function() {
                            $("#asignacion" + i).val("");
                            $("#asignacion" + i).focus();
                        });
                        return false;
                    }
                }
            }

            $("#totalRegistros").val(filaLicencias);
            $("#totalFilaTabla").val($("#contenidoTabla tr").length);
            //guardarContrato();

            $("#guardarContrato").removeClass("boton5").addClass("boton2");
            $("#fondo").show();
            $("html, body").scrollTop(0);

            if (msie > 0) // If Internet Explorer, return version number
            {
                $("#formContratoSAM").submit();
                setTimeout(function(){
                    $("#contenedorCentral").empty();
                    $.post("<?= $GLOBALS['domain_root'] ?>/sam/compras.php", { token : localStorage.licensingassuranceToken }, function(data){
                        $("#contenedorCentral").append(data[0].div);
                    }, "json");
                }, 5000);
            }
            else  // If another browser, return 0
            {
                $("#token").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#formContratoSAM")[0]);	
                $.ajax({
                    type: "POST",
                    url: "<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/guardarContratoAjax.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        $("#fondo").hide();
                        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                        result = data[0].result;
                        if(result === 0){
                            $.alert.open("alert", "No se guardó el contrato", {"Aceptar" : "Aceptar"}, function() {
                                location.href="listadoContratos.php";
                            });
                        }
                        else if(result === 1){
                            $.alert.open("info", "Se guardó el contrato con éxito", {"Aceptar" : "Aceptar"}, function() {
                                location.href="listadoContratos.php";
                            });
                        }
                        else if(result === 2){
                            $.alert.open("alert", "No se guardó el detalle del contrato", {"Aceptar" : "Aceptar"}, function() {
                                location.href="listadoContratos.php";
                            });
                        }
                        else{
                            $.alert.open("alert", "Se guardó parcialmente el detalle del contrato", {"Aceptar" : "Aceptar"}, function() {
                                location.href="listadoContratos.php";
                            });
                        }
                    }
                })
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        $("#guardarContrato").removeClass("boton2").addClass("boton5");
                    });
                });
            }
        });

        $("#subir").click(function(){
            $("#idFab").val($("#fabricante").val());
            if($("#subir").hasClass("boton2")){
                return false;
            }
            if($("#idFab").val() === ""){
                alert("Debe seleccionar el fabricante");
                $("#fabricante").focus();
                $("html, body").scrollTop(0);
                return false;
            }
            $("#archivoCompra").click();
        });

        $("#subirOtros").click(function(){
            $("#idFab").val($("#fabricante").val());
            if($("#subir").hasClass("boton2")){
                return false;
            }
            if($("#idFab").val() === ""){
                alert("Debe seleccionar el fabricante");
                $("#fabricante").focus();
                $("html, body").scrollTop(0);
                return false;
            }
            $("#archivoCompraOtros").click();
            $("#contenidoTabla").empty();
        });

        $("#archivoCompra").change(function(e){
            if(addArchivoCompra(e) === false){
                $("#archivoCompra").val("");
                $("#archivoCompra").replaceWith($("#archivoCompra").clone(true));
            }
            else{
                $("html, body").scrollTop(0);
                $("#fondo").show();
                $("#tokenCargarArchivo").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#subirArchivo")[0]);
                $("#contenidoTabla").empty();
                filaLicencias = 0;
                $("#totalCantidad").val(0);
                $("#totalPrecio").val(0);
                $.ajax({
                    type: "POST",
                    url: "<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/cargarArchivoCompra.php", 
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",  
                    cache:false,
                    success: function(data){
                        $("#fondo").hide();
                        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                
                        result = data[0].result;
                        if(result === 0){
                            $.alert.open("warning", "Alert", "El archivo no se pudo cargar", {"Aceptar" : "Aceptar"});
                        }
                        else if(result === 1){
                            $("#contenidoTabla").append(data[0].row);
                            filaLicencias = data[0].total;
                            sumarPrecio();
                        }
                        else if(result === 2){
                            $.alert.open("warning", "Alert", "No se pudo guardar el archivo", {"Aceptar" : "Aceptar"});
                        }
                        else if(result === 3){
                            $.alert.open("warning", "Alert", "La cabecera del archivo CSV no es compatible", {"Aceptar" : "Aceptar"});
                        }
                        else if(result === 4){
                            $.alert.open("warning", "Alert", "Los separadores válidos para archivo CSV son \",\" o \";\"", {"Aceptar" : "Aceptar"});
                        }
                    }
                })
                .fail(function( jqXHR ){
                    $("#fondo").hide();
                    $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    });
                });
                $("#archivoCompra").val("");
                $("#archivoCompra").replaceWith($("#archivoCompra").clone(true));
            }
        });

        $("#archivoCompraOtros").change(function(e){
            if(addArchivoCompra(e) === false){
                $("#archivoCompraOtros").val("");
                $("#archivoCompraOtros").replaceWith($("#archivoCompraOtros").clone(true));
            }
            else{
                $("#tokenCargarArchivoOtros").val(localStorage.licensingassuranceToken);
                var formData = new FormData($("#subirArchivoOtros")[0]);	
                var ajax = new XMLHttpRequest();
                ajax.upload.addEventListener("progress", progressHandler, true);
                ajax.addEventListener("load", completeHandler, false);
                ajax.addEventListener("error", errorHandler, false);
                ajax.addEventListener("abort", abortHandler, false);
                ajax.open("POST", "<?= $GLOBALS["domain_root"] ?>/sam/repositorios/ajax/subirArchivoCompras.php", true);
                ajax.send(formData);
            }
        });
        
        $("#salirListado").click(function(){
            $("#cargarArchivoOtros").val("");
            $("#productoOtros").empty();
            $("#edicionOtros").empty();
            $("#versionOtros").empty();
            $("#SKUOtros").empty();
            $("#tipoOtros").empty();
            $("#cantidadesOtros").empty();
            $("#precioUnitarioOtros").empty();
            $("#asignacionOtros").empty();
            $("#barraProgreso").val(0);
            $("#fondo1").hide();
            $("#modal-campos").hide();
            $("#archivoCompraOtros").val("");
            $("#archivoCompraOtros").replaceWith($("#archivoCompraOtros").clone(true));
            $("#totalCantidad").val(0);
            $("#totalPrecio").val("0.00");
        });
        
        $("#procesar").click(function(){
            $("#fondo1").hide();
            $("#modal-campos").hide();
            $("#fondo").show();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/cargarArchivoCompraOtros.php", { fabricante: $("#fabricante").val(), 
            productoOtros : $("#productoOtros").val(), edicionOtros : $("#edicionOtros").val(), versionOtros : $("#versionOtros").val(),
            SKUOtros : $("#SKUOtros").val(), tipoOtros : $("#tipoOtros").val(), cantidadesOtros : $("#cantidadesOtros").val(),
            precioUnitarioOtros : $("#precioUnitarioOtros").val(), asignacionOtros : $("#asignacionOtros").val(), 
            archivo : $("#cargarArchivoOtros").val(), token : localStorage.licensingassuranceToken }, function (data) {
                $("#fondo").hide();
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                
                $("#contenidoTabla").append(data[0].row);
                filaLicencias = data[0].total;
                sumarPrecio();
                
                $("#salirListado").click();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });
    });

    function progressHandler(e){
        var porcentaje = (e.loaded / e.total) * 100;
        $("#barraProgreso").val(Math.round(porcentaje)); 
    }

    function completeHandler(e){
        var data = [{resultado:true, sesion:true}];
        $("#barraProgreso").val(100);
        res = e.target.responseText.split("*");
        
        if(parseInt(res[0]) === 0){
            data[0].sesion = false;
        } else if(parseInt(e.target.responseText) === 2){
            data[0].resultado = false;
        }
        
        <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
        
        if(parseInt(res[0]) === 1){
            $("#cargarArchivoOtros").val(res[2]);
            $("#productoOtros").append(res[1]);
            $("#edicionOtros").append(res[1]);
            $("#versionOtros").append(res[1]);
            $("#SKUOtros").append(res[1]);
            $("#tipoOtros").append(res[1]);
            $("#cantidadesOtros").append(res[1]);
            $("#precioUnitarioOtros").append(res[1]);
            $("#precioUnitarioOtros").append("<option value='N/A'>N/A</option>");
            $("#asignacionOtros").append(res[1]);
            $("#fondo1").show();
            $("#modal-campos").show();
            
        } else if(parseInt(res[0]) === 3){
            $("#barraProgreso").val(0);
            $.alert.open('alert', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 4){
            $("#barraProgreso").val(0);
            $.alert.open('alert', "Debe seleccionar el archivo antes de subirlo", {'Aceptar' : 'Aceptar'}, function() {
            });
        } else if(parseInt(res[0]) === 5){
            $("#barraProgreso").val(0);
            $.alert.open('alert', "Los separadores válidos para archivo CSV son \",\" o \";\"", {'Aceptar' : 'Aceptar'}, function() {
            });
        }    
    }

    function errorHandler(){
        $.alert.open('error', "No se cargó el archivo", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function abortHandler(){
        $.alert.open('alert', "Carga del archivo abortada", {'Aceptar' : 'Aceptar'}, function() {
        });
    }

    function addArchivo(e){
        file = e.target.files[0]; 
        if (!file.type.match("application/pdf")){
            $.alert.open('alert', "El archivo a cargar debe ser .pdf", {'Aceptar' : 'Aceptar'}, function() {
            });
            return false;
        }
        else if(parseInt(file.size) / 1024 > 5120 ){
            $.alert.open('alert', "El tamaño permitido es de 5 MB", {'Aceptar' : 'Aceptar'}, function() {
            });
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $($("#cargarArchivo").val()).empty();
            $($("#cargarArchivo").val()).append(file.name);
            return true;
        }
    }

    function addArchivoCompra(e){
        file = e.target.files[0]; 
        if (!file.type.match("application/vnd.ms-excel")){
            $.alert.open('alert', "El archivo a cargar debe ser .csv", {'Aceptar' : 'Aceptar'}, function() {
            });
            return false;
        }
        if(parseInt(file.size) / 1024 > 5120 ){
            $.alert.open('alert', "El tamaño permitido es de 5 MB", {'Aceptar' : 'Aceptar'}, function() {
            });
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $($("#cargarArchivo").val()).empty();
            $($("#cargarArchivo").val()).append(file.name);
            return true;
        }
    }

    function fileOnload(e) {
        result=e.target.result;
    }

    function sumarCantidad(){
        cant = 0;
        for(i=0; i < filaLicencias; i++){
            if($("#check"+i).length > 0 && $("#cantidad" + i).val() !== ""){
                cant += parseInt($("#cantidad" + i).val()); 
            }
        }
        $("#totalCantidad").val(cant);
    }

    function sumarPrecio(){
        prec = 0;
        cant = 0;
        for(i=0; i <= filaLicencias; i++){
            if($("#check"+i).length > 0 && $("#precio" + i).val() !== ""){
                cant += parseInt($("#cantidad" + i).val()); 
                prec += parseInt($("#cantidad" + i).val()) * parseFloat($("#precio" + i).val()); 
            }
        }
        $("#totalCantidad").val(cant);
        $("#totalPrecio").val(prec.toFixed(2));
    }
    
    function agrLicencia() {
        if ($("#fabricante").val() === "") {
            $("#errorFabricante").show();
            $("#fabricante").focus();
            return false;
        }

        if ($("#agregarLicencia").hasClass("boton2")) {
            return false;
        }
        
        desactivarBtn++;
        if(desactivarBtn === 1){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/repositorios/ajax/agrLicencias.php", { fabricante: $("#fabricante").val(), fila: filaLicencias, token : localStorage.licensingassuranceToken }, function (data) {
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#contenidoTabla").append(data[0].row);
                filaLicencias += 1;
                desactivarBtn = 0;
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    desactivarBtn = 0;
                });
            });
        }
    }
    
    function edicProducto(producto, fila) {
        if(validarRegistrosDuplicados(fila) === false){
            return false;
        }
        
        desactivarBtn++;
        if(desactivarBtn === 1){
            $("#edicion" + fila).empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/ajax/edicionProducto.php", { fabricante: $("#fabricante").val(), producto: producto, token : localStorage.licensingassuranceToken }, function (data) {
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                
                $("#edicion" + fila).append(data[0].option);
                desactivarBtn = 0;
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    desactivarBtn = 0;
                });
            });
        }
    }

    function edicProductoVersion(edicion, fila) {
        if(validarRegistrosDuplicados(fila) === false){
            return false;
        }
        
        desactivarBtn++;
        if(desactivarBtn === 1){
            $("#version" + fila).empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/ajax/edicionProductoVersion.php", { fabricante: $("#fabricante").val(), producto: $("#producto" + fila).val(), edicion: edicion, token : localStorage.licensingassuranceToken }, function (data) {
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#version" + fila).append(data[0].option);
                desactivarBtn = 0;
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    desactivarBtn = 0;
                });
            });
        }
    }
    
    function validarRegistrosDuplicados(i){
        for(index = 0; index < filaLicencias; index++){
            if(index !== i && parseInt($("#producto" + i).val()) === parseInt($("#producto" + index).val())
            && parseInt($("#edicion" + i).val()) === parseInt($("#edicion" + index).val())
            && parseInt($("#version" + i).val()) === parseInt($("#version" + index).val())
            && $("#asignacion" + i).val() === $("#asignacion" + index).val()){
                $.alert.open("warning", "Alerta", "Ya se registro este producto", {"Aceptar" : "Aceptar"}, function(){
                    $("#asignacion" + i).val("");
                    $("#asignacion" + i).focus();
                });
                return false;
            }
        }
    }
</script>