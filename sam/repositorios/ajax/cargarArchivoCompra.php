<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_productos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_ediciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_versiones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalenciasPDO.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        $error = 0;
        $result = 0;
        $filaLicencia = 0;
        $fila   = "";
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $fabricante = 0;
            if(isset($_POST["idFab"]) && filter_var($_POST["idFab"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["idFab"];
            }
            
            if(is_uploaded_file($_FILES["archivoCompra"]["tmp_name"])){
                $temporal_archive = $_FILES['archivoCompra']['tmp_name'];
                if($general->obtenerSeparadorUniversal($temporal_archive, 1, 7) === true){
                    if (($fichero = fopen($temporal_archive, "r")) !== FALSE) {
                        $i = 0;
                        while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                            if($i == 0 && ($datos[0] != "Producto" || utf8_encode($datos[1]) != "Edición" || utf8_encode($datos[2]) != "Versión" || 
                            $datos[3] != "SKU" || $datos[4] != "Tipo" || $datos[5] != "Cantidades" || $datos[6] != "Precio Unitario" ||
                            utf8_encode($datos[7]) != "Asignación")){
                                $result = 3;
                                break;
                            } else{
                                break;
                            }
                            $i++;
                        }
                    }
                } else{
                    $result =  4;
                }

                if($result == 0){
                    $archivo = $GLOBALS['app_root'] . "/sam/compras/" . $_SESSION["client_id"] . "_formularioCompra" . date("dmYHis") . ".csv";
                    if (move_uploaded_file($_FILES['archivoCompra']['tmp_name'], $archivo)) {
                        /*$general = new General();
                        $fabricante = 3;
                        $archivo = $GLOBALS['app_root'] . "/sam/compras/10_formularioCompra11082017062047.csv";*/
                        $nueva_funcion  = new funcionesSam();
                        $nuevo_producto = new Productos();
                        $nuevo_edicion  = new Ediciones();
                        $nuevo_version  = new Versiones();
                        $equivalencia   = new EquivalenciasPDO();
                        
                        $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

                        if(file_exists($archivo)){
                            if($general->obtenerSeparadorUniversal($archivo, 1, 8) === true){
                                if (($fichero = fopen($archivo, "r")) !== FALSE) {
                                    $filaLicencia = 0;
                                    $i = 0;
                                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                                        if($i > 0){
                                            $idProducto = 0;
                                            if($datos[3] != "" && $datos[4] != "" && $datos[5] != "" && $datos[6] != "" && $datos[7] != ""){

                                                $arrayProducto = $nuevo_producto->getProductoNombre(utf8_encode(trim($datos[0])));
                                                $idProducto    = $arrayProducto["idProducto"];                          

                                                $edicionAux    = utf8_encode(trim($datos[1]));
                                                if(ord($datos[1]) == 160 || empty($edicionAux)){
                                                    $edicionAux = "";
                                                }

                                                $arrayEdicion = $nuevo_edicion->getEdicionNombre($edicionAux);
                                                $idEdicion    = $arrayEdicion["idEdicion"];

                                                $versionAux    = utf8_encode(trim($datos[2]));
                                                if(ord($datos[2]) == 160 || empty($versionAux)){
                                                    $versionAux = "";
                                                }

                                                $arrayVersion = $nuevo_version->getVersionNombre($versionAux);
                                                $idVersion    = $arrayVersion["id"];

                                                if($fabricante == 10){
                                                    $fabricante = 3;
                                                }
                                                $prodFabricante    = $nueva_funcion->obtenerProdFabricante($fabricante);
                                                $edicionesProducto = $equivalencia->edicionesProducto($fabricante, $idProducto);
                                                $versionEdicion    = $equivalencia->versionesEdicion($fabricante, $idProducto, $idEdicion);
                                                
                                                if($general->validarAsignacion($_SESSION["client_id"], $_SESSION["client_empleado"], utf8_encode($datos[7]))){
                                                    $fila .= '<tr style="border-bottom-style: 1px solid #ddd" id="'.$filaLicencia.'">
                                                        <td style="text-align:center;"><input type="checkbox" id="check'.$filaLicencia.'" name="check[]"></td>
                                                        <td><select id="producto'.$filaLicencia.'" name="producto[]" style="height:30px;" onchange="edicProducto(this.value, '.$filaLicencia.')">
                                                            <option value = "">--Seleccione--</option>';
                                                            foreach ($prodFabricante as $row) {
                                                                $fila .=  '<option value="' . $row["idProducto"] . '" ';
                                                                if($row["idProducto"] == $idProducto){
                                                                    $fila .= 'selected="selected"';
                                                                }
                                                                //$fila .= '>' . utf8_decode($row["nombre"]) . '</option>';
                                                                $fila .= '>' . $row["nombre"] . '</option>';
                                                            }
                                                            $fila .= '
                                                        </select></td>
                                                        <td style="border-bottom-style: 1px solid #ddd"><select id="edicion'.$filaLicencia.'" name="edicion[]" onchange="edicProductoVersion(this.value, ' . $filaLicencia . ')" style="height:30px;">
                                                            <option value = "">--Seleccione--</option>';
                                                            foreach ($edicionesProducto as $row) {
                                                                $fila .=  '<option value="' . $row["idEdicion"] . '" ';
                                                                if($row["idEdicion"] == $idEdicion){
                                                                    $fila .= 'selected="selected"';
                                                                }
                                                                //$fila .= '>' . utf8_decode($row["nombre"]) . '</option>';
                                                                $fila .= '>' . $row["nombre"] . '</option>';
                                                            }
                                                            $fila .= '</select></td>
                                                        <td>
                                                            <select id="version'.$filaLicencia.'" name="version[]" style="height:30px;">
                                                                <option value="">--Seleccione--</option>';
                                                                foreach ($versionEdicion as $row) {
                                                                    $fila .=  '<option value="' . $row["id"] . '" ';
                                                                    if($row["id"] == $idVersion){
                                                                        $fila .= 'selected="selected"';
                                                                    }
                                                                    //$fila .= '>' . utf8_decode($row["nombre"]) . '</option>';
                                                                    $fila .= '>' . $row["nombre"] . '</option>';
                                                                }
                                                                $fila .= '
                                                            </select>
                                                        </td>
                                                        <td><input type="text" id="SKU'.$filaLicencia.'" name="SKU[]" style="height:30px;" value="' . $datos[3] . '"></td>';

                                                        if(utf8_encode($datos[4]) == "Subscripción"){
                                                            $datos[4] = "subscripcion";
                                                        } else if(utf8_encode($datos[4]) == "Software Assurance" && $fabricante == 3){
                                                            $datos[4] = "software assurance";
                                                        } else if($datos[4] == "Perpetuo"){
                                                            $datos[4] = "perpetuo";
                                                        } else{
                                                            $datos[4] = "otro";
                                                        }

                                                        $fila .= '<td><select id="tipo'.$filaLicencia.'" name="tipo[]" style="height:30px;">
                                                            <option value = "">--Seleccione--</option>
                                                            <option value = "perpetuo"';
                                                                if($datos[4] == "perpetuo"){
                                                                    $fila .= ' selected="selected"';  
                                                                }
                                                            $fila .= '>Perpetuo</option>';
                                                            if($fabricante == 3){
                                                                $fila .= '<option value = "software assurance"';
                                                                    if($datos[4] == "software assurance"){
                                                                        $fila .= ' selected="selected"';  
                                                                    }
                                                                $fila .= '>Software Assurance</option>';
                                                            }
                                                            $fila .= '<option value = "subscripcion"';
                                                                if($datos[4] == "subscripcion"){
                                                                    $fila .= ' selected="selected"';  
                                                                }
                                                            $fila .= '>Subscripci&oacute;n</option>
                                                            <option value = "otro"';
                                                                if($datos[4] == "otro"){
                                                                    $fila .= ' selected="selected"';  
                                                                }
                                                            $fila .= '>Otro</option>
                                                        </select></td>
                                                        <td><input type="text" id="cantidad'.$filaLicencia.'" name="cantidad[]" style="width:70px; height:30px; text-align:right" maxlength=4 value="' . $datos[5] . '" onblur="sumarCantidad()"></td>
                                                        <td><input type="text" id="precio'.$filaLicencia.'" name="precio[]" style="width:70px; height:30px; text-align:right" maxlength=10 value="' . $datos[6] . '" onblur="sumarPrecio()"></td>
                                                        <td>
                                                            <select id="asignacion'.$filaLicencia.'" name="asignacion[]" style="height:30px;">
                                                                <option value="">--Seleccione--</option>';
                                                                foreach($asignaciones as $row){
                                                                    $fila .= '<option value="' . $row["asignacion"] . '"';
                                                                    if(utf8_encode($datos[7]) == $row["asignacion"]){ 
                                                                        $fila .= "selected='selected'"; 
                                                                    } 
                                                                    $fila .= '>' . $row["asignacion"] . '</option>';
                                                                } 
                                                            $fila .= '</select>
                                                        </td>
                                                    </tr>';
                                                    $filaLicencia++;
                                                }
                                            }
                                        }
                                        $i++;
                                    }
                                }
                                $result = 1;
                            }
                        }
                    }
                    else{
                        $result = 2;
                    }
                }
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'row'=>$fila, 'result'=>$result, 
            'total'=>$filaLicencia, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);