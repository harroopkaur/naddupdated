<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/repositorio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
} 

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }
    
    if($verifSesion[0]){
        $_SESSION['client_tiempo'] = $verifSesion[1];
        //if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            if(isset($_FILES["archivoCompraOtros"]) && is_uploaded_file($_FILES["archivoCompraOtros"]["tmp_name"])){	
                $archivoTemp = $_FILES["archivoCompraOtros"]["tmp_name"];
                $tipoArchivo = $_FILES["archivoCompraOtros"]["type"];
                $tamano = $_FILES["archivoCompraOtros"]["size"];
                $mensajeError = $_FILES["archivoCompraOtros"]["error"];
                
                $nombreArchivo = $_SESSION["client_id"]."_formularioCompra".date("dmYHis") . ".csv";
                
                if(move_uploaded_file($archivoTemp, $GLOBALS["app_root"] . "/sam/compras/" . $nombreArchivo)){
                    $archivo = $GLOBALS["app_root"] . "/sam/compras/". $nombreArchivo;
            
                    if($general->obtenerSeparador($archivo) === true){
                        if (($fichero = fopen($archivo, "r")) !== false) {
                            $i = 0;

                            $option = "<option value=''>--Seleccione--</option>";
                            if(($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                                for($i = 0; $i < count($datos); $i++){
                                    $option .= "<option value='" . utf8_encode($datos[$i]) . "'>" . utf8_encode($datos[$i]) . "</option>";
                                }
                            }
                            fclose($fichero);
                        }
                    } else{
                        echo "5*";
                    } 
                        echo "1*" . $option . "*" . $nombreArchivo; 
                } else{
                    echo "3*"; 
                }
            } else {
                echo "4*";
            } 
        //}
    }
    else{
        $general->eliminarSesion();
        echo "0*"; 
    }
} else{
    echo "2*";
}