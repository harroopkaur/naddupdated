<?php
require_once("../../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$nuevo_contrato = new funcionesSam();
$log = new log();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){        
                $idFabricante = 0;
                if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                    $idFabricante = $_POST["fabricante"];
                }
                
                $numero = "";
                if(isset($_POST["numContrato"])){
                    $numero = $general->get_escape($_POST["numContrato"]);
                }
                
                $tipoContrato = "";
                if(isset($_POST["tipoContrato"])){
                    $tipoContrato = $general->get_escape($_POST["tipoContrato"]);
                }
                
                $fechaEfectiva = "0000/00/00";
                if(isset($_POST["fechaEfectiva"])){
                    $fechaEfectiva = $general->get_escape($_POST["fechaEfectiva"]);
                }
                
                $fechaExpiracion = "0000/00/00";
                if(isset($_POST["fechaExpiracion"])){
                    $fechaExpiracion = $general->get_escape($_POST["fechaExpiracion"]);
                }
                
                $fechaProxima = "0000/00/00";
                if(isset($_POST["fechaProxima"])){
                    $fechaProxima = $general->get_escape($_POST["fechaProxima"]);
                }
                
                $comentarios = "";
                if(isset($_POST["comentario"])){
                    $comentarios = $general->get_escape($_POST["comentario"]);
                }
                
                $subsidiaria = "";
                if(isset($_POST["subsidiaria"])){
                    $subsidiaria = $general->get_escape($_POST["subsidiaria"]);
                }
                
                $proveedor = "";
                if(isset($_POST["proveedor"])){
                    $proveedor = $general->get_escape($_POST["proveedor"]);
                }
                
                $opcion = "";
                if(isset($_POST["bandOpcion"])){
                    $opcion = $general->get_escape($_POST["bandOpcion"]);
                }
                
                $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
                

                if($opcion == "agregar"){
                        $archivo1    = "";
                        $archivo2    = "";
                        $archivo3    = "";
                        $archivo4    = "";
                        $archivo5    = "";
                }
                else{
                        $archivo1    = "";
                        if(isset($_POST["archivoActual1"])){
                            $archivo1 = $general->get_escape($_POST["archivoActual1"]);
                        }
                        
                        $archivo2    = "";
                        if(isset($_POST["archivoActual2"])){
                            $archivo2 = $general->get_escape($_POST["archivoActual2"]);
                        }
                        
                        $archivo3    = "";
                        if(isset($_POST["archivoActual3"])){
                            $archivo3 = $general->get_escape($_POST["archivoActual3"]);
                        }
                        
                        $archivo4    = "";
                        if(isset($_POST["archivoActual4"])){
                            $archivo4 = $general->get_escape($_POST["archivoActual4"]);
                        }
                        
                        $archivo5    = "";
                        if(isset($_POST["archivoActual5"])){
                            $archivo5 = $general->get_escape($_POST["archivoActual5"]);
                        }  
                }

                if($opcion == "agregar"){
                    $result = $nuevo_contrato->guardarContrato($_SESSION["client_id"], $_SESSION["client_empleado"], 
                              $idFabricante, $numero, $tipoContrato, $general->reordenarFecha($fechaEfectiva, "/", "-"), $general->reordenarFecha($fechaExpiracion, "/", "-"), 
                              $general->reordenarFecha($fechaProxima, "/", "-"), $comentarios, $archivo1, $archivo2, $archivo3, 
                              $archivo4, $archivo5, $subsidiaria, $proveedor);
                        
                    $log->insertar(28, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");    
                }
                else{
                    $contrato = 0;
                    if(isset($_POST["contrato"]) && filter_var($_POST["contrato"], FILTER_VALIDATE_INT) !== false){
                        $contrato = $_POST["contrato"];
                    }

                    $result = $nuevo_contrato->editarContrato($contrato, $idFabricante, $numero, $tipoContrato, 
                              $general->reordenarFecha($fechaEfectiva, "/", "-"), $general->reordenarFecha($fechaExpiracion, "/", "-"), 
                              $general->reordenarFecha($fechaProxima, "/", "-"), $comentarios, $archivo1, $archivo2, $archivo3, 
                              $archivo4, $archivo5, $subsidiaria, $proveedor);
                    
                    $log->insertar(28, 2, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
                }

                if($result == 1){
                        if($opcion == "agregar"){
                                $contrato = $nuevo_contrato->getUltimoContrato();
                        }

                        if(is_uploaded_file($_FILES["fileArchivo1"]["tmp_name"]) ){	
                                if($archivo1 != "" && $opcion == "editar"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo1)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo1);
                                        }
                                }
                                $direccion1 = $contrato . "." . $_FILES["fileArchivo1"]["name"];
                                $archivo1   = $direccion1;
                                move_uploaded_file($_FILES['fileArchivo1']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion1); 
                        }
                        else{
                                if($_POST["bandElimArchivo1"] == "true"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo1)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo1);
                                        }
                                        $archivo1 = "";
                                }
                        }

                        if(is_uploaded_file($_FILES["fileArchivo2"]["tmp_name"]) ){	
                                if($archivo2 != "" && $opcion == "editar"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo2)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo2);
                                        }
                                }
                                $direccion2 = $contrato . "." . $_FILES["fileArchivo2"]["name"];
                                $archivo2   = $direccion2;
                                move_uploaded_file($_FILES['fileArchivo2']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion2);
                        }
                        else{
                                if($_POST["bandElimArchivo2"] == "true"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo2)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo2);
                                        }
                                        $archivo2 = "";
                                }
                        }

                        if(is_uploaded_file($_FILES["fileArchivo3"]["tmp_name"]) ){	
                                if($archivo3 != "" && $opcion == "editar"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo3)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo3);
                                        }
                                }
                                $direccion3 = $contrato . "." . $_FILES["fileArchivo3"]["name"];
                                $archivo3   = $direccion3;
                                move_uploaded_file($_FILES['fileArchivo3']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion3);
                        }
                        else{
                                if($_POST["bandElimArchivo3"] == "true"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo3)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo3);
                                        }
                                        $archivo3 = "";
                                }
                        }

                        if(is_uploaded_file($_FILES["fileArchivo4"]["tmp_name"]) ){	
                                if($archivo4 != "" && $opcion == "editar"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo4)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo4);
                                        }
                                }
                                $direccion4 = $contrato . "." . $_FILES["fileArchivo4"]["name"];
                                $archivo4   = $direccion4;
                                move_uploaded_file($_FILES['fileArchivo4']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion4);
                        }
                        else{
                                if($_POST["bandElimArchivo4"] == "true"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo4)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo4);
                                        }
                                        $archivo4 = "";
                                }
                        }

                        if(is_uploaded_file($_FILES["fileArchivo5"]["tmp_name"]) ){	
                                if($archivo5 != "" && $opcion == "editar"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo5)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo5);
                                        }
                                }
                                $direccion5 = $contrato . "." . $_FILES["fileArchivo5"]["name"];
                                $archivo5   = $direccion5;
                                move_uploaded_file($_FILES['fileArchivo5']['tmp_name'], $GLOBALS['app_root'] ."/sam/contratos/" . $direccion5);
                        }
                        else{
                                if($_POST["bandElimArchivo5"] == "true"){
                                        if(file_exists ( $GLOBALS['app_root'] . "/sam/contratos/" . $archivo5)){
                                                unlink($GLOBALS['app_root'] . "/sam/contratos/" . $archivo5);
                                        }
                                        $archivo5 = "";
                                }
                        }

                        $result = $nuevo_contrato->actualizarArchivosContrato($contrato, $archivo1, $archivo2, $archivo3, $archivo4, $archivo5);

                        if($opcion == "editar"){
                            $nuevo_contrato->eliminarDetalleContrato($contrato, $asignaciones);
                        }

                        $aux = 0;
                        //for($i=0; $i < $_POST["totalRegistros"]; $i++){
                        $idProducto = $_POST["producto"]; 
                        $idEdicion  = $_POST["edicion"];
                        $version    = $_POST["version"];
                        $sku        = $_POST["SKU"];
                        $tipo       = $_POST["tipo"];
                        $cantidad   = $_POST["cantidad"];
                        $precio     = $_POST["precio"];
                        $asignacion = $_POST["asignacion"];

                        for($i=0; $i < count($idProducto); $i++){
                            if(filter_var($idProducto[$i], FILTER_VALIDATE_INT) === false){
                                $idProducto[$i] = 0;
                            }
                            
                            if(filter_var($idEdicion[$i], FILTER_VALIDATE_INT) === false){
                                $idEdicion[$i] = 0;
                            }
                            
                            if(filter_var($version[$i], FILTER_VALIDATE_INT) === false){
                                $version[$i] = 0;
                            }
                            
                            if(filter_var($cantidad[$i], FILTER_VALIDATE_INT) === false){
                                $cantidad[$i] = 0;
                            }
                            
                            if(filter_var($precio[$i], FILTER_VALIDATE_FLOAT) === false){
                                $precio[$i] = 0;
                            }
                            
                            if($nuevo_contrato->existeDetalleContrato($contrato, $idProducto[$i], $idEdicion[$i], $version[$i], $general->get_escape($asignacion[$i])) == 0){
                                $result = $nuevo_contrato->guardarDetalleContrato($contrato, $idProducto[$i], $idEdicion[$i], $version[$i], $general->get_escape($sku[$i]), $general->get_escape($tipo[$i]), $cantidad[$i], $precio[$i], $general->get_escape($asignacion[$i]));
                                if($result == 1){
                                    $aux++;
                                }
                            } else{
                                $result = $nuevo_contrato->actualizarDetalleContrato($contrato, $idProducto[$i], $idEdicion[$i], $version[$i], $general->get_escape($sku[$i]), $general->get_escape($tipo[$i]), $cantidad[$i], $precio[$i], $general->get_escape($asignacion[$i]));
                                if($result == 1){
                                    $aux++;
                                }
                            }
                        }

                        //if($aux == count($_POST["totalRegistros"])){
                        if($aux == count($idProducto)){
                                $result = 1;
                        }
                        else if($aux == 0){
                                $result = 2;
                        }
                        else{
                                $result = 3;
                        }

                }
                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);