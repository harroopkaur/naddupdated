<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_productos.php");
require_once($GLOBALS["app_root"] . "/clases/clase_ediciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_versiones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_equivalenciasPDO.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];

        $result = 0;
        $filaLicencia = 0;
        $fila   = "";
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $general     = new General();
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }
            
            $archivo = "";
            if(isset($_POST["archivo"])){
                $archivo = $general->get_escape($_POST["archivo"]);
            }
            
            $productos = "";
            if(isset($_POST["productoOtros"])){
                $productos = $general->get_escape($_POST["productoOtros"]);
            }
            
            $edicion = "";
            if(isset($_POST["edicionOtros"])){
                $edicion = $general->get_escape($_POST["edicionOtros"]);
            }
            
            $version = "";
            if(isset($_POST["versionOtros"])){
                $version = $general->get_escape($_POST["versionOtros"]);
            }
            
            $sku = "";
            if(isset($_POST["SKUOtros"])){
                $sku = $general->get_escape($_POST["SKUOtros"]);
            }
            
            $tipo = "";
            if(isset($_POST["tipoOtros"])){
                $tipo = $general->get_escape($_POST["tipoOtros"]);
            }
            
            $cantidad = "";
            if(isset($_POST["cantidadesOtros"])){
                $cantidad = $general->get_escape($_POST["cantidadesOtros"]);
            }
            
            $precio = "";
            if(isset($_POST["precioUnitarioOtros"])){
                $precio = $general->get_escape($_POST["precioUnitarioOtros"]);
            }
            
            $asignacion = "";
            if(isset($_POST["asignacionOtros"])){
                $asignacion = $general->get_escape($_POST["asignacionOtros"]);
            }
            
            $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $nueva_funcion  = new funcionesSam();
            $nuevo_producto = new Productos();
            $nuevo_edicion  = new Ediciones();
            $nuevo_version  = new Versiones();
            $equivalencia   = new EquivalenciasPDO();

            $iProducto = 0;
            $iEdicion = 0;
            $iVersion = 0;
            $iSKU = 0;
            $iTipo = 0;
            $iCantidad = 0;
            $iPrecio = 0;
            $iAsignacion = 0;
            
            $archivo = $GLOBALS['app_root'] . "/sam/compras/" . $archivo;

            if($general->obtenerSeparador($archivo) === true){
                if (($fichero = fopen($archivo, "r")) !== FALSE) {
                    $filaLicencia = 0;
                    $i = 0;
                    $fila = "";

                    while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== FALSE) {
                        if($i == 0){
                            for($j = 0; $j < count($datos); $j++){
                                if(utf8_encode($datos[$j]) == $productos){
                                    $iProducto =  $j;
                                }
                                
                                if(utf8_encode($datos[$j]) == $edicion){
                                    $iEdicion =  $j;
                                }
                                
                                if(utf8_encode($datos[$j]) == $version){
                                    $iVersion =  $j;
                                }
                                
                                if(utf8_encode($datos[$j]) == $sku){
                                    $iSKU =  $j;
                                }
                                
                                if(utf8_encode($datos[$j]) == $tipo){
                                    $iTipo =  $j;
                                }
                                
                                if(utf8_encode($datos[$j]) == $cantidad){
                                    $iCantidad =  $j;
                                }
                                
                                if(utf8_encode($datos[$j]) == $precio){
                                    $iPrecio =  $j;
                                }
                                
                                if(utf8_encode($datos[$j]) == $asignacion){
                                    $iAsignacion =  $j;
                                }
                            }
                        } else {
                            $idProducto = 0;
                            if($datos[$iProducto] != "" && $datos[$iEdicion] != "" && $datos[$iVersion] != "" && $datos[$iSKU] != "" 
                               && $datos[$iTipo] != "" && $datos[$iCantidad] != "" && $datos[$iPrecio] != "" && $datos[$iAsignacion] != ""){

                                $arrayProducto = $nuevo_producto->getProductoNombre(utf8_encode(trim($datos[$iProducto])));
                                $idProducto    = $arrayProducto["idProducto"];                          

                                $edicionAux    = utf8_encode(trim($datos[$iEdicion]));
                                if(ord($datos[$iEdicion]) == 160 || empty($edicionAux)){
                                    $edicionAux = "";
                                }

                                $arrayEdicion = $nuevo_edicion->getEdicionNombre($edicionAux);
                                $idEdicion    = $arrayEdicion["idEdicion"];

                                $versionAux    = utf8_encode(trim($datos[$iVersion]));
                                if(ord($datos[$iVersion]) == 160 || empty($versionAux)){
                                    $versionAux = "";
                                }

                                $arrayVersion = $nuevo_version->getVersionNombre($versionAux);
                                $idVersion    = $arrayVersion["id"];

                                if($fabricante == 10){
                                    $fabricante = 3;
                                }
                                $prodFabricante    = $nueva_funcion->obtenerProdFabricante($fabricante);
                                $edicionesProducto = $equivalencia->edicionesProducto($fabricante, $idProducto);
                                $versionEdicion    = $equivalencia->versionesEdicion($fabricante, $idProducto, $idEdicion);

                                if($general->validarAsignacion($_SESSION["client_id"], $_SESSION["client_empleado"], utf8_encode($datos[$iAsignacion]))){
                                    $fila .= '<tr style="border-bottom-style: 1px solid #ddd" id="'.$filaLicencia.'">
                                        <td style="text-align:center;"><input type="checkbox" id="check'.$filaLicencia.'" name="check[]"></td>
                                        <td><select id="producto'.$filaLicencia.'" name="producto[]" style="height:30px;" onchange="edicProducto(this.value, '.$filaLicencia.')">
                                            <option value = "">--Seleccione--</option>';
                                            foreach ($prodFabricante as $row) {
                                                $fila .=  '<option value="' . $row["idProducto"] . '" ';
                                                if($row["idProducto"] == $idProducto){
                                                    $fila .= 'selected="selected"';
                                                }
                                                //$fila .= '>' . utf8_decode($row["nombre"]) . '</option>';
                                                $fila .= '>' . $row["nombre"] . '</option>';
                                            }
                                            $fila .= '
                                        </select></td>
                                        <td style="border-bottom-style: 1px solid #ddd"><select id="edicion'.$filaLicencia.'" name="edicion[]" onchange="edicProductoVersion(this.value, ' . $filaLicencia . ')" style="height:30px;">
                                            <option value = "">--Seleccione--</option>';
                                            foreach ($edicionesProducto as $row) {
                                                $fila .=  '<option value="' . $row["idEdicion"] . '" ';
                                                if($row["idEdicion"] == $idEdicion){
                                                    $fila .= 'selected="selected"';
                                                }
                                                //$fila .= '>' . utf8_decode($row["nombre"]) . '</option>';
                                                $fila .= '>' . $row["nombre"] . '</option>';
                                            }
                                            $fila .= '</select></td>
                                        <td>
                                            <select id="version'.$filaLicencia.'" name="version[]" style="height:30px;">
                                                <option value="">--Seleccione--</option>';
                                                foreach ($versionEdicion as $row) {
                                                    $fila .=  '<option value="' . $row["id"] . '" ';
                                                    if($row["id"] == $idVersion){
                                                        $fila .= 'selected="selected"';
                                                    }
                                                    //$fila .= '>' . utf8_decode($row["nombre"]) . '</option>';
                                                    $fila .= '>' . $row["nombre"] . '</option>';
                                                }
                                                $fila .= '
                                            </select>
                                        </td>
                                        <td><input type="text" id="SKU'.$filaLicencia.'" name="SKU[]" style="height:30px;" value="' . $datos[3] . '"></td>';

                                        if(utf8_encode($datos[$iTipo]) == "Subscripción"){
                                            $datos[$iTipo] = "subscripcion";
                                        } else if(utf8_encode($datos[$iTipo]) == "Software Assurance" && $fabricante == 3){
                                            $datos[$iTipo] = "software assurance";
                                        } else if($datos[$iTipo] == "Perpetuo"){
                                            $datos[$iTipo] = "perpetuo";
                                        } else{
                                            $datos[$iTipo] = "otro";
                                        }

                                        $fila .= '<td><select id="tipo'.$filaLicencia.'" name="tipo[]" style="height:30px;">
                                            <option value = "">--Seleccione--</option>
                                             <option value = "perpetuo"';
                                                if($datos[$iTipo] == "perpetuo"){
                                                    $fila .= ' selected="selected"';  
                                                }
                                            $fila .= '>Perpetuo</option>';
                                            if($fabricante == 3){
                                                $fila .= '<option value = "software assurance"';
                                                    if($datos[$iTipo] == "software assurance"){
                                                        $fila .= ' selected="selected"';  
                                                    }
                                                $fila .= '>Software Assurance</option>';
                                            }
                                            $fila .= '<option value = "subscripcion"';
                                                if($datos[$iTipo] == "subscripcion"){
                                                    $fila .= ' selected="selected"';  
                                                }
                                            $fila .= '>Subscripci&oacute;n</option>
                                            <option value = "otro"';
                                                if($datos[$iTipo] == "otro"){
                                                    $fila .= ' selected="selected"';  
                                                }
                                            $fila .= '>Otro</option>
                                        </select></td>
                                        <td><input type="text" id="cantidad'.$filaLicencia.'" name="cantidad[]" style="width:70px; height:30px; text-align:right" maxlength=4 value="' . $datos[$iCantidad] . '" onblur="sumarCantidad()"></td>
                                        <td><input type="text" id="precio'.$filaLicencia.'" name="precio[]" style="width:70px; height:30px; text-align:right" maxlength=10 value="'; 
                                        if($iPrecio == ""){ $fila .= 0; } else { $fila .= $datos[$iPrecio]; }
                                        $fila .= '" onblur="sumarPrecio()"></td>
                                        <td>
                                            <select id="asignacion'.$filaLicencia.'" name="asignacion[]" style="height:30px;">
                                                <option value="">--Seleccione--</option>';
                                                foreach($asignaciones as $row){
                                                    $fila .= '<option value="' . $row["asignacion"] . '"';
                                                    if(utf8_encode($datos[$iAsignacion]) == $row["asignacion"]){ 
                                                        $fila .= "selected='selected'"; 
                                                    } 
                                                    $fila .= '>' . $row["asignacion"] . '</option>';
                                                } 
                                            $fila .= '</select>
                                        </td>
                                    </tr>';
                                    $filaLicencia++;
                                }
                            }
                        }
                        $i++;
                    }
                }
                $result = 1;
            }
            else{
                $result = 3;
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'row'=>$fila, 'result'=>$result, 
            'total'=>$filaLicencia, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);