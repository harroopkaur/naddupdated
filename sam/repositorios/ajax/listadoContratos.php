<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $cliente   = $_SESSION["client_id"];
                
                $pagina = 0;
                if(isset($_POST["pagina"]) && filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false){
                    $pagina    = $_POST["pagina"];
                }
                
                $limite = 0;
                if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                    $limite    = $_POST["limite"];
                }
                
                $orden = "";
                if(isset($_POST["orden"])){
                    $orden = $general->get_escape($_POST["orden"]);
                }
                
                $direccion = "";
                if(isset($_POST["direccion"])){
                    $direccion = $general->get_escape($_POST["direccion"]);
                }
                
                $div = '';
                $i = 0;
                $total = $nueva_funcion->totalRegistrosContratos($cliente, $_SESSION["client_empleado"]);
                //$query = $nueva_funcion->listadoContratos($cliente, $pagina, $limite);
                $query = $nueva_funcion->listadoContratos($cliente, $_SESSION["client_empleado"], $pagina, $limite, $orden, $direccion);
                foreach($query as $row){
                $div .= '<tr style="border-bottom: 1px solid #ddd;" id="'. $i . '"><input type="hidden" id="idContrato' . $i . '" name="idContrato' . $i . '" value="' . $row["idContrato"] . '">
                                <td style="text-align:center; border-bottom: 1px solid #ddd;"><input type="checkbox" id="check' . $i . '" name="check' . $i . '"></td>
                                <td style="border-bottom: 1px solid #ddd;"><a href="#" style="color:blue;" onclick="verContrato(' . $row["idContrato"] . ', \'consulta\')">' . $row["numero"] . '</a></td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["nombre"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["fechaEfectiva"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["fechaExpiracion"]. '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["fechaProxima"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["estado"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["comentarios"] . '</td>
                        </tr>';
                        $i++;
                }

                $ultimo  = "no";
                $primero = "no";

                $limiteNuevo = $limite * $pagina;
                if($limiteNuevo > $total || $pagina == "ultima"){
                        $limiteNuevo = $total;
                        $ultimo      = "si";
                        $pagina      = $nueva_funcion->ultimaPaginaContratos($cliente, $_SESSION["client_empleado"], $limite);
                }

                if($pagina == 1){
                        $primero = "si";
                }

                $sinPaginacion = "no";
                if($limite > $total){
                        $sinPaginacion = "si";
                }

                $paginacion = (($pagina - 1) * $limite) + 1 .'-' . $limiteNuevo . ' de ' . $total;

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
                'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
                'sinPaginacion'=>$sinPaginacion, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);