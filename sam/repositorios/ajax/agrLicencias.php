<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $fabricante = 5;
                if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                    $fabricante     = $_POST["fabricante"];
                }
                
                $filaLicencia = 0;
                if(isset($_POST["fila"]) && filter_var($_POST["fila"], FILTER_VALIDATE_INT) !== false){
                    $filaLicencia   = $_POST["fila"];
                }
                
                $nuevo_producto = new funcionesSam();

                if($fabricante == 10){
                    $fabricante = 3;
                }
                $prodFabricante = $nuevo_producto->obtenerProdFabricante($fabricante);
                $versiones      = $nuevo_producto->obtenerVersiones();
                
                $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
                
                $fila = '<tr style="border-bottom-style: 1px solid #ddd" id="'.$filaLicencia.'">
                                <td style="text-align:center;"><input type="checkbox" id="check'.$filaLicencia.'" name="check[]"></td>
                                <td><select id="producto'.$filaLicencia.'" name="producto[]" style="height:30px;" onchange="edicProducto(this.value, '.$filaLicencia.')">
                                        <option value = "">--Seleccione--</option>';
                                        foreach ($prodFabricante as $row) {
                                            $fila .=  '<option value="' . $row["idProducto"] . '">' . $row["nombre"] . '</option>';
                                        }
                                        $fila .= '
                                </select></td>
                                <td style="border-bottom-style: 1px solid #ddd"><select id="edicion'.$filaLicencia.'" name="edicion[]" onchange="edicProductoVersion(this.value, ' . $filaLicencia . ')" style="height:30px;">
                                        <option value = "">--Seleccione--</option>
                                </select></td>
                                <td>
                                        <select id="version'.$filaLicencia.'" name="version[]" onchange="validarRegistrosDuplicados('.$filaLicencia.')" style="height:30px;">
                                                <option value="">--Seleccione--</option>';
                                                /*foreach ($versiones as $row) {
                                                        $fila .=  '<option value="' . $row["id"] . '">' . utf8_decode($row["nombre"]) . '</option>';
                                                }*/
                                                $fila .= '
                                        </select>
                                </td>
                                <td><input type="text" id="SKU'.$filaLicencia.'" name="SKU[]" style="height:30px;"></td>
                                <td><select id="tipo'.$filaLicencia.'" name="tipo[]" style="height:30px;">
                                        <option value = "">--Seleccione--</option>
                                        <option value = "perpetuo">Perpetuo</option>';
                                        
                                        if($fabricante == 3){
                                            $fila .= '<option value = "software assurance">Software Assurance</option>';
                                        }
                                                
                                        $fila .= '<option value = "subscripcion">Subscripci&oacute;n</option>
                                    <option value = "otro">Otro</option>
                                </select></td>
                                <td><input type="text" id="cantidad'.$filaLicencia.'" name="cantidad[]" style="width:70px; height:30px; text-align:right" maxlength=7 value="0" onblur="sumarCantidad()"></td>
                                <td><input type="text" id="precio'.$filaLicencia.'" name="precio[]" style="width:70px; height:30px; text-align:right" maxlength=10 value="0.00" onblur="sumarPrecio()"></td>
                                <td>
                                    <select id="asignacion'.$filaLicencia.'" name="asignacion[]" onchange="validarRegistrosDuplicados('.$filaLicencia.')" style="height:30px;">
                                        <option value="">--Seleccione--</option>';
                                        foreach($asignaciones as $row){
                                            $fila .= '<option value="' . $row["asignacion"] . '">' . $row["asignacion"] . '</option>';
                                        } 
                                    $fila .= '</select>
                        </tr>' . $nuevo_producto->agrLicencias($filaLicencia);	
                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'row'=>$fila, 
                'fila'=>$filaLicencia, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);