<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $result  = 0;
            
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST['fabricante'], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }
            
            $archivo = 0;
            if(isset($_POST["archivo"]) && filter_var($_POST['archivo'], FILTER_VALIDATE_INT) !== false){
                $archivo = $_POST["archivo"];
            }

            if($fabricante == 1){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioAdobe.php");
                $repositorio = new repoDespliegueAdobe();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 2){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioIbm.php");
                $repositorio = new repoDespliegueIbm();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 3){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorio.php");
                $repositorio = new repoDespliegue();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 4){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioOracle.php");
                $repositorio = new repoDespliegueOracle();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 5){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioSAP.php");
                $repositorio = new repoDespliegueSAP();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 6){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioVMWare.php");
                $repositorio = new repoDespliegueVMWare();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 7){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioUnixIbm.php");
                $repositorio = new repoDespliegueUnixIbm();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 8){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioUnixOracle.php");
                $repositorio = new repoDespliegueUnixOracle();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }
            else if($fabricante == 10){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioSPLA.php");
                $repositorio = new repoDespliegueSPLA();
                if($repositorio->eliminarEncSamArchivo($archivo)){
                    $result = 1;
                }
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);