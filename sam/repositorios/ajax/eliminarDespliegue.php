<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje = $general->obtenerMensaje();
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}

$array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje));
if($verifSesion[0]){
    $sesion                    = true;
    $_SESSION['client_tiempo'] = $verifSesion[1];
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
        $result  = 0;
        $fabricante = $_POST["fabricante"];

        if($fabricante == 1){
            require_once("clases/repositorioAdobe.php");
            $repositorio = new repoDespliegueAdobe();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 2){
            require_once("clases/repositorioIbm.php");
            $repositorio = new repoDespliegueIbm();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 3){
            require_once("clases/repositorio.php");
            $repositorio = new repoDespliegue();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 4){
            require_once("clases/repositorioOracle.php");
            $repositorio = new repoDespliegueOracle();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 5){
            require_once("clases/repositorioSAP.php");
            $repositorio = new repoDespliegueSAP();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 6){
            require_once("clases/repositorioVMWare.php");
            $repositorio = new repoDespliegueVMWare();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 7){
            require_once("clases/repositorioUnixIbm.php");
            $repositorio = new repoDespliegueUnixIbm();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 8){
            require_once("clases/repositorioUnixOracle.php");
            $repositorio = new repoDespliegueUnixOracle();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }
        else if($fabricante == 10){
            require_once("clases/repositorioSPLA.php");
            $repositorio = new repoDespliegueSPLA();
            if($repositorio->eliminarEncSam($_SESSION["client_id"])){
                $result = 1;
            }
        }

        $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result));
    }
}
else{
    $general->eliminarSesion();
}
echo json_encode($array);