<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }
            
            $archivo = 0;
            if(isset($_POST["archivo"]) && filter_var($_POST["archivo"], FILTER_VALIDATE_INT) !== false){
                $archivo = $_POST["archivo"];
            }
            
            $mostrar = array();
            if($fabricante == 1){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioAdobe.php");
                $despliegue = new repoDespliegueAdobe();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/adobe/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/adobe/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/adobe/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/adobe/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/adobe/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/adobe/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 2){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioIbm.php");
                $despliegue = new repoDespliegueIbm();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/ibm/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/ibm/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/ibm/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/ibm/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/ibm/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/ibm/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 3){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorio.php");
                $despliegue = new repoDespliegue();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/microsoft/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/microsoft/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/microsoft/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/microsoft/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/microsoft/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/microsoft/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 4){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioOracle.php");
                $despliegue = new repoDespliegueOracle();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/oracle/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/oracle/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/oracle/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/oracle/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/oracle/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/oracle/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 5){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioSAP.php");
                $despliegue = new repoDespliegueSAP();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/sap/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/sap/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/sap/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/sap/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoDespliegue3"] != "" && file_exists($GLOBALS["app_root"] . "/sam/sap/archivos/" . $row["archivoDespliegue3"])){
                    $mostrar['archivoDespliegue3'] = $GLOBALS["domain_root"] . "/sam/sap/archivos/" . $row["archivoDespliegue3"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/sap/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/sap/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 6){
                require_once($GLOBALS["app_root"] . "/clases/repositorioVMWare.php");
                $despliegue = new repoDespliegueVMWare();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/vmware/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/vmware/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/vmware/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/vmware/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 7){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioUnixIbm.php");
                $despliegue = new repoDespliegueUnixIbm();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixibm/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/unixibm/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixibm/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/unixibm/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoDespliegue3"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixibm/archivos/" . $row["archivoDespliegue3"])){
                    $mostrar['archivoDespliegue3'] = $GLOBALS["domain_root"] . "/sam/unixibm/archivos/" . $row["archivoDespliegue3"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixibm/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/unixibm/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 8){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioUnixOracle.php");
                $despliegue = new repoDespliegueUnixOracle();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoDespliegue3"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue3"])){
                    $mostrar['archivoDespliegue3'] = $GLOBALS["domain_root"] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue3"];
                }
                
                if($row["archivoCompra"] != "" && file_exists($GLOBALS["app_root"] . "/sam/unixoracle/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/unixoracle/archivos/" . $row["archivoCompra"];
                }
            }
            else if($fabricante == 10){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioSPLA.php");
                $despliegue = new repoDespliegueSPLA();
                $row = $despliegue->encSamArchivo($archivo);
                if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/spla/archivos/" . $row["archivoDespliegue1"])){
                    $mostrar['archivoDespliegue1'] = $GLOBALS["domain_root"] . "/sam/spla/archivos/" . $row["archivoDespliegue1"];
                }
                
                if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/spla/archivos/" . $row["archivoDespliegue2"])){
                    $mostrar['archivoDespliegue2'] = $GLOBALS["domain_root"] . "/sam/spla/archivos/" . $row["archivoDespliegue2"];
                }
                
                if($row["archivoCompra"] != "" && file_exist($GLOBALS["app_root"] . "/sam/spla/archivos/" . $row["archivoCompra"])){
                    $mostrar['archivoCompra'] = $GLOBALS["domain_root"] . "/sam/spla/archivos/" . $row["archivoCompra"];
                }
            }
            
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'mostrar'=>$mostrar, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);