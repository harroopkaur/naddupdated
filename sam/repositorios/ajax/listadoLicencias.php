<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $asignacion = "";
                if(isset($_POST["asignacion"])){
                    $asignacion = $general->get_escape($_POST["asignacion"]);
                }
                
                $fabricante = 0;
                if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                    $fabricante = $_POST["fabricante"];
                }
                
                $pagina = 0;
                if(isset($_POST["pagina"]) && filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false){
                    $pagina = $_POST["pagina"];
                }
                
                $limite = 0;
                if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                    $limite     = $_POST["limite"];
                }
                
                $orden = "fabricantes.nombre, detalleContrato.idDetalleContrato";
                /*if(isset($_POST["orden"])){
                    $orden = $general->get_escape($_POST["orden"]);
                }*/
                
                $direccion = "asc";
                /*if(isset($_POST["direccion"])){
                    $direccion  = $general->get_escape($_POST["direccion"]);
                }*/
                
                $listadoAsignacion = $nueva_funcion->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
                
                $div = '';
                $i = 0;
                $total = $nueva_funcion->totalRegistrosLicencias($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante, $asignacion, $listadoAsignacion);
                $query = $nueva_funcion->listadoLicencias($_SESSION["client_id"], $_SESSION["client_empleado"], $asignacion, $listadoAsignacion, $fabricante, $pagina, $limite, $orden, $direccion);
                foreach($query as $row){
                $div .= '<tr style="border-bottom-style: 1px solid #ddd">
                                <td style="text-align:center; border-bottom: 1px solid #ddd;"><input type="checkbox" id="check' . $i . '" name="check' . $i . '"></td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["nombre"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["numero"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["proveedor"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $general->reordenarFecha($row["fechaEfectiva"], "-", "/") . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $general->reordenarFecha($row["fechaExpiracion"], "-", "/") . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["producto"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["edicion"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["version"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["cantidad"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["precio"] . '</td>
                                <td style="border-bottom: 1px solid #ddd;">' . $row["asignacion"] . '</td>
                        </tr>';
                        $i++;
                }
                
                /*$div .= '<script>
                    $(document).ready(function(){
                        $("#listaLicencias").trigger("update"); 
                        $("#listaLicencias").trigger("sorton"); 
                        $("#listaLicencias").tablesorter();
                    });
                </script>';*/

                $ultimo  = "no";
                $primero = "no";

                $limiteNuevo = $limite * $pagina;
                if($limiteNuevo > $total || $pagina == "ultima"){
                        $limiteNuevo = $total;
                        $ultimo      = "si";
                        $pagina      = $nueva_funcion->ultimaPaginaLicencias($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante, $asignacion, $listadoAsignacion, $limite);
                }

                if($pagina == 1){
                        $primero = "si";
                }

                $sinPaginacion = "no";
                if($limite > $total){
                        $sinPaginacion = "si";
                }

                $nuevoInicio = (($pagina - 1) * $limite) + 1;
                if($nuevoInicio < 0){
                    $nuevoInicio = 0;
                }
                $paginacion = $nuevoInicio .'-' . $limiteNuevo . ' de ' . $total;

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
                'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
                'sinPaginacion'=>$sinPaginacion, 'ultimaPagina'=>$pagina, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);