<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/repositorio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $result  = 0;
            $repositorio = new repoDespliegue();
            $fabricante = 0;
            if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                $fabricante = $_POST["fabricante"];
            }

            if($fabricante == 1){
                $carpeta = "adobe";
            }
            else if($fabricante == 2){
                $carpeta = "ibm";
            }
            else if($fabricante == 3){
                $carpeta = "microsoft";
            }
            else if($fabricante == 4){
                $carpeta = "oracle";
            }
            else if($fabricante == 5){
                $carpeta = "sap";
            }
            else if($fabricante == 6){
                $carpeta = "vmware";
            }
            else if($fabricante == 7){
                $carpeta = "unixibm";
            }
            else if($fabricante == 8){
                $carpeta = "unixoracle";
            }
            else if($fabricante == 10){
                $carpeta = "spla";
            }

            $posicion = "";
            if(isset($_POST["posicion"]) && filter_var($_POST["posicion"], FILTER_VALIDATE_INT) !== false){
                $posicion = $_POST["posicion"];
            }
            
            $tabla = $repositorio->archivoArchEncSam($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante);
            if(count($tabla) > 0 &&  $tabla["custom".$posicion] != ""){
                if(file_exists($GLOBALS['app_root'] . "/sam/". $carpeta ."/custom/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $tabla["custom".$posicion])){
                    unlink($GLOBALS['app_root'] . "/sam/". $carpeta ."/custom/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $tabla["custom".$posicion]);
                }
                $repositorio->limpiarCustomEncabSam($_SESSION["client_id"], $_SESSION["client_empleado"], $fabricante, $posicion);
                $result = 1;
            }
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);