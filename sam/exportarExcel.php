<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../configuracion/inicio.php");
require_once("../clases/clase_general.php");
require_once("clases/funciones.php");

$general     = new General();
$funciones = new funcionesSam();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    
    $fabricante = "";
    if(isset($_GET["vert"]) && is_numeric($_GET["vert"])){
        $fabricante = $_GET["vert"];
    }
    
    $asignacion = "";
    if(isset($_GET["vert1"])){
        $asignacion = $general->get_escape($_GET["vert1"]);
    }
        
    $listadoAsignacion = $funciones->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
    
    //$lista = $funciones->reporteLicencias1($_SESSION["client_id"], $_SESSION["client_empleado"], $where, $inicio, $total);
    $lista = $funciones->listadoLicencias1($_SESSION["client_id"], $_SESSION["client_empleado"], $asignacion, $listadoAsignacion, $fabricante);
    
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"


    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Fabricante')
                ->setCellValue('B1', 'Número de contrato')
                ->setCellValue('C1', 'Proveedor')
                ->setCellValue('D1', 'Fecha Efectiva')
                ->setCellValue('E1', 'Fecha Expiración')
                ->setCellValue('F1', 'Producto')
                ->setCellValue('G1', 'Edición')
                ->setCellValue('H1', 'Versión')
                ->setCellValue('I1', 'Cantidades')
                ->setCellValue('J1', 'Precio')
                ->setCellValue('K1', 'Asignación');
    
    $i=2;	
    foreach($lista as $row){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $row["nombre"])
                    ->setCellValue('B'.$i, $row["numero"])
                    ->setCellValue('C'.$i, $row["proveedor"])
                    ->setCellValue('D'.$i, $general->reordenarFecha($row["fechaEfectiva"], "-", "/"))
                    ->setCellValue('E'.$i, $general->reordenarFecha($row["fechaExpiracion"], "-", "/"))
                    ->setCellValue('F'.$i, $row["producto"])
                    ->setCellValue('G'.$i, $row["edicion"])
                    ->setCellValue('H'.$i, $row["version"])
                    ->setCellValue('I'.$i, $row["cantidad"])
                    ->setCellValue('J'.$i, $row["precio"])
                    ->setCellValue('K'.$i, $row["asignacion"]);

        $i++;
    }
    
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="listadoLicencias.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}