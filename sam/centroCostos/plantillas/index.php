<div style="width:745px; margin:0 auto; overflow:hidden;">
    <fieldset class="fieldset pointer" style="width:300px; height:200px; margin-top:20px; float:left;" id="asignarCentroCosto">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Asignar Centros de Costos</span></legend>
        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/sam/asignarCostos.png" style="width:300px; height:auto;">
    </fieldset>
    <fieldset class="fieldset pointer" style="width:300px; height:200px; margin-top:20px; margin-left:60px; float:left; overflow:hidden; position:relative;" id="UsuarioXEquipo">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Usuarios por Equipo</span></legend>
        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/sam/usuariosPorEquipo.png" style="width:210px; height:auto; top:50%; margin-top:-105px; left:50%; margin-left: -100px; position:absolute;">
    </fieldset>
    <fieldset class="fieldset pointer" style="width:300px; height:200px; margin:0 auto; overflow:hidden; position:relative;" id="resumenCentroCosto">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Resumen por Centro de Costo</span></legend>
        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/sam/ResumenCC3.png" style="width:210px; height:auto; top:50%; margin-top:-105px; left:50%; margin-left: -100px; position:absolute;">
    </fieldset>
</div>

<script>
    $(document).ready(function(){
        $("#listadoAsigUsuarioEquipo").tableHeadFixer();

        $("#asignarCentroCosto").click(function(){
            /*$("#fondo").show();
            $.post("' . $GLOBALS['domain_root'] . '/sam/asignarCentroCosto.php", { token : localStorage.licensingassuranceToken }, function(data){
                if(data[0].resultado === false){
                    location.href = "' . $GLOBALS["domain_root"] . '";
                    return false;
                }
                if(data[0].sesion === "false"){
                    $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                        location.href = "' . $GLOBALS['domain_root'] . '";
                        return false;
                    });
                }

                $("#contenedorCentral").empty();
                $("#contenedorCentral").append(data[0].div);
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });*/
            location.href="asignarCentroCosto.php";
        });

        $("#UsuarioXEquipo").click(function(){
            /*$("#fondo").show();
            $.post("' . $GLOBALS['domain_root'] . '/sam/usuarioEquipo.php", { token : localStorage.licensingassuranceToken }, function(data){
                if(data[0].resultado === false){
                    location.href = "' . $GLOBALS["domain_root"] . '";
                    return false;
                }
                if(data[0].sesion === "false"){
                    $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                        location.href = "' . $GLOBALS['domain_root'] . '";
                        return false;
                    });
                }

                $("#contenedorCentral").empty();
                $("#contenedorCentral").append(data[0].div);
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });*/
            location.href="usuarioEquipo.php";
        });

        $("#resumenCentroCosto").click(function(){
            /*$("#fondo").show();
            $.post("' . $GLOBALS['domain_root'] . '/sam/resumenCentroCosto.php", { token : localStorage.licensingassuranceToken }, function(data){
                if(data[0].resultado === false){
                    location.href = "' . $GLOBALS["domain_root"] . '";
                    return false;
                }
                if(data[0].sesion === "false"){
                    $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                        location.href = "' . $GLOBALS['domain_root'] . '";
                        return false;
                    });
                }

                $("#contenedorCentral").empty();
                $("#contenedorCentral").append(data[0].div);
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });*/
            location.href="resumenCentroCosto.php";
        });
    });
</script>