<form id="formCentroCosto" name="formCentroCosto" method="post" enctype="multipart/form-data">
    <input type="hidden" id="tokenCentroCosto" name="token">
    <div style="width:100%; height:400px; overflow:hidden;">
        <table class="tablap" id="listadoAsigUsuarioEquipo" style="margin-top:-5px;">
            <thead>
                <tr>
                    <th class="text-center">Usuario</th>
                    <th class="text-center">Equipo Asignado</th>
                    <th class="text-center">Centro de Costos</th>
                    <th class="text-center">Costo</th>
                </tr>
            </thead>
            <tbody id="listUsuaEquipo">
            <?php
            $i = 0;
            foreach($listado as $row){
            ?>
                <tr>
                    <td><input type="hidden" id="idUsuarioEquipo<?= $i ?>" name="idUsuarioEquipo[]" value="<?= $row["id"] . "*" . $row["host_name"] . "*" . $row["usuario"] ?>"><?= utf8_encode($row["usuario"]) ?></td>
                    <td><?= $row["host_name"] ?></td>
                    <td><input type="text" id="centroCosto<?= $i ?>" name="centroCosto[]" value="<?= $row["centroCosto"] ?>" readonly></td>
                    <td class="text-right"><?= round($row["costo"], 0) ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
            </tbody>
        </table>
    </div>

    <br>

    <div style="float:right;" class="botonesSAM boton5" id="guardarAsignacion">Guardar</div> 
    <div style="float:right;" class="botonesSAM boton5" id="asignacion">Asignar</div>
    <div style="float:right;" class="botonesSAM boton5" id="editarCentroCosto">Editar</div>
    <div style="float:right;" class="botonesSAM boton5" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/centroCostos/';">Atras</div> 
    <div style="float:right;" class="botonesSAM boton5" onclick="window.open('<?= $GLOBALS["domain_root"] ?>/sam/centroCostos/reportes/asignarCentroCosto.php');">Exportar</div> 
    <input type="file" class="hide" id="archivo" name="archivo" accept=".csv">
    <div style="float:right;" class="botonesSAM boton5" onclick="$('#archivo').click();">Importar</div>
</form>

<script>
    $(document).ready(function(){
        $("#listadoAsigUsuarioEquipo").tableHeadFixer();
        $("#listadoAsigUsuarioEquipo").tablesorter();
        
        $("#asignacion").click(function(){
            for(i = 0; i < $("#listUsuaEquipo tr").length; i++){
                $("#centroCosto" + i).prop("readonly", "");
            }
            $("#centroCosto0").focus();
        });

        $("#editarCentroCosto").click(function(){
            j = 0;
            bandera = false;
            for(i = 0; i < $("#listUsuaEquipo tr").length; i++){
                if($("#centroCosto" + i).val() === ""){
                    j++;
                } else{
                    $("#centroCosto" + i).prop("readonly", "");
                    if(bandera === false){
                        $("#centroCosto" + i).focus();
                        bandera = true;
                    }
                }
            }

            if(i === j){
                $.alert.open("alert", "Debe asignar primero el centro de costo antes de editar", {"Aceptar" : "Aceptar"}, function() {
                });
            }
        });

        $("#guardarAsignacion").click(function(){
            $("#fondo").show();
            $("#tokenCentroCosto").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formCentroCosto")[0]);	
            $.ajax({
                type: "POST",
                url: "ajax/guardarCentroCosto.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    $("#fondo").hide();
                    <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>

                    if(data[0].result === 0){
                        $.alert.open("alert", "No se guardó los registros de Centro de Costo", {"Aceptar" : "Aceptar"}, function() {
                        }); 
                    } else if(data[0].result === 1){
                        $.alert.open("info", "Centro de Costo guardado con éxito", {"Aceptar" : "Aceptar"}, function() {
                            location.href="<?= $GLOBALS["domain_root"] ?>/sam/centroCostos/";
                        }); 
                    } else if(data[0].result === 1){
                        $.alert.open("alert", "No se guardaron todos los registros del Centro de Costo", {"Aceptar" : "Aceptar"}, function() {
                        }); 
                    }

                    for(i = 0; i < $("#listUsuaEquipo tr").length; i++){
                        $("#centroCosto" + i).prop("readonly", "readonly");
                    }
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });
        
        $("#archivo").change(function(){
            $("#fondo").show();
            $("#tokenCentroCosto").val(localStorage.licensingassuranceToken);
            var formData = new FormData($("#formCentroCosto")[0]);	
            $.ajax({
                type: "POST",
                url: "ajax/importarCentroCosto.php", 
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",  
                cache:false,
                success: function(data){
                    $("#fondo").hide();
                    <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                    
                    $("#listUsuaEquipo").empty();
                    $("#listUsuaEquipo").append(data[0].lista);
                }
            })
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });
    });
</script>