<div style="width:100%; max-height:400px;">
    <table class="tablap" id="listadoAsigUsuarioEquipo" style="margin-top:-5px;">
        <thead>
            <tr>
                <th class="text-center">Centro Costo</th>
                <th class="text-center">Costo por Equipo</th>
            </tr>
        </thead>
        <tbody>
        <?php    
        $i = 0;
        $NombreEquipoAux = "";
        $centroAux = "";
        foreach($listado as $row){
        ?>
            <tr>
                <td><?= $row["centroCosto"] ?></td>
                <td class="text-right"><?= round($row["costo"], 0) ?></td>
            </tr>
        <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
</div>

<br>

<div style="float:right;" class="botonesSAM boton5" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/centroCostos/';">Atras</div> 
<div style="float:right;" class="botonesSAM boton5" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/centroCostos/reportes/resumenCentroCosto.php';">Exportar</div> 

<script>
    $(document).ready(function(){
        $("#listadoAsigUsuarioEquipo").tableHeadFixer();
    });
</script>