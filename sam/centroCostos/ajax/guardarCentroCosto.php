<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_centro_costo.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $centerCostos = new centroCostos();
            
            $idCentroCosto = $_POST["idUsuarioEquipo"];
            $centroCosto = $_POST["centroCosto"];
            
            $result = 0;
            
            $j = 0;
            for($i = 0; $i < count($idCentroCosto); $i++){
                $id = 0;
                $string = $general->get_escape($idCentroCosto[$i]);
                $array = explode("*", $string);
                
                if(filter_var($array[0], FILTER_VALIDATE_INT) !== false){
                    $id = $array[0];
                }
                
                $equipo = null;
                if(isset($array[1])){
                    $equipo = $general->get_escape($array[1]);
                }
                
                $usuario = null;
                if(isset($array[2])){
                    $usuario = $general->get_escape($array[2]);
                }
                
                $centro = null;
                if(isset($centroCosto[$i]) && $centroCosto[$i] != ""){
                    $centro = $general->get_escape($centroCosto[$i]);
                }
                
                if($id == 0){
                    if($centerCostos->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $equipo, $usuario, $centro)){
                        $j++;
                    }
                } else{
                    if($centerCostos->actualizar($id, $centro)){
                        $j++;
                    }
                }
            }
            
            if($i > $j && $j > 0){
                $result = 2;
            } else if($i == $j){
                $result = 1;
            }
            
            $array = array(0=>array('sesion'=>$sesion, 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);