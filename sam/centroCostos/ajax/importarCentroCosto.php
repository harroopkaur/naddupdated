<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_centro_costo.php");

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $lista = "";
            if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
                $usuarioEquipo = new centroCostos();
                $listado = $usuarioEquipo->listarUsuarioEquipoTodos($_SESSION["client_id"], $_SESSION["client_empleado"]);

                $i = 0;
                if($general->obtenerSeparadorUniversal($_FILES["archivo"]["tmp_name"], 1, 3) === true){
                    foreach($listado as $row){
                        $listaAux = '<tr>
                                <td><input type="hidden" id="idUsuarioEquipo' . $i . '" name="idUsuarioEquipo[]" value="' . $row["id"] . "*" . $row["host_name"] . "*" . $row["usuario"] . '">' . $row["usuario"] . '</td>
                                <td>' . $row["host_name"] . '</td>
                                <td><input type="text" id="centroCosto' . $i . '" name="centroCosto[]" value="' . $row["centroCosto"] . '" readonly></td>
                                <td class="text-right">' . round($row["costo"], 0) . '</td>
                            </tr>';
                        if (($fichero = fopen($_FILES["archivo"]["tmp_name"], "r")) !== false){
                            while (($datos = fgetcsv($fichero, 1000, $general->separador)) !== false) {
                                if($row["host_name"] == utf8_encode($datos[1]) && $row["usuario"] == utf8_encode($datos[0])){
                                    $listaAux = '<tr>
                                        <td><input type="hidden" id="idUsuarioEquipo' . $i . '" name="idUsuarioEquipo[]" value="' . $row["id"] . "*" . $row["host_name"] . "*" . $row["usuario"] . '">' . $row["usuario"] . '</td>
                                        <td>' . $row["host_name"] . '</td>
                                        <td><input type="text" id="centroCosto' . $i . '" name="centroCosto[]" value="' . utf8_encode($datos[2]) . '" readonly></td>
                                        <td class="text-right">' . round($row["costo"], 0) . '</td>
                                    </tr>';
                                    break;
                                }
                            }
                            fclose($fichero);
                        }
                        
                        $lista .= $listaAux;
                        $i++;
                    }
                }
            }
            $array = array(0=>array('sesion'=>$sesion, 'lista'=>$lista, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);