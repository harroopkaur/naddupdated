<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS['app_root'] . "/clases/clase_centro_costo.php");
$nueva_funcion = new funcionesSam();
$usuarioEquipo = new centroCostos();
$listado = $usuarioEquipo->listarUsuarioEquipoTodos($_SESSION["client_id"], $_SESSION["client_empleado"]);
$general = new General();