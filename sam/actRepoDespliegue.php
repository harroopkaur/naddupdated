<?php
require_once("../configuracion/inicio.php");
require_once("../clases/clase_general.php");
require_once("../clases/clase_archivos_fabricantes.php");

//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $result = 0;
            $opcion = 0;
            $archivo = 0;
            $fechaHora = date("dmYHis");
            
            $archivosDespliegue = new clase_archivos_fabricantes();
            
            if(isset($_POST["tabla"]) && filter_var($_POST["tabla"], FILTER_VALIDATE_INT) !== false){
                $opcion = $_POST["tabla"];
            }     
            
            $rowDespliegue = $archivosDespliegue->archivosDespliegue($_SESSION["client_id"], $_SESSION["client_empleado"], $opcion);
            
            if($opcion == 1){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioAdobe.php");
                $repositorio = new repoDespliegueAdobe();
                
                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/adobe/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/adobe/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/adobe/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/adobe/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/adobe/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/adobe/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "LAD_Output" . $fechaHora . ".rar";
                $archivoDespliegue2 = "LAE_Output" . $fechaHora . ".csv";
                $archivoCompra = "compraAdobe" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoDespliegue2, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/adobe/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/adobe/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/adobe/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/adobe/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/adobe/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/adobe/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/adobe/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/adobe/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/adobe/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarDetalle($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumen($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            $result = 1;
                        }
                    }
                } 
            }
            if($opcion == 2){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioIbm.php");
                $repositorio = new repoDespliegueIbm();

                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/ibm/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/ibm/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/ibm/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/ibm/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/ibm/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/ibm/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "LAD_Output" . $fechaHora . ".rar";
                $archivoDespliegue2 = "LAE_Output" . $fechaHora . ".csv";
                $archivoCompra = "compraIBM" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoDespliegue2, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/ibm/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/ibm/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/ibm/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/ibm/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/ibm/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/ibm/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/ibm/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/ibm/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/ibm/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarDetalle($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumen($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            $result = 1;
                        }
                    }
                }  
            }
            else if($opcion == 3){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorio.php");
                $repositorio = new repoDespliegue();
                
                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "LAD_Output" . $fechaHora . ".rar";
                $archivoDespliegue2 = "LAE_Output" . $fechaHora . ".csv";
                $archivoCompra = "compraMicrosoft" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION['client_empleado'], $archivoDespliegue1, $archivoDespliegue2, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/microsoft/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/microsoft/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/microsoft/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/microsoft/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/microsoft/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/microsoft/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarDetalle($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumen($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            if($repositorio->agregarWindowServerClienteSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                if($repositorio->agregarAlineacionWindowServerSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                    if($repositorio->agregarSqlServerClienteSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                        if($repositorio->agregarAlineacionSqlServerSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                            if($repositorio->agregarDesarrolloPruebasVSSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                                if($repositorio->agregarDesarrolloPruebasMSDNSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                                    if($repositorio->agregarEscaneoSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                                        $result = 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if($opcion == 4){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioOracle.php");
                $repositorio = new repoDespliegueOracle();

                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/oracle/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/oracle/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/oracle/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/oracle/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/oracle/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/oracle/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "LAD_Output" . $fechaHora . ".rar";
                $archivoDespliegue2 = "LAE_Output" . $fechaHora . ".csv";
                $archivoCompra = "compraOracle" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoDespliegue2, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/oracle/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/oracle/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/oracle/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/oracle/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/oracle/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/oracle/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/oracle/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/oracle/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/oracle/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarDetalle($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumen($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            $result = 1;
                        }
                    }
                }
            }
            if($opcion == 5){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioSAP.php");
                $repositorio = new repoDespliegueSAP();

                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoDespliegue3"] != "" && file_exists($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoDespliegue3"])){
                        unlink($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoDespliegue3"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/sap/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo, $_SESSION["client_empleado"]);
                }
                
                $archivoDespliegue1 = "Output_1_texto Componentes Sistema" . $fechaHora . ".txt";
                $archivoDespliegue2 = "Output_1_texto_Usuarios x Modulo" . $fechaHora . ".txt";
                $archivoDespliegue3 = "Output_1_texto Aging Usuarios" . $fechaHora . ".txt";
                $archivoCompra = "compraSAP" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoDespliegue2, $archivoDespliegue3, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sap/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/sap/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/sap/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sap/archivos_csvf2/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/sap/archivos_csvf2/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/sap/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoDespliegue3"] != "" && file_exists($GLOBALS['app_root'] . "/sap/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue3"])){
                    copy($GLOBALS['app_root'] . "/sap/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue3"], 
                    $GLOBALS['app_root'] . "/sam/sap/archivos/" . $archivoDespliegue3);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/sap/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/sap/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/sap/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarResumenServidor($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumenUsuario($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarUsabilidadUsuario($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                $result = 1;
                            }
                        }
                    }
                }
            }
            if($opcion == 6){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioVMWare.php");
                $repositorio = new repoDespliegueVMWare();

                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/vmware/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/vmware/archivos/" . $row["archivoDespliegue1"]);
                    }
                   
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/vmware/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/vmware/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "licenciasESXiVcenter" . $fechaHora . ".csv";
                $archivoCompra = "License_Keys" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/VMWare/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/VMWare/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/vmware/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/VMWare/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/VMWare/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/vmware/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarResumen($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarDetalleContrato($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            $result = 1;
                        }
                    }
                }
            }
            if($opcion == 7){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioUnixIbm.php");
                $repositorio = new repoDespliegueUnixIbm();

                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoDespliegue3"] != "" && file_exists($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoDespliegue3"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoDespliegue3"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "archivosConsolidadosSolaris" . $fechaHora . ".rar";
                $archivoDespliegue2 = "archivosConsolidadosAIX" . $fechaHora . ".rar";
                $archivoDespliegue3 = "archivosConsolidadosLinux" . $fechaHora . ".rar";
                $archivoCompra = "compraUnixIBM" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoDespliegue2, $archivoDespliegue3, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/unixIbm/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/unixIbm/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/unixIbm/archivos_csvf2/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/unixIbm/archivos_csvf2/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoDespliegue3"] != "" && file_exists($GLOBALS['app_root'] . "/unixIbm/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue3"])){
                    copy($GLOBALS['app_root'] . "/unixIbm/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue3"], 
                    $GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $archivoDespliegue3);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/unixIbm/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/unixIbm/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/unixibm/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarResumenSolaris($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumenAIX($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarResumenLinux($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                if($repositorio->agregarDetalleSolaris($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                    if($repositorio->agregarDetalleAIX($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                        if($repositorio->agregarDetalleLinux($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                            $result = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if($opcion == 8){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioUnixOracle.php");
                $repositorio = new repoDespliegueUnixOracle();

                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoDespliegue3"] != "" && file_exists($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue3"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoDespliegue3"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "archivosConsolidadosSolaris" . $fechaHora . ".rar";
                $archivoDespliegue2 = "archivosConsolidadosAIX" . $fechaHora . ".rar";
                $archivoDespliegue3 = "archivosConsolidadosLinux" . $fechaHora . ".rar";
                $archivoCompra = "compraUnixOrcle" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoDespliegue2, $archivoDespliegue3, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/unixOracle/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/unixOracle/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/unixOracle/archivos_csvf2/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/unixOracle/archivos_csvf2/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoDespliegue3"] != "" && file_exists($GLOBALS['app_root'] . "/unixOracle/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue3"])){
                    copy($GLOBALS['app_root'] . "/unixOracle/archivos_csvf3/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue3"], 
                    $GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $archivoDespliegue3);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/unixOracle/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/unixOracle/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/unixoracle/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarResumenSolaris($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumenAIX($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarResumenLinux($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                if($repositorio->agregarDetalleSolaris($_SESSION['client_id'],$_SESSION["client_empleado"],  $archivo)){
                                    if($repositorio->agregarDetalleAIX($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                        if($repositorio->agregarDetalleLinux($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                            $result = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if($opcion == 10){
                require_once($GLOBALS["app_root"] . "/sam/clases/repositorioSPLA.php");
                $repositorio = new repoDespliegueSPLA();

                if($repositorio->cantRepositorios($_SESSION['client_id'], $_SESSION["client_empleado"]) == 2){
                    $row = $repositorio->obtenerEncSamArchivoElim($_SESSION["client_id"], $_SESSION["client_empleado"]);
                    
                    if($row["archivo"] !== ""){
                        $archivo = $row["archivo"];
                    }
                    
                    if($row["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/sam/spla/archivos/" . $row["archivoDespliegue1"])){
                        unlink($GLOBALS['app_root'] . "/sam/spla/archivos/" . $row["archivoDespliegue1"]);
                    }
                    
                    if($row["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/sam/spla/archivos/" . $row["archivoDespliegue2"])){
                        unlink($GLOBALS['app_root'] . "/sam/spla/archivos/" . $row["archivoDespliegue2"]);
                    }
                    
                    if($row["archivoCompra"] != "" &&  file_exists($GLOBALS['app_root'] . "/sam/spla/archivos/" . $row["archivoCompra"])){
                        unlink($GLOBALS['app_root'] . "/sam/spla/archivos/" . $row["archivoCompra"]);
                    }
                    
                    $repositorio->eliminarEncSamArchivo($archivo);
                }
                
                $archivoDespliegue1 = "LAD_Output" . $fechaHora . ".rar";
                $archivoDespliegue2 = "LAE_Output" . $fechaHora . ".csv";
                $archivoCompra = "compraSPLA" . $fechaHora . ".csv";
                
                $repositorio->agregarEncSam($_SESSION['client_id'], $_SESSION["client_empleado"], $archivoDespliegue1, $archivoDespliegue2, $archivoCompra);
                
                if($rowDespliegue["archivoDespliegue1"] != "" && file_exists($GLOBALS['app_root'] . "/spla/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"])){
                    copy($GLOBALS['app_root'] . "/spla/archivos_csvf1/" . $_SESSION["client_id"] . "/" . $_SESSION["client_empleado"] . "/" . $rowDespliegue["archivoDespliegue1"], 
                    $GLOBALS['app_root'] . "/sam/spla/archivos/" . $archivoDespliegue1);
                }
                
                if($rowDespliegue["archivoDespliegue2"] != "" && file_exists($GLOBALS['app_root'] . "/spla/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"])){
                    copy($GLOBALS['app_root'] . "/spla/archivos_csvf2/" . $rowDespliegue["archivoDespliegue2"], 
                    $GLOBALS['app_root'] . "/sam/spla/archivos/" . $archivoDespliegue2);
                }
                
                if($rowDespliegue["archivoCompras"] != "" && file_exists($GLOBALS['app_root'] . "/spla/archivos_csvf4/" . $rowDespliegue["archivoCompras"])){
                    copy($GLOBALS['app_root'] . "/spla/archivos_csvf4/" . $rowDespliegue["archivoCompras"], 
                    $GLOBALS['app_root'] . "/sam/spla/archivos/" . $archivoCompra);
                }
                
                $arch = $repositorio->archivoEncSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"]);
                $archivo = $arch["archivo"];
                
                if($repositorio->agregarDetalle($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                    $result = 2;
                    if($repositorio->agregarResumen($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                        if($repositorio->agregarBalance($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                            if($repositorio->agregarWindowServerClienteSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                if($repositorio->agregarAlineacionWindowServerSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                    if($repositorio->agregarSqlServerClienteSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                        if($repositorio->agregarAlineacionSqlServerSamArchivo($_SESSION['client_id'], $_SESSION["client_empleado"], $archivo)){
                                            $result = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);