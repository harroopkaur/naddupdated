<div class="textog negro" style="font-size:28px; margin:20px; text-align:center;">Pol&iacute;ticas: <p style="color:#06B6FF; display:inline">Financiera, Legislaci&oacute;n y Pol&iacute;ticas</p></div>

<br>
<div class="contenedorProcesosSamLeft">
        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/nuebe.jpg" class="tamNube">

        <br><br><br><br>
        <p style="font-size:20px;">Las Pol&iacute;ticas de los Procesos Claves (PC) del SAM</p> 
</div>

<form id="formPC79" name="formPC79" method="post" enctype="multipart/form-data">
    <input type="hidden" id="tokenPC79" name="token">
    <input type="hidden" id="actualPolFinan" name="actualPolFinan" value="<?= $PolFinan ?>">
    <input type="hidden" id="actualPolLegis" name="actualPolLegis" value="<?= $PolLegis ?>">
    <input type="hidden" id="actualPolPolit" name="actualPolPolit" value="<?= $PolPolit ?>">
    <input type="hidden" id="carpeta" name="carpeta" value="<?= $carpeta ?>">
    <input type="hidden" id="cargarArchivo" name="cargarArchivo">

    <div class="contenedorProcesosSamRight">
        <div class="contenedorTabla">
            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">7</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileFinan" name="fileFinan" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textFinan"><?php if($PolFinan != ""){ echo $PolFinan; } else { echo 'Administraci&oacute;n financiera'; } ?></p>
                        <div class="btn-elim" id="elimFinan" <?php if($PolFinan == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnFinan">
                        <p class="centrarContTabla">Pol&iacute;tica Financiera</p>
                    </div>
                </div>
            </div>

            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">8</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileLegis" name="fileLegis" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textLegis"><?php if($PolLegis != ""){ echo $PolLegis; } else{ echo 'Administraci&oacute;n de legislaci&oacute;n de activos'; } ?></p>
                        <div class="btn-elim" id="elimLegis" <?php if($PolLegis == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnLegis">
                        <p class="centrarContTabla">Pol&iacute;tica Legislaci&oacute;n</p>
                    </div>
                </div>
            </div>

            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">9</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="filePolit" name="filePolit" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textPolit"><?php if($PolPolit != ""){ echo  $PolPolit; } else { echo 'Administraci&oacute;n de pol&iacute;ticas'; } ?></p>
                        <div class="btn-elim" id="elimPolit" <?php if($PolPolit == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnPolit">
                        <p class="centrarContTabla">Pol&iacute;ticas</p>
                    </div>
                </div>
            </div>
        </div>

        <div style="margin-right:-10px;">
            <div class="botonesSAM boton1" id="btnAgrPC79">Guardar</div>
            <div class="botonesSAM boton5" onclick="location.href='pc4-6.php';">Atras</div>
        </div>
    </div>
</form>

<script>
    var tipoArchivo = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    $(document).ready(function(){
        $("#btnFinan").click(function(){
            $("#cargarArchivo").val("#textFinan");
            $("#fileFinan").click();
        });

        $("#btnLegis").click(function(){
            $("#cargarArchivo").val("#textLegis");
            $("#fileLegis").click();
        });

        $("#btnPolit").click(function(){
            $("#cargarArchivo").val("#textPolit");
            $("#filePolit").click();
        });

        $("#fileFinan").change(function(e){
            if (msie > 0){
                $("#textFinan").empty();
                $("#textFinan").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#fileLegis").change(function(e){
            if (msie > 0){
                $("#textLegis").empty();
                $("#textLegis").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#filePolit").change(function(e){
            if (msie > 0){
                $("#textPolit").empty();
                $("#textPolit").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#btnAgrPC79").click(function(){
            $("#fondo").show();
            if (msie > 0) // If Internet Explorer, return version number
            {
                $("#formPC79").submit();
                setTimeout(function(){
                    location.href="pc10-12.php";
                }, 5000);
            }
            else  // If another browser, return 0
            {
                if($("#fileFinan").val() === "" && $("#fileLegis").val() === "" && $("#filePolit").val() === ""){
                    location.href="pc10-12.php";
                }
                else{
                    $("#tokenPC79").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#formPC79")[0]);	
                    $.ajax({
                        type: "POST",
                        url: "<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/guardarPC7-9.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            $("#fondo").hide();
                            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                                    
                            if(data[0].result === 0){
                                $.alert.open("alert", "No se guardó ningún archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="pc7-9.php";
                                }); 
                            }
                            else if(data[0].result === 1){
                                $.alert.open("info", "Se guardó " + data[0].result + " archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="pc10-12.php";
                                }); 
                            }
                            else{
                                $.alert.open("info", "Se guardó " + data[0].result + " archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="pc10-12.php";
                                }); 
                            }
                        }
                    })
                    .fail(function(jqXHR){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                            location.href="pc7-9.php";
                        });
                    });
                }
            }
        });

        $("#elimFinan").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "financiera", archivoActual : $("#actualPolFinan").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc7-9.php";
                    });
                }
            }, "json")
            .fail(function(){
                alert("hubo un error");
            });
        });

        $("#elimLegis").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "legislacion", archivoActual : $("#actualPolLegis").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc7-9.php";
                    });
                }
            }, "json")
            .fail(function(){
                alert("hubo un error");
            });
        });

        $("#elimPolit").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "politicas", archivoActual : $("#actualPolPolit").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc7-9.php";
                    });
                }
            }, "json")
            .fail(function(){
                alert("hubo un error");
            });
        });
    });

    function addArchivo(e){
        file = e.target.files[0]; 
        if (!file.type.match(tipoArchivo) && !file.type.match("application/pdf")){
            alert("El archivo a cargar debe ser .docx");
            return false;
        }
        else if(parseInt(file.size) / 1024 > 5120 ){
            alert("El tamaño permitido es de 5 MB");
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $($("#cargarArchivo").val()).empty();
            $($("#cargarArchivo").val()).append(file.name);
        }
    }

    function fileOnload(e) {
        result=e.target.result;
    }
</script>