<div class="textog negro" style="font-size:28px; margin:20px; text-align:center;">Pol&iacute;ticas: <p style="color:#06B6FF; display:inline">Programas, Proyectos y Proveedores</p></div>

<br>
<div class="contenedorProcesosSamLeft">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/nuebe.jpg" class="tamNube">

    <br><br><br><br>
    <p style="font-size:20px;">Las Pol&iacute;ticas de los Procesos Claves (PC) del SAM</p> 
</div>

<form id="formPC1012" name="formPC1012" method="post" enctype="multipart/form-data">
    <input type="hidden" id="tokenPC1012" name="token">
    <input type="hidden" id="actualPolProgr" name="actualPolProgr" value="<?= $PolProgr ?>">
    <input type="hidden" id="actualPolProye" name="actualPolProye" value="<?= $PolProye ?>">
    <input type="hidden" id="actualPolProve" name="actualPolPolit" value="<?= $PolProve ?>">
    <input type="hidden" id="cargarArchivo" name="cargarArchivo">

    <div class="contenedorProcesosSamRight">
        <div class="contenedorTabla">
            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">10</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileProgr" name="fileProgr" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textProgr"><?php if($PolProgr != ""){ echo $PolProgr; } else { echo 'Administraci&oacute;n de programas'; } ?></p>
                        <div class="btn-elim" id="elimProgr" <?php if($PolProgr == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnProgr">
                        <p class="centrarContTabla">Pol&iacute;tica Programas</p>
                    </div>
                </div>
            </div>

            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">11</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileProye" name="fileProye" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textProye"><?php if($PolProye != ""){ echo $PolProye; } else { echo 'Administraci&oacute;n de proyectos'; } ?></p>
                        <div class="btn-elim" id="elimProye" <?php if($PolProye == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnProye">
                        <p class="centrarContTabla">Pol&iacute;tica Proyectos</p>
                    </div>
                </div>
            </div>

            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">12</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileProve" name="fileProve" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textProve"><?php if($PolProve != ""){ echo $PolProve; } else { echo 'Administraci&oacute;n de proveedores'; } ?></p>
                        <div class="btn-elim" id="elimProve" <?php if($PolProve == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnProve">
                        <p class="centrarContTabla">Pol&iacute;tica Proveedores</p>
                    </div>
                </div>
            </div>
        </div>

        <div style="margin-right:-10px;">
            <div class="botonesSAM boton1" id="btnAgrPC1012">Finalizar</div>
            <div class="botonesSAM boton5" onclick="location.href='pc7-9.php';">Atras</div>
        </div>
    </div>
</form>

<script>
    var tipoArchivo = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    $(document).ready(function(){
        $("#btnProgr").click(function(){
            $("#cargarArchivo").val("#textProgr");
            $("#fileProgr").click();
        });

        $("#btnProye").click(function(){
            $("#cargarArchivo").val("#textProye");
            $("#fileProye").click();
        });

        $("#btnProve").click(function(){
            $("#cargarArchivo").val("#textProve");
            $("#fileProve").click();
        });

        $("#fileProgr").change(function(e){
            if (msie > 0){
                $("#textProgr").empty();
                $("#textProgr").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#fileProye").change(function(e){
            if (msie > 0){
                $("#textProye").empty();
                $("#textProye").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#fileProve").change(function(e){
            if (msie > 0){
                $("#textProve").empty();
                $("#textProve").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#btnAgrPC1012").click(function(){
            $("#fondo").show();
            if (msie > 0) // If Internet Explorer, return version number
            {
                $("#formPC1012").submit();
                location.href='<?= $GLOBALS["domain_root"] ?>/sam/procesosSam/';
            }
            else  // If another browser, return 0
            {
                if($("#fileFinan").val() === "" && $("#fileLegis").val() === "" && $("#filePolit").val() === ""){
                    location.href='<?= $GLOBALS["domain_root"] ?>/sam/procesosSam/';
                }
                else{
                    $("#tokenPC1012").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#formPC1012")[0]);	
                    $.ajax({
                        type: "POST",
                        url: "<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/guardarPC10-12.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            $("#fondo").hide();
                            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                                    
                            if(data[0].result === 0){
                                $.alert.open("alert", "No se guardó ningún archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="pc10-12.php";
                                }); 
                            }
                            else if(data[0].result === 1){
                                $.alert.open("info", "Se guardó " + data[0].result + " archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="<?= $GLOBALS["domain_root"] ?>/sam/procesosSam/";
                                });
                            }
                            else{
                                $.alert.open("info", "Se guardó " + data[0].result + " archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="<?= $GLOBALS["domain_root"] ?>/sam/procesosSam/";
                                });
                            }
                        }
                    })
                    .fail(function(jqXHR){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                            location.href="pc10-12.php";
                        });
                    });
                }
            }
        });

        $("#elimProgr").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "programas", archivoActual : $("#actualPolProgr").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc10-12.php";
                    });
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#elimProye").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "proyectos", archivoActual : $("#actualPolProye").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc10-12.php";
                    });
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#elimProve").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "proveedores", archivoActual : $("#actualPolProve").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc10-12.php";
                    });
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });
    });

    function addArchivo(e){
        file = e.target.files[0]; 
        if (!file.type.match(tipoArchivo) && !file.type.match("application/pdf")){
            alert("El archivo a cargar debe ser .docx");
            return false;
        }
        else if(parseInt(file.size) / 1024 > 5120 ){
            alert("El tamaño permitido es de 5 MB");
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $($("#cargarArchivo").val()).empty();
            $($("#cargarArchivo").val()).append(file.name);
        }
    }

    function fileOnload(e) {
        result=e.target.result;
    }
</script>    