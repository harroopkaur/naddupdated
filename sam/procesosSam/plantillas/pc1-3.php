<div class="textog negro" style="font-size:28px; margin:20px; text-align:center;">Pol&iacute;ticas: <p style="color:#06B6FF; display:inline">Adquisici&oacute;n, Identificaci&oacute;n, Conformidad</p></div>

<br>
<div class="contenedorProcesosSamLeft">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/nuebe.jpg" class="tamNube">

    <br><br><br><br>
    <p style="font-size:20px;">Las Pol&iacute;ticas de los Procesos Claves (PC) del SAM</p> 
</div>

<form id="formPC13" name="formPC13" method="post" enctype="multipart/form-data">
    <input type="hidden" id="tokenPC13" name="token">
    <input type="hidden" id="actualPolAdqui" name="actualPolAdqui" value="<?= $PolAdqui ?>">
    <input type="hidden" id="actualPolIdent" name="actualPolIdent" value="<?= $PolIdent ?>">
    <input type="hidden" id="actualPolConform" name="actualPolConform" value="<?= $PolConform ?>">
    <input type="hidden" id="cargarArchivo" name="cargarArchivo">

    <div class="contenedorProcesosSamRight">		
        <div class="contenedorTabla">
            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">1</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileAdqui" name="fileAdqui" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textAdqui"><?php if($PolAdqui != ""){ echo $PolAdqui; } else{ echo 'Administraci&oacute;n de adquisici&oacute;n'; } ?></p>
                        <div class="btn-elim" id="elimAdqui" <?php if($PolAdqui == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnAdqui">
                        <p class="centrarContTabla">Pol&iacute;tica Adquisici&oacute;n</p>
                    </div>
                </div>
            </div>

            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">2</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileIdent" name="fileIdent" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textIdent"><?php if($PolIdent != ""){ echo $PolIdent; } else{ echo 'Administraci&oacute;n de identificaci&oacute;n de activos'; } ?></p>
                        <div class="btn-elim" id="elimIdent" <?php if($PolIdent == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnIdent">
                        <p class="centrarContTabla">Pol&iacute;tica Identificaci&oacute;n</p>
                    </div>
                </div>
            </div>

            <div class="contenidosTabla">
                <div class="columna1Tabla">
                    <div class="cuadrado1">3</div>
                </div>
                <div class="columna2Tabla">
                    <div class="alinearCirculo">
                        <div class="circulo"></div>
                    </div>
                    <div class="contProcesos estiloTabla">
                        <input type="file" id="fileConform" name="fileConform" style="display:none;" accept=".docx, .pdf">
                        <p class="centrarContTabla" id="textConform"><?php if($PolConform != ""){ echo $PolConform; } else{ echo 'Administraci&oacute;n de conformidad'; } ?></p>
                        <div class="btn-elim" id="elimConform" <?php if($PolConform == ""){ echo 'style="display:none;"'; } ?>>X</div>
                    </div>
                </div>
                <div class="columna3Tabla">
                    <div class="contArchivos activado1 estiloTabla" id="btnConform">
                        <p class="centrarContTabla">Pol&iacute;tica Conformidad</p>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-right:-10px;">
            <div class="botonesSAM boton1" id="btnAgrPC13">Guardar</div>
            <div class="botonesSAM boton5" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/procesosSam/';">Atras</div>
        </div>
    </div>
</form>

<script>
    var tipoArchivo = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    $(document).ready(function(){
        $("#btnAdqui").click(function(){
            $("#cargarArchivo").val("#textAdqui");
            $("#fileAdqui").click();
        });

        $("#btnIdent").click(function(){
            $("#cargarArchivo").val("#textIdent");
            $("#fileIdent").click();
        });

        $("#btnConform").click(function(){
            $("#cargarArchivo").val("#textConform");
            $("#fileConform").click();
        });

        $("#fileAdqui").change(function(e){
            if (msie > 0){
                $("#textAdqui").empty();
                $("#textAdqui").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#fileIdent").change(function(e){
            if (msie > 0){
                $("#textIdent").empty();
                $("#textIdent").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#fileConform").change(function(e){
            if (msie > 0){
                $("#textConform").empty();
                $("#textConform").append("Cargado");
            }else{
                addArchivo(e);
            }
        });

        $("#btnAgrPC13").click(function(){
            $("#fondo").show();
            if (msie > 0) // If Internet Explorer, return version number
            {
                $("#formPC13").submit();
                setTimeout(function(){
                    location.href="pc4-6.php";
                }, 5000);
            }
            else  // If another browser, return 0
            {
                if($("#fileAdqui").val() === "" && $("#fileIdent").val() === "" && $("#fileConform").val() === ""){
                    location.href="pc4-6.php";
                }
                else{
                    $("#tokenPC13").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#formPC13")[0]);	
                    $.ajax({
                        type: "POST",
                        url: "<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/guardarPC1-3.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            $("#fondo").hide();
                            <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                                    
                            if(data[0].result === 0){
                                $.alert.open("alert", "No se guardó ningún archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="pc1-3.php";
                                }); 
                            }
                            else if(data[0].result === 1){
                                $.alert.open("info", "Se guardó " + data[0].result + " archivo", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="pc4-6.php";
                                }); 
                            }
                            else{
                                $.alert.open("info", "Se guardó " + data[0].result + " archivos", {"Aceptar" : "Aceptar"}, function() {
                                    location.href="pc4-6.php";
                                });
                            }
                        }
                    })
                    .fail(function(jqXHR){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                            location.href="pc1-3.php";
                        });
                    });
                }
            }
        });

        $("#elimAdqui").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "adquisicion", archivoActual : $("#actualPolAdqui").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    }); 
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc1-3.php";
                    });
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#elimIdent").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "identificacion", archivoActual : $("#actualPolIdent").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc1-3.php";
                    });
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });

        $("#elimConform").click(function(){
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/procesosSam/ajax/eliminarArchivo.php", { opcion : "conformidad", archivoActual : $("#actualPolConform").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo eliminar el archivo", {"Aceptar" : "Aceptar"}, function() {
                    });
                }
                else{
                    $.alert.open("info", "Archivo eliminado con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="pc1-3.php";
                    });
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                });
            });
        });
    });

    function addArchivo(e){
        file = e.target.files[0]; 
        if (!file.type.match(tipoArchivo)  && !file.type.match("application/pdf")){
            alert("El archivo a cargar debe ser .docx");
            return false;
        }
        else if(parseInt(file.size) / 1024 > 5120 ){
            alert("El tamaño permitido es de 5 MB");
            return false;
        }
        else{
            reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            $($("#cargarArchivo").val()).empty();
            $($("#cargarArchivo").val()).append(file.name);
        }
    }

    function fileOnload(e) {
        result=e.target.result;
    }
</script>