<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/politicas.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $eliminarArchivo = new politicasSam();

                $tabla = $eliminarArchivo->politicas($_SESSION["client_id"]);
                foreach($tabla as $row){
                        $carpeta   = $row["carpeta"];
                }
                
                $opcion = "";
                if(isset($_POST["opcion"])){
                    $opcion = $general->get_escape($_POST["opcion"]);
                }
                
                $archivoActual = "";
                if(isset($_POST["archivoActual"])){
                    $archivoActual = $general->get_escape($_POST["archivoActual"]);
                }

                $elim = 0;

                if($eliminarArchivo->updateArchivo($_SESSION["client_id"], $opcion, "") == 1){
                        if(file_exists ( $GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivoActual)){
                                unlink($GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivoActual);
                        }
                        $elim = 1;
                }

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$elim, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);