<?php
require_once("../../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/politicas.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");
$nuevas_politicas = new politicasSam();
$log = new log();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $tabla = $nuevas_politicas->politicas($_SESSION["client_id"]);
            foreach($tabla as $row){
                    $carpeta = $row["carpeta"]; 
            }

            $archivo1 = "";
            $archivo2 = "";
            $archivo3 = "";

            if(isset($_POST["actualPolProgr"])){
                    $archivo1 = $general->get_escape($_POST["actualPolProgr"]);
            }

            if(isset($_POST["actualPolProye"])){
                    $archivo2 = $general->get_escape($_POST["actualPolProye"]);
            }

            if(isset($_POST["actualPolProve"])){
                    $archivo3 = $general->get_escape($_POST["actualPolProve"]);
            }

            $i = 0;
            if(is_uploaded_file($_FILES["fileProgr"]["tmp_name"]) ){	
                    if($nuevas_politicas->updateArchivo($_SESSION["client_id"], "programas", $_FILES['fileProgr']['name']) == 1){
                            if($archivo1 != "" && file_exists ( $GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivo1)){
                                    unlink($GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivo1);
                            }
                            move_uploaded_file($_FILES['fileProgr']['tmp_name'], $GLOBALS['app_root1'] . "/" . $carpeta . "/" .  $_FILES["fileProgr"]["name"]);
                            $i++;
                    }
            }

            if(is_uploaded_file($_FILES["fileProye"]["tmp_name"]) ){	
                    if($nuevas_politicas->updateArchivo($_SESSION["client_id"], "proyectos", $_FILES['fileProye']['name']) == 1){
                            if($archivo2 != "" && file_exists ( $GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivo2)){
                                    unlink($GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivo2);
                            }
                            move_uploaded_file($_FILES['fileProye']['tmp_name'], $GLOBALS['app_root1'] . "/" . $carpeta . "/" . $_FILES["fileProye"]["name"]);
                            $i++;
                    }
            }

            if(is_uploaded_file($_FILES["fileProve"]["tmp_name"]) ){	
                    if($nuevas_politicas->updateArchivo($_SESSION["client_id"], "proveedores", $_FILES['fileProve']['name']) == 1){
                            if($archivo3 != "" && file_exists ( $GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivo3)){
                                    unlink($GLOBALS['app_root1'] . "/" . $carpeta . "/" . $archivo3);
                            }
                            move_uploaded_file($_FILES['fileProve']['tmp_name'], $GLOBALS['app_root1'] . "/" . $carpeta . "/" . $_FILES["fileProve"]["name"]);
                            $i++;
                    }
            }

            $log->insertar(32, 2, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$i, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);