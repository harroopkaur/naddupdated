<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS['app_root'] . "/sam/clases/politicas.php");
$nueva_funcion = new funcionesSam();
$nuevas_politicas = new politicasSam();
$general = new General();
$PolFinan   = "";
$PolLegis   = "";
$PolPolit   = "";
$carpeta    = "";

$tabla = $nuevas_politicas->politicas($_SESSION["client_id"]);
foreach($tabla as $row){
    $PolFinan   = $row["financiera"];
    $PolLegis   = $row["legislacion"];
    $PolPolit   = $row["politicas"]; 
    $carpeta    = $row["carpeta"];
}