<?php
require_once("../configuracion/inicio.php");
require_once("../clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam($conn);

//inicio middleware
require_once("../clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>'false', 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = 'true';
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $opcion = "";
                if(isset($_POST["opcion"])){
                    $opcion = $general->get_escape($_POST["opcion"]);
                }
                
                $div = '<img src="' . $GLOBALS['domain_root'] . '/imagenes/sam/General.png" style="width:100%; height:auto;">';

                $menuSuperior = "";
                if($opcion == "true"){
                        $menuSuperior = $nueva_funcion->menu3("procesos", $_SESSION["client_empleado"]);
                        $div .= $nueva_funcion->getFuncionesMenuSuperior();
                }

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div,
                'menuSuperior'=>$menuSuperior, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
?>