<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/acceso.php");
$nuevo_acceso = new accesoSam();

$array = "";
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $general = new General();
    $result = 0;
    $login = "";
    if(isset($_POST["login"])){
        $login   = $general->get_escape($_POST["login"]); 
    }
    
    $pass = "";
    if(isset($_POST["pass"])){
        $pass    = $general->get_escape($_POST["pass"]);
    }
    
    $carpeta = "";
    if(isset($_POST["carpeta"])){
        $carpeta = $general->get_escape($_POST["carpeta"]);
    }

    if (count($nuevo_acceso->validarLoginSam($login, $carpeta)) == 0) {
        $result = 2;
    } else {
        $tabla = $nuevo_acceso->validarAccesoCarpetas($login, $pass, $carpeta);
        if (count($tabla) > 0) {
            foreach ($tabla as $row) {
                $_SESSION["idClienteSam"] = $row["idCliente"];
                $_SESSION["nombreSam"] = $login; //$row["nombre"];
                $_SESSION["apellidoSam"] = ""; //$row["apellido"];
                $_SESSION["empresaSam"] = $row["empresa"];
                $_SESSION["carpeta"] = $carpeta;
            }
            $_SESSION["usuLogueado"] = true;

            $result = 1;
        }
    }
    $array = array(0 => array('mensaje' => '', 'result' => $result));
}
echo json_encode($array);