<div style="float:left; width:19%; border:1px solid; padding:10px; text-align:center;">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/inicio/nuebe.jpg" style="width:150px;">
    <br>
    <br>
    <p style="font-size:20px; padding:50px 0px 120px 0px;">Acceso para los usuarios de las pol&iacute;ticas y formularios</p> 
</div>

<input type="hidden" id="carpetaActual" name="carpetaActual" value="<?= $link ?>">

<div style="float:right; width:73%; padding:90px 0px 90px 0px; ">
    <div style="overflow:hidden; margin: 1em 25%;">
        <p style="float:left; line-height:30px; width:50px; text-align:right;">Usuario</p>
        <input type="text" id="login" name="login" style="float:left; margin-left:15px; height:25px; width:200px;" value="<?= $login ?>" maxlength="50">
        <br style="clear:left;">
        <br>
        <p style="float:left; line-height:30px; width:50px; text-align:right;">Clave</p>
        <input type="password" id="pass" name="pass" style="float:left; margin-left:15px; height:25px; width:200px;" value="<?= $pass ?>" maxlength="100">
        <br style="clear:left;">
        <br>
        <p style="float:left; line-height:30px; width:50px; text-align:right;">&nbsp;</p>
        <p style="float:left; margin-left:15px; height:25px; width:200px;">www.licensingassurance.com/</p>
        <br style="clear:left;">
        <p style="float:left; line-height:30px; width:50px; text-align:right;">Link</p>
        <input type="text" id="link" name="link" style="float:left; margin-left:15px; height:25px; width:200px;" value="<?= $link ?>" onkeyup = "return validarLink(this.value)" maxlength="50">
    </div>
</div>
<br style="clear:right; ">
<br>
<div style="float:right;" class="botonesSAM boton5" id="guardarAcceso">Guardar</div>

<script>
    var validarLetras   = /^[a-zA-Z]+$/;
    var click = 1;
    $(document).ready(function(){
        $("#guardarAcceso").click(function(){
            if($("#login").val() === ""){
                alert("Debe llenar el usuario");
                return false;
            }
            if($("#pass").val() === ""){
                alert("Debe llenar la clave");
                return false;
            }
            if($("#link").val() === ""){
                alert("Debe llenar el link");
                return false;
            }
            click++;
            $("#fondo").show();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/acceso/ajax/guardarAcceso.php", { login : $("#login").val(), pass : $("#pass").val(), link : $("#link").val(), carpetaActual : $("#carpetaActual").val(), token : localStorage.licensingassuranceToken }, function(data){
                $("#fondo").hide();
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                if(data[0].result === 0){
                    $.alert.open("alert", "No se pudo guardar la información", {"Aceptar" : "Aceptar"}, function() {
                        click = 0;
                    });
                }
                else if(data[0].result === 1){
                    $.alert.open("info", "información guardada con éxito", {"Aceptar" : "Aceptar"}, function() {
                        location.href="<?= $GLOBALS["domain_root"] . "/sam/acceso/" ?>";
                    });
                }
                else if(data[0].result === 2){
                    $.alert.open("alert", "El link ya existe, coloque otro nombre en el link", {"Aceptar" : "Aceptar"}, function() {
                        click = 0;
                    });
                }
                else if(data[0].result === -2){
                    $.alert.open("alert", "Ya existe el usuario", {"Aceptar" : "Aceptar"}, function() {
                        click = 0;
                    });
                }
                else{
                    $.alert.open("alert", "No se pudo verificar el usuario", {"Aceptar" : "Aceptar"}, function() {
                        click = 0;
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                    click = 0;
                });
            });
        });
    });

    function validarLink(texto){
        if(!validarLetras.test(texto)){
            texto = texto.substring(0,texto.length-1);
            $("#link").val(texto);
        }
    }
</script>