<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS['app_root'] . "/sam/clases/acceso.php");
$nueva_funcion = new funcionesSam();
$nuevo_acceso  = new accesoSam();
$general = new General();
$tabla = $nuevo_acceso->datosAcceso($_SESSION["client_id"]);
$login = "";
$pass  = "";
$link  = "";
foreach($tabla as $row){
    $login = $row["login"];
    $pass  = $row["pass"];
    $link  = $row["carpeta"];
}