<?php
require_once("../../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/acceso.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");
$nuevo_acceso = new accesoSam();
$log = new log();

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $login = "";
            if(isset($_POST["login"])){
                $login = $general->get_escape($_POST["login"]);
            }
            
            $pass = "";
            if(isset($_POST["pass"])){
                $pass = $general->get_escape($_POST["pass"]);
            }
            
            $link = "";
            if(isset($_POST["link"])){
                $link = $general->get_escape($_POST["link"]);
            }
            
            $carpetaActual =  "";
            if(isset($_POST["carpetaActual"])){
                $carpetaActual = $general->get_escape($_POST["carpetaActual"]);
            }

            $tabla = $nuevo_acceso->datosAcceso($_SESSION["client_id"]);

            $result = $nuevo_acceso->validarLogin($login, $_SESSION["client_id"]);

            if ($result == 1) {
                if (!file_exists($GLOBALS['app_root1'] . "/" . $link) || (file_exists($GLOBALS['app_root1'] . "/" . $link) && $link == $carpetaActual)) {
                    if (count($tabla) > 0) {
                        foreach ($tabla as $row) {
                            if ($row["pass"] == $pass) {
                                $result = $nuevo_acceso->editarAcceso1($_SESSION["client_id"], $login, $link);
                                if ($result == 1) {
                                    if (file_exists($GLOBALS['app_root1'] . "/" . $carpetaActual)) {
                                        rename($GLOBALS['app_root1'] . "/" . $carpetaActual, $GLOBALS['app_root1'] . "/" . $link);
                                        copy($GLOBALS['app_root'] . "/sam/inicioCarpetasSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/index.php");
                                        copy($GLOBALS['app_root'] . "/plantillas/salidaSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php");
                                    }
                                    else{
                                        mkdir($GLOBALS['app_root1'] . "/" . $link);
                                        $i = 0;
                                        do {
                                            copy($GLOBALS['app_root'] . "/sam/inicioCarpetasSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/index.php");
                                            $i++;
                                        } while (!file_exists($GLOBALS['app_root1'] . "/" . $link . "/index.php") || $i < 5);


                                        $i = 0;
                                        do {
                                            copy($GLOBALS['app_root'] . "/plantillas/salidaSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php");
                                            $i++;
                                        } while (!file_exists($GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php") || $i == 5);
                                    }
                                }
                            } else {
                                $result = $nuevo_acceso->editarAcceso($_SESSION["client_id"], $login, $pass, $link);
                                if ($result == 1) {
                                    if (file_exists($GLOBALS['app_root1'] . "/" . $carpetaActual)) {
                                        rename($GLOBALS['app_root1'] . "/" . $carpetaActual, $GLOBALS['app_root1'] . "/" . $link);
                                        copy($GLOBALS['app_root'] . "/sam/inicioCarpetasSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/index.php");
                                        copy($GLOBALS['app_root'] . "/plantillas/salidaSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php");
                                    }
                                    else{
                                        mkdir($GLOBALS['app_root1'] . "/" . $link);
                                        $i = 0;
                                        do {
                                            copy($GLOBALS['app_root'] . "/sam/inicioCarpetasSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/index.php");
                                            $i++;
                                        } while (!file_exists($GLOBALS['app_root1'] . "/" . $link . "/index.php") || $i < 5);


                                        $i = 0;
                                        do {
                                            copy($GLOBALS['app_root'] . "/plantillas/salidaSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php");
                                            $i++;
                                        } while (!file_exists($GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php") || $i == 5);
                                    }
                                }
                            }
                        }
                    } else {
                        $result = $nuevo_acceso->guardarAcceso($_SESSION["client_id"], $login, $pass, $link);
                        if ($result == 1) {
                            mkdir($GLOBALS['app_root1'] . "/" . $link);
                            $i = 0;
                            do {
                                copy($GLOBALS['app_root'] . "/sam/inicioCarpetasSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/index.php");
                                $i++;
                            } while (!file_exists($GLOBALS['app_root1'] . "/" . $link . "/index.php") || $i < 5);


                            $i = 0;
                            do {
                                copy($GLOBALS['app_root'] . "/plantillas/salidaSAM.php", $GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php");
                                $i++;
                            } while (!file_exists($GLOBALS['app_root1'] . "/" . $link . "/salidaSAM.php") || $i == 5);
                        }
                    }
                } else {
                    $result = 2;
                }
                
                $log->insertar(35, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
            }
            
            $array = array(0 => array('sesion'=>$sesion, 'mensaje'=>'', 'result' => $result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);