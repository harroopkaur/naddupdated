<div style="width:calc(100% - 40px); padding:20px; overflow:hidden;">
    <form id="form1" name="form1" method="get" action="index.php">
        <table style="width:100%; border:1px solid;">
            <thead style="background-color:#C1C1C1;">
                <tr>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="empleado" name="empleado" value="<?= $empleado ?>" maxlength="20" style="width:120px;"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="IP" name="IP" value="<?= $IP ?>" maxlength="12" style="width:120px;"></th>                
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                        <select id="modulo" name="modulo" style="width:120px;">
                            <option value="">--Seleccione--</option>
                            <?php 
                            foreach($modulos as $row){
                            ?>
                                <option value="<?= $row["idDetalle"] ?>" <?php if($row["idDetalle"] == $idModulo){ echo "selected='selected'"; } ?>><?= $row["descripcion"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                        <select id="operacion" name="operacion" style="width:120px;">
                            <option value="">--Seleccione--</option>
                            <?php 
                            foreach($operaciones as $row){
                            ?>
                                <option value="<?= $row["idDetalle"] ?>" <?php if($row["idDetalle"] == $idOperacion){ echo "selected='selected'"; } ?>><?= $row["descripcion"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="descrip" name="descrip" value="<?= $descrip ?>" maxlength="20" style="width:120px;"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="fecha" name="fecha" value="<?= $fecha ?>" maxlength="20" style="width:120px;"></th>
                </tr>
                <tr>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Empleado</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">IP</th>                
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Módulo</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Operación</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Descripción</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($listado as $row){
                ?>
                    <tr>
                        <td><?= $row["nombre"] ?></td>
                        <td><?= $row["IP"] ?></td>
                        <td><?= $row["modulo"] ?></td>
                        <td><?= $row["operacion"] ?></td>
                        <td><?= $row["comentario"] ?></td>
                        <td><?= $row["fecha"] ?></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        
        <br><br>
        <div style="float:right; margin-right:20px; margin-bottom:20px;"><div class="botones_m2 boton1" onclick="$('#form1').submit();">Buscar</div></div>
    </form>
</div>     