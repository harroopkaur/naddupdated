<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
$general = new General();
$nueva_funcion = new funcionesSam();

$log = new log();
$tablaMaestra = new tablaMaestra();

$modulos = $tablaMaestra->listadoTablaMaestra(22);
$operaciones = $tablaMaestra->listadoTablaMaestra(23);

$idModulo = "";
if(isset($_GET["modulo"]) && is_numeric($_GET["modulo"])){
    $idModulo = intval($_GET["modulo"]);
}

$idOperacion = "";
if(isset($_GET["operacion"]) && is_numeric($_GET["operacion"])){
    $idOperacion = intval($_GET["operacion"]);
}

$descrip = "";
if(isset($_GET["descrip"])){
    $descrip = $general->get_escape($_GET["descrip"]);
}

$IP = "";
if(isset($_GET["IP"])){
    $IP = $general->get_escape($_GET["IP"]);
}

$empleado = "";
if(isset($_GET["empleado"])){
    $empleado = $general->get_escape($_GET["empleado"]);
}

$fecha = "";
if(isset($_GET["fecha"])){
    $fecha = $general->get_escape($_GET["fecha"]);
}

$nose = "";
if(isset($_GET["fecha"])){
    $nose = $general->get_escape($_GET["fecha"]);
}

$listado = $log->listadoLogPaginado($idModulo, $idOperacion, $_SESSION["client_id"], $empleado, $IP, $descrip, $fecha);