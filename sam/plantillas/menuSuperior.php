<?php
if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 2) && $opcionLateral == "procesos") {
?>
    <div class="botones_m2 boton2 <?php if($opcionSuperior == "procesos"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/procesosSam/';">General</div>
    <div class="botones_m2 boton2 <?php if($opcionSuperior == "PC13"){ echo 'activado1'; } ?>" onclick="location.href='pc1-3.php';">PC 1-3</div>
    <div class="botones_m2 boton2 <?php if($opcionSuperior == "PC46"){ echo 'activado1'; } ?>" onclick="location.href='pc4-6.php';">PC 4-6</div>
    <div class="botones_m2 boton2 <?php if($opcionSuperior == "PC79"){ echo 'activado1'; } ?>" onclick="location.href='pc7-9.php';">PC 7-9</div>
    <div class="botones_m2 boton2 <?php if($opcionSuperior == "PC1012"){ echo 'activado1'; } ?>" onclick="location.href='pc10-12.php';">PC 10-12</div>
<?php
} else {
    if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 3) && $opcionLateral == "sam") {
    ?>    
        <div class="botones_m2 boton2 <?php if ($opcionSuperior == "sam"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/repositorios/';" title="Portada del módulo">General</div>
        <div class="botones_m2 boton2 <?php if ($opcionSuperior == "contratos"){ echo 'activado1'; } ?>" onclick="location.href='listadoContratos.php';" title="Incluir, editar, eliminar contratos">Contratos</div>
        <div class="botones_m2 boton2 <?php if ($opcionSuperior == "licencias"){ echo 'activado1'; } ?>" onclick="location.href='listadoLicencias.php';" title="Listar licencias por Software/Asignación">Licencias</div>
        <div class="botones_m2 boton2 <?php if ($opcionSuperior == "despliegue"){ echo 'activado1'; } ?>" onclick="location.href='despliegue.php';" title="Historial de Despliegue">Despliegue</div>
    <?php
    } else {
        if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 4) && $opcionLateral == "acceso") {
        ?>
            <div class="botones_m2 boton2 <?php if ($opcionSuperior == "acceso"){ echo 'activado1'; } ?>" onclick="location.href='<?php $GLOBALS["domain_root"] ?> /sam/acceso/';">General</div>
        <?php
        } else {
            if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 21) && $opcionLateral == "asignacion") {
            ?>
                <div class="botones_m2 boton2 <?php if ($opcionSuperior == "asignacion"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/asignacion/';">General</div>
                <div class="botones_m2 boton2 <?php if ($opcionSuperior == "listado"){ echo 'activado1'; } if(!$nueva_funcion->existe_permiso($_SESSION["client_empleado"], 18)){ echo ' hide'; } ?>" onclick="location.href='listado.php';">Listado</div>
                <div class="botones_m2 boton2 <?php if ($opcionSuperior == "historial"){ echo 'activado1'; } if(!$nueva_funcion->existe_permiso($_SESSION["client_empleado"], 18)){ echo ' hide'; }  ?>" onclick="location.href='historial.php';">Historial Traslado</div>
                <div class="botones_m2 boton2 <?php if ($opcionSuperior == "traslado"){ echo 'activado1'; } if(!$nueva_funcion->existe_permiso($_SESSION["client_empleado"], 19)){ echo ' hide'; }  ?>" onclick="location.href='traslado.php';">Traslado</div>
                <div class="botones_m2 boton2 <?php if ($opcionSuperior == "aprobacion"){ echo 'activado1'; } if(!$nueva_funcion->existe_permiso($_SESSION["client_empleado"], 20)){ echo ' hide'; }  ?>" onclick="location.href='aprobacion.php';">Aprobación</div>
            <?php
            }
        }
    }
}