<?php
if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 4)) {
?>
    <div class="botones_m3Sam boton2 <?php if($opcionLateral == "acceso"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/acceso/';" title="Crear enlace externo para acceso a políticas de licenciamiento">Accesos SAM</div>
<?php
}

if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 21)) {
?>
    <div class="botones_m3Sam boton2 <?php if($opcionLateral == "asignacion"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/asignacion/';" title="Acceso a Asignación">Asignación</div>
<?php
}

if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 17)) {
?>
    <div class="botones_m3Sam boton2 <?php if($opcionLateral == "centroCostos"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/centroCostos/';" title="Asignación de Centro de Costos">Centro Costos</div>
<?php
}

if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 22)) {
?>
    <div class="botones_m3Sam boton2 <?php if($opcionLateral == "log"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/log/';" title="Log de Eventos">Log</div>
<?php
}

if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 2)) {   
?>
    <div class="botones_m3Sam boton2 <?php if($opcionLateral == "procesos"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/procesosSam/';" title="Acceso a políticas de licenciamiento de software">Procesos SAM</div>
<?php
}

if ($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 3)) {
?>
    <div class="botones_m3Sam boton2 <?php if($opcionLateral == "sam"){ echo 'activado1'; } ?>" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/sam/repositorios/';" title="Acceso a la información de licenciamiento">Repositorios</div>
<?php
}