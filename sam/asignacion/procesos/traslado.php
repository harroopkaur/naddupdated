<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS['app_root'] . "/clases/clase_trasladoCabLicencia.php");
require_once($GLOBALS['app_root'] . "/clases/clase_trasladoDetLicencia.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$nueva_funcion = new funcionesSam();
$general = new General();
$cabTraslado = new cabTrasladoLicencia();
$detTraslado = new detTrasladoLicencia();
$log = new log();

$listadoAsignacion = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
$fabricantes = $nueva_funcion->obtenerFabricantes(); //$nueva_funcion->obtenerFabricantesWindows();
$numTraslado = $cabTraslado->obtenerUltId() + 1;

$agregar = 0;
$exito = 0;
if(isset($_POST["insertar"]) && $_POST["insertar"] == 1){
    $agregar = 1;
    $descripcion = "";
    if(isset($_POST["descripcion"])){
        $descripcion = $general->truncarString($general->get_escape($_POST["descripcion"]), 70);
    }
    
    $observacion = "";
    if(isset($_POST["observacion"])){
        $observacion = $general->truncarString($general->get_escape($_POST["observacion"]), 250);
    }
    
    if($cabTraslado->insertar($_SESSION["client_id"], $_SESSION["client_empleado"], $descripcion, $observacion)){
        $id = $cabTraslado->obtenerUltId();
        $licencias = array();
        if(isset($_POST["valores"])){
            $licencias = $_POST["valores"];
        }
       
        $disponible = array();
        if(isset($_POST["dispon"])){
            $disponible = $_POST["dispon"];
        }
        
        $cantidad = array();
        if(isset($_POST["cantTrasla"])){
            $cantidad = $_POST["cantTrasla"];
        }
        
        $asignacion = array();
        if(isset($_POST["asigTras"])){
            $asignacion = $_POST["asigTras"];
        }
        
        $j = 0;
        for($i = 0; $i < count($licencias); $i++){
            $idDetalle = 0;
            if(filter_var($licencias[$i], FILTER_VALIDATE_INT) !== false){
                $idDetalle = $licencias[$i];
            }
            
            $disponTras = 0;
            if(filter_var($disponible[$i], FILTER_VALIDATE_INT) !== false){
                $disponTras = $disponible[$i];
            }
            
            $cantidadTras = 0;
            if(filter_var($cantidad[$i], FILTER_VALIDATE_INT) !== false){
                $cantidadTras = $cantidad[$i];
            }
            
            $asigTras = $general->get_escape($asignacion[$i]);
            
            if($detTraslado->insertar($id, $idDetalle, $disponTras, $cantidadTras, $asigTras)){
                $j++;
            }
        }
        
        if($j == $i){
            $exito = 1;
        } else{
            $exito = 2;
        }
        
        $log->insertar(34, 1, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
    }
}