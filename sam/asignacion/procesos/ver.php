<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS['app_root'] . "/clases/clase_trasladoCabLicencia.php");
require_once($GLOBALS['app_root'] . "/clases/clase_trasladoDetLicencia.php");

$nueva_funcion = new funcionesSam();
$general = new General();
$cabTraslado = new cabTrasladoLicencia();
$detTraslado = new detTrasladoLicencia();

$id = 0;
if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $id = $_GET["id"];
}

$dataCab = $cabTraslado->dataTraslado($id);
$dataDet = $detTraslado->dataTraslado($id);
