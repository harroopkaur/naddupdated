<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();
$general = new General();

$listadoAsignacion = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
$fabricantes = $nueva_funcion->obtenerFabricantes(); //$nueva_funcion->obtenerFabricantesWindows();

$exito = 0;
$agregar = 0;
if(isset($_POST["insertar"]) && $_POST["insertar"] == 1){
    $agregar = 1;
    $idDetalleContrato = $_POST["idDetalleContrato"];
    $cantManual = $_POST["cantManual"];
    $instManual = $_POST["instManual"];
    $j = 0;
    for($i = 0; $i < count($idDetalleContrato); $i++){
        $id = 0;
        if(filter_var($idDetalleContrato[$i], FILTER_VALIDATE_INT) !== false){
            $id = $idDetalleContrato[$i];
        } 
        
        $cantidad = 0;
        if(filter_var($instManual[$i], FILTER_VALIDATE_INT) !== false){
            $cantidad = $instManual[$i];
        } 
        
        $cantidadManual = 0;
        if(filter_var($cantManual[$i], FILTER_VALIDATE_INT) !== false){
            $cantidadManual = $cantManual[$i];
        } 
        
        if($nueva_funcion->actualizarInstalacionManual($id, $cantidadManual, $cantidad)){
            $j++;
        }
    }
    
    if($j == $i){
        $exito = 1;
    } else if($j < $i && $j > 0){
        $exito = 2;
    }
}