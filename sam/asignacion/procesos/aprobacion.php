<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/clases/clase_trasladoCabLicencia.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$nueva_funcion = new funcionesSam();
$general = new General();
$cabLicencia = new cabTrasladoLicencia();
$log = new log();

$historial = $cabLicencia->historialPaginadoPorAprobar($_SESSION["client_id"]);

$eliminar = 0;
$exito = 0;
if(isset($_POST["eliminar"]) && $_POST["eliminar"] == 1){
    $eliminar = 1;
    $id = 0;
    if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
        $id = $_POST["id"];
    }
    
    if($cabLicencia->actualizarEstado($id, 0)){
        $exito = 1;
    }
    
    $log->insertar(34, 3, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
}