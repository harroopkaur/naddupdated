<?php
//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
require_once($GLOBALS['app_root'] . "/clases/clase_trasladoCabLicencia.php");
require_once($GLOBALS['app_root'] . "/clases/clase_trasladoDetLicencia.php");
require_once($GLOBALS["app_root"] . "/clases/clase_log.php");

$nueva_funcion = new funcionesSam();
$general = new General();
$cabTraslado = new cabTrasladoLicencia();
$detTraslado = new detTrasladoLicencia();
$log = new log();

$id = 0;
if(isset($_GET["id"]) && filter_var($_GET["id"], FILTER_VALIDATE_INT) !== false){
    $id = $_GET["id"];
}

$listadoAsignacion = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
$fabricantes = $nueva_funcion->obtenerFabricantesWindows();

$agregar = 0;
$exito = 0;
if(isset($_POST["insertar"]) && ($_POST["insertar"] == 1 || $_POST["insertar"] == 2)){
    $agregar = $_POST["insertar"];
    $id = 0;
    if(isset($_POST["idTraslado"]) && filter_var($_POST["idTraslado"], FILTER_VALIDATE_INT) !== false){
        $id = $_POST["idTraslado"];
    }
    
    $descripcion = "";
    
    $licencias = array();
    if(isset($_POST["valores"])){
        $licencias = $_POST["valores"];
    }

    $disponible = array();
    if(isset($_POST["dispon"])){
        $disponible = $_POST["dispon"];
    }

    $cantidad = array();
    if(isset($_POST["cantTrasla"])){
        $cantidad = $_POST["cantTrasla"];
    }
    
    $asignacion = array();
    if(isset($_POST["asigTras"])){
        $asignacion = $_POST["asigTras"];
    }
    
    if($agregar == 1){
        if(isset($_POST["descripcion"])){
            $descripcion = $general->truncarString($general->get_escape($_POST["descripcion"]), 70);
        }

        $observacion = "";
        if(isset($_POST["observacion"])){
            $observacion = $general->truncarString($general->get_escape($_POST["observacion"]), 250);
        }

        if($cabTraslado->actualizar($id, $descripcion, $observacion)){
            $detTraslado->eliminarIdTraslado($id);
            
            $idDetTraslado = array();
            if(isset($_POST["idDetTraslado"])){
                $idDetTraslado = $_POST["idDetTraslado"];
            }

            $j = 0;
            for($i = 0; $i < count($licencias); $i++){
                $idDetalle = 0;
                if(filter_var($licencias[$i], FILTER_VALIDATE_INT) !== false){
                    $idDetalle = $licencias[$i];
                }
                
                $idTraslado = 0;
                if(filter_var($idDetTraslado[$i], FILTER_VALIDATE_INT) !== false){
                    $idTraslado = $idDetTraslado[$i];
                }

                $disponTras = 0;
                if(filter_var($disponible[$i], FILTER_VALIDATE_INT) !== false){
                    $disponTras = $disponible[$i];
                }

                $cantidadTras = 0;
                if(filter_var($cantidad[$i], FILTER_VALIDATE_INT) !== false){
                    $cantidadTras = $cantidad[$i];
                }

                $asigTras = $general->get_escape($asignacion[$i]);

                if($idTraslado == 0){
                    if($detTraslado->insertar($id, $idDetalle, $disponTras, $cantidadTras, $asigTras)){
                        $j++;
                    }
                } else{
                    if($detTraslado->actualizar($idTraslado, $cantidadTras, $asigTras)){
                        $j++;
                    }
                }
            }

            if($j == $i){
                $exito = 1;
            } else{
                $exito = 2;
            }
            
            $log->insertar(34, 4, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
        }
    } else {
        if($cabTraslado->actualizarEstado($id, 2)){
            $j = 0;
            for($i = 0; $i < count($licencias); $i++){
                $idDetalle = 0;
                if(filter_var($licencias[$i], FILTER_VALIDATE_INT) !== false){
                    $idDetalle = $licencias[$i];
                }

                $cantidadTras = 0;
                if(filter_var($cantidad[$i], FILTER_VALIDATE_INT) !== false){
                    $cantidadTras = $cantidad[$i];
                }

                $asigTras = $general->get_escape($asignacion[$i]);
                
                $descripProducto = $nueva_funcion->obtenerDescripProductos($idDetalle);
                
                $comprobacion = $nueva_funcion->comprobarDetalleContrato($descripProducto["idContrato"], $descripProducto["idProducto"], 
                $descripProducto["idEdicion"], $descripProducto["id"], $asigTras);
                if(count($comprobacion["idDetalleContrato"]) > 0){
                    if($nueva_funcion->sumarCantidadDetalleContrato($comprobacion["idDetalleContrato"], $cantidadTras) && $nueva_funcion->restarCantidadDetalleContrato($idDetalle, $cantidadTras)){
                        $j++;
                    }
                } else{
                    if($nueva_funcion->guardarDetalleContrato($descripProducto["idContrato"], $descripProducto["idProducto"], 
                    $descripProducto["idEdicion"], $descripProducto["id"], $descripProducto["sku"], $descripProducto["tipo"], 
                    $cantidadTras, $descripProducto["precio"], $asigTras) && $nueva_funcion->restarCantidadDetalleContrato($idDetalle, $cantidadTras)){
                        $j++;
                    }
                }
            }
            if($j == $i){
                $exito = 1;
            } else{
                $exito = 2;
            }
            
            $log->insertar(34, 2, $_SESSION["client_id"], $_SESSION["client_empleado"], $general->getRealIP(), "");
        }
    }
}

$dataCab = $cabTraslado->dataTraslado($id);
$dataDet = $detTraslado->dataTraslado($id);