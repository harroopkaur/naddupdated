<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                //$general = new General();                                
                $div = '';
                $i = 0;
                
                $asignacion = "";
                if(isset($_POST["asignacion"])){
                    $asignacion = $general->get_escape($_POST["asignacion"]);
                }
                
                $fabricante = 3;
                if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                    $fabricante = $_POST["fabricante"];
                }
                
                $contrato = "";
                if(isset($_POST["contrato"])){
                    $contrato = $general->get_escape($_POST["contrato"]);
                }
                
                $familia = "";
                if(isset($_POST["familia"])){
                    $familia = $general->get_escape($_POST["familia"]);
                }
                
                $edicion = "";
                if(isset($_POST["edicion"])){
                    $edicion = $general->get_escape($_POST["edicion"]);
                }
                
                $version = "";
                if(isset($_POST["version"])){
                    $version = $general->get_escape($_POST["version"]);
                }
                
                $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $query = $nueva_funcion->licenciasTrasladoSAM($_SESSION["client_id"], $fabricante, $contrato, 
                $familia, $edicion, $version, $asignacion, $asignaciones);
                foreach($query as $row){
                $div .= '<tr style="border-bottom-style: 1px solid #ddd">
                                <td style="text-align:center;"><input type="checkbox" id="check' . $i . '" name="check' . $i . '"
                                value="' . $row["numero"] . '*' . $row["producto"] . '*' . $row["edicion"] . '*' . $row["version"] . '*' . 
                                $row["asignacion"] . '*' . ($row["cantidad"] - $row["instalaciones"]) . '*' . $row["idDetalleContrato"] . '"></td>
                                <td>' . $row["nombre"] . '</td>
                                <td>' . $row["numero"] . '</td>
                                <td>' . $row["producto"] . '</td>
                                <td>' . $row["edicion"] . '</td>
                                <td>' . $row["version"] . '</td>
                                <td>' . $row["asignacion"] . '</td>';
                                if (($row["cantidad"] - $row["instalaciones"]) < ($row["cantManual"] - $row["instManual"])){
                                    $div .= '<td>' . ($row["cantidad"] - $row["instalaciones"]) . '</td>';
                                } else{
                                    $div .= '<td>' . ($row["cantManual"] - $row["instManual"]) . '</td>';
                                }
                        $div .= '</tr>';
                        $i++;
                }
                
                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);