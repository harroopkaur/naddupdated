<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS['app_root'] . "/clases/clase_general.php");
require_once($GLOBALS['app_root'] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();

//inicio middleware
require_once($GLOBALS['app_root'] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                //$general = new General();
                $asignacion = "";
                if(isset($_POST["asignacion"])){
                    $asignacion = $general->get_escape($_POST["asignacion"]);
                }
                
                $fabricante = 0;
                if(isset($_POST["fabricante"]) && filter_var($_POST["fabricante"], FILTER_VALIDATE_INT) !== false){
                    $fabricante = $_POST["fabricante"];
                }
                
                $limite = 0;
                if(isset($_POST["limite"]) && filter_var($_POST["limite"], FILTER_VALIDATE_INT) !== false){
                    $limite     = $_POST["limite"];
                }
                
                $pagina = 0;
                if(isset($_POST["pagina"]) && filter_var($_POST["pagina"], FILTER_VALIDATE_INT) !== false ||
                $_POST["pagina"] == "ultima"){
                    $pagina = $_POST["pagina"];
                }
                
                $asignaciones = $general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
                $total = count($nueva_funcion->balanzaAsignacionSAM($_SESSION["client_id"], $fabricante, $asignacion, $asignaciones));
                
                /*if($pagina == "ultima"){
                    $inicio = (ceil($total / $limite) - 1) * $limite;
                }
                else{               
                    $inicio = ($pagina - 1) * $limite; 
                }*/
                
                $div = '';
                $i = 0;
               
                $query = $nueva_funcion->balanzaAsignacionSAMPaginado($_SESSION["client_id"], $fabricante, $asignacion, $asignaciones, $pagina, $limite);
                foreach($query as $row){
                $div .= '<tr style="border-bottom-style: 1px solid #ddd">
                            <td style="text-align:center;">
                                <input type="hidden" id="idDetalleContrato' . $i . '" name="idDetalleContrato[]" value="' . $row["idDetalleContrato"] . '">
                                <input type="checkbox" id="check' . $i . '" name="check' . $i . '">
                            </td>
                            <td>' . $row["nombre"] . '</td>
                            <td>' . $row["numero"] . '</td>
                            <td>' . $row["producto"] . '</td>
                            <td>' . $row["edicion"] . '</td>
                            <td>' . $row["version"] . '</td>
                            <td>' . $row["cantidad"] . '</td>
                            <td>' . $row["instalaciones"] . '</td>
                            <td>' . ($row["cantidad"] - $row["instalaciones"]) . '</td>';
                            if($row["cantidad"] > 0){
                                $div .= '<td>' . round(($row["instalaciones"] / $row["cantidad"]) * 100, 0) . '</td>';
                            } else{
                                $div .= '<td>0</td>';
                            }
                            $div .= '<td><input type="text" id="cantManual' . $i . '" name="cantManual[]" style="width:50px;" maxlength="4" value="' . $row["cantManual"] . '"></td>
                            <td><input type="text" id="instManual' . $i . '" name="instManual[]" style="width:50px;" maxlength="4" value="' . $row["instManual"] . '"></td>
                            <td id="dispManual' . $i . '">' . $row["dispManual"] . '</td>
                            <td>' . $row["precio"] . '</td>
                            <td>' . $row["asignacion"] . '</td>
                            <script>
                                $("#instManual' . $i . '").numeric(false);
                            </script>
                        </tr>';
                        $div .= '<script> 
                            $(document).ready(function(){
                                $("#cantManual' . $i . '").click(function(){
                                    $("#cantManual' . $i . '").val("");
                                        
                                    restar(' . $i . ');
                                });

                                $("#instManual' . $i . '").click(function(){
                                    $("#instManual' . $i . '").val("");
                                        
                                    restar(' . $i . ');
                                });   
                                
                                $("#cantManual' . $i . '").blur(function(){
                                    if ($("#cantManual' . $i . '").val() === ""){
                                        $("#cantManual' . $i . '").val(0);
                                    }
                                    
                                    restar(' . $i . ');
                                });

                                $("#instManual' . $i . '").blur(function(){
                                    if ($("#instManual' . $i . '").val() === ""){
                                        $("#instManual' . $i . '").val(0);
                                    }
                                    
                                    restar(' . $i . ');
                                });   
                                
                                $("#cantManual' . $i . '").keyup(function(){
                                    if ($("#cantManual' . $i . '").val() === ""){
                                        $("#cantManual' . $i . '").val(0);
                                    }
                                    
                                    restar(' . $i . ');
                                });

                                $("#instManual' . $i . '").keyup(function(){
                                    if ($("#instManual' . $i . '").val() === ""){
                                        $("#instManual' . $i . '").val(0);
                                    }
                                    
                                    restar(' . $i . ');
                                });   
                            });
                            
                            function restar(i){
                                cantManual = $("#cantManual"+i).val();
                                instManual = $("#instManual"+i).val();
                                if (cantManual === ""){
                                    cantManual = 0;
                                }
                                
                                if (instManual === ""){
                                    instManual = 0;
                                }
                                
                                disp = cantManual - instManual;
                                $("#dispManual"+i).empty();
                                $("#dispManual"+i).append(disp);
                            }
                        </script>';
                    $i++;
                }

                $ultimo  = "no";
                $primero = "no";

                $limiteNuevo = $limite * $pagina;
                if($limiteNuevo > $total || $pagina == "ultima"){
                        $limiteNuevo = $total;
                        $ultimo      = "si";
                        $pagina      = $nueva_funcion->ultimaPaginaLicenciasAsignacion($_SESSION["client_id"], $fabricante, $asignacion, $asignaciones, $limite);
                }

                if($pagina == 1){
                        $primero = "si";
                }

                $sinPaginacion = "no";
                if($limite > $total){
                        $sinPaginacion = "si";
                }

                $nuevoInicio = (($pagina - 1) * $limite) + 1;
                if($nuevoInicio < 0){
                    $nuevoInicio = 0;
                }
                $paginacion = $nuevoInicio .'-' . $limiteNuevo . ' de ' . $total;

                $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$div, 
                'paginacion'=>$paginacion, 'primero'=>$primero, 'ultimo'=>$ultimo, 
                'sinPaginacion'=>$sinPaginacion, 'ultimaPagina'=>$pagina, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);