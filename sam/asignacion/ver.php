<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/sam/asignacion/procesos/ver.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/sam/clases/funciones.php");
$nueva_funcion = new funcionesSam();

require_once($GLOBALS["app_root"] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");

$atras = "historial.php";
?>

<section class="contenedor">
    <div class="divMenuPrincipal">
        <?php
        $opcionm1 = 12;
        include_once($GLOBALS['app_root'] . "/plantillas/menu1.php");
        ?>
    </div>

    
    <div class="divContenedorRight">
        <div class="divMenuContenido">
            
            <div style="margin-left:150px;" id="contenedor_ver2">
            <?php
                $opcionLateral = "asignacion";
                $opcionSuperior = "historial";
                include_once($GLOBALS["app_root"] . "/sam/plantillas/menuSuperior.php");
            ?>
            </div>
        </div>
        
        <div style="width:130px; margin:0; padding:0; padding-top: 50px; height:100%; float:left">
            <?php 
                include_once($GLOBALS["app_root"] . "/sam/plantillas/menuLateral.php");
            ?>
        </div>

        <div class="bordeContenedorSam">
            <div class="contenido" style="padding:10px;" id="contenedorCentral">
                <?php
                    include_once($GLOBALS['app_root'] . "/sam/asignacion/plantillas/ver.php");
                ?>
            </div>
        </div>
    </div>
</section>
<?php
include_once($GLOBALS['app_root'] . "/plantillas/pie.php");