<h1 class="textog negro" style="margin:20px; text-align:center;">Historial de <p style="color:#06B6FF; display:inline">Traslados</p></h1>

<div style="clear: both;">
    <table id="listaTraslados" style="width:100%; border:1px solid;">
        <thead style="background-color:#C1C1C1;">
            <tr>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Traslado</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Descripción</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fecha</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Observación</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Estado</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Ver</th>
            </tr>
        </thead>
        <tbody id="tablaTraslados">
            <?php 
            foreach($historial as $row){
            ?>
                <tr class="pointer" onclick="location.href = 'ver.php?id=<?= $row["id"] ?>';">
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["id"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["descripcion"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["fecha"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["observacion"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["estado"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_027_search.png" border="0" alt="Ver" style="vertical-align: middle; height:20px; width:auto;" title="Ver"/>
                    </th>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>