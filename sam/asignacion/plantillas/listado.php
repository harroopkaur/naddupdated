<?php
if ($agregar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alert', 'No se pudo actualizar las instalaciones manuales', {'Aceptar': 'Aceptar'});
    </script>
    <?php
} else if ($agregar == 1 && $exito == 1) {
?>
<script type="text/javascript">
    $.alert.open('info', 'Instalaciones manuales actualzadas con éxito', {'Aceptar': 'Aceptar'});
</script>
<?php 
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todas las instalaciones manuales', {'Aceptar': 'Aceptar'});
    </script>
<?php    
}
?>

<h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Asignaciones</p></h1>

<input type="hidden" id="pagina" value="1">

<div style="float:left;">
    <p style="float:left; line-height:30px;">Asignación</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="asignacion" name="asignacion">
        <option value="">--Seleccione--</option>
        <?php
        foreach($listadoAsignacion as $row){
        ?>
            <option value="<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="float:left;">
    <p style="float:left; margin-left:20px; line-height:30px;">Software</p>
    <select style="height:30px; width:164px; margin-left:20px; border:2px solid;" id="fabricante" name="fabricante">
        <option value="">--Seleccione--</option>
        <?php
        foreach($fabricantes as $row){
        ?>
            <option value="<?= $row["idFabricante"] ?>"><?= $row["nombre"] ?></option>
        <?php
        }
        ?>
    </select>
</div>

<div style="width:150px; height:40px; background-color:#06B6FF; color:white; text-align:center; line-height:40px; border-radius:10px; float:right; cursor:pointer;" id="exportar">Exportar Excel</div>

<br>
<br>

<div style="float:left">
    <p style="float:left; font-size:12px; margin-left:10px; text-align:center; display:none;" id="numPaginas"><?= '0-0 de 0' ?></p>

    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_171_fast_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="primero">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_170_step_backward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="atras">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_178_step_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="siguiente">
    <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_177_fast_forward.png" style="float:left; margin-left:10px; margin-top:5px; cursor:pointer; display:none;" id="ultimo">

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">Mostrar</p>

    <select style="border:2px solid; float:left; width:50px; height:30px; margin-left:20px; text-align:center;" id="limite" name="limite">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="150">150</option>
        <option value="200">200</option>
        <option value="300">300</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
    </select>

    <p style="float:left; margin-left:10px; text-align:center; line-height:30px;">por p&aacute;gina</p>
</div>

<br>
<br>

<style>
    #listaLicencias thead th{
        background-color:#C1C1C1;
    }
</style>

<form id="form1" name="form1" method="post" enctype="multipart/form-data" action="listado.php">
    <input type="hidden" id="insertar" name="insertar" value="1">
    <div style="clear: both; width:100%; max-height:400px; overflow:auto;">
        <table id="listaLicencias" style="width:1200px; border:1px solid;">
            <thead style="background-color:#C1C1C1;">
                <tr>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkAll" name="checkAll"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fabricante</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">N&uacute;mero de contrato</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Producto</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Edici&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Versi&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Cantidades</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Instalaciones</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Disponible</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">% Utilizado</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Cantidad Manual</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Instalación Manual</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Disponible Manual</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Precio</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Asignaci&oacute;n</th>
                </tr>
            </thead>
            <tbody id="tablaLicencias">
            </tbody>
        </table>
    </div>
</form>

<?php 
if($nueva_funcion->existe_permiso($_SESSION["client_empleado"], 19)){
?>
    <br>
    <div style="float:right;" class="botonesSAM boton5" onclick="actualizar()">Actualizar</div>
<?php
}
?>

<script>
    var pagina = 1;
    $(document).ready(function(){
        $("#listaLicencias").tablesorter();
        $("#listaLicencias").tableHeadFixer();
        
        $("#checkAll").click(function(){
            if($("#checkAll").prop("checked")){
                for(i=0; i < $("#tablaLicencias tr").length; i++){
                    if($("#check"+i).length > 0){
                        $("#check"+i).attr("checked", true);
                    }
                }
            }
            else{
                for(i=0; i < $("#tablaLicencias tr").length; i++){
                    if($("#check"+i).length > 0){
                        $("#check"+i).attr("checked", false);
                    }
                }
            }
        });

        $("#borrar").click(function(){
            for(i=0; i < $("#tablaLicencias tr").length; i++){
                if($("#check"+i).prop("checked")){
                    $("#"+i).remove();
                }
            }
            $("#checkAll").attr("checked", false);
        });

        $("#asignacion").change(function(){
            if(!validarFabricante()){
                return false;
            }
           
            $("#fondo").show();
            $("#tablaLicencias").empty();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listado.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#tablaLicencias").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#numPaginas").show();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });

        $("#fabricante").change(function(){
            if(!validarFabricante()){
                return false;
            }
            
            $("#fondo").show();
            $("#tablaLicencias").empty();
            pagina = 1;
            $("#pagina").val(pagina);
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listado.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                $("#tablaLicencias").append(data[0].div);
                $("#numPaginas").empty();
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#numPaginas").show();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });

        $("#limite").change(function(){
            if(!validarFabricante()){
                return false;
            }
            
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listado.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                $("#tablaLicencias").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#numPaginas").show();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });

        $("#primero").click(function(){
            if(!validarFabricante()){
                return false;
            }
            
            $("#fondo").show();
            pagina = 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listado.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?> 
                        
                $("#tablaLicencias").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                $("#primero").hide();
                $("#atras").hide();
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });

        $("#ultimo").click(function(){
            if(!validarFabricante()){
                return false;
            }
            
            $("#fondo").show();
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $("#pagina").val("ultima");
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listado.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : "ultima", limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#tablaLicencias").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                pagina = data[0].ultimaPagina;
                $("#pagina").val(pagina);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                $("#siguiente").hide();
                $("#ultimo").hide();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });

        $("#siguiente").click(function(){
            if(!validarFabricante()){
                return false;
            }
            
            $("#fondo").show();
            pagina += 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listado.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#tablaLicencias").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].sinPaginacion === "no"){
                    $("#primero").show();
                    $("#atras").show();
                }
                if(data[0].ultimo === "si"){
                    $("#siguiente").hide();
                    $("#ultimo").hide();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });

        $("#atras").click(function(){
            if(!validarFabricante()){
                return false;
            }
            
            $("#fondo").show();
            pagina -= 1;
            $("#pagina").val(pagina);
            $("#tablaLicencias").empty();
            $("#numPaginas").empty();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listado.php", { asignacion : $("#asignacion").val(), fabricante : $("#fabricante").val(), pagina : pagina, limite : $("#limite").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $("#tablaLicencias").append(data[0].div);
                $("#numPaginas").append(data[0].paginacion);
                if(data[0].primero === "si"){
                    $("#primero").hide();
                    $("#atras").hide();
                }
                if(data[0].sinPaginacion === "no"){
                    $("#siguiente").show();
                    $("#ultimo").show();
                }
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });	

        $("#exportar").click(function(){
            location.href="<?= $GLOBALS['domain_root'] ?>/sam/asignacion/reportes/exportar.php?vert="+$("#fabricante").val()+"&vert1="+$("#asignacion").val()+"&vert2="+$("#pagina").val()+"&vert3="+$("#limite").val();
        });
    });
    
    function validarFabricante(){
        result = true;
        if($("#fabricante").val() === ""){
            $.alert.open("warning", "Alert", "Debe seleccionar el fabricante", {"Aceptar" : "Aceptar"});
            result = false;
        }
        return result;
    }
    
    function actualizar(){
        if($("#tablaLicencias tr").length === 0){
            $.alert.open("warning", "Alert", "No existen registros para actualizar", {"Aceptar" : "Aceptar"});
            return false;
        }
        document.getElementById("form1").submit();
    }
</script>