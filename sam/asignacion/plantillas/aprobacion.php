<?php
if ($eliminar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alert', 'No se pudo eliminar el traslado', {'Aceptar': 'Aceptar'});
    </script>
    <?php
} else if ($eliminar == 1 && $exito == 1) {
?>
<script type="text/javascript">
    $.alert.open('info', 'Traslado eliminado con éxito', {'Aceptar': 'Aceptar'}, function(){
        location.href = "aprobacion.php";
    });
</script>
<?php
}
?>

<form id="formElim" name="formElim" method="post" action="aprobacion.php">
    <input type="hidden" id="eliminar" name="eliminar" value="1">
    <input type="hidden" id="id" name="id">
</form>
<h1 class="textog negro" style="margin:20px; text-align:center;">Historial de <p style="color:#06B6FF; display:inline">Traslados</p></h1>

<div style="clear: both;">
    <table id="listaTraslados" style="width:100%; border:1px solid;">
        <thead style="background-color:#C1C1C1;">
            <tr>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Traslado</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Descripción</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fecha</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Observación</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Estado</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Ver</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Editar</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Eliminar</th>
            </tr>
        </thead>
        <tbody id="tablaTraslados">
            <?php 
            foreach($historial as $row){
            ?>
                <tr class="pointer">
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["id"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["descripcion"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["fecha"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["observacion"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["estado"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_027_search.png" border="0" alt="Ver" style="vertical-align: middle; height:20px; width:auto;" title="Ver"
                        onclick="location.href = 'verAprobacion.php?id=<?= $row["id"] ?>';"/>
                    </th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_150_edit.png" border="0" alt="Editar" style="vertical-align: middle; height:20px; width:auto;" title="editar"
                        onclick="location.href = 'editar.php?id=<?= $row["id"] ?>';"/>
                    </th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                        <img src="<?= $GLOBALS["domain_root"] ?>/imagenes/png/glyphicons_016_bin.png" border="0" alt="Eliminar" style="vertical-align: middle; height:20px; width:auto;" title="eliminar"
                        onclick="eliminar(<?= $row["id"] ?>);"/>
                    </th>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>

<script>
    function eliminar(id){
        $("#id").val(id);
        $.alert.open('confirm', 'Confirmar', 'Desea eliminar el traslado', {'Si': 'Si', 'No': 'No'}, function(button){
            if(button === "Si"){
                $("#formElim").submit();
            }
        });
    }
</script>