<h1 class="textog negro" style="margin:20px; text-align:center;">Traslado de <p style="color:#06B6FF; display:inline">Licencias</p></h1>

<fieldset class="fieldset">
    <legend style=" font-weight:bold; margin-left:15px;">Licencias a Trasladar</legend>

    <div class="float-lt" style="width: 100px;">Traslado: </div><input type="text" id="traslado" name="traslado" class="float-lt" value="<?= $dataCab["id"] ?>" readonly>
    <br style="clear:both;"><br>
    
    <div class="float-lt" style="width: 100px;">Fecha: </div><input type="text" id="fecha" name="fecha" class="float-lt" value="<?= $dataCab["fecha"] ?>" readonly>
    <br style="clear:both;"><br>
    
    <div class="float-lt" style="width: 100px;">Descripción: </div><input type="text" id="descripcion" name="descripcion" class="float-lt" value="<?= $dataCab["descripcion"] ?>" readonly>
    <br style="clear:both;"><br>

    <div class="float-lt" style="width: 100px;">Observación: </div><textarea id="observacion" name="observacion" 
    maxlength="250" style="width:500px; height:50px; border:1px solid #C1C1C1; float:left;" readonly><?= $dataCab["observacion"] ?></textarea>
    <br style="clear:both;"><br>

    <table id="listaLicenciasTraslado" style="width:100%; border:1px solid;">
        <thead style="background-color:#C1C1C1;">
            <tr>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">N&uacute;mero de contrato</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Producto</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Edici&oacute;n</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Versi&oacute;n</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Disponible</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Asignaci&oacute;n</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Cantidad Traslado</th>
                <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Asignación Traslado</th>
            </tr>
        </thead>
        <tbody id="tablaLicenciasTraslado">
            <?php 
            foreach($dataDet as $row){
            ?>
                <tr>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["numero"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["producto"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["edicion"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["version"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["disponible"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["asignacion"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["cantTraslado"] ?></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;"><?= $row["asigTraslado"] ?></th>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</fieldset>    

<br>
<div class="botones_m2 boton1 float-rt" style="float:right;" onclick="location.href='<?= $atras ?>';">Atras</div>