<?php
if ($agregar == 1 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alert', 'No se pudo actualizar la cabecera del traslado', {'Aceptar': 'Aceptar'});
    </script>
    <?php
} else if ($agregar == 1 && $exito == 1) {
?>
<script type="text/javascript">
    $.alert.open('info', 'Traslado actualzado con éxito', {'Aceptar': 'Aceptar'}, function(){
        location.href = "aprobacion.php";
    });
</script>
<?php 
} else if ($agregar == 1 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todo el detalle del traslado', {'Aceptar': 'Aceptar'});
    </script>
<?php    
}

if ($agregar == 2 && $exito == 0) {
    ?>
    <script type="text/javascript">
        $.alert.open('warning', 'Alert', 'No se pudo aprobar el traslado', {'Aceptar': 'Aceptar'});
    </script>
    <?php
} else if ($agregar == 2 && $exito == 1) {
?>
<script type="text/javascript">
    $.alert.open('info', 'Traslado aprobado con éxito', {'Aceptar': 'Aceptar'}, function(){
        location.href = "aprobacion.php";
    });
</script>
<?php 
} else if ($agregar == 2 && $exito == 2) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'No se pudo actualizar todo el detalle del traslado aprobado', {'Aceptar': 'Aceptar'});
    </script>
<?php    
}
?>

<h1 class="textog negro" style="margin:20px; text-align:center;">Traslado de <p style="color:#06B6FF; display:inline">Licencias</p></h1>

<form id="formTraslado" name="formTraslado" method="post" enctype="multipart/form-data" action="editar.php">
    <fieldset class="fieldset">
        <legend style=" font-weight:bold; margin-left:15px;">Licencias a Trasladar</legend>
        
        <input type="hidden" id="insertar" name="insertar">
        <input type="hidden" id="idTraslado" name="idTraslado" value="<?= $dataCab["id"] ?>">
        <div class="float-lt" style="width: 100px;">Traslado: </div><input type="text" id="traslado" name="traslado" class="float-lt" value="<?= $dataCab["id"] ?>" readonly>
        <br style="clear:both;"><br>

        <div class="float-lt" style="width: 100px;">Fecha: </div><input type="text" id="fecha" name="fecha" class="float-lt" value="<?= $dataCab["fecha"] ?>" readonly>
        <br style="clear:both;"><br>

        <div class="float-lt" style="width: 100px;">Descripción: </div><input type="text" id="descripcion" name="descripcion" class="float-lt" value="<?= $dataCab["descripcion"] ?>">
        <br style="clear:both;"><br>

        <div class="float-lt" style="width: 100px;">Observación: </div><textarea id="observacion" name="observacion" 
        maxlength="250" style="width:500px; height:50px; border:1px solid #C1C1C1; float:left;"><?= $dataCab["observacion"] ?></textarea>
        <br style="clear:both;"><br>

        <table id="listaLicenciasTraslado" style="width:100%; border:1px solid;">
            <thead style="background-color:#C1C1C1;">
                <tr>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkAll" name="checkAll"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">N&uacute;mero de contrato</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Producto</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Edici&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Versi&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Disponible</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Asignaci&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Cantidad Traslado</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Asignación Traslado</th>
                </tr>
            </thead>
            <tbody id="tablaLicenciasTraslado">
                <?php 
                $i = 0;
                foreach($dataDet as $row){
                ?>
                    <tr id="row<?= $i ?>">
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">
                            <input type="checkbox" id="checkTras<?= $i ?>" name="checkTras[]">
                            <input type="hidden" id="dispon<?= $i ?>" name="dispon[]" value="<?= $row["disponible"] ?>">
                            <input type="hidden" id="asig<?= $i ?>" name="asig[]" value="<?= $row["asignacion"] ?>">
                            <input type="hidden" id="valores<?= $i ?>" name="valores[]" value="<?= $row["idDetalleContrato"] ?>">
                            <input type="hidden" id="idDetTraslado<?= $i ?>" name="idDetTraslado[]" value="<?= $row["id"] ?>">
                        </th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["numero"] ?></th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["producto"] ?></th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["edicion"] ?></th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["version"] ?></th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["disponible"] ?></th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><?= $row["asignacion"] ?></th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                            <input type="text" id="cantTrasla<?= $i ?>" name="cantTrasla[]" maxlength="4" style="width:100px; text-align:right;" value="<?= $row["cantTraslado"] ?>">
                        </th>
                        <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">
                            <select id="asigTras<?= $i ?>" name="asigTras[]" style="width:100px;">';
                                <option value="">--Seleccione--</option>'
                                <?php 
                                foreach($listadoAsignacion as $row1){
                                ?>
                                    <option value="<?= $row1["asignacion"] ?>" <?php if($row1["asignacion"] == $row["asigTraslado"]){ 
                                        echo "selected='selected'"; } ?>><?= $row1["asignacion"] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </th>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </fieldset>    

    <br>
    <div class="botones_m2 boton1 float-rt" style="float:right;" id="aprobarTraslado">Aprobar</div>
    <div class="botones_m2 boton1 float-rt" style="float:right;" id="guardarTraslado">Actualizar</div>
    <div class="botones_m2 boton1 float-rt" style="float:right;" id="eliminarTraslado">Eliminar</div>
    <div class="botones_m2 boton1 float-rt" style="float:right;" id="buscarLicencia">Agregar</div>
    <div class="botones_m2 boton1 float-rt" style="float:right;" onclick="location.href='aprobacion.php';">Atras</div>
</form>

<div class="modal-personal" id="modal-licenciasTraslado">
    <div class="modal-personal-head">
        <h1 class="textog negro" style="margin:20px; text-align:center;">Listado de <p style="color:#06B6FF; display:inline">Licencias</p></h1>
    </div>
    <div class="modal-personal-body">
        <table style="width:100%; border:1px solid;">
            <thead style="background-color:#C1C1C1;">
                <tr>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">&nbsp;</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">
                        <select id="fabri" name="fabri" style="width:100px;">
                            <option value="">--Seleccione--</option>
                            <?php 
                            foreach($fabricantes as $row){
                            ?>
                                <option value="<?= $row["idFabricante"] ?>"><?= $row["nombre"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="contr" name="contr" maxlength="20" style="width:100px;"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="familia" name="familia" maxlength="20" style="width:100px;"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="edicion" name="edicion" maxlength="20" style="width:100px;"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="version" name="version" maxlength="20" style="width:100px;"></th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">
                        <select id="asig" name="asig" style="width:100px;">
                            <option value="">--Seleccione--</option>
                            <?php 
                            foreach($listadoAsignacion as $row){
                            ?>
                                <option value="<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">&nbsp;</th>
                </tr>
                <tr>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;">&nbsp;</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Fabricante</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">N&uacute;mero de contrato</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Producto</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Edici&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Versi&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">Asignaci&oacute;n</th>
                    <th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">Disponible</th>
                </tr>
            </thead>
            <tbody id="modalTablaLicenciasTraslado">
            </tbody>
        </table>
    </div>
    <div class="modal-personal-footer">
        <div class="boton1 botones_m2Alterno" style="border:1px solid; float:right;" onclick="salirModalTraslado()">Salir</div>
        <div class="boton1 botones_m2Alterno" id="cargarLicencias" style="border:1px solid; float:right;">Cargar</div>
        <div class="boton1 botones_m2Alterno" id="filtrarLicencias" style="border:1px solid; float:right;">Buscar</div>
    </div>
</div>
<!--fin modal compras repositorio-->

<script>
    indice = <?= $i ?>;
    $(document).ready(function(){
        $("#buscarLicencia").click(function(){
            $("#fondo1").show();
            $("#modalTablaLicenciasTraslado").empty();
            $("#modal-licenciasTraslado").show();
        });
        
        $("#filtrarLicencias").click(function(){
            $("#fondo1").hide();
            $("#modal-licenciasTraslado").hide();
            $("#fondo").show();
            $.post("<?= $GLOBALS['domain_root'] ?>/sam/asignacion/ajax/listadoTraslado.php", { asignacion : $("#asig").val(), 
            fabricante : $("#fabri").val(), contrato : $("#contr").val(), familia : $("#familia").val(), 
            edicion : $("#edicion").val(), version : $("#version").val(), token : localStorage.licensingassuranceToken }, function(data){
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                $("#fondo").hide();
                $("#fondo1").show();
                $("#modalTablaLicenciasTraslado").empty();
                $("#modalTablaLicenciasTraslado").append(data[0].div)
                $("#modal-licenciasTraslado").show();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"});
            });
        });
        
        $("#cargarLicencias").click(function(){
            for(i = 0; i < $("#modalTablaLicenciasTraslado tr").length; i++){
                row = '';
                aprobado = 0;
                if($("#check" + i).prop("checked")){
                    valores = $("#check" + i).val();
                    arrayValores = valores.split("*");
                    
                    if(arrayValores[5] > 0){
                        aprobado = 1;
                        row = '<tr id="row' + indice + '">';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF;"><input type="checkbox" id="checkTras' + indice + '" name="checkTras[]">';
                        row += '<input type="hidden" id="idDetTraslado' + indice + '" name="idDetTraslado[]" value="0">';
                        row += '<input type="hidden" id="dispon' + indice + '" name="dispon[]" value="' + arrayValores[5] + '">';
                        row += '<input type="hidden" id="asig' + indice + '" name="asig[]" value="' + arrayValores[4] + '">';
                        row += '<input type="hidden" id="valores'+indice+'" name="valores[]" value="' + arrayValores[6] + '"></th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">' + arrayValores[0] + '</th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">' + arrayValores[1] + '</th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">' + arrayValores[2] + '</th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">' + arrayValores[3] + '</th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">' + arrayValores[5] + '</th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;">' + arrayValores[4] + '</th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; cursor:pointer;"><input type="text" id="cantTrasla' + indice + '" name="cantTrasla[]" maxlength="4" style="width:100px; text-align:right;" value="0"></th>';
                        row += '<th style="text-align:center; border-left: 1px solid #FFFFFF; cursor:pointer;">';
                        row += '<select id="asigTras' + indice + '" name="asigTras[]" style="width:100px;">';
                        row += '<option value="">--Seleccione--</option>'
                        <?php 
                        foreach($listadoAsignacion as $row){
                        ?>
                            row += '<option value="<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>';
                        <?php
                        }
                        ?>
                        row += '</select></th>';
                        row += '</tr>';
                        indice++;
                    }
                }
                
                if(aprobado === 1){
                    $("#tablaLicenciasTraslado").append(row);
                }
            }
            
            salirModalTraslado();
        });
        
        $("#checkAll").click(function(){
            if($("#checkAll").prop("checked")){
                for(i = 0; i < indice; i++){
                    $("#checkTras" + i).prop("checked", true);
                }
            } else{
                for(i = 0; i < indice; i++){
                    $("#checkTras" + i).prop("checked", false);
                }
            }
        });
        
        $("#eliminarTraslado").click(function(){
            for(i = 0; i < indice; i++){
                if($("#checkTras" + i).prop("checked")){
                    $("#row" + i).remove();
                }
            }
            
            if($("#tablaLicenciasTraslado tr").length === 0){
                $("#checkAll").prop("checked", false);
                indice = 0;
            }
        });
        
        $("#guardarTraslado").click(function(){
            if($("#descripcion").val() === ""){
                $.alert.open('warning', 'Alert', 'Debe llenar la descripción del traslado', {'Aceptar': 'Aceptar'}, function(){
                    $("#descripcion").focus();
                });
                return false;
            }
            
            if($("#tablaLicenciasTraslado tr").lentgh === 0){
                $.alert.open('warning', 'Alert', 'Debe agregar', {'Aceptar': 'Aceptar'});
                return false;
            }
            
            disponible = new Array();
            for(i = 0; i < indice; i++){
                if(disponible[parseInt($("#valores" + i).val())] === undefined){
                    disponible[$("#valores" + i).val()] = parseInt($("#dispon" + i).val()) - parseInt($("#cantTrasla" + i).val()); //$("#dispon" + indice).val()
                } else{
                    disponible[$("#valores" + i).val()] -= parseInt($("#cantTrasla" + i).val());
                }
                
                
                if(disponible[$("#valores" + i).val()] < 0){
                    $.alert.open('warning', 'Alert', 'La cantidad disponible es ' + (parseInt(disponible[$("#valores" + i).val()]) + parseInt($("#cantTrasla" + i).val())), {'Aceptar': 'Aceptar'}, function(){
                            $("#cantTrasla" + i).val(parseInt(disponible[$("#valores" + i).val()]) + parseInt($("#cantTrasla" + i).val()));
                            $("#cantTrasla" + i).focus();
                    });
                    return false;
                }
            }
            
            for(i = 0; i < indice; i++){
                if(parseInt($("#cantTrasla" + i).val()) <= 0){
                    $.alert.open('warning', 'Alert', 'Existen Cantidades en 0', {'Aceptar': 'Aceptar'}, function(){
                            $("#cantTrasla" + i).focus();
                    });
                    return false;
                }
                
                if($("#asigTras" + i).val() === ""){
                    $.alert.open('warning', 'Alert', 'Existen licencias sin asignación', {'Aceptar': 'Aceptar'}, function(){
                        $("#asigTras" + i).focus();
                    });
                    return false;
                }
                
                if($("#asig" + i).val() !== undefined && $("#asigTras" + i).val() === $("#asig" + i).val()){
                    $.alert.open('warning', 'Alert', 'La asignación de actual no puede ser la misma que la de destino', {'Aceptar': 'Aceptar'}, function(){
                        $("#asigTras" + i).focus();
                    });
                    return false;
                }
            }
            $("#insertar").val(1);
            $("#formTraslado").submit();            
        });
        
        $("#aprobarTraslado").click(function(){
            $("#insertar").val(2);
            $("#formTraslado").submit();
        });
    });
    
    function salirModalTraslado(){
        $("#fondo1").hide();
        $("#modal-licenciasTraslado").hide();
        $("#fabri").val("");
        $("#contr").val("");
        $("#familia").val("");
        $("#edicion").val("");
        $("#version").val("");
    }
</script>