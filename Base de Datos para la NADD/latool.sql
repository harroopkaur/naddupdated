-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 02-10-2019 a las 14:40:01
-- Versión del servidor: 5.6.45
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `latool`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity_links`
--

CREATE TABLE `activity_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `link_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_node` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_node` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `client_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity_tasks`
--

CREATE TABLE `activity_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `node_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `progress` double DEFAULT NULL,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date_real` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `revised_s` tinyint(4) NOT NULL DEFAULT '0',
  `comments` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `file` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente`
--

CREATE TABLE `agente` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `tx_host_name` varchar(100) NOT NULL,
  `tx_serial_disco` varchar(255) NOT NULL,
  `tx_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_add_remove`
--

CREATE TABLE `agente_add_remove` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_regist` varchar(500) DEFAULT NULL,
  `tx_editor` varchar(100) DEFAULT NULL,
  `tx_verson` varchar(100) DEFAULT NULL,
  `tx_dia_instlc` varchar(50) DEFAULT NULL,
  `tx_softwr` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_add_remove_aux`
--

CREATE TABLE `agente_add_remove_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_regist` varchar(500) DEFAULT NULL,
  `tx_editor` varchar(100) DEFAULT NULL,
  `tx_verson` varchar(100) DEFAULT NULL,
  `tx_dia_instlc` varchar(50) DEFAULT NULL,
  `tx_softwr` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_archivo`
--

CREATE TABLE `agente_archivo` (
  `id_archvo` int(11) UNSIGNED NOT NULL,
  `tx_nb_archvo` varchar(100) NOT NULL,
  `tx_nb_tabla` varchar(100) NOT NULL,
  `nu_campos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_archivo_campo`
--

CREATE TABLE `agente_archivo_campo` (
  `id_archvo_campo` int(11) UNSIGNED NOT NULL,
  `id_archvo` int(11) UNSIGNED NOT NULL,
  `tx_nb_campo` varchar(100) NOT NULL,
  `nu_orden_campo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_autodesk`
--

CREATE TABLE `agente_autodesk` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_regist` varchar(500) DEFAULT NULL,
  `tx_producto` varchar(100) DEFAULT NULL,
  `tx_verson` varchar(30) DEFAULT NULL,
  `tx_serial` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_autodesk_aux`
--

CREATE TABLE `agente_autodesk_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_regist` varchar(500) DEFAULT NULL,
  `tx_producto` varchar(100) DEFAULT NULL,
  `tx_verson` varchar(30) DEFAULT NULL,
  `tx_serial` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_config`
--

CREATE TABLE `agente_config` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `val` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_desinstalacion`
--

CREATE TABLE `agente_desinstalacion` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_descrp` varchar(100) DEFAULT NULL,
  `tx_fe_desins` varchar(50) DEFAULT NULL,
  `tx_usuaro_desins` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_desinstalacion_aux`
--

CREATE TABLE `agente_desinstalacion_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_descrp` varchar(100) DEFAULT NULL,
  `tx_fe_desins` varchar(50) DEFAULT NULL,
  `tx_usuaro_desins` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_hist`
--

CREATE TABLE `agente_hist` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `fe_ejeccn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_llave`
--

CREATE TABLE `agente_llave` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_tipo_llave` varchar(100) DEFAULT NULL,
  `tx_llave` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_llave_aux`
--

CREATE TABLE `agente_llave_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_tipo_llave` varchar(100) DEFAULT NULL,
  `tx_llave` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_memoria`
--

CREATE TABLE `agente_memoria` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `dbl_memoria` double(11,2) DEFAULT NULL,
  `dbl_memoria_disp` double(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_memoria_aux`
--

CREATE TABLE `agente_memoria_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `dbl_memoria` double(11,2) DEFAULT NULL,
  `dbl_memoria_disp` double(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_metering_errors`
--

CREATE TABLE `agente_metering_errors` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(150) DEFAULT NULL,
  `error` varchar(150) DEFAULT NULL,
  `origen` varchar(250) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_metering_errors_aux`
--

CREATE TABLE `agente_metering_errors_aux` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(150) DEFAULT NULL,
  `error` varchar(150) DEFAULT NULL,
  `origen` varchar(250) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_metering_results`
--

CREATE TABLE `agente_metering_results` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(150) DEFAULT NULL,
  `proceso` varchar(150) DEFAULT NULL,
  `pid` varchar(100) DEFAULT NULL,
  `creado` varchar(20) DEFAULT NULL,
  `horaInicio` varchar(20) DEFAULT NULL,
  `finalizado` varchar(20) DEFAULT NULL,
  `horaFin` varchar(20) DEFAULT NULL,
  `duracion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_metering_results_aux`
--

CREATE TABLE `agente_metering_results_aux` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(150) DEFAULT NULL,
  `proceso` varchar(150) DEFAULT NULL,
  `pid` varchar(100) DEFAULT NULL,
  `creado` varchar(20) DEFAULT NULL,
  `horaInicio` varchar(20) DEFAULT NULL,
  `finalizado` varchar(20) DEFAULT NULL,
  `horaFin` varchar(20) DEFAULT NULL,
  `duracion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_procesador`
--

CREATE TABLE `agente_procesador` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_tipo_cpu` varchar(100) DEFAULT NULL,
  `tx_nu_cpu` varchar(100) DEFAULT NULL,
  `tx_nu_cores` varchar(100) DEFAULT NULL,
  `tx_procsd_logico` varchar(100) DEFAULT NULL,
  `tx_tipo_escano` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_procesador_aux`
--

CREATE TABLE `agente_procesador_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_tipo_cpu` varchar(100) DEFAULT NULL,
  `tx_nu_cpu` varchar(100) DEFAULT NULL,
  `tx_nu_cores` varchar(100) DEFAULT NULL,
  `tx_procsd_logico` varchar(100) DEFAULT NULL,
  `tx_tipo_escano` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_proceso`
--

CREATE TABLE `agente_proceso` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_nb_procso` varchar(100) DEFAULT NULL,
  `tx_ruta_procso` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_proceso_aux`
--

CREATE TABLE `agente_proceso_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_nb_procso` varchar(100) DEFAULT NULL,
  `tx_ruta_procso` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_resultado_escaneo`
--

CREATE TABLE `agente_resultado_escaneo` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_status` varchar(100) DEFAULT NULL,
  `tx_error` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_resultado_escaneo_aux`
--

CREATE TABLE `agente_resultado_escaneo_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_status` varchar(100) DEFAULT NULL,
  `tx_error` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_scheduling`
--

CREATE TABLE `agente_scheduling` (
  `id_scheduling` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_seguridad`
--

CREATE TABLE `agente_seguridad` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_captin` varchar(100) DEFAULT NULL,
  `tx_descrp` varchar(100) DEFAULT NULL,
  `tx_fix_commnt` varchar(100) DEFAULT NULL,
  `tx_hotfix_id` varchar(100) DEFAULT NULL,
  `tx_instll_date` varchar(100) DEFAULT NULL,
  `tx_install_by` varchar(100) DEFAULT NULL,
  `tx_install_on` varchar(100) DEFAULT NULL,
  `tx_name` varchar(100) DEFAULT NULL,
  `tx_servce_pack` varchar(100) DEFAULT NULL,
  `tx_status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_seguridad_aux`
--

CREATE TABLE `agente_seguridad_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_captin` varchar(100) DEFAULT NULL,
  `tx_descrp` varchar(100) DEFAULT NULL,
  `tx_fix_commnt` varchar(100) DEFAULT NULL,
  `tx_hotfix_id` varchar(100) DEFAULT NULL,
  `tx_instll_date` varchar(100) DEFAULT NULL,
  `tx_install_by` varchar(100) DEFAULT NULL,
  `tx_install_on` varchar(100) DEFAULT NULL,
  `tx_name` varchar(100) DEFAULT NULL,
  `tx_servce_pack` varchar(100) DEFAULT NULL,
  `tx_status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_serial_maquina`
--

CREATE TABLE `agente_serial_maquina` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_serial_maquna` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_serial_maquina_aux`
--

CREATE TABLE `agente_serial_maquina_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_serial_maquna` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_servicio`
--

CREATE TABLE `agente_servicio` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_nb_prodct` varchar(250) DEFAULT NULL,
  `tx_estado` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_servicio_aux`
--

CREATE TABLE `agente_servicio_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_nb_prodct` varchar(100) DEFAULT NULL,
  `tx_estado` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_sistema_operativo`
--

CREATE TABLE `agente_sistema_operativo` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_sistma_opertv` varchar(100) DEFAULT NULL,
  `tx_fe_creacn` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_sistema_operativo_aux`
--

CREATE TABLE `agente_sistema_operativo_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_sistma_opertv` varchar(100) DEFAULT NULL,
  `tx_fe_creacn` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_sql_data`
--

CREATE TABLE `agente_sql_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_llave_regist` varchar(200) DEFAULT NULL,
  `tx_edicon` varchar(100) DEFAULT NULL,
  `tx_verson` varchar(100) DEFAULT NULL,
  `tx_ruta_instlc` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_sql_data_aux`
--

CREATE TABLE `agente_sql_data_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_llave_regist` varchar(200) DEFAULT NULL,
  `tx_edicon` varchar(100) DEFAULT NULL,
  `tx_verson` varchar(100) DEFAULT NULL,
  `tx_ruta_instlc` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_tipo_equipo`
--

CREATE TABLE `agente_tipo_equipo` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_fabrcn` varchar(100) DEFAULT NULL,
  `tx_modelo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_tipo_equipo_aux`
--

CREATE TABLE `agente_tipo_equipo_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_fabrcn` varchar(100) DEFAULT NULL,
  `tx_modelo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_usabilidad_software`
--

CREATE TABLE `agente_usabilidad_software` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_softwr` varchar(300) DEFAULT NULL,
  `tx_last_exec` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_usabilidad_software_aux`
--

CREATE TABLE `agente_usabilidad_software_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_hot_name` varchar(100) DEFAULT NULL,
  `tx_softwr` varchar(300) DEFAULT NULL,
  `tx_last_exec` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_usuario_equipo`
--

CREATE TABLE `agente_usuario_equipo` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_domino` varchar(100) DEFAULT NULL,
  `tx_usuario` varchar(100) DEFAULT NULL,
  `tx_ip` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_usuario_equipo_aux`
--

CREATE TABLE `agente_usuario_equipo_aux` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_agente` int(11) UNSIGNED NOT NULL,
  `tx_dato_contrl` varchar(100) DEFAULT NULL,
  `tx_host_name` varchar(100) DEFAULT NULL,
  `tx_domino` varchar(100) DEFAULT NULL,
  `tx_usuario` varchar(100) DEFAULT NULL,
  `tx_ip` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionSqlServidores`
--

CREATE TABLE `alineacionSqlServidores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `centroCosto` varchar(70) DEFAULT NULL,
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionSqlServidoresSam`
--

CREATE TABLE `alineacionSqlServidoresSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `centroCosto` varchar(70) DEFAULT NULL,
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionSqlServidoresSPLA`
--

CREATE TABLE `alineacionSqlServidoresSPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionSqlServidoresSPLASam`
--

CREATE TABLE `alineacionSqlServidoresSPLASam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionWindowsServidores`
--

CREATE TABLE `alineacionWindowsServidores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `centroCosto` varchar(70) DEFAULT NULL,
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionWindowsServidoresSam`
--

CREATE TABLE `alineacionWindowsServidoresSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `centroCosto` varchar(70) DEFAULT NULL,
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionWindowsServidoresSPLA`
--

CREATE TABLE `alineacionWindowsServidoresSPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alineacionWindowsServidoresSPLASam`
--

CREATE TABLE `alineacionWindowsServidoresSPLASam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `familia` varchar(70) DEFAULT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL,
  `tipo` enum('Virtual','') DEFAULT 'Virtual',
  `cpu` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `licSrv` int(11) DEFAULT NULL,
  `licProc` int(11) DEFAULT NULL,
  `licCore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appConsolidado_offices`
--

CREATE TABLE `appConsolidado_offices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `idCorreo` bigint(20) UNSIGNED NOT NULL,
  `serialHDD` varchar(250) NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appDetalles_equipo`
--

CREATE TABLE `appDetalles_equipo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `idCorreo` bigint(20) UNSIGNED NOT NULL,
  `serialHDD` varchar(250) NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT '0.00',
  `dias2` float(10,2) DEFAULT '0.00',
  `dias3` float(10,2) DEFAULT '0.00',
  `minimo` int(10) NOT NULL DEFAULT '0',
  `activo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tipo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rango` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `errors` varchar(250) DEFAULT NULL,
  `asignacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appEscaneo`
--

CREATE TABLE `appEscaneo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `idCorreo` bigint(20) UNSIGNED NOT NULL,
  `serialHDD` varchar(250) NOT NULL,
  `LAD` varchar(30) DEFAULT NULL,
  `fechaCarga` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appEscaneoCorreo`
--

CREATE TABLE `appEscaneoCorreo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `correo` varchar(70) NOT NULL,
  `clientes_nadd_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appFilepcs`
--

CREATE TABLE `appFilepcs` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `idCorreo` bigint(20) UNSIGNED NOT NULL,
  `serialHDD` varchar(250) NOT NULL,
  `dn` varchar(250) DEFAULT NULL,
  `objectclass` varchar(250) DEFAULT NULL,
  `cn` varchar(250) DEFAULT NULL,
  `useracountcontrol` varchar(250) DEFAULT NULL,
  `lastlogon` varchar(250) DEFAULT NULL,
  `pwdlastset` varchar(250) DEFAULT NULL,
  `os` varchar(250) DEFAULT NULL,
  `lastlogontimes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appResumen_office`
--

CREATE TABLE `appResumen_office` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `idCorreo` bigint(20) UNSIGNED NOT NULL,
  `serialHDD` varchar(250) NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `familia` varchar(250) DEFAULT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(11) DEFAULT NULL,
  `fecha_instalacion` date DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivosFabricantes`
--

CREATE TABLE `archivosFabricantes` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fabricante` int(11) UNSIGNED NOT NULL COMMENT 'identificador del fabricante',
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo segundo archivo de despliegue',
  `archivoDespliegue3` varchar(100) DEFAULT NULL COMMENT 'nombre del tercer archivo de despliegue',
  `archivoCompras` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compras',
  `fechaRegistro` datetime NOT NULL COMMENT 'fecha en que se hizo el registro',
  `tipoDespliegue` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivosIncrementoFabricantes`
--

CREATE TABLE `archivosIncrementoFabricantes` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fabricante` int(11) UNSIGNED NOT NULL COMMENT 'identificador del fabricante',
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo segundo archivo de despliegue',
  `archivoDespliegue3` varchar(100) DEFAULT NULL COMMENT 'nombre del tercer archivo de despliegue',
  `archivoCompras` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compras',
  `fechaRegistro` datetime NOT NULL COMMENT 'fecha en que se hizo el registro',
  `tipoDespliegue` tinyint(4) DEFAULT NULL COMMENT 'tipo de despliegue'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `departamento_id` int(10) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asigCliente`
--

CREATE TABLE `asigCliente` (
  `idAsignacion` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asigClienteEmple`
--

CREATE TABLE `asigClienteEmple` (
  `idAsignacion` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion`
--

CREATE TABLE `asignacion` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_adobe`
--

CREATE TABLE `balance_adobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_adobeSam`
--

CREATE TABLE `balance_adobeSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_ibm`
--

CREATE TABLE `balance_ibm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_ibmSam`
--

CREATE TABLE `balance_ibmSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_office`
--

CREATE TABLE `balance_office` (
  `id` int(10) UNSIGNED NOT NULL,
  `cliente` int(10) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_office2`
--

CREATE TABLE `balance_office2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `tipoCompra` varchar(20) DEFAULT NULL,
  `cantidadGAP` int(11) DEFAULT NULL,
  `totalGAP` int(11) DEFAULT NULL,
  `balanceGAP` int(11) DEFAULT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_office2Sam`
--

CREATE TABLE `balance_office2Sam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` int(11) DEFAULT '0',
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `tipoCompra` varchar(20) DEFAULT NULL,
  `cantidadGAP` int(11) DEFAULT NULL,
  `totalGAP` int(11) DEFAULT NULL,
  `balanceGAP` int(11) DEFAULT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_oracle`
--

CREATE TABLE `balance_oracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_oracleSam`
--

CREATE TABLE `balance_oracleSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` int(11) UNSIGNED DEFAULT '0',
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_Sap`
--

CREATE TABLE `balance_Sap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_SapSam`
--

CREATE TABLE `balance_SapSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_SPLA`
--

CREATE TABLE `balance_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) NOT NULL,
  `version` text NOT NULL,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_SPLASam`
--

CREATE TABLE `balance_SPLASam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) NOT NULL,
  `version` text NOT NULL,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_unixIBM`
--

CREATE TABLE `balance_unixIBM` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_UnixIBMSam`
--

CREATE TABLE `balance_UnixIBMSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_unixOracle`
--

CREATE TABLE `balance_unixOracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_UnixOracleSam`
--

CREATE TABLE `balance_UnixOracleSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_VMWare`
--

CREATE TABLE `balance_VMWare` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balance_VMWareSam`
--

CREATE TABLE `balance_VMWareSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `office` varchar(250) DEFAULT NULL,
  `version` text,
  `precio` float(10,2) NOT NULL,
  `instalaciones` int(10) UNSIGNED NOT NULL,
  `compra` float(10,2) UNSIGNED NOT NULL,
  `balance` float(10,2) NOT NULL,
  `balancec` float(10,2) NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centroCosto`
--

CREATE TABLE `centroCosto` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `usuario` varchar(70) NOT NULL,
  `centroCosto` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificaciones`
--

CREATE TABLE `certificaciones` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(70) NOT NULL,
  `ruta` varchar(80) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `correo` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `empresa` varchar(250) NOT NULL,
  `pais` int(11) UNSIGNED NOT NULL,
  `userName` varchar(250) DEFAULT NULL,
  `clave` varchar(250) DEFAULT NULL,
  `estado` int(11) UNSIGNED NOT NULL,
  `fecha_registro` date NOT NULL DEFAULT '0000-00-00',
  `fecha1` date NOT NULL DEFAULT '0000-00-00',
  `fecha2` date NOT NULL DEFAULT '0000-00-00',
  `nivelServicio` tinyint(4) NOT NULL,
  `dashboard` varchar(70) DEFAULT NULL,
  `correoEnvioApp` varchar(70) DEFAULT NULL,
  `img_header` varchar(150) DEFAULT NULL,
  `img_footer` varchar(150) DEFAULT NULL,
  `html_header` text,
  `html_footer` text,
  `cantidadCorreosDominio` int(11) DEFAULT '0',
  `cantidadDespliegueEquipos` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_dominios`
--

CREATE TABLE `clientes_dominios` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `dominio` varchar(100) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_nadd`
--

CREATE TABLE `clientes_nadd` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombreEmpresa` varchar(70) NOT NULL,
  `correo` varchar(70) NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `reseller_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_user`
--

CREATE TABLE `clientes_user` (
  `id` int(10) NOT NULL,
  `clientes_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` int(10) UNSIGNED NOT NULL,
  `cliente` int(10) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `compra` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras2`
--

CREATE TABLE `compras2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `tipo` varchar(20) DEFAULT NULL,
  `compra` int(11) DEFAULT '0',
  `precio` double(11,2) NOT NULL DEFAULT '0.00',
  `asignacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_adobe`
--

CREATE TABLE `compras_adobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `compra` int(11) DEFAULT '0',
  `precio` double(15,2) NOT NULL DEFAULT '0.00',
  `cantidadGAP` int(11) DEFAULT NULL,
  `totalGAP` int(11) DEFAULT NULL,
  `balanceGAP` int(11) DEFAULT NULL,
  `asignacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_ibm`
--

CREATE TABLE `compras_ibm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `compra` int(11) DEFAULT '0',
  `precio` double(15,2) NOT NULL DEFAULT '0.00',
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_oracle`
--

CREATE TABLE `compras_oracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `compra` int(11) DEFAULT '0',
  `precio` double(15,2) NOT NULL DEFAULT '0.00',
  `asignacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_Sap`
--

CREATE TABLE `compras_Sap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `compra` int(10) NOT NULL,
  `precio` double(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_SPLA`
--

CREATE TABLE `compras_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `compra` int(10) NOT NULL,
  `precio` double(11,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_unixIBM`
--

CREATE TABLE `compras_unixIBM` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `compra` int(10) NOT NULL,
  `precio` double(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_unixOracle`
--

CREATE TABLE `compras_unixOracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `compra` int(10) NOT NULL,
  `precio` double(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado`
--

CREATE TABLE `consolidado` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoProcesadores`
--

CREATE TABLE `consolidadoProcesadores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `tipo_CPU` varchar(250) NOT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `procesadores_logicos` int(11) NOT NULL,
  `tipo_escaneo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoProcesadoresIBM`
--

CREATE TABLE `consolidadoProcesadoresIBM` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `tipo_CPU` varchar(250) NOT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `procesadores_logicos` int(11) NOT NULL,
  `tipo_escaneo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoProcesadoresIBMAux`
--

CREATE TABLE `consolidadoProcesadoresIBMAux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `tipo_CPU` varchar(250) NOT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `procesadores_logicos` int(11) NOT NULL,
  `tipo_escaneo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoProcesadoresMSCloud`
--

CREATE TABLE `consolidadoProcesadoresMSCloud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `tipo_CPU` varchar(250) NOT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `procesadores_logicos` int(11) NOT NULL,
  `tipo_escaneo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoProcesadoresSAMDiagnostic`
--

CREATE TABLE `consolidadoProcesadoresSAMDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `tipo_CPU` varchar(250) NOT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `procesadores_logicos` int(11) NOT NULL,
  `tipo_escaneo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoProcesadores_SPLA`
--

CREATE TABLE `consolidadoProcesadores_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `tipo_CPU` varchar(250) NOT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `procesadores_logicos` int(11) NOT NULL,
  `tipo_escaneo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoTipoEquipo`
--

CREATE TABLE `consolidadoTipoEquipo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `fabricante` varchar(250) NOT NULL,
  `modelo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoTipoEquipoMSCloud`
--

CREATE TABLE `consolidadoTipoEquipoMSCloud` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `fabricante` varchar(250) NOT NULL,
  `modelo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoTipoEquipoSAMDiagnostic`
--

CREATE TABLE `consolidadoTipoEquipoSAMDiagnostic` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `fabricante` varchar(250) NOT NULL,
  `modelo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidadoTipoEquipo_SPLA`
--

CREATE TABLE `consolidadoTipoEquipo_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `fabricante` varchar(250) NOT NULL,
  `modelo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_adobe`
--

CREATE TABLE `consolidado_adobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) DEFAULT NULL,
  `host_name` varchar(250) DEFAULT NULL,
  `registro` text,
  `editor` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `fecha_instalacion` int(11) UNSIGNED DEFAULT NULL,
  `sofware` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_adobeAux`
--

CREATE TABLE `consolidado_adobeAux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED DEFAULT NULL,
  `dato_control` varchar(250) DEFAULT NULL,
  `host_name` varchar(250) DEFAULT NULL,
  `registro` text,
  `editor` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `fecha_instalacion` int(11) UNSIGNED DEFAULT NULL,
  `sofware` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_ibm`
--

CREATE TABLE `consolidado_ibm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) DEFAULT NULL,
  `host_name` varchar(250) DEFAULT NULL,
  `registro` text,
  `editor` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `fecha_instalacion` int(11) UNSIGNED DEFAULT NULL,
  `sofware` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_ibmAux`
--

CREATE TABLE `consolidado_ibmAux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED DEFAULT NULL,
  `dato_control` varchar(250) DEFAULT NULL,
  `host_name` varchar(250) DEFAULT NULL,
  `registro` text,
  `editor` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `fecha_instalacion` int(11) UNSIGNED DEFAULT NULL,
  `sofware` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_MSCloud`
--

CREATE TABLE `consolidado_MSCloud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_offices`
--

CREATE TABLE `consolidado_offices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_officesAux`
--

CREATE TABLE `consolidado_officesAux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED DEFAULT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_oracle`
--

CREATE TABLE `consolidado_oracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `host_name` varchar(250) DEFAULT NULL,
  `product` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `file` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_OracleAIX`
--

CREATE TABLE `consolidado_OracleAIX` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `software` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_OracleLinux`
--

CREATE TABLE `consolidado_OracleLinux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `software` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_OracleSolaris`
--

CREATE TABLE `consolidado_OracleSolaris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `software` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_others`
--

CREATE TABLE `consolidado_others` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00',
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_SAMDiagnostic`
--

CREATE TABLE `consolidado_SAMDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_server`
--

CREATE TABLE `consolidado_server` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00',
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_SPLA`
--

CREATE TABLE `consolidado_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_SPLAAux`
--

CREATE TABLE `consolidado_SPLAAux` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(250) NOT NULL,
  `host_name` varchar(250) NOT NULL,
  `registro` text NOT NULL,
  `editor` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` int(11) UNSIGNED NOT NULL,
  `sofware` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_UnixAIX`
--

CREATE TABLE `consolidado_UnixAIX` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `software` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_UnixLinux`
--

CREATE TABLE `consolidado_UnixLinux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `software` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consolidado_UnixSolaris`
--

CREATE TABLE `consolidado_UnixSolaris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `software` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE `contrato` (
  `idContrato` int(11) UNSIGNED NOT NULL COMMENT 'identificador del contrato',
  `idCliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `idFabricante` int(11) NOT NULL COMMENT 'identificador del fabricante',
  `numero` varchar(40) NOT NULL COMMENT 'numero del contrato',
  `tipo` varchar(50) NOT NULL COMMENT 'tipod de contrato',
  `fechaEfectiva` date NOT NULL COMMENT 'fecha efectiva del contrato',
  `fechaExpiracion` date NOT NULL COMMENT 'fecha de expiracion del contrato',
  `fechaProxima` date NOT NULL,
  `comentarios` varchar(250) DEFAULT NULL COMMENT 'comentarios del contrato',
  `archivo1` varchar(70) DEFAULT NULL,
  `archivo2` varchar(70) DEFAULT NULL,
  `archivo3` varchar(70) DEFAULT NULL,
  `archivo4` varchar(70) DEFAULT NULL,
  `archivo5` varchar(70) DEFAULT NULL,
  `correoExpiracion90` datetime DEFAULT NULL,
  `correoExpiracion60` datetime DEFAULT NULL,
  `correoExpiracion30` datetime DEFAULT NULL,
  `correoProxima90` datetime DEFAULT NULL,
  `correoProxima60` datetime DEFAULT NULL,
  `correoProxima30` datetime DEFAULT NULL,
  `subsidiaria` varchar(70) NOT NULL,
  `proveedor` varchar(70) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratosVMWare`
--

CREATE TABLE `contratosVMWare` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `accountNumber` int(11) NOT NULL,
  `accountName` varchar(70) NOT NULL,
  `product` varchar(100) NOT NULL,
  `licenseKey` varchar(70) NOT NULL,
  `Qty` int(11) NOT NULL,
  `unitMeasure` varchar(20) NOT NULL,
  `folderNamePath` varchar(70) NOT NULL,
  `licenseKeyNotes` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `number` int(11) NOT NULL,
  `poNumber` varchar(70) DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderQty` int(11) NOT NULL,
  `supportLevel` varchar(40) DEFAULT NULL,
  `licenseCoverageEndDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacionesLicensing`
--

CREATE TABLE `cotizacionesLicensing` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombreSolicitante` varchar(70) NOT NULL,
  `email` varchar(80) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `horario` varchar(70) NOT NULL,
  `nivelServicio` int(11) NOT NULL,
  `pcs` int(11) NOT NULL,
  `servidores` int(11) NOT NULL,
  `fabricantes` varchar(70) NOT NULL,
  `otro` varchar(70) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 por responder, 0 anulado, 2 respondido'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacionProveedor`
--

CREATE TABLE `cotizacionProveedor` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fabricante` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estatus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 anulado, 1 recibido, 2 cotizado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dashboard`
--

CREATE TABLE `dashboard` (
  `cliente` int(11) UNSIGNED NOT NULL,
  `numero` int(11) NOT NULL,
  `presentacion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrolloPruebaMSDN`
--

CREATE TABLE `desarrolloPruebaMSDN` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDesarrolloPruebaVS` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(20) NOT NULL,
  `fechaInstalacion` date DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `equipoUsuario` varchar(70) DEFAULT NULL,
  `msdn` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrolloPruebaMSDNSam`
--

CREATE TABLE `desarrolloPruebaMSDNSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(20) NOT NULL,
  `fechaInstalacion` date DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `equipoUsuario` varchar(70) DEFAULT NULL,
  `msdn` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrolloPruebaSQL`
--

CREATE TABLE `desarrolloPruebaSQL` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(20) NOT NULL,
  `fechaInstalacion` date DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `equipoUsuario` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrolloPruebaVS`
--

CREATE TABLE `desarrolloPruebaVS` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(20) NOT NULL,
  `fechaInstalacion` date DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `equipoUsuario` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrolloPruebaVSSam`
--

CREATE TABLE `desarrolloPruebaVSSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(20) NOT NULL,
  `fechaInstalacion` date DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `equipoUsuario` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrolloPruebaWindows`
--

CREATE TABLE `desarrolloPruebaWindows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(20) NOT NULL,
  `fechaInstalacion` date DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `equipoUsuario` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleContrato`
--

CREATE TABLE `detalleContrato` (
  `idDetalleContrato` int(11) UNSIGNED NOT NULL COMMENT 'identificador del detalle del contrato',
  `idContrato` int(11) UNSIGNED NOT NULL COMMENT 'identificador del contrato',
  `idProducto` int(11) NOT NULL COMMENT 'identificador del producto',
  `idEdicion` int(11) NOT NULL COMMENT 'identificador de la edicion',
  `version` varchar(20) NOT NULL COMMENT 'version del producto',
  `sku` varchar(100) NOT NULL COMMENT 'sku del producto',
  `tipo` enum('subscripcion','perpetuo','otro','software assurance') NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double(15,2) NOT NULL,
  `asignacion` varchar(50) NOT NULL,
  `cantManual` int(11) DEFAULT '0',
  `instManual` int(11) DEFAULT '0',
  `metrica` varchar(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleContrato_VMWare`
--

CREATE TABLE `detalleContrato_VMWare` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `producto` varchar(100) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `llave` varchar(70) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `medida` varchar(20) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `numero` int(11) NOT NULL,
  `fechaOrden` date NOT NULL,
  `soporte` varchar(40) DEFAULT NULL,
  `fechaFinCobertura` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleContrato_VMWareSam`
--

CREATE TABLE `detalleContrato_VMWareSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `producto` varchar(100) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `llave` varchar(70) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `medida` varchar(20) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `numero` int(11) NOT NULL,
  `fechaOrden` date NOT NULL,
  `soporte` varchar(40) DEFAULT NULL,
  `fechaFinCobertura` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleCotProveedor`
--

CREATE TABLE `detalleCotProveedor` (
  `id` int(11) UNSIGNED NOT NULL,
  `idCotizacion` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estatus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 anulado, 1 activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleEvento`
--

CREATE TABLE `detalleEvento` (
  `id` int(11) NOT NULL,
  `evento` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleMaestra`
--

CREATE TABLE `detalleMaestra` (
  `idDetalle` int(11) NOT NULL COMMENT 'identificador del detalle de la tabla',
  `idMaestra` int(11) NOT NULL COMMENT 'identificador de la tabla maestra',
  `descripcion` varchar(150) NOT NULL COMMENT 'descripcion de la tabla',
  `campo1` varchar(150) DEFAULT NULL,
  `campo2` varchar(512) DEFAULT NULL,
  `campo3` varchar(256) DEFAULT NULL,
  `idForaneo` int(11) DEFAULT NULL COMMENT 'identificador foraneo',
  `prioridad` int(11) DEFAULT NULL COMMENT 'orden de prioridad de la busqueda',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleServidorSap`
--

CREATE TABLE `detalleServidorSap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `compSoft` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo`
--

CREATE TABLE `detalles_equipo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `dias1` float(10,2) NOT NULL,
  `dias2` float(10,2) NOT NULL,
  `dias3` float(10,2) NOT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo2`
--

CREATE TABLE `detalles_equipo2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `os` varchar(250) DEFAULT NULL,
  `familia` varchar(150) DEFAULT NULL,
  `edicion` varchar(150) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL,
  `asignacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo2BancoSolidario`
--

CREATE TABLE `detalles_equipo2BancoSolidario` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL,
  `asignacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo2Sam`
--

CREATE TABLE `detalles_equipo2Sam` (
  `id` bigint(20) NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipoDesuso`
--

CREATE TABLE `detalles_equipoDesuso` (
  `id` int(11) UNSIGNED NOT NULL,
  `idPrueba` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT '0.00',
  `dias2` float(10,2) DEFAULT '0.00',
  `dias3` float(10,2) DEFAULT '0.00',
  `minimo` int(10) DEFAULT '0',
  `activo` int(10) UNSIGNED DEFAULT '0',
  `tipo` int(10) UNSIGNED DEFAULT '0',
  `rango` int(10) UNSIGNED DEFAULT '0',
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipoMSCloud`
--

CREATE TABLE `detalles_equipoMSCloud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT '0.00',
  `dias2` float(10,2) DEFAULT '0.00',
  `dias3` float(10,2) DEFAULT '0.00',
  `minimo` int(10) NOT NULL DEFAULT '0',
  `activo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tipo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rango` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipoSAMDiagnostic`
--

CREATE TABLE `detalles_equipoSAMDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `os` varchar(250) DEFAULT NULL,
  `familia` varchar(150) DEFAULT NULL,
  `edicion` varchar(150) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `dias1` float(10,2) DEFAULT '0.00',
  `dias2` float(10,2) DEFAULT '0.00',
  `dias3` float(10,2) DEFAULT '0.00',
  `minimo` int(10) NOT NULL DEFAULT '0',
  `activo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tipo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rango` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_adobe`
--

CREATE TABLE `detalles_equipo_adobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(150) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL,
  `asignacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_adobeSam`
--

CREATE TABLE `detalles_equipo_adobeSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_ibm`
--

CREATE TABLE `detalles_equipo_ibm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(150) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL,
  `asignacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_ibmSam`
--

CREATE TABLE `detalles_equipo_ibmSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_oracle`
--

CREATE TABLE `detalles_equipo_oracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(150) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_OracleAIX`
--

CREATE TABLE `detalles_equipo_OracleAIX` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_OracleAIXSam`
--

CREATE TABLE `detalles_equipo_OracleAIXSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_OracleLinux`
--

CREATE TABLE `detalles_equipo_OracleLinux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(20) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_OracleLinuxSam`
--

CREATE TABLE `detalles_equipo_OracleLinuxSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_oracleSam`
--

CREATE TABLE `detalles_equipo_oracleSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_OracleSolaris`
--

CREATE TABLE `detalles_equipo_OracleSolaris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(20) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_OracleSolarisSam`
--

CREATE TABLE `detalles_equipo_OracleSolarisSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_SPLA`
--

CREATE TABLE `detalles_equipo_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_SPLASam`
--

CREATE TABLE `detalles_equipo_SPLASam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `familia` varchar(150) NOT NULL,
  `edicion` varchar(150) NOT NULL,
  `version` varchar(50) NOT NULL,
  `dias1` float(10,2) DEFAULT NULL,
  `dias2` float(10,2) DEFAULT NULL,
  `dias3` float(10,2) DEFAULT NULL,
  `minimo` int(10) NOT NULL,
  `activo` int(10) UNSIGNED NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `rango` int(10) UNSIGNED NOT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_UnixAIX`
--

CREATE TABLE `detalles_equipo_UnixAIX` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_UnixAIXSam`
--

CREATE TABLE `detalles_equipo_UnixAIXSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_UnixLinux`
--

CREATE TABLE `detalles_equipo_UnixLinux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(150) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(20) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_UnixLinuxSam`
--

CREATE TABLE `detalles_equipo_UnixLinuxSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_UnixSolaris`
--

CREATE TABLE `detalles_equipo_UnixSolaris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(20) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_equipo_UnixSolarisSam`
--

CREATE TABLE `detalles_equipo_UnixSolarisSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `versionOs` varchar(100) DEFAULT NULL,
  `virtual` varchar(20) DEFAULT NULL,
  `memoria` int(11) DEFAULT NULL,
  `cpu` int(11) DEFAULT '0',
  `versionCpu` varchar(70) DEFAULT NULL,
  `core` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ediciones`
--

CREATE TABLE `ediciones` (
  `idEdicion` int(11) NOT NULL COMMENT 'identificador de la edicion',
  `nombre` varchar(100) NOT NULL COMMENT 'nombre de la edicion',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edicproducto`
--

CREATE TABLE `edicproducto` (
  `idFabricante` int(11) NOT NULL COMMENT 'identificador del fabricante',
  `idProducto` int(11) NOT NULL COMMENT 'identificador del producto',
  `idEdicion` int(11) NOT NULL COMMENT 'identificador de la edicion',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elimEquiposLAEAdobe`
--

CREATE TABLE `elimEquiposLAEAdobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(30) NOT NULL COMMENT 'nombre del archivo',
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elimEquiposLAEIbm`
--

CREATE TABLE `elimEquiposLAEIbm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(30) NOT NULL COMMENT 'nombre del archivo',
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elimEquiposLAEMicrosoft`
--

CREATE TABLE `elimEquiposLAEMicrosoft` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(30) NOT NULL COMMENT 'nombre del archivo',
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `apellido` varchar(70) NOT NULL,
  `correo` varchar(70) NOT NULL,
  `userName` varchar(70) NOT NULL,
  `clave` varchar(150) NOT NULL,
  `estado` int(11) UNSIGNED NOT NULL,
  `fecha_registro` date NOT NULL,
  `ultAcceso` date DEFAULT NULL,
  `userNADD` varchar(70) DEFAULT NULL,
  `passNADD` varchar(128) DEFAULT NULL,
  `fechaIniNADD` date DEFAULT NULL,
  `fechaFinNADD` date DEFAULT NULL,
  `userCent` varchar(70) DEFAULT NULL,
  `passCent` varchar(128) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchAdobeSam`
--

CREATE TABLE `encArchAdobeSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchIbmSam`
--

CREATE TABLE `encArchIbmSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchOracleSam`
--

CREATE TABLE `encArchOracleSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchSam`
--

CREATE TABLE `encArchSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchSapSam`
--

CREATE TABLE `encArchSapSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchSPLASam`
--

CREATE TABLE `encArchSPLASam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchUnixIBMSam`
--

CREATE TABLE `encArchUnixIBMSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchUnixOracleSam`
--

CREATE TABLE `encArchUnixOracleSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encArchVMWareSam`
--

CREATE TABLE `encArchVMWareSam` (
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL COMMENT 'fecha en que se actualizo el repositorio',
  `custom` varchar(150) DEFAULT NULL COMMENT 'nombre archivo de la carpeta custom',
  `custom1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta custom',
  `fechaCustom` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta custom',
  `fechaCustom1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta custom',
  `otros` varchar(150) DEFAULT NULL COMMENT 'nombre del archivo de la carpeta otros',
  `otros1` varchar(150) DEFAULT NULL COMMENT 'nombre del segundo archivo de la carpeta otros',
  `fechaOtros` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo la carpeta otros',
  `fechaOtros1` datetime DEFAULT NULL COMMENT 'fecha en que se actualizo el segundo archivo de la carpeta otros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafAdobeSam`
--

CREATE TABLE `encGrafAdobeSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compra'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafIbmSam`
--

CREATE TABLE `encGrafIbmSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compras'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafOracleSam`
--

CREATE TABLE `encGrafOracleSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compras'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafSam`
--

CREATE TABLE `encGrafSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compras'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafSapSam`
--

CREATE TABLE `encGrafSapSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoDespliegue3` varchar(100) DEFAULT NULL COMMENT 'nombre del tercer archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compras'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafSPLASam`
--

CREATE TABLE `encGrafSPLASam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compras'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafUnixIBMSam`
--

CREATE TABLE `encGrafUnixIBMSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoDespliegue3` varchar(100) DEFAULT NULL COMMENT 'nombre del tercer archivo de la carpeta otrosdespliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compra'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafUnixOracleSam`
--

CREATE TABLE `encGrafUnixOracleSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoDespliegue2` varchar(100) DEFAULT NULL COMMENT 'nombre del segundo archivo de despliegue',
  `archivoDespliegue3` varchar(100) DEFAULT NULL COMMENT 'nombre del tercer archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compra'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encGrafVMWareSam`
--

CREATE TABLE `encGrafVMWareSam` (
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` datetime NOT NULL,
  `archivoDespliegue1` varchar(100) DEFAULT NULL COMMENT 'nombre del primer archivo de despliegue',
  `archivoCompra` varchar(100) DEFAULT NULL COMMENT 'nombre del archivo de compra'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equipos`
--

CREATE TABLE `escaneo_equipos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `status` varchar(250) NOT NULL,
  `errors` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equipos2`
--

CREATE TABLE `escaneo_equipos2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equipos2Sam`
--

CREATE TABLE `escaneo_equipos2Sam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equiposDesuso`
--

CREATE TABLE `escaneo_equiposDesuso` (
  `id` int(11) UNSIGNED NOT NULL,
  `idPrueba` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equiposMSCloud`
--

CREATE TABLE `escaneo_equiposMSCloud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equiposSAMDiagnostic`
--

CREATE TABLE `escaneo_equiposSAMDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equipos_adobe`
--

CREATE TABLE `escaneo_equipos_adobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equipos_ibm`
--

CREATE TABLE `escaneo_equipos_ibm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equipos_oracle`
--

CREATE TABLE `escaneo_equipos_oracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_equipos_SPLA`
--

CREATE TABLE `escaneo_equipos_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escaneo_troubleshoot`
--

CREATE TABLE `escaneo_troubleshoot` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED DEFAULT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `errors` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `pais` int(11) NOT NULL,
  `estado` varchar(70) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fabricantes`
--

CREATE TABLE `fabricantes` (
  `idFabricante` int(11) NOT NULL COMMENT 'identificador del fabricante',
  `nombre` varchar(50) NOT NULL COMMENT 'nombre del fabricante',
  `logo` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `color` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fabricantesCliente`
--

CREATE TABLE `fabricantesCliente` (
  `idFabricante` int(11) UNSIGNED NOT NULL COMMENT 'identificador del fabricante',
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador del cliente',
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs`
--

CREATE TABLE `filepcs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs2`
--

CREATE TABLE `filepcs2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) DEFAULT NULL,
  `objectclass` varchar(250) DEFAULT NULL,
  `cn` varchar(250) DEFAULT NULL,
  `useracountcontrol` varchar(250) DEFAULT NULL,
  `lastlogon` varchar(250) DEFAULT NULL,
  `pwdlastset` varchar(250) DEFAULT NULL,
  `os` varchar(250) DEFAULT NULL,
  `lastlogontimes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs2BancoSolidario`
--

CREATE TABLE `filepcs2BancoSolidario` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcsAux`
--

CREATE TABLE `filepcsAux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) DEFAULT NULL,
  `objectclass` varchar(250) DEFAULT NULL,
  `cn` varchar(250) DEFAULT NULL,
  `useracountcontrol` varchar(250) DEFAULT NULL,
  `lastlogon` varchar(250) DEFAULT NULL,
  `pwdlastset` varchar(250) DEFAULT NULL,
  `os` varchar(250) DEFAULT NULL,
  `lastlogontimes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcsDesuso`
--

CREATE TABLE `filepcsDesuso` (
  `id` int(11) UNSIGNED NOT NULL,
  `idPrueba` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcsMSCloud`
--

CREATE TABLE `filepcsMSCloud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcsSAMDiagnostic`
--

CREATE TABLE `filepcsSAMDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `dn` varchar(250) DEFAULT NULL,
  `objectclass` varchar(250) DEFAULT NULL,
  `cn` varchar(250) DEFAULT NULL,
  `useracountcontrol` varchar(250) DEFAULT NULL,
  `lastlogon` varchar(250) DEFAULT NULL,
  `pwdlastset` varchar(250) DEFAULT NULL,
  `os` varchar(250) DEFAULT NULL,
  `lastlogontimes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs_adobe`
--

CREATE TABLE `filepcs_adobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs_adobeAux`
--

CREATE TABLE `filepcs_adobeAux` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs_ibm`
--

CREATE TABLE `filepcs_ibm` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs_ibmAux`
--

CREATE TABLE `filepcs_ibmAux` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs_oracle`
--

CREATE TABLE `filepcs_oracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filepcs_SPLA`
--

CREATE TABLE `filepcs_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) NOT NULL,
  `objectclass` varchar(250) NOT NULL,
  `cn` varchar(250) NOT NULL,
  `useracountcontrol` varchar(250) NOT NULL,
  `lastlogon` varchar(250) NOT NULL,
  `pwdlastset` varchar(250) NOT NULL,
  `os` varchar(250) NOT NULL,
  `lastlogontimes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gantt`
--

CREATE TABLE `gantt` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(70) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ganttDetalle`
--

CREATE TABLE `ganttDetalle` (
  `idGantt` bigint(20) UNSIGNED NOT NULL,
  `L1` date DEFAULT NULL,
  `L2` date DEFAULT NULL,
  `L3` date DEFAULT NULL,
  `L4` date DEFAULT NULL,
  `TS1` date DEFAULT NULL,
  `TS2` date DEFAULT NULL,
  `TS3` date DEFAULT NULL,
  `TS4` date DEFAULT NULL,
  `RS1` date DEFAULT NULL,
  `RS2` date DEFAULT NULL,
  `RS3` date DEFAULT NULL,
  `RS4` date DEFAULT NULL,
  `RP1` date DEFAULT NULL,
  `RP2` date DEFAULT NULL,
  `RP3` date DEFAULT NULL,
  `RP4` date DEFAULT NULL,
  `Opt1` date DEFAULT NULL,
  `Opt2` date DEFAULT NULL,
  `Opt3` date DEFAULT NULL,
  `Opt4` date DEFAULT NULL,
  `Mit1` date DEFAULT NULL,
  `Mit2` date DEFAULT NULL,
  `Mit3` date DEFAULT NULL,
  `Mit4` date DEFAULT NULL,
  `Reno1` date DEFAULT NULL,
  `Reno2` date DEFAULT NULL,
  `Reno3` date DEFAULT NULL,
  `Reno4` date DEFAULT NULL,
  `TU1` date DEFAULT NULL,
  `TU2` date DEFAULT NULL,
  `TU3` date DEFAULT NULL,
  `TU4` date DEFAULT NULL,
  `Rev1` date DEFAULT NULL,
  `Rev2` date DEFAULT NULL,
  `Rev3` date DEFAULT NULL,
  `Rev4` date DEFAULT NULL,
  `P1` date DEFAULT NULL,
  `P2` date DEFAULT NULL,
  `P3` date DEFAULT NULL,
  `P4` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicoAppsSPLA`
--

CREATE TABLE `historicoAppsSPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idHistorico` bigint(20) UNSIGNED NOT NULL,
  `VM` varchar(70) NOT NULL,
  `customers` varchar(70) DEFAULT NULL,
  `VMDate` date DEFAULT NULL,
  `hostDate` date DEFAULT NULL,
  `prod` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `sockets` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `cpu` int(11) NOT NULL,
  `Qty` int(11) DEFAULT '0',
  `SKU` varchar(70) DEFAULT NULL,
  `type` varchar(70) DEFAULT NULL,
  `tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicoCurrentSPLA`
--

CREATE TABLE `historicoCurrentSPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idHistorico` bigint(20) UNSIGNED NOT NULL,
  `customers` varchar(70) DEFAULT NULL,
  `prod` varchar(70) NOT NULL,
  `SKU` varchar(70) DEFAULT NULL,
  `inst` int(11) NOT NULL,
  `Qty` int(11) DEFAULT '0',
  `price` double(15,2) DEFAULT '0.00',
  `QtySale` int(11) DEFAULT '0',
  `priceSale` double(15,2) DEFAULT '0.00',
  `tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicoCustomerSPLA`
--

CREATE TABLE `historicoCustomerSPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idHistorico` bigint(20) UNSIGNED NOT NULL,
  `nombreCliente` varchar(70) NOT NULL,
  `contrato` varchar(70) NOT NULL,
  `inicioContrato` date NOT NULL,
  `finContrato` date NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicoSPLA`
--

CREATE TABLE `historicoSPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(70) NOT NULL,
  `fecha` date NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicoVMSPLA`
--

CREATE TABLE `historicoVMSPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idHistorico` bigint(20) UNSIGNED NOT NULL,
  `DC` varchar(70) NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `VM` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `sockets` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `cpu` int(11) NOT NULL,
  `hostDate` date DEFAULT NULL,
  `VMDate` date DEFAULT NULL,
  `customers` varchar(70) DEFAULT NULL,
  `Qty` int(11) DEFAULT '0',
  `SKU` varchar(70) DEFAULT NULL,
  `type` varchar(70) DEFAULT NULL,
  `QtySale` int(11) DEFAULT '0',
  `priceSale` double(15,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hostsDiagnostic`
--

CREATE TABLE `hostsDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `Host` varchar(70) NOT NULL,
  `CPUCount` int(11) NOT NULL,
  `CoresPerCPU` int(11) NOT NULL,
  `TotalNumberCores` int(11) NOT NULL,
  `Datacenter` varchar(70) DEFAULT NULL,
  `Cluster` varchar(70) DEFAULT NULL,
  `CPUModel` varchar(70) NOT NULL,
  `TotalVMsCount` int(11) NOT NULL,
  `htAvailable` varchar(20) DEFAULT NULL,
  `htActive` varchar(20) DEFAULT NULL,
  `NumberVMsPerCore` int(11) NOT NULL,
  `NumbervCPUs` int(11) NOT NULL,
  `NumbervCPUsPerCore` int(11) NOT NULL,
  `BootTime` varchar(30) DEFAULT NULL,
  `BIOSDate` varchar(30) DEFAULT NULL,
  `vCenterUUID` varchar(70) NOT NULL,
  `InstallationDate` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lau_users`
--

CREATE TABLE `lau_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dn` varchar(250) DEFAULT NULL,
  `objectclass` varchar(250) DEFAULT NULL,
  `cn` varchar(250) DEFAULT NULL,
  `lastlogon` varchar(250) DEFAULT NULL,
  `pwdlastset` varchar(250) DEFAULT NULL,
  `sAMAccountName` varchar(250) DEFAULT NULL,
  `lastLogonTimestamp` varchar(250) DEFAULT NULL,
  `mail` varchar(250) DEFAULT NULL,
  `minimo` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `licenciasCentralizador`
--

CREATE TABLE `licenciasCentralizador` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'identificador del generador de seriales',
  `cliente` int(11) UNSIGNED NOT NULL COMMENT 'identificador de la empresa',
  `fechaIni` date NOT NULL COMMENT 'fecha de inicio de la App',
  `fechaFin` date NOT NULL COMMENT 'fecha fin de la App',
  `fechaCreacion` datetime NOT NULL,
  `serial` varchar(100) NOT NULL COMMENT 'serial de validacion',
  `serialHDD` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado, 2 en uso'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `licenciasESXiVCenter`
--

CREATE TABLE `licenciasESXiVCenter` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `licenseKey` varchar(70) DEFAULT NULL,
  `product` varchar(100) DEFAULT NULL,
  `usages` varchar(20) DEFAULT NULL,
  `capacity` varchar(20) DEFAULT NULL,
  `labels` varchar(70) DEFAULT NULL,
  `expires` varchar(20) DEFAULT NULL,
  `assigned` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `links`
--

CREATE TABLE `links` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_officce`
--

CREATE TABLE `lista_officce` (
  `id` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `precio` float(10,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logWebtool`
--

CREATE TABLE `logWebtool` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idModulo` int(11) NOT NULL,
  `idOperacion` int(11) NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `IP` varchar(15) NOT NULL,
  `comentario` varchar(70) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moduloRecordatorio`
--

CREATE TABLE `moduloRecordatorio` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MSCloud`
--

CREATE TABLE `MSCloud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(70) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `LAD` varchar(70) DEFAULT NULL,
  `LAE` varchar(70) DEFAULT NULL,
  `archivo` varchar(70) DEFAULT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivelServicio`
--

CREATE TABLE `nivelServicio` (
  `id` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(20) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification`
--

CREATE TABLE `notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `showed` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `noti_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sender_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagosFabricantes`
--

CREATE TABLE `pagosFabricantes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) NOT NULL,
  `descripcion` varchar(70) NOT NULL,
  `fechaReg` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagosFabricantesDetalle`
--

CREATE TABLE `pagosFabricantesDetalle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idPago` bigint(20) NOT NULL,
  `idFabricante` int(11) NOT NULL,
  `aniversario` date DEFAULT NULL,
  `renovacion` date DEFAULT NULL,
  `monto` double(15,2) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(20) UNSIGNED NOT NULL,
  `nombre` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `bandera` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `estado` int(2) UNSIGNED NOT NULL DEFAULT '1',
  `fecha_registro` date NOT NULL DEFAULT '0000-00-00',
  `usuario_registro` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fecha_actualizacion` date NOT NULL DEFAULT '0000-00-00',
  `usuario_actualizacion` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `modulo` int(11) UNSIGNED NOT NULL,
  `submodulo` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisoSam`
--

CREATE TABLE `permisoSam` (
  `idCliente` int(11) DEFAULT '0' COMMENT 'identificador del cliente',
  `idEmpleado` int(11) UNSIGNED NOT NULL,
  `idPermiso` int(11) NOT NULL COMMENT 'identificador del permiso'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisosGenerales`
--

CREATE TABLE `permisosGenerales` (
  `idPermisos` int(11) UNSIGNED NOT NULL COMMENT 'identificador del permiso',
  `nombre` varchar(50) NOT NULL COMMENT 'nombre del permiso',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phases`
--

CREATE TABLE `phases` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `phase_type_id` int(11) DEFAULT NULL,
  `phase_state_id` int(11) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phasesfactories`
--

CREATE TABLE `phasesfactories` (
  `id` int(10) UNSIGNED NOT NULL,
  `phases_id` int(10) UNSIGNED DEFAULT NULL,
  `factories_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phase_state`
--

CREATE TABLE `phase_state` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phase_type`
--

CREATE TABLE `phase_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `poc`
--

CREATE TABLE `poc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pocSAP`
--

CREATE TABLE `pocSAP` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `politSam`
--

CREATE TABLE `politSam` (
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `adquisicion` varchar(70) DEFAULT NULL,
  `identificacion` varchar(70) DEFAULT NULL,
  `conformidad` varchar(70) DEFAULT NULL,
  `comunicacion` varchar(70) DEFAULT NULL,
  `disposicion` varchar(70) DEFAULT NULL,
  `documentacion` varchar(70) DEFAULT NULL,
  `financiera` varchar(70) DEFAULT NULL,
  `legislacion` varchar(70) DEFAULT NULL,
  `politicas` varchar(70) DEFAULT NULL,
  `programas` varchar(70) DEFAULT NULL,
  `proyectos` varchar(70) DEFAULT NULL,
  `proveedores` varchar(70) DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `carpeta` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portal`
--

CREATE TABLE `portal` (
  `id` int(11) NOT NULL,
  `tipoUsuario` varchar(25) NOT NULL,
  `tipoDocumento` varchar(25) NOT NULL,
  `para` varchar(20) NOT NULL,
  `theme` varchar(70) NOT NULL,
  `manufacturer` varchar(20) NOT NULL,
  `spanish` varchar(400) NOT NULL,
  `english` varchar(400) NOT NULL,
  `imgSpanish` varchar(30) NOT NULL,
  `imgEnglish` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodFabricante`
--

CREATE TABLE `prodFabricante` (
  `idProducto` int(11) NOT NULL COMMENT 'identificador del producto',
  `idFabricante` int(11) NOT NULL COMMENT 'identificador del fabricante',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(11) NOT NULL COMMENT 'identificador del producto',
  `nombre` varchar(50) NOT NULL COMMENT 'nombre del producto',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pruebaDesuso`
--

CREATE TABLE `pruebaDesuso` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(70) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pvuCliente`
--

CREATE TABLE `pvuCliente` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `servidor` varchar(70) NOT NULL,
  `tipoProcesador` varchar(70) NOT NULL,
  `producto` varchar(100) NOT NULL,
  `conteoProcesadores` int(11) NOT NULL,
  `conteoCore` int(11) NOT NULL,
  `usaProcesadores` int(11) NOT NULL,
  `usaCore` int(11) NOT NULL,
  `pvu` int(11) DEFAULT '0',
  `conteo` int(11) DEFAULT '0',
  `totalPvu` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pvuClienteAIX`
--

CREATE TABLE `pvuClienteAIX` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) NOT NULL,
  `empleado` int(11) NOT NULL,
  `servidor` varchar(70) NOT NULL,
  `tipoProcesador` varchar(70) DEFAULT NULL,
  `producto` varchar(100) NOT NULL,
  `conteoProcesadores` int(11) DEFAULT NULL,
  `conteoCore` int(11) DEFAULT NULL,
  `usaProcesadores` int(11) DEFAULT NULL,
  `usaCore` int(11) DEFAULT NULL,
  `pvu` int(11) DEFAULT NULL,
  `conteo` int(11) DEFAULT NULL,
  `totalPvu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pvuClienteLinux`
--

CREATE TABLE `pvuClienteLinux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) NOT NULL,
  `empleado` int(11) NOT NULL,
  `servidor` varchar(70) NOT NULL,
  `tipoProcesador` varchar(70) NOT NULL,
  `producto` varchar(100) NOT NULL,
  `conteoProcesadores` int(11) DEFAULT NULL,
  `conteoCore` int(11) DEFAULT NULL,
  `usaProcesadores` int(11) DEFAULT NULL,
  `usaCore` int(11) DEFAULT NULL,
  `pvu` int(11) DEFAULT NULL,
  `conteo` int(11) DEFAULT NULL,
  `totalPvu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pvuClienteSolaris`
--

CREATE TABLE `pvuClienteSolaris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) NOT NULL,
  `empleado` int(11) NOT NULL,
  `servidor` varchar(70) NOT NULL,
  `tipoProcesador` varchar(70) DEFAULT NULL,
  `producto` varchar(100) NOT NULL,
  `conteoProcesadores` int(11) DEFAULT NULL,
  `conteoCore` int(11) DEFAULT NULL,
  `usaProcesadores` int(11) DEFAULT NULL,
  `usaCore` int(11) DEFAULT NULL,
  `pvu` int(11) DEFAULT NULL,
  `conteo` int(11) DEFAULT NULL,
  `totalPvu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recordatorios`
--

CREATE TABLE `recordatorios` (
  `id` int(11) UNSIGNED NOT NULL,
  `modulo` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `correo` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportarFallas`
--

CREATE TABLE `reportarFallas` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fabricante` int(11) UNSIGNED NOT NULL,
  `motivo` varchar(512) NOT NULL,
  `correo` varchar(70) NOT NULL,
  `fecha` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 anulado, 1 recibido, 2 respondido'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respCotizProv`
--

CREATE TABLE `respCotizProv` (
  `id` int(11) UNSIGNED NOT NULL,
  `idCotizacionProv` int(11) UNSIGNED NOT NULL,
  `proveedor` varchar(70) NOT NULL,
  `nroCotizacion` varchar(20) NOT NULL,
  `archivo` varchar(70) DEFAULT NULL,
  `estatus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 anulado, 1 activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumenServidorSap`
--

CREATE TABLE `resumenServidorSap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `compSoft` varchar(250) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumenServidorSapSam`
--

CREATE TABLE `resumenServidorSapSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `compSoft` varchar(250) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) DEFAULT NULL,
  `version` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumenUsuariosModuloSap`
--

CREATE TABLE `resumenUsuariosModuloSap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `usuarios` varchar(250) NOT NULL,
  `componente` varchar(250) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `diasUltAcceso` int(11) NOT NULL,
  `departamento` varchar(70) DEFAULT NULL,
  `cargo` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumenUsuariosModuloSapSam`
--

CREATE TABLE `resumenUsuariosModuloSapSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `usuarios` varchar(250) NOT NULL,
  `componente` varchar(250) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `diasUltAcceso` int(11) NOT NULL,
  `departamento` varchar(70) DEFAULT NULL,
  `cargo` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_adobe`
--

CREATE TABLE `resumen_adobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `fecha_instalacion` date DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_adobeSam`
--

CREATE TABLE `resumen_adobeSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_ibm`
--

CREATE TABLE `resumen_ibm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(11) DEFAULT NULL,
  `fecha_instalacion` date DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_ibmSam`
--

CREATE TABLE `resumen_ibmSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(11) NOT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_MSCloud`
--

CREATE TABLE `resumen_MSCloud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(11) DEFAULT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_office`
--

CREATE TABLE `resumen_office` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` int(11) NOT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_office2`
--

CREATE TABLE `resumen_office2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(20) DEFAULT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_office2Sam`
--

CREATE TABLE `resumen_office2Sam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(11) DEFAULT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_oracle`
--

CREATE TABLE `resumen_oracle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_OracleAIX`
--

CREATE TABLE `resumen_OracleAIX` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_OracleAIXSam`
--

CREATE TABLE `resumen_OracleAIXSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_OracleLinux`
--

CREATE TABLE `resumen_OracleLinux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_OracleLinuxSam`
--

CREATE TABLE `resumen_OracleLinuxSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_oracleSam`
--

CREATE TABLE `resumen_oracleSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_OracleSolaris`
--

CREATE TABLE `resumen_OracleSolaris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_OracleSolarisSam`
--

CREATE TABLE `resumen_OracleSolarisSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_others`
--

CREATE TABLE `resumen_others` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_SAMDiagnostic`
--

CREATE TABLE `resumen_SAMDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(11) DEFAULT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_server`
--

CREATE TABLE `resumen_server` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `fecha_instalacion` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_SPLA`
--

CREATE TABLE `resumen_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `familia` varchar(250) DEFAULT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(11) DEFAULT NULL,
  `fecha_instalacion` date DEFAULT '0000-00-00',
  `Qty` int(11) DEFAULT '0',
  `SKU` varchar(70) DEFAULT NULL,
  `type` varchar(70) DEFAULT NULL,
  `QtySale` int(11) DEFAULT '0',
  `priceSale` double(15,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_SPLASam`
--

CREATE TABLE `resumen_SPLASam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(250) DEFAULT NULL,
  `familia` varchar(250) DEFAULT NULL,
  `edicion` varchar(250) DEFAULT NULL,
  `version` varchar(11) DEFAULT NULL,
  `fecha_instalacion` date DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_UnixAIX`
--

CREATE TABLE `resumen_UnixAIX` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_UnixAIXSam`
--

CREATE TABLE `resumen_UnixAIXSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_UnixLinux`
--

CREATE TABLE `resumen_UnixLinux` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_UnixLinuxSam`
--

CREATE TABLE `resumen_UnixLinuxSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_UnixSolaris`
--

CREATE TABLE `resumen_UnixSolaris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_UnixSolarisSam`
--

CREATE TABLE `resumen_UnixSolarisSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_VMWare`
--

CREATE TABLE `resumen_VMWare` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `licenseKey` varchar(70) DEFAULT NULL,
  `familia` varchar(100) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `uso` int(11) DEFAULT '0',
  `metrica` varchar(20) DEFAULT NULL,
  `capacidad` varchar(20) NOT NULL,
  `etiqueta` varchar(70) NOT NULL,
  `asignado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumen_VMWareSam`
--

CREATE TABLE `resumen_VMWareSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `licenseKey` varchar(70) DEFAULT NULL,
  `familia` varchar(100) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `uso` int(11) DEFAULT '0',
  `metrica` varchar(20) DEFAULT NULL,
  `capacidad` varchar(20) NOT NULL,
  `etiqueta` varchar(70) NOT NULL,
  `asignado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_user`
--

CREATE TABLE `roles_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `roles_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SAMDiagnostic`
--

CREATE TABLE `SAMDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(70) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `LAD` varchar(70) DEFAULT NULL,
  `LAE` varchar(70) DEFAULT NULL,
  `resultado` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scheduling`
--

CREATE TABLE `scheduling` (
  `id` int(11) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `tx_descrip` varchar(70) NOT NULL,
  `fe_fecha_creacion` datetime NOT NULL,
  `fe_fecha_inicio` datetime NOT NULL,
  `tx_schedule` varchar(70) NOT NULL,
  `int_repeat_days` int(11) DEFAULT NULL,
  `int_repeat_weeks` int(11) DEFAULT NULL,
  `tx_days` varchar(70) DEFAULT NULL,
  `tx_months` varchar(120) DEFAULT NULL,
  `num_days` varchar(100) DEFAULT NULL,
  `tiny_status` tinyint(4) NOT NULL DEFAULT '1',
  `tx_tipo_scheduling` varchar(1) DEFAULT 'L'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_query`
--

CREATE TABLE `service_query` (
  `id` int(11) NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `query` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_serial`
--

CREATE TABLE `service_serial` (
  `id` int(11) NOT NULL,
  `serial` bigint(20) DEFAULT NULL,
  `id_disco` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sheet1`
--

CREATE TABLE `sheet1` (
  `numero` int(11) NOT NULL,
  `producto` varchar(200) NOT NULL,
  `que_es` varchar(200) DEFAULT NULL,
  `licenciable` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SPLACliente`
--

CREATE TABLE `SPLACliente` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `nombreCliente` varchar(70) NOT NULL,
  `contrato` varchar(70) NOT NULL,
  `inicioContrato` date NOT NULL,
  `finContrato` date NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `VM` varchar(70) DEFAULT NULL,
  `licenciasWin` varchar(70) DEFAULT NULL,
  `licenciasSQL` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SPLADiagnostic`
--

CREATE TABLE `SPLADiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(70) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `Host` varchar(70) DEFAULT NULL,
  `VMs` varchar(70) DEFAULT NULL,
  `archivo` varchar(20) DEFAULT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SPLASKU`
--

CREATE TABLE `SPLASKU` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `SKU` varchar(70) NOT NULL,
  `description` varchar(100) NOT NULL,
  `product` varchar(70) NOT NULL,
  `partNumber` varchar(30) NOT NULL,
  `price` double(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SPLAVirtualMachine`
--

CREATE TABLE `SPLAVirtualMachine` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `DC` varchar(70) NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `VM` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `sockets` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `cpu` int(11) NOT NULL,
  `hostDate` date DEFAULT NULL,
  `VMDate` date DEFAULT NULL,
  `customers` varchar(70) DEFAULT NULL,
  `Qty` int(11) DEFAULT '0',
  `SKU` varchar(70) DEFAULT NULL,
  `type` varchar(70) DEFAULT NULL,
  `QtySale` int(11) DEFAULT '0',
  `priceSale` double(15,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sqlServerClientes`
--

CREATE TABLE `sqlServerClientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `MSDN` enum('Si','No') NOT NULL DEFAULT 'No',
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `centroCosto` varchar(70) DEFAULT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sqlServerClientesSam`
--

CREATE TABLE `sqlServerClientesSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `MSDN` enum('Si','No') NOT NULL DEFAULT 'No',
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `centroCosto` varchar(70) DEFAULT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sqlServerClientesSPLA_Sam`
--

CREATE TABLE `sqlServerClientesSPLA_Sam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sqlServerClientes_SPLA`
--

CREATE TABLE `sqlServerClientes_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `submodulos`
--

CREATE TABLE `submodulos` (
  `id` int(11) UNSIGNED NOT NULL,
  `modulo` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tablaMaestra`
--

CREATE TABLE `tablaMaestra` (
  `id` int(11) NOT NULL COMMENT 'identificador de la tabla maestra',
  `descripcion` varchar(40) NOT NULL COMMENT 'descripcion de la tabla maestra',
  `observacion` varchar(80) DEFAULT NULL COMMENT 'observacion de la tabla',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 anulado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tablaPVU`
--

CREATE TABLE `tablaPVU` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idMaestraProcesador` int(11) NOT NULL DEFAULT '8',
  `idProcesador` int(11) NOT NULL,
  `idMaestraModelo` int(11) DEFAULT '9',
  `idModeloServidor` int(11) DEFAULT NULL,
  `pvu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_conversion`
--

CREATE TABLE `tabla_conversion` (
  `id` int(11) UNSIGNED NOT NULL,
  `codigo` varchar(500) NOT NULL,
  `os` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_equivalencia`
--

CREATE TABLE `tabla_equivalencia` (
  `id` int(11) UNSIGNED NOT NULL,
  `fabricante` int(11) NOT NULL,
  `familia` int(11) NOT NULL,
  `edicion` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `precio` float(10,2) UNSIGNED NOT NULL,
  `fechaActualizacion` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_equivalencia1`
--

CREATE TABLE `tabla_equivalencia1` (
  `id` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `precio` float(10,2) UNSIGNED NOT NULL,
  `familia1` int(11) DEFAULT NULL,
  `edicion1` int(11) DEFAULT NULL,
  `version1` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_equivalenciaOriginal`
--

CREATE TABLE `tabla_equivalenciaOriginal` (
  `id` int(11) UNSIGNED NOT NULL,
  `familia` varchar(250) NOT NULL,
  `edicion` varchar(250) NOT NULL,
  `version` varchar(250) NOT NULL,
  `precio` float(10,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `task_type_id` int(10) UNSIGNED DEFAULT NULL,
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `progress` double(8,2) DEFAULT '0.00',
  `start_date` datetime NOT NULL,
  `parent` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temas`
--

CREATE TABLE `temas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_area` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_t` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_id` int(10) UNSIGNED NOT NULL,
  `msj` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archivo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archivo2` text COLLATE utf8mb4_unicode_ci,
  `archivo3` text COLLATE utf8mb4_unicode_ci,
  `gerente_crm_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `codigo` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cierre_t` datetime DEFAULT NULL,
  `cliente_id` int(11) NOT NULL,
  `tema` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trasladoCabLicencias`
--

CREATE TABLE `trasladoCabLicencias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` varchar(70) NOT NULL,
  `observacion` varchar(250) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 por aprobar, 0 eliminado, 2 aprobado '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trasladoDetLicencias`
--

CREATE TABLE `trasladoDetLicencias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idTrasladoLicencia` bigint(20) UNSIGNED NOT NULL,
  `idDetalleContrato` int(11) NOT NULL,
  `disponible` int(11) NOT NULL,
  `cantTraslado` int(11) NOT NULL,
  `asignacion` varchar(70) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trueUp`
--

CREATE TABLE `trueUp` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(70) NOT NULL,
  `ruta` varchar(70) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trueUpDetalle`
--

CREATE TABLE `trueUpDetalle` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idTrueUp` bigint(20) UNSIGNED NOT NULL,
  `producto` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(20) NOT NULL,
  `precioEstimado` double(15,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` double(15,2) NOT NULL,
  `cantidadPropuesta` int(11) DEFAULT NULL,
  `totalPropuesto` double(15,2) DEFAULT NULL,
  `comentarios` varchar(70) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usabilidadMicrosoft`
--

CREATE TABLE `usabilidadMicrosoft` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'identificador del registro',
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `equipo` varchar(100) NOT NULL,
  `proceso` varchar(100) NOT NULL,
  `PID` varchar(100) NOT NULL,
  `creado` date NOT NULL,
  `horaInicio` time NOT NULL,
  `finalizado` date NOT NULL,
  `horaFin` time NOT NULL,
  `duracion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usabilidadUsuariosSap`
--

CREATE TABLE `usabilidadUsuariosSap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `usuarios` varchar(250) NOT NULL,
  `fechaCreacion` varchar(20) NOT NULL,
  `fechaBaja` varchar(20) DEFAULT NULL,
  `alta` int(11) NOT NULL,
  `baja` int(11) NOT NULL,
  `diasUltEntrada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usabilidadUsuariosSapSam`
--

CREATE TABLE `usabilidadUsuariosSapSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `usuarios` varchar(250) NOT NULL,
  `fechaCreacion` varchar(20) NOT NULL,
  `fechaBaja` varchar(20) DEFAULT NULL,
  `alta` int(11) NOT NULL,
  `baja` int(11) NOT NULL,
  `diasUltEntrada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles_id` int(10) UNSIGNED NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `avatar` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `last_login` date DEFAULT NULL,
  `today_login` date DEFAULT NULL,
  `notify_email` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioEquipoAdobe`
--

CREATE TABLE `usuarioEquipoAdobe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(70) NOT NULL,
  `host_name` varchar(70) NOT NULL,
  `dominio` varchar(70) NOT NULL,
  `usuario` varchar(70) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `centroCosto` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioEquipoIbm`
--

CREATE TABLE `usuarioEquipoIbm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(70) NOT NULL,
  `host_name` varchar(70) NOT NULL,
  `dominio` varchar(70) NOT NULL,
  `usuario` varchar(70) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `centroCosto` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioEquipoMicrosoft`
--

CREATE TABLE `usuarioEquipoMicrosoft` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `dato_control` varchar(70) NOT NULL,
  `host_name` varchar(70) NOT NULL,
  `dominio` varchar(70) NOT NULL,
  `usuario` varchar(70) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `centroCosto` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` text NOT NULL,
  `contrasena` text NOT NULL,
  `nombre` text NOT NULL,
  `apellido` text NOT NULL,
  `email` text NOT NULL,
  `tipo` int(10) UNSIGNED NOT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `fecha_registro` date NOT NULL DEFAULT '0000-00-00',
  `usuario_registro` int(10) UNSIGNED NOT NULL,
  `fecha_actualizacion` date NOT NULL DEFAULT '0000-00-00',
  `usuario_actualizacion` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosModuloSap`
--

CREATE TABLE `usuariosModuloSap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `usuarios` varchar(250) NOT NULL,
  `componente` varchar(250) NOT NULL,
  `nombreComponente` varchar(250) NOT NULL,
  `diasUltAcceso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `versiones`
--

CREATE TABLE `versiones` (
  `id` int(11) NOT NULL COMMENT 'identificador de la version',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'nombre de la version',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 anulado, 1 activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `id` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `modulo` varchar(30) NOT NULL DEFAULT 'Home',
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `VMsDiagnostic`
--

CREATE TABLE `VMsDiagnostic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idDiagnostic` bigint(20) UNSIGNED NOT NULL,
  `VM` varchar(70) DEFAULT NULL,
  `Host` varchar(70) NOT NULL,
  `Cluster` varchar(70) DEFAULT NULL,
  `Datacenter` varchar(70) NOT NULL,
  `OS` varchar(70) DEFAULT NULL,
  `BIOSDate` varchar(30) DEFAULT NULL,
  `StatusVM` varchar(20) NOT NULL,
  `DNSName` varchar(70) DEFAULT NULL,
  `PowerOn` varchar(30) DEFAULT NULL,
  `VMID` varchar(20) NOT NULL,
  `VMUUID` varchar(70) NOT NULL,
  `vCenterUUID` varchar(70) NOT NULL,
  `vCores` int(11) NOT NULL,
  `Sockets` int(11) NOT NULL,
  `CoresPerSocket` int(11) NOT NULL,
  `CreationTime` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `windowServerClientes`
--

CREATE TABLE `windowServerClientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `MSDN` enum('Si','No') NOT NULL DEFAULT 'No',
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `centroCosto` varchar(70) DEFAULT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `windowServerClientesSam`
--

CREATE TABLE `windowServerClientesSam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `MSDN` enum('Si','No') NOT NULL DEFAULT 'No',
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `centroCosto` int(70) DEFAULT NULL,
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `windowServerClientesSPLASam`
--

CREATE TABLE `windowServerClientesSPLASam` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archivo` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) NOT NULL,
  `host` varchar(70) NOT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `windowServerClientes_SPLA`
--

CREATE TABLE `windowServerClientes_SPLA` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `empleado` int(11) UNSIGNED NOT NULL,
  `cluster` varchar(70) DEFAULT NULL,
  `host` varchar(70) DEFAULT NULL,
  `equipo` varchar(70) NOT NULL,
  `familia` varchar(70) NOT NULL,
  `edicion` varchar(70) NOT NULL,
  `version` varchar(70) NOT NULL,
  `tipo` enum('Fisico','Virtual') NOT NULL DEFAULT 'Fisico',
  `cpu` int(11) NOT NULL,
  `cores` int(11) NOT NULL,
  `licSrv` int(11) NOT NULL,
  `licProc` int(11) NOT NULL,
  `licCore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `_tabtemporalcontratos`
--

CREATE TABLE `_tabtemporalcontratos` (
  `id` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `empleado` int(11) NOT NULL,
  `dato_control` varchar(250) DEFAULT NULL,
  `host_name` varchar(250) DEFAULT NULL,
  `registro` text,
  `editor` varchar(250) DEFAULT NULL,
  `version` varchar(250) DEFAULT NULL,
  `fecha_instalacion` int(11) DEFAULT NULL,
  `sofware` varchar(200) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activity_links`
--
ALTER TABLE `activity_links`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `activity_tasks`
--
ALTER TABLE `activity_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `agente`
--
ALTER TABLE `agente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `agente_add_remove`
--
ALTER TABLE `agente_add_remove`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_add_remove_aux`
--
ALTER TABLE `agente_add_remove_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_archivo`
--
ALTER TABLE `agente_archivo`
  ADD PRIMARY KEY (`id_archvo`),
  ADD UNIQUE KEY `id_archvo` (`id_archvo`);

--
-- Indices de la tabla `agente_archivo_campo`
--
ALTER TABLE `agente_archivo_campo`
  ADD PRIMARY KEY (`id_archvo_campo`),
  ADD UNIQUE KEY `id_archvo_campo` (`id_archvo_campo`);

--
-- Indices de la tabla `agente_autodesk`
--
ALTER TABLE `agente_autodesk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_autodesk_aux`
--
ALTER TABLE `agente_autodesk_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_config`
--
ALTER TABLE `agente_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `agente_desinstalacion`
--
ALTER TABLE `agente_desinstalacion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_desinstalacion_aux`
--
ALTER TABLE `agente_desinstalacion_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_hist`
--
ALTER TABLE `agente_hist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_llave`
--
ALTER TABLE `agente_llave`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_llave_aux`
--
ALTER TABLE `agente_llave_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_memoria`
--
ALTER TABLE `agente_memoria`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_memoria_aux`
--
ALTER TABLE `agente_memoria_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_metering_errors`
--
ALTER TABLE `agente_metering_errors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_metering_errors_aux`
--
ALTER TABLE `agente_metering_errors_aux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_metering_results`
--
ALTER TABLE `agente_metering_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_metering_results_aux`
--
ALTER TABLE `agente_metering_results_aux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_procesador`
--
ALTER TABLE `agente_procesador`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_procesador_aux`
--
ALTER TABLE `agente_procesador_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_proceso`
--
ALTER TABLE `agente_proceso`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_proceso_aux`
--
ALTER TABLE `agente_proceso_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_resultado_escaneo`
--
ALTER TABLE `agente_resultado_escaneo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_resultado_escaneo_aux`
--
ALTER TABLE `agente_resultado_escaneo_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_scheduling`
--
ALTER TABLE `agente_scheduling`
  ADD PRIMARY KEY (`id_scheduling`,`id_agente`),
  ADD KEY `id_scheduling` (`id_scheduling`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_seguridad`
--
ALTER TABLE `agente_seguridad`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_seguridad_aux`
--
ALTER TABLE `agente_seguridad_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_serial_maquina`
--
ALTER TABLE `agente_serial_maquina`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_serial_maquina_aux`
--
ALTER TABLE `agente_serial_maquina_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_servicio`
--
ALTER TABLE `agente_servicio`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_servicio_aux`
--
ALTER TABLE `agente_servicio_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_sistema_operativo`
--
ALTER TABLE `agente_sistema_operativo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_sistema_operativo_aux`
--
ALTER TABLE `agente_sistema_operativo_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_sql_data`
--
ALTER TABLE `agente_sql_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_sql_data_aux`
--
ALTER TABLE `agente_sql_data_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_tipo_equipo`
--
ALTER TABLE `agente_tipo_equipo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_tipo_equipo_aux`
--
ALTER TABLE `agente_tipo_equipo_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_usabilidad_software`
--
ALTER TABLE `agente_usabilidad_software`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_usabilidad_software_aux`
--
ALTER TABLE `agente_usabilidad_software_aux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_usuario_equipo`
--
ALTER TABLE `agente_usuario_equipo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `agente_usuario_equipo_aux`
--
ALTER TABLE `agente_usuario_equipo_aux`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_agente` (`id_agente`);

--
-- Indices de la tabla `alineacionSqlServidores`
--
ALTER TABLE `alineacionSqlServidores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`) USING BTREE;

--
-- Indices de la tabla `alineacionSqlServidoresSam`
--
ALTER TABLE `alineacionSqlServidoresSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `alineacionSqlServidoresSPLA`
--
ALTER TABLE `alineacionSqlServidoresSPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `alineacionSqlServidoresSPLASam`
--
ALTER TABLE `alineacionSqlServidoresSPLASam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `empleado_fk` (`id`),
  ADD KEY `empleado` (`empleado`);

--
-- Indices de la tabla `alineacionWindowsServidores`
--
ALTER TABLE `alineacionWindowsServidores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `alineacionWindowsServidoresSam`
--
ALTER TABLE `alineacionWindowsServidoresSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `empleado_fk` (`id`),
  ADD KEY `empleado` (`empleado`);

--
-- Indices de la tabla `alineacionWindowsServidoresSPLA`
--
ALTER TABLE `alineacionWindowsServidoresSPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`) USING BTREE;

--
-- Indices de la tabla `alineacionWindowsServidoresSPLASam`
--
ALTER TABLE `alineacionWindowsServidoresSPLASam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`),
  ADD KEY `archivo_fk` (`archivo`) USING BTREE;

--
-- Indices de la tabla `appConsolidado_offices`
--
ALTER TABLE `appConsolidado_offices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCorreo` (`idCorreo`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `serialHDD` (`serialHDD`);

--
-- Indices de la tabla `appDetalles_equipo`
--
ALTER TABLE `appDetalles_equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equipo` (`equipo`),
  ADD KEY `idCorreo` (`idCorreo`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `serialHDD` (`serialHDD`);

--
-- Indices de la tabla `appEscaneo`
--
ALTER TABLE `appEscaneo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `idCorreo` (`idCorreo`),
  ADD KEY `serialHDD` (`serialHDD`);

--
-- Indices de la tabla `appEscaneoCorreo`
--
ALTER TABLE `appEscaneoCorreo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `clientes_nadd_id_fk` (`clientes_nadd_id`);

--
-- Indices de la tabla `appFilepcs`
--
ALTER TABLE `appFilepcs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cn` (`cn`),
  ADD KEY `idCorreo` (`idCorreo`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `serialHDD` (`serialHDD`);

--
-- Indices de la tabla `appResumen_office`
--
ALTER TABLE `appResumen_office`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCorreo` (`idCorreo`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `serialHDD` (`serialHDD`);

--
-- Indices de la tabla `archivosFabricantes`
--
ALTER TABLE `archivosFabricantes`
  ADD PRIMARY KEY (`cliente`,`fabricante`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `archivosIncrementoFabricantes`
--
ALTER TABLE `archivosIncrementoFabricantes`
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado` (`empleado`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `asigCliente`
--
ALTER TABLE `asigCliente`
  ADD PRIMARY KEY (`idAsignacion`,`cliente`),
  ADD KEY `cliente_fk` (`cliente`);

--
-- Indices de la tabla `asigClienteEmple`
--
ALTER TABLE `asigClienteEmple`
  ADD PRIMARY KEY (`empleado`,`cliente`,`idAsignacion`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `asignacion_fk` (`idAsignacion`),
  ADD KEY `empleado_fk` (`empleado`) USING BTREE;

--
-- Indices de la tabla `asignacion`
--
ALTER TABLE `asignacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `balance_adobe`
--
ALTER TABLE `balance_adobe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_adobeSam`
--
ALTER TABLE `balance_adobeSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_ibm`
--
ALTER TABLE `balance_ibm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_ibmSam`
--
ALTER TABLE `balance_ibmSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_office`
--
ALTER TABLE `balance_office`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `balance_office2`
--
ALTER TABLE `balance_office2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_office2Sam`
--
ALTER TABLE `balance_office2Sam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_oracle`
--
ALTER TABLE `balance_oracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_oracleSam`
--
ALTER TABLE `balance_oracleSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_Sap`
--
ALTER TABLE `balance_Sap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_SapSam`
--
ALTER TABLE `balance_SapSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_SPLA`
--
ALTER TABLE `balance_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_SPLASam`
--
ALTER TABLE `balance_SPLASam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_unixIBM`
--
ALTER TABLE `balance_unixIBM`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_UnixIBMSam`
--
ALTER TABLE `balance_UnixIBMSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_unixOracle`
--
ALTER TABLE `balance_unixOracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_UnixOracleSam`
--
ALTER TABLE `balance_UnixOracleSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_VMWare`
--
ALTER TABLE `balance_VMWare`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `balance_VMWareSam`
--
ALTER TABLE `balance_VMWareSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centroCosto`
--
ALTER TABLE `centroCosto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado` (`empleado`),
  ADD KEY `registro` (`cliente`,`empleado`,`equipo`,`usuario`);

--
-- Indices de la tabla `certificaciones`
--
ALTER TABLE `certificaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes_dominios`
--
ALTER TABLE `clientes_dominios`
  ADD PRIMARY KEY (`id`,`cliente`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `cliente_dominio_unico` (`cliente`,`dominio`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `clientes_nadd`
--
ALTER TABLE `clientes_nadd`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes_user`
--
ALTER TABLE `clientes_user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `compras2`
--
ALTER TABLE `compras2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `compras_adobe`
--
ALTER TABLE `compras_adobe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `compras_ibm`
--
ALTER TABLE `compras_ibm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `compras_oracle`
--
ALTER TABLE `compras_oracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `compras_Sap`
--
ALTER TABLE `compras_Sap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `compras_SPLA`
--
ALTER TABLE `compras_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `compras_unixIBM`
--
ALTER TABLE `compras_unixIBM`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `compras_unixOracle`
--
ALTER TABLE `compras_unixOracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado`
--
ALTER TABLE `consolidado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidadoProcesadores`
--
ALTER TABLE `consolidadoProcesadores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidadoProcesadoresIBM`
--
ALTER TABLE `consolidadoProcesadoresIBM`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidadoProcesadoresIBMAux`
--
ALTER TABLE `consolidadoProcesadoresIBMAux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidadoProcesadoresMSCloud`
--
ALTER TABLE `consolidadoProcesadoresMSCloud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `consolidadoProcesadoresSAMDiagnostic`
--
ALTER TABLE `consolidadoProcesadoresSAMDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `consolidadoProcesadores_SPLA`
--
ALTER TABLE `consolidadoProcesadores_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidadoTipoEquipo`
--
ALTER TABLE `consolidadoTipoEquipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidadoTipoEquipoMSCloud`
--
ALTER TABLE `consolidadoTipoEquipoMSCloud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `consolidadoTipoEquipoSAMDiagnostic`
--
ALTER TABLE `consolidadoTipoEquipoSAMDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `consolidadoTipoEquipo_SPLA`
--
ALTER TABLE `consolidadoTipoEquipo_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_adobe`
--
ALTER TABLE `consolidado_adobe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_adobeAux`
--
ALTER TABLE `consolidado_adobeAux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_ibm`
--
ALTER TABLE `consolidado_ibm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_ibmAux`
--
ALTER TABLE `consolidado_ibmAux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_MSCloud`
--
ALTER TABLE `consolidado_MSCloud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `consolidado_offices`
--
ALTER TABLE `consolidado_offices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_officesAux`
--
ALTER TABLE `consolidado_officesAux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_oracle`
--
ALTER TABLE `consolidado_oracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_OracleAIX`
--
ALTER TABLE `consolidado_OracleAIX`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_OracleLinux`
--
ALTER TABLE `consolidado_OracleLinux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_OracleSolaris`
--
ALTER TABLE `consolidado_OracleSolaris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_others`
--
ALTER TABLE `consolidado_others`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_SAMDiagnostic`
--
ALTER TABLE `consolidado_SAMDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `consolidado_server`
--
ALTER TABLE `consolidado_server`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_SPLA`
--
ALTER TABLE `consolidado_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_SPLAAux`
--
ALTER TABLE `consolidado_SPLAAux`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `consolidado_UnixAIX`
--
ALTER TABLE `consolidado_UnixAIX`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_UnixLinux`
--
ALTER TABLE `consolidado_UnixLinux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `consolidado_UnixSolaris`
--
ALTER TABLE `consolidado_UnixSolaris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`idContrato`),
  ADD KEY `idFabricante` (`idFabricante`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `contratosVMWare`
--
ALTER TABLE `contratosVMWare`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `cotizacionesLicensing`
--
ALTER TABLE `cotizacionesLicensing`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `cotizacionProveedor`
--
ALTER TABLE `cotizacionProveedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `fabricante` (`fabricante`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`cliente`,`numero`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `desarrolloPruebaMSDN`
--
ALTER TABLE `desarrolloPruebaMSDN`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDesarrolloPruebaVS` (`idDesarrolloPruebaVS`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `desarrolloPruebaMSDNSam`
--
ALTER TABLE `desarrolloPruebaMSDNSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `desarrolloPruebaSQL`
--
ALTER TABLE `desarrolloPruebaSQL`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `empleado_fk` (`empleado`),
  ADD KEY `cliente_fk` (`cliente`);

--
-- Indices de la tabla `desarrolloPruebaVS`
--
ALTER TABLE `desarrolloPruebaVS`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `desarrolloPruebaVSSam`
--
ALTER TABLE `desarrolloPruebaVSSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `desarrolloPruebaWindows`
--
ALTER TABLE `desarrolloPruebaWindows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalleContrato`
--
ALTER TABLE `detalleContrato`
  ADD PRIMARY KEY (`idDetalleContrato`),
  ADD KEY `detalleContrato_i` (`idContrato`);

--
-- Indices de la tabla `detalleContrato_VMWare`
--
ALTER TABLE `detalleContrato_VMWare`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalleContrato_VMWareSam`
--
ALTER TABLE `detalleContrato_VMWareSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalleCotProveedor`
--
ALTER TABLE `detalleCotProveedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCotizacion` (`idCotizacion`);

--
-- Indices de la tabla `detalleEvento`
--
ALTER TABLE `detalleEvento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `evento` (`evento`);

--
-- Indices de la tabla `detalleMaestra`
--
ALTER TABLE `detalleMaestra`
  ADD PRIMARY KEY (`idDetalle`,`idMaestra`),
  ADD KEY `idMaestra` (`idMaestra`);

--
-- Indices de la tabla `detalleServidorSap`
--
ALTER TABLE `detalleServidorSap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo`
--
ALTER TABLE `detalles_equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo2`
--
ALTER TABLE `detalles_equipo2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado` (`empleado`),
  ADD KEY `equipo` (`equipo`) USING BTREE;

--
-- Indices de la tabla `detalles_equipo2Sam`
--
ALTER TABLE `detalles_equipo2Sam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipoDesuso`
--
ALTER TABLE `detalles_equipoDesuso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPrueba` (`idPrueba`);

--
-- Indices de la tabla `detalles_equipoMSCloud`
--
ALTER TABLE `detalles_equipoMSCloud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `detalles_equipoSAMDiagnostic`
--
ALTER TABLE `detalles_equipoSAMDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `detalles_equipo_adobe`
--
ALTER TABLE `detalles_equipo_adobe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_adobeSam`
--
ALTER TABLE `detalles_equipo_adobeSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_ibm`
--
ALTER TABLE `detalles_equipo_ibm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_ibmSam`
--
ALTER TABLE `detalles_equipo_ibmSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_oracle`
--
ALTER TABLE `detalles_equipo_oracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_OracleAIX`
--
ALTER TABLE `detalles_equipo_OracleAIX`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_OracleAIXSam`
--
ALTER TABLE `detalles_equipo_OracleAIXSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_OracleLinux`
--
ALTER TABLE `detalles_equipo_OracleLinux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_OracleLinuxSam`
--
ALTER TABLE `detalles_equipo_OracleLinuxSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_oracleSam`
--
ALTER TABLE `detalles_equipo_oracleSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_OracleSolaris`
--
ALTER TABLE `detalles_equipo_OracleSolaris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_OracleSolarisSam`
--
ALTER TABLE `detalles_equipo_OracleSolarisSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_SPLA`
--
ALTER TABLE `detalles_equipo_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_SPLASam`
--
ALTER TABLE `detalles_equipo_SPLASam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_UnixAIX`
--
ALTER TABLE `detalles_equipo_UnixAIX`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_UnixAIXSam`
--
ALTER TABLE `detalles_equipo_UnixAIXSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_UnixLinux`
--
ALTER TABLE `detalles_equipo_UnixLinux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_UnixLinuxSam`
--
ALTER TABLE `detalles_equipo_UnixLinuxSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_UnixSolaris`
--
ALTER TABLE `detalles_equipo_UnixSolaris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `detalles_equipo_UnixSolarisSam`
--
ALTER TABLE `detalles_equipo_UnixSolarisSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `ediciones`
--
ALTER TABLE `ediciones`
  ADD PRIMARY KEY (`idEdicion`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `edicproducto`
--
ALTER TABLE `edicproducto`
  ADD PRIMARY KEY (`idFabricante`,`idProducto`,`idEdicion`),
  ADD KEY `idFabricante` (`idFabricante`),
  ADD KEY `idProducto` (`idProducto`),
  ADD KEY `idEdicion` (`idEdicion`);

--
-- Indices de la tabla `elimEquiposLAEAdobe`
--
ALTER TABLE `elimEquiposLAEAdobe`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `elimEquiposLAEIbm`
--
ALTER TABLE `elimEquiposLAEIbm`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `elimEquiposLAEMicrosoft`
--
ALTER TABLE `elimEquiposLAEMicrosoft`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `encArchAdobeSam`
--
ALTER TABLE `encArchAdobeSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchIbmSam`
--
ALTER TABLE `encArchIbmSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchOracleSam`
--
ALTER TABLE `encArchOracleSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchSam`
--
ALTER TABLE `encArchSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchSapSam`
--
ALTER TABLE `encArchSapSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchSPLASam`
--
ALTER TABLE `encArchSPLASam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchUnixIBMSam`
--
ALTER TABLE `encArchUnixIBMSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchUnixOracleSam`
--
ALTER TABLE `encArchUnixOracleSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encArchVMWareSam`
--
ALTER TABLE `encArchVMWareSam`
  ADD PRIMARY KEY (`cliente`),
  ADD UNIQUE KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encGrafAdobeSam`
--
ALTER TABLE `encGrafAdobeSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encGrafIbmSam`
--
ALTER TABLE `encGrafIbmSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encGrafOracleSam`
--
ALTER TABLE `encGrafOracleSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encGrafSam`
--
ALTER TABLE `encGrafSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encGrafSapSam`
--
ALTER TABLE `encGrafSapSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`) USING BTREE;

--
-- Indices de la tabla `encGrafSPLASam`
--
ALTER TABLE `encGrafSPLASam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encGrafUnixIBMSam`
--
ALTER TABLE `encGrafUnixIBMSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`) USING BTREE;

--
-- Indices de la tabla `encGrafUnixOracleSam`
--
ALTER TABLE `encGrafUnixOracleSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `encGrafVMWareSam`
--
ALTER TABLE `encGrafVMWareSam`
  ADD PRIMARY KEY (`archivo`,`cliente`),
  ADD UNIQUE KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_equipos`
--
ALTER TABLE `escaneo_equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_equipos2`
--
ALTER TABLE `escaneo_equipos2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_equipos2Sam`
--
ALTER TABLE `escaneo_equipos2Sam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_equiposDesuso`
--
ALTER TABLE `escaneo_equiposDesuso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPrueba` (`idPrueba`);

--
-- Indices de la tabla `escaneo_equiposMSCloud`
--
ALTER TABLE `escaneo_equiposMSCloud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `escaneo_equiposSAMDiagnostic`
--
ALTER TABLE `escaneo_equiposSAMDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `escaneo_equipos_adobe`
--
ALTER TABLE `escaneo_equipos_adobe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_equipos_ibm`
--
ALTER TABLE `escaneo_equipos_ibm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_equipos_oracle`
--
ALTER TABLE `escaneo_equipos_oracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_equipos_SPLA`
--
ALTER TABLE `escaneo_equipos_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `escaneo_troubleshoot`
--
ALTER TABLE `escaneo_troubleshoot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `fabricantes`
--
ALTER TABLE `fabricantes`
  ADD PRIMARY KEY (`idFabricante`);

--
-- Indices de la tabla `fabricantesCliente`
--
ALTER TABLE `fabricantesCliente`
  ADD PRIMARY KEY (`idFabricante`,`cliente`);

--
-- Indices de la tabla `filepcs`
--
ALTER TABLE `filepcs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `filepcs2`
--
ALTER TABLE `filepcs2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`),
  ADD KEY `equipo` (`cn`);

--
-- Indices de la tabla `filepcsAux`
--
ALTER TABLE `filepcsAux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`id`),
  ADD KEY `empleado_fk` (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado` (`empleado`);

--
-- Indices de la tabla `filepcsDesuso`
--
ALTER TABLE `filepcsDesuso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPrueba` (`idPrueba`);

--
-- Indices de la tabla `filepcsMSCloud`
--
ALTER TABLE `filepcsMSCloud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `filepcsSAMDiagnostic`
--
ALTER TABLE `filepcsSAMDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`),
  ADD KEY `cn` (`cn`);

--
-- Indices de la tabla `filepcs_adobe`
--
ALTER TABLE `filepcs_adobe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `filepcs_adobeAux`
--
ALTER TABLE `filepcs_adobeAux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `filepcs_ibm`
--
ALTER TABLE `filepcs_ibm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `filepcs_ibmAux`
--
ALTER TABLE `filepcs_ibmAux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `filepcs_oracle`
--
ALTER TABLE `filepcs_oracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `filepcs_SPLA`
--
ALTER TABLE `filepcs_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `gantt`
--
ALTER TABLE `gantt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`);

--
-- Indices de la tabla `ganttDetalle`
--
ALTER TABLE `ganttDetalle`
  ADD KEY `idGantt` (`idGantt`);

--
-- Indices de la tabla `historicoAppsSPLA`
--
ALTER TABLE `historicoAppsSPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idHistorico` (`idHistorico`);

--
-- Indices de la tabla `historicoCurrentSPLA`
--
ALTER TABLE `historicoCurrentSPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idHistorico` (`idHistorico`);

--
-- Indices de la tabla `historicoCustomerSPLA`
--
ALTER TABLE `historicoCustomerSPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idHistorico` (`idHistorico`);

--
-- Indices de la tabla `historicoSPLA`
--
ALTER TABLE `historicoSPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `historicoVMSPLA`
--
ALTER TABLE `historicoVMSPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idHistorico` (`idHistorico`);

--
-- Indices de la tabla `hostsDiagnostic`
--
ALTER TABLE `hostsDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `lau_users`
--
ALTER TABLE `lau_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleado` (`empleado`),
  ADD KEY `cn` (`cn`);

--
-- Indices de la tabla `licenciasCentralizador`
--
ALTER TABLE `licenciasCentralizador`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `licenciasESXiVCenter`
--
ALTER TABLE `licenciasESXiVCenter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lista_officce`
--
ALTER TABLE `lista_officce`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `logWebtool`
--
ALTER TABLE `logWebtool`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idModulo_fk` (`idModulo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `idOperacion_fk` (`idOperacion`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `moduloRecordatorio`
--
ALTER TABLE `moduloRecordatorio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `MSCloud`
--
ALTER TABLE `MSCloud`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nivelServicio`
--
ALTER TABLE `nivelServicio`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagosFabricantes`
--
ALTER TABLE `pagosFabricantes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagosFabricantesDetalle`
--
ALTER TABLE `pagosFabricantesDetalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(250));

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisoSam`
--
ALTER TABLE `permisoSam`
  ADD PRIMARY KEY (`idEmpleado`,`idPermiso`),
  ADD KEY `empleado` (`idEmpleado`),
  ADD KEY `permiso` (`idPermiso`);

--
-- Indices de la tabla `permisosGenerales`
--
ALTER TABLE `permisosGenerales`
  ADD PRIMARY KEY (`idPermisos`),
  ADD UNIQUE KEY `idPermisos` (`idPermisos`);

--
-- Indices de la tabla `phases`
--
ALTER TABLE `phases`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `phasesfactories`
--
ALTER TABLE `phasesfactories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `phase_state`
--
ALTER TABLE `phase_state`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `phase_type`
--
ALTER TABLE `phase_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `poc`
--
ALTER TABLE `poc`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pocSAP`
--
ALTER TABLE `pocSAP`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `politSam`
--
ALTER TABLE `politSam`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `portal`
--
ALTER TABLE `portal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prodFabricante`
--
ALTER TABLE `prodFabricante`
  ADD PRIMARY KEY (`idProducto`,`idFabricante`),
  ADD KEY `idProducto` (`idProducto`),
  ADD KEY `idFabricante` (`idFabricante`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pruebaDesuso`
--
ALTER TABLE `pruebaDesuso`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `pvuCliente`
--
ALTER TABLE `pvuCliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `pvuClienteAIX`
--
ALTER TABLE `pvuClienteAIX`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pvuClienteLinux`
--
ALTER TABLE `pvuClienteLinux`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pvuClienteSolaris`
--
ALTER TABLE `pvuClienteSolaris`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recordatorios`
--
ALTER TABLE `recordatorios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modulo` (`modulo`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `reportarFallas`
--
ALTER TABLE `reportarFallas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `respCotizProv`
--
ALTER TABLE `respCotizProv`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `idCotizacionProv` (`idCotizacionProv`);

--
-- Indices de la tabla `resumenServidorSap`
--
ALTER TABLE `resumenServidorSap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumenServidorSapSam`
--
ALTER TABLE `resumenServidorSapSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumenUsuariosModuloSap`
--
ALTER TABLE `resumenUsuariosModuloSap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumenUsuariosModuloSapSam`
--
ALTER TABLE `resumenUsuariosModuloSapSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_adobe`
--
ALTER TABLE `resumen_adobe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_adobeSam`
--
ALTER TABLE `resumen_adobeSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_ibm`
--
ALTER TABLE `resumen_ibm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_ibmSam`
--
ALTER TABLE `resumen_ibmSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_MSCloud`
--
ALTER TABLE `resumen_MSCloud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `resumen_office`
--
ALTER TABLE `resumen_office`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_office2`
--
ALTER TABLE `resumen_office2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado` (`empleado`),
  ADD KEY `familia` (`familia`),
  ADD KEY `edicion` (`edicion`),
  ADD KEY `version` (`version`),
  ADD KEY `equipo` (`equipo`);

--
-- Indices de la tabla `resumen_office2Sam`
--
ALTER TABLE `resumen_office2Sam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_oracle`
--
ALTER TABLE `resumen_oracle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_OracleAIX`
--
ALTER TABLE `resumen_OracleAIX`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_OracleAIXSam`
--
ALTER TABLE `resumen_OracleAIXSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE;

--
-- Indices de la tabla `resumen_OracleLinux`
--
ALTER TABLE `resumen_OracleLinux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_OracleLinuxSam`
--
ALTER TABLE `resumen_OracleLinuxSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_oracleSam`
--
ALTER TABLE `resumen_oracleSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_OracleSolaris`
--
ALTER TABLE `resumen_OracleSolaris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_OracleSolarisSam`
--
ALTER TABLE `resumen_OracleSolarisSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_others`
--
ALTER TABLE `resumen_others`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_SAMDiagnostic`
--
ALTER TABLE `resumen_SAMDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `resumen_server`
--
ALTER TABLE `resumen_server`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_SPLA`
--
ALTER TABLE `resumen_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_SPLASam`
--
ALTER TABLE `resumen_SPLASam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_UnixAIX`
--
ALTER TABLE `resumen_UnixAIX`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_UnixAIXSam`
--
ALTER TABLE `resumen_UnixAIXSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_UnixLinux`
--
ALTER TABLE `resumen_UnixLinux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_UnixLinuxSam`
--
ALTER TABLE `resumen_UnixLinuxSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_UnixSolaris`
--
ALTER TABLE `resumen_UnixSolaris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_UnixSolarisSam`
--
ALTER TABLE `resumen_UnixSolarisSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_VMWare`
--
ALTER TABLE `resumen_VMWare`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `resumen_VMWareSam`
--
ALTER TABLE `resumen_VMWareSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `cliente_fk` (`cliente`) USING BTREE,
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles_user`
--
ALTER TABLE `roles_user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `SAMDiagnostic`
--
ALTER TABLE `SAMDiagnostic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `scheduling`
--
ALTER TABLE `scheduling`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `service_query`
--
ALTER TABLE `service_query`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `service_serial`
--
ALTER TABLE `service_serial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sheet1`
--
ALTER TABLE `sheet1`
  ADD PRIMARY KEY (`numero`);

--
-- Indices de la tabla `SPLACliente`
--
ALTER TABLE `SPLACliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `SPLADiagnostic`
--
ALTER TABLE `SPLADiagnostic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `SPLASKU`
--
ALTER TABLE `SPLASKU`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `SPLAVirtualMachine`
--
ALTER TABLE `SPLAVirtualMachine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `sqlServerClientes`
--
ALTER TABLE `sqlServerClientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `sqlServerClientesSam`
--
ALTER TABLE `sqlServerClientesSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `sqlServerClientesSPLA_Sam`
--
ALTER TABLE `sqlServerClientesSPLA_Sam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `sqlServerClientes_SPLA`
--
ALTER TABLE `sqlServerClientes_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `submodulos`
--
ALTER TABLE `submodulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tablaMaestra`
--
ALTER TABLE `tablaMaestra`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tablaPVU`
--
ALTER TABLE `tablaPVU`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idMaestraProcesador` (`idProcesador`),
  ADD KEY `idModeloServidor` (`idModeloServidor`),
  ADD KEY `idMaestraProcesador_2` (`idMaestraProcesador`,`idProcesador`),
  ADD KEY `idMaestraModelo` (`idMaestraModelo`,`idModeloServidor`);

--
-- Indices de la tabla `tabla_conversion`
--
ALTER TABLE `tabla_conversion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tabla_equivalencia`
--
ALTER TABLE `tabla_equivalencia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tabla_equivalencia1`
--
ALTER TABLE `tabla_equivalencia1`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tabla_equivalenciaOriginal`
--
ALTER TABLE `tabla_equivalenciaOriginal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temas`
--
ALTER TABLE `temas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trasladoCabLicencias`
--
ALTER TABLE `trasladoCabLicencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trasladoDetLicencias`
--
ALTER TABLE `trasladoDetLicencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idTraslado` (`idTrasladoLicencia`),
  ADD KEY `idDetalleContrato` (`idDetalleContrato`),
  ADD KEY `asignacion` (`asignacion`);

--
-- Indices de la tabla `trueUp`
--
ALTER TABLE `trueUp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`);

--
-- Indices de la tabla `trueUpDetalle`
--
ALTER TABLE `trueUpDetalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idTrueUp` (`idTrueUp`);

--
-- Indices de la tabla `usabilidadMicrosoft`
--
ALTER TABLE `usabilidadMicrosoft`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `usabilidadUsuariosSap`
--
ALTER TABLE `usabilidadUsuariosSap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `usabilidadUsuariosSapSam`
--
ALTER TABLE `usabilidadUsuariosSapSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `archivo` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarioEquipoAdobe`
--
ALTER TABLE `usuarioEquipoAdobe`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarioEquipoIbm`
--
ALTER TABLE `usuarioEquipoIbm`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarioEquipoMicrosoft`
--
ALTER TABLE `usuarioEquipoMicrosoft`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuariosModuloSap`
--
ALTER TABLE `usuariosModuloSap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `versiones`
--
ALTER TABLE `versiones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `VMsDiagnostic`
--
ALTER TABLE `VMsDiagnostic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDiagnostic` (`idDiagnostic`);

--
-- Indices de la tabla `windowServerClientes`
--
ALTER TABLE `windowServerClientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `windowServerClientesSam`
--
ALTER TABLE `windowServerClientesSam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `windowServerClientesSPLASam`
--
ALTER TABLE `windowServerClientesSPLASam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_fk` (`archivo`),
  ADD KEY `cliente_fk` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `windowServerClientes_SPLA`
--
ALTER TABLE `windowServerClientes_SPLA`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `empleado_fk` (`empleado`);

--
-- Indices de la tabla `_tabtemporalcontratos`
--
ALTER TABLE `_tabtemporalcontratos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activity_links`
--
ALTER TABLE `activity_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `activity_tasks`
--
ALTER TABLE `activity_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente`
--
ALTER TABLE `agente`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_add_remove`
--
ALTER TABLE `agente_add_remove`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_add_remove_aux`
--
ALTER TABLE `agente_add_remove_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_archivo`
--
ALTER TABLE `agente_archivo`
  MODIFY `id_archvo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_archivo_campo`
--
ALTER TABLE `agente_archivo_campo`
  MODIFY `id_archvo_campo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_autodesk`
--
ALTER TABLE `agente_autodesk`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_autodesk_aux`
--
ALTER TABLE `agente_autodesk_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_config`
--
ALTER TABLE `agente_config`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_desinstalacion`
--
ALTER TABLE `agente_desinstalacion`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_desinstalacion_aux`
--
ALTER TABLE `agente_desinstalacion_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_hist`
--
ALTER TABLE `agente_hist`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_llave`
--
ALTER TABLE `agente_llave`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_llave_aux`
--
ALTER TABLE `agente_llave_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_memoria`
--
ALTER TABLE `agente_memoria`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_memoria_aux`
--
ALTER TABLE `agente_memoria_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_metering_errors`
--
ALTER TABLE `agente_metering_errors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_metering_errors_aux`
--
ALTER TABLE `agente_metering_errors_aux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_metering_results`
--
ALTER TABLE `agente_metering_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_metering_results_aux`
--
ALTER TABLE `agente_metering_results_aux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_procesador`
--
ALTER TABLE `agente_procesador`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_procesador_aux`
--
ALTER TABLE `agente_procesador_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_proceso`
--
ALTER TABLE `agente_proceso`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_proceso_aux`
--
ALTER TABLE `agente_proceso_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_resultado_escaneo`
--
ALTER TABLE `agente_resultado_escaneo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_resultado_escaneo_aux`
--
ALTER TABLE `agente_resultado_escaneo_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_seguridad`
--
ALTER TABLE `agente_seguridad`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_seguridad_aux`
--
ALTER TABLE `agente_seguridad_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_serial_maquina`
--
ALTER TABLE `agente_serial_maquina`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_serial_maquina_aux`
--
ALTER TABLE `agente_serial_maquina_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_servicio`
--
ALTER TABLE `agente_servicio`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_servicio_aux`
--
ALTER TABLE `agente_servicio_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_sistema_operativo`
--
ALTER TABLE `agente_sistema_operativo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_sistema_operativo_aux`
--
ALTER TABLE `agente_sistema_operativo_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_sql_data`
--
ALTER TABLE `agente_sql_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_sql_data_aux`
--
ALTER TABLE `agente_sql_data_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_tipo_equipo`
--
ALTER TABLE `agente_tipo_equipo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_tipo_equipo_aux`
--
ALTER TABLE `agente_tipo_equipo_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_usabilidad_software`
--
ALTER TABLE `agente_usabilidad_software`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_usabilidad_software_aux`
--
ALTER TABLE `agente_usabilidad_software_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_usuario_equipo`
--
ALTER TABLE `agente_usuario_equipo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agente_usuario_equipo_aux`
--
ALTER TABLE `agente_usuario_equipo_aux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionSqlServidores`
--
ALTER TABLE `alineacionSqlServidores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionSqlServidoresSam`
--
ALTER TABLE `alineacionSqlServidoresSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionSqlServidoresSPLA`
--
ALTER TABLE `alineacionSqlServidoresSPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionSqlServidoresSPLASam`
--
ALTER TABLE `alineacionSqlServidoresSPLASam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionWindowsServidores`
--
ALTER TABLE `alineacionWindowsServidores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionWindowsServidoresSam`
--
ALTER TABLE `alineacionWindowsServidoresSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionWindowsServidoresSPLA`
--
ALTER TABLE `alineacionWindowsServidoresSPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alineacionWindowsServidoresSPLASam`
--
ALTER TABLE `alineacionWindowsServidoresSPLASam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appConsolidado_offices`
--
ALTER TABLE `appConsolidado_offices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appDetalles_equipo`
--
ALTER TABLE `appDetalles_equipo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appEscaneo`
--
ALTER TABLE `appEscaneo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appEscaneoCorreo`
--
ALTER TABLE `appEscaneoCorreo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appFilepcs`
--
ALTER TABLE `appFilepcs`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `appResumen_office`
--
ALTER TABLE `appResumen_office`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `asignacion`
--
ALTER TABLE `asignacion`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_adobe`
--
ALTER TABLE `balance_adobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_adobeSam`
--
ALTER TABLE `balance_adobeSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_ibm`
--
ALTER TABLE `balance_ibm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_ibmSam`
--
ALTER TABLE `balance_ibmSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_office`
--
ALTER TABLE `balance_office`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_office2`
--
ALTER TABLE `balance_office2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_office2Sam`
--
ALTER TABLE `balance_office2Sam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_oracle`
--
ALTER TABLE `balance_oracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_oracleSam`
--
ALTER TABLE `balance_oracleSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_Sap`
--
ALTER TABLE `balance_Sap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_SapSam`
--
ALTER TABLE `balance_SapSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_SPLA`
--
ALTER TABLE `balance_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_SPLASam`
--
ALTER TABLE `balance_SPLASam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_unixIBM`
--
ALTER TABLE `balance_unixIBM`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_UnixIBMSam`
--
ALTER TABLE `balance_UnixIBMSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_unixOracle`
--
ALTER TABLE `balance_unixOracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_UnixOracleSam`
--
ALTER TABLE `balance_UnixOracleSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_VMWare`
--
ALTER TABLE `balance_VMWare`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `balance_VMWareSam`
--
ALTER TABLE `balance_VMWareSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `centroCosto`
--
ALTER TABLE `centroCosto`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `certificaciones`
--
ALTER TABLE `certificaciones`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes_dominios`
--
ALTER TABLE `clientes_dominios`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes_nadd`
--
ALTER TABLE `clientes_nadd`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes_user`
--
ALTER TABLE `clientes_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras2`
--
ALTER TABLE `compras2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras_adobe`
--
ALTER TABLE `compras_adobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras_ibm`
--
ALTER TABLE `compras_ibm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras_oracle`
--
ALTER TABLE `compras_oracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras_Sap`
--
ALTER TABLE `compras_Sap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras_SPLA`
--
ALTER TABLE `compras_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras_unixIBM`
--
ALTER TABLE `compras_unixIBM`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras_unixOracle`
--
ALTER TABLE `compras_unixOracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado`
--
ALTER TABLE `consolidado`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoProcesadores`
--
ALTER TABLE `consolidadoProcesadores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoProcesadoresIBM`
--
ALTER TABLE `consolidadoProcesadoresIBM`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoProcesadoresIBMAux`
--
ALTER TABLE `consolidadoProcesadoresIBMAux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoProcesadoresMSCloud`
--
ALTER TABLE `consolidadoProcesadoresMSCloud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoProcesadoresSAMDiagnostic`
--
ALTER TABLE `consolidadoProcesadoresSAMDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoProcesadores_SPLA`
--
ALTER TABLE `consolidadoProcesadores_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoTipoEquipo`
--
ALTER TABLE `consolidadoTipoEquipo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoTipoEquipoMSCloud`
--
ALTER TABLE `consolidadoTipoEquipoMSCloud`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoTipoEquipoSAMDiagnostic`
--
ALTER TABLE `consolidadoTipoEquipoSAMDiagnostic`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidadoTipoEquipo_SPLA`
--
ALTER TABLE `consolidadoTipoEquipo_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_adobe`
--
ALTER TABLE `consolidado_adobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_adobeAux`
--
ALTER TABLE `consolidado_adobeAux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_ibm`
--
ALTER TABLE `consolidado_ibm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_ibmAux`
--
ALTER TABLE `consolidado_ibmAux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_MSCloud`
--
ALTER TABLE `consolidado_MSCloud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_offices`
--
ALTER TABLE `consolidado_offices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_officesAux`
--
ALTER TABLE `consolidado_officesAux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_oracle`
--
ALTER TABLE `consolidado_oracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_OracleAIX`
--
ALTER TABLE `consolidado_OracleAIX`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_OracleLinux`
--
ALTER TABLE `consolidado_OracleLinux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_OracleSolaris`
--
ALTER TABLE `consolidado_OracleSolaris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_others`
--
ALTER TABLE `consolidado_others`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_SAMDiagnostic`
--
ALTER TABLE `consolidado_SAMDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_server`
--
ALTER TABLE `consolidado_server`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_SPLA`
--
ALTER TABLE `consolidado_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_SPLAAux`
--
ALTER TABLE `consolidado_SPLAAux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_UnixAIX`
--
ALTER TABLE `consolidado_UnixAIX`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_UnixLinux`
--
ALTER TABLE `consolidado_UnixLinux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consolidado_UnixSolaris`
--
ALTER TABLE `consolidado_UnixSolaris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contrato`
--
ALTER TABLE `contrato`
  MODIFY `idContrato` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'identificador del contrato';

--
-- AUTO_INCREMENT de la tabla `contratosVMWare`
--
ALTER TABLE `contratosVMWare`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cotizacionesLicensing`
--
ALTER TABLE `cotizacionesLicensing`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cotizacionProveedor`
--
ALTER TABLE `cotizacionProveedor`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desarrolloPruebaMSDN`
--
ALTER TABLE `desarrolloPruebaMSDN`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desarrolloPruebaMSDNSam`
--
ALTER TABLE `desarrolloPruebaMSDNSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desarrolloPruebaSQL`
--
ALTER TABLE `desarrolloPruebaSQL`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desarrolloPruebaVS`
--
ALTER TABLE `desarrolloPruebaVS`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desarrolloPruebaVSSam`
--
ALTER TABLE `desarrolloPruebaVSSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desarrolloPruebaWindows`
--
ALTER TABLE `desarrolloPruebaWindows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalleContrato`
--
ALTER TABLE `detalleContrato`
  MODIFY `idDetalleContrato` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'identificador del detalle del contrato';

--
-- AUTO_INCREMENT de la tabla `detalleContrato_VMWare`
--
ALTER TABLE `detalleContrato_VMWare`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalleContrato_VMWareSam`
--
ALTER TABLE `detalleContrato_VMWareSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalleCotProveedor`
--
ALTER TABLE `detalleCotProveedor`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalleEvento`
--
ALTER TABLE `detalleEvento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalleServidorSap`
--
ALTER TABLE `detalleServidorSap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo`
--
ALTER TABLE `detalles_equipo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo2`
--
ALTER TABLE `detalles_equipo2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo2Sam`
--
ALTER TABLE `detalles_equipo2Sam`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipoDesuso`
--
ALTER TABLE `detalles_equipoDesuso`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipoMSCloud`
--
ALTER TABLE `detalles_equipoMSCloud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipoSAMDiagnostic`
--
ALTER TABLE `detalles_equipoSAMDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_adobe`
--
ALTER TABLE `detalles_equipo_adobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_adobeSam`
--
ALTER TABLE `detalles_equipo_adobeSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_ibm`
--
ALTER TABLE `detalles_equipo_ibm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_ibmSam`
--
ALTER TABLE `detalles_equipo_ibmSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_oracle`
--
ALTER TABLE `detalles_equipo_oracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_OracleAIX`
--
ALTER TABLE `detalles_equipo_OracleAIX`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_OracleAIXSam`
--
ALTER TABLE `detalles_equipo_OracleAIXSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_OracleLinux`
--
ALTER TABLE `detalles_equipo_OracleLinux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_OracleLinuxSam`
--
ALTER TABLE `detalles_equipo_OracleLinuxSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_oracleSam`
--
ALTER TABLE `detalles_equipo_oracleSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_OracleSolaris`
--
ALTER TABLE `detalles_equipo_OracleSolaris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_OracleSolarisSam`
--
ALTER TABLE `detalles_equipo_OracleSolarisSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_SPLA`
--
ALTER TABLE `detalles_equipo_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_SPLASam`
--
ALTER TABLE `detalles_equipo_SPLASam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_UnixAIX`
--
ALTER TABLE `detalles_equipo_UnixAIX`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_UnixAIXSam`
--
ALTER TABLE `detalles_equipo_UnixAIXSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_UnixLinux`
--
ALTER TABLE `detalles_equipo_UnixLinux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_UnixLinuxSam`
--
ALTER TABLE `detalles_equipo_UnixLinuxSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_UnixSolaris`
--
ALTER TABLE `detalles_equipo_UnixSolaris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalles_equipo_UnixSolarisSam`
--
ALTER TABLE `detalles_equipo_UnixSolarisSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ediciones`
--
ALTER TABLE `ediciones`
  MODIFY `idEdicion` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador de la edicion';

--
-- AUTO_INCREMENT de la tabla `elimEquiposLAEAdobe`
--
ALTER TABLE `elimEquiposLAEAdobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `elimEquiposLAEIbm`
--
ALTER TABLE `elimEquiposLAEIbm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `elimEquiposLAEMicrosoft`
--
ALTER TABLE `elimEquiposLAEMicrosoft`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafAdobeSam`
--
ALTER TABLE `encGrafAdobeSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafIbmSam`
--
ALTER TABLE `encGrafIbmSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafOracleSam`
--
ALTER TABLE `encGrafOracleSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafSam`
--
ALTER TABLE `encGrafSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafSapSam`
--
ALTER TABLE `encGrafSapSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafSPLASam`
--
ALTER TABLE `encGrafSPLASam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafUnixIBMSam`
--
ALTER TABLE `encGrafUnixIBMSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafUnixOracleSam`
--
ALTER TABLE `encGrafUnixOracleSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encGrafVMWareSam`
--
ALTER TABLE `encGrafVMWareSam`
  MODIFY `archivo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equipos`
--
ALTER TABLE `escaneo_equipos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equipos2`
--
ALTER TABLE `escaneo_equipos2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equipos2Sam`
--
ALTER TABLE `escaneo_equipos2Sam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equiposDesuso`
--
ALTER TABLE `escaneo_equiposDesuso`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equiposMSCloud`
--
ALTER TABLE `escaneo_equiposMSCloud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equiposSAMDiagnostic`
--
ALTER TABLE `escaneo_equiposSAMDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equipos_adobe`
--
ALTER TABLE `escaneo_equipos_adobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equipos_ibm`
--
ALTER TABLE `escaneo_equipos_ibm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equipos_oracle`
--
ALTER TABLE `escaneo_equipos_oracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_equipos_SPLA`
--
ALTER TABLE `escaneo_equipos_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escaneo_troubleshoot`
--
ALTER TABLE `escaneo_troubleshoot`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fabricantes`
--
ALTER TABLE `fabricantes`
  MODIFY `idFabricante` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador del fabricante';

--
-- AUTO_INCREMENT de la tabla `filepcs`
--
ALTER TABLE `filepcs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcs2`
--
ALTER TABLE `filepcs2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcsAux`
--
ALTER TABLE `filepcsAux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcsDesuso`
--
ALTER TABLE `filepcsDesuso`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcsMSCloud`
--
ALTER TABLE `filepcsMSCloud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcsSAMDiagnostic`
--
ALTER TABLE `filepcsSAMDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcs_adobe`
--
ALTER TABLE `filepcs_adobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcs_adobeAux`
--
ALTER TABLE `filepcs_adobeAux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcs_ibm`
--
ALTER TABLE `filepcs_ibm`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcs_ibmAux`
--
ALTER TABLE `filepcs_ibmAux`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcs_oracle`
--
ALTER TABLE `filepcs_oracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filepcs_SPLA`
--
ALTER TABLE `filepcs_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gantt`
--
ALTER TABLE `gantt`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historicoAppsSPLA`
--
ALTER TABLE `historicoAppsSPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historicoCurrentSPLA`
--
ALTER TABLE `historicoCurrentSPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historicoCustomerSPLA`
--
ALTER TABLE `historicoCustomerSPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historicoSPLA`
--
ALTER TABLE `historicoSPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historicoVMSPLA`
--
ALTER TABLE `historicoVMSPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `hostsDiagnostic`
--
ALTER TABLE `hostsDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lau_users`
--
ALTER TABLE `lau_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `licenciasCentralizador`
--
ALTER TABLE `licenciasCentralizador`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'identificador del generador de seriales';

--
-- AUTO_INCREMENT de la tabla `licenciasESXiVCenter`
--
ALTER TABLE `licenciasESXiVCenter`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lista_officce`
--
ALTER TABLE `lista_officce`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `logWebtool`
--
ALTER TABLE `logWebtool`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `moduloRecordatorio`
--
ALTER TABLE `moduloRecordatorio`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `MSCloud`
--
ALTER TABLE `MSCloud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `nivelServicio`
--
ALTER TABLE `nivelServicio`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagosFabricantes`
--
ALTER TABLE `pagosFabricantes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagosFabricantesDetalle`
--
ALTER TABLE `pagosFabricantesDetalle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permisosGenerales`
--
ALTER TABLE `permisosGenerales`
  MODIFY `idPermisos` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'identificador del permiso';

--
-- AUTO_INCREMENT de la tabla `phases`
--
ALTER TABLE `phases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `phasesfactories`
--
ALTER TABLE `phasesfactories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `phase_state`
--
ALTER TABLE `phase_state`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `phase_type`
--
ALTER TABLE `phase_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `poc`
--
ALTER TABLE `poc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pocSAP`
--
ALTER TABLE `pocSAP`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `portal`
--
ALTER TABLE `portal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador del producto';

--
-- AUTO_INCREMENT de la tabla `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pruebaDesuso`
--
ALTER TABLE `pruebaDesuso`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pvuCliente`
--
ALTER TABLE `pvuCliente`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pvuClienteAIX`
--
ALTER TABLE `pvuClienteAIX`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pvuClienteLinux`
--
ALTER TABLE `pvuClienteLinux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pvuClienteSolaris`
--
ALTER TABLE `pvuClienteSolaris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `recordatorios`
--
ALTER TABLE `recordatorios`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reportarFallas`
--
ALTER TABLE `reportarFallas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `respCotizProv`
--
ALTER TABLE `respCotizProv`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumenServidorSap`
--
ALTER TABLE `resumenServidorSap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumenServidorSapSam`
--
ALTER TABLE `resumenServidorSapSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumenUsuariosModuloSap`
--
ALTER TABLE `resumenUsuariosModuloSap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumenUsuariosModuloSapSam`
--
ALTER TABLE `resumenUsuariosModuloSapSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_adobe`
--
ALTER TABLE `resumen_adobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_adobeSam`
--
ALTER TABLE `resumen_adobeSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_ibm`
--
ALTER TABLE `resumen_ibm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_ibmSam`
--
ALTER TABLE `resumen_ibmSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_MSCloud`
--
ALTER TABLE `resumen_MSCloud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_office`
--
ALTER TABLE `resumen_office`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_office2`
--
ALTER TABLE `resumen_office2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_office2Sam`
--
ALTER TABLE `resumen_office2Sam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_oracle`
--
ALTER TABLE `resumen_oracle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_OracleAIX`
--
ALTER TABLE `resumen_OracleAIX`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_OracleAIXSam`
--
ALTER TABLE `resumen_OracleAIXSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_OracleLinux`
--
ALTER TABLE `resumen_OracleLinux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_OracleLinuxSam`
--
ALTER TABLE `resumen_OracleLinuxSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_oracleSam`
--
ALTER TABLE `resumen_oracleSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_OracleSolaris`
--
ALTER TABLE `resumen_OracleSolaris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_OracleSolarisSam`
--
ALTER TABLE `resumen_OracleSolarisSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_others`
--
ALTER TABLE `resumen_others`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_SAMDiagnostic`
--
ALTER TABLE `resumen_SAMDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_server`
--
ALTER TABLE `resumen_server`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_SPLA`
--
ALTER TABLE `resumen_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_SPLASam`
--
ALTER TABLE `resumen_SPLASam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_UnixAIX`
--
ALTER TABLE `resumen_UnixAIX`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_UnixAIXSam`
--
ALTER TABLE `resumen_UnixAIXSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_UnixLinux`
--
ALTER TABLE `resumen_UnixLinux`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_UnixLinuxSam`
--
ALTER TABLE `resumen_UnixLinuxSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_UnixSolaris`
--
ALTER TABLE `resumen_UnixSolaris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_UnixSolarisSam`
--
ALTER TABLE `resumen_UnixSolarisSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_VMWare`
--
ALTER TABLE `resumen_VMWare`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resumen_VMWareSam`
--
ALTER TABLE `resumen_VMWareSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles_user`
--
ALTER TABLE `roles_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `SAMDiagnostic`
--
ALTER TABLE `SAMDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `scheduling`
--
ALTER TABLE `scheduling`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `service_query`
--
ALTER TABLE `service_query`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `service_serial`
--
ALTER TABLE `service_serial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `SPLACliente`
--
ALTER TABLE `SPLACliente`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `SPLADiagnostic`
--
ALTER TABLE `SPLADiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `SPLASKU`
--
ALTER TABLE `SPLASKU`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `SPLAVirtualMachine`
--
ALTER TABLE `SPLAVirtualMachine`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sqlServerClientes`
--
ALTER TABLE `sqlServerClientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sqlServerClientesSam`
--
ALTER TABLE `sqlServerClientesSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sqlServerClientesSPLA_Sam`
--
ALTER TABLE `sqlServerClientesSPLA_Sam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sqlServerClientes_SPLA`
--
ALTER TABLE `sqlServerClientes_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `submodulos`
--
ALTER TABLE `submodulos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tablaMaestra`
--
ALTER TABLE `tablaMaestra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador de la tabla maestra';

--
-- AUTO_INCREMENT de la tabla `tablaPVU`
--
ALTER TABLE `tablaPVU`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tabla_conversion`
--
ALTER TABLE `tabla_conversion`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tabla_equivalencia`
--
ALTER TABLE `tabla_equivalencia`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tabla_equivalencia1`
--
ALTER TABLE `tabla_equivalencia1`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tabla_equivalenciaOriginal`
--
ALTER TABLE `tabla_equivalenciaOriginal`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `temas`
--
ALTER TABLE `temas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trasladoCabLicencias`
--
ALTER TABLE `trasladoCabLicencias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trasladoDetLicencias`
--
ALTER TABLE `trasladoDetLicencias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trueUp`
--
ALTER TABLE `trueUp`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trueUpDetalle`
--
ALTER TABLE `trueUpDetalle`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usabilidadMicrosoft`
--
ALTER TABLE `usabilidadMicrosoft`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'identificador del registro';

--
-- AUTO_INCREMENT de la tabla `usabilidadUsuariosSap`
--
ALTER TABLE `usabilidadUsuariosSap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usabilidadUsuariosSapSam`
--
ALTER TABLE `usabilidadUsuariosSapSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarioEquipoAdobe`
--
ALTER TABLE `usuarioEquipoAdobe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarioEquipoIbm`
--
ALTER TABLE `usuarioEquipoIbm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarioEquipoMicrosoft`
--
ALTER TABLE `usuarioEquipoMicrosoft`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuariosModuloSap`
--
ALTER TABLE `usuariosModuloSap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `versiones`
--
ALTER TABLE `versiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador de la version';

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `VMsDiagnostic`
--
ALTER TABLE `VMsDiagnostic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `windowServerClientes`
--
ALTER TABLE `windowServerClientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `windowServerClientesSam`
--
ALTER TABLE `windowServerClientesSam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `windowServerClientesSPLASam`
--
ALTER TABLE `windowServerClientesSPLASam`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `windowServerClientes_SPLA`
--
ALTER TABLE `windowServerClientes_SPLA`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `_tabtemporalcontratos`
--
ALTER TABLE `_tabtemporalcontratos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `agente`
--
ALTER TABLE `agente`
  ADD CONSTRAINT `agente_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `agente_add_remove`
--
ALTER TABLE `agente_add_remove`
  ADD CONSTRAINT `agente_add_remove_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_add_remove_aux`
--
ALTER TABLE `agente_add_remove_aux`
  ADD CONSTRAINT `agente_add_remove_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_autodesk`
--
ALTER TABLE `agente_autodesk`
  ADD CONSTRAINT `agente_autodesk_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_autodesk_aux`
--
ALTER TABLE `agente_autodesk_aux`
  ADD CONSTRAINT `agente_autodesk_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_desinstalacion`
--
ALTER TABLE `agente_desinstalacion`
  ADD CONSTRAINT `agente_desinstalacion_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_desinstalacion_aux`
--
ALTER TABLE `agente_desinstalacion_aux`
  ADD CONSTRAINT `agente_desinstalacion_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_hist`
--
ALTER TABLE `agente_hist`
  ADD CONSTRAINT `agente_hist_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_llave`
--
ALTER TABLE `agente_llave`
  ADD CONSTRAINT `agente_llave_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_llave_aux`
--
ALTER TABLE `agente_llave_aux`
  ADD CONSTRAINT `agente_llave_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_memoria`
--
ALTER TABLE `agente_memoria`
  ADD CONSTRAINT `agente_memoria_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_memoria_aux`
--
ALTER TABLE `agente_memoria_aux`
  ADD CONSTRAINT `agente_memoria_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_metering_errors`
--
ALTER TABLE `agente_metering_errors`
  ADD CONSTRAINT `agente_metering_errors_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_metering_errors_aux`
--
ALTER TABLE `agente_metering_errors_aux`
  ADD CONSTRAINT `agente_metering_errors_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_metering_results`
--
ALTER TABLE `agente_metering_results`
  ADD CONSTRAINT `agente_metering_results_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_metering_results_aux`
--
ALTER TABLE `agente_metering_results_aux`
  ADD CONSTRAINT `agente_metering_results_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_procesador`
--
ALTER TABLE `agente_procesador`
  ADD CONSTRAINT `agente_procesador_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_procesador_aux`
--
ALTER TABLE `agente_procesador_aux`
  ADD CONSTRAINT `agente_procesador_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_proceso`
--
ALTER TABLE `agente_proceso`
  ADD CONSTRAINT `agente_proceso_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_proceso_aux`
--
ALTER TABLE `agente_proceso_aux`
  ADD CONSTRAINT `agente_proceso_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_resultado_escaneo`
--
ALTER TABLE `agente_resultado_escaneo`
  ADD CONSTRAINT `agente_resultado_escaneo_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_resultado_escaneo_aux`
--
ALTER TABLE `agente_resultado_escaneo_aux`
  ADD CONSTRAINT `agente_resultado_escaneo_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_scheduling`
--
ALTER TABLE `agente_scheduling`
  ADD CONSTRAINT `agente_scheduling_fk` FOREIGN KEY (`id_scheduling`) REFERENCES `scheduling` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `agente_scheduling_fk1` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_seguridad`
--
ALTER TABLE `agente_seguridad`
  ADD CONSTRAINT `agente_seguridad_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_seguridad_aux`
--
ALTER TABLE `agente_seguridad_aux`
  ADD CONSTRAINT `agente_seguridad_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_serial_maquina`
--
ALTER TABLE `agente_serial_maquina`
  ADD CONSTRAINT `agente_serial_maquina_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_serial_maquina_aux`
--
ALTER TABLE `agente_serial_maquina_aux`
  ADD CONSTRAINT `agente_serial_maquina_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_servicio`
--
ALTER TABLE `agente_servicio`
  ADD CONSTRAINT `agente_servicio_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_servicio_aux`
--
ALTER TABLE `agente_servicio_aux`
  ADD CONSTRAINT `agente_servicio_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_sistema_operativo`
--
ALTER TABLE `agente_sistema_operativo`
  ADD CONSTRAINT `agente_sistema_operativo_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_sistema_operativo_aux`
--
ALTER TABLE `agente_sistema_operativo_aux`
  ADD CONSTRAINT `agente_sistema_operativo_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_sql_data`
--
ALTER TABLE `agente_sql_data`
  ADD CONSTRAINT `agente_sql_data_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_sql_data_aux`
--
ALTER TABLE `agente_sql_data_aux`
  ADD CONSTRAINT `agente_sql_data_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_tipo_equipo`
--
ALTER TABLE `agente_tipo_equipo`
  ADD CONSTRAINT `agente_tipo_equipo_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_tipo_equipo_aux`
--
ALTER TABLE `agente_tipo_equipo_aux`
  ADD CONSTRAINT `agente_tipo_equipo_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_usabilidad_software`
--
ALTER TABLE `agente_usabilidad_software`
  ADD CONSTRAINT `agente_usabilidad_software_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_usabilidad_software_aux`
--
ALTER TABLE `agente_usabilidad_software_aux`
  ADD CONSTRAINT `agente_usabilidad_software_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_usuario_equipo`
--
ALTER TABLE `agente_usuario_equipo`
  ADD CONSTRAINT `agente_usuario_equipo_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `agente_usuario_equipo_aux`
--
ALTER TABLE `agente_usuario_equipo_aux`
  ADD CONSTRAINT `agente_usuario_equipo_aux_fk` FOREIGN KEY (`id_agente`) REFERENCES `agente` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `alineacionSqlServidores`
--
ALTER TABLE `alineacionSqlServidores`
  ADD CONSTRAINT `alineacionSqlServidores_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`),
  ADD CONSTRAINT `alineacionsSqlServidores_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `alineacionSqlServidoresSam`
--
ALTER TABLE `alineacionSqlServidoresSam`
  ADD CONSTRAINT `alineacionSqlServidoresSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `alineacionSqlServidoresSam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `alineacionSqlServidoresSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `alineacionSqlServidoresSPLA`
--
ALTER TABLE `alineacionSqlServidoresSPLA`
  ADD CONSTRAINT `alineacionSqlServidoresSPLA_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `alineacionSqlServidoresSPLA_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `alineacionSqlServidoresSPLASam`
--
ALTER TABLE `alineacionSqlServidoresSPLASam`
  ADD CONSTRAINT `alineacionSqlServidoresSPLASam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSPLASam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `alineacionSqlServidoresSPLASam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `alineacionSqlServidoresSPLASam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `alineacionWindowsServidores`
--
ALTER TABLE `alineacionWindowsServidores`
  ADD CONSTRAINT `alineacionWindowsServidores_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `alineacionWindowsServidores_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `alineacionWindowsServidoresSam`
--
ALTER TABLE `alineacionWindowsServidoresSam`
  ADD CONSTRAINT `alineacionWindowsServidoresSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `alineacionWindowsServidoresSam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `alineacionWindowsServidoresSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `alineacionWindowsServidoresSPLA`
--
ALTER TABLE `alineacionWindowsServidoresSPLA`
  ADD CONSTRAINT `alineacionWindowsServidoresSPLA_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `alineacionWindowsServidoresSPLA_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `alineacionWindowsServidoresSPLASam`
--
ALTER TABLE `alineacionWindowsServidoresSPLASam`
  ADD CONSTRAINT `alineacionWindowsServidoresSPLASam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSPLASam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `alineacionWindowsServidoresSPLASam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `alineacionWindowsServidoresSPLASam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `appConsolidado_offices`
--
ALTER TABLE `appConsolidado_offices`
  ADD CONSTRAINT `appConsolidado_offices_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `appConsolidado_offices_fk1` FOREIGN KEY (`idCorreo`) REFERENCES `appEscaneoCorreo` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `appDetalles_equipo`
--
ALTER TABLE `appDetalles_equipo`
  ADD CONSTRAINT `appDetalles_equipo_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `appDetalles_equipo_fk1` FOREIGN KEY (`idCorreo`) REFERENCES `appEscaneoCorreo` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `appEscaneo`
--
ALTER TABLE `appEscaneo`
  ADD CONSTRAINT `appEscaneo_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `appEscaneo_fk1` FOREIGN KEY (`idCorreo`) REFERENCES `appEscaneoCorreo` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `appEscaneoCorreo`
--
ALTER TABLE `appEscaneoCorreo`
  ADD CONSTRAINT `appEscaneoCorreo_fk` FOREIGN KEY (`clientes_nadd_id`) REFERENCES `clientes_nadd` (`id`);

--
-- Filtros para la tabla `appFilepcs`
--
ALTER TABLE `appFilepcs`
  ADD CONSTRAINT `appFilepcs_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `appFilepcs_fk1` FOREIGN KEY (`idCorreo`) REFERENCES `appEscaneoCorreo` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `appResumen_office`
--
ALTER TABLE `appResumen_office`
  ADD CONSTRAINT `appResumen_office_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `appResumen_office_fk1` FOREIGN KEY (`idCorreo`) REFERENCES `appEscaneoCorreo` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `archivosFabricantes`
--
ALTER TABLE `archivosFabricantes`
  ADD CONSTRAINT `archivosFabricantes_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `archivosFabricantes_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `archivosIncrementoFabricantes`
--
ALTER TABLE `archivosIncrementoFabricantes`
  ADD CONSTRAINT `archivosIncrementoFabricantes_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `archivosIncrementoFabricantes_fk1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `asigCliente`
--
ALTER TABLE `asigCliente`
  ADD CONSTRAINT `asigCliente_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `asigClienteEmple`
--
ALTER TABLE `asigClienteEmple`
  ADD CONSTRAINT `asigClienteEmple_ibfk_1` FOREIGN KEY (`idAsignacion`) REFERENCES `asigCliente` (`idAsignacion`),
  ADD CONSTRAINT `asigClienteEmple_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `asigClienteEmple_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_adobe`
--
ALTER TABLE `balance_adobe`
  ADD CONSTRAINT `balance_adobe_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_adobe_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_adobeSam`
--
ALTER TABLE `balance_adobeSam`
  ADD CONSTRAINT `balance_adobeSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafAdobeSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_adobeSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_adobeSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_ibm`
--
ALTER TABLE `balance_ibm`
  ADD CONSTRAINT `balance_ibm_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_ibm_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_ibmSam`
--
ALTER TABLE `balance_ibmSam`
  ADD CONSTRAINT `balance_ibmSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafIbmSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_ibmSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_ibmSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_office2`
--
ALTER TABLE `balance_office2`
  ADD CONSTRAINT `balance_office2_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_office2_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_office2Sam`
--
ALTER TABLE `balance_office2Sam`
  ADD CONSTRAINT `balance_office2Sam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_office2Sam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_office2Sam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_oracle`
--
ALTER TABLE `balance_oracle`
  ADD CONSTRAINT `balance_oracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_oracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_oracleSam`
--
ALTER TABLE `balance_oracleSam`
  ADD CONSTRAINT `balance_oracleSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_oracleSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_oracleSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_Sap`
--
ALTER TABLE `balance_Sap`
  ADD CONSTRAINT `balance_Sap_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_Sap_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_SapSam`
--
ALTER TABLE `balance_SapSam`
  ADD CONSTRAINT `balance_SapSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSapSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_SapSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_SapSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_SPLA`
--
ALTER TABLE `balance_SPLA`
  ADD CONSTRAINT `balance_SPLA_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_SPLA_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_SPLASam`
--
ALTER TABLE `balance_SPLASam`
  ADD CONSTRAINT `balance_SPLASam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSPLASam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_SPLASam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_SPLASam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_unixIBM`
--
ALTER TABLE `balance_unixIBM`
  ADD CONSTRAINT `balance_unixIBM_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_unixIBM_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_UnixIBMSam`
--
ALTER TABLE `balance_UnixIBMSam`
  ADD CONSTRAINT `balance_UnixIBMSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixIBMSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_UnixIBMSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_UnixIBMSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_unixOracle`
--
ALTER TABLE `balance_unixOracle`
  ADD CONSTRAINT `balance_unixOracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_unixOracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_UnixOracleSam`
--
ALTER TABLE `balance_UnixOracleSam`
  ADD CONSTRAINT `balance_UnixOracleSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_UnixOracleSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_UnixOracleSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_VMWare`
--
ALTER TABLE `balance_VMWare`
  ADD CONSTRAINT `balance_VMWare_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_VMWare_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `balance_VMWareSam`
--
ALTER TABLE `balance_VMWareSam`
  ADD CONSTRAINT `balance_VMWareSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafVMWareSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `balance_VMWareSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `balance_VMWareSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `centroCosto`
--
ALTER TABLE `centroCosto`
  ADD CONSTRAINT `centroCosto_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `centroCosto_fk1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `clientes_dominios`
--
ALTER TABLE `clientes_dominios`
  ADD CONSTRAINT `clientes_dominios_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `compras2`
--
ALTER TABLE `compras2`
  ADD CONSTRAINT `compras2_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras2_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `compras_adobe`
--
ALTER TABLE `compras_adobe`
  ADD CONSTRAINT `compras_adobe_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras_adobe_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `compras_ibm`
--
ALTER TABLE `compras_ibm`
  ADD CONSTRAINT `compras_ibm_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras_ibm_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `compras_oracle`
--
ALTER TABLE `compras_oracle`
  ADD CONSTRAINT `compras_oracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras_oracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `compras_Sap`
--
ALTER TABLE `compras_Sap`
  ADD CONSTRAINT `compras_Sap_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras_Sap_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `compras_SPLA`
--
ALTER TABLE `compras_SPLA`
  ADD CONSTRAINT `compras_SPLA_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras_SPLA_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `compras_unixIBM`
--
ALTER TABLE `compras_unixIBM`
  ADD CONSTRAINT `compras_unixIBM_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras_unixIBM_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `compras_unixOracle`
--
ALTER TABLE `compras_unixOracle`
  ADD CONSTRAINT `compras_unixOracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `compras_unixOracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado`
--
ALTER TABLE `consolidado`
  ADD CONSTRAINT `consolidado_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidadoProcesadores`
--
ALTER TABLE `consolidadoProcesadores`
  ADD CONSTRAINT `consolidadoProcesadores_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidadoProcesadores_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidadoProcesadoresIBM`
--
ALTER TABLE `consolidadoProcesadoresIBM`
  ADD CONSTRAINT `consolidadoProcesadoresIBM_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidadoProcesadoresIBM_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidadoProcesadoresIBMAux`
--
ALTER TABLE `consolidadoProcesadoresIBMAux`
  ADD CONSTRAINT `consolidadoProcesadoresIBMAux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidadoProcesadoresIBMAux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidadoProcesadoresMSCloud`
--
ALTER TABLE `consolidadoProcesadoresMSCloud`
  ADD CONSTRAINT `consolidadoProcesadoresMSCloud_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `MSCloud` (`id`);

--
-- Filtros para la tabla `consolidadoProcesadoresSAMDiagnostic`
--
ALTER TABLE `consolidadoProcesadoresSAMDiagnostic`
  ADD CONSTRAINT `consolidadoProcesadoresSAMDiagnostic_ibfk_1` FOREIGN KEY (`idDiagnostic`) REFERENCES `SAMDiagnostic` (`id`);

--
-- Filtros para la tabla `consolidadoProcesadores_SPLA`
--
ALTER TABLE `consolidadoProcesadores_SPLA`
  ADD CONSTRAINT `consolidadoProcesadores_SPLA_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidadoProcesadores_SPLA_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidadoTipoEquipo`
--
ALTER TABLE `consolidadoTipoEquipo`
  ADD CONSTRAINT `consolidadoTipoEquipo_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidadoTipoEquipo_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidadoTipoEquipoMSCloud`
--
ALTER TABLE `consolidadoTipoEquipoMSCloud`
  ADD CONSTRAINT `consolidadoTipoEquipoMSCloud_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `MSCloud` (`id`);

--
-- Filtros para la tabla `consolidadoTipoEquipoSAMDiagnostic`
--
ALTER TABLE `consolidadoTipoEquipoSAMDiagnostic`
  ADD CONSTRAINT `consolidadoTipoEquipoSAMDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SAMDiagnostic` (`id`);

--
-- Filtros para la tabla `consolidadoTipoEquipo_SPLA`
--
ALTER TABLE `consolidadoTipoEquipo_SPLA`
  ADD CONSTRAINT `consolidadoTipoEquipo_SPLA_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidadoTipoEquipo_SPLA_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_adobe`
--
ALTER TABLE `consolidado_adobe`
  ADD CONSTRAINT `consolidado_adobe_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_adobe_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_adobeAux`
--
ALTER TABLE `consolidado_adobeAux`
  ADD CONSTRAINT `consolidado_adobeAux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_adobeAux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_ibm`
--
ALTER TABLE `consolidado_ibm`
  ADD CONSTRAINT `consolidado_ibm_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_ibm_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_ibmAux`
--
ALTER TABLE `consolidado_ibmAux`
  ADD CONSTRAINT `consolidado_ibmAux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_ibmAux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_MSCloud`
--
ALTER TABLE `consolidado_MSCloud`
  ADD CONSTRAINT `consolidado_MSCloud_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `MSCloud` (`id`);

--
-- Filtros para la tabla `consolidado_offices`
--
ALTER TABLE `consolidado_offices`
  ADD CONSTRAINT `consolidado_offices_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_offices_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_officesAux`
--
ALTER TABLE `consolidado_officesAux`
  ADD CONSTRAINT `consolidado_officesAux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_officesAux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_oracle`
--
ALTER TABLE `consolidado_oracle`
  ADD CONSTRAINT `consolidado_oracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_oracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_OracleAIX`
--
ALTER TABLE `consolidado_OracleAIX`
  ADD CONSTRAINT `consolidado_OracleAIX_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_OracleAIX_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_OracleLinux`
--
ALTER TABLE `consolidado_OracleLinux`
  ADD CONSTRAINT `consolidado_OracleLinux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_OracleLinux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_OracleSolaris`
--
ALTER TABLE `consolidado_OracleSolaris`
  ADD CONSTRAINT `consolidado_OracleSolaris_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_OracleSolaris_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_others`
--
ALTER TABLE `consolidado_others`
  ADD CONSTRAINT `consolidado_others_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_others_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_SAMDiagnostic`
--
ALTER TABLE `consolidado_SAMDiagnostic`
  ADD CONSTRAINT `consolidado_SAMDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SAMDiagnostic` (`id`);

--
-- Filtros para la tabla `consolidado_server`
--
ALTER TABLE `consolidado_server`
  ADD CONSTRAINT `consolidado_server_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_server_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_SPLA`
--
ALTER TABLE `consolidado_SPLA`
  ADD CONSTRAINT `consolidado_SPLA_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_SPLA_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_UnixAIX`
--
ALTER TABLE `consolidado_UnixAIX`
  ADD CONSTRAINT `consolidado_UnixAIX_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_UnixAIX_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_UnixLinux`
--
ALTER TABLE `consolidado_UnixLinux`
  ADD CONSTRAINT `consolidado_UnixLinux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_UnixLinux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `consolidado_UnixSolaris`
--
ALTER TABLE `consolidado_UnixSolaris`
  ADD CONSTRAINT `consolidado_UnixSolaris_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `consolidado_UnixSolaris_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `contrato_fk` FOREIGN KEY (`idFabricante`) REFERENCES `fabricantes` (`idFabricante`),
  ADD CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`),
  ADD CONSTRAINT `contrato_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `contratosVMWare`
--
ALTER TABLE `contratosVMWare`
  ADD CONSTRAINT `contratosVMWare_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `contratosVMWare_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `cotizacionProveedor`
--
ALTER TABLE `cotizacionProveedor`
  ADD CONSTRAINT `cotizacionProveedor_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `cotizacionProveedor_fk1` FOREIGN KEY (`fabricante`) REFERENCES `fabricantes` (`idFabricante`),
  ADD CONSTRAINT `cotizacionProveedor_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `dashboard`
--
ALTER TABLE `dashboard`
  ADD CONSTRAINT `dashboard_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `desarrolloPruebaMSDN`
--
ALTER TABLE `desarrolloPruebaMSDN`
  ADD CONSTRAINT `desarrolloPruebaMSDN_fk` FOREIGN KEY (`idDesarrolloPruebaVS`) REFERENCES `desarrolloPruebaVS` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `desarrolloPruebaMSDN_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `desarrolloPruebaMSDN_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `desarrolloPruebaMSDNSam`
--
ALTER TABLE `desarrolloPruebaMSDNSam`
  ADD CONSTRAINT `desarrolloPruebaMSDNSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `desarrolloPruebaMSDNSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`),
  ADD CONSTRAINT `desarrolloPruebaMSDNSam_ibfk_3` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`);

--
-- Filtros para la tabla `desarrolloPruebaSQL`
--
ALTER TABLE `desarrolloPruebaSQL`
  ADD CONSTRAINT `desarrolloPruebaSQL_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`),
  ADD CONSTRAINT `desarrolloPruebaSQL_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `desarrolloPruebaVS`
--
ALTER TABLE `desarrolloPruebaVS`
  ADD CONSTRAINT `desarrolloPruebaVS_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `desarrolloPruebaVS_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `desarrolloPruebaVSSam`
--
ALTER TABLE `desarrolloPruebaVSSam`
  ADD CONSTRAINT `desarrolloPruebaVSSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `desarrolloPruebaVSSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `desarrolloPruebaVSSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `desarrolloPruebaWindows`
--
ALTER TABLE `desarrolloPruebaWindows`
  ADD CONSTRAINT `desarrolloPruebaWindows_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `desarrolloPruebaWindows_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalleContrato`
--
ALTER TABLE `detalleContrato`
  ADD CONSTRAINT `detalleContrato_ibfk_1` FOREIGN KEY (`idContrato`) REFERENCES `contrato` (`idContrato`);

--
-- Filtros para la tabla `detalleContrato_VMWare`
--
ALTER TABLE `detalleContrato_VMWare`
  ADD CONSTRAINT `detalleContrato_VMWare_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalleContrato_VMWare_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalleContrato_VMWareSam`
--
ALTER TABLE `detalleContrato_VMWareSam`
  ADD CONSTRAINT `detalleContrato_VMWareSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafVMWareSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalleContrato_VMWareSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalleContrato_VMWareSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalleCotProveedor`
--
ALTER TABLE `detalleCotProveedor`
  ADD CONSTRAINT `detalleCotProveedor_fk` FOREIGN KEY (`idCotizacion`) REFERENCES `cotizacionProveedor` (`id`);

--
-- Filtros para la tabla `detalleEvento`
--
ALTER TABLE `detalleEvento`
  ADD CONSTRAINT `detalleEvento_fk` FOREIGN KEY (`evento`) REFERENCES `eventos` (`id`);

--
-- Filtros para la tabla `detalleMaestra`
--
ALTER TABLE `detalleMaestra`
  ADD CONSTRAINT `detalleMaestra_fk` FOREIGN KEY (`idMaestra`) REFERENCES `tablaMaestra` (`id`);

--
-- Filtros para la tabla `detalleServidorSap`
--
ALTER TABLE `detalleServidorSap`
  ADD CONSTRAINT `detalleServidorSap_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalleServidorSap_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo`
--
ALTER TABLE `detalles_equipo`
  ADD CONSTRAINT `detalles_equipo_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo2`
--
ALTER TABLE `detalles_equipo2`
  ADD CONSTRAINT `detalles_equipo2_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo2_fk1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo2Sam`
--
ALTER TABLE `detalles_equipo2Sam`
  ADD CONSTRAINT `detalles_equipo2Sam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo2Sam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo2Sam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipoDesuso`
--
ALTER TABLE `detalles_equipoDesuso`
  ADD CONSTRAINT `detalles_equipoDesuso_fk` FOREIGN KEY (`idPrueba`) REFERENCES `pruebaDesuso` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `detalles_equipoMSCloud`
--
ALTER TABLE `detalles_equipoMSCloud`
  ADD CONSTRAINT `detalles_equipoMSCLoud_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `MSCloud` (`id`);

--
-- Filtros para la tabla `detalles_equipoSAMDiagnostic`
--
ALTER TABLE `detalles_equipoSAMDiagnostic`
  ADD CONSTRAINT `detalles_equipoSAMDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SAMDiagnostic` (`id`);

--
-- Filtros para la tabla `detalles_equipo_adobe`
--
ALTER TABLE `detalles_equipo_adobe`
  ADD CONSTRAINT `detalles_equipo_adobe_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_adobe_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_adobeSam`
--
ALTER TABLE `detalles_equipo_adobeSam`
  ADD CONSTRAINT `detalles_equipo_adobeSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafAdobeSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_adobeSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_adobeSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_ibm`
--
ALTER TABLE `detalles_equipo_ibm`
  ADD CONSTRAINT `detalles_equipo_ibm_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_ibm_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_ibmSam`
--
ALTER TABLE `detalles_equipo_ibmSam`
  ADD CONSTRAINT `detalles_equipo_ibmSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafIbmSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_ibmSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_ibmSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_oracle`
--
ALTER TABLE `detalles_equipo_oracle`
  ADD CONSTRAINT `detalles_equipo_oracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_oracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_OracleAIX`
--
ALTER TABLE `detalles_equipo_OracleAIX`
  ADD CONSTRAINT `detalles_equipo_OracleAIX_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_OracleAIX_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_OracleAIXSam`
--
ALTER TABLE `detalles_equipo_OracleAIXSam`
  ADD CONSTRAINT `detalles_equipo_OracleAIXSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_OracleAIXSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_OracleAIXSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_OracleLinux`
--
ALTER TABLE `detalles_equipo_OracleLinux`
  ADD CONSTRAINT `detalles_equipo_OracleLinux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_OracleLinux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_OracleLinuxSam`
--
ALTER TABLE `detalles_equipo_OracleLinuxSam`
  ADD CONSTRAINT `detalles_equipo_OracleLinuxSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_OracleLinuxSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_OracleLinuxSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_oracleSam`
--
ALTER TABLE `detalles_equipo_oracleSam`
  ADD CONSTRAINT `detalles_equipo_oracleSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_oracleSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_oracleSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_OracleSolaris`
--
ALTER TABLE `detalles_equipo_OracleSolaris`
  ADD CONSTRAINT `detalles_equipo_OracleSolaris_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_OracleSolaris_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_OracleSolarisSam`
--
ALTER TABLE `detalles_equipo_OracleSolarisSam`
  ADD CONSTRAINT `detalles_equipo_OracleSolarisSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_OracleSolarisSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_OracleSolarisSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_SPLA`
--
ALTER TABLE `detalles_equipo_SPLA`
  ADD CONSTRAINT `detalles_equipo_SPLA_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_SPLA_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_SPLASam`
--
ALTER TABLE `detalles_equipo_SPLASam`
  ADD CONSTRAINT `detalles_equipo_SPLASam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSPLASam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_SPLASam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_SPLASam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_UnixAIX`
--
ALTER TABLE `detalles_equipo_UnixAIX`
  ADD CONSTRAINT `detalles_equipo_UnixAIX_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_UnixAIX_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_UnixAIXSam`
--
ALTER TABLE `detalles_equipo_UnixAIXSam`
  ADD CONSTRAINT `detalles_equipo_UnixAIXSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixIBMSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_UnixAIXSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_UnixAIXSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_UnixLinux`
--
ALTER TABLE `detalles_equipo_UnixLinux`
  ADD CONSTRAINT `detalles_equipo_UnixLinux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_UnixLinux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_UnixLinuxSam`
--
ALTER TABLE `detalles_equipo_UnixLinuxSam`
  ADD CONSTRAINT `detalles_equipo_UnixLinuxSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixIBMSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_UnixLinuxSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_UnixLinuxSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_UnixSolaris`
--
ALTER TABLE `detalles_equipo_UnixSolaris`
  ADD CONSTRAINT `detalles_equipo_UnixSolaris_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_UnixSolaris_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `detalles_equipo_UnixSolarisSam`
--
ALTER TABLE `detalles_equipo_UnixSolarisSam`
  ADD CONSTRAINT `detalles_equipo_UnixSolarisSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixIBMSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalles_equipo_UnixSolarisSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `detalles_equipo_UnixSolarisSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `edicproducto`
--
ALTER TABLE `edicproducto`
  ADD CONSTRAINT `edicproducto_fk` FOREIGN KEY (`idFabricante`) REFERENCES `fabricantes` (`idFabricante`),
  ADD CONSTRAINT `edicproducto_fk1` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`idProducto`);

--
-- Filtros para la tabla `encArchAdobeSam`
--
ALTER TABLE `encArchAdobeSam`
  ADD CONSTRAINT `encArchAdobeSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchAdobeSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchIbmSam`
--
ALTER TABLE `encArchIbmSam`
  ADD CONSTRAINT `encArchIbmSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchIbmSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchOracleSam`
--
ALTER TABLE `encArchOracleSam`
  ADD CONSTRAINT `encArchOracleSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchOracleSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchSam`
--
ALTER TABLE `encArchSam`
  ADD CONSTRAINT `encArchSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchSapSam`
--
ALTER TABLE `encArchSapSam`
  ADD CONSTRAINT `encArchSapSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchSapSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchSPLASam`
--
ALTER TABLE `encArchSPLASam`
  ADD CONSTRAINT `encArchSPLASam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchSPLASam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchUnixIBMSam`
--
ALTER TABLE `encArchUnixIBMSam`
  ADD CONSTRAINT `encArchUnixIBMSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchUnixIBMSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchUnixOracleSam`
--
ALTER TABLE `encArchUnixOracleSam`
  ADD CONSTRAINT `encArchUnixOracleSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchUnixOracleSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encArchVMWareSam`
--
ALTER TABLE `encArchVMWareSam`
  ADD CONSTRAINT `encArchVMWareSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encArchVMWareSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafAdobeSam`
--
ALTER TABLE `encGrafAdobeSam`
  ADD CONSTRAINT `encGrafAdobeSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafAdobeSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafIbmSam`
--
ALTER TABLE `encGrafIbmSam`
  ADD CONSTRAINT `encGrafIbmSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafIbmSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafOracleSam`
--
ALTER TABLE `encGrafOracleSam`
  ADD CONSTRAINT `encGrafOracleSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafOracleSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafSam`
--
ALTER TABLE `encGrafSam`
  ADD CONSTRAINT `encGrafSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafSapSam`
--
ALTER TABLE `encGrafSapSam`
  ADD CONSTRAINT `encGrafSapSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafSapSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafSPLASam`
--
ALTER TABLE `encGrafSPLASam`
  ADD CONSTRAINT `encGrafSPLASam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafSPLASam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafUnixIBMSam`
--
ALTER TABLE `encGrafUnixIBMSam`
  ADD CONSTRAINT `encGrafUnixIBMSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafUnixIBMSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafUnixOracleSam`
--
ALTER TABLE `encGrafUnixOracleSam`
  ADD CONSTRAINT `encGrafUnixOracleSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafUnixOracleSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `encGrafVMWareSam`
--
ALTER TABLE `encGrafVMWareSam`
  ADD CONSTRAINT `encGrafVMWareSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `encGrafVMWareSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_equipos`
--
ALTER TABLE `escaneo_equipos`
  ADD CONSTRAINT `escaneo_equipos_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_equipos_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_equipos2`
--
ALTER TABLE `escaneo_equipos2`
  ADD CONSTRAINT `escaneo_equipos2_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_equipos2_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_equipos2Sam`
--
ALTER TABLE `escaneo_equipos2Sam`
  ADD CONSTRAINT `escaneo_equipos2Sam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `escaneo_equipos2Sam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_equipos2Sam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_equiposDesuso`
--
ALTER TABLE `escaneo_equiposDesuso`
  ADD CONSTRAINT `escaneo_equiposDesuso_fk` FOREIGN KEY (`idPrueba`) REFERENCES `pruebaDesuso` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `escaneo_equiposMSCloud`
--
ALTER TABLE `escaneo_equiposMSCloud`
  ADD CONSTRAINT `escaneo_equiposMSCloud_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `MSCloud` (`id`);

--
-- Filtros para la tabla `escaneo_equiposSAMDiagnostic`
--
ALTER TABLE `escaneo_equiposSAMDiagnostic`
  ADD CONSTRAINT `escaneo_equiposSAMDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SAMDiagnostic` (`id`);

--
-- Filtros para la tabla `escaneo_equipos_adobe`
--
ALTER TABLE `escaneo_equipos_adobe`
  ADD CONSTRAINT `escaneo_equipos_adobe_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_equipos_adobe_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_equipos_ibm`
--
ALTER TABLE `escaneo_equipos_ibm`
  ADD CONSTRAINT `escaneo_equipos_ibm_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_equipos_ibm_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_equipos_oracle`
--
ALTER TABLE `escaneo_equipos_oracle`
  ADD CONSTRAINT `escaneo_equipos_oracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_equipos_oracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_equipos_SPLA`
--
ALTER TABLE `escaneo_equipos_SPLA`
  ADD CONSTRAINT `escaneo_equipos_SPLA_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_equipos_SPLA_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `escaneo_troubleshoot`
--
ALTER TABLE `escaneo_troubleshoot`
  ADD CONSTRAINT `escaneo_troubleshoot_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `escaneo_troubleshoot_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcs`
--
ALTER TABLE `filepcs`
  ADD CONSTRAINT `filepcs_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcs2`
--
ALTER TABLE `filepcs2`
  ADD CONSTRAINT `filepcs2_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs2_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcsAux`
--
ALTER TABLE `filepcsAux`
  ADD CONSTRAINT `filepcsAux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcsAux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcsDesuso`
--
ALTER TABLE `filepcsDesuso`
  ADD CONSTRAINT `filepcsDesuso_fk` FOREIGN KEY (`idPrueba`) REFERENCES `pruebaDesuso` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `filepcsMSCloud`
--
ALTER TABLE `filepcsMSCloud`
  ADD CONSTRAINT `filepcsMSCloud_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `MSCloud` (`id`);

--
-- Filtros para la tabla `filepcsSAMDiagnostic`
--
ALTER TABLE `filepcsSAMDiagnostic`
  ADD CONSTRAINT `filepcsSAMDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SAMDiagnostic` (`id`);

--
-- Filtros para la tabla `filepcs_adobe`
--
ALTER TABLE `filepcs_adobe`
  ADD CONSTRAINT `filepcs_adobe_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs_adobe_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcs_adobeAux`
--
ALTER TABLE `filepcs_adobeAux`
  ADD CONSTRAINT `filepcs_adobeAux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs_adobeAux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcs_ibm`
--
ALTER TABLE `filepcs_ibm`
  ADD CONSTRAINT `filepcs_ibm_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs_ibm_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcs_ibmAux`
--
ALTER TABLE `filepcs_ibmAux`
  ADD CONSTRAINT `filepcs_ibmAux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs_ibmAux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcs_oracle`
--
ALTER TABLE `filepcs_oracle`
  ADD CONSTRAINT `filepcs_oracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs_oracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `filepcs_SPLA`
--
ALTER TABLE `filepcs_SPLA`
  ADD CONSTRAINT `filepcs_SPLA_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `filepcs_SPLA_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `gantt`
--
ALTER TABLE `gantt`
  ADD CONSTRAINT `gantt_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `ganttDetalle`
--
ALTER TABLE `ganttDetalle`
  ADD CONSTRAINT `ganttDetalle_fk` FOREIGN KEY (`idGantt`) REFERENCES `gantt` (`id`);

--
-- Filtros para la tabla `historicoAppsSPLA`
--
ALTER TABLE `historicoAppsSPLA`
  ADD CONSTRAINT `historicoAppsSPLA_ibfk_1` FOREIGN KEY (`idHistorico`) REFERENCES `historicoSPLA` (`id`);

--
-- Filtros para la tabla `historicoCurrentSPLA`
--
ALTER TABLE `historicoCurrentSPLA`
  ADD CONSTRAINT `historicoCurrentSPLA_ibfk_1` FOREIGN KEY (`idHistorico`) REFERENCES `historicoSPLA` (`id`);

--
-- Filtros para la tabla `historicoCustomerSPLA`
--
ALTER TABLE `historicoCustomerSPLA`
  ADD CONSTRAINT `historicoCustomerSPLA_fk` FOREIGN KEY (`idHistorico`) REFERENCES `historicoSPLA` (`id`);

--
-- Filtros para la tabla `historicoSPLA`
--
ALTER TABLE `historicoSPLA`
  ADD CONSTRAINT `cliente_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `historicoVMSPLA`
--
ALTER TABLE `historicoVMSPLA`
  ADD CONSTRAINT `historicoVMSPLA_ibfk_1` FOREIGN KEY (`idHistorico`) REFERENCES `historicoSPLA` (`id`);

--
-- Filtros para la tabla `hostsDiagnostic`
--
ALTER TABLE `hostsDiagnostic`
  ADD CONSTRAINT `hostsDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SPLADiagnostic` (`id`);

--
-- Filtros para la tabla `licenciasCentralizador`
--
ALTER TABLE `licenciasCentralizador`
  ADD CONSTRAINT `licenciasCentralizador_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `licenciasESXiVCenter`
--
ALTER TABLE `licenciasESXiVCenter`
  ADD CONSTRAINT `licenciasESXiVCenter_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `licenciasESXiVCenter_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `logWebtool`
--
ALTER TABLE `logWebtool`
  ADD CONSTRAINT `logWebtool_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `logWebtool_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `politSam`
--
ALTER TABLE `politSam`
  ADD CONSTRAINT `politSam_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `prodFabricante`
--
ALTER TABLE `prodFabricante`
  ADD CONSTRAINT `prodFabricante_fk` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`idProducto`),
  ADD CONSTRAINT `prodFabricante_fk1` FOREIGN KEY (`idFabricante`) REFERENCES `fabricantes` (`idFabricante`);

--
-- Filtros para la tabla `pvuCliente`
--
ALTER TABLE `pvuCliente`
  ADD CONSTRAINT `pvuCliente_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `pvuCliente_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `recordatorios`
--
ALTER TABLE `recordatorios`
  ADD CONSTRAINT `recordatorios_fk` FOREIGN KEY (`modulo`) REFERENCES `moduloRecordatorio` (`id`),
  ADD CONSTRAINT `recordatorios_fk1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `respCotizProv`
--
ALTER TABLE `respCotizProv`
  ADD CONSTRAINT `respCotizProv_fk` FOREIGN KEY (`idCotizacionProv`) REFERENCES `cotizacionProveedor` (`id`);

--
-- Filtros para la tabla `resumenServidorSap`
--
ALTER TABLE `resumenServidorSap`
  ADD CONSTRAINT `resumenServidorSap_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumenServidorSap_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumenServidorSapSam`
--
ALTER TABLE `resumenServidorSapSam`
  ADD CONSTRAINT `resumenServidorSapSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSapSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumenServidorSapSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumenServidorSapSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumenUsuariosModuloSap`
--
ALTER TABLE `resumenUsuariosModuloSap`
  ADD CONSTRAINT `resumenUsuariosModuloSap_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumenUsuariosModuloSap_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumenUsuariosModuloSapSam`
--
ALTER TABLE `resumenUsuariosModuloSapSam`
  ADD CONSTRAINT `resumenUsuariosModuloSapSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSapSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumenUsuariosModuloSapSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumenUsuariosModuloSapSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_adobe`
--
ALTER TABLE `resumen_adobe`
  ADD CONSTRAINT `resumen_adobe_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_adobe_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_adobeSam`
--
ALTER TABLE `resumen_adobeSam`
  ADD CONSTRAINT `resumen_adobeSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafAdobeSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_adobeSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_adobeSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_ibm`
--
ALTER TABLE `resumen_ibm`
  ADD CONSTRAINT `resumen_ibm_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_ibm_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_ibmSam`
--
ALTER TABLE `resumen_ibmSam`
  ADD CONSTRAINT `resumen_ibmSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafIbmSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_ibmSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_ibmSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_MSCloud`
--
ALTER TABLE `resumen_MSCloud`
  ADD CONSTRAINT `resumen_MSCloud_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `MSCloud` (`id`);

--
-- Filtros para la tabla `resumen_office`
--
ALTER TABLE `resumen_office`
  ADD CONSTRAINT `resumen_office_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_office_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_office2`
--
ALTER TABLE `resumen_office2`
  ADD CONSTRAINT `resumen_office2_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_office2_fk1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_office2Sam`
--
ALTER TABLE `resumen_office2Sam`
  ADD CONSTRAINT `resumen_office2Sam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_office2Sam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_office2Sam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_oracle`
--
ALTER TABLE `resumen_oracle`
  ADD CONSTRAINT `resumen_oracle_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_oracle_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_OracleAIX`
--
ALTER TABLE `resumen_OracleAIX`
  ADD CONSTRAINT `resumen_OracleAIX_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_OracleAIX_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_OracleAIXSam`
--
ALTER TABLE `resumen_OracleAIXSam`
  ADD CONSTRAINT `resumen_OracleAIXSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_OracleAIXSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_OracleAIXSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_OracleLinux`
--
ALTER TABLE `resumen_OracleLinux`
  ADD CONSTRAINT `resumen_OracleLinux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_OracleLinux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_OracleLinuxSam`
--
ALTER TABLE `resumen_OracleLinuxSam`
  ADD CONSTRAINT `resumen_OracleLinuxSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_OracleLinuxSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_OracleLinuxSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_oracleSam`
--
ALTER TABLE `resumen_oracleSam`
  ADD CONSTRAINT `resumen_oracleSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_oracleSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_oracleSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_OracleSolaris`
--
ALTER TABLE `resumen_OracleSolaris`
  ADD CONSTRAINT `resumen_OracleSolaris_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_OracleSolaris_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_OracleSolarisSam`
--
ALTER TABLE `resumen_OracleSolarisSam`
  ADD CONSTRAINT `resumen_OracleSolarisSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixOracleSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_OracleSolarisSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_OracleSolarisSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_others`
--
ALTER TABLE `resumen_others`
  ADD CONSTRAINT `resumen_others_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_others_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_SAMDiagnostic`
--
ALTER TABLE `resumen_SAMDiagnostic`
  ADD CONSTRAINT `resumen_SAMDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SAMDiagnostic` (`id`);

--
-- Filtros para la tabla `resumen_server`
--
ALTER TABLE `resumen_server`
  ADD CONSTRAINT `resumen_server_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_server_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_SPLA`
--
ALTER TABLE `resumen_SPLA`
  ADD CONSTRAINT `resumen_SPLA_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_SPLA_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_SPLASam`
--
ALTER TABLE `resumen_SPLASam`
  ADD CONSTRAINT `resumen_SPLASam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSPLASam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_SPLASam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_SPLASam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_UnixAIX`
--
ALTER TABLE `resumen_UnixAIX`
  ADD CONSTRAINT `resumen_UnixAIX_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_UnixAIX_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_UnixAIXSam`
--
ALTER TABLE `resumen_UnixAIXSam`
  ADD CONSTRAINT `resumen_UnixAIXSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixIBMSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_UnixAIXSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_UnixAIXSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_UnixLinux`
--
ALTER TABLE `resumen_UnixLinux`
  ADD CONSTRAINT `resumen_UnixLinux_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_UnixLinux_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_UnixLinuxSam`
--
ALTER TABLE `resumen_UnixLinuxSam`
  ADD CONSTRAINT `resumen_UnixLinuxSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixIBMSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_UnixLinuxSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_UnixLinuxSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_UnixSolaris`
--
ALTER TABLE `resumen_UnixSolaris`
  ADD CONSTRAINT `resumen_UnixSolaris_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_UnixSolaris_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_UnixSolarisSam`
--
ALTER TABLE `resumen_UnixSolarisSam`
  ADD CONSTRAINT `resumen_UnixSolarisSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafUnixIBMSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_UnixSolarisSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_UnixSolarisSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_VMWare`
--
ALTER TABLE `resumen_VMWare`
  ADD CONSTRAINT `resumen_VMWare_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_VMWare_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `resumen_VMWareSam`
--
ALTER TABLE `resumen_VMWareSam`
  ADD CONSTRAINT `resumen_VMWareSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafVMWareSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `resumen_VMWareSam_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `resumen_VMWareSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `scheduling`
--
ALTER TABLE `scheduling`
  ADD CONSTRAINT `scheduling_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `SPLACliente`
--
ALTER TABLE `SPLACliente`
  ADD CONSTRAINT `SPLACliente_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `SPLACliente_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `SPLAVirtualMachine`
--
ALTER TABLE `SPLAVirtualMachine`
  ADD CONSTRAINT `SPLAVirtualMachine_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `SPLAVirtualMachine_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `sqlServerClientes`
--
ALTER TABLE `sqlServerClientes`
  ADD CONSTRAINT `sqlServerClientes_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `sqlServerClientes_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `sqlServerClientesSam`
--
ALTER TABLE `sqlServerClientesSam`
  ADD CONSTRAINT `sqlServerClientesSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `sqlServerClientesSam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `sqlServerClientesSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `sqlServerClientesSPLA_Sam`
--
ALTER TABLE `sqlServerClientesSPLA_Sam`
  ADD CONSTRAINT `sqlServerClientesSPLA_Sam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSPLASam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `sqlServerClientesSPLA_Sam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `sqlServerClientesSPLA_Sam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `sqlServerClientes_SPLA`
--
ALTER TABLE `sqlServerClientes_SPLA`
  ADD CONSTRAINT `sqlServerClientes_SPLA_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `sqlServerClientes_SPLA_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `tablaPVU`
--
ALTER TABLE `tablaPVU`
  ADD CONSTRAINT `tablaPVU_fk` FOREIGN KEY (`idMaestraProcesador`,`idProcesador`) REFERENCES `detalleMaestra` (`idMaestra`, `idDetalle`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tablaPVU_fk1` FOREIGN KEY (`idMaestraModelo`,`idModeloServidor`) REFERENCES `detalleMaestra` (`idMaestra`, `idDetalle`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `trasladoDetLicencias`
--
ALTER TABLE `trasladoDetLicencias`
  ADD CONSTRAINT `trasladoDetLicencias_ibfk_1` FOREIGN KEY (`idTrasladoLicencia`) REFERENCES `trasladoCabLicencias` (`id`);

--
-- Filtros para la tabla `trueUp`
--
ALTER TABLE `trueUp`
  ADD CONSTRAINT `trueUp_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `trueUpDetalle`
--
ALTER TABLE `trueUpDetalle`
  ADD CONSTRAINT `trueUpDetalle_fk` FOREIGN KEY (`idTrueUp`) REFERENCES `trueUp` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `usabilidadMicrosoft`
--
ALTER TABLE `usabilidadMicrosoft`
  ADD CONSTRAINT `usabilidadMicrosoft_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `usabilidadMicrosoft_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `usabilidadUsuariosSap`
--
ALTER TABLE `usabilidadUsuariosSap`
  ADD CONSTRAINT `usabilidadUsuariosSap_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `usabilidadUsuariosSap_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `usabilidadUsuariosSapSam`
--
ALTER TABLE `usabilidadUsuariosSapSam`
  ADD CONSTRAINT `usabilidadUsuariosSapSam_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `usabilidadUsuariosSapSam_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`),
  ADD CONSTRAINT `usabilidadUsuariosSapSam_ibfk_3` FOREIGN KEY (`archivo`) REFERENCES `encGrafSapSam` (`archivo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `usuarioEquipoMicrosoft`
--
ALTER TABLE `usuarioEquipoMicrosoft`
  ADD CONSTRAINT `usuarioEquipoMicrosoft_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `usuarioEquipoMicrosoft_ibfk_2` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `usuariosModuloSap`
--
ALTER TABLE `usuariosModuloSap`
  ADD CONSTRAINT `usuariosModuloSap_fk` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `usuariosModuloSap_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `VMsDiagnostic`
--
ALTER TABLE `VMsDiagnostic`
  ADD CONSTRAINT `VMsDiagnostic_fk` FOREIGN KEY (`idDiagnostic`) REFERENCES `SPLADiagnostic` (`id`);

--
-- Filtros para la tabla `windowServerClientes`
--
ALTER TABLE `windowServerClientes`
  ADD CONSTRAINT `windowServerClientes_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `windowServerClientes_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `windowServerClientesSam`
--
ALTER TABLE `windowServerClientesSam`
  ADD CONSTRAINT `windowServerClientesSam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `windowServerClientesSam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `windowServerClientesSam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `windowServerClientesSPLASam`
--
ALTER TABLE `windowServerClientesSPLASam`
  ADD CONSTRAINT `windowServerClientesSPLASam_ibfk_1` FOREIGN KEY (`archivo`) REFERENCES `encGrafSPLASam` (`archivo`) ON DELETE CASCADE,
  ADD CONSTRAINT `windowServerClientesSPLASam_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `windowServerClientesSPLASam_ibfk_3` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);

--
-- Filtros para la tabla `windowServerClientes_SPLA`
--
ALTER TABLE `windowServerClientes_SPLA`
  ADD CONSTRAINT `windowServerClientes_SPLA_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `windowServerClientes_SPLA_ibfk_1` FOREIGN KEY (`empleado`) REFERENCES `empleados` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
