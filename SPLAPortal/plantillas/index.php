<div class="container mt-4">
    <div class="row bordeEncabezado">
        <div class="col col-md-3"><img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/logo.png" alt="SPLA" class="col-md-12 .img-fluid"></div>
        <div class="col p-2 text-center h2"><?= $portal->su0012 ?></div>
    </div>
    
    <div class="row">
        <div class="col bg-primary p-2 text-center text-light h2"><?= $portal->su0013 ?></div>
    </div>
    
    <div class="row">
        <div class="col bg-primary p-2 text-center text-light h2"><?= $portal->su0014 ?></div>
    </div>
    
    <div class="row justify-content-md-center mt-4">
        <div class="col-4 contenedorCentral">                
            <p class="mt-5 text-center h3"><b><?= $portal->su0015 ?></b></p>

            <div class="text-center">
                <button name="enviar" id="enviar" value="1" class="btn btn-lg btn-primary mt-4"><?= $portal->su0016 ?></button>
            </div>
            
            <div class="mt-5"></div>
        </div>
    </div>
</div>

<script>
    $(function(){
       $("#enviar").click(function(){
           location.href = "<?= $GLOBALS["domain_root1"] . $portal->su0018 ?>";
       }); 
    });
</script>