<div class="container-fluid cabeceraWelcome">
    <div class="row">
        <div class="col col-md-3"><img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/logo.png" alt="SPLA" class="col-md-6 .img-fluid"></div>
        <div class="col col-md-6"></div>
        <div class="col">
            <div class="float-right"><span class="align-middle"><?= $portal->su0013 ?></span>
                <a href="<?= $GLOBALS["domain_root1"] . $portal->su0005 ?>">
                    <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/salir.png" alt="exit" class="col-md-5 .img-fluid">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container mt-4">    
    <div class="row">
        <div class="col-md-2 mt-3">
            <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/preguntarSelected.png" alt="pregunta SPLA" class="col-md-7 .img-fluid"><br>
            <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/limpiar.png" alt="limpiar pregunta SPLA" id="limpiar" class="col-md-8 .img-fluid pointer">
        </div>
        <div class="col mt-3">
            <form>
                <div class="form-group">
                    <label for="inputTema"><?= $portal->su0007 ?></label>
                    <input type="text" class="form-control" id="inputTema" name="inputTema" maxlength="50">
                </div>
                
                <div class="form-group">
                    <label for="descrip"><?= $portal->su0008 ?></label>
                    <textarea class="form-control" id="descrip" name="descrip" maxlength="200" rows="7"></textarea>
                </div>
                
                <div class="form-group">
                    <label for="inputEmail"><?= $portal->su0009 ?></label>
                    <input type="text" class="form-control" id="inputEmail" name="inputEmail" maxlength="50">
                </div>
                
                <button type="button" class="btn btn-primary float-right" id="enviarEmail"><?= $portal->su0010 ?></button>
            </form>
        </div>
    </div>
</div>

<script>
    $(function(){
        $("#limpiar").hover(function(){
            $("#limpiar").attr("src", "<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/limpiarSelected.png");
        }, function(){
            // change to any color that was previously used.
            $("#limpiar").attr("src", "<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/limpiar.png");
        });
        
        $("#limpiar").click(function(){
            $("#inputTema").val("");
            $("#descrip").val("");
            $("#inputEmail").val("");
            $("#inputTema").focus();
        });
               
        $("#enviarEmail").click(function(){
            if($("#inputTema").val() === ""){
                $.alert.open('warning', "<?= $portal->su0021 ?>", "<?= 'Debe agregar el Tema' ?>", {'<?= $portal->su0022 ?>' : '<?= $portal->su0022 ?>'}, function(){
                    $("#inputTema").focus();
                });
                return false;
            }
            
            if($("#descrip").val() === ""){
                $.alert.open('warning', "<?= $portal->su0021 ?>", "<?= 'Debe agregar la Descripción del tema' ?>", {'<?= $portal->su0022 ?>' : '<?= $portal->su0022 ?>'}, function(){
                    $("#descrip").focus();
                });
                return false;
            }
            
            if($("#inputEmail").val() === ""){
                $.alert.open('warning', "<?= $portal->su0021 ?>", "<?= 'Debe agregar el correo donde podamos contactarlo' ?>", {'<?= $portal->su0022 ?>' : '<?= $portal->su0022 ?>'}, function(){
                    $("#inputEmail").focus();
                });
                return false;
            }
            
            email = $("#inputEmail").val();            
            email = email.toLowerCase();
            if(email.indexOf("hotmail") > 0 || email.indexOf("gmail") > 0 || email.indexOf("yahoo") > 0){
                $.alert.open('warning', "<?= $portal->su0021 ?>", "<?= 'Debe ser un correo corporativo' ?>", {'<?= $portal->su0022 ?>' : '<?= $portal->su0022 ?>'}, function(){
                    $("#inputEmail").focus();
                });
                return false;
            }
            
            $("#fondo").show();
            $.post("<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/ajax/emailSend.php", { tema : $("#inputTema").val(), descrip : $("#descrip").val(), email : $("#inputEmail").val() }, function(data){
                $("#fondo").hide();
                if(data[0].result === true){
                    location.href = "<?= $GLOBALS["domain_root1"] . $portal->su0019 ?>";
                } else{
                    $.alert.open('warning', "<?= $portal->su0021 ?>", "<?= $portal->su0020 ?>", {'<?= $portal->su0022 ?>' : '<?= $portal->su0022 ?>'});
                } 
            }, 'json')
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error", "Error: " + jqXHR.status, {'<?= $portal->su0022 ?>' : '<?= $portal->su0022 ?>'});
            });
        });
    });
</script>