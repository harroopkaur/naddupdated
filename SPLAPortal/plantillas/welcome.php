<div class="container-fluid cabeceraWelcome">
    <div class="row">
        <div class="col col-md-3"><img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/logo.png" alt="SPLA" class="col-md-6 .img-fluid"></div>
        <div class="col col-md-6"></div>
        <div class="col">
            <div class="float-right"><span class="align-middle"><?= $portal->su0013 ?></span>
                <a href="<?= $GLOBALS["domain_root1"] . $portal->su0005 ?>">
                    <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/salir.png" alt="exit" class="col-md-5 .img-fluid">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container mt-4">    
    <div class="row">
        <div class="col-md-2 mt-3">
            <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/preguntar.png" alt="pregunta SPLA" class="col-md-7 .img-fluid"><br>
            <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/limpiar.png" alt="limpiar pregunta SPLA" class="col-md-8 .img-fluid">
        </div>
        <div class="col mt-3 border">
            <p class="h3 text-center colorTituloCabeceraWelcome"><?= $portal->su0002 ?></p>
            <p class="h5 text-justify colorParrafoCabeceraWelcome"><?= $portal->su0003 ?></p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-2 mt-5"></div>
        <div class="col mt-5">
            <div class="row">
                <div class="col-12 btn btn-primary" id="ingresar"><?= $portal->su0004 ?></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-2 mt-5"></div>
        <div class="col-md-10 mt-5"> 
            <div class="row">
                <div class="col-md-4">
                    <a href="https://www.licensingassurance.com/contact.php" target="_blank">
                        <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/linkDemo.png" alt="SPLA Demo" class="col-md-12 .img-fluid border">        
                    </a>
                </div>
                
                <div class="col-md-4">
                    <a href="https://www.licensingassurance.com/PortafolioSPLA.pdf" target="_blank">
                        <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/descargarApp.png" alt="SPLA App" class="col-md-12 .img-fluid border">
                    </a>
                </div>
                
                <div class="col-md-4">
                    <a href="https://www.licensingassurance.com/contact.php" target="_blank">
                        <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/consultaAuditoria.png" alt="SPLA Audit" class="col-md-12 .img-fluid border">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
       $("#ingresar").click(function(){
           location.href = "<?= $GLOBALS["domain_root1"] . $portal->su0006 ?>";
       }); 
    });
</script>