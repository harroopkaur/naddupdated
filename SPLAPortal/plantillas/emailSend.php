<div class="container-fluid cabeceraWelcome">
    <div class="row">
        <div class="col col-md-3"><img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/logo.png" alt="SPLA" class="col-md-6 .img-fluid"></div>
        <div class="col col-md-6"></div>
        <div class="col">
            <div class="float-right"><span class="align-middle"><?= $portal->su0013 ?></span>
                <a href="<?= $GLOBALS["domain_root1"] . $portal->su0005 ?>">
                    <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/salir.png" alt="exit" class="col-md-5 .img-fluid">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container mt-4">    
    <div class="row">
        <div class="col-md-2 mt-3">
            <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/preguntar.png" alt="pregunta SPLA" class="col-md-7 .img-fluid"><br>
            <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/limpiar.png" alt="limpiar pregunta SPLA" id="limpiar" class="col-md-8 .img-fluid pointer">
        </div>
        <div class="col mt-3">
            <div class="row">
                <div class="col-12 fondoEmailSend">
                    <div class="row justify-content-md-center">
                        <div class="col-4 fondoImgEmailSend">
                            <img src="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/img/logo.png" alt="SPLA" class="col-md-12 .img-fluid">
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12 mt-3"></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 text-center cabeceraWelcome colorParrafoEmailSend h4">
                    <p class="mt-4"><?= $portal->su0023 ?></p>
                    <p class="mt-4"></p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 fondoEmailSend">
                    <div class="row justify-content-md-center">
                        <div class="col-4 cabeceraWelcome mt-4 text-center">
                            <a href="https://www.licensingassurance.com" class="hipervinculoEmailSend mt-4">https://www.licensingassurance.com</a>
                            <div class="mt-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>