<!doctype html>
<html lang="<?= $portal->su0001 ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="<?= $GLOBALS["domain_root1"] ?>/img/Logo.ico">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="<?= $GLOBALS["domain_root1"] ?>/SPLAPortal/css/estilos.css">
        <link href="<?= $GLOBALS["domain_root1"] ?>/webtool/plugin-alert/css/alert.min.css" rel="stylesheet" />
        <link href="<?= $GLOBALS["domain_root1"] ?>/webtool/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        
        <title><?= $portal->su0011 ?></title>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>-->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script src="<?= $GLOBALS["domain_root1"] ?>/webtool/plugin-alert/js/alert.js"></script>
    </head>
    <body>