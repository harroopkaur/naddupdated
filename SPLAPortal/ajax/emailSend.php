<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_email.php");

$array = array(0=>array('result'=>false));

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    $general     = new General();
    $email = new email();
    
    /*$corr = "<m_acero_n@hotmail.com>";
    $copy = "<it@licensingassurance.com>,<colososhark@gmail.com>";*/
    $corr = "<maribelperozo@licensingassurance.com>";
    $copy = "<gerardoaldana@licensingassurance.com>, <gerardoaldana@licensingassurance.com>,<dannarojas@licensingassurance.com>";
    
    $tema = "";
    if(isset($_POST["tema"])){
        $tema = $general->get_escape($_POST["tema"]);
    }
    
    $descrip = "";
    if(isset($_POST["descrip"])){
        $descrip = $general->get_escape($_POST["descrip"]);
    }
    
    $correo = "";
    if(isset($_POST["email"])){
        $correo = $general->get_escape($_POST["email"]);
    }
    
    $result = false;    
    if($email->enviar_preguntaSPLA($corr, $copy, "Tema SPLA: " . $tema, $descrip, $correo)){
        $result = true;
    }
    
    $array = array(0=>array('result'=>$result, 'error'=>$error));
}
echo json_encode($array);